
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `c_password` varchar(255) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_login` datetime NOT NULL,
  `ip_address` varchar(50) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes- Deleted Data, No - Not Deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `username`, `password`, `c_password`, `email`, `last_login`, `ip_address`, `is_deleted`) VALUES
(1, 'Administrator', '202cb962ac59075b964b07152d234b70', '', 'admin@jobportal.com', '2019-03-26 08:45:37', '192.168.1.162', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_master`
--

CREATE TABLE `advertisement_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `type` enum('Google','Image') CHARACTER SET utf8 NOT NULL DEFAULT 'Image',
  `link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `google_adsense` text CHARACTER SET utf8 NOT NULL,
  `level` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_master`
--

CREATE TABLE `blog_master` (
  `id` int(11) NOT NULL,
  `status` enum('UNAPPROVED','APPROVED') CHARACTER SET utf8 NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `alias` text CHARACTER SET utf8 NOT NULL,
  `content` longblob NOT NULL,
  `created_on` datetime NOT NULL,
  `blog_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_master`
--

INSERT INTO `blog_master` (`id`, `status`, `title`, `alias`, `content`, `created_on`, `blog_image`, `is_deleted`) VALUES
(1, 'APPROVED', 'Blog 1', 'blog-1', 0x3c68333e456d626564204375726174656420436f6e74656e74206f6e20596f757220426c6f673c2f68333e0d0a0d0a3c703e54686572652061726520612076617269657479206f6620746f6f6c7320746861742063616e2073747265616d6c696e652074686520636f6e74656e74206375726174696f6e2070726f636573732c20616e6420736f6d65206f662074686f736520746f6f6c7320656e61626c6520796f7520746f20656d6265642074686520636f6e74656e7420796f7520637572617465206f6e20796f757220626c6f672e205479706963616c6c792c2074686520666f726d617474696e6720697320646f6e6520666f7220796f752c20736f207468652070726f6365737320697320717569636b20616e6420656173792e20596f75206a757374207069636b2074686520736f75726365732c2061646420796f757220636f6d6d656e7461727920746f20656163682c20636f707920616e6420706173746520736f6d6520656d62656420636f646520696e746f206120626c6f6720706f7374206f7220626c6f6720706167652c20636c69636b20746865207075626c69736820627574746f6e2c20616e6420796f75262333393b726520646f6e652e20466f72206578616d706c652c20746f6f6c73206c696b65c2a0616e64c2a0c2a0626f7468206f666665722065617379207761797320746f20656d626564206375726174656420636f6e74656e74206f6e20796f757220626c6f672e20596f752063616e2073656520616e206578616d706c65206f66206375726174656420636f6e74656e7420656d62656464656420696e746f206120626c6f672070616765207573696e672074686520526562656c6d6f75736520746f6f6c206f6e202e3c2f703e, '2018-12-31 07:21:20', '7fd6292e2206f9e100bf0febe6a00d13.png', 'No'),
(2, 'APPROVED', 'The New Yorker', 'xdcgfdxgh', 0x3c703e6465777165647771727765713c2f703e, '2018-12-31 08:31:00', '1ac9556c002792e64837b66ee5190030.jpg', 'No'),
(3, 'APPROVED', 'xcbcx', 'xcbcx', 0x3c703e6366766e62646a673c2f703e, '2018-12-31 08:38:41', '', 'Yes'),
(4, 'APPROVED', 'sdfgsdgdsggdDay', 'sdfgsdgdsggdday', 0x3c703e7364677364673c2f703e, '2019-01-07 06:54:51', '', 'No'),
(5, 'APPROVED', 'blog postsd11', 'blog-postsd11', 0x3c703e73626c6f6720706f737473643131c2a0626c6f6720706f737473643131207620762076207676626c6f6720706f737473643131626c6f6720706f737473643131626c6f6720706f737473643131626c6f6720706f737473643131626c6f6720706f7374736431313c2f703e, '2019-02-22 03:44:00', '77f0b32eaadbc573e24d719b04a272d8.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city_master`
--

CREATE TABLE `city_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `city_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_master`
--

INSERT INTO `city_master` (`id`, `status`, `city_name`, `country_id`, `state_id`, `is_deleted`) VALUES
(1, 'APPROVED', 'Ahmedabad', 1, 1, 'No'),
(2, 'APPROVED', 'Vadodara', 1, 1, 'No'),
(3, 'APPROVED', 'Surat', 1, 1, 'No'),
(4, 'APPROVED', 'Mumbai', 1, 29, 'No'),
(5, 'APPROVED', 'Pune', 1, 29, 'No'),
(6, 'APPROVED', 'Jalandhar', 1, 2, 'No'),
(7, 'APPROVED', 'Punjab Kot', 1, 2, 'No'),
(8, 'APPROVED', 'Sydney', 3, 4, 'No'),
(9, 'APPROVED', 'Valsad', 1, 1, 'No'),
(10, 'APPROVED', 'Wollongong', 3, 4, 'No'),
(11, 'APPROVED', 'Brisbane', 3, 6, 'No'),
(12, 'APPROVED', 'Adelaide', 3, 7, 'No'),
(13, 'APPROVED', 'Hobart', 3, 8, 'No'),
(14, 'APPROVED', 'Melbourne', 3, 9, 'No'),
(15, 'APPROVED', 'Perth', 3, 10, 'No'),
(16, 'APPROVED', 'Airdrie', 5, 12, 'No'),
(17, 'APPROVED', 'Camrose', 5, 12, 'No'),
(18, 'APPROVED', 'Brampton', 5, 11, 'No'),
(19, 'APPROVED', 'Cambridge', 5, 11, 'No'),
(20, 'APPROVED', 'Mississauga', 5, 11, 'No'),
(21, 'APPROVED', 'Jamnagar', 1, 1, 'No'),
(22, 'APPROVED', 'Rajkot', 1, 1, 'No'),
(23, 'APPROVED', 'Bhavnagar', 1, 1, 'No'),
(24, 'APPROVED', 'Gandhinagar', 1, 1, 'No'),
(25, 'APPROVED', 'Mehsana', 1, 1, 'No'),
(26, 'APPROVED', 'Kutch', 1, 1, 'No'),
(27, 'APPROVED', 'Surendranagar', 1, 1, 'No'),
(28, 'APPROVED', 'Delhi', 1, 19, 'No'),
(29, 'APPROVED', 'Bengaluru', 1, 25, 'No'),
(30, 'APPROVED', 'Hyderabad', 1, 39, 'No'),
(31, 'APPROVED', 'Chennai', 1, 38, 'No'),
(32, 'APPROVED', 'Kolkata', 1, 43, 'No'),
(33, 'APPROVED', 'Jaipur', 1, 36, 'No'),
(34, 'APPROVED', 'Lucknow', 1, 41, 'No'),
(35, 'APPROVED', 'Kanpur', 1, 41, 'No'),
(36, 'APPROVED', 'Nagpur', 1, 29, 'No'),
(37, 'APPROVED', 'Visakhapatnam', 1, 13, 'No'),
(38, 'APPROVED', 'Indore', 1, 28, 'No'),
(39, 'APPROVED', 'Thane', 1, 29, 'No'),
(40, 'APPROVED', 'Bhopal', 1, 28, 'No'),
(41, 'APPROVED', 'Pimpri-Chinchwad', 1, 29, 'No'),
(42, 'APPROVED', 'Patna', 1, 16, 'No'),
(43, 'APPROVED', 'Ghaziabad', 1, 41, 'No'),
(44, 'APPROVED', 'Ludhiana', 1, 2, 'No'),
(45, 'APPROVED', 'Coimbatore', 1, 38, 'No'),
(46, 'APPROVED', 'Agra', 1, 41, 'No'),
(47, 'APPROVED', 'Madurai', 1, 38, 'No'),
(48, 'APPROVED', 'Nashik', 1, 29, 'No'),
(49, 'APPROVED', 'Faridabad', 1, 21, 'No'),
(50, 'APPROVED', 'Meerut', 1, 41, 'No'),
(51, 'APPROVED', 'Kalyan-Dombivali', 1, 29, 'No'),
(52, 'APPROVED', 'Vasai-Virar', 1, 29, 'No'),
(53, 'APPROVED', 'Varanasi', 1, 41, 'No'),
(54, 'APPROVED', 'Srinagar', 1, 23, 'No'),
(55, 'APPROVED', 'Aurangabad', 1, 29, 'No'),
(56, 'APPROVED', 'Dhanbad', 1, 24, 'No'),
(57, 'APPROVED', 'Amritsar', 1, 2, 'No'),
(58, 'APPROVED', 'Navi Mumbai', 1, 29, 'No'),
(59, 'APPROVED', 'Allahabad', 1, 41, 'No'),
(60, 'APPROVED', 'Ranchi', 1, 24, 'No'),
(61, 'APPROVED', 'Howrah', 1, 43, 'No'),
(62, 'APPROVED', 'Jabalpur', 1, 28, 'No'),
(63, 'APPROVED', 'Gwalior', 1, 28, 'No'),
(64, 'APPROVED', 'Vijayawada', 1, 13, 'No'),
(65, 'APPROVED', 'Jodhpur', 1, 36, 'No'),
(66, 'APPROVED', 'Raipur', 1, 18, 'No'),
(67, 'APPROVED', 'Kota', 1, 36, 'No'),
(68, 'APPROVED', 'Guwahati', 1, 15, 'No'),
(69, 'APPROVED', 'Chandigarh', 1, 17, 'No'),
(70, 'APPROVED', 'Thiruvananthapuram', 1, 26, 'No'),
(71, 'APPROVED', 'Guntur', 1, 13, 'No'),
(72, 'APPROVED', 'Bhilai', 1, 18, 'No'),
(73, 'APPROVED', 'Gurgaon', 1, 21, 'No'),
(74, 'APPROVED', 'Jamshedpur', 1, 24, 'No'),
(75, 'APPROVED', 'Hubballi-Dharwad', 1, 25, 'No'),
(76, 'APPROVED', 'Mysore', 1, 25, 'No'),
(77, 'APPROVED', 'Kochi', 1, 26, 'No'),
(78, 'APPROVED', 'Solapur', 1, 29, 'No'),
(79, 'APPROVED', 'Mira-Bhayandar', 1, 29, 'No'),
(80, 'APPROVED', 'Bhiwandi', 1, 29, 'No'),
(81, 'APPROVED', 'Amravati', 1, 29, 'No'),
(82, 'APPROVED', 'Bhubaneswar', 1, 34, 'No'),
(83, 'APPROVED', 'Cuttack', 1, 34, 'No'),
(84, 'APPROVED', 'Bikaner', 1, 36, 'No'),
(85, 'APPROVED', 'Tiruchirappalli', 1, 38, 'No'),
(86, 'APPROVED', 'Tiruppur', 1, 38, 'No'),
(87, 'APPROVED', 'Salem', 1, 38, 'No'),
(88, 'APPROVED', 'Warangal', 1, 39, 'No'),
(89, 'APPROVED', 'Bareilly', 1, 41, 'No'),
(90, 'APPROVED', 'Moradabad', 1, 41, 'No'),
(91, 'APPROVED', 'Aligarh', 1, 41, 'No'),
(92, 'APPROVED', 'Saharanpur', 1, 41, 'No'),
(93, 'APPROVED', 'Gorakhpur', 1, 41, 'No'),
(94, 'APPROVED', 'Noida', 1, 41, 'No'),
(95, 'APPROVED', 'Firozabad', 1, 41, 'No'),
(96, 'APPROVED', 'Nellore', 1, 13, 'No'),
(97, 'APPROVED', 'Dehradun', 1, 42, 'No'),
(98, 'APPROVED', 'Durgapur', 1, 43, 'No'),
(99, 'APPROVED', 'Asansol', 1, 43, 'No'),
(100, 'APPROVED', 'Rourkela', 1, 34, 'No'),
(101, 'APPROVED', 'Nanded', 1, 29, 'No'),
(102, 'APPROVED', 'Kolhapur', 1, 29, 'No'),
(103, 'APPROVED', 'Ajmer', 1, 36, 'No'),
(104, 'APPROVED', 'Gulbarga', 1, 25, 'No'),
(105, 'APPROVED', 'Ujjain', 1, 28, 'No'),
(106, 'APPROVED', 'Loni', 1, 41, 'No'),
(107, 'APPROVED', 'Siliguri', 1, 43, 'No'),
(108, 'APPROVED', 'Jhansi', 1, 41, 'No'),
(109, 'APPROVED', 'Ulhasnagar', 1, 29, 'No'),
(110, 'APPROVED', 'Jammu', 1, 23, 'No'),
(111, 'APPROVED', 'Sangli-Miraj & Kupwad', 1, 29, 'No'),
(112, 'APPROVED', 'Mangalore', 1, 25, 'No'),
(113, 'APPROVED', 'Erode', 1, 38, 'No'),
(114, 'APPROVED', 'Belgaum', 1, 25, 'No'),
(115, 'APPROVED', 'Ambattur', 1, 38, 'No'),
(116, 'APPROVED', 'Tirunelveli', 1, 38, 'No'),
(117, 'APPROVED', 'Malegaon', 1, 29, 'No'),
(118, 'APPROVED', 'Gaya', 1, 16, 'No'),
(119, 'APPROVED', 'Jalgaon', 1, 29, 'No'),
(120, 'APPROVED', 'Udaipur', 1, 36, 'No'),
(121, 'APPROVED', 'Maheshtala', 1, 43, 'No'),
(122, 'APPROVED', 'Davanagere', 1, 25, 'No'),
(123, 'APPROVED', 'Kozhikode', 1, 26, 'No'),
(124, 'APPROVED', 'Akola', 1, 29, 'No'),
(125, 'APPROVED', 'Kurnool', 1, 13, 'No'),
(126, 'APPROVED', 'Rajpur Sonarpur', 1, 43, 'No'),
(127, 'APPROVED', 'Rajahmundry', 1, 13, 'No'),
(128, 'APPROVED', 'Bokaro', 1, 24, 'No'),
(129, 'APPROVED', 'South Dumdum', 1, 43, 'No'),
(130, 'APPROVED', 'Bellary', 1, 25, 'No'),
(131, 'APPROVED', 'Patiala', 1, 2, 'No'),
(132, 'APPROVED', 'Gopalpur', 1, 43, 'No'),
(133, 'APPROVED', 'Agartala', 1, 40, 'No'),
(134, 'APPROVED', 'Bhagalpur', 1, 16, 'No'),
(135, 'APPROVED', 'Muzaffarnagar', 1, 41, 'No'),
(136, 'APPROVED', 'Bhatpara', 1, 43, 'No'),
(137, 'APPROVED', 'Panihati', 1, 43, 'No'),
(138, 'APPROVED', 'Latur', 1, 29, 'No'),
(139, 'APPROVED', 'Dhule', 1, 29, 'No'),
(140, 'APPROVED', 'Tirupati', 1, 13, 'No'),
(141, 'APPROVED', 'Rohtak', 1, 21, 'No'),
(142, 'APPROVED', 'Korba', 1, 18, 'No'),
(143, 'APPROVED', 'Bhilwara', 1, 36, 'No'),
(144, 'APPROVED', 'Berhampur', 1, 34, 'No'),
(145, 'APPROVED', 'Bilaspur', 1, 18, 'No'),
(146, 'APPROVED', 'Satara', 1, 29, 'No'),
(147, 'APPROVED', 'Bijapur', 1, 25, 'No'),
(148, 'APPROVED', 'Alwar', 1, 36, 'No'),
(149, 'APPROVED', 'Panipat', 1, 21, 'No'),
(150, 'APPROVED', 'Darbhanga', 1, 16, 'No'),
(151, 'APPROVED', 'Bathinda', 1, 2, 'No'),
(152, 'APPROVED', 'Sonipat', 1, 21, 'No'),
(153, 'APPROVED', 'Ratlam', 1, 28, 'No'),
(154, 'APPROVED', 'Bharatpur', 1, 36, 'No'),
(155, 'APPROVED', 'Begusarai', 1, 16, 'No'),
(156, 'APPROVED', 'Gandhidham', 1, 1, 'No'),
(157, 'APPROVED', 'Puducherry', 1, 35, 'No'),
(158, 'APPROVED', 'Pali', 1, 36, 'No'),
(159, 'APPROVED', 'Vellore', 1, 38, 'No'),
(160, 'APPROVED', 'Shimla', 1, 22, 'No'),
(161, 'APPROVED', 'Itanagar', 1, 14, 'No'),
(162, 'APPROVED', 'Dispur', 1, 15, 'No'),
(163, 'APPROVED', 'Panaji', 1, 20, 'No'),
(164, 'APPROVED', 'Shillong', 1, 31, 'No'),
(165, 'APPROVED', 'Imphal', 1, 30, 'No'),
(166, 'APPROVED', 'Aizawl', 1, 32, 'No'),
(167, 'APPROVED', 'Kohima', 1, 33, 'No'),
(168, 'APPROVED', 'Gangtok', 1, 37, 'No'),
(169, 'APPROVED', 'Dubai', 4, 5, 'No'),
(170, 'APPROVED', 'Abu Dhabi', 4, 44, 'No'),
(171, 'APPROVED', 'Sharjah', 4, 45, 'No'),
(172, 'APPROVED', 'Ajman', 4, 46, 'No'),
(173, 'APPROVED', 'Fujairah', 4, 48, 'No'),
(174, 'APPROVED', 'Umm al-Quwain', 4, 49, 'No'),
(175, 'APPROVED', 'Ras Al Khaimah', 4, 47, 'No'),
(176, 'APPROVED', 'Al Ain', 4, 44, 'No'),
(177, 'APPROVED', 'Dibba', 4, 48, 'No'),
(178, 'APPROVED', 'Khor Fakkan', 4, 45, 'No'),
(179, 'APPROVED', 'Jebel Ali', 4, 5, 'No'),
(180, 'APPROVED', 'Liwa', 4, 44, 'No'),
(181, 'APPROVED', 'Zayed City', 4, 44, 'No'),
(182, 'APPROVED', 'Hatta', 4, 5, 'No'),
(183, 'APPROVED', 'Al Salam City', 4, 49, 'No'),
(184, 'APPROVED', 'Auckland', 8, 50, 'No'),
(185, 'APPROVED', 'Christchurch', 8, 50, 'No'),
(186, 'APPROVED', 'Wellington', 8, 53, 'No'),
(187, 'APPROVED', 'Hamilton', 8, 53, 'No'),
(188, 'APPROVED', 'Dunedin', 8, 56, 'No'),
(189, 'APPROVED', 'Canterbury', 8, 57, 'No'),
(190, 'APPROVED', 'Hawke\'s Bay', 8, 52, 'No'),
(191, 'APPROVED', 'Marlborough', 8, 55, 'No'),
(192, 'APPROVED', 'Nelson', 8, 54, 'No'),
(193, 'APPROVED', 'New Plymouth', 8, 51, 'No'),
(194, 'APPROVED', 'Otago', 8, 58, 'No'),
(195, 'APPROVED', 'Southland', 8, 59, 'No'),
(196, 'APPROVED', 'Quetta', 2, 60, 'No'),
(197, 'APPROVED', 'Turbat', 2, 60, 'No'),
(198, 'APPROVED', 'Islamabad', 2, 61, 'No'),
(199, 'APPROVED', 'Peshawar', 2, 62, 'No'),
(200, 'APPROVED', 'Abbottabad', 2, 62, 'No'),
(201, 'APPROVED', 'Dera Ismail Khan', 2, 62, 'No'),
(202, 'APPROVED', 'Lahore', 2, 63, 'No'),
(203, 'APPROVED', 'Faisalabad', 2, 63, 'No'),
(204, 'APPROVED', 'Rawalpindi', 2, 63, 'No'),
(205, 'APPROVED', 'Multan', 2, 63, 'No'),
(206, 'APPROVED', 'Sialkot', 2, 63, 'No'),
(207, 'APPROVED', 'Karachi', 2, 64, 'No'),
(208, 'APPROVED', 'Hyderabad', 2, 64, 'No'),
(209, 'APPROVED', 'Mirpur Khas', 2, 64, 'No'),
(210, 'APPROVED', 'Jacobabad', 2, 64, 'No'),
(211, 'APPROVED', 'East London', 9, 65, 'No'),
(212, 'APPROVED', 'Port Elizabeth', 9, 65, 'No'),
(213, 'APPROVED', 'Queenstown', 9, 65, 'No'),
(214, 'APPROVED', 'Bethlehem', 9, 66, 'No'),
(215, 'APPROVED', 'Virginia', 9, 66, 'No'),
(216, 'APPROVED', 'Johannesburg', 9, 67, 'No'),
(217, 'APPROVED', 'Randburg', 9, 67, 'No'),
(218, 'APPROVED', 'Randfontein', 9, 67, 'No'),
(219, 'APPROVED', 'Durban', 9, 68, 'No'),
(220, 'APPROVED', 'Pietermaritzburg', 9, 68, 'No'),
(221, 'APPROVED', 'Pinetown', 9, 68, 'No'),
(222, 'APPROVED', 'Giyani', 9, 69, 'No'),
(223, 'APPROVED', 'Phalaborwa', 9, 69, 'No'),
(224, 'APPROVED', 'Emalahleni', 9, 70, 'No'),
(225, 'APPROVED', 'Secunda', 9, 70, 'No'),
(226, 'APPROVED', 'Klerksdorp', 9, 71, 'No'),
(227, 'APPROVED', 'Mahikeng', 9, 71, 'No'),
(228, 'APPROVED', 'Potchefstroom', 9, 71, 'No'),
(229, 'APPROVED', 'Kimberley', 9, 72, 'No'),
(230, 'APPROVED', 'Port Nolloth', 9, 72, 'No'),
(231, 'APPROVED', 'Bellville', 9, 73, 'No'),
(232, 'APPROVED', 'Cape Town', 9, 73, 'No'),
(233, 'APPROVED', 'George', 9, 73, 'No'),
(234, 'APPROVED', 'Paarl', 9, 73, 'No'),
(235, 'APPROVED', 'Worcester', 9, 73, 'No'),
(236, 'APPROVED', 'London', 7, 74, 'No'),
(237, 'APPROVED', 'Birmingham', 7, 74, 'No'),
(238, 'APPROVED', 'Liverpool', 7, 74, 'No'),
(239, 'APPROVED', 'Bristol', 7, 74, 'No'),
(240, 'APPROVED', 'Manchester', 7, 74, 'No'),
(241, 'APPROVED', 'Sheffield', 7, 74, 'No'),
(242, 'APPROVED', 'Leeds', 7, 74, 'No'),
(243, 'APPROVED', 'Leicester', 7, 74, 'No'),
(244, 'APPROVED', 'Armagh', 7, 75, 'No'),
(245, 'APPROVED', 'Belfast', 7, 75, 'No'),
(246, 'APPROVED', 'Londonderry', 7, 75, 'No'),
(247, 'APPROVED', 'Lisburn', 7, 75, 'No'),
(248, 'APPROVED', 'Newry', 7, 75, 'No'),
(249, 'APPROVED', 'Aberdeen', 7, 76, 'No'),
(250, 'APPROVED', 'Dundee', 7, 76, 'No'),
(251, 'APPROVED', 'Edinburgh', 7, 76, 'No'),
(252, 'APPROVED', 'Glasgow', 7, 76, 'No'),
(253, 'APPROVED', 'Inverness', 7, 76, 'No'),
(254, 'APPROVED', 'Stirling', 7, 76, 'No'),
(255, 'APPROVED', 'Bangor', 7, 77, 'No'),
(256, 'APPROVED', 'Cardiff', 7, 77, 'No'),
(257, 'APPROVED', 'Newport', 7, 77, 'No'),
(258, 'APPROVED', 'St Davids', 7, 77, 'No'),
(259, 'APPROVED', 'Swansea', 7, 77, 'No'),
(260, 'APPROVED', 'Cambridge', 7, 74, 'No'),
(261, 'APPROVED', 'York', 7, 74, 'No'),
(262, 'APPROVED', 'Nottingham', 7, 74, 'No'),
(263, 'APPROVED', 'Kingston', 7, 74, 'No'),
(264, 'APPROVED', 'Birmingham', 6, 78, 'No'),
(265, 'APPROVED', 'Montgomery', 6, 78, 'No'),
(266, 'APPROVED', 'Mobile', 6, 78, 'No'),
(267, 'APPROVED', 'Huntsville', 6, 78, 'No'),
(268, 'APPROVED', 'Tuscaloosa', 6, 78, 'No'),
(269, 'APPROVED', 'Anchorage', 6, 79, 'No'),
(270, 'APPROVED', 'Fairbanks', 6, 79, 'No'),
(271, 'APPROVED', 'Juneau', 6, 79, 'No'),
(272, 'APPROVED', 'Ketchikan', 6, 79, 'No'),
(273, 'APPROVED', 'Phoenix', 6, 80, 'No'),
(274, 'APPROVED', 'Tucson', 6, 80, 'No'),
(275, 'APPROVED', 'Mesa', 6, 80, 'No'),
(276, 'APPROVED', 'Glendale', 6, 80, 'No'),
(277, 'APPROVED', 'Little Rock', 6, 81, 'No'),
(278, 'APPROVED', 'Fort Smith', 6, 81, 'No'),
(279, 'APPROVED', 'Fayetteville', 6, 81, 'No'),
(280, 'APPROVED', 'Los Angeles', 6, 82, 'No'),
(281, 'APPROVED', 'San Diego', 6, 82, 'No'),
(282, 'APPROVED', 'San Jose', 6, 82, 'No'),
(283, 'APPROVED', 'San Francisco', 6, 82, 'No'),
(284, 'APPROVED', 'Denver', 6, 83, 'No'),
(285, 'APPROVED', 'Colorado Springs', 6, 83, 'No'),
(286, 'APPROVED', 'Aurora', 6, 83, 'No'),
(287, 'APPROVED', 'Fort Collins', 6, 83, 'No'),
(288, 'APPROVED', 'Lakewood', 6, 83, 'No'),
(289, 'APPROVED', 'Chicago', 6, 84, 'No'),
(290, 'APPROVED', 'Aurora', 6, 84, 'No'),
(291, 'APPROVED', 'Rockford', 6, 84, 'No'),
(292, 'APPROVED', 'Joliet', 6, 84, 'No'),
(293, 'APPROVED', 'Naperville', 6, 84, 'No'),
(294, 'APPROVED', 'Boston', 6, 85, 'No'),
(295, 'APPROVED', 'Worcester', 6, 85, 'No'),
(296, 'APPROVED', 'Springfield', 6, 85, 'No'),
(297, 'APPROVED', 'Lowell', 6, 85, 'No'),
(298, 'APPROVED', 'Cambridge', 6, 85, 'No'),
(299, 'APPROVED', 'Detroit', 6, 86, 'No'),
(300, 'APPROVED', 'Grand Rapids', 6, 86, 'No'),
(301, 'APPROVED', 'Warren', 6, 86, 'No'),
(302, 'APPROVED', 'Sterling Heights', 6, 86, 'No'),
(303, 'APPROVED', 'Ann Arbor', 6, 86, 'No'),
(304, 'APPROVED', 'New York City', 6, 87, 'No'),
(305, 'APPROVED', 'Buffalo', 6, 87, 'No'),
(306, 'APPROVED', 'Rochester', 6, 87, 'No'),
(307, 'APPROVED', 'Yonkers', 6, 87, 'No'),
(308, 'APPROVED', 'Syracuse', 6, 87, 'No'),
(309, 'APPROVED', 'Cleveland', 6, 88, 'No'),
(310, 'APPROVED', 'Cincinnati', 6, 88, 'No'),
(311, 'APPROVED', 'Toledo', 6, 88, 'No'),
(312, 'APPROVED', 'Philadelphia', 6, 89, 'No'),
(313, 'APPROVED', 'Pittsburgh', 6, 89, 'No'),
(314, 'APPROVED', 'Allentown', 6, 89, 'No'),
(315, 'APPROVED', 'Erie', 6, 89, 'No'),
(316, 'APPROVED', 'Memphis', 6, 90, 'No'),
(317, 'APPROVED', 'Nashville', 6, 90, 'No'),
(318, 'APPROVED', 'Knoxville', 6, 90, 'No'),
(319, 'APPROVED', 'Chattanooga', 6, 90, 'No'),
(320, 'APPROVED', 'Seattle', 6, 91, 'No'),
(321, 'APPROVED', 'Spokane', 6, 91, 'No'),
(322, 'APPROVED', 'Tacoma', 6, 91, 'No'),
(323, 'APPROVED', 'Vancouver', 6, 91, 'No'),
(324, 'APPROVED', 'Test', 1, 36, 'Yes'),
(325, 'UNAPPROVED', '1323123123123', 8, 50, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 DEFAULT 'APPROVED',
  `page_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `page_content` longtext CHARACTER SET utf8 NOT NULL,
  `page_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `status`, `page_title`, `page_content`, `page_url`, `is_deleted`) VALUES
(24, 'APPROVED', 'About us', '<div class=\"sixteen columns\">\r\n<div class=\"padding-right\">Terms And Conditions\r\n<div class=\"company-info\">\r\n<div class=\"content\">\r\n<h4>Advantages  %</h4>\r\n</div>\r\n\r\n<div class=\"clearfix\"> </div>\r\n</div>\r\n\r\n<p><strong>1.</strong>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n\r\n<p><strong>2.</strong>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>\r\n\r\n<p><strong>3.</strong>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth.</p>\r\n\r\n<ul>\r\n <li>Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!</li>\r\n <li>Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.</li>\r\n <li>Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.</li>\r\n <li>We’re looking for associates who enjoy interacting with people and working in a fast-paced environment!</li>\r\n</ul>\r\n\r\n<div class=\"company-info\">\r\n<div class=\"content\">\r\n<h4>Lorem ipsum dolor sit amet.</h4>\r\n</div>\r\n\r\n<div class=\"clearfix\"> </div>\r\n</div>\r\n\r\n<p><strong>1.</strong>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\r\n\r\n<p><strong>2.</strong>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>\r\n\r\n<p><strong>3.</strong>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth.</p>\r\n\r\n<h4>voluptatum deleniti</h4>\r\n\r\n<ul>\r\n <li>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.</li>\r\n <li>Must be available to work required shifts including weekends, evenings and holidays.</li>\r\n <li>Must be able to perform repeated bending, standing and reaching.</li>\r\n <li>Must be able to occasionally lift up to 50 pounds</li>\r\n</ul>\r\n</div>\r\n</div>', 'about-us', 'No'),
(30, 'APPROVED', 'Contact Us', '<p>Just for test&nbsp;</p>\r\n\r\n<p>Contact us form</p>\r\n', 'contact-us', 'No'),
(31, 'APPROVED', 'Careers', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n', 'careers', 'No'),
(32, 'APPROVED', 'Privacy Policy', '<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p> </p>\r\n\r\n<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p> </p>\r\n\r\n<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p> </p>\r\n\r\n<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p> </p>\r\n\r\n<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'privacy-policy', 'No'),
(33, 'APPROVED', 'Terms of Service', '<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n', 'terms-of-service', 'No'),
(34, 'APPROVED', 'Refund Policy', '<p>Coming soon</p>', 'dfgd-gdf', 'No'),
(35, 'APPROVED', 'Employer Registration Benefits', '<p>Register now for <strong>FREE<sup>*</sup></strong></p>\r\n\r\n<ul>\r\n <li>See all your orders</li>\r\n <li>Shipping is always free</li>\r\n <li>Save your favorites</li>\r\n <li>Fast checkout</li>\r\n <li>Get a gift <small>(only new customers)</small></li>\r\n <li>Holiday discounts up to 30% off</li>\r\n</ul>', 'employer-registration-cms', 'No'),
(36, 'APPROVED', 'Jobseeker Registration Benefits', '<p>Register now for <strong>FREE<sup>*</sup></strong></p>\r\n\r\n<ul>\r\n <li>See all your orders</li>\r\n <li>Shipping is always free</li>\r\n <li>Save your favorites</li>\r\n <li>Fast checkout</li>\r\n <li>Get a gift <small>(only new customers)</small></li>\r\n <li>Holiday discounts up to 30% off</li>\r\n</ul>', 'jobseeker-registration-benefits', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `company_size_master`
--

CREATE TABLE `company_size_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `company_size` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_size_master`
--

INSERT INTO `company_size_master` (`id`, `status`, `company_size`, `is_deleted`) VALUES
(2, 'APPROVED', '151-500 Employee', 'No'),
(3, 'APPROVED', '101-150 Employee', 'No'),
(4, 'APPROVED', '0-50 Employee', 'No'),
(5, 'APPROVED', '51-100 Employee', 'No'),
(6, 'APPROVED', 'More than 500 Employee', 'No'),
(7, 'APPROVED', 'Demo Company Size', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `company_type_master`
--

CREATE TABLE `company_type_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `company_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_type_master`
--

INSERT INTO `company_type_master` (`id`, `status`, `company_type`, `is_deleted`) VALUES
(1, 'APPROVED', 'Partnership', 'No'),
(2, 'APPROVED', 'Public Listed', 'No'),
(3, 'APPROVED', 'Private Limited', 'No'),
(4, 'APPROVED', '12324', 'Yes'),
(5, 'APPROVED', 'Sole Proprietor', 'No'),
(6, 'APPROVED', 'Government Ministry', 'No'),
(7, 'APPROVED', 'Statutory Board', 'No'),
(8, 'APPROVED', 'Non-Profitable Organization', 'No'),
(9, 'APPROVED', 'Small and Medium Enterprise', 'No'),
(10, 'APPROVED', 'Multinational Corporation', 'No'),
(11, 'APPROVED', 'Limited Exempt Private Company', 'No'),
(12, 'APPROVED', 'N Demo Com Type', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `country_master`
--

CREATE TABLE `country_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `country_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `country_code` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'For use mobile code',
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country_master`
--

INSERT INTO `country_master` (`id`, `status`, `country_name`, `country_code`, `is_deleted`) VALUES
(1, 'APPROVED', 'India', '+91', 'No'),
(2, 'APPROVED', 'Pakistan', '+92', 'No'),
(3, 'APPROVED', 'Australia', '61', 'No'),
(4, 'APPROVED', 'United Arab Emirates', '+971', 'No'),
(5, 'APPROVED', 'Canada', '+1', 'No'),
(6, 'APPROVED', 'United States', '+1', 'No'),
(7, 'APPROVED', 'United Kingdom', '+44', 'No'),
(8, 'APPROVED', 'New Zealand', '+64', 'No'),
(9, 'APPROVED', 'South Africa', '+27', 'No'),
(10, 'UNAPPROVED', 'A test', 'A test f', 'Yes'),
(11, 'UNAPPROVED', '55555', 'kjhkh', 'Yes'),
(12, 'APPROVED', '56456', 'rtyryr', 'Yes'),
(13, 'APPROVED', 'dcxvbgv', 'vnhygjmuhg', 'Yes'),
(14, 'APPROVED', 'vcngv1651465', '2123', 'Yes'),
(15, 'APPROVED', 'ZCDsafs4127845685', '126755', 'Yes'),
(16, 'APPROVED', 'cvhbfg1644444444', '245685', 'Yes'),
(17, 'APPROVED', 'ADHjas564556979', '5265655', 'Yes'),
(18, 'APPROVED', '241765sdkfjrndsjfgtru', '12', 'Yes'),
(19, 'APPROVED', 'zxfvds12366497', '232569', 'Yes'),
(20, 'APPROVED', 'dxgfdhdg52876452', '12665', 'Yes'),
(21, 'APPROVED', 'cvhbgfj5246768', '545', 'Yes'),
(22, 'APPROVED', 'zfjbsdgiohnfd45649849', '212157457', 'Yes'),
(23, 'APPROVED', 'xzfvdsh4255868', '21425896', 'Yes'),
(24, 'APPROVED', 'dxfhgfj', '82328936', 'Yes'),
(25, 'APPROVED', 'bnbbnkugvjj  gvvcjgh mhkl', '1758', 'Yes'),
(26, 'APPROVED', 'cxbghjl', '154', 'Yes'),
(27, 'APPROVED', 'zFCdszg', '91', 'Yes'),
(28, 'APPROVED', 'Test', '155', 'Yes'),
(29, 'APPROVED', 'test', '92', 'Yes'),
(30, 'APPROVED', 'aa', '23', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `coupan_code`
--

CREATE TABLE `coupan_code` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') NOT NULL DEFAULT 'UNAPPROVED',
  `coupan_code` varchar(255) NOT NULL,
  `discount_amount` double NOT NULL,
  `active_from` date NOT NULL,
  `expired_on` date NOT NULL,
  `created_on` datetime NOT NULL,
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupan_code`
--

INSERT INTO `coupan_code` (`id`, `status`, `coupan_code`, `discount_amount`, `active_from`, `expired_on`, `created_on`, `is_deleted`) VALUES
(1, 'APPROVED', '100', 1, '2018-09-01', '2018-10-17', '2018-09-07 07:09:07', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `credit_plan_employer`
--

CREATE TABLE `credit_plan_employer` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `plan_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `plan_type` enum('Free','Paid') CHARACTER SET utf8 NOT NULL DEFAULT 'Paid',
  `plan_currency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `plan_amount` double NOT NULL,
  `plan_duration` int(11) NOT NULL,
  `plan_offers` text CHARACTER SET utf8 NOT NULL,
  `job_post_limit` int(11) NOT NULL,
  `cv_access_limit` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `highlight_job_limit` int(11) NOT NULL,
  `contacts` int(11) NOT NULL,
  `job_life` int(11) NOT NULL COMMENT 'in days',
  `created_on` datetime NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_plan_employer`
--

INSERT INTO `credit_plan_employer` (`id`, `status`, `plan_name`, `plan_type`, `plan_currency`, `plan_amount`, `plan_duration`, `plan_offers`, `job_post_limit`, `cv_access_limit`, `message`, `highlight_job_limit`, `contacts`, `job_life`, `created_on`, `is_deleted`) VALUES
(1, 'APPROVED', 'Free', 'Paid', 'INR', 0, 30, '', 5, 5, 0, 1, 0, 30, '2017-05-01 06:40:25', 'No'),
(2, 'APPROVED', 'Bronze', 'Paid', 'INR', 110, 35, '35', 35, 35, 35, 35, 35, 35, '2017-05-02 14:27:56', 'No'),
(3, 'APPROVED', 'Silver', 'Paid', 'INR', 1, 60, 'Test offer', 50, 50, 50, 50, 50, 50, '2017-06-23 09:38:07', 'No'),
(4, 'APPROVED', 'Gold', 'Paid', 'CAD', 660, 1000, 'Free access to all job seeker', 50, 60, 400, 34, 2147483647, 398, '2017-11-04 06:39:50', 'Yes'),
(5, 'APPROVED', 'GOLD', 'Paid', 'INR', 2000, 100, '', 30, 30, 30, 5, 30, 0, '2017-12-20 09:45:06', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `credit_plan_jobseeker`
--

CREATE TABLE `credit_plan_jobseeker` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `plan_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `plan_type` enum('Free','Paid') CHARACTER SET utf8 NOT NULL DEFAULT 'Paid',
  `plan_currency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `plan_amount` double NOT NULL,
  `plan_duration` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `highlight_application` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `relevant_jobs` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `performance_report` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `contacts` int(11) NOT NULL,
  `job_post_notification` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `plan_offers` text CHARACTER SET utf8 NOT NULL,
  `created_on` datetime NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_plan_jobseeker`
--

INSERT INTO `credit_plan_jobseeker` (`id`, `status`, `plan_name`, `plan_type`, `plan_currency`, `plan_amount`, `plan_duration`, `message`, `highlight_application`, `relevant_jobs`, `performance_report`, `contacts`, `job_post_notification`, `plan_offers`, `created_on`, `is_deleted`) VALUES
(1, 'APPROVED', 'Free', 'Free', 'INR', 0, 30, 0, 'No', 'No', 'No', 0, 'No', '', '2017-05-01 06:22:42', 'No'),
(2, 'APPROVED', 'Bronze', 'Paid', 'INR', 1, 30, 15, 'No', 'No', 'Yes', 15, 'No', '', '2017-05-01 08:53:45', 'No'),
(3, 'APPROVED', 'Silver', 'Paid', 'INR', 3, 55, 35, 'Yes', 'Yes', 'Yes', 35, 'Yes', '', '2017-05-03 11:03:23', 'No'),
(4, 'APPROVED', 'Gold', 'Paid', 'INR', 3000, 70, 55, 'Yes', 'Yes', 'Yes', 55, 'Yes', '', '2017-05-03 11:03:56', 'No'),
(5, 'APPROVED', 'Extended', 'Paid', 'INR', 5000, 85, 75, 'Yes', 'Yes', 'Yes', 75, 'Yes', '', '2017-05-03 11:04:13', 'No'),
(6, 'APPROVED', 'Premium', 'Paid', 'USD', 450, 800, 33, 'No', 'No', 'No', 2147483647, 'No', 'free access to any Recent job', '2017-11-04 05:46:32', 'No'),
(7, 'APPROVED', 'GOLD', 'Paid', 'INR', 500, 10, 10, 'No', 'No', 'No', 10, 'No', '', '2017-12-20 07:45:25', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `currency_master`
--

CREATE TABLE `currency_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `currency_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes- deleted, No - Not deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_master`
--

INSERT INTO `currency_master` (`id`, `status`, `currency_name`, `currency_code`, `is_deleted`) VALUES
(1, 'APPROVED', 'Indian Rupee', 'INR', 'No'),
(2, 'APPROVED', 'US Dollor', 'USD', 'No'),
(3, 'APPROVED', 'Pound', 'GBP', 'No'),
(4, 'APPROVED', 'Canadian Dollar', 'CAD', 'No'),
(5, 'APPROVED', 'Japanese Yen', 'JPY', 'No'),
(6, 'APPROVED', 'N Demo', 'NDM', 'Yes');

-- --------------------------------------------------------

--
-- Stand-in structure for view `delete_employer_view`
-- (See below for the actual view)
--
CREATE TABLE `delete_employer_view` (
`id` int(255)
,`req_id` int(255)
,`reason_for_del` text
,`is_deleted` enum('Yes','No')
,`email` varchar(255)
,`fullname` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `delete_job_seekar_view`
-- (See below for the actual view)
--
CREATE TABLE `delete_job_seekar_view` (
`id` int(255)
,`req_id` int(255)
,`reason_for_del` text
,`is_deleted` enum('Yes','No')
,`email` varchar(200)
,`fullname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `delete_profile_request`
--

CREATE TABLE `delete_profile_request` (
  `id` int(255) NOT NULL,
  `req_id` int(255) NOT NULL,
  `user_type` enum('emp','js') NOT NULL,
  `reason_for_del` text NOT NULL,
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `educational_qualification_master`
--

CREATE TABLE `educational_qualification_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `qualification_level_id` int(15) NOT NULL COMMENT 'jobseeker_qualification_level table field id',
  `educational_qualification` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educational_qualification_master`
--

INSERT INTO `educational_qualification_master` (`id`, `status`, `qualification_level_id`, `educational_qualification`, `is_deleted`) VALUES
(1, 'APPROVED', 1, 'Not Pursuing Graduation', 'No'),
(2, 'APPROVED', 1, 'B.A', 'No'),
(3, 'APPROVED', 1, 'B.Arch', 'No'),
(4, 'APPROVED', 1, 'B.Des.', 'No'),
(5, 'APPROVED', 1, 'B.El.Ed', 'No'),
(6, 'APPROVED', 1, 'B.P.Ed', 'No'),
(7, 'APPROVED', 1, 'B.U.M.S', 'No'),
(8, 'APPROVED', 1, 'BAMS', 'No'),
(9, 'APPROVED', 1, 'BCA', 'No'),
(10, 'APPROVED', 1, 'B.B.A/ B.M.S', 'No'),
(11, 'APPROVED', 1, 'B.Com', 'No'),
(12, 'APPROVED', 1, 'B.Ed', 'No'),
(13, 'APPROVED', 1, 'BDS', 'No'),
(14, 'APPROVED', 1, 'BFA', 'No'),
(15, 'APPROVED', 1, 'BHM', 'No'),
(16, 'APPROVED', 1, 'B.Pharma', 'No'),
(17, 'APPROVED', 1, 'B.Sc', 'No'),
(18, 'APPROVED', 1, 'B.Tech/B.E.', 'No'),
(19, 'APPROVED', 1, 'BHMS', 'No'),
(20, 'APPROVED', 1, 'LLB', 'No'),
(21, 'APPROVED', 1, 'MBBS', 'No'),
(22, 'APPROVED', 1, 'Diploma', 'No'),
(23, 'APPROVED', 1, 'BVSC', 'No'),
(24, 'APPROVED', 3, 'MPHIL', 'No'),
(25, 'APPROVED', 3, 'Ph.D', 'No'),
(26, 'APPROVED', 2, 'CA', 'No'),
(27, 'APPROVED', 2, 'CS', 'No'),
(28, 'APPROVED', 2, 'DM', 'No'),
(29, 'APPROVED', 2, 'ICWA (CMA)', 'No'),
(30, 'APPROVED', 2, 'Integrated PG', 'No'),
(31, 'APPROVED', 2, 'LLM', 'No'),
(32, 'APPROVED', 2, 'M.A', 'No'),
(33, 'APPROVED', 2, 'M.Arch', 'No'),
(34, 'APPROVED', 2, 'M.Ch', 'No'),
(35, 'APPROVED', 2, 'M.Com', 'No'),
(36, 'APPROVED', 2, 'M.Des.', 'No'),
(37, 'APPROVED', 2, 'M.Ed', 'No'),
(38, 'APPROVED', 2, 'M.Pharma', 'No'),
(39, 'APPROVED', 2, 'MDS', 'No'),
(40, 'APPROVED', 2, 'MFA', 'No'),
(41, 'APPROVED', 2, 'MS/M.Sc(Science)', 'No'),
(42, 'APPROVED', 2, 'M.Tech', 'No'),
(43, 'APPROVED', 2, 'MBA/PGDM', 'No'),
(44, 'APPROVED', 2, 'MCA', 'No'),
(45, 'APPROVED', 2, 'Medical-MS/MD', 'No'),
(46, 'APPROVED', 2, 'PG Diploma', 'No'),
(47, 'APPROVED', 2, 'MVSC', 'No'),
(48, 'APPROVED', 2, 'MCM', 'No'),
(49, 'APPROVED', 7, 'N Demo', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') NOT NULL DEFAULT 'UNAPPROVED',
  `template_name` varchar(255) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_content` text NOT NULL,
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `status`, `template_name`, `email_subject`, `email_content`, `is_deleted`) VALUES
(1, 'APPROVED', 'Job Seeker Registration', 'New Job Seeker Registration', '<h6>Dear Member,</h6>\r\n\r\n<p><strong>Thank you for being a registered member with websitename</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>You are now ready to search and contact millions of validated jobs.It is our prime emphasis to help   you find a suitable job. You will be assigned with personalized username and password . Please find  the login details below& confirm your email-id by clicking the following link<br>\r\n<br>\r\n<strong>Dear, yourname<br>\r\nEmail-ID : email_id<br>\r\nConfirmation Link : <a href=\"site domain name/cpass/email_id\"> site domain name/cpass/email_id </a></strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(2, 'APPROVED', 'Contact us admin', 'Someone has tried to contact you on your website from contact us page', '<p>Dear admin,</p>\r\n\r\n<p>This mail is to inform you that someone has tried to contact you from your website webname.</p>\r\n\r\n<p>Following are the details that has been provided by him/her.</p>\r\n\r\n<p><strong>Name : name_provided<br>\r\nEmail  : email_provided<br>\r\nMessage : message_provided</strong><br>\r\n<strong>Contact Mobile : mobile_no_provided</strong></p>', 'No'),
(3, 'APPROVED', 'a  add fgdf', 'a  ag dfg df', '<p>a &nbsp;ag dfg&nbsp;</p>\r\n', 'Yes'),
(4, 'APPROVED', ' sddfdf dfhdf', ' dfhdh dfh dfh dfh', '<p>hdfhhdhdf hfg hfdgh redg</p>\r\n', 'Yes'),
(5, 'APPROVED', 'Forgot password', 'Forgot password', '<div>Dear Member,&nbsp;</div>\r\n\r\n<p>This is a notification that you have recently reset your password.</p>\r\n\r\n<p>Your new password is :&nbsp;xxxxx</p>\r\n\r\n<p>Thank you for choosing us to reach you better.</p>\r\n\r\n<p>Regards,<br />\r\nwebsitename.</p>\r\n', 'No'),
(6, 'APPROVED', 'Employer Registration', 'New Employer  Registration', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>Thank you for registering as an employer at websitename</strong></p>\r\n\r\n<p>You are now ready to search and contact millions of job seekers and post various types jobs for them.It is our prime emphasis to help   you find a suitable candidate. From now on your email id will be your username and password which you provided at the time of registration . Please verify your account by clicking the below given link and enjoy our sevices<br>\r\n<br>\r\n<strong>Dear, yourname<br>\r\nEmail-ID : email_id<br>\r\nConfirmation Link : <a href=\"site domain name/cpass/email_id\"> site domain name/cpass/email_id </a></strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(7, 'APPROVED', 'Employer password reset successfull', 'Password reset for your account as employer on webfriendlyname was successfull', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>This is the mail regarding the password change for your account as an employer at websitename.Please note that your the password of your account is changed which will be effective from now onwards.</strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(8, 'APPROVED', 'Employer password reset', 'Password reset for your account as employer on webfriendlyname', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>This is the mail regarding the password change request for your account as an employer at websitename.Please ignore this email if the password change request was not made by you.</strong></p>\r\n\r\n<p><br>\r\n<strong>Dear, yourname<br>\r\nPlease click the below link and set the new password for your account as an employer at websitename.<br>\r\nPassword reset Link : <a href=\"site domain name/cpass/email_id\">site domain name/cpass/email_id</a></strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(9, 'APPROVED', 'Employer profile edited', 'Employer section changed successfully.', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>This is the mail regarding the profile details change for your account as an employer at websitename.Please note that your profile has been changed and if you think it was not you, then login to your account and change the password.</strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(10, 'APPROVED', 'a test', 'a test', '<p>a test</p>', 'Yes'),
(11, 'APPROVED', 'a test', 'a test', '<p>a test</p>', 'Yes'),
(12, 'APPROVED', 'a test', 'a test', '<p>a test</p>', 'No'),
(13, 'APPROVED', 'Send message to employer', 'Job seeker sent message', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>This  mail is  regarding the job seeker  send a message on websitename.For more details check your account.</strong></p>\r\n\r\n<p><strong>Job seeker name : sender_name</strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(14, 'APPROVED', 'Apply for job', 'New application receive for job', '<h6>Dear Employer,</h6>\r\n\r\n<p><strong>This mail is  regarding the </strong>new application is  receive for job <strong>on websitename.For details check your account.</strong></p>\r\n\r\n<p>Applicant name : sender_name</p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(15, 'APPROVED', 'Send message to jobseeker', 'Employer sent message', '<h6>Dear jobseeker,</h6>\r\n\r\n<p><strong>This is mail is  regarding the employer send a message on websitename.For more details check your account.</strong></p>\r\n\r\n<p><strong>Employer name : sender_name</strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>', 'No'),
(16, 'APPROVED', 'Jobseeker password reset', 'Password reset for your account as jobseeker on webfriendlyname', '<p>Dear Job seeker,</p>\r\n\r\n<p><strong>This is the mail regarding the password change request for your account as an jobseeker at websitename.Please ignore this email if the password change request was not made by you.</strong></p>\r\n\r\n<p><strong>Dear, yourname<br>\r\nPlease click the below link and set the new password for your account as an job seeker at websitename.<br>\r\nPassword reset Link : <a href=\"site domain name/cpass/email_id\">site domain name/cpass/email_id</a></strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>', 'No'),
(17, 'APPROVED', 'Job seeker password reset successfull', 'Password reset for your account as job seeker on webfriendlyname was successfull', '<p>Dear Job seeker,</p>\r\n\r\n<p><strong>This is the mail regarding the password change for your account as an jobseeker at websitename.Please note that your the password of your account is changed which will be effective from now onwards.</strong></p>\r\n\r\n<p>Regards ,<br>\r\nwebsitename</p>\r\n\r\n<p> </p>', 'No'),
(18, 'APPROVED', 'http://192.168.1.111/job_portal/login/forgot-password', 'View of forgot password for job seeker', '', 'Yes'),
(19, 'APPROVED', 'posted_job', 'Your Recent Posted Job ', '<h4>Dear Employer,</h4>\r\n\r\n<p><strong>This is the mail regarding your recent job posted on our website.Please Find below link to access your recent posted job</strong></p>\r\n<p>Job Url : </p> <a href=\"job_link\">job_link</a>\r\n\r\n\r\n<p>Thank You</p>', 'No'),
(20, 'UNAPPROVED', '.', '.', '', 'Yes'),
(21, 'UNAPPROVED', 'N Demo Template', 'test', '<p>N Demo Templates</p>\r\n\r\n<ul>\r\n <li>demo</li>\r\n <li>test</li>\r\n</ul>', 'Yes'),
(22, 'UNAPPROVED', 'N Demo Template 2', 'test', '<p>dsfsdfsdfsdfsdf</p>', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `employement_type`
--

CREATE TABLE `employement_type` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `employement_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employement_type`
--

INSERT INTO `employement_type` (`id`, `status`, `employement_type`, `is_deleted`) VALUES
(1, 'APPROVED', 'Full Time', 'No'),
(2, 'APPROVED', 'Part Time', 'No'),
(3, 'APPROVED', 'N Demo Emp Type', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `employer_down_js_resume`
--

CREATE TABLE `employer_down_js_resume` (
  `id` int(255) NOT NULL,
  `emp_id` int(255) NOT NULL,
  `jobseeker_id` int(255) NOT NULL,
  `last_dwn_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employer_master`
--

CREATE TABLE `employer_master` (
  `id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email_ver_str` text NOT NULL,
  `email_ver_status` enum('No','Yes') NOT NULL DEFAULT 'No',
  `title` int(11) DEFAULT NULL COMMENT 'Join personal_titles_master on id',
  `fullname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `functional_area` text NOT NULL COMMENT 'comma seperated ids of fn areas',
  `job_profile` text CHARACTER SET utf8 NOT NULL,
  `email_verified` enum('No','Yes') CHARACTER SET utf8 NOT NULL DEFAULT 'No',
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `password` text CHARACTER SET utf8 NOT NULL,
  `country` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `pincode` int(11) NOT NULL,
  `mobile` varchar(25) CHARACTER SET utf8 NOT NULL,
  `mobile_verified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `profile_pic` varchar(255) CHARACTER SET utf8 NOT NULL,
  `profile_pic_approval` enum('UNAPPROVED','APPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `company_name` text CHARACTER SET utf8 NOT NULL,
  `company_profile` text,
  `company_website` text,
  `company_email` varchar(255) NOT NULL,
  `company_type` int(11) NOT NULL,
  `company_size` int(11) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `company_logo_approval` enum('UNAPPROVED','APPROVED') NOT NULL DEFAULT 'UNAPPROVED',
  `industry` int(11) NOT NULL,
  `industry_hire` text,
  `function_area_hire` varchar(255) DEFAULT NULL,
  `skill_hire` text,
  `register_date` datetime NOT NULL,
  `facebook_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gplus_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `linkedin_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8 NOT NULL,
  `device_id` text CHARACTER SET utf8 NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No',
  `plan_status` enum('Active','Paid','Expired') NOT NULL DEFAULT 'Active',
  `plan_name` varchar(255) NOT NULL,
  `plan_expired` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `employer_master_view`
-- (See below for the actual view)
--
CREATE TABLE `employer_master_view` (
`id` int(11)
,`email` varchar(255)
,`email_ver_str` text
,`email_ver_status` enum('No','Yes')
,`title` int(11)
,`fullname` varchar(255)
,`designation` text
,`functional_area` text
,`job_profile` text
,`email_verified` enum('No','Yes')
,`status` enum('APPROVED','UNAPPROVED')
,`password` text
,`country` int(11)
,`city` int(11)
,`address` text
,`pincode` int(11)
,`mobile` varchar(25)
,`mobile_verified` enum('Yes','No')
,`profile_pic` varchar(255)
,`profile_pic_approval` enum('UNAPPROVED','APPROVED')
,`company_name` text
,`company_profile` text
,`company_website` text
,`company_email` varchar(255)
,`company_type` int(11)
,`company_size` int(11)
,`company_logo` varchar(255)
,`company_logo_approval` enum('UNAPPROVED','APPROVED')
,`industry` int(11)
,`industry_hire` text
,`function_area_hire` varchar(255)
,`skill_hire` text
,`register_date` datetime
,`facebook_url` varchar(255)
,`gplus_url` varchar(255)
,`twitter_url` varchar(255)
,`linkedin_url` varchar(255)
,`user_agent` text
,`device_id` text
,`last_login` datetime
,`is_deleted` enum('Yes','No')
,`plan_status` enum('Active','Paid','Expired')
,`plan_name` varchar(255)
,`plan_expired` date
,`city_name` varchar(255)
,`country_name` varchar(255)
,`personal_titles` varchar(255)
,`industries_name` varchar(255)
,`company_type_name` varchar(255)
,`company_size_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `employer_viewed_js_contact`
--

CREATE TABLE `employer_viewed_js_contact` (
  `id` int(255) NOT NULL,
  `emp_id` int(255) NOT NULL,
  `jobseeker_id` int(255) NOT NULL,
  `last_viewed_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `fn_area_role_view`
-- (See below for the actual view)
--
CREATE TABLE `fn_area_role_view` (
`id` int(11)
,`status` enum('APPROVED','UNAPPROVED')
,`functional_area` int(11)
,`role_name` varchar(255)
,`is_deleted` enum('Yes','No')
,`fnarea_status` enum('APPROVED','UNAPPROVED')
,`functional_name` varchar(255)
,`fnarea_is_deleted` enum('Yes','No')
);

-- --------------------------------------------------------

--
-- Table structure for table `functional_area_master`
--

CREATE TABLE `functional_area_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `functional_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `functional_area_master`
--

INSERT INTO `functional_area_master` (`id`, `status`, `functional_name`, `is_deleted`) VALUES
(1, 'APPROVED', 'Accounts / Finance / Tax / CS / Audit', 'No'),
(2, 'APPROVED', 'Agent', 'Yes'),
(3, 'APPROVED', 'Analytics & Business Intelligence', 'No'),
(4, 'APPROVED', 'Architecture / Interior Design', 'No'),
(5, 'APPROVED', 'Banking / Insurance', 'No'),
(6, 'APPROVED', 'Beauty / Fitness / Spa Services', 'No'),
(7, 'APPROVED', 'Content / Journalism', 'No'),
(8, 'APPROVED', 'Corporate Planning / Consulting', 'No'),
(9, 'APPROVED', 'CSR & Sustainability', 'No'),
(10, 'APPROVED', 'Engineering Design / R&D', 'No'),
(11, 'APPROVED', 'Export / Import / Merchandising', 'No'),
(12, 'APPROVED', 'Fashion / Garments / Merchandising', 'No'),
(13, 'APPROVED', 'Guards / Security Services', 'No'),
(14, 'APPROVED', 'Hotels / Restaurants', 'No'),
(15, 'APPROVED', 'HR / Administration / IR', 'No'),
(16, 'APPROVED', 'IT Software - Client Server', 'No'),
(17, 'APPROVED', 'IT Software - Mainframe', 'No'),
(18, 'APPROVED', 'IT Software - Middleware', 'No'),
(19, 'APPROVED', 'IT Software - Mobile', 'No'),
(20, 'APPROVED', 'IT Software - Other', 'No'),
(21, 'APPROVED', 'IT Software - System Programming', 'No'),
(22, 'APPROVED', 'IT Software - Telecom Software', 'No'),
(23, 'APPROVED', 'IT Software - Application Programming / Maintenance', 'No'),
(24, 'APPROVED', 'IT Software - DBA / Datawarehousing', 'No'),
(25, 'APPROVED', 'IT Software - E-Commerce / Internet Technologies', 'No'),
(26, 'APPROVED', 'IT Software - Embedded /EDA /VLSI /ASIC /Chip Des.', 'No'),
(27, 'APPROVED', 'IT Software - ERP / CRM', 'No'),
(28, 'APPROVED', 'IT Software - Network Administration / Security', 'No'),
(29, 'APPROVED', 'IT Software - QA & Testing', 'No'),
(30, 'APPROVED', 'IT Software - Systems / EDP / MIS', 'No'),
(31, 'APPROVED', 'IT- Hardware / Telecom / Technical Staff / Support', 'No'),
(32, 'APPROVED', 'ITES / BPO / KPO / Customer Service / Operations', 'No'),
(33, 'APPROVED', 'Legal', 'No'),
(34, 'APPROVED', 'Marketing / Advertising / MR / PR', 'No'),
(35, 'APPROVED', 'Packaging', 'No'),
(36, 'APPROVED', 'Pharma / Biotech / Healthcare / Medical / R&D', 'No'),
(37, 'APPROVED', 'Production / Maintenance / Quality', 'No'),
(38, 'APPROVED', 'Purchase / Logistics / Supply Chain', 'No'),
(39, 'APPROVED', 'Sales / BD', 'No'),
(40, 'APPROVED', 'Secretary / Front Office / Data Entry', 'No'),
(41, 'APPROVED', 'Self Employed / Consultants', 'No'),
(42, 'APPROVED', 'Shipping', 'No'),
(43, 'APPROVED', 'Site Engineering / Project Management', 'No'),
(44, 'APPROVED', 'Teaching / Education', 'No'),
(45, 'APPROVED', 'Ticketing / Travel / Airlines', 'No'),
(46, 'APPROVED', 'Top Management', 'No'),
(47, 'APPROVED', 'TV / Films / Production', 'No'),
(48, 'APPROVED', 'Web / Graphic Design / Visualiser', 'No'),
(49, 'APPROVED', 'N Demo Function', 'Yes'),
(50, 'UNAPPROVED', 'N Demo Function', 'Yes'),
(51, 'APPROVED', 'Agent', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `industries_master`
--

CREATE TABLE `industries_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `industries_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data',
  `is_featured` enum('No','Yes') NOT NULL DEFAULT 'No',
  `icon_name` varchar(255) DEFAULT NULL,
  `icone_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industries_master`
--

INSERT INTO `industries_master` (`id`, `status`, `industries_name`, `is_deleted`, `is_featured`, `icon_name`, `icone_code`) VALUES
(0, 'APPROVED', 'Select All', 'No', 'Yes', NULL, NULL),
(1, 'APPROVED', 'Accounting/Finance', 'No', 'Yes', 'fa fa-whatsapp', '&#xf232;'),
(2, 'APPROVED', 'Advertising/PR/MR/Events', 'No', 'Yes', 'fa fa-500px', '&#xf26e;'),
(3, 'APPROVED', 'Agriculture/Dairy', 'No', 'Yes', 'fa fa-500px', '&#xf26e;'),
(4, 'APPROVED', 'Animation', 'No', 'Yes', NULL, NULL),
(5, 'APPROVED', 'Architecture/Interior Designing', 'No', 'Yes', NULL, NULL),
(6, 'APPROVED', 'Auto/Auto Ancillary', 'No', 'Yes', NULL, NULL),
(7, 'APPROVED', 'Aviation / Aerospace Firms', 'No', 'Yes', NULL, NULL),
(8, 'APPROVED', 'Banking/Financial Services/Broking', 'No', 'Yes', NULL, NULL),
(9, 'APPROVED', 'BPO/ITES', 'No', 'No', NULL, NULL),
(10, 'APPROVED', 'Brewery / Distillery', 'No', 'No', NULL, NULL),
(11, 'APPROVED', 'Broadcasting', 'No', 'No', NULL, NULL),
(12, 'APPROVED', 'Ceramics /Sanitary ware', 'No', 'No', NULL, NULL),
(13, 'APPROVED', 'Chemicals/PetroChemical/Plastic/Rubber', 'No', 'No', NULL, NULL),
(14, 'APPROVED', 'Construction/Engineering/Cement/Metals', 'No', 'No', NULL, NULL),
(15, 'APPROVED', 'Consumer Durables', 'No', 'No', NULL, NULL),
(16, 'APPROVED', 'Courier/Transportation/Freight', 'No', 'No', NULL, NULL),
(17, 'APPROVED', 'Defence/Government', 'No', 'No', NULL, NULL),
(18, 'APPROVED', 'Education/Teaching/Training', 'No', 'No', NULL, NULL),
(19, 'APPROVED', 'Electricals / Switchgears', 'No', 'No', NULL, NULL),
(20, 'APPROVED', 'Export/Import', 'No', 'Yes', 'ln  ln-icon-Globe', NULL),
(21, 'APPROVED', 'Facility Management', 'No', 'No', NULL, NULL),
(22, 'APPROVED', 'Fertilizers/Pesticides', 'No', 'No', NULL, NULL),
(23, 'APPROVED', 'FMCG/Foods/Beverage', 'No', 'No', NULL, NULL),
(24, 'APPROVED', 'Food Processing', 'No', 'Yes', NULL, NULL),
(25, 'APPROVED', 'Fresher/Trainee', 'No', 'Yes', 'ln ln-icon-Student-Female', NULL),
(26, 'APPROVED', 'Gems & Jewellery', 'No', 'No', NULL, NULL),
(27, 'APPROVED', 'Glass', 'No', 'No', NULL, NULL),
(28, 'APPROVED', 'Heat Ventilation Air Conditioning', 'No', 'No', NULL, NULL),
(29, 'APPROVED', 'Hotels/Restaurants/Airlines/Travel', 'No', 'Yes', 'ln ln-icon-Car', NULL),
(30, 'APPROVED', 'Industrial Products/Heavy Machinery', 'No', 'No', NULL, NULL),
(31, 'APPROVED', 'Insurance', 'No', 'No', NULL, NULL),
(32, 'APPROVED', 'Internet/Ecommerce', 'No', 'No', NULL, NULL),
(33, 'APPROVED', 'IT-Hardware & Networking', 'No', 'No', NULL, NULL),
(34, 'APPROVED', 'IT-Software/Software Services', 'No', 'Yes', 'ln  ln-icon-Laptop-3', NULL),
(35, 'APPROVED', 'KPO / Research /Analytics', 'No', 'No', NULL, NULL),
(36, 'APPROVED', 'Leather', 'No', 'No', NULL, NULL),
(37, 'APPROVED', 'Legal', 'No', 'No', NULL, NULL),
(38, 'APPROVED', 'Media/Dotcom/Entertainment', 'No', 'Yes', 'ln  ln-icon-Plates', NULL),
(39, 'APPROVED', 'Medical/Healthcare/Hospital', 'No', 'Yes', 'ln  ln-icon-Medical-Sign', NULL),
(40, 'APPROVED', 'Medical Devices / Equipments', 'No', 'Yes', 'ln ln-icon-Laptop-3', NULL),
(41, 'APPROVED', 'Mining', 'No', 'No', NULL, NULL),
(42, 'APPROVED', 'NGO/Social Services', 'No', 'No', NULL, NULL),
(43, 'APPROVED', 'Office Equipment/Automation', 'No', 'No', NULL, NULL),
(44, 'APPROVED', 'Oil and Gas/Power/Infrastructure/Energy', 'No', 'No', NULL, NULL),
(45, 'APPROVED', 'Paper', 'No', 'No', NULL, NULL),
(46, 'APPROVED', 'Pharma/Biotech/Clinical Research', 'No', 'No', NULL, NULL),
(47, 'APPROVED', 'Printing/Packaging', 'No', 'No', NULL, NULL),
(48, 'APPROVED', 'Publishing', 'No', 'No', NULL, NULL),
(49, 'APPROVED', 'Real Estate/Property', 'No', 'Yes', 'ln ln-icon-Laptop-3', NULL),
(50, 'APPROVED', 'Recruitment', 'No', 'Yes', 'fa fa-user', NULL),
(51, 'APPROVED', 'Retail', 'No', 'No', NULL, NULL),
(52, 'APPROVED', 'Security/Law Enforcement', 'No', 'No', NULL, NULL),
(53, 'APPROVED', 'Semiconductors/Electronics', 'No', 'No', NULL, NULL),
(54, 'APPROVED', 'Shipping/Marine', 'No', 'No', NULL, NULL),
(55, 'APPROVED', 'Steel', 'No', 'No', NULL, NULL),
(56, 'APPROVED', 'Strategy /Management Consulting Firms', 'No', 'No', NULL, NULL),
(57, 'APPROVED', 'Sugar', 'No', 'No', NULL, NULL),
(58, 'APPROVED', 'Telcom/ISP', 'No', 'No', NULL, NULL),
(59, 'APPROVED', 'Textiles/Garments/Accessories', 'No', 'No', NULL, NULL),
(60, 'APPROVED', 'Tyres', 'No', 'No', NULL, NULL),
(61, 'APPROVED', 'Water Treatment / Waste Management', 'No', 'No', NULL, NULL),
(62, 'APPROVED', 'Wellness / Fitness / Sports / Beauty', 'No', 'No', NULL, NULL),
(63, 'APPROVED', 'Test', 'No', 'Yes', 'fa fa-question', NULL),
(64, 'APPROVED', 'N Demo Industry', 'Yes', 'No', 'fa fa-500px', '&#xf26e;');

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker`
--

CREATE TABLE `jobseeker` (
  `id` int(11) NOT NULL,
  `facebook_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gplus_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `linkedin_id` varchar(255) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `email_ver_str` text NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL,
  `verify_email` enum('No','Yes') CHARACTER SET utf8 NOT NULL DEFAULT 'No',
  `title` int(11) NOT NULL COMMENT 'Join personal_titles_master on id',
  `fullname` varchar(200) CHARACTER SET utf8 NOT NULL,
  `gender` enum('Male','Female','Other') CHARACTER SET utf8 NOT NULL,
  `birthdate` date NOT NULL,
  `marital_status` int(11) NOT NULL COMMENT 'join marital_status_master on id',
  `address` text CHARACTER SET utf8 NOT NULL COMMENT 'current address',
  `city` int(11) NOT NULL COMMENT 'current city join city_master on id',
  `country` int(11) NOT NULL DEFAULT '0' COMMENT 'current country join country_master on id',
  `mobile` varchar(20) CHARACTER SET utf8 NOT NULL,
  `mobile_verified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `landline` varchar(35) CHARACTER SET utf8 NOT NULL,
  `pincode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `website` varchar(255) CHARACTER SET utf8 NOT NULL,
  `profile_pic` varchar(100) CHARACTER SET utf8 NOT NULL,
  `profile_pic_approval` enum('UNAPPROVED','APPROVED') CHARACTER SET utf8 NOT NULL,
  `profile_summary` text CHARACTER SET utf8 NOT NULL,
  `home_city` varchar(255) CHARACTER SET utf8 NOT NULL,
  `preferred_city` text CHARACTER SET utf8 NOT NULL,
  `resume_headline` text CHARACTER SET utf8 NOT NULL,
  `total_experience` varchar(155) CHARACTER SET utf8 NOT NULL,
  `annual_salary` varchar(255) CHARACTER SET utf8 NOT NULL,
  `currency_type` varchar(25) CHARACTER SET utf8 NOT NULL,
  `industry` int(11) NOT NULL COMMENT 'join industries_master on id',
  `functional_area` int(11) NOT NULL COMMENT 'join functional_area_master on id',
  `job_role` int(11) NOT NULL COMMENT 'join role_master on id',
  `key_skill` text CHARACTER SET utf8 NOT NULL,
  `email_verification` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `resume_file` varchar(100) CHARACTER SET utf8 NOT NULL,
  `resume_last_update` datetime NOT NULL,
  `resume_verification` enum('UNAPPROVED','APPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `desire_job_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `employment_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prefered_shift` varchar(255) CHARACTER SET utf8 NOT NULL,
  `expected_annual_salary` varchar(255) CHARACTER SET utf8 NOT NULL,
  `exp_salary_currency_type` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Expected annual salary currency type',
  `register_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `profile_visibility` enum('No','Yes') CHARACTER SET utf8 NOT NULL DEFAULT 'Yes',
  `facebook_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gplus_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `twitter_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `linkedin_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `device_id` text CHARACTER SET utf8 NOT NULL,
  `user_agent` text CHARACTER SET utf8 NOT NULL COMMENT 'device or browser - registered from',
  `user_ip` varchar(100) CHARACTER SET utf8 NOT NULL,
  `plan_status` enum('Active','Paid','Expired') NOT NULL DEFAULT 'Active',
  `plan_name` varchar(255) NOT NULL,
  `plan_expired` date NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_access_employer`
--

CREATE TABLE `jobseeker_access_employer` (
  `id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `is_block` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_follow` enum('No','Yes') NOT NULL DEFAULT 'No',
  `is_liked` enum('Yes','No') NOT NULL DEFAULT 'No',
  `liked_on` datetime NOT NULL,
  `followed_on` datetime NOT NULL,
  `blocked_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the data of job seeker who follows the employer';

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_education`
--

CREATE TABLE `jobseeker_education` (
  `id` int(20) NOT NULL,
  `js_id` int(11) NOT NULL,
  `passing_year` year(4) NOT NULL,
  `qualification_level` varchar(255) CHARACTER SET utf8 NOT NULL,
  `institute` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'also entry of certificate name',
  `specialization` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Also entry of certificate description',
  `marks` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_certificate_X_XII` enum('Certi','Edu','X','XII') NOT NULL DEFAULT 'Edu' COMMENT 'Certi means cerificate detaisl, Edu means education detail , X means 10 th , XII means 12 th',
  `update_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_language`
--

CREATE TABLE `jobseeker_language` (
  `id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `proficiency_level` varchar(255) NOT NULL,
  `reading` enum('No','Yes') NOT NULL,
  `writing` enum('No','Yes') NOT NULL,
  `speaking` enum('No','Yes') NOT NULL,
  `update_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobseeker_language`
--

INSERT INTO `jobseeker_language` (`id`, `js_id`, `language`, `proficiency_level`, `reading`, `writing`, `speaking`, `update_on`) VALUES
(1, 1, 'English', 'Poor', 'Yes', 'Yes', 'Yes', '2018-12-11 13:29:39'),
(2, 1, 'German - deutsch', 'Average', 'Yes', 'No', 'Yes', '2018-12-11 13:29:39'),
(3, 32, 'English', 'Fair', 'Yes', 'Yes', 'Yes', '2019-02-14 06:08:44'),
(4, 41, 'retert', 'Fair', 'Yes', 'Yes', 'Yes', '2019-02-15 09:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_qualification_level`
--

CREATE TABLE `jobseeker_qualification_level` (
  `id` int(15) NOT NULL,
  `qualification_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table for getting qualification like , diploma , graduate ,Master , phd etc';

--
-- Dumping data for table `jobseeker_qualification_level`
--

INSERT INTO `jobseeker_qualification_level` (`id`, `qualification_name`, `status`, `is_deleted`) VALUES
(1, 'Graduate', 'APPROVED', 'No'),
(2, 'Post Graduate', 'APPROVED', 'No'),
(3, 'Doctorate', 'APPROVED', 'No'),
(4, 'Class X', 'APPROVED', 'Yes'),
(6, 'Class XII', 'APPROVED', 'Yes'),
(7, 'N Demo', 'APPROVED', 'Yes');

-- --------------------------------------------------------

--
-- Stand-in structure for view `jobseeker_view`
-- (See below for the actual view)
--
CREATE TABLE `jobseeker_view` (
`id` int(11)
,`facebook_id` varchar(255)
,`gplus_id` varchar(255)
,`status` enum('APPROVED','UNAPPROVED')
,`email` varchar(200)
,`password` text
,`email_ver_str` text
,`verify_email` enum('No','Yes')
,`title` int(11)
,`fullname` varchar(200)
,`gender` enum('Male','Female','Other')
,`birthdate` date
,`marital_status` int(11)
,`address` text
,`city` int(11)
,`country` int(11)
,`mobile` varchar(20)
,`mobile_verified` enum('Yes','No')
,`landline` varchar(35)
,`pincode` varchar(10)
,`website` varchar(255)
,`profile_pic` varchar(100)
,`profile_pic_approval` enum('UNAPPROVED','APPROVED')
,`profile_summary` text
,`home_city` varchar(255)
,`preferred_city` text
,`resume_headline` text
,`total_experience` varchar(155)
,`annual_salary` varchar(255)
,`currency_type` varchar(25)
,`industry` int(11)
,`functional_area` int(11)
,`job_role` int(11)
,`key_skill` text
,`email_verification` enum('No','Yes')
,`resume_file` varchar(100)
,`resume_last_update` datetime
,`resume_verification` enum('UNAPPROVED','APPROVED')
,`desire_job_type` varchar(255)
,`employment_type` varchar(255)
,`prefered_shift` varchar(255)
,`expected_annual_salary` varchar(255)
,`exp_salary_currency_type` varchar(255)
,`register_date` datetime
,`last_login` datetime
,`profile_visibility` enum('No','Yes')
,`facebook_url` varchar(255)
,`gplus_url` varchar(255)
,`twitter_url` varchar(255)
,`linkedin_url` varchar(255)
,`device_id` text
,`user_agent` text
,`user_ip` varchar(100)
,`plan_status` enum('Active','Paid','Expired')
,`is_deleted` enum('Yes','No')
,`plan_name` varchar(255)
,`plan_expired` date
,`personal_titles` varchar(255)
,`marital_status_val` varchar(255)
,`city_name` varchar(255)
,`country_name` varchar(255)
,`industries_name` varchar(255)
,`functional_name` varchar(255)
,`role_name` varchar(255)
,`annual_salary_name` varchar(255)
,`ex_annual_salary_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_viewed_employer_contact`
--

CREATE TABLE `jobseeker_viewed_employer_contact` (
  `id` int(255) NOT NULL,
  `js_id` int(255) NOT NULL,
  `employer_id` int(255) NOT NULL,
  `last_viewed_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_viewed_jobs`
--

CREATE TABLE `jobseeker_viewed_jobs` (
  `id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `update_on` datetime NOT NULL,
  `liked_on` datetime NOT NULL,
  `saved_on` datetime NOT NULL,
  `is_liked` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_saved` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table saves the jobs that job seeker has viewed';

-- --------------------------------------------------------

--
-- Table structure for table `jobseeker_workhistory`
--

CREATE TABLE `jobseeker_workhistory` (
  `id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `joining_date` date NOT NULL,
  `leaving_date` date DEFAULT NULL,
  `industry` int(11) NOT NULL,
  `functional_area` int(11) NOT NULL,
  `job_role` int(11) NOT NULL,
  `annual_salary` varchar(255) CHARACTER SET utf8 NOT NULL,
  `currency_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `achievements` text CHARACTER SET utf8,
  `update_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `jobseeker_workhistory_view`
-- (See below for the actual view)
--
CREATE TABLE `jobseeker_workhistory_view` (
`id` int(11)
,`js_id` int(11)
,`company_name` varchar(255)
,`joining_date` date
,`leaving_date` date
,`industry` int(11)
,`functional_area` int(11)
,`job_role` int(11)
,`annual_salary` varchar(255)
,`achievements` text
,`update_on` datetime
,`currency_type` varchar(255)
,`industries_name` varchar(255)
,`functional_name` varchar(255)
,`role_name` varchar(255)
,`salary_range` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `job_application_history`
--

CREATE TABLE `job_application_history` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `is_shortlisted` enum('No','Yes') NOT NULL DEFAULT 'No',
  `applied_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table saves the Job Application history of job seeker';

-- --------------------------------------------------------

--
-- Table structure for table `job_payment_master`
--

CREATE TABLE `job_payment_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `job_payment` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_payment_master`
--

INSERT INTO `job_payment_master` (`id`, `status`, `job_payment`, `is_deleted`) VALUES
(1, 'APPROVED', 'On Hour', 'No'),
(2, 'APPROVED', 'Monthly', 'No'),
(3, 'APPROVED', 'On Task', 'No'),
(4, 'UNAPPROVED', 'Demo Maange Job Payment', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `job_posting`
--

CREATE TABLE `job_posting` (
  `id` int(15) NOT NULL,
  `job_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `job_description` text CHARACTER SET utf8 NOT NULL,
  `skill_keyword` text CHARACTER SET utf8 NOT NULL COMMENT 'skill for require job like php,css,html etc',
  `location_hiring` varchar(200) NOT NULL COMMENT 'hire for which location',
  `company_hiring` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link_job` varchar(255) CHARACTER SET utf8 NOT NULL,
  `job_industry` int(11) NOT NULL,
  `functional_area` int(11) NOT NULL,
  `job_post_role` int(11) NOT NULL,
  `work_experience_from` varchar(255) CHARACTER SET utf8 NOT NULL,
  `work_experience_to` varchar(255) NOT NULL,
  `job_salary_from` varchar(255) NOT NULL,
  `job_salary_to` varchar(255) NOT NULL,
  `currency_type` varchar(255) NOT NULL,
  `job_shift` int(11) NOT NULL,
  `job_type` int(11) NOT NULL,
  `job_education` text CHARACTER SET utf8 NOT NULL,
  `total_requirenment` int(11) NOT NULL,
  `desire_candidate_profile` text CHARACTER SET utf8 NOT NULL,
  `job_payment` int(15) NOT NULL,
  `no_of_views` int(11) NOT NULL COMMENT 'how much time view the post',
  `no_of_like` int(15) NOT NULL,
  `no_of_apply` int(11) NOT NULL COMMENT 'how much apply for the job',
  `posted_by` int(11) NOT NULL,
  `posted_on` datetime NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') NOT NULL DEFAULT 'UNAPPROVED',
  `currently_hiring_status` enum('Yes','No') DEFAULT 'Yes',
  `job_highlighted` enum('No','Yes') NOT NULL DEFAULT 'No',
  `is_deleted` enum('No','Yes') NOT NULL DEFAULT 'No',
  `job_life` int(11) NOT NULL DEFAULT '0' COMMENT 'Job life days to job expired',
  `job_expired_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table for store posted job data';

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_posting_view`
-- (See below for the actual view)
--
CREATE TABLE `job_posting_view` (
`id` int(15)
,`job_title` varchar(255)
,`job_description` text
,`skill_keyword` text
,`location_hiring` varchar(200)
,`company_hiring` varchar(255)
,`link_job` varchar(255)
,`job_industry` int(11)
,`functional_area` int(11)
,`job_post_role` int(11)
,`work_experience_from` varchar(255)
,`work_experience_to` varchar(255)
,`job_salary_from` varchar(255)
,`job_salary_to` varchar(255)
,`currency_type` varchar(255)
,`job_shift` int(11)
,`job_type` int(11)
,`job_education` text
,`total_requirenment` int(11)
,`desire_candidate_profile` text
,`job_payment` int(15)
,`no_of_views` int(11)
,`no_of_like` int(15)
,`no_of_apply` int(11)
,`posted_by` int(11)
,`posted_on` datetime
,`status` enum('APPROVED','UNAPPROVED')
,`currently_hiring_status` enum('Yes','No')
,`job_highlighted` enum('No','Yes')
,`is_deleted` enum('No','Yes')
,`job_life` int(11)
,`job_expired_on` date
,`industries_name` varchar(255)
,`functional_name` varchar(255)
,`job_type_name` varchar(255)
,`job_shift_type` varchar(255)
,`job_role_name` varchar(255)
,`posted_by_employee` varchar(255)
,`email` varchar(255)
,`profile_pic` varchar(255)
,`company_name` text
,`profile_pic_approval` enum('UNAPPROVED','APPROVED')
,`company_profile` text
,`company_website` text
,`company_email` varchar(255)
,`company_logo` varchar(255)
,`company_logo_approval` enum('UNAPPROVED','APPROVED')
,`employer_delete` enum('Yes','No')
,`employer_status` enum('APPROVED','UNAPPROVED')
);

-- --------------------------------------------------------

--
-- Table structure for table `job_type_master`
--

CREATE TABLE `job_type_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `job_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_type_master`
--

INSERT INTO `job_type_master` (`id`, `status`, `job_type`, `is_deleted`) VALUES
(1, 'APPROVED', 'Temporary/Contractual', 'No'),
(2, 'APPROVED', 'Permanent', 'No'),
(3, 'APPROVED', 'N Demo Job Type', 'Yes'),
(4, 'APPROVED', 'hii', 'Yes'),
(5, 'APPROVED', 'df dsf dsfgdgfdg', 'Yes'),
(6, 'APPROVED', 'dssf', 'Yes'),
(7, 'APPROVED', 'dfdfgg', 'Yes'),
(8, 'APPROVED', 'Test', 'No'),
(9, 'APPROVED', 'select * from admin_user;', 'Yes'),
(10, 'APPROVED', 'QUERY', 'No'),
(11, 'APPROVED', 'show tables;', 'No');

-- --------------------------------------------------------

--
-- Stand-in structure for view `js_education_view`
-- (See below for the actual view)
--
CREATE TABLE `js_education_view` (
`id` int(20)
,`js_id` int(11)
,`passing_year` year(4)
,`qualification_level` varchar(255)
,`institute` varchar(255)
,`specialization` varchar(255)
,`marks` varchar(255)
,`is_certificate_X_XII` enum('Certi','Edu','X','XII')
,`update_on` datetime
,`educational_qualification` varchar(255)
,`qualification_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `key_skill_master`
--

CREATE TABLE `key_skill_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `key_skill_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `key_skill_master`
--

INSERT INTO `key_skill_master` (`id`, `status`, `key_skill_name`, `is_deleted`) VALUES
(1, 'APPROVED', 'Business Planning', 'No'),
(2, 'APPROVED', 'Communication', 'No'),
(3, 'APPROVED', 'Conflict Management', 'No'),
(4, 'APPROVED', 'Creativity', 'No'),
(5, 'APPROVED', 'Decision Making', 'No'),
(6, 'APPROVED', 'Entrepreneurial', 'No'),
(7, 'APPROVED', 'Accounting', 'No'),
(8, 'APPROVED', 'Tax Planning', 'No'),
(9, 'APPROVED', 'Inter-Personal', 'No'),
(10, 'APPROVED', 'Leadership', 'No'),
(11, 'APPROVED', 'Management', 'No'),
(12, 'APPROVED', 'Negotiation', 'No'),
(13, 'APPROVED', 'Networking', 'No'),
(14, 'APPROVED', 'Personnel Management', 'No'),
(15, 'APPROVED', 'Presentation', 'No'),
(16, 'APPROVED', 'Selling', 'No'),
(17, 'APPROVED', 'Stress Management', 'No'),
(18, 'APPROVED', 'Teamwork & Team Building', 'No'),
(19, 'APPROVED', 'Writing', 'No'),
(20, 'APPROVED', 'CCC', 'No'),
(21, 'APPROVED', '4th Dimenssion', 'No'),
(22, 'APPROVED', 'MS Office', 'Yes'),
(23, 'APPROVED', 'Hardware', 'No'),
(25, 'APPROVED', 'Basic Knowledge', 'Yes'),
(26, 'APPROVED', '3D Studio Max', 'No'),
(27, 'APPROVED', 'Abap4', 'Yes'),
(28, 'APPROVED', 'Windows server', 'No'),
(29, 'APPROVED', 'Email', 'No'),
(30, 'APPROVED', 'Web and Social Skills', 'No'),
(31, 'APPROVED', 'Corel Draw', 'No'),
(32, 'APPROVED', 'End User Support', 'No'),
(33, 'APPROVED', 'Internet', 'No'),
(34, 'APPROVED', 'Web Page Design', 'No'),
(35, 'APPROVED', 'Adobe Illustrator', 'No'),
(36, 'APPROVED', 'Adobe Photoshop', 'No'),
(37, 'APPROVED', 'Autocad', 'No'),
(38, 'APPROVED', 'Database Design', 'No'),
(39, 'APPROVED', 'Ethernet', 'No'),
(40, 'APPROVED', 'Frontpage', 'No'),
(41, 'APPROVED', 'FTP', 'No'),
(42, 'APPROVED', 'LINUX', 'No'),
(43, 'APPROVED', 'Macintosh', 'No'),
(44, 'APPROVED', 'Macromedia Dreamweaver', 'No'),
(45, 'APPROVED', 'Matlab', 'No'),
(46, 'APPROVED', 'MVS', 'No'),
(47, 'APPROVED', 'Object Oriented Programming', 'No'),
(48, 'APPROVED', 'Optical Fibre', 'No'),
(49, 'APPROVED', 'Pagemaker', 'No'),
(50, 'APPROVED', 'DTP', 'No'),
(51, 'APPROVED', 'SAP', 'No'),
(52, 'APPROVED', 'WAN', 'No'),
(53, 'APPROVED', 'LAN', 'No'),
(54, 'APPROVED', 'MAN', 'No'),
(55, 'APPROVED', 'Tally', 'No'),
(59, 'APPROVED', 'AccPac', 'Yes'),
(60, 'APPROVED', 'ActiveX', 'No'),
(61, 'APPROVED', 'ADABAS', 'No'),
(62, 'APPROVED', 'ADO.NET', 'No'),
(65, 'APPROVED', 'AIX', 'No'),
(66, 'APPROVED', 'Apache', 'No'),
(67, 'APPROVED', 'Appletalk', 'No'),
(68, 'APPROVED', 'ASP', 'Yes'),
(69, 'APPROVED', 'ASP.NET', 'No'),
(70, 'APPROVED', 'Assembly Language', 'No'),
(71, 'APPROVED', 'Asynch', 'No'),
(72, 'APPROVED', 'ATM', 'No'),
(74, 'APPROVED', 'Baan', 'No'),
(75, 'APPROVED', 'Banyan Vines', 'No'),
(76, 'APPROVED', 'BASIC', 'No'),
(77, 'APPROVED', 'Bay Networks', 'No'),
(78, 'APPROVED', 'Bluetooth', 'No'),
(79, 'APPROVED', 'Bridges', 'No'),
(80, 'APPROVED', 'C Language', 'No'),
(81, 'APPROVED', 'C#', 'No'),
(82, 'APPROVED', 'C#.NET', 'No'),
(83, 'APPROVED', 'C++ Language', 'No'),
(84, 'APPROVED', 'CCMail', 'No'),
(85, 'APPROVED', 'CDMA', 'No'),
(86, 'APPROVED', 'CGI-Script', 'No'),
(87, 'APPROVED', 'CICS', 'No'),
(89, 'APPROVED', 'Cisco Routers', 'No'),
(90, 'APPROVED', 'Clipper', 'No'),
(91, 'APPROVED', 'Cobol', 'No'),
(92, 'APPROVED', 'Cold Fusion', 'No'),
(93, 'APPROVED', 'COM/DCOM/MTS', 'No'),
(94, 'APPROVED', 'CORBA', 'No'),
(96, 'APPROVED', 'Crystal Report', 'No'),
(97, 'APPROVED', 'Cytrix', 'No'),
(98, 'APPROVED', 'Data Mining', 'No'),
(99, 'APPROVED', 'Data Modeling', 'No'),
(100, 'APPROVED', 'Data Warehouse', 'No'),
(102, 'APPROVED', 'DB2', 'No'),
(103, 'APPROVED', 'DBase', 'No'),
(104, 'APPROVED', 'DECNET', 'No'),
(105, 'APPROVED', 'Delphi', 'No'),
(106, 'APPROVED', 'DHTML', 'No'),
(107, 'APPROVED', 'EDI', 'No'),
(108, 'APPROVED', 'Eiffel', 'No'),
(109, 'APPROVED', 'EJB', 'No'),
(111, 'APPROVED', 'FDDI', 'No'),
(112, 'APPROVED', 'Fortran', 'No'),
(113, 'APPROVED', 'Foxpro', 'No'),
(115, 'APPROVED', 'Frame Relay', 'No'),
(118, 'APPROVED', 'GSM', 'No'),
(119, 'APPROVED', 'Gupta/Centura', 'No'),
(120, 'APPROVED', 'Harvard Graphics', 'No'),
(121, 'APPROVED', 'HDLC', 'No'),
(122, 'APPROVED', 'Homesite', 'No'),
(123, 'APPROVED', 'HP Open View', 'No'),
(124, 'APPROVED', 'HPUX', 'No'),
(125, 'APPROVED', 'HTML', 'No'),
(126, 'APPROVED', 'Hubs', 'No'),
(127, 'APPROVED', 'IIS', 'No'),
(128, 'APPROVED', 'IMS', 'No'),
(129, 'APPROVED', 'Informix', 'No'),
(130, 'APPROVED', 'Ingres', 'No'),
(131, 'APPROVED', 'Interbase', 'No'),
(133, 'APPROVED', 'Internet Security', 'No'),
(134, 'APPROVED', 'Iplanet', 'No'),
(135, 'APPROVED', 'ISAM', 'No'),
(137, 'APPROVED', 'ISDN', 'No'),
(138, 'APPROVED', 'Jasmine', 'No'),
(139, 'APPROVED', 'Java', 'No'),
(140, 'APPROVED', 'Java Applets', 'No'),
(141, 'APPROVED', 'Java Script', 'No'),
(142, 'APPROVED', 'Java Servlets', 'No'),
(143, 'APPROVED', 'JBuilder', 'No'),
(144, 'APPROVED', 'JD Edwards', 'No'),
(145, 'APPROVED', 'JDBC', 'No'),
(146, 'APPROVED', 'JMS', 'No'),
(147, 'APPROVED', 'Jrun', 'No'),
(148, 'APPROVED', 'JSF', 'No'),
(149, 'APPROVED', 'JSP', 'No'),
(150, 'APPROVED', 'Kais Powertools', 'No'),
(151, 'APPROVED', 'L View Pro', 'No'),
(153, 'APPROVED', 'Lan Manager', 'No'),
(154, 'APPROVED', 'Lan Server', 'No'),
(155, 'APPROVED', 'Lantastic', 'No'),
(156, 'APPROVED', 'Lease Lines', 'No'),
(158, 'APPROVED', 'LISP', 'No'),
(159, 'APPROVED', 'LOGO', 'No'),
(160, 'APPROVED', 'Lotus 123', 'No'),
(161, 'APPROVED', 'Lotus Domino', 'No'),
(162, 'APPROVED', 'Lotus Notes', 'No'),
(163, 'APPROVED', 'Lotus Smart Suite', 'No'),
(165, 'APPROVED', 'Macromedia Director', 'No'),
(167, 'APPROVED', 'Macromedia Fireworks', 'No'),
(168, 'APPROVED', 'Macromedia Flash', 'No'),
(169, 'APPROVED', 'Macromedia Freehand', 'No'),
(171, 'APPROVED', 'Manhatan Application', 'No'),
(173, 'APPROVED', 'Maya', 'No'),
(174, 'APPROVED', 'Motif-XWindows', 'No'),
(175, 'APPROVED', 'MS Access', 'Yes'),
(177, 'APPROVED', 'MS DOS', 'Yes'),
(178, 'APPROVED', 'MS Excel', 'Yes'),
(179, 'APPROVED', 'MS Exchange', 'Yes'),
(181, 'APPROVED', 'MS Powerpoint', 'Yes'),
(182, 'APPROVED', 'MS Project', 'Yes'),
(183, 'APPROVED', 'MS Publisher', 'Yes'),
(184, 'APPROVED', 'MS Visual Sourcesafe', 'Yes'),
(185, 'APPROVED', 'MS Word', 'No'),
(186, 'APPROVED', 'Multinet', 'No'),
(188, 'APPROVED', 'MYOB', 'No'),
(189, 'APPROVED', 'MySql', 'No'),
(190, 'APPROVED', 'Net Object Fusion', 'No'),
(191, 'APPROVED', 'Netbios', 'No'),
(192, 'APPROVED', 'Netscape', 'No'),
(193, 'APPROVED', 'Network Cards', 'No'),
(195, 'APPROVED', 'Novell', 'No'),
(197, 'APPROVED', 'Object Stone', 'No'),
(198, 'APPROVED', 'ODBC', 'No'),
(199, 'APPROVED', 'OLAP', 'No'),
(201, 'APPROVED', 'Oracle 7', 'No'),
(202, 'APPROVED', 'Oracle 8', 'No'),
(203, 'APPROVED', 'Oracle Applications', 'No'),
(204, 'APPROVED', 'Oracle Designer', 'No'),
(205, 'APPROVED', 'OS/2', 'No'),
(206, 'APPROVED', 'OS/400', 'No'),
(207, 'APPROVED', 'PABX', 'No'),
(209, 'APPROVED', 'Paint Shop Pro', 'No'),
(210, 'APPROVED', 'PALM', 'No'),
(211, 'APPROVED', 'Paradox', 'No'),
(213, 'APPROVED', 'Pascal', 'No'),
(214, 'APPROVED', 'Peoplesoft', 'No'),
(215, 'APPROVED', 'Perl', 'No'),
(216, 'APPROVED', 'PHP', 'No'),
(217, 'APPROVED', 'PL/SQL', 'No'),
(218, 'APPROVED', 'Power Builder', 'No'),
(219, 'APPROVED', 'Pro\'C', 'No'),
(220, 'APPROVED', 'Progress', 'No'),
(221, 'APPROVED', 'Progress 4GL', 'No'),
(222, 'APPROVED', 'Prolog', 'No'),
(223, 'APPROVED', 'Python', 'No'),
(224, 'APPROVED', 'Quark Express', 'No'),
(225, 'APPROVED', 'Quark XPress', 'No'),
(226, 'APPROVED', 'Quattro Pro', 'No'),
(227, 'APPROVED', 'Rational Rose', 'No'),
(228, 'APPROVED', 'Retek Application', 'No'),
(229, 'APPROVED', 'REXX', 'No'),
(230, 'APPROVED', 'RPG', 'No'),
(232, 'APPROVED', 'SAS', 'No'),
(234, 'APPROVED', 'SCO UNIX', 'No'),
(235, 'APPROVED', 'Semantic Cafe', 'No'),
(236, 'APPROVED', 'Shell Scripts', 'No'),
(237, 'APPROVED', 'Site Server', 'No'),
(238, 'APPROVED', 'Small Talk', 'No'),
(239, 'APPROVED', 'SMS', 'No'),
(240, 'APPROVED', 'SOLARIS', 'No'),
(241, 'APPROVED', 'Sound Forge', 'No'),
(242, 'APPROVED', 'SPSS', 'No'),
(243, 'APPROVED', 'SQL Server', 'No'),
(244, 'APPROVED', 'Star Office', 'No'),
(245, 'APPROVED', 'Sun OS', 'No'),
(246, 'APPROVED', 'SWING', 'No'),
(247, 'APPROVED', 'Sybase', 'No'),
(248, 'APPROVED', 'System 7.0', 'No'),
(249, 'APPROVED', 'Tandem', 'No'),
(250, 'APPROVED', 'UBS', 'No'),
(251, 'APPROVED', 'ULEAD COOL 3D', 'No'),
(252, 'APPROVED', 'Unify', 'No'),
(253, 'APPROVED', 'UNIX', 'No'),
(254, 'APPROVED', 'VAX RDB', 'No'),
(255, 'APPROVED', 'VB.NET', 'No'),
(256, 'APPROVED', 'VBScript', 'No'),
(257, 'APPROVED', 'Versant', 'No'),
(258, 'APPROVED', 'Visio', 'No'),
(259, 'APPROVED', 'Visual Age', 'No'),
(260, 'APPROVED', 'Visual Basic', 'No'),
(261, 'APPROVED', 'Visual C++', 'No'),
(262, 'APPROVED', 'Visual Cafe', 'No'),
(263, 'APPROVED', 'Visual InterDev', 'No'),
(264, 'APPROVED', 'Visual J++', 'No'),
(265, 'APPROVED', 'VMS', 'No'),
(266, 'APPROVED', 'VSAM', 'No'),
(268, 'APPROVED', 'Weblogic', 'No'),
(269, 'APPROVED', 'WebSphere', 'No'),
(270, 'APPROVED', 'WinCE', 'No'),
(271, 'APPROVED', 'Windows 2000', 'No'),
(272, 'APPROVED', 'Windows 95/98', 'No'),
(273, 'APPROVED', 'Windows NT 4.0', 'No'),
(275, 'APPROVED', 'Windows XP', 'No'),
(276, 'APPROVED', 'WML', 'No'),
(277, 'APPROVED', 'Word Perfect', 'No'),
(278, 'APPROVED', 'X Windows', 'No'),
(279, 'APPROVED', 'XENIX', 'No'),
(280, 'APPROVED', 'XML', 'No'),
(281, 'APPROVED', 'Teamwork', 'No'),
(282, 'APPROVED', 'Responsibility', 'No'),
(283, 'APPROVED', 'Commitment to career', 'No'),
(284, 'APPROVED', 'Commercial awareness', 'No'),
(285, 'APPROVED', 'Career motivation', 'No'),
(289, 'APPROVED', 'Trustworthiness & Ethics', 'No'),
(290, 'APPROVED', 'Results orientation', 'No'),
(291, 'APPROVED', 'Problem solving', 'No'),
(292, 'APPROVED', 'Organisation', 'No'),
(294, 'APPROVED', 'Negotiating', 'Yes'),
(296, 'APPROVED', 'Coaching', 'No'),
(297, 'APPROVED', 'Teaching', 'No'),
(298, 'APPROVED', 'Quick learning', 'No'),
(299, 'APPROVED', 'Effective study', 'No'),
(300, 'APPROVED', 'Analytical', 'No'),
(301, 'APPROVED', 'Risk Taking', 'No'),
(302, 'APPROVED', 'Sales Ability', 'No'),
(303, 'APPROVED', 'Resourcefulness', 'No'),
(304, 'APPROVED', 'Reliability', 'No'),
(306, 'APPROVED', 'Determination', 'No'),
(307, 'APPROVED', 'Ethics', 'No'),
(308, 'APPROVED', 'Critical thinking', 'No'),
(309, 'APPROVED', 'Customer service', 'No'),
(310, 'APPROVED', 'Patience', 'No'),
(311, 'APPROVED', 'Multi-Tasking', 'No'),
(312, 'APPROVED', 'Diplomacy', 'No'),
(314, 'APPROVED', 'Active Listening', 'No'),
(315, 'APPROVED', 'Administrative', 'No'),
(317, 'APPROVED', 'Behavioral', 'No'),
(318, 'APPROVED', 'Blue Collar Job Skills', 'No'),
(319, 'APPROVED', 'Business Intelligence', 'No'),
(320, 'APPROVED', 'Business', 'No'),
(321, 'APPROVED', 'Business Storytelling', 'No'),
(322, 'APPROVED', 'Collaboration', 'No'),
(324, 'APPROVED', 'Computer', 'No'),
(326, 'APPROVED', 'Conflict Resolution', 'No'),
(327, 'APPROVED', 'Consulting', 'No'),
(328, 'APPROVED', 'Content Strategy', 'No'),
(329, 'APPROVED', 'Creative Thinking', 'No'),
(333, 'APPROVED', 'Delegation', 'No'),
(334, 'APPROVED', 'Deductive Reasoning', 'No'),
(335, 'APPROVED', 'Digital Marketing', 'No'),
(336, 'APPROVED', 'Digital Media', 'No'),
(337, 'APPROVED', 'Editing', 'No'),
(339, 'APPROVED', 'Finance', 'No'),
(340, 'APPROVED', 'Flexibility', 'No'),
(341, 'APPROVED', 'General', 'No'),
(342, 'APPROVED', 'Hard Skills', 'No'),
(343, 'APPROVED', 'Health Care Skills Listed by Job', 'No'),
(344, 'APPROVED', 'High School Student Skills', 'No'),
(345, 'APPROVED', 'Hospitality Industry', 'No'),
(346, 'APPROVED', 'Inductive Reasoning', 'No'),
(347, 'APPROVED', 'Information Technology', 'No'),
(348, 'APPROVED', 'Interpersonal', 'No'),
(350, 'APPROVED', 'Legal', 'No'),
(351, 'APPROVED', 'Listening', 'No'),
(352, 'APPROVED', 'Logical Thinking', 'No'),
(354, 'APPROVED', 'Marketing', 'No'),
(355, 'APPROVED', 'Microsoft Office Skills', 'No'),
(356, 'APPROVED', 'Motivational', 'No'),
(357, 'APPROVED', 'Multitasking', 'Yes'),
(358, 'APPROVED', 'Negotiations', 'Yes'),
(359, 'APPROVED', 'Nonverbal Communication', 'No'),
(360, 'APPROVED', 'Organizational', 'No'),
(361, 'APPROVED', 'Personal', 'No'),
(362, 'APPROVED', 'Persuasive', 'No'),
(365, 'APPROVED', 'Public Speaking', 'No'),
(366, 'APPROVED', 'QuickBooks', 'No'),
(367, 'APPROVED', 'Research', 'No'),
(368, 'APPROVED', 'Retail Industry', 'No'),
(369, 'APPROVED', 'Sales', 'No'),
(370, 'APPROVED', 'Social Media', 'No'),
(371, 'APPROVED', 'Soft Skills', 'No'),
(372, 'APPROVED', 'Strategic Planning', 'No'),
(373, 'APPROVED', 'Strengths (List of Examples)', 'No'),
(374, 'APPROVED', 'Team Building', 'No'),
(376, 'APPROVED', 'Tech Skills Listed by Job', 'No'),
(377, 'APPROVED', 'Technical', 'No'),
(378, 'APPROVED', 'Time Management', 'No'),
(379, 'APPROVED', 'Transferable', 'No'),
(380, 'APPROVED', 'Verbal Communication', 'No'),
(383, 'APPROVED', 'Adobe InDesign', 'No'),
(385, 'APPROVED', 'Analytics', 'Yes'),
(386, 'APPROVED', 'Android', 'No'),
(387, 'APPROVED', 'APIs', 'No'),
(388, 'APPROVED', 'Art Design', 'No'),
(390, 'APPROVED', 'Backup Management', 'No'),
(391, 'APPROVED', 'C', 'No'),
(392, 'APPROVED', 'C++', 'No'),
(393, 'APPROVED', 'Certifications', 'No'),
(394, 'APPROVED', 'Client Server', 'No'),
(395, 'APPROVED', 'Client Support', 'No'),
(396, 'APPROVED', 'Configuration', 'No'),
(397, 'APPROVED', 'Content Managment', 'No'),
(398, 'APPROVED', 'Content Management Systems (CMS)', 'Yes'),
(400, 'APPROVED', 'Corel Word Perfect', 'No'),
(401, 'APPROVED', 'CSS', 'No'),
(402, 'APPROVED', 'Data Analytics', 'No'),
(403, 'APPROVED', 'Desktop Publishing', 'No'),
(404, 'APPROVED', 'Design', 'No'),
(405, 'APPROVED', 'Diagnostics', 'No'),
(406, 'APPROVED', 'Documentation', 'No'),
(409, 'APPROVED', 'Engineering', 'No'),
(410, 'APPROVED', 'Excel', 'Yes'),
(411, 'APPROVED', 'FileMaker Pro', 'No'),
(413, 'APPROVED', 'Graphic Design', 'No'),
(415, 'APPROVED', 'Help Desk', 'No'),
(417, 'APPROVED', 'Implementation', 'No'),
(418, 'APPROVED', 'Installation', 'No'),
(420, 'APPROVED', 'iOS', 'No'),
(421, 'APPROVED', 'iPhone', 'No'),
(424, 'APPROVED', 'Javascript', 'No'),
(425, 'APPROVED', 'Mac', 'No'),
(428, 'APPROVED', 'Microsoft Excel', 'No'),
(429, 'APPROVED', 'Microsoft Office', 'No'),
(430, 'APPROVED', 'Microsoft Outlook', 'No'),
(431, 'APPROVED', 'Microsoft Publisher', 'No'),
(432, 'APPROVED', 'Microsoft Word', 'No'),
(433, 'APPROVED', 'Microsoft Visual', 'No'),
(434, 'APPROVED', 'Mobile', 'No'),
(436, 'APPROVED', 'Networks', 'Yes'),
(437, 'APPROVED', 'Open Source Software', 'No'),
(438, 'APPROVED', 'Oracle', 'No'),
(441, 'APPROVED', 'Presentations', 'Yes'),
(442, 'APPROVED', 'Processing', 'No'),
(443, 'APPROVED', 'Programming', 'No'),
(444, 'APPROVED', 'PT Modeler', 'No'),
(447, 'APPROVED', 'Ruby', 'No'),
(448, 'APPROVED', 'Shade', 'No'),
(449, 'APPROVED', 'Software', 'No'),
(450, 'APPROVED', 'Spreadsheet', 'No'),
(451, 'APPROVED', 'SQL', 'No'),
(452, 'APPROVED', 'Support', 'No'),
(453, 'APPROVED', 'Systems Administration', 'No'),
(454, 'APPROVED', 'Tech Support', 'No'),
(455, 'APPROVED', 'Troubleshooting', 'No'),
(457, 'APPROVED', 'UI/UX', 'No'),
(459, 'APPROVED', 'Windows', 'No'),
(460, 'APPROVED', 'Word Processing', 'No'),
(462, 'APPROVED', 'XHTML', 'No'),
(463, 'APPROVED', 'HTML 5', 'No'),
(464, 'APPROVED', 'Bootstrap', 'No'),
(465, 'APPROVED', 'Ajax', 'No'),
(466, 'APPROVED', 'jQuery', 'No'),
(467, 'APPROVED', 'Node.js', 'No'),
(468, 'APPROVED', 'Angular', 'No'),
(469, 'APPROVED', 'JSON', 'No'),
(470, 'APPROVED', 'N Demo Skill', 'Yes'),
(471, 'UNAPPROVED', 'Max', 'No'),
(474, 'APPROVED', 'Abpa4', 'No'),
(481, 'APPROVED', 'Data', 'Yes'),
(482, 'APPROVED', 'Data', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `key_skill_master_new`
--

CREATE TABLE `key_skill_master_new` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `key_skill_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status_master`
--

CREATE TABLE `marital_status_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `marital_status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marital_status_master`
--

INSERT INTO `marital_status_master` (`id`, `status`, `marital_status`, `is_deleted`) VALUES
(1, 'APPROVED', 'Widowed', 'No'),
(2, 'APPROVED', 'Married', 'No'),
(3, 'APPROVED', 'Unmarried', 'No'),
(4, 'APPROVED', 'Divorced', 'No'),
(5, 'APPROVED', 'Separated', 'No'),
(6, 'UNAPPROVED', 'ggggg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `sender` varchar(150) CHARACTER SET utf8 NOT NULL,
  `receiver` varchar(150) CHARACTER SET utf8 NOT NULL,
  `subject` text CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `status` enum('draft','sent') CHARACTER SET utf8 NOT NULL,
  `sent_on` datetime NOT NULL,
  `trash_sender` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `trash_receiver` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `sender_type` enum('job_seeker','employer') NOT NULL DEFAULT 'job_seeker',
  `read_status` enum('Unread','Read') NOT NULL DEFAULT 'Unread'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personal_titles_master`
--

CREATE TABLE `personal_titles_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `personal_titles` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_titles_master`
--

INSERT INTO `personal_titles_master` (`id`, `status`, `personal_titles`, `is_deleted`) VALUES
(1, 'APPROVED', 'Mr.', 'No'),
(2, 'APPROVED', 'Mrs.', 'No'),
(3, 'APPROVED', 'Ms.', 'No'),
(4, 'APPROVED', 'Ma\'am', 'No'),
(5, 'APPROVED', 'Dr.', 'No'),
(6, 'APPROVED', 'Prof.', 'No'),
(7, 'APPROVED', 'Er.', 'No'),
(8, 'APPROVED', 'a', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `plan_employer`
--

CREATE TABLE `plan_employer` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `current_plan` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `plan_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `plan_type` enum('Free','Paid') CHARACTER SET utf8 NOT NULL DEFAULT 'Paid',
  `plan_currency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `plan_amount` double NOT NULL,
  `plan_duration` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `message_used` int(11) NOT NULL,
  `job_life` int(11) NOT NULL,
  `job_post_limit` int(11) NOT NULL,
  `job_post_limit_used` int(11) NOT NULL,
  `contacts` int(11) NOT NULL,
  `contacts_used` int(11) NOT NULL,
  `cv_access_limit` int(11) NOT NULL,
  `cv_access_limit_used` int(11) NOT NULL,
  `highlight_job_limit` int(11) NOT NULL,
  `highlight_job_limit_used` int(11) NOT NULL,
  `plan_offers` text CHARACTER SET utf8 NOT NULL,
  `coupan_code` varchar(50) NOT NULL,
  `discount_amount` double NOT NULL,
  `service_tax` double NOT NULL,
  `final_amount` double NOT NULL,
  `activated_on` date NOT NULL,
  `expired_on` date NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_note` text NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `plan_jobseeker`
--

CREATE TABLE `plan_jobseeker` (
  `id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `current_plan` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `plan_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `plan_type` enum('Free','Paid') CHARACTER SET utf8 NOT NULL DEFAULT 'Paid',
  `plan_currency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `plan_amount` double NOT NULL,
  `plan_duration` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `message_used` int(11) NOT NULL,
  `highlight_application` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `relevant_jobs` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `performance_report` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `contacts` int(11) NOT NULL,
  `contacts_used` int(11) NOT NULL,
  `job_post_notification` enum('No','Yes') CHARACTER SET utf8 NOT NULL,
  `plan_offers` text CHARACTER SET utf8 NOT NULL,
  `coupan_code` varchar(50) NOT NULL,
  `discount_amount` double NOT NULL,
  `service_tax` double NOT NULL,
  `final_amount` double NOT NULL,
  `activated_on` date NOT NULL,
  `expired_on` date NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_note` text NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply_table`
--

CREATE TABLE `reply_table` (
  `id` int(11) NOT NULL,
  `created_on` date NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `reply_from` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

CREATE TABLE `role_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `functional_area` int(11) NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_master`
--

INSERT INTO `role_master` (`id`, `status`, `functional_area`, `role_name`, `is_deleted`) VALUES
(1, 'APPROVED', 1, 'Accounts Exec./Accountant', 'No'),
(2, 'APPROVED', 1, 'Cost Accountant', 'No'),
(3, 'APPROVED', 1, 'Taxation(Direct) Mgr', 'No'),
(4, 'APPROVED', 1, 'Taxation(Indirect) Mgr', 'No'),
(5, 'APPROVED', 1, 'Accounts Mgr', 'No'),
(6, 'APPROVED', 1, 'Financial Accountant', 'No'),
(7, 'APPROVED', 1, 'ICWA', 'No'),
(8, 'APPROVED', 1, 'Chartered Accountant', 'No'),
(9, 'APPROVED', 1, 'Finance Exec.', 'No'),
(10, 'APPROVED', 1, 'Credit/Control Exec.', 'No'),
(11, 'APPROVED', 1, 'Investor Relationship-Exec./Mgr', 'No'),
(12, 'APPROVED', 1, 'Credit/Control Mgr', 'No'),
(13, 'APPROVED', 1, 'Financial Analyst', 'No'),
(14, 'APPROVED', 1, 'Audit Mgr', 'No'),
(15, 'APPROVED', 1, 'Forex Mgr', 'No'),
(16, 'APPROVED', 1, 'Treasury Mgr', 'No'),
(17, 'APPROVED', 1, 'Finance/Budgeting Mgr', 'No'),
(18, 'APPROVED', 1, 'Head/VP/GM-Finance/Audit', 'No'),
(19, 'APPROVED', 1, 'Head/VP/GM-Accounts', 'No'),
(20, 'APPROVED', 2, 'DSA/Company Rep.', 'No'),
(21, 'APPROVED', 2, 'Independent Rep.', 'No'),
(22, 'APPROVED', 2, 'Life-Insurance Agent', 'No'),
(23, 'APPROVED', 2, 'Non-Life Insurance Agent', 'No'),
(24, 'APPROVED', 2, 'Real Estate Agent', 'No'),
(25, 'APPROVED', 2, 'Travel Agent', 'No'),
(26, 'APPROVED', 3, 'Data Analyst', 'No'),
(27, 'APPROVED', 3, 'Financial Analyst', 'No'),
(28, 'APPROVED', 3, 'Business Analyst', 'No'),
(29, 'APPROVED', 3, 'Analytics Manager', 'No'),
(30, 'APPROVED', 3, 'Head/VP/GM - Analytics & BI', 'No'),
(31, 'APPROVED', 3, 'Fresher', 'No'),
(32, 'APPROVED', 3, 'Trainee', 'No'),
(33, 'APPROVED', 4, 'Architect', 'No'),
(34, 'APPROVED', 4, 'Draughtsman', 'No'),
(35, 'APPROVED', 4, 'Project Architect', 'No'),
(36, 'APPROVED', 4, 'Naval Architect', 'No'),
(37, 'APPROVED', 4, 'Landscape Architect', 'No'),
(38, 'APPROVED', 4, 'Town Planner', 'No'),
(39, 'APPROVED', 4, 'Interior Designer', 'No'),
(40, 'APPROVED', 4, 'Outside Consultant', 'No'),
(41, 'APPROVED', 4, 'Fresher', 'No'),
(42, 'APPROVED', 4, 'Trainee', 'No'),
(43, 'APPROVED', 5, 'Cust. Service Exec.', 'No'),
(44, 'APPROVED', 5, 'Cust. Service Mgr', 'No'),
(45, 'APPROVED', 5, 'Collections Officer', 'No'),
(46, 'APPROVED', 5, 'Collections Mgr', 'No'),
(47, 'APPROVED', 5, 'CRM/Phone/Internet Banking Exec.', 'No'),
(48, 'APPROVED', 5, 'Sales Officer', 'No'),
(49, 'APPROVED', 5, 'Credit Officer', 'No'),
(50, 'APPROVED', 5, 'Branch Mgr', 'No'),
(51, 'APPROVED', 5, 'Regional Mgr', 'No'),
(52, 'APPROVED', 5, 'National Head', 'No'),
(53, 'APPROVED', 5, 'Asset Operations/Documentation-Exec./Mgr', 'No'),
(54, 'APPROVED', 5, 'Domestic Private Banking-Exec./Mgr', 'No'),
(55, 'APPROVED', 5, 'Product Mgr-Auto/Home Loans', 'No'),
(56, 'APPROVED', 5, 'Cards-Sales Officer/Exec.', 'No'),
(57, 'APPROVED', 5, 'Cards OperationsExec.', 'No'),
(58, 'APPROVED', 5, 'Cards Operations Mgr', 'No'),
(59, 'APPROVED', 5, 'Collections Exec.', 'No'),
(60, 'APPROVED', 5, 'Card Approvals Officer', 'No'),
(61, 'APPROVED', 5, 'Merchant Acquisition Exec.', 'No'),
(62, 'APPROVED', 5, 'Business Alliances Mgr', 'No'),
(63, 'APPROVED', 5, 'Product Mgr-Cards', 'No'),
(64, 'APPROVED', 5, 'Back Office Exec.', 'No'),
(65, 'APPROVED', 5, 'Money Markets Dealer', 'No'),
(66, 'APPROVED', 5, 'Forex Dealer', 'No'),
(67, 'APPROVED', 5, 'Sales/BD Mgr-Forex', 'No'),
(68, 'APPROVED', 5, 'Forex Operations Mgr', 'No'),
(69, 'APPROVED', 5, 'Debt Instrument Dealer', 'No'),
(70, 'APPROVED', 5, 'Sales/BD Mgr-Debt Instruments', 'No'),
(71, 'APPROVED', 5, 'Debt Operations Mgr', 'No'),
(72, 'APPROVED', 5, 'Derivatives Dealer', 'No'),
(73, 'APPROVED', 5, 'Sales/BD Mgr-Derivatives', 'No'),
(74, 'APPROVED', 5, 'Treasury Operations Mgr', 'No'),
(75, 'APPROVED', 5, 'Clearing Officer', 'No'),
(76, 'APPROVED', 5, 'Cash Officer', 'No'),
(77, 'APPROVED', 5, 'Operations Officer', 'No'),
(78, 'APPROVED', 5, 'Operations Mgr', 'No'),
(79, 'APPROVED', 5, 'Depository Services-Exec./Mgr', 'No'),
(80, 'APPROVED', 5, 'Legal Officer', 'No'),
(81, 'APPROVED', 5, 'Legal Mgr', 'No'),
(82, 'APPROVED', 5, 'Operations Mgr', 'No'),
(83, 'APPROVED', 5, 'Trade Finance Operations Mgr', 'No'),
(84, 'APPROVED', 5, 'Technology Mgr', 'No'),
(85, 'APPROVED', 5, 'ATM Operations Mgr', 'No'),
(86, 'APPROVED', 5, 'Audit Mgr', 'No'),
(87, 'APPROVED', 5, 'Finance/Budgeting Mgr', 'No'),
(88, 'APPROVED', 5, 'EFT / ACH Manager', 'No'),
(89, 'APPROVED', 5, 'Relationship Exec.', 'No'),
(90, 'APPROVED', 5, 'Client Servicing/Key Account Mgr', 'No'),
(91, 'APPROVED', 5, 'Credit Analyst-Corporate Banking', 'No'),
(92, 'APPROVED', 5, 'Credit Mgr-Corporate Banking', 'No'),
(93, 'APPROVED', 5, 'Bad Debts/Workouts Mgr', 'No'),
(94, 'APPROVED', 5, 'Debt Analyst', 'No'),
(95, 'APPROVED', 5, 'Mergers & Acquisitions Analyst', 'No'),
(96, 'APPROVED', 5, 'Equity Analyst', 'No'),
(97, 'APPROVED', 5, 'Equity Mgr', 'No'),
(98, 'APPROVED', 5, 'Domestic Debt Mgr', 'No'),
(99, 'APPROVED', 5, 'Offshore Debt Mgr', 'No'),
(100, 'APPROVED', 5, 'Mergers&Acquisitions Mgr', 'No'),
(101, 'APPROVED', 5, 'Corporate Advisory Mgr', 'No'),
(102, 'APPROVED', 5, 'Project Finance Mgr', 'No'),
(103, 'APPROVED', 5, 'Issues/IPO Mgr', 'No'),
(104, 'APPROVED', 5, 'Legal Officer', 'No'),
(105, 'APPROVED', 5, 'Legal Mgr', 'No'),
(106, 'APPROVED', 5, 'Asset Manager', 'No'),
(107, 'APPROVED', 5, 'Wealth Manager', 'No'),
(108, 'APPROVED', 5, 'Insurance Analyst', 'No'),
(109, 'APPROVED', 5, 'Actuary Mgr', 'No'),
(110, 'APPROVED', 5, 'Underwriter', 'No'),
(111, 'APPROVED', 5, 'Insurance Advisor', 'No'),
(112, 'APPROVED', 5, 'Unit Mgr', 'No'),
(113, 'APPROVED', 5, 'Sales/BD-Mgr', 'No'),
(114, 'APPROVED', 5, 'Branch Mgr', 'No'),
(115, 'APPROVED', 5, 'Product Mgr', 'No'),
(116, 'APPROVED', 5, 'Sales Head', 'No'),
(117, 'APPROVED', 5, 'Regional Mgr', 'No'),
(118, 'APPROVED', 5, 'Legal Officer', 'No'),
(119, 'APPROVED', 5, 'Legal Mgr', 'No'),
(120, 'APPROVED', 5, 'Insurance Analyst', 'No'),
(121, 'APPROVED', 5, 'Actuary Mgr', 'No'),
(122, 'APPROVED', 5, 'Underwriter', 'No'),
(123, 'APPROVED', 5, 'Head-Underwriting', 'No'),
(124, 'APPROVED', 5, 'Insurance Advisor', 'No'),
(125, 'APPROVED', 5, 'Unit Mgr', 'No'),
(126, 'APPROVED', 5, 'Sales/BD-Mgr', 'No'),
(127, 'APPROVED', 5, 'Branch Mgr', 'No'),
(128, 'APPROVED', 5, 'Product Mgr', 'No'),
(129, 'APPROVED', 5, 'Sales Head', 'No'),
(130, 'APPROVED', 5, 'Regional Mgr', 'No'),
(131, 'APPROVED', 5, 'Legal Officer', 'No'),
(132, 'APPROVED', 5, 'Legal Mgr', 'No'),
(133, 'APPROVED', 5, 'Banc Assurance', 'No'),
(134, 'APPROVED', 5, 'Insurance Operations Officer', 'No'),
(135, 'APPROVED', 5, 'Insurance Operations Mgr', 'No'),
(136, 'APPROVED', 5, 'CRM/Cust. Service Exec.', 'No'),
(137, 'APPROVED', 5, 'CRM/Cust. Service Mgr', 'No'),
(138, 'APPROVED', 5, 'Claims Exec.', 'No'),
(139, 'APPROVED', 5, 'Claims Mgr', 'No'),
(140, 'APPROVED', 5, 'Investment/Treasury Mgr', 'No'),
(141, 'APPROVED', 5, 'Analyst', 'No'),
(142, 'APPROVED', 5, 'Broker/Trader', 'No'),
(143, 'APPROVED', 5, 'Sales/BD Mgr-Broking', 'No'),
(144, 'APPROVED', 5, 'Sales Exec./Investment Advisor', 'No'),
(145, 'APPROVED', 5, 'Sales/BD Mgr', 'No'),
(146, 'APPROVED', 5, 'Mktg Mgr', 'No'),
(147, 'APPROVED', 5, 'Portfolio Mgr', 'No'),
(148, 'APPROVED', 5, 'Analyst', 'No'),
(149, 'APPROVED', 5, 'CRM/Cust. Service Exec.', 'No'),
(150, 'APPROVED', 5, 'CRM/Cust. Service Mgr', 'No'),
(151, 'APPROVED', 5, 'Operations Exec.', 'No'),
(152, 'APPROVED', 5, 'Operations Mgr', 'No'),
(153, 'APPROVED', 5, 'Fund Mgr-Debt', 'No'),
(154, 'APPROVED', 5, 'Fund Mgr-Equity', 'No'),
(155, 'APPROVED', 5, 'Private Equity/Hedge Fund/VC-Mgr', 'No'),
(156, 'APPROVED', 5, 'Head/VP/GM-Treasury', 'No'),
(157, 'APPROVED', 5, 'Head/VP/GM-Legal', 'No'),
(158, 'APPROVED', 5, 'Head/VP/GM-Operations', 'No'),
(159, 'APPROVED', 5, 'Head/VP/GM-CFO/Financial Controller', 'No'),
(160, 'APPROVED', 5, 'Head/VP/GM-Depository Services', 'No'),
(161, 'APPROVED', 5, 'Head/VP/GM-Relationships', 'No'),
(162, 'APPROVED', 5, 'Head/VP/GM-Credit/Risk', 'No'),
(163, 'APPROVED', 5, 'Head/VP/GM-Equity', 'No'),
(164, 'APPROVED', 5, 'Head/VP/GM-Domestic/Offshore Debt', 'No'),
(165, 'APPROVED', 5, 'Head/VP/GM-Mergers & Acquisitions', 'No'),
(166, 'APPROVED', 5, 'Head/VP/GM-Corporate Advisory', 'No'),
(167, 'APPROVED', 5, 'Head/VP/GM-Project Finance', 'No'),
(168, 'APPROVED', 5, 'Head/VP/GM-Investment Banking', 'No'),
(169, 'APPROVED', 5, 'Head/VP/GM-Underwritting', 'No'),
(170, 'APPROVED', 5, 'Head/VP/GM-Mktg', 'No'),
(171, 'APPROVED', 5, 'Head/VP/GM-Insurance Operations', 'No'),
(172, 'APPROVED', 5, 'Head/VP/GM-Claims', 'No'),
(173, 'APPROVED', 5, 'Head/VP/GM-Sales', 'No'),
(174, 'APPROVED', 5, 'Head/VP/GM-Fund Management', 'No'),
(175, 'APPROVED', 5, 'Head/VP/GM-Private Equity/Hedge Fund/VC', 'No'),
(176, 'APPROVED', 5, 'Head/VP/GM-Broking', 'No'),
(177, 'APPROVED', 6, 'Hair Stylist', 'No'),
(178, 'APPROVED', 6, 'Make-Up Artist', 'No'),
(179, 'APPROVED', 6, 'Beauty Manager / Beautician', 'No'),
(180, 'APPROVED', 6, 'Spa Therapist', 'No'),
(181, 'APPROVED', 6, 'Yoga Trainer', 'No'),
(182, 'APPROVED', 6, 'Fitness Trainer / Gym Instructor', 'No'),
(183, 'APPROVED', 6, 'Slimming Manager', 'No'),
(184, 'APPROVED', 6, 'Centre Head / Branch Head / Club Manager', 'No'),
(185, 'APPROVED', 7, 'Content Developer', 'No'),
(186, 'APPROVED', 7, 'Freelance Journalist', 'No'),
(187, 'APPROVED', 7, 'Business Content Developer', 'No'),
(188, 'APPROVED', 7, 'Fashion Content Developer', 'No'),
(189, 'APPROVED', 7, 'Features Content Developer', 'No'),
(190, 'APPROVED', 7, 'Intnl Business Content Developer', 'No'),
(191, 'APPROVED', 7, 'IT/Technical Content Developer', 'No'),
(192, 'APPROVED', 7, 'Sports Content Developer', 'No'),
(193, 'APPROVED', 7, 'Political Content Developer', 'No'),
(194, 'APPROVED', 7, 'Journalist', 'No'),
(195, 'APPROVED', 7, 'Sub Editor/Reporter', 'No'),
(196, 'APPROVED', 7, 'Sr Sub Editor/Sr Reporter', 'No'),
(197, 'APPROVED', 7, 'Coresspondent/Asst. Editor/Associate Editor', 'No'),
(198, 'APPROVED', 7, 'Principal Coresspondent/Features Writer/Resident Writer', 'No'),
(199, 'APPROVED', 7, 'Chief of Bureau/Editor in Chief', 'No'),
(200, 'APPROVED', 7, 'Investigative Journalist', 'No'),
(201, 'APPROVED', 7, 'Proof Reader', 'No'),
(202, 'APPROVED', 7, 'Business Editor', 'No'),
(203, 'APPROVED', 7, 'Fashion Editor', 'No'),
(204, 'APPROVED', 7, 'Features Editor', 'No'),
(205, 'APPROVED', 7, 'Intnl Business Editor', 'No'),
(206, 'APPROVED', 7, 'IT/Technical Editor', 'No'),
(207, 'APPROVED', 7, 'Managing Editor', 'No'),
(208, 'APPROVED', 7, 'Sports Editor', 'No'),
(209, 'APPROVED', 7, 'Political Editor', 'No'),
(210, 'APPROVED', 7, 'Trainee', 'No'),
(211, 'APPROVED', 7, 'Fresher', 'No'),
(212, 'APPROVED', 8, 'Outside Consultant', 'No'),
(213, 'APPROVED', 8, 'Sr Outside Consultant', 'No'),
(214, 'APPROVED', 8, 'Corporate Planning/Strategy Mgr', 'No'),
(215, 'APPROVED', 8, 'Research Associate', 'No'),
(216, 'APPROVED', 8, 'Business Analyst', 'No'),
(217, 'APPROVED', 8, 'EA to Chairman/President/VP', 'No'),
(218, 'APPROVED', 8, 'Head/VP/GM-Corporate Planning/Strategy', 'No'),
(219, 'APPROVED', 8, 'VP/President/Partner', 'No'),
(220, 'APPROVED', 8, 'CEO/MD/Director', 'No'),
(221, 'APPROVED', 8, 'Trainees', 'No'),
(222, 'APPROVED', 8, 'Freshers', 'No'),
(223, 'APPROVED', 9, 'CSR Manager', 'No'),
(224, 'APPROVED', 9, 'SustainabilityManager', 'No'),
(225, 'APPROVED', 9, 'Head / VP / GM-CSR', 'No'),
(226, 'APPROVED', 9, 'Head / VP / GM-Sustainability', 'No'),
(227, 'APPROVED', 9, 'Other', 'No'),
(228, 'APPROVED', 10, 'R&D Exec.', 'No'),
(229, 'APPROVED', 10, 'Clinical Research Associate/Scientist', 'No'),
(230, 'APPROVED', 10, 'Clinical Research Mgr', 'No'),
(231, 'APPROVED', 10, 'Analytical Chemistry Associate/Scientist', 'No'),
(232, 'APPROVED', 10, 'Analytical Chemistry Mgr', 'No'),
(233, 'APPROVED', 10, 'Chemical Research Associate/Scientist', 'No'),
(234, 'APPROVED', 10, 'Chemical Research Mgr', 'No'),
(235, 'APPROVED', 10, 'Bio/Pharma Informatics-Associate/Scientist', 'No'),
(236, 'APPROVED', 10, 'Formulation Scientists', 'No'),
(237, 'APPROVED', 10, 'Microbiologist', 'No'),
(238, 'APPROVED', 10, 'Molecular Biology', 'No'),
(239, 'APPROVED', 10, 'Nutritionist', 'No'),
(240, 'APPROVED', 10, 'Research Scientist', 'No'),
(241, 'APPROVED', 10, 'Bio-Tech Research Associate/Scientist', 'No'),
(242, 'APPROVED', 10, 'Bio-Tech Research Mgr', 'No'),
(243, 'APPROVED', 10, 'Pharmacist/Chemist/Bio Chemist', 'No'),
(244, 'APPROVED', 10, 'Bio-Statistician', 'No'),
(245, 'APPROVED', 10, 'Lab Technician/Medical Technician/Lab Staff', 'No'),
(246, 'APPROVED', 10, 'Product Development Exec.', 'No'),
(247, 'APPROVED', 10, 'Product Development Mgr', 'No'),
(248, 'APPROVED', 10, 'Other Scientist', 'No'),
(249, 'APPROVED', 10, 'Drug Regulatory Dr.', 'No'),
(250, 'APPROVED', 10, 'Documentation/Medical Writing', 'No'),
(251, 'APPROVED', 10, 'Regulatory Affairs Mgr', 'No'),
(252, 'APPROVED', 10, 'QA&QC-Executive', 'No'),
(253, 'APPROVED', 10, 'QA&QC Mgr', 'No'),
(254, 'APPROVED', 10, 'Design Engineer', 'No'),
(255, 'APPROVED', 10, 'Sr. Design Engineer', 'No'),
(256, 'APPROVED', 10, 'Tech. Lead/Project Lead', 'No'),
(257, 'APPROVED', 10, 'Head/VP/GM-R&D', 'No'),
(258, 'APPROVED', 10, 'Head/VP/GM-Production', 'No'),
(259, 'APPROVED', 10, 'Head/VP/GM-Formulations', 'No'),
(260, 'APPROVED', 10, 'Head/VP/GM-QA/QC', 'No'),
(261, 'APPROVED', 10, 'Head/VP/GM-Regulatory Affairs', 'No'),
(262, 'APPROVED', 10, 'Research Associate', 'No'),
(263, 'APPROVED', 10, 'Fresher', 'No'),
(264, 'APPROVED', 10, 'Postdoc Position/Fellowship', 'No'),
(265, 'APPROVED', 10, 'Practical Training/Internship', 'No'),
(266, 'APPROVED', 10, 'Trainee', 'No'),
(267, 'APPROVED', 10, 'Other', 'No'),
(268, 'APPROVED', 11, 'Documentation/Shipping Exec./Mgr', 'No'),
(269, 'APPROVED', 11, 'Production Exec.', 'No'),
(270, 'APPROVED', 11, 'Purchase Officer', 'No'),
(271, 'APPROVED', 11, 'Floor Mgr', 'No'),
(272, 'APPROVED', 11, 'Production Mgr', 'No'),
(273, 'APPROVED', 11, 'Merchandiser', 'No'),
(274, 'APPROVED', 11, 'QA/QC Exec.', 'No'),
(275, 'APPROVED', 11, 'QA/QC Mgr', 'No'),
(276, 'APPROVED', 11, 'BD Mgr', 'No'),
(277, 'APPROVED', 11, 'Head/VP/GM-Documentation/Shipping', 'No'),
(278, 'APPROVED', 11, 'Head/VP/GM-Production', 'No'),
(279, 'APPROVED', 11, 'Head/VP/GM-Purchase', 'No'),
(280, 'APPROVED', 11, 'VP/GM-Quality', 'No'),
(281, 'APPROVED', 11, 'CEO/MD/Director', 'No'),
(282, 'APPROVED', 11, 'Liason Officer/Mgr', 'No'),
(283, 'APPROVED', 11, 'Trader', 'No'),
(284, 'APPROVED', 11, 'Agent', 'No'),
(285, 'APPROVED', 11, 'Other', 'No'),
(286, 'APPROVED', 11, 'Fresher', 'No'),
(287, 'APPROVED', 11, 'Trainee', 'No'),
(288, 'APPROVED', 12, 'Textile Designer', 'No'),
(289, 'APPROVED', 12, 'Other', 'No'),
(290, 'APPROVED', 12, 'Trainee', 'No'),
(291, 'APPROVED', 12, 'Merchandiser', 'No'),
(292, 'APPROVED', 12, 'Footwear Designer', 'No'),
(293, 'APPROVED', 12, 'Apparel/Garment Designer', 'No'),
(294, 'APPROVED', 12, 'Jewellery Designer', 'No'),
(295, 'APPROVED', 12, 'Accessory Designer', 'No'),
(296, 'APPROVED', 12, 'Freelancer', 'No'),
(297, 'APPROVED', 12, 'Fresher', 'No'),
(298, 'APPROVED', 13, 'Security Guard', 'No'),
(299, 'APPROVED', 13, 'Security Supervisor', 'No'),
(300, 'APPROVED', 13, 'Security Mgr', 'No'),
(301, 'APPROVED', 13, 'Policeman', 'No'),
(302, 'APPROVED', 13, 'Army/Navy/Airforce Personnel', 'No'),
(303, 'APPROVED', 13, 'Chief Security Officer', 'No'),
(304, 'APPROVED', 13, 'Other', 'No'),
(305, 'APPROVED', 13, 'Trainee', 'No'),
(306, 'APPROVED', 13, 'Fresher', 'No'),
(307, 'APPROVED', 14, 'Bartender', 'No'),
(308, 'APPROVED', 14, 'Commis', 'No'),
(309, 'APPROVED', 14, 'Night Manager', 'No'),
(310, 'APPROVED', 14, 'Pastry Chef', 'No'),
(311, 'APPROVED', 14, 'Bar Manager', 'No'),
(312, 'APPROVED', 14, 'Steward', 'No'),
(313, 'APPROVED', 14, 'Captain', 'No'),
(314, 'APPROVED', 14, 'Host/Hostess', 'No'),
(315, 'APPROVED', 14, 'Butler', 'No'),
(316, 'APPROVED', 14, 'Chef De Partis', 'No'),
(317, 'APPROVED', 14, 'Executive Sous Chef/Chef De Cuisine', 'No'),
(318, 'APPROVED', 14, 'Sous Chef', 'No'),
(319, 'APPROVED', 14, 'Banquet Sales Exec./ Mgr', 'No'),
(320, 'APPROVED', 14, 'Restaurant Mgr', 'No'),
(321, 'APPROVED', 14, 'F&B Mgr', 'No'),
(322, 'APPROVED', 14, 'General Manager', 'No'),
(323, 'APPROVED', 14, 'Waiter / Waitress / Sommelier', 'No'),
(324, 'APPROVED', 14, 'Busser', 'No'),
(325, 'APPROVED', 14, 'Housekeeping Exec./Asst.', 'No'),
(326, 'APPROVED', 14, 'Housekeeping Mgr', 'No'),
(327, 'APPROVED', 14, 'Cashier', 'No'),
(328, 'APPROVED', 14, 'Front Office/Guest Relations Exec./Mgr', 'No'),
(329, 'APPROVED', 14, 'Travel Desk Mgr', 'No'),
(330, 'APPROVED', 14, 'Lobby/Duty Mgr', 'No'),
(331, 'APPROVED', 14, 'Staff Function', 'No'),
(332, 'APPROVED', 14, 'Guest Service Agent', 'No'),
(333, 'APPROVED', 14, 'Concierge', 'No'),
(334, 'APPROVED', 14, 'Executive/Master Chef', 'No'),
(335, 'APPROVED', 14, 'Head/VP/GM-F&B', 'No'),
(336, 'APPROVED', 14, 'Head/VP/GM/National Manager-Sales', 'No'),
(337, 'APPROVED', 14, 'Head/VP/-PR/Corp. Communication', 'No'),
(338, 'APPROVED', 14, 'Head/VP/GM-Accounts', 'No'),
(339, 'APPROVED', 14, 'CEO/MD/Director', 'No'),
(340, 'APPROVED', 14, 'Health Club Asst./Mgr', 'No'),
(341, 'APPROVED', 14, 'Masseur', 'No'),
(342, 'APPROVED', 14, 'Leisure Staff / Manager', 'No'),
(343, 'APPROVED', 14, 'Revenue Manager', 'No'),
(344, 'APPROVED', 14, 'Spa Therapist', 'No'),
(345, 'APPROVED', 14, 'Security Manager / Officer', 'No'),
(346, 'APPROVED', 14, 'Casino Manager', 'No'),
(347, 'APPROVED', 14, 'Event Planner', 'No'),
(348, 'APPROVED', 14, 'Other', 'No'),
(349, 'APPROVED', 14, 'Fresher', 'No'),
(350, 'APPROVED', 14, 'Trainee', 'No'),
(351, 'APPROVED', 15, 'HR Executive', 'No'),
(352, 'APPROVED', 15, 'HR Manager', 'No'),
(353, 'APPROVED', 15, 'Recruitment Executive', 'No'),
(354, 'APPROVED', 15, 'Recruitment Manager', 'No'),
(355, 'APPROVED', 15, 'Pay Roll/Compensation Manager', 'No'),
(356, 'APPROVED', 15, 'Performance Management Manager', 'No'),
(357, 'APPROVED', 15, 'Industrial/Labour Relations Manager', 'No'),
(358, 'APPROVED', 15, 'Training Manager', 'No'),
(359, 'APPROVED', 15, 'Staffing Specialist/ Manpower Planning', 'No'),
(360, 'APPROVED', 15, 'HR Business Partner', 'No'),
(361, 'APPROVED', 15, 'Payroll Executive', 'No'),
(362, 'APPROVED', 15, 'Employee Relations Executive', 'No'),
(363, 'APPROVED', 15, 'Employee Relations Manager', 'No'),
(364, 'APPROVED', 15, 'Executive/ Sr Executive - Administration', 'No'),
(365, 'APPROVED', 15, 'Manager / Sr Manager - Administration', 'No'),
(366, 'APPROVED', 15, 'Executive/ Sr Executive - Facility Management', 'No'),
(367, 'APPROVED', 15, 'Manager / Sr Manager - Facility Management', 'No'),
(368, 'APPROVED', 15, 'Travel Desk- Coordinator', 'No'),
(369, 'APPROVED', 15, 'Transport Executive', 'No'),
(370, 'APPROVED', 15, 'Transport Manager', 'No'),
(371, 'APPROVED', 15, 'Head/VP/GM-HR', 'No'),
(372, 'APPROVED', 15, 'Head/VP/GM-Training & Development', 'No'),
(373, 'APPROVED', 15, 'Head/VP/GM-Admin & Facilities', 'No'),
(374, 'APPROVED', 15, 'Head/VP/GM-Recruitment', 'No'),
(375, 'APPROVED', 15, 'Head/VP/GM-Compensation & Benefits', 'No'),
(376, 'APPROVED', 15, 'Head/VP/GM-Facility Management', 'No'),
(377, 'APPROVED', 15, 'Outside Consultant', 'No'),
(378, 'APPROVED', 15, 'Other', 'No'),
(379, 'APPROVED', 15, 'Trainee', 'No'),
(380, 'APPROVED', 15, 'Fresher', 'No'),
(381, 'APPROVED', 16, 'Software Developer', 'No'),
(382, 'APPROVED', 16, 'Team Lead/Tech Lead', 'No'),
(383, 'APPROVED', 16, 'System Analyst', 'No'),
(384, 'APPROVED', 16, 'Tech Architect', 'No'),
(385, 'APPROVED', 16, 'Database Architect/Designer', 'No'),
(386, 'APPROVED', 16, 'Project Lead', 'No'),
(387, 'APPROVED', 16, 'Testing Engnr', 'No'),
(388, 'APPROVED', 16, 'Product Mgr', 'No'),
(389, 'APPROVED', 16, 'Graphic/Web Designer', 'No'),
(390, 'APPROVED', 16, 'Release Mgr', 'No'),
(391, 'APPROVED', 16, 'DBA', 'No'),
(392, 'APPROVED', 16, 'Network Admin', 'No'),
(393, 'APPROVED', 16, 'System Admin', 'No'),
(394, 'APPROVED', 16, 'System Security', 'No'),
(395, 'APPROVED', 16, 'Tech Support Engnr', 'No'),
(396, 'APPROVED', 16, 'Maintenance Engnr', 'No'),
(397, 'APPROVED', 16, 'Webmaster', 'No'),
(398, 'APPROVED', 16, 'IT/Networking-Mgr', 'No'),
(399, 'APPROVED', 16, 'Information Systems(MIS)-Mgr', 'No'),
(400, 'APPROVED', 16, 'System Integration Technician', 'No'),
(401, 'APPROVED', 16, 'Business Analyst', 'No'),
(402, 'APPROVED', 16, 'Datawarehousing Technician', 'No'),
(403, 'APPROVED', 16, 'Outside Technical Consultant', 'No'),
(404, 'APPROVED', 16, 'Functional Outside Consultant', 'No'),
(405, 'APPROVED', 16, 'EDP Analyst', 'No'),
(406, 'APPROVED', 16, 'Solution Architect/Enterprise Architect', 'No'),
(407, 'APPROVED', 16, 'Subject Matter Expert', 'No'),
(408, 'APPROVED', 16, 'ERP Consultant', 'No'),
(409, 'APPROVED', 16, 'Technical Writer', 'No'),
(410, 'APPROVED', 16, 'Instructional Designer', 'No'),
(411, 'APPROVED', 16, 'Technical Documentor', 'No'),
(412, 'APPROVED', 16, 'QA/QC Exec.', 'No'),
(413, 'APPROVED', 16, 'QA/QC Mgr', 'No'),
(414, 'APPROVED', 16, 'Project Mgr-IT/Software', 'No'),
(415, 'APPROVED', 16, 'Program Mgr', 'No'),
(416, 'APPROVED', 16, 'Head/VP/GM-Quality', 'No'),
(417, 'APPROVED', 16, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(418, 'APPROVED', 16, 'CIO', 'No'),
(419, 'APPROVED', 16, 'Practice Head/Practice Manager', 'No'),
(420, 'APPROVED', 16, 'Trainer/Faculty', 'No'),
(421, 'APPROVED', 16, 'Trainee', 'No'),
(422, 'APPROVED', 16, 'Fresher', 'No'),
(423, 'APPROVED', 16, 'Outside Consultant', 'No'),
(424, 'APPROVED', 16, 'IT/Technical Content Developer', 'No'),
(425, 'APPROVED', 17, 'Software Developer', 'No'),
(426, 'APPROVED', 17, 'Team Lead/Tech Lead', 'No'),
(427, 'APPROVED', 17, 'System Analyst', 'No'),
(428, 'APPROVED', 17, 'Tech Architect', 'No'),
(429, 'APPROVED', 17, 'Database Architect/Designer', 'No'),
(430, 'APPROVED', 17, 'Project Lead', 'No'),
(431, 'APPROVED', 17, 'Testing Engnr', 'No'),
(432, 'APPROVED', 17, 'Product Mgr', 'No'),
(433, 'APPROVED', 17, 'Graphic/Web Designer', 'No'),
(434, 'APPROVED', 17, 'Release Mgr', 'No'),
(435, 'APPROVED', 17, 'DBA', 'No'),
(436, 'APPROVED', 17, 'Network Admin', 'No'),
(437, 'APPROVED', 17, 'System Admin', 'No'),
(438, 'APPROVED', 17, 'System Security', 'No'),
(439, 'APPROVED', 17, 'Tech Support Engnr', 'No'),
(440, 'APPROVED', 17, 'Maintenance Engnr', 'No'),
(441, 'APPROVED', 17, 'Webmaster', 'No'),
(442, 'APPROVED', 17, 'IT/Networking-Mgr', 'No'),
(443, 'APPROVED', 17, 'Information Systems(MIS)-Mgr', 'No'),
(444, 'APPROVED', 17, 'System Integration Technician', 'No'),
(445, 'APPROVED', 17, 'Business Analyst', 'No'),
(446, 'APPROVED', 17, 'Datawarehousing Technician', 'No'),
(447, 'APPROVED', 17, 'Outside Technical Consultant', 'No'),
(448, 'APPROVED', 17, 'Functional Outside Consultant', 'No'),
(449, 'APPROVED', 17, 'EDP Analyst', 'No'),
(450, 'APPROVED', 17, 'Solution Architect/Enterprise Architect', 'No'),
(451, 'APPROVED', 17, 'Subject Matter Expert', 'No'),
(452, 'APPROVED', 17, 'ERP Consultant', 'No'),
(453, 'APPROVED', 17, 'Technical Writer', 'No'),
(454, 'APPROVED', 17, 'Instructional Designer', 'No'),
(455, 'APPROVED', 17, 'Technical Documentor', 'No'),
(456, 'APPROVED', 17, 'QA/QC Exec.', 'No'),
(457, 'APPROVED', 17, 'QA/QC Mgr', 'No'),
(458, 'APPROVED', 17, 'Project Mgr-IT/Software', 'No'),
(459, 'APPROVED', 17, 'Program Mgr', 'No'),
(460, 'APPROVED', 17, 'Head/VP/GM-Quality', 'No'),
(461, 'APPROVED', 17, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(462, 'APPROVED', 17, 'CIO', 'No'),
(463, 'APPROVED', 17, 'Practice Head/Practice Manager', 'No'),
(464, 'APPROVED', 17, 'Trainer/Faculty', 'No'),
(465, 'APPROVED', 17, 'Trainee', 'No'),
(466, 'APPROVED', 17, 'Fresher', 'No'),
(467, 'APPROVED', 17, 'Outside Consultant', 'No'),
(468, 'APPROVED', 17, 'IT/Technical Content Developer', 'No'),
(469, 'APPROVED', 18, 'Software Developer', 'No'),
(470, 'APPROVED', 18, 'Team Lead/Tech Lead', 'No'),
(471, 'APPROVED', 18, 'System Analyst', 'No'),
(472, 'APPROVED', 18, 'Tech Architect', 'No'),
(473, 'APPROVED', 18, 'Database Architect/Designer', 'No'),
(474, 'APPROVED', 18, 'Project Lead', 'No'),
(475, 'APPROVED', 18, 'Testing Engnr', 'No'),
(476, 'APPROVED', 18, 'Product Mgr', 'No'),
(477, 'APPROVED', 18, 'Graphic/Web Designer', 'No'),
(478, 'APPROVED', 18, 'Release Mgr', 'No'),
(479, 'APPROVED', 18, 'DBA', 'No'),
(480, 'APPROVED', 18, 'Network Admin', 'No'),
(481, 'APPROVED', 18, 'System Admin', 'No'),
(482, 'APPROVED', 18, 'System Security', 'No'),
(483, 'APPROVED', 18, 'Tech Support Engnr', 'No'),
(484, 'APPROVED', 18, 'Maintenance Engnr', 'No'),
(485, 'APPROVED', 18, 'Webmaster', 'No'),
(486, 'APPROVED', 18, 'IT/Networking-Mgr', 'No'),
(487, 'APPROVED', 18, 'Information Systems(MIS)-Mgr', 'No'),
(488, 'APPROVED', 18, 'System Integration Technician', 'No'),
(489, 'APPROVED', 18, 'Business Analyst', 'No'),
(490, 'APPROVED', 18, 'Datawarehousing Technician', 'No'),
(491, 'APPROVED', 18, 'Outside Technical Consultant', 'No'),
(492, 'APPROVED', 18, 'Functional Outside Consultant', 'No'),
(493, 'APPROVED', 18, 'EDP Analyst', 'No'),
(494, 'APPROVED', 18, 'Solution Architect/Enterprise Architect', 'No'),
(495, 'APPROVED', 18, 'Subject Matter Expert', 'No'),
(496, 'APPROVED', 18, 'ERP Consultant', 'No'),
(497, 'APPROVED', 18, 'Technical Writer', 'No'),
(498, 'APPROVED', 18, 'Instructional Designer', 'No'),
(499, 'APPROVED', 18, 'Technical Documentor', 'No'),
(500, 'APPROVED', 18, 'QA/QC Exec.', 'No'),
(501, 'APPROVED', 18, 'QA/QC Mgr', 'No'),
(502, 'APPROVED', 18, 'Project Mgr-IT/Software', 'No'),
(503, 'APPROVED', 18, 'Program Mgr', 'No'),
(504, 'APPROVED', 18, 'Head/VP/GM-Quality', 'No'),
(505, 'APPROVED', 18, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(506, 'APPROVED', 18, 'CIO', 'No'),
(507, 'APPROVED', 18, 'Practice Head/Practice Manager', 'No'),
(508, 'APPROVED', 18, 'Trainer/Faculty', 'No'),
(509, 'APPROVED', 18, 'Trainee', 'No'),
(510, 'APPROVED', 18, 'Fresher', 'No'),
(511, 'APPROVED', 18, 'Outside Consultant', 'No'),
(512, 'APPROVED', 18, 'IT/Technical Content Developer', 'No'),
(513, 'APPROVED', 19, 'Software Developer', 'No'),
(514, 'APPROVED', 19, 'Team Lead/Tech Lead', 'No'),
(515, 'APPROVED', 19, 'System Analyst', 'No'),
(516, 'APPROVED', 19, 'Tech Architect', 'No'),
(517, 'APPROVED', 19, 'Database Architect/Designer', 'No'),
(518, 'APPROVED', 19, 'Project Lead', 'No'),
(519, 'APPROVED', 19, 'Testing Engnr', 'No'),
(520, 'APPROVED', 19, 'Product Mgr', 'No'),
(521, 'APPROVED', 19, 'Graphic/Web Designer', 'No'),
(522, 'APPROVED', 19, 'Release Mgr', 'No'),
(523, 'APPROVED', 19, 'DBA', 'No'),
(524, 'APPROVED', 19, 'Network Admin', 'No'),
(525, 'APPROVED', 19, 'System Admin', 'No'),
(526, 'APPROVED', 19, 'System Security', 'No'),
(527, 'APPROVED', 19, 'Tech Support Engnr', 'No'),
(528, 'APPROVED', 19, 'Maintenance Engnr', 'No'),
(529, 'APPROVED', 19, 'Webmaster', 'No'),
(530, 'APPROVED', 19, 'IT/Networking-Mgr', 'No'),
(531, 'APPROVED', 19, 'Information Systems(MIS)-Mgr', 'No'),
(532, 'APPROVED', 19, 'System Integration Technician', 'No'),
(533, 'APPROVED', 19, 'Business Analyst', 'No'),
(534, 'APPROVED', 19, 'Datawarehousing Technician', 'No'),
(535, 'APPROVED', 19, 'Outside Technical Consultant', 'No'),
(536, 'APPROVED', 19, 'Functional Outside Consultant', 'No'),
(537, 'APPROVED', 19, 'EDP Analyst', 'No'),
(538, 'APPROVED', 19, 'Solution Architect/Enterprise Architect', 'No'),
(539, 'APPROVED', 19, 'Subject Matter Expert', 'No'),
(540, 'APPROVED', 19, 'ERP Consultant', 'No'),
(541, 'APPROVED', 19, 'Technical Writer', 'No'),
(542, 'APPROVED', 19, 'Instructional Designer', 'No'),
(543, 'APPROVED', 19, 'Technical Documentor', 'No'),
(544, 'APPROVED', 19, 'QA/QC Exec.', 'No'),
(545, 'APPROVED', 19, 'QA/QC Mgr', 'No'),
(546, 'APPROVED', 19, 'Project Mgr-IT/Software', 'No'),
(547, 'APPROVED', 19, 'Program Mgr', 'No'),
(548, 'APPROVED', 19, 'Head/VP/GM-Quality', 'No'),
(549, 'APPROVED', 19, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(550, 'APPROVED', 19, 'CIO', 'No'),
(551, 'APPROVED', 19, 'Practice Head/Practice Manager', 'No'),
(552, 'APPROVED', 19, 'Trainer/Faculty', 'No'),
(553, 'APPROVED', 19, 'Trainee', 'No'),
(554, 'APPROVED', 19, 'Fresher', 'No'),
(555, 'APPROVED', 19, 'Outside Consultant', 'No'),
(556, 'APPROVED', 19, 'IT/Technical Content Developer', 'No'),
(557, 'APPROVED', 20, 'Software Developer', 'No'),
(558, 'APPROVED', 20, 'Team Lead/Tech Lead', 'No'),
(559, 'APPROVED', 20, 'System Analyst', 'No'),
(560, 'APPROVED', 20, 'Tech Architect', 'No'),
(561, 'APPROVED', 20, 'Database Architect/Designer', 'No'),
(562, 'APPROVED', 20, 'Project Lead', 'No'),
(563, 'APPROVED', 20, 'Testing Engnr', 'No'),
(564, 'APPROVED', 20, 'Product Mgr', 'No'),
(565, 'APPROVED', 20, 'Graphic/Web Designer', 'No'),
(566, 'APPROVED', 20, 'Release Mgr', 'No'),
(567, 'APPROVED', 20, 'DBA', 'No'),
(568, 'APPROVED', 20, 'Network Admin', 'No'),
(569, 'APPROVED', 20, 'System Admin', 'No'),
(570, 'APPROVED', 20, 'System Security', 'No'),
(571, 'APPROVED', 20, 'Tech Support Engnr', 'No'),
(572, 'APPROVED', 20, 'Maintenance Engnr', 'No'),
(573, 'APPROVED', 20, 'Webmaster', 'No'),
(574, 'APPROVED', 20, 'IT/Networking-Mgr', 'No'),
(575, 'APPROVED', 20, 'Information Systems(MIS)-Mgr', 'No'),
(576, 'APPROVED', 20, 'System Integration Technician', 'No'),
(577, 'APPROVED', 20, 'Business Analyst', 'No'),
(578, 'APPROVED', 20, 'Datawarehousing Technician', 'No'),
(579, 'APPROVED', 20, 'Outside Technical Consultant', 'No'),
(580, 'APPROVED', 20, 'Functional Outside Consultant', 'No'),
(581, 'APPROVED', 20, 'EDP Analyst', 'No'),
(582, 'APPROVED', 20, 'Solution Architect/Enterprise Architect', 'No'),
(583, 'APPROVED', 20, 'Subject Matter Expert', 'No'),
(584, 'APPROVED', 20, 'ERP Consultant', 'No'),
(585, 'APPROVED', 20, 'Technical Writer', 'No'),
(586, 'APPROVED', 20, 'Instructional Designer', 'No'),
(587, 'APPROVED', 20, 'Technical Documentor', 'No'),
(588, 'APPROVED', 20, 'QA/QC Exec.', 'No'),
(589, 'APPROVED', 20, 'QA/QC Mgr', 'No'),
(590, 'APPROVED', 20, 'Project Mgr-IT/Software', 'No'),
(591, 'APPROVED', 20, 'Program Mgr', 'No'),
(592, 'APPROVED', 20, 'Head/VP/GM-Quality', 'No'),
(593, 'APPROVED', 20, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(594, 'APPROVED', 20, 'CIO', 'No'),
(595, 'APPROVED', 20, 'Practice Head/Practice Manager', 'No'),
(596, 'APPROVED', 20, 'Trainer/Faculty', 'No'),
(597, 'APPROVED', 20, 'Trainee', 'No'),
(598, 'APPROVED', 20, 'Fresher', 'No'),
(599, 'APPROVED', 20, 'Outside Consultant', 'No'),
(600, 'APPROVED', 20, 'IT/Technical Content Developer', 'No'),
(601, 'APPROVED', 21, 'Software Developer', 'No'),
(602, 'APPROVED', 21, 'Team Lead/Tech Lead', 'No'),
(603, 'APPROVED', 21, 'System Analyst', 'No'),
(604, 'APPROVED', 21, 'Tech Architect', 'No'),
(605, 'APPROVED', 21, 'Database Architect/Designer', 'No'),
(606, 'APPROVED', 21, 'Project Lead', 'No'),
(607, 'APPROVED', 21, 'Testing Engnr', 'No'),
(608, 'APPROVED', 21, 'Product Mgr', 'No'),
(609, 'APPROVED', 21, 'Graphic/Web Designer', 'No'),
(610, 'APPROVED', 21, 'Release Mgr', 'No'),
(611, 'APPROVED', 21, 'DBA', 'No'),
(612, 'APPROVED', 21, 'Network Admin', 'No'),
(613, 'APPROVED', 21, 'System Admin', 'No'),
(614, 'APPROVED', 21, 'System Security', 'No'),
(615, 'APPROVED', 21, 'Tech Support Engnr', 'No'),
(616, 'APPROVED', 21, 'Maintenance Engnr', 'No'),
(617, 'APPROVED', 21, 'Webmaster', 'No'),
(618, 'APPROVED', 21, 'IT/Networking-Mgr', 'No'),
(619, 'APPROVED', 21, 'Information Systems(MIS)-Mgr', 'No'),
(620, 'APPROVED', 21, 'System Integration Technician', 'No'),
(621, 'APPROVED', 21, 'Business Analyst', 'No'),
(622, 'APPROVED', 21, 'Datawarehousing Technician', 'No'),
(623, 'APPROVED', 21, 'Outside Technical Consultant', 'No'),
(624, 'APPROVED', 21, 'Functional Outside Consultant', 'No'),
(625, 'APPROVED', 21, 'EDP Analyst', 'No'),
(626, 'APPROVED', 21, 'Solution Architect/Enterprise Architect', 'No'),
(627, 'APPROVED', 21, 'Subject Matter Expert', 'No'),
(628, 'APPROVED', 21, 'ERP Consultant', 'No'),
(629, 'APPROVED', 21, 'Technical Writer', 'No'),
(630, 'APPROVED', 21, 'Instructional Designer', 'No'),
(631, 'APPROVED', 21, 'Technical Documentor', 'No'),
(632, 'APPROVED', 21, 'QA/QC Exec.', 'No'),
(633, 'APPROVED', 21, 'QA/QC Mgr', 'No'),
(634, 'APPROVED', 21, 'Project Mgr-IT/Software', 'No'),
(635, 'APPROVED', 21, 'Program Mgr', 'No'),
(636, 'APPROVED', 21, 'Head/VP/GM-Quality', 'No'),
(637, 'APPROVED', 21, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(638, 'APPROVED', 21, 'CIO', 'No'),
(639, 'APPROVED', 21, 'Practice Head/Practice Manager', 'No'),
(640, 'APPROVED', 21, 'Trainer/Faculty', 'No'),
(641, 'APPROVED', 21, 'Trainee', 'No'),
(642, 'APPROVED', 21, 'Fresher', 'No'),
(643, 'APPROVED', 21, 'Outside Consultant', 'No'),
(644, 'APPROVED', 21, 'IT/Technical Content Developer', 'No'),
(645, 'APPROVED', 22, 'Software Developer', 'No'),
(646, 'APPROVED', 22, 'Team Lead/Tech Lead', 'No'),
(647, 'APPROVED', 22, 'System Analyst', 'No'),
(648, 'APPROVED', 22, 'Tech Architect', 'No'),
(649, 'APPROVED', 22, 'Database Architect/Designer', 'No'),
(650, 'APPROVED', 22, 'Project Lead', 'No'),
(651, 'APPROVED', 22, 'Testing Engnr', 'No'),
(652, 'APPROVED', 22, 'Product Mgr', 'No'),
(653, 'APPROVED', 22, 'Graphic/Web Designer', 'No'),
(654, 'APPROVED', 22, 'Release Mgr', 'No'),
(655, 'APPROVED', 22, 'DBA', 'No'),
(656, 'APPROVED', 22, 'Network Admin', 'No'),
(657, 'APPROVED', 22, 'System Admin', 'No'),
(658, 'APPROVED', 22, 'System Security', 'No'),
(659, 'APPROVED', 22, 'Tech Support Engnr', 'No'),
(660, 'APPROVED', 22, 'Maintenance Engnr', 'No'),
(661, 'APPROVED', 22, 'Webmaster', 'No'),
(662, 'APPROVED', 22, 'IT/Networking-Mgr', 'No'),
(663, 'APPROVED', 22, 'Information Systems(MIS)-Mgr', 'No'),
(664, 'APPROVED', 22, 'System Integration Technician', 'No'),
(665, 'APPROVED', 22, 'Business Analyst', 'No'),
(666, 'APPROVED', 22, 'Datawarehousing Technician', 'No'),
(667, 'APPROVED', 22, 'Outside Technical Consultant', 'No'),
(668, 'APPROVED', 22, 'Functional Outside Consultant', 'No'),
(669, 'APPROVED', 22, 'EDP Analyst', 'No'),
(670, 'APPROVED', 22, 'Solution Architect/Enterprise Architect', 'No'),
(671, 'APPROVED', 22, 'Subject Matter Expert', 'No'),
(672, 'APPROVED', 22, 'ERP Consultant', 'No'),
(673, 'APPROVED', 22, 'Technical Writer', 'No'),
(674, 'APPROVED', 22, 'Instructional Designer', 'No'),
(675, 'APPROVED', 22, 'Technical Documentor', 'No'),
(676, 'APPROVED', 22, 'QA/QC Exec.', 'No'),
(677, 'APPROVED', 22, 'QA/QC Mgr', 'No'),
(678, 'APPROVED', 22, 'Project Mgr-IT/Software', 'No'),
(679, 'APPROVED', 22, 'Program Mgr', 'No'),
(680, 'APPROVED', 22, 'Head/VP/GM-Quality', 'No'),
(681, 'APPROVED', 22, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(682, 'APPROVED', 22, 'CIO', 'No'),
(683, 'APPROVED', 22, 'Practice Head/Practice Manager', 'No'),
(684, 'APPROVED', 22, 'Trainer/Faculty', 'No'),
(685, 'APPROVED', 22, 'Trainee', 'No'),
(686, 'APPROVED', 22, 'Fresher', 'No'),
(687, 'APPROVED', 22, 'Outside Consultant', 'No'),
(688, 'APPROVED', 22, 'IT/Technical Content Developer', 'No'),
(689, 'APPROVED', 23, 'Software Developer', 'No'),
(690, 'APPROVED', 23, 'Team Lead/Tech Lead', 'No'),
(691, 'APPROVED', 23, 'System Analyst', 'No'),
(692, 'APPROVED', 23, 'Tech Architect', 'No'),
(693, 'APPROVED', 23, 'Database Architect/Designer', 'No'),
(694, 'APPROVED', 23, 'Project Lead', 'No'),
(695, 'APPROVED', 23, 'Testing Engnr', 'No'),
(696, 'APPROVED', 23, 'Product Mgr', 'No'),
(697, 'APPROVED', 23, 'Graphic/Web Designer', 'No'),
(698, 'APPROVED', 23, 'Release Mgr', 'No'),
(699, 'APPROVED', 23, 'DBA', 'No'),
(700, 'APPROVED', 23, 'Network Admin', 'No'),
(701, 'APPROVED', 23, 'System Admin', 'No'),
(702, 'APPROVED', 23, 'System Security', 'No'),
(703, 'APPROVED', 23, 'Tech Support Engnr', 'No'),
(704, 'APPROVED', 23, 'Maintenance Engnr', 'No'),
(705, 'APPROVED', 23, 'Webmaster', 'No'),
(706, 'APPROVED', 23, 'IT/Networking-Mgr', 'No'),
(707, 'APPROVED', 23, 'Information Systems(MIS)-Mgr', 'No'),
(708, 'APPROVED', 23, 'System Integration Technician', 'No'),
(709, 'APPROVED', 23, 'Business Analyst', 'No'),
(710, 'APPROVED', 23, 'Datawarehousing Technician', 'No'),
(711, 'APPROVED', 23, 'Outside Technical Consultant', 'No'),
(712, 'APPROVED', 23, 'Functional Outside Consultant', 'No'),
(713, 'APPROVED', 23, 'EDP Analyst', 'No'),
(714, 'APPROVED', 23, 'Solution Architect/Enterprise Architect', 'No'),
(715, 'APPROVED', 23, 'Subject Matter Expert', 'No'),
(716, 'APPROVED', 23, 'ERP Consultant', 'No'),
(717, 'APPROVED', 23, 'Technical Writer', 'No'),
(718, 'APPROVED', 23, 'Instructional Designer', 'No'),
(719, 'APPROVED', 23, 'Technical Documentor', 'No'),
(720, 'APPROVED', 23, 'QA/QC Exec.', 'No'),
(721, 'APPROVED', 23, 'QA/QC Mgr', 'No'),
(722, 'APPROVED', 23, 'Project Mgr-IT/Software', 'No'),
(723, 'APPROVED', 23, 'Program Mgr', 'No'),
(724, 'APPROVED', 23, 'Head/VP/GM-Quality', 'No'),
(725, 'APPROVED', 23, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(726, 'APPROVED', 23, 'CIO', 'No'),
(727, 'APPROVED', 23, 'Practice Head/Practice Manager', 'No'),
(728, 'APPROVED', 23, 'Trainer/Faculty', 'No'),
(729, 'APPROVED', 23, 'Trainee', 'No'),
(730, 'APPROVED', 23, 'Fresher', 'No'),
(731, 'APPROVED', 23, 'Outside Consultant', 'No'),
(732, 'APPROVED', 23, 'IT/Technical Content Developer', 'No'),
(733, 'APPROVED', 24, 'Software Developer', 'No'),
(734, 'APPROVED', 24, 'Team Lead/Tech Lead', 'No'),
(735, 'APPROVED', 24, 'System Analyst', 'No'),
(736, 'APPROVED', 24, 'Tech Architect', 'No'),
(737, 'APPROVED', 24, 'Database Architect/Designer', 'No'),
(738, 'APPROVED', 24, 'Project Lead', 'No'),
(739, 'APPROVED', 24, 'Testing Engnr', 'No'),
(740, 'APPROVED', 24, 'Product Mgr', 'No'),
(741, 'APPROVED', 24, 'Graphic/Web Designer', 'No'),
(742, 'APPROVED', 24, 'Release Mgr', 'No'),
(743, 'APPROVED', 24, 'DBA', 'No'),
(744, 'APPROVED', 24, 'Network Admin', 'No'),
(745, 'APPROVED', 24, 'System Admin', 'No'),
(746, 'APPROVED', 24, 'System Security', 'No'),
(747, 'APPROVED', 24, 'Tech Support Engnr', 'No'),
(748, 'APPROVED', 24, 'Maintenance Engnr', 'No'),
(749, 'APPROVED', 24, 'Webmaster', 'No'),
(750, 'APPROVED', 24, 'IT/Networking-Mgr', 'No'),
(751, 'APPROVED', 24, 'Information Systems(MIS)-Mgr', 'No'),
(752, 'APPROVED', 24, 'System Integration Technician', 'No'),
(753, 'APPROVED', 24, 'Business Analyst', 'No'),
(754, 'APPROVED', 24, 'Datawarehousing Technician', 'No'),
(755, 'APPROVED', 24, 'Outside Technical Consultant', 'No'),
(756, 'APPROVED', 24, 'Functional Outside Consultant', 'No'),
(757, 'APPROVED', 24, 'EDP Analyst', 'No'),
(758, 'APPROVED', 24, 'Solution Architect/Enterprise Architect', 'No'),
(759, 'APPROVED', 24, 'Subject Matter Expert', 'No'),
(760, 'APPROVED', 24, 'ERP Consultant', 'No'),
(761, 'APPROVED', 24, 'Technical Writer', 'No'),
(762, 'APPROVED', 24, 'Instructional Designer', 'No'),
(763, 'APPROVED', 24, 'Technical Documentor', 'No'),
(764, 'APPROVED', 24, 'QA/QC Exec.', 'No'),
(765, 'APPROVED', 24, 'QA/QC Mgr', 'No'),
(766, 'APPROVED', 24, 'Project Mgr-IT/Software', 'No'),
(767, 'APPROVED', 24, 'Program Mgr', 'No'),
(768, 'APPROVED', 24, 'Head/VP/GM-Quality', 'No'),
(769, 'APPROVED', 24, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(770, 'APPROVED', 24, 'CIO', 'No'),
(771, 'APPROVED', 24, 'Practice Head/Practice Manager', 'No'),
(772, 'APPROVED', 24, 'Trainer/Faculty', 'No'),
(773, 'APPROVED', 24, 'Trainee', 'No'),
(774, 'APPROVED', 24, 'Fresher', 'No'),
(775, 'APPROVED', 24, 'Outside Consultant', 'No'),
(776, 'APPROVED', 24, 'IT/Technical Content Developer', 'No'),
(777, 'APPROVED', 25, 'Software Developer', 'No'),
(778, 'APPROVED', 25, 'Team Lead/Tech Lead', 'No'),
(779, 'APPROVED', 25, 'System Analyst', 'No'),
(780, 'APPROVED', 25, 'Tech Architect', 'No'),
(781, 'APPROVED', 25, 'Database Architect/Designer', 'No'),
(782, 'APPROVED', 25, 'Project Lead', 'No'),
(783, 'APPROVED', 25, 'Testing Engnr', 'No'),
(784, 'APPROVED', 25, 'Product Mgr', 'No'),
(785, 'APPROVED', 25, 'Graphic/Web Designer', 'No'),
(786, 'APPROVED', 25, 'Release Mgr', 'No'),
(787, 'APPROVED', 25, 'DBA', 'No'),
(788, 'APPROVED', 25, 'Network Admin', 'No'),
(789, 'APPROVED', 25, 'System Admin', 'No'),
(790, 'APPROVED', 25, 'System Security', 'No'),
(791, 'APPROVED', 25, 'Tech Support Engnr', 'No'),
(792, 'APPROVED', 25, 'Maintenance Engnr', 'No'),
(793, 'APPROVED', 25, 'Webmaster', 'No'),
(794, 'APPROVED', 25, 'IT/Networking-Mgr', 'No'),
(795, 'APPROVED', 25, 'Information Systems(MIS)-Mgr', 'No'),
(796, 'APPROVED', 25, 'System Integration Technician', 'No'),
(797, 'APPROVED', 25, 'Business Analyst', 'No'),
(798, 'APPROVED', 25, 'Datawarehousing Technician', 'No'),
(799, 'APPROVED', 25, 'Outside Technical Consultant', 'No'),
(800, 'APPROVED', 25, 'Functional Outside Consultant', 'No'),
(801, 'APPROVED', 25, 'EDP Analyst', 'No'),
(802, 'APPROVED', 25, 'Solution Architect/Enterprise Architect', 'No'),
(803, 'APPROVED', 25, 'Subject Matter Expert', 'No'),
(804, 'APPROVED', 25, 'ERP Consultant', 'No'),
(805, 'APPROVED', 25, 'Technical Writer', 'No'),
(806, 'APPROVED', 25, 'Instructional Designer', 'No'),
(807, 'APPROVED', 25, 'Technical Documentor', 'No'),
(808, 'APPROVED', 25, 'QA/QC Exec.', 'No'),
(809, 'APPROVED', 25, 'QA/QC Mgr', 'No'),
(810, 'APPROVED', 25, 'Project Mgr-IT/Software', 'No'),
(811, 'APPROVED', 25, 'Program Mgr', 'No'),
(812, 'APPROVED', 25, 'Head/VP/GM-Quality', 'No'),
(813, 'APPROVED', 25, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(814, 'APPROVED', 25, 'CIO', 'No'),
(815, 'APPROVED', 25, 'Practice Head/Practice Manager', 'No'),
(816, 'APPROVED', 25, 'Trainer/Faculty', 'No'),
(817, 'APPROVED', 25, 'Trainee', 'No'),
(818, 'APPROVED', 25, 'Fresher', 'No'),
(819, 'APPROVED', 25, 'Outside Consultant', 'No'),
(820, 'APPROVED', 25, 'IT/Technical Content Developer', 'No'),
(821, 'APPROVED', 26, 'Software Developer', 'No'),
(822, 'APPROVED', 26, 'Team Lead/Tech Lead', 'No'),
(823, 'APPROVED', 26, 'System Analyst', 'No'),
(824, 'APPROVED', 26, 'Tech Architect', 'No'),
(825, 'APPROVED', 26, 'Database Architect/Designer', 'No'),
(826, 'APPROVED', 26, 'Project Lead', 'No'),
(827, 'APPROVED', 26, 'Testing Engnr', 'No'),
(828, 'APPROVED', 26, 'Product Mgr', 'No'),
(829, 'APPROVED', 26, 'Graphic/Web Designer', 'No'),
(830, 'APPROVED', 26, 'Release Mgr', 'No'),
(831, 'APPROVED', 26, 'DBA', 'No'),
(832, 'APPROVED', 26, 'Network Admin', 'No'),
(833, 'APPROVED', 26, 'System Admin', 'No'),
(834, 'APPROVED', 26, 'System Security', 'No'),
(835, 'APPROVED', 26, 'Tech Support Engnr', 'No'),
(836, 'APPROVED', 26, 'Maintenance Engnr', 'No'),
(837, 'APPROVED', 26, 'Webmaster', 'No'),
(838, 'APPROVED', 26, 'IT/Networking-Mgr', 'No'),
(839, 'APPROVED', 26, 'Information Systems(MIS)-Mgr', 'No'),
(840, 'APPROVED', 26, 'System Integration Technician', 'No'),
(841, 'APPROVED', 26, 'Business Analyst', 'No'),
(842, 'APPROVED', 26, 'Datawarehousing Technician', 'No'),
(843, 'APPROVED', 26, 'Outside Technical Consultant', 'No'),
(844, 'APPROVED', 26, 'Functional Outside Consultant', 'No'),
(845, 'APPROVED', 26, 'EDP Analyst', 'No'),
(846, 'APPROVED', 26, 'Solution Architect/Enterprise Architect', 'No'),
(847, 'APPROVED', 26, 'Subject Matter Expert', 'No'),
(848, 'APPROVED', 26, 'ERP Consultant', 'No'),
(849, 'APPROVED', 26, 'Technical Writer', 'No'),
(850, 'APPROVED', 26, 'Instructional Designer', 'No'),
(851, 'APPROVED', 26, 'Technical Documentor', 'No'),
(852, 'APPROVED', 26, 'QA/QC Exec.', 'No'),
(853, 'APPROVED', 26, 'QA/QC Mgr', 'No'),
(854, 'APPROVED', 26, 'Project Mgr-IT/Software', 'No'),
(855, 'APPROVED', 26, 'Program Mgr', 'No'),
(856, 'APPROVED', 26, 'Head/VP/GM-Quality', 'No'),
(857, 'APPROVED', 26, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(858, 'APPROVED', 26, 'CIO', 'No'),
(859, 'APPROVED', 26, 'Practice Head/Practice Manager', 'No'),
(860, 'APPROVED', 26, 'Trainer/Faculty', 'No'),
(861, 'APPROVED', 26, 'Trainee', 'No'),
(862, 'APPROVED', 26, 'Fresher', 'No'),
(863, 'APPROVED', 26, 'Outside Consultant', 'No'),
(864, 'APPROVED', 26, 'IT/Technical Content Developer', 'No'),
(865, 'APPROVED', 27, 'Software Developer', 'No'),
(866, 'APPROVED', 27, 'Team Lead/Tech Lead', 'No'),
(867, 'APPROVED', 27, 'System Analyst', 'No'),
(868, 'APPROVED', 27, 'Tech Architect', 'No'),
(869, 'APPROVED', 27, 'Database Architect/Designer', 'No'),
(870, 'APPROVED', 27, 'Project Lead', 'No'),
(871, 'APPROVED', 27, 'Testing Engnr', 'No'),
(872, 'APPROVED', 27, 'Product Mgr', 'No'),
(873, 'APPROVED', 27, 'Graphic/Web Designer', 'No'),
(874, 'APPROVED', 27, 'Release Mgr', 'No'),
(875, 'APPROVED', 27, 'DBA', 'No'),
(876, 'APPROVED', 27, 'Network Admin', 'No'),
(877, 'APPROVED', 27, 'System Admin', 'No'),
(878, 'APPROVED', 27, 'System Security', 'No'),
(879, 'APPROVED', 27, 'Tech Support Engnr', 'No'),
(880, 'APPROVED', 27, 'Maintenance Engnr', 'No'),
(881, 'APPROVED', 27, 'Webmaster', 'No'),
(882, 'APPROVED', 27, 'IT/Networking-Mgr', 'No'),
(883, 'APPROVED', 27, 'Information Systems(MIS)-Mgr', 'No'),
(884, 'APPROVED', 27, 'System Integration Technician', 'No'),
(885, 'APPROVED', 27, 'Business Analyst', 'No'),
(886, 'APPROVED', 27, 'Datawarehousing Technician', 'No'),
(887, 'APPROVED', 27, 'Outside Technical Consultant', 'No'),
(888, 'APPROVED', 27, 'Functional Outside Consultant', 'No'),
(889, 'APPROVED', 27, 'EDP Analyst', 'No'),
(890, 'APPROVED', 27, 'Solution Architect/Enterprise Architect', 'No'),
(891, 'APPROVED', 27, 'Subject Matter Expert', 'No'),
(892, 'APPROVED', 27, 'ERP Consultant', 'No'),
(893, 'APPROVED', 27, 'Technical Writer', 'No'),
(894, 'APPROVED', 27, 'Instructional Designer', 'No'),
(895, 'APPROVED', 27, 'Technical Documentor', 'No'),
(896, 'APPROVED', 27, 'QA/QC Exec.', 'No'),
(897, 'APPROVED', 27, 'QA/QC Mgr', 'No'),
(898, 'APPROVED', 27, 'Project Mgr-IT/Software', 'No'),
(899, 'APPROVED', 27, 'Program Mgr', 'No'),
(900, 'APPROVED', 27, 'Head/VP/GM-Quality', 'No'),
(901, 'APPROVED', 27, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(902, 'APPROVED', 27, 'CIO', 'No'),
(903, 'APPROVED', 27, 'Practice Head/Practice Manager', 'No'),
(904, 'APPROVED', 27, 'Trainer/Faculty', 'No'),
(905, 'APPROVED', 27, 'Trainee', 'No'),
(906, 'APPROVED', 27, 'Fresher', 'No'),
(907, 'APPROVED', 27, 'Outside Consultant', 'No'),
(908, 'APPROVED', 27, 'IT/Technical Content Developer', 'No'),
(909, 'APPROVED', 28, 'Software Developer', 'No'),
(910, 'APPROVED', 28, 'Team Lead/Tech Lead', 'No'),
(911, 'APPROVED', 28, 'System Analyst', 'No'),
(912, 'APPROVED', 28, 'Tech Architect', 'No'),
(913, 'APPROVED', 28, 'Database Architect/Designer', 'No'),
(914, 'APPROVED', 28, 'Project Lead', 'No'),
(915, 'APPROVED', 28, 'Testing Engnr', 'No'),
(916, 'APPROVED', 28, 'Product Mgr', 'No'),
(917, 'APPROVED', 28, 'Graphic/Web Designer', 'No'),
(918, 'APPROVED', 28, 'Release Mgr', 'No'),
(919, 'APPROVED', 28, 'DBA', 'No'),
(920, 'APPROVED', 28, 'Network Admin', 'No'),
(921, 'APPROVED', 28, 'System Admin', 'No'),
(922, 'APPROVED', 28, 'System Security', 'No'),
(923, 'APPROVED', 28, 'Tech Support Engnr', 'No'),
(924, 'APPROVED', 28, 'Maintenance Engnr', 'No'),
(925, 'APPROVED', 28, 'Webmaster', 'No'),
(926, 'APPROVED', 28, 'IT/Networking-Mgr', 'No'),
(927, 'APPROVED', 28, 'Information Systems(MIS)-Mgr', 'No'),
(928, 'APPROVED', 28, 'System Integration Technician', 'No'),
(929, 'APPROVED', 28, 'Business Analyst', 'No'),
(930, 'APPROVED', 28, 'Datawarehousing Technician', 'No'),
(931, 'APPROVED', 28, 'Outside Technical Consultant', 'No'),
(932, 'APPROVED', 28, 'Functional Outside Consultant', 'No'),
(933, 'APPROVED', 28, 'EDP Analyst', 'No'),
(934, 'APPROVED', 28, 'Solution Architect/Enterprise Architect', 'No'),
(935, 'APPROVED', 28, 'Subject Matter Expert', 'No'),
(936, 'APPROVED', 28, 'ERP Consultant', 'No'),
(937, 'APPROVED', 28, 'Technical Writer', 'No'),
(938, 'APPROVED', 28, 'Instructional Designer', 'No'),
(939, 'APPROVED', 28, 'Technical Documentor', 'No'),
(940, 'APPROVED', 28, 'QA/QC Exec.', 'No'),
(941, 'APPROVED', 28, 'QA/QC Mgr', 'No'),
(942, 'APPROVED', 28, 'Project Mgr-IT/Software', 'No'),
(943, 'APPROVED', 28, 'Program Mgr', 'No'),
(944, 'APPROVED', 28, 'Head/VP/GM-Quality', 'No'),
(945, 'APPROVED', 28, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(946, 'APPROVED', 28, 'CIO', 'No'),
(947, 'APPROVED', 28, 'Practice Head/Practice Manager', 'No'),
(948, 'APPROVED', 28, 'Trainer/Faculty', 'No'),
(949, 'APPROVED', 28, 'Trainee', 'No'),
(950, 'APPROVED', 28, 'Fresher', 'No'),
(951, 'APPROVED', 28, 'Outside Consultant', 'No'),
(952, 'APPROVED', 28, 'IT/Technical Content Developer', 'No'),
(953, 'APPROVED', 29, 'Software Developer', 'No'),
(954, 'APPROVED', 29, 'Team Lead/Tech Lead', 'No'),
(955, 'APPROVED', 29, 'System Analyst', 'No'),
(956, 'APPROVED', 29, 'Tech Architect', 'No'),
(957, 'APPROVED', 29, 'Database Architect/Designer', 'No'),
(958, 'APPROVED', 29, 'Project Lead', 'No'),
(959, 'APPROVED', 29, 'Testing Engnr', 'No'),
(960, 'APPROVED', 29, 'Product Mgr', 'No'),
(961, 'APPROVED', 29, 'Graphic/Web Designer', 'No'),
(962, 'APPROVED', 29, 'Release Mgr', 'No'),
(963, 'APPROVED', 29, 'DBA', 'No'),
(964, 'APPROVED', 29, 'Network Admin', 'No'),
(965, 'APPROVED', 29, 'System Admin', 'No'),
(966, 'APPROVED', 29, 'System Security', 'No'),
(967, 'APPROVED', 29, 'Tech Support Engnr', 'No'),
(968, 'APPROVED', 29, 'Maintenance Engnr', 'No'),
(969, 'APPROVED', 29, 'Webmaster', 'No'),
(970, 'APPROVED', 29, 'IT/Networking-Mgr', 'No'),
(971, 'APPROVED', 29, 'Information Systems(MIS)-Mgr', 'No'),
(972, 'APPROVED', 29, 'System Integration Technician', 'No'),
(973, 'APPROVED', 29, 'Business Analyst', 'No'),
(974, 'APPROVED', 29, 'Datawarehousing Technician', 'No'),
(975, 'APPROVED', 29, 'Outside Technical Consultant', 'No'),
(976, 'APPROVED', 29, 'Functional Outside Consultant', 'No'),
(977, 'APPROVED', 29, 'EDP Analyst', 'No'),
(978, 'APPROVED', 29, 'Solution Architect/Enterprise Architect', 'No'),
(979, 'APPROVED', 29, 'Subject Matter Expert', 'No'),
(980, 'APPROVED', 29, 'ERP Consultant', 'No'),
(981, 'APPROVED', 29, 'Technical Writer', 'No'),
(982, 'APPROVED', 29, 'Instructional Designer', 'No'),
(983, 'APPROVED', 29, 'Technical Documentor', 'No'),
(984, 'APPROVED', 29, 'QA/QC Exec.', 'No'),
(985, 'APPROVED', 29, 'QA/QC Mgr', 'No'),
(986, 'APPROVED', 29, 'Project Mgr-IT/Software', 'No'),
(987, 'APPROVED', 29, 'Program Mgr', 'No'),
(988, 'APPROVED', 29, 'Head/VP/GM-Quality', 'No'),
(989, 'APPROVED', 29, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(990, 'APPROVED', 29, 'CIO', 'No'),
(991, 'APPROVED', 29, 'Practice Head/Practice Manager', 'No'),
(992, 'APPROVED', 29, 'Trainer/Faculty', 'No'),
(993, 'APPROVED', 29, 'Trainee', 'No'),
(994, 'APPROVED', 29, 'Fresher', 'No'),
(995, 'APPROVED', 29, 'Outside Consultant', 'No'),
(996, 'APPROVED', 29, 'IT/Technical Content Developer', 'No'),
(997, 'APPROVED', 30, 'Software Developer', 'No'),
(998, 'APPROVED', 30, 'Team Lead/Tech Lead', 'No'),
(999, 'APPROVED', 30, 'System Analyst', 'No'),
(1000, 'APPROVED', 30, 'Tech Architect', 'No'),
(1001, 'APPROVED', 30, 'Database Architect/Designer', 'No'),
(1002, 'APPROVED', 30, 'Project Lead', 'No'),
(1003, 'APPROVED', 30, 'Testing Engnr', 'No'),
(1004, 'APPROVED', 30, 'Product Mgr', 'No'),
(1005, 'APPROVED', 30, 'Graphic/Web Designer', 'No'),
(1006, 'APPROVED', 30, 'Release Mgr', 'No'),
(1007, 'APPROVED', 30, 'DBA', 'No'),
(1008, 'APPROVED', 30, 'Network Admin', 'No'),
(1009, 'APPROVED', 30, 'System Admin', 'No'),
(1010, 'APPROVED', 30, 'System Security', 'No'),
(1011, 'APPROVED', 30, 'Tech Support Engnr', 'No'),
(1012, 'APPROVED', 30, 'Maintenance Engnr', 'No'),
(1013, 'APPROVED', 30, 'Webmaster', 'No'),
(1014, 'APPROVED', 30, 'IT/Networking-Mgr', 'No'),
(1015, 'APPROVED', 30, 'Information Systems(MIS)-Mgr', 'No'),
(1016, 'APPROVED', 30, 'System Integration Technician', 'No'),
(1017, 'APPROVED', 30, 'Business Analyst', 'No'),
(1018, 'APPROVED', 30, 'Datawarehousing Technician', 'No'),
(1019, 'APPROVED', 30, 'Outside Technical Consultant', 'No'),
(1020, 'APPROVED', 30, 'Functional Outside Consultant', 'No'),
(1021, 'APPROVED', 30, 'EDP Analyst', 'No'),
(1022, 'APPROVED', 30, 'Solution Architect/Enterprise Architect', 'No'),
(1023, 'APPROVED', 30, 'Subject Matter Expert', 'No'),
(1024, 'APPROVED', 30, 'ERP Consultant', 'No'),
(1025, 'APPROVED', 30, 'Technical Writer', 'No'),
(1026, 'APPROVED', 30, 'Instructional Designer', 'No');
INSERT INTO `role_master` (`id`, `status`, `functional_area`, `role_name`, `is_deleted`) VALUES
(1027, 'APPROVED', 30, 'Technical Documentor', 'No'),
(1028, 'APPROVED', 30, 'QA/QC Exec.', 'No'),
(1029, 'APPROVED', 30, 'QA/QC Mgr', 'No'),
(1030, 'APPROVED', 30, 'Project Mgr-IT/Software', 'No'),
(1031, 'APPROVED', 30, 'Program Mgr', 'No'),
(1032, 'APPROVED', 30, 'Head/VP/GM-Quality', 'No'),
(1033, 'APPROVED', 30, 'Head/VP/GM-Technology(IT)/CTO', 'No'),
(1034, 'APPROVED', 30, 'CIO', 'No'),
(1035, 'APPROVED', 30, 'Practice Head/Practice Manager', 'No'),
(1036, 'APPROVED', 30, 'Trainer/Faculty', 'No'),
(1037, 'APPROVED', 30, 'Trainee', 'No'),
(1038, 'APPROVED', 30, 'Fresher', 'No'),
(1039, 'APPROVED', 30, 'Outside Consultant', 'No'),
(1040, 'APPROVED', 30, 'IT/Technical Content Developer', 'No'),
(1041, 'APPROVED', 31, 'Customer Support Engnr/Technician', 'No'),
(1042, 'APPROVED', 31, 'Technical Support Mgr', 'No'),
(1043, 'APPROVED', 31, 'Head/VP/GM-Tech. Support', 'No'),
(1044, 'APPROVED', 31, 'RF Engnr', 'No'),
(1045, 'APPROVED', 31, 'RF Installation Engnr', 'No'),
(1046, 'APPROVED', 31, 'RF System Designer', 'No'),
(1047, 'APPROVED', 31, 'GPRS Engnr', 'No'),
(1048, 'APPROVED', 31, 'GSM Engnr', 'No'),
(1049, 'APPROVED', 31, 'Embedded Technologies Engnr', 'No'),
(1050, 'APPROVED', 31, 'Switching/Router Engnr', 'No'),
(1051, 'APPROVED', 31, 'Mech. Engnr -Telecom', 'No'),
(1052, 'APPROVED', 31, 'Civil Engnr -Telecom', 'No'),
(1053, 'APPROVED', 31, 'Electrical Engnr -Telecom', 'No'),
(1054, 'APPROVED', 31, 'Network Planning Engnr', 'No'),
(1055, 'APPROVED', 31, 'Network Planning Manager', 'No'),
(1056, 'APPROVED', 31, 'Security Engnr', 'No'),
(1057, 'APPROVED', 31, 'Maintenance Engnr', 'No'),
(1058, 'APPROVED', 31, 'Hardware Design Engnr', 'No'),
(1059, 'APPROVED', 31, 'Tech Lead -Hardware Design', 'No'),
(1060, 'APPROVED', 31, 'Hardware Installation Technician', 'No'),
(1061, 'APPROVED', 31, 'QA/QC Exec.', 'No'),
(1062, 'APPROVED', 31, 'QA/QC Mgr', 'No'),
(1063, 'APPROVED', 31, 'Network Admin', 'No'),
(1064, 'APPROVED', 31, 'System Admin', 'No'),
(1065, 'APPROVED', 31, 'Project Mgr-Telecom', 'No'),
(1066, 'APPROVED', 31, 'Head/VP/GM-Operations', 'No'),
(1067, 'APPROVED', 31, 'Head/VP/GM-Quality', 'No'),
(1068, 'APPROVED', 31, 'CEO/MD/Director', 'No'),
(1069, 'APPROVED', 31, 'SBU Head/Profit Centre Head', 'No'),
(1070, 'APPROVED', 31, 'CTO/Head/VP-Technology (Telecom/ISP)', 'No'),
(1071, 'APPROVED', 31, 'CIO', 'No'),
(1072, 'APPROVED', 31, 'Outside Consultant', 'No'),
(1073, 'APPROVED', 31, 'Other', 'No'),
(1074, 'APPROVED', 31, 'Trainee', 'No'),
(1075, 'APPROVED', 31, 'Fresher', 'No'),
(1076, 'APPROVED', 32, 'Associate/Sr. Associate -(NonTechnical)', 'No'),
(1077, 'APPROVED', 32, 'Associate/Sr. Associate -(Technical)', 'No'),
(1078, 'APPROVED', 32, 'Team Leader -(NonTechnical)', 'No'),
(1079, 'APPROVED', 32, 'Team Leader -(Technical)', 'No'),
(1080, 'APPROVED', 32, 'Asst. Mgr/Mgr -(NonTechnical)', 'No'),
(1081, 'APPROVED', 32, 'Asst. Mgr/Mgr (Technical)', 'No'),
(1082, 'APPROVED', 32, 'Telecalling/Telemarketing Exec.', 'No'),
(1083, 'APPROVED', 32, 'Associate/Sr. Associate -(NonTechnical)', 'No'),
(1084, 'APPROVED', 32, 'Associate/Sr. Associate -(Technical)', 'No'),
(1085, 'APPROVED', 32, 'Team Leader -(NonTechnical)', 'No'),
(1086, 'APPROVED', 32, 'Team Leader -(Technical)', 'No'),
(1087, 'APPROVED', 32, 'Asst. Mgr/Mgr -(Technical)', 'No'),
(1088, 'APPROVED', 32, 'Asst. Mgr / Mgr -(NonTechnical)', 'No'),
(1089, 'APPROVED', 32, 'Process Flow Analyst', 'No'),
(1090, 'APPROVED', 32, 'Business/EDP Analyst', 'No'),
(1091, 'APPROVED', 32, 'BD Mgr', 'No'),
(1092, 'APPROVED', 32, 'Transitions/Migrations Mgr', 'No'),
(1093, 'APPROVED', 32, 'Operations Mgr', 'No'),
(1094, 'APPROVED', 32, 'Infrastructure & Technology Mgr', 'No'),
(1095, 'APPROVED', 32, 'Dialer Mgr', 'No'),
(1096, 'APPROVED', 32, 'Technical/Process Trainer', 'No'),
(1097, 'APPROVED', 32, 'Voice & Accent Trainer', 'No'),
(1098, 'APPROVED', 32, 'Soft Skills Trainer', 'No'),
(1099, 'APPROVED', 32, 'QA/QC Exec.', 'No'),
(1100, 'APPROVED', 32, 'QA/QC Mgr', 'No'),
(1101, 'APPROVED', 32, 'Quality Coach', 'No'),
(1102, 'APPROVED', 32, 'Team Leader-QA/QC', 'No'),
(1103, 'APPROVED', 32, 'Head/VP/GM-Operations', 'No'),
(1104, 'APPROVED', 32, 'Head/VP/GM-Training & Development', 'No'),
(1105, 'APPROVED', 32, 'Head/VP/GM-Transitions', 'No'),
(1106, 'APPROVED', 32, 'Service Delivery Leader', 'No'),
(1107, 'APPROVED', 32, 'Head/VP/GM-QA & QC', 'No'),
(1108, 'APPROVED', 32, 'Medical Transcriptionist', 'No'),
(1109, 'APPROVED', 32, 'Fresher', 'No'),
(1110, 'APPROVED', 32, 'Trainee', 'No'),
(1111, 'APPROVED', 32, 'Outside Consultant', 'No'),
(1112, 'APPROVED', 32, 'Other', 'No'),
(1113, 'APPROVED', 33, 'Apprentice/Intern', 'No'),
(1114, 'APPROVED', 33, 'Private Attorney/Lawyer', 'No'),
(1115, 'APPROVED', 33, 'Advisor/Outside Consultant', 'No'),
(1116, 'APPROVED', 33, 'Law Officer', 'No'),
(1117, 'APPROVED', 33, 'Legal Mgr', 'No'),
(1118, 'APPROVED', 33, 'Company Secretary', 'No'),
(1119, 'APPROVED', 33, 'Head/VP/GM-Legal', 'No'),
(1120, 'APPROVED', 33, 'Drug Regulatory Dr.', 'No'),
(1121, 'APPROVED', 33, 'Documentation/Medical Writing', 'No'),
(1122, 'APPROVED', 33, 'Regulatory Affairs Mgr', 'No'),
(1123, 'APPROVED', 33, 'Head/VP/GM-Regulatory Affairs', 'No'),
(1124, 'APPROVED', 33, 'Other', 'No'),
(1125, 'APPROVED', 33, 'Fresher', 'No'),
(1126, 'APPROVED', 33, 'Trainee', 'No'),
(1127, 'APPROVED', 34, 'Client Servicing Exec.', 'No'),
(1128, 'APPROVED', 34, 'Client Servicing/Key Account Mgr', 'No'),
(1129, 'APPROVED', 34, 'Account Director', 'No'),
(1130, 'APPROVED', 34, 'Creative Director', 'No'),
(1131, 'APPROVED', 34, 'Media Planning Exec./Mgr', 'No'),
(1132, 'APPROVED', 34, 'Media Buying Exec./Mgr', 'No'),
(1133, 'APPROVED', 34, 'Events/Promotion Exec.', 'No'),
(1134, 'APPROVED', 34, 'Events/Promotion Mgr', 'No'),
(1135, 'APPROVED', 34, 'Corp. Communication Exec.', 'No'),
(1136, 'APPROVED', 34, 'Direct Mktg Exec.', 'No'),
(1137, 'APPROVED', 34, 'Direct Mktg Mgr', 'No'),
(1138, 'APPROVED', 34, 'Product Exec.', 'No'),
(1139, 'APPROVED', 34, 'Product/Brand Mgr', 'No'),
(1140, 'APPROVED', 34, 'Business Alliances Mgr', 'No'),
(1141, 'APPROVED', 34, 'Mktg Mgr', 'No'),
(1142, 'APPROVED', 34, 'Zonal Marketing Manager', 'No'),
(1143, 'APPROVED', 34, 'Branch Marketing Manager', 'No'),
(1144, 'APPROVED', 34, 'Regional Marketing Manager', 'No'),
(1145, 'APPROVED', 34, 'Retail Marketing Manager', 'No'),
(1146, 'APPROVED', 34, 'Rural Marketing Manager', 'No'),
(1147, 'APPROVED', 34, 'Assistant / Associate Marketing Manager', 'No'),
(1148, 'APPROVED', 34, 'International Marketing Manager', 'No'),
(1149, 'APPROVED', 34, 'Sourcing Manager', 'No'),
(1150, 'APPROVED', 34, 'Manager Marketing - Internal / External Communication', 'No'),
(1151, 'APPROVED', 34, 'Manager - Market Research /Consumer Insights / Industry Analysis', 'No'),
(1152, 'APPROVED', 34, 'Search Engine Marketing/SEM Specialist', 'No'),
(1153, 'APPROVED', 34, 'Search Engine Optimisation /SEO Specialist', 'No'),
(1154, 'APPROVED', 34, 'Search Engine Optimisation /SEO Lead', 'No'),
(1155, 'APPROVED', 34, 'Search Engine Optimisation /SEO Analyst', 'No'),
(1156, 'APPROVED', 34, 'Affiliate Marketing Manager', 'No'),
(1157, 'APPROVED', 34, 'Email Marketing Manager', 'No'),
(1158, 'APPROVED', 34, 'PPC/Pay Per Click Specialist', 'No'),
(1159, 'APPROVED', 34, 'PPC/Pay Per Click Lead', 'No'),
(1160, 'APPROVED', 34, 'Display Marketing Executive', 'No'),
(1161, 'APPROVED', 34, 'Display Marketing Manager', 'No'),
(1162, 'APPROVED', 34, 'Social Media Marketing Manager', 'No'),
(1163, 'APPROVED', 34, 'Art Director/Sr Art Director', 'No'),
(1164, 'APPROVED', 34, 'Asst Art Director', 'No'),
(1165, 'APPROVED', 34, 'Visualiser', 'No'),
(1166, 'APPROVED', 34, 'Sr Visualiser', 'No'),
(1167, 'APPROVED', 34, 'Copywriter', 'No'),
(1168, 'APPROVED', 34, 'Graphic Designer', 'No'),
(1169, 'APPROVED', 34, 'MR Exec./Mgr', 'No'),
(1170, 'APPROVED', 34, 'MR Field Supervisor', 'No'),
(1171, 'APPROVED', 34, 'PR Exec.', 'No'),
(1172, 'APPROVED', 34, 'PR & Media Relations Mgr', 'No'),
(1173, 'APPROVED', 34, 'Head/Mgr/GM-Media Planning', 'No'),
(1174, 'APPROVED', 34, 'Head/Mgr/GM-Media Buying', 'No'),
(1175, 'APPROVED', 34, 'Head/VP/GM-PR/Corp. Communication', 'No'),
(1176, 'APPROVED', 34, 'Head/VP/GM-Mktg', 'No'),
(1177, 'APPROVED', 34, 'Head/VP/GM-Business Alliances', 'No'),
(1178, 'APPROVED', 34, 'Head/VP/GM- MR', 'No'),
(1179, 'APPROVED', 34, 'Head/VP/GM-Client Servicing', 'No'),
(1180, 'APPROVED', 34, 'National Creative Director/VP-Creative', 'No'),
(1181, 'APPROVED', 34, 'Head/VP/GM/ Mgr-Online/Digital Marketing', 'No'),
(1182, 'APPROVED', 34, 'Other', 'No'),
(1183, 'APPROVED', 34, 'Outside Consultant', 'No'),
(1184, 'APPROVED', 34, 'Trainee', 'No'),
(1185, 'APPROVED', 34, 'Fresher', 'No'),
(1186, 'APPROVED', 35, 'Scientist', 'No'),
(1187, 'APPROVED', 35, 'Packaging Development Exec./Mgr', 'No'),
(1188, 'APPROVED', 35, 'Head/VP/GM-Packaging Development', 'No'),
(1189, 'APPROVED', 35, 'Fresher', 'No'),
(1190, 'APPROVED', 35, 'Trainee', 'No'),
(1191, 'APPROVED', 35, 'Other', 'No'),
(1192, 'APPROVED', 35, 'Outside Consultant', 'No'),
(1193, 'APPROVED', 36, 'Clinical Research Associate/Scientist', 'No'),
(1194, 'APPROVED', 36, 'Clinical Research Mgr', 'No'),
(1195, 'APPROVED', 36, 'Analytical Chemistry Associate/Scientist', 'No'),
(1196, 'APPROVED', 36, 'Analytical Chemistry Mgr', 'No'),
(1197, 'APPROVED', 36, 'Chemical Research Associate/Scientist', 'No'),
(1198, 'APPROVED', 36, 'Chemical Research Mgr', 'No'),
(1199, 'APPROVED', 36, 'Bio/Pharma Informatics-Associate/Scientist', 'No'),
(1200, 'APPROVED', 36, 'Formulation Scientist', 'No'),
(1201, 'APPROVED', 36, 'Microbiologist', 'No'),
(1202, 'APPROVED', 36, 'Molecular Biology', 'No'),
(1203, 'APPROVED', 36, 'Other Scientist', 'No'),
(1204, 'APPROVED', 36, 'Nutritionist', 'No'),
(1205, 'APPROVED', 36, 'Research Scientist', 'No'),
(1206, 'APPROVED', 36, 'Bio-Tech Research Associate/Scientist', 'No'),
(1207, 'APPROVED', 36, 'Bio-Tech Research Mgr', 'No'),
(1208, 'APPROVED', 36, 'Pharmacist/Chemist/Bio Chemist', 'No'),
(1209, 'APPROVED', 36, 'Bio-Statistician', 'No'),
(1210, 'APPROVED', 36, 'Chief Medical Officer/Head Medical Services', 'No'),
(1211, 'APPROVED', 36, 'Clinical Researcher', 'No'),
(1212, 'APPROVED', 36, 'Intern', 'No'),
(1213, 'APPROVED', 36, 'Admin Services/Medical Facilities', 'No'),
(1214, 'APPROVED', 36, 'Lab Technician/Medical Technician/Lab Staff', 'No'),
(1215, 'APPROVED', 36, 'Medical Officer', 'No'),
(1216, 'APPROVED', 36, 'Nurse', 'No'),
(1217, 'APPROVED', 36, 'Medical Superintendent/Director', 'No'),
(1218, 'APPROVED', 36, 'Anaesthetist', 'No'),
(1219, 'APPROVED', 36, 'Cardiologist', 'No'),
(1220, 'APPROVED', 36, 'Dermatologist', 'No'),
(1221, 'APPROVED', 36, 'Dietician/Nutritionist', 'No'),
(1222, 'APPROVED', 36, 'ENT Specialist', 'No'),
(1223, 'APPROVED', 36, 'General Practitioner', 'No'),
(1224, 'APPROVED', 36, 'Gynaeocologist', 'No'),
(1225, 'APPROVED', 36, 'Hepatologist', 'No'),
(1226, 'APPROVED', 36, 'Microbiologist', 'No'),
(1227, 'APPROVED', 36, 'Nephrologist', 'No'),
(1228, 'APPROVED', 36, 'Neurologist', 'No'),
(1229, 'APPROVED', 36, 'Oncologist', 'No'),
(1230, 'APPROVED', 36, 'Opthamologist', 'No'),
(1231, 'APPROVED', 36, 'Orthopaedist', 'No'),
(1232, 'APPROVED', 36, 'Paramedic', 'No'),
(1233, 'APPROVED', 36, 'Pathologist', 'No'),
(1234, 'APPROVED', 36, 'Pediatrician', 'No'),
(1235, 'APPROVED', 36, 'Pharmacist/Chemist/Bio Chemist', 'No'),
(1236, 'APPROVED', 36, 'Physiotherapist', 'No'),
(1237, 'APPROVED', 36, 'Psychiatrist', 'No'),
(1238, 'APPROVED', 36, 'Radiologist', 'No'),
(1239, 'APPROVED', 36, 'Surgeon', 'No'),
(1240, 'APPROVED', 36, 'Medical Rep.', 'No'),
(1241, 'APPROVED', 36, 'Head Nurse / Nursing Superintendent / Clinical Instructor', 'No'),
(1242, 'APPROVED', 36, 'Paramedic Nurse', 'No'),
(1243, 'APPROVED', 36, 'Pathology Assistant', 'No'),
(1244, 'APPROVED', 36, 'Radiologic technologists', 'No'),
(1245, 'APPROVED', 36, 'Staff Nurse', 'No'),
(1246, 'APPROVED', 36, 'Surgical Nurse', 'No'),
(1247, 'APPROVED', 36, 'Ward Sister / Ward Supervisor', 'No'),
(1248, 'APPROVED', 36, 'Drug Regulatory Dr.', 'No'),
(1249, 'APPROVED', 36, 'Documentation/Medical Writing', 'No'),
(1250, 'APPROVED', 36, 'Regulatory Affairs Mgr', 'No'),
(1251, 'APPROVED', 36, 'Other', 'No'),
(1252, 'APPROVED', 37, 'Industrial Engnr', 'No'),
(1253, 'APPROVED', 37, 'Design Engnr/Mgr', 'No'),
(1254, 'APPROVED', 37, 'Factory Head', 'No'),
(1255, 'APPROVED', 37, 'Engineering Mgr', 'No'),
(1256, 'APPROVED', 37, 'Production Mgr', 'No'),
(1257, 'APPROVED', 37, 'QA/QC Exec.', 'No'),
(1258, 'APPROVED', 37, 'QA/QC Mgr', 'No'),
(1259, 'APPROVED', 37, 'Product Development Exec.', 'No'),
(1260, 'APPROVED', 37, 'Product Development Mgr', 'No'),
(1261, 'APPROVED', 37, 'Workman/Foreman/Technician', 'No'),
(1262, 'APPROVED', 37, 'Service/Maintenance Engnr', 'No'),
(1263, 'APPROVED', 37, 'Service/Maintenance Supervisor', 'No'),
(1264, 'APPROVED', 37, 'Project Mgr-Production/Manufacturing/Maintenance', 'No'),
(1265, 'APPROVED', 37, 'Safety Officer/Mgr', 'No'),
(1266, 'APPROVED', 37, 'Environment Engnr/Officer', 'No'),
(1267, 'APPROVED', 37, 'Health-Officer/Mgr', 'No'),
(1268, 'APPROVED', 37, 'Head/VP/GM-QA/QC', 'No'),
(1269, 'APPROVED', 37, 'Head/VP/GM-Production/Manufacturing/Maintenance', 'No'),
(1270, 'APPROVED', 37, 'Head/VP/GM-Operations', 'No'),
(1271, 'APPROVED', 37, 'SBU Head/Profit Centre Head', 'No'),
(1272, 'APPROVED', 37, 'Head/VP/GM-Regulatory Affairs', 'No'),
(1273, 'APPROVED', 37, 'Outside Consultant', 'No'),
(1274, 'APPROVED', 37, 'Trainee', 'No'),
(1275, 'APPROVED', 37, 'Fresher', 'No'),
(1276, 'APPROVED', 37, 'Other', 'No'),
(1277, 'APPROVED', 38, 'Store Keeper/Warehouse Assistant', 'No'),
(1278, 'APPROVED', 38, 'Warehouse Mgr', 'No'),
(1279, 'APPROVED', 38, 'CFA', 'No'),
(1280, 'APPROVED', 38, 'Logistics Exec.', 'No'),
(1281, 'APPROVED', 38, 'Logistics Mgr', 'No'),
(1282, 'APPROVED', 38, 'Transport/Distribution Mgr', 'No'),
(1283, 'APPROVED', 38, 'Purchase Exec.', 'No'),
(1284, 'APPROVED', 38, 'Purchase/Vendor DevelopmentMgr', 'No'),
(1285, 'APPROVED', 38, 'Material Mgmt Exec./Mgr', 'No'),
(1286, 'APPROVED', 38, 'Commercial Mgr', 'No'),
(1287, 'APPROVED', 38, 'QA/QC Exec.', 'No'),
(1288, 'APPROVED', 38, 'QA/QC Mgr', 'No'),
(1289, 'APPROVED', 38, 'Commodity Trading Mgr', 'No'),
(1290, 'APPROVED', 38, 'Head/VP/GM-SCM/Logistics', 'No'),
(1291, 'APPROVED', 38, 'Head/VP/GM-Commercial', 'No'),
(1292, 'APPROVED', 38, 'Head/VP/GM-Purchase/Material Mgmt', 'No'),
(1293, 'APPROVED', 38, 'Other', 'No'),
(1294, 'APPROVED', 38, 'Trainee', 'No'),
(1295, 'APPROVED', 38, 'Fresher', 'No'),
(1296, 'APPROVED', 39, 'Sales Exec./Officer', 'No'),
(1297, 'APPROVED', 39, 'Counter Sales', 'No'),
(1298, 'APPROVED', 39, 'Medical Rep.', 'No'),
(1299, 'APPROVED', 39, 'Merchandiser', 'No'),
(1300, 'APPROVED', 39, 'Sales/BD Mgr', 'No'),
(1301, 'APPROVED', 39, 'Sales Promotion Mgr', 'No'),
(1302, 'APPROVED', 39, 'Retail Store Mgr', 'No'),
(1303, 'APPROVED', 39, 'Branch Mgr', 'No'),
(1304, 'APPROVED', 39, 'Regional Mgr', 'No'),
(1305, 'APPROVED', 39, 'Area Sales Manager', 'No'),
(1306, 'APPROVED', 39, 'Sales Exec./Officer', 'No'),
(1307, 'APPROVED', 39, 'Sales/BD Mgr', 'No'),
(1308, 'APPROVED', 39, 'Client Servicing/Key Account Mgr', 'No'),
(1309, 'APPROVED', 39, 'Branch Mgr/Regional Mgr', 'No'),
(1310, 'APPROVED', 39, 'Sales Exec./Officer', 'No'),
(1311, 'APPROVED', 39, 'Sales/BD Mgr', 'No'),
(1312, 'APPROVED', 39, 'Sales Promotion Mgr', 'No'),
(1313, 'APPROVED', 39, 'Banquet Sales Exec./Mgr', 'No'),
(1314, 'APPROVED', 39, 'Institutional Sales/BD Mgr', 'No'),
(1315, 'APPROVED', 39, 'Sales Executive / Officer', 'No'),
(1316, 'APPROVED', 39, 'Sales / BD Manager', 'No'),
(1317, 'APPROVED', 39, 'Client Relationship Manager', 'No'),
(1318, 'APPROVED', 39, 'Key Account Manager', 'No'),
(1319, 'APPROVED', 39, 'Area / Territory Manager', 'No'),
(1320, 'APPROVED', 39, 'Regional Sales Manager', 'No'),
(1321, 'APPROVED', 39, 'Sales Trainer', 'No'),
(1322, 'APPROVED', 39, 'Telesales/Telemarketing Exec./Officer', 'No'),
(1323, 'APPROVED', 39, 'Sales Promotion Mgr', 'No'),
(1324, 'APPROVED', 39, 'Front Desk/Cashier/Billing', 'No'),
(1325, 'APPROVED', 39, 'Sales Coordinator', 'No'),
(1326, 'APPROVED', 39, 'Proposal Response Manager', 'No'),
(1327, 'APPROVED', 39, 'Bid Manager', 'No'),
(1328, 'APPROVED', 39, 'Collaterals / Flyers Manager', 'No'),
(1329, 'APPROVED', 39, 'RFI / RFP Manager', 'No'),
(1330, 'APPROVED', 39, 'Pre Sales Consultant', 'No'),
(1331, 'APPROVED', 39, 'Post Sales Consultant', 'No'),
(1332, 'APPROVED', 39, 'Service Engineer', 'No'),
(1333, 'APPROVED', 39, 'Service Manager', 'No'),
(1334, 'APPROVED', 39, 'Head/VP/GM/National Mgr -Sales', 'No'),
(1335, 'APPROVED', 39, 'Head / VP/ GM/ National Manager After Sales', 'No'),
(1336, 'APPROVED', 39, 'Other', 'No'),
(1337, 'APPROVED', 39, 'Trainee', 'No'),
(1338, 'APPROVED', 39, 'Fresher', 'No'),
(1339, 'APPROVED', 40, 'Stenographer/Data Entry Operator', 'No'),
(1340, 'APPROVED', 40, 'Receptionist', 'No'),
(1341, 'APPROVED', 40, 'Secretary/PA', 'No'),
(1342, 'APPROVED', 40, 'Other', 'No'),
(1343, 'APPROVED', 40, 'Fresher', 'No'),
(1344, 'APPROVED', 40, 'Trainee', 'No'),
(1345, 'APPROVED', 41, 'CEO/MD/Director', 'No'),
(1346, 'APPROVED', 41, 'Outside Consultant', 'No'),
(1347, 'APPROVED', 41, 'Director', 'No'),
(1348, 'APPROVED', 41, 'VP/President/Partner', 'No'),
(1349, 'APPROVED', 41, 'Other', 'No'),
(1350, 'APPROVED', 42, 'Deck Cadet', 'No'),
(1351, 'APPROVED', 42, 'Trainee Cadet', 'No'),
(1352, 'APPROVED', 42, 'Marine Captain / Master Mariner', 'No'),
(1353, 'APPROVED', 42, 'Ship Captain', 'No'),
(1354, 'APPROVED', 42, 'Cabin Attendent', 'No'),
(1355, 'APPROVED', 42, 'Chief Mate', 'No'),
(1356, 'APPROVED', 42, 'Chief Operation Officer', 'No'),
(1357, 'APPROVED', 42, 'Seaman', 'No'),
(1358, 'APPROVED', 42, 'Able Seaman (AB)', 'No'),
(1359, 'APPROVED', 42, 'Ordinary Seaman (OS)', 'No'),
(1360, 'APPROVED', 42, 'Chief Electro Technical Officer (ETO)', 'No'),
(1361, 'APPROVED', 42, 'Electrical Officer', 'No'),
(1362, 'APPROVED', 42, 'Radio Officer', 'No'),
(1363, 'APPROVED', 42, 'Chief Engineer', 'No'),
(1364, 'APPROVED', 42, 'Electrical Engineer', 'No'),
(1365, 'APPROVED', 42, 'Gas Engineer', 'No'),
(1366, 'APPROVED', 42, 'Reefer Engineer', 'No'),
(1367, 'APPROVED', 42, 'Trainee Engineer', 'No'),
(1368, 'APPROVED', 42, '2nd Engineer', 'No'),
(1369, 'APPROVED', 42, '3rd Engineer', 'No'),
(1370, 'APPROVED', 42, '4th Engineer', 'No'),
(1371, 'APPROVED', 42, '5th Engineer', 'No'),
(1372, 'APPROVED', 42, 'Chief Mechanic / Machinist / Motorman', 'No'),
(1373, 'APPROVED', 42, 'Pumpman', 'No'),
(1374, 'APPROVED', 42, 'Crane Operator', 'No'),
(1375, 'APPROVED', 42, 'Deck Fitter / Oilers', 'No'),
(1376, 'APPROVED', 42, 'Engine Fitter', 'No'),
(1377, 'APPROVED', 42, 'Steward', 'No'),
(1378, 'APPROVED', 42, 'Chief Steward', 'No'),
(1379, 'APPROVED', 42, 'Laundry Man', 'No'),
(1380, 'APPROVED', 42, 'Bosun', 'No'),
(1381, 'APPROVED', 42, 'Wiper', 'No'),
(1382, 'APPROVED', 42, 'Cook', 'No'),
(1383, 'APPROVED', 42, 'Chief Cook', 'No'),
(1384, 'APPROVED', 42, 'Sous Chef', 'No'),
(1385, 'APPROVED', 42, 'Chef', 'No'),
(1386, 'APPROVED', 42, 'Bar Tender', 'No'),
(1387, 'APPROVED', 42, 'Musician', 'No'),
(1388, 'APPROVED', 42, 'Purser', 'No'),
(1389, 'APPROVED', 43, 'Trainee', 'No'),
(1390, 'APPROVED', 43, 'Project Mgr-Telecom', 'No'),
(1391, 'APPROVED', 43, 'Project Mgr-IT/Software', 'No'),
(1392, 'APPROVED', 43, 'Project Mgr-Production/Manufacturing/Maintenance', 'No'),
(1393, 'APPROVED', 43, 'Civil Engnr-Telecom', 'No'),
(1394, 'APPROVED', 43, 'Civil Engnr-Municipal', 'No'),
(1395, 'APPROVED', 43, 'Civil Engnr-Water/Wastewater', 'No'),
(1396, 'APPROVED', 43, 'Civil Engnr-Land Development', 'No'),
(1397, 'APPROVED', 43, 'Civil Engnr-Aviation', 'No'),
(1398, 'APPROVED', 43, 'Civil Engnr-Highway Roadway', 'No'),
(1399, 'APPROVED', 43, 'Civil Engnr-Traffic', 'No'),
(1400, 'APPROVED', 43, 'Civil Engnr-Other', 'No'),
(1401, 'APPROVED', 43, 'Electrical Engnr-Telecom', 'No'),
(1402, 'APPROVED', 43, 'Electrical Engnr-Commercial', 'No'),
(1403, 'APPROVED', 43, 'Electrical Engnr-Industrial', 'No'),
(1404, 'APPROVED', 43, 'Electrical Engnr-Utility', 'No'),
(1405, 'APPROVED', 43, 'Electrical Engnr-Other', 'No'),
(1406, 'APPROVED', 43, 'Geotechnical Engnr', 'No'),
(1407, 'APPROVED', 43, 'Mech. Engnr-Telecom', 'No'),
(1408, 'APPROVED', 43, 'Mech. Engnr-HVAC', 'No'),
(1409, 'APPROVED', 43, 'Mech. Engnr-Plumbing/Fire Protection', 'No'),
(1410, 'APPROVED', 43, 'Mech. Engnr-Other', 'No'),
(1411, 'APPROVED', 43, 'Process Engnr-Plant Design', 'No'),
(1412, 'APPROVED', 43, 'Structural Engnr-Bridge', 'No'),
(1413, 'APPROVED', 43, 'Structural Engnr-Building', 'No'),
(1414, 'APPROVED', 43, 'Structural Engnr-Other', 'No'),
(1415, 'APPROVED', 43, 'Geographic Information Systems/GIS', 'No'),
(1416, 'APPROVED', 43, 'Construction-General Building', 'No'),
(1417, 'APPROVED', 43, 'Construction-Heavy', 'No'),
(1418, 'APPROVED', 43, 'Construction-Residential', 'No'),
(1419, 'APPROVED', 43, 'Construction-Specialty', 'No'),
(1420, 'APPROVED', 43, 'Construction-Construction Management', 'No'),
(1421, 'APPROVED', 43, 'Construction-Other', 'No'),
(1422, 'APPROVED', 43, 'Maintenance Engnr', 'No'),
(1423, 'APPROVED', 43, 'Engnr-Other', 'No'),
(1424, 'APPROVED', 43, 'Fresher', 'No'),
(1425, 'APPROVED', 44, 'Counsellor', 'No'),
(1426, 'APPROVED', 44, 'Librarian', 'No'),
(1427, 'APPROVED', 44, 'Teacher/ Private Tutor', 'No'),
(1428, 'APPROVED', 44, 'Special Education Teacher', 'No'),
(1429, 'APPROVED', 44, 'Translator', 'No'),
(1430, 'APPROVED', 44, 'Transcriptionist', 'No'),
(1431, 'APPROVED', 44, 'Junior/Primary/Assistant Teacher', 'No'),
(1432, 'APPROVED', 44, 'Class Teacher / Classroom coordinator', 'No'),
(1433, 'APPROVED', 44, 'Head Teacher / Head Mistress / Head Master', 'No'),
(1434, 'APPROVED', 44, 'Nursery Teacher', 'No'),
(1435, 'APPROVED', 44, 'School Teacher', 'No'),
(1436, 'APPROVED', 44, 'Vice Principal', 'No'),
(1437, 'APPROVED', 44, 'Principal', 'No'),
(1438, 'APPROVED', 44, 'Curriculum Designer', 'No'),
(1439, 'APPROVED', 44, 'Lab Assistant', 'No'),
(1440, 'APPROVED', 44, 'Warden', 'No'),
(1441, 'APPROVED', 44, 'Trainer', 'No'),
(1442, 'APPROVED', 44, 'Soft Skill Trainer', 'No'),
(1443, 'APPROVED', 44, 'Technical / Process Trainer', 'No'),
(1444, 'APPROVED', 44, 'Voice and Accent Trainer', 'No'),
(1445, 'APPROVED', 44, 'Creche Teacher / Incharge / Attendant', 'No'),
(1446, 'APPROVED', 44, 'DaycareTeacher / Incharge / Attendant', 'No'),
(1447, 'APPROVED', 44, 'English Teacher', 'No'),
(1448, 'APPROVED', 44, 'French Teacher', 'No'),
(1449, 'APPROVED', 44, 'German Teacher', 'No'),
(1450, 'APPROVED', 44, 'Hindi Teacher', 'No'),
(1451, 'APPROVED', 44, 'Sanskrit Teacher', 'No'),
(1452, 'APPROVED', 44, 'Spanish Teacher', 'No'),
(1453, 'APPROVED', 44, 'Tamil Teacher', 'No'),
(1454, 'APPROVED', 44, 'Japanese Teacher', 'No'),
(1455, 'APPROVED', 44, 'Arabic Teacher', 'No'),
(1456, 'APPROVED', 44, 'Urdu Teacher', 'No'),
(1457, 'APPROVED', 44, 'Bengali Teacher', 'No'),
(1458, 'APPROVED', 44, 'Chinese Teacher', 'No'),
(1459, 'APPROVED', 44, 'Punjabi Teacher', 'No'),
(1460, 'APPROVED', 44, 'Italian Teacher', 'No'),
(1461, 'APPROVED', 44, 'Accounts Teacher', 'No'),
(1462, 'APPROVED', 44, 'Biology Teacher', 'No'),
(1463, 'APPROVED', 44, 'Chemistry Teacher', 'No'),
(1464, 'APPROVED', 44, 'Commerce Teacher', 'No'),
(1465, 'APPROVED', 44, 'Computer Teacher', 'No'),
(1466, 'APPROVED', 44, 'Economics Teacher', 'No'),
(1467, 'APPROVED', 44, 'Geography Teacher', 'No'),
(1468, 'APPROVED', 44, 'History Teacher', 'No'),
(1469, 'APPROVED', 44, 'Social Studies Teacher', 'No'),
(1470, 'APPROVED', 44, 'Mathematics Teacher', 'No'),
(1471, 'APPROVED', 44, 'Physics Teacher', 'No'),
(1472, 'APPROVED', 44, 'Science Teacher', 'No'),
(1473, 'APPROVED', 44, 'Business Studies Teacher', 'No'),
(1474, 'APPROVED', 44, 'Arts Teacher', 'No'),
(1475, 'APPROVED', 44, 'Dance Teacher', 'No'),
(1476, 'APPROVED', 44, 'Drawing Teacher', 'No'),
(1477, 'APPROVED', 44, 'Music Teacher', 'No'),
(1478, 'APPROVED', 44, 'Sports / Physical Education Teacher', 'No'),
(1479, 'APPROVED', 44, 'Yoga Teacher', 'No'),
(1480, 'APPROVED', 44, 'Drama/Theater Teacher', 'No'),
(1481, 'APPROVED', 44, 'Home Science Teacher', 'No'),
(1482, 'APPROVED', 44, 'Lecturer/Professor', 'No'),
(1483, 'APPROVED', 44, 'Assistant Professor', 'No'),
(1484, 'APPROVED', 44, 'Chancellor', 'No'),
(1485, 'APPROVED', 44, 'Vice - Chancellor', 'No'),
(1486, 'APPROVED', 44, 'Dean / Director', 'No'),
(1487, 'APPROVED', 44, 'Chairman', 'No'),
(1488, 'APPROVED', 44, 'HOD', 'No'),
(1489, 'APPROVED', 44, 'Visiting Faculty / Guest Faculty', 'No'),
(1490, 'APPROVED', 44, 'Academic Coordinator', 'No'),
(1491, 'APPROVED', 44, 'Other', 'No'),
(1492, 'APPROVED', 44, 'Trainee', 'No'),
(1493, 'APPROVED', 44, 'Fresher', 'No'),
(1494, 'APPROVED', 45, 'Travel Agent', 'No'),
(1495, 'APPROVED', 45, 'Reservations Exec.', 'No'),
(1496, 'APPROVED', 45, 'Reservations Mgr', 'No'),
(1497, 'APPROVED', 45, 'Tour Mngmt Exec.', 'No'),
(1498, 'APPROVED', 45, 'Tour Mgmt Mgr/Sr. Mgr', 'No'),
(1499, 'APPROVED', 45, 'Operations Exec.', 'No'),
(1500, 'APPROVED', 45, 'BD Mgr', 'No'),
(1501, 'APPROVED', 45, 'Mktg Mgr', 'No'),
(1502, 'APPROVED', 45, 'Branch Mgr', 'No'),
(1503, 'APPROVED', 45, 'Regional Mgr', 'No'),
(1504, 'APPROVED', 45, 'General Mgr', 'No'),
(1505, 'APPROVED', 45, 'Cashier/Billing Mgr', 'No'),
(1506, 'APPROVED', 45, 'Operations Mgr', 'No'),
(1507, 'APPROVED', 45, 'Cabin Crew', 'No'),
(1508, 'APPROVED', 45, 'Ground Staff', 'No'),
(1509, 'APPROVED', 45, 'AviationEngnr', 'No'),
(1510, 'APPROVED', 45, 'Maintenance Engnr', 'No'),
(1511, 'APPROVED', 45, 'Air traffic controller', 'No'),
(1512, 'APPROVED', 45, 'Airline Security Officer', 'No'),
(1513, 'APPROVED', 45, 'Airport Coordinator', 'No'),
(1514, 'APPROVED', 45, 'Cargo Operations / Officer / Loadmaster', 'No'),
(1515, 'APPROVED', 45, 'Co-Pilot / First Officer / Second Officer / Third Officer / Captain', 'No'),
(1516, 'APPROVED', 45, 'Flight Dispatcher', 'No'),
(1517, 'APPROVED', 45, 'Flight Engineer / Air Mechanic', 'No'),
(1518, 'APPROVED', 45, 'Helicopter Pilots', 'No'),
(1519, 'APPROVED', 45, 'Inflight Services / Operations', 'No'),
(1520, 'APPROVED', 45, 'Radio Operator / Radio Engineer / Radar Operator', 'No'),
(1521, 'APPROVED', 45, 'SBU/Profit Center Head', 'No'),
(1522, 'APPROVED', 45, 'Head/VP/GM-Tour Management', 'No'),
(1523, 'APPROVED', 45, 'CEO/MD/Director', 'No'),
(1524, 'APPROVED', 45, 'Other', 'No'),
(1525, 'APPROVED', 45, 'Fresher', 'No'),
(1526, 'APPROVED', 45, 'Trainee', 'No'),
(1527, 'APPROVED', 45, 'Outside Consultant', 'No'),
(1528, 'APPROVED', 46, 'CEO/MD/Director', 'No'),
(1529, 'APPROVED', 46, 'CIO', 'No'),
(1530, 'APPROVED', 46, 'Creative Director', 'No'),
(1531, 'APPROVED', 46, 'National Creative Director/VP-Creative', 'No'),
(1532, 'APPROVED', 46, 'CTO/Head/VP-Technology (Telecom/ISP)', 'No'),
(1533, 'APPROVED', 46, 'Executive/Master Chef', 'No'),
(1534, 'APPROVED', 46, 'Head/VP/GM-Documentation/Shipping', 'No'),
(1535, 'APPROVED', 46, 'Head/VP/GM-BD', 'No'),
(1536, 'APPROVED', 46, 'Head/VP/GM-Relationships', 'No'),
(1537, 'APPROVED', 46, 'Head/VP/GM-Transitions', 'No'),
(1538, 'APPROVED', 46, 'Head/VP/GM-HR', 'No'),
(1539, 'APPROVED', 46, 'Head/VP/GM-Training and Development', 'No'),
(1540, 'APPROVED', 46, 'Head/VP/GM-Technology (IT)/CTO', 'No'),
(1541, 'APPROVED', 46, 'Head/Mgr/GM-Media Buying', 'No'),
(1542, 'APPROVED', 46, 'Head/Mgr/GM-Media Planning', 'No'),
(1543, 'APPROVED', 46, 'Head/VP/GM-Operations', 'No'),
(1544, 'APPROVED', 46, 'Head/VP/GM-SCM/Logistics', 'No'),
(1545, 'APPROVED', 46, 'Head/VP/GM-Admin & Facilities', 'No'),
(1546, 'APPROVED', 46, 'Head/VP/GM-Commercial', 'No'),
(1547, 'APPROVED', 46, 'Head/VP/GM-Mktg', 'No'),
(1548, 'APPROVED', 46, 'Head/VP/GM- MR', 'No'),
(1549, 'APPROVED', 46, 'Head/VP/GM- Purchase/Material Mgmt', 'No'),
(1550, 'APPROVED', 46, 'Head/VP/GM -Accounts', 'No'),
(1551, 'APPROVED', 46, 'Head/VP/GM-F&B', 'No'),
(1552, 'APPROVED', 46, 'Head/VP/GM-Business Alliances', 'No'),
(1553, 'APPROVED', 46, 'Head/VP/GM-Finance/Audit', 'No'),
(1554, 'APPROVED', 46, 'Head/VP/GM-Investment Banking', 'No'),
(1555, 'APPROVED', 46, 'Head/VP/GM-Private Equity/Hedge Fund/VC', 'No'),
(1556, 'APPROVED', 46, 'Head/VP/GM-Project Finance', 'No'),
(1557, 'APPROVED', 46, 'Head/VP/GM-QA&QC', 'No'),
(1558, 'APPROVED', 46, 'Head/VP/GM-Quality', 'No'),
(1559, 'APPROVED', 46, 'Head/VP/GM-Sales', 'No'),
(1560, 'APPROVED', 46, 'Head/VP/GM-Underwritting', 'No'),
(1561, 'APPROVED', 46, 'Head/VP/GM-Operations', 'No'),
(1562, 'APPROVED', 46, 'Head/VP/GM-Fund Mgmt', 'No'),
(1563, 'APPROVED', 46, 'Head/VP/GM -Credit/Risk', 'No'),
(1564, 'APPROVED', 46, 'Head/VP/GM-Depository Services', 'No'),
(1565, 'APPROVED', 46, 'Head/VP/GM-Legal', 'No'),
(1566, 'APPROVED', 46, 'Head/VP/GM-Production/Manufacturing/Maintenance', 'No'),
(1567, 'APPROVED', 46, 'Head/VP/GM-Tour Mgmt', 'No'),
(1568, 'APPROVED', 46, 'Head/VP/-PR/Corp. Communication', 'No'),
(1569, 'APPROVED', 46, 'Head/VP/GM-Broking', 'No'),
(1570, 'APPROVED', 46, 'Head/VP/GM-CFO/Financial Controller', 'No'),
(1571, 'APPROVED', 46, 'Head/VP/GM-Credit', 'No'),
(1572, 'APPROVED', 46, 'Head/VP/GM-R&D', 'No'),
(1573, 'APPROVED', 46, 'Head/VP/GM-Regulatory Affairs', 'No'),
(1574, 'APPROVED', 46, 'Head/VP/GM-Claims', 'No'),
(1575, 'APPROVED', 46, 'Head/VP/GM-Client Servicing', 'No'),
(1576, 'APPROVED', 46, 'Head/VP/GM-Equity', 'No'),
(1577, 'APPROVED', 46, 'Head/VP/GM-Mergers & Acquisitions', 'No'),
(1578, 'APPROVED', 46, 'Head/VP/GM-Packaging Development', 'No'),
(1579, 'APPROVED', 46, 'Head/VP/GM-Corporate Planning/Strategy', 'No'),
(1580, 'APPROVED', 46, 'Head/VP/GM-Production', 'No'),
(1581, 'APPROVED', 46, 'Head/VP/GM-Treasury', 'No'),
(1582, 'APPROVED', 46, 'Head/VP/GM-Corporate Advisory', 'No'),
(1583, 'APPROVED', 46, 'Head/VP/GM-Domestic Debt', 'No'),
(1584, 'APPROVED', 46, 'Head/VP/GM-Formulations', 'No'),
(1585, 'APPROVED', 46, 'Head/VP/GM-Insurance Operations', 'No'),
(1586, 'APPROVED', 46, 'Head/VP/GM-Offshore Debt', 'No'),
(1587, 'APPROVED', 46, 'Head/VP/GM/National Manager-Sales', 'No'),
(1588, 'APPROVED', 46, 'SBU/Profit Center Head', 'No'),
(1589, 'APPROVED', 46, 'Service Delivery Leader', 'No'),
(1590, 'APPROVED', 46, 'VP/President/Partner', 'No'),
(1591, 'APPROVED', 46, 'Head/VP/GM-Recruitment', 'No'),
(1592, 'APPROVED', 47, 'News Anchor/TV Presenter', 'No'),
(1593, 'APPROVED', 47, 'News Compiler', 'No'),
(1594, 'APPROVED', 47, 'Correspondent', 'No'),
(1595, 'APPROVED', 47, 'Sr/Principal Coresspondent', 'No'),
(1596, 'APPROVED', 47, 'News Editor', 'No'),
(1597, 'APPROVED', 47, 'News/Features Head', 'No'),
(1598, 'APPROVED', 47, 'Spot Boy', 'No'),
(1599, 'APPROVED', 47, 'Animation/Graphic Artist', 'No'),
(1600, 'APPROVED', 47, 'Stunt Coordinator', 'No'),
(1601, 'APPROVED', 47, 'Wardrobe/Make-Up/Hair Artist', 'No'),
(1602, 'APPROVED', 47, 'AV Editor', 'No'),
(1603, 'APPROVED', 47, 'Visualiser', 'No'),
(1604, 'APPROVED', 47, 'Sound Mixer/Engr', 'No'),
(1605, 'APPROVED', 47, 'Locations Mgr', 'No'),
(1606, 'APPROVED', 47, 'Lighting Technician', 'No'),
(1607, 'APPROVED', 47, 'Special Effects Technician', 'No'),
(1608, 'APPROVED', 47, 'Photographer', 'No'),
(1609, 'APPROVED', 47, 'Camera Man/Technician', 'No'),
(1610, 'APPROVED', 47, 'Choreographer', 'No'),
(1611, 'APPROVED', 47, 'Asst. Editor/Editor', 'No'),
(1612, 'APPROVED', 47, 'Head-Lighting', 'No'),
(1613, 'APPROVED', 47, 'Head-Special Effects', 'No'),
(1614, 'APPROVED', 47, 'Music Director', 'No'),
(1615, 'APPROVED', 47, 'Cinematographer', 'No'),
(1616, 'APPROVED', 47, 'Asst. Director/Director', 'No'),
(1617, 'APPROVED', 47, 'TV Producer', 'No'),
(1618, 'APPROVED', 47, 'Film Producer', 'No'),
(1619, 'APPROVED', 47, 'Other', 'No'),
(1620, 'APPROVED', 47, 'Fresher', 'No'),
(1621, 'APPROVED', 47, 'Trainee', 'No'),
(1622, 'APPROVED', 48, 'Art Director/Sr Art Director', 'No'),
(1623, 'APPROVED', 48, 'Interaction Designer', 'No'),
(1624, 'APPROVED', 48, 'Product Designer', 'No'),
(1625, 'APPROVED', 48, 'Textile Designer', 'No'),
(1626, 'APPROVED', 48, 'User Experience Designer', 'No'),
(1627, 'APPROVED', 48, 'Visual Merchandiser', 'No'),
(1628, 'APPROVED', 48, 'Visualiser', 'No'),
(1629, 'APPROVED', 48, 'Web Designer', 'No'),
(1630, 'APPROVED', 48, 'Copywriter', 'No'),
(1631, 'APPROVED', 48, 'Graphic Designer', 'No'),
(1632, 'APPROVED', 48, 'Animation Designer', 'No'),
(1633, 'APPROVED', 48, 'Creative Director', 'No'),
(1634, 'APPROVED', 48, 'National Creative Director/VP-Creative', 'No'),
(1635, 'APPROVED', 48, 'Commercial Artist', 'No'),
(1636, 'APPROVED', 48, 'Other', 'No'),
(1637, 'APPROVED', 48, 'Fresher', 'No'),
(1638, 'APPROVED', 48, 'Trainee', 'No'),
(1639, 'APPROVED', 50, 'Demo Role', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `salary_range`
--

CREATE TABLE `salary_range` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') NOT NULL,
  `salary_range` varchar(255) NOT NULL,
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_range`
--

INSERT INTO `salary_range` (`id`, `status`, `salary_range`, `is_deleted`) VALUES
(1, 'APPROVED', 'Rs. 5000 - Rs. 10000', 'No'),
(2, 'APPROVED', 'Rs. 10000 - Rs. 20000', 'No'),
(3, 'APPROVED', 'Rs. 20000 - Rs. 50000', 'No'),
(4, 'APPROVED', 'Rs. 50000 - Rs. 150000', 'No'),
(5, 'APPROVED', 'Rs. 150000 - Rs. 250000', 'No'),
(6, 'APPROVED', 'Rs. 250000 - Rs. 500000', 'No'),
(7, 'APPROVED', 'Rs. 500000 - Rs. 1000000', 'No'),
(8, 'APPROVED', 'Above Rs. 1000000', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `send_bulk_email`
--

CREATE TABLE `send_bulk_email` (
  `id` int(11) NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email_subject` varchar(500) CHARACTER SET utf8 NOT NULL,
  `email_content` text CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shift_type`
--

CREATE TABLE `shift_type` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `shift_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift_type`
--

INSERT INTO `shift_type` (`id`, `status`, `shift_type`, `is_deleted`) VALUES
(1, 'APPROVED', 'Day', 'No'),
(2, 'APPROVED', 'Night', 'No'),
(3, 'APPROVED', 'Flexible', 'No'),
(4, 'APPROVED', 'N Demo Job Shift Type', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `site_config`
--

CREATE TABLE `site_config` (
  `id` enum('1') CHARACTER SET utf8 NOT NULL,
  `web_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `web_frienly_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `upload_logo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `contact_no` varchar(250) CHARACTER SET utf8 NOT NULL,
  `website_title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `website_description` text CHARACTER SET utf8 NOT NULL,
  `website_keywords` text CHARACTER SET utf8 NOT NULL,
  `upload_favicon` text CHARACTER SET utf8 NOT NULL,
  `google_analytics_code` text CHARACTER SET utf8 NOT NULL,
  `footer_text` text CHARACTER SET utf8 NOT NULL,
  `from_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `contact_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `job_prefix` varchar(255) CHARACTER SET utf8 NOT NULL,
  `facebook_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `twitter_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `linkedin_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `google_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `colour_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `default_currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_address` text CHARACTER SET utf8 NOT NULL,
  `service_tax` double NOT NULL,
  `map_address` text CHARACTER SET utf8 NOT NULL,
  `map_tooltip` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes- Deleted Data, No - Not Deleted data',
  `sms_api` text NOT NULL,
  `sms_api_status` enum('APPROVED','UNAPPROVED') NOT NULL DEFAULT 'APPROVED',
  `current_date_crone` date NOT NULL,
  `client_id` int(11) NOT NULL,
  `web_appkey` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_config`
--

INSERT INTO `site_config` (`id`, `web_name`, `web_frienly_name`, `upload_logo`, `contact_no`, `website_title`, `website_description`, `website_keywords`, `upload_favicon`, `google_analytics_code`, `footer_text`, `from_email`, `contact_email`, `job_prefix`, `facebook_link`, `twitter_link`, `linkedin_link`, `google_link`, `colour_name`, `default_currency`, `full_address`, `service_tax`, `map_address`, `map_tooltip`, `is_deleted`, `sms_api`, `sms_api_status`, `current_date_crone`, `client_id`, `web_appkey`) VALUES
('1', 'http://192.168.1.111/job_portal/', 'Job Portal', '587a94999f620db9e3ceaf91e5147d5d.png', '9988774455', 'Job Portal title', 'job portal Website Description', 'Free Recruitment, Recruiters, Job Seeker, HR, Opportunity, Resourcing, Job Board, Human Resources, Organization hiring, Job ads, Job Advertisement, Job Portal, India Job Portal, Out Of Job Seekers', '3636cddfaa12df0cb6178954b2f16ce2.png', 'Google Analytics Code', 'Copyright 2016-2017 by Job Portal. All Rights Reserved.', 'info@narjisinfotechdesigners.com', 'info@narjisinfotechdesigners.com', 'job portal', 'http://www.facebook.com', 'http://www.twitter.com', 'https://in.linkedin.com/', 'https://plus.google.com', '#008000', 'INR', 'C/319, Siddhivinayak Tower, Near Kataria Automobile, \r\nBehind BMW Show Room, Near Makarba, Sarkhej, Ahmedabad, Gujarat 380051', 0, 'Siddhivinayak Tower, Sarkhej, Ahmedabad', '<strong>Narjis Infotech</strong><br>318 Siddhivinayak tower </br>BMW motors', 'No', 'http://dnd.saakshisoftware.com/api/mt/SendSMS?user=7skyee&password=k@7skyee&senderid=KNOCKJ&channel=trans&DCS=0&flashsms=0&number=##contacts##&text=##sms_text##&route=15', 'APPROVED', '2019-03-26', 375, '2bc69bc43349d5c3d7c50f4b7ca23531');

-- --------------------------------------------------------

--
-- Table structure for table `skill_language_master`
--

CREATE TABLE `skill_language_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `skill_language` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill_language_master`
--

INSERT INTO `skill_language_master` (`id`, `status`, `skill_language`, `is_deleted`) VALUES
(1, 'APPROVED', 'Gujarati', 'No'),
(2, 'APPROVED', 'English', 'No'),
(3, 'APPROVED', 'Hindi', 'No'),
(4, 'APPROVED', 'German - deutsch', 'No'),
(5, 'APPROVED', 'Russian', 'No'),
(6, 'APPROVED', 'Polish', 'No'),
(7, 'APPROVED', 'Ukrainian', 'No'),
(8, 'APPROVED', 'Spanish - Español', 'No'),
(9, 'APPROVED', 'Portuguese - Portuguese', 'No'),
(10, 'APPROVED', 'French - français', 'No'),
(11, 'APPROVED', 'Italian - italiano', 'No'),
(12, 'APPROVED', 'Romanian', 'No'),
(13, 'APPROVED', 'Greek', 'No'),
(14, 'APPROVED', 'Hungarian', 'No'),
(15, 'APPROVED', 'Chinese', 'No'),
(16, 'APPROVED', 'Japanese', 'No'),
(17, 'APPROVED', 'Punjabi', 'No'),
(18, 'APPROVED', 'Tamil', 'No'),
(19, 'APPROVED', 'Telugu', 'No'),
(20, 'APPROVED', 'Kannada', 'No'),
(21, 'APPROVED', 'Malayalam', 'No'),
(22, 'APPROVED', 'Marathi', 'No'),
(23, 'APPROVED', 'Konkani', 'No'),
(24, 'APPROVED', 'Bengali', 'No'),
(25, 'APPROVED', 'Sindhi', 'No'),
(26, 'APPROVED', 'Urdu', 'No'),
(27, 'APPROVED', 'Arabic', 'No'),
(28, 'APPROVED', 'Sanskrit', 'No'),
(29, 'APPROVED', 'Persian', 'No'),
(30, 'APPROVED', 'N Demo Language', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `skill_level_master`
--

CREATE TABLE `skill_level_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `skill_level` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill_level_master`
--

INSERT INTO `skill_level_master` (`id`, `status`, `skill_level`, `is_deleted`) VALUES
(1, 'APPROVED', 'Very poor', 'No'),
(2, 'APPROVED', 'Poor', 'No'),
(3, 'APPROVED', 'Fair', 'No'),
(4, 'APPROVED', 'Expert', 'No'),
(5, 'APPROVED', 'Average', 'No'),
(6, 'APPROVED', 'N Demo Skill', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `sms_templates`
--

CREATE TABLE `sms_templates` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `template_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sms_content` text CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_templates`
--

INSERT INTO `sms_templates` (`id`, `status`, `template_name`, `sms_content`, `is_deleted`) VALUES
(1, 'APPROVED', 'Registration', 'You have successfully registered on #web_name#', 'No'),
(2, 'APPROVED', 'Welcome Message', 'જોબ પોર્ટલ વેબસાઇટ પર આપનું સ્વાગત છે', 'No'),
(3, 'APPROVED', 'Account activated', 'Your account has been activated, your login id is xxxx', 'No'),
(4, 'APPROVED', 'Employer account activated', 'Your account as an employer has been activated, your login id is xxxx', 'No'),
(5, 'APPROVED', 'Registration Employer', 'Congratulations ! You have successfully registered as an employer on #web_name# .Activate your account thorough email and login with the same email id.', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `social_login_master`
--

CREATE TABLE `social_login_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL DEFAULT 'UNAPPROVED',
  `social_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `client_key` text CHARACTER SET utf8 NOT NULL,
  `client_secret` text CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_login_master`
--

INSERT INTO `social_login_master` (`id`, `status`, `social_name`, `client_key`, `client_secret`, `is_deleted`) VALUES
(1, 'APPROVED', 'facebook', '624116227785293', 'ab13a4139e217fb7afbe504797f8f8ba', 'No'),
(2, 'APPROVED', 'google', '18769205332-3pdbdf0kbu5oj8q8fusd59tqq8s6vf9d.apps.googleusercontent.com', 'm2ef2dviPoL8N_CL4f59FK_f', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `state_master`
--

CREATE TABLE `state_master` (
  `id` int(11) NOT NULL,
  `status` enum('APPROVED','UNAPPROVED') CHARACTER SET utf8 NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `is_deleted` enum('Yes','No') CHARACTER SET utf8 NOT NULL DEFAULT 'No' COMMENT 'Yes - Deleted data, No - Not deleted data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state_master`
--

INSERT INTO `state_master` (`id`, `status`, `country_id`, `state_name`, `is_deleted`) VALUES
(1, 'APPROVED', 1, 'Gujarat', 'No'),
(2, 'APPROVED', 1, 'Punjab', 'No'),
(4, 'APPROVED', 3, 'New South Wales', 'No'),
(5, 'APPROVED', 4, 'Dubai', 'No'),
(6, 'APPROVED', 3, 'Queensland', 'No'),
(7, 'APPROVED', 3, 'South Australia', 'No'),
(8, 'APPROVED', 3, 'Tasmania', 'No'),
(9, 'APPROVED', 3, 'Victoria', 'No'),
(10, 'APPROVED', 3, 'Western Australia', 'No'),
(11, 'APPROVED', 5, 'Ontario', 'No'),
(12, 'APPROVED', 5, 'Alberta', 'No'),
(13, 'APPROVED', 1, 'Andhra Pradesh', 'No'),
(14, 'APPROVED', 1, 'Arunachal Pradesh', 'No'),
(15, 'APPROVED', 1, 'Assam', 'No'),
(16, 'APPROVED', 1, 'Bihar', 'No'),
(17, 'APPROVED', 1, 'Chandigarh', 'No'),
(18, 'APPROVED', 1, 'Chhattisgarh', 'No'),
(19, 'APPROVED', 1, 'National Capital Territory of Delhi', 'No'),
(20, 'APPROVED', 1, 'Goa', 'No'),
(21, 'APPROVED', 1, 'Haryana', 'No'),
(22, 'APPROVED', 1, 'Himachal Pradesh', 'No'),
(23, 'APPROVED', 1, 'Jammu and Kashmir', 'No'),
(24, 'APPROVED', 1, 'Jharkhand', 'No'),
(25, 'APPROVED', 1, 'Karnataka', 'No'),
(26, 'APPROVED', 1, 'Kerala', 'No'),
(27, 'APPROVED', 1, 'Lakshadweep', 'No'),
(28, 'APPROVED', 1, 'Madhya Pradesh', 'No'),
(29, 'APPROVED', 1, 'Maharashtra', 'No'),
(30, 'APPROVED', 1, 'Manipur', 'No'),
(31, 'APPROVED', 1, 'Meghalaya', 'No'),
(32, 'APPROVED', 1, 'Mizoram', 'No'),
(33, 'APPROVED', 1, 'Nagaland', 'No'),
(34, 'APPROVED', 1, 'Odisha', 'No'),
(35, 'APPROVED', 1, 'Puducherry', 'No'),
(36, 'APPROVED', 1, 'Rajasthan', 'No'),
(37, 'APPROVED', 1, 'Sikkim', 'No'),
(38, 'APPROVED', 1, 'Tamil Nadu', 'No'),
(39, 'APPROVED', 1, 'Telangana', 'No'),
(40, 'APPROVED', 1, 'Tripura', 'No'),
(41, 'APPROVED', 1, 'Uttar Pradesh', 'No'),
(42, 'APPROVED', 1, 'Uttarakhand', 'No'),
(43, 'APPROVED', 1, 'West Bengal', 'No'),
(44, 'APPROVED', 4, 'Abu Dhabi', 'No'),
(45, 'APPROVED', 4, 'Sharjah', 'No'),
(46, 'APPROVED', 4, 'Ajman', 'No'),
(47, 'APPROVED', 4, 'Ras Al Khaimah', 'No'),
(48, 'APPROVED', 4, 'Fujairah', 'No'),
(49, 'APPROVED', 4, 'Umm al-Quwain', 'No'),
(50, 'APPROVED', 8, 'Auckland', 'No'),
(51, 'APPROVED', 8, 'New Plymouth', 'No'),
(52, 'APPROVED', 8, 'Hawke\'s Bay', 'No'),
(53, 'APPROVED', 8, 'Wellington', 'No'),
(54, 'APPROVED', 8, 'Nelson', 'No'),
(55, 'APPROVED', 8, 'Marlborough', 'No'),
(56, 'APPROVED', 8, 'Westland', 'No'),
(57, 'APPROVED', 8, 'Canterbury', 'No'),
(58, 'APPROVED', 8, 'Otago', 'No'),
(59, 'APPROVED', 8, 'Southland', 'No'),
(60, 'APPROVED', 2, 'Balochistan', 'No'),
(61, 'APPROVED', 2, 'Islamabad Capital Territory', 'No'),
(62, 'APPROVED', 2, 'Khyber-Pakhtunkhwa', 'No'),
(63, 'APPROVED', 2, 'Punjab', 'No'),
(64, 'APPROVED', 2, 'Sindh', 'No'),
(65, 'APPROVED', 9, 'Eastern Cape', 'No'),
(66, 'APPROVED', 9, 'Free State', 'No'),
(67, 'APPROVED', 9, 'Gauteng', 'No'),
(68, 'APPROVED', 9, 'KwaZulu-Natal', 'No'),
(69, 'APPROVED', 9, 'Limpopo', 'No'),
(70, 'APPROVED', 9, 'Mpumalanga', 'No'),
(71, 'APPROVED', 9, 'North West', 'No'),
(72, 'APPROVED', 9, 'Northern Cape', 'No'),
(73, 'APPROVED', 9, 'Western Cape', 'No'),
(74, 'APPROVED', 7, 'England', 'No'),
(75, 'APPROVED', 7, 'Northern Ireland', 'No'),
(76, 'APPROVED', 7, 'Scotland', 'No'),
(77, 'APPROVED', 7, 'Wales', 'No'),
(78, 'APPROVED', 6, 'Alabama', 'No'),
(79, 'APPROVED', 6, 'Alaska', 'No'),
(80, 'APPROVED', 6, 'Arizona', 'No'),
(81, 'APPROVED', 6, 'Arkansas', 'No'),
(82, 'APPROVED', 6, 'California', 'No'),
(83, 'APPROVED', 6, 'Colorado', 'No'),
(84, 'APPROVED', 6, 'Illinois', 'No'),
(85, 'APPROVED', 6, 'Massachusetts', 'No'),
(86, 'APPROVED', 6, 'Michigan', 'No'),
(87, 'APPROVED', 6, 'New York', 'No'),
(88, 'APPROVED', 6, 'Ohio', 'No'),
(89, 'APPROVED', 6, 'Pennsylvania', 'No'),
(90, 'APPROVED', 6, 'Tennessee', 'No'),
(91, 'APPROVED', 6, 'Washington', 'No'),
(92, 'UNAPPROVED', 1, '34', 'Yes'),
(93, 'APPROVED', 26, 'z', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_history_reply`
--

CREATE TABLE `ticket_history_reply` (
  `id` mediumint(9) NOT NULL,
  `ticket_number` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` enum('A','C','S','','') NOT NULL COMMENT 'A- Admin,C- Client, S - Staff',
  `comment` text NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_oppsite` enum('Pending','Updated') NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_table`
--

CREATE TABLE `ticket_table` (
  `id` int(11) NOT NULL,
  `status` enum('Open','Resolve','Close','Reopen') NOT NULL DEFAULT 'Open',
  `ticket_number` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `subject` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `attachment_1` varchar(255) NOT NULL,
  `attachment_2` varchar(255) NOT NULL,
  `attachment_3` varchar(255) NOT NULL,
  `is_deleted` enum('YES','NO','','') NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_service`
--

CREATE TABLE `web_service` (
  `id` int(11) NOT NULL,
  `status` enum('UNAPPROVED','APPROVED') NOT NULL DEFAULT 'APPROVED',
  `service_name` varchar(255) NOT NULL,
  `service_url` varchar(255) NOT NULL,
  `method` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `perameter` text NOT NULL,
  `success_response` text NOT NULL,
  `error_response` text NOT NULL,
  `is_deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_service`
--

INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(1, 'APPROVED', 'Get Tocken', 'http://192.168.1.111/job_portal/common_request/get_tocken', 'GET', '<p>Call this api to get security token required to further</p>\r\n', '<p>None(No Parameters required)</p>\r\n', '<p>{&quot;tocken&quot;:&quot;c49c0c553f709c20e6ca787313fdcca0&quot;,&quot;status&quot;:&quot;success&quot;}</p>\r\n', '', 'No'),
(2, 'APPROVED', 'User Login', 'http://192.168.1.111/job_portal/login/login', 'POST', '<p>User login is required following actions</p>', '<p>action= login</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>password</p>\r\n\r\n<p>rememberme= Yes</p>\r\n\r\n<p>csrf_job_portal</p>', '<p>{<br>\r\n  \"token\": \"c49c0c553f709c20e6ca787313fdcca0\",<br>\r\n  \"user_agent\": \"NI-AAPP\",<br>\r\n  \"errmessage\": \"Custom success msg\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '<p>{</p>\r\n\r\n<p>“status” : error,</p>\r\n\r\n<p>\" errmessage\": custom error msg,</p>\r\n\r\n<p>}</p>', 'No'),
(3, 'APPROVED', 'User Account details Register', 'http://192.168.1.111/job_portal /sign_up/add_account_detail', 'POST', '<p style=\"margin-left:4.0pt\">1.User register with fullname , email and password,&nbsp; after add this details redirect to second register form.</p>\r\n\r\n<p style=\"margin-left:4.0pt\">User registration is required following actions</p>\r\n', '<p>action= sign_up<br />\r\nuser_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal</p>\r\n\r\n<p>fullname<br />\r\nemail<br />\r\npassword<br />\r\nc_password<br />\r\n&nbsp;</p>\r\n', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\r\n&quot;token&quot;:&quot;2ae0d20d1507712bef1776d6a71c336b&quot;,\r\n&quot;errmessage&quot;:&quot;Account details added successfully.&quot;</code><code>,\r\n&quot;status&quot;:&quot;success&quot;,\r\n&quot;reg_index_id&quot;:&quot;55&quot;\r\n}</code></pre>\r\n</div>\r\n', '<p>{<br />\r\n&ldquo;status&rdquo; : error,<br />\r\n&quot;errmessage&quot;: custom error msg,<br />\r\n}</p>\r\n', 'No'),
(4, 'APPROVED', 'Contact us api ', 'http://192.168.1.111/job_portal/common_request/get_site_data', 'POST', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;,<br />\r\n&nbsp; &quot;config_data&quot;: {<br />\r\n&nbsp; &nbsp; &quot;id&quot;: &quot;1&quot;,<br />\r\n&nbsp; &nbsp; &quot;web_name&quot;: &quot;http://192.168.1.111/job_portal/&quot;,<br />\r\n&nbsp; &nbsp; &quot;web_frienly_name&quot;: &quot;Job Portal&quot;,<br />\r\n&nbsp; &nbsp; &quot;upload_logo&quot;: &quot;5400bb93bb3e5a3e76f82b3e288d88bb.png&quot;,<br />\r\n&nbsp; &nbsp; &quot;contact_no&quot;: &quot;9988774455&quot;,<br />\r\n&nbsp; &nbsp; &quot;website_title&quot;: &quot;Job Portal&quot;,<br />\r\n&nbsp; &nbsp; &quot;website_description&quot;: &quot;Job Portal Website Description&quot;,<br />\r\n&nbsp; &nbsp; &quot;website_keywords&quot;: &quot;Job Portal Website Keywords&quot;,<br />\r\n&nbsp; &nbsp; &quot;upload_favicon&quot;: &quot;8e0ac830411592305d08cc6c475931c0.png&quot;,<br />\r\n&nbsp; &nbsp; &quot;google_analytics_code&quot;: &quot;google anlytic code&quot;,<br />\r\n&nbsp; &nbsp; &quot;footer_text&quot;: &quot;Job Portal 2017&quot;,<br />\r\n&nbsp; &nbsp; &quot;from_email&quot;: &quot;info@narjisenterprise.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;to_email&quot;: &quot;info@marriagedestiny.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;feedback_email&quot;: &quot;info@narjisenterprise.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;contact_email&quot;: &quot;narjisenterprise@gmail.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;job_prefix&quot;: &quot;job portal&quot;,<br />\r\n&nbsp; &nbsp; &quot;facebook_link&quot;: &quot;http://www.facebook.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;twitter_link&quot;: &quot;http://www.twitter.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;linkedin_link&quot;: &quot;https://in.linkedin.com/&quot;,<br />\r\n&nbsp; &nbsp; &quot;google_link&quot;: &quot;https://plus.google.com&quot;,<br />\r\n&nbsp; &nbsp; &quot;colour_name&quot;: &quot;#008080&quot;,<br />\r\n&nbsp; &nbsp; &quot;default_currency&quot;: &quot;$&quot;,<br />\r\n&nbsp; &nbsp; &quot;full_address&quot;: &quot;C/319, Siddhivinayak Tower, Near Kataria Automobile, \\r\\nBehind BMW Show Room, Near Makarba, Sarkhej, Ahmedabad, Gujarat 380051&quot;,<br />\r\n&nbsp; &nbsp; &quot;map_address&quot;: &quot;Ahmedabad ,Vasna&quot;,<br />\r\n&nbsp; &nbsp; &quot;map_tooltip&quot;: &quot;&lt;strong&gt;Our Office&lt;/strong&gt;&lt;br&gt;318 Siddhivinayak tower &lt;/br&gt;BMW motors&quot;,<br />\r\n&nbsp; &nbsp; &quot;is_deleted&quot;: &quot;No&quot;<br />\r\n&nbsp; }<br />\r\n}</p>\r\n', '<p>{<br />\r\n&ldquo;status&rdquo; : error,<br />\r\n&quot; errmessage&quot;: custom error msg,<br />\r\n}</p>\r\n', 'No'),
(5, 'APPROVED', 'Contact us form', 'http://192.168.1.111/job_portal/contact/contact_mail', 'POST', '<p>Submit the contact us form</p>\r\n', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api<br />\r\nemail&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Valid email format<br />\r\nname&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<br />\r\nComment</p>\r\n\r\n<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\nemail&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Valid email format<br />\r\nname&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<br />\r\nComment<br />\r\n&nbsp;</p>\r\n', '<p><br />\r\n{<br />\r\n&nbsp; &nbsp;&quot;token&quot;:&quot;4e3793f8d996d0008863585d3ec2f607&quot;,<br />\r\n&nbsp; &nbsp;&quot;successmessage&quot;:&quot;Your query is send successfully&quot;,<br />\r\n&nbsp; &nbsp;&quot;status&quot;:&quot;success&quot;<br />\r\n}</p>\r\n', '<p>{<br />\r\n&nbsp; &nbsp;&quot;token&quot;:&quot;adc5942992a64ba1f5494ade1808f1fd&quot;,<br />\r\n&nbsp; &nbsp;&quot;errormessage&quot;:&quot;</p>\r\n\r\n<ul>\r\n	<li>The Email field must contain a valid email address.&lt;\\/li&gt;\\n&quot;,<br />\r\n	&nbsp; &nbsp;&quot;status&quot;:&quot;error&quot;<br />\r\n	}\r\n	<p>&nbsp;</p>\r\n	</li>\r\n</ul>\r\n', 'No'),
(6, 'APPROVED', 'Get All Blog', 'http://192.168.1.111/job_portal/blog/allblogs', 'POST', '<p>This api will give details of all blogs</p>\r\n', '<p>csrf_job_portal= &nbsp;token obtained from api 1.API mandatory token api<br />\r\nlimit_per_page&nbsp;&nbsp; &nbsp;= Data required to show per page<br />\r\npage =Page number<br />\r\nuser_agent = NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;total_records&quot;: 8,<br />\r\n&nbsp; &quot;data&quot;: [<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;8&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;status&quot;: &quot;APPROVED&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;title&quot;: &quot;Letest &nbsp;blog 2&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;alias&quot;: &quot;letest-blog-2&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;content&quot;: &quot;&lt;p&gt;Just for test&lt;/p&gt;\\r\\n&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;created_on&quot;: &quot;2017-03-14 06:09:54&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;blog_image&quot;: &quot;http://192.168.1.111/job_portal/assets/blog_image/ae9f32e86bb5bc06a6ac2c0813c98e1e.png&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;is_deleted&quot;: &quot;No&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;7&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;status&quot;: &quot;APPROVED&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;title&quot;: &quot;Letest &nbsp;blog&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;alias&quot;: &quot;letest-blog&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;content&quot;: &quot;&lt;p&gt;Letest &amp;nbsp;blog&lt;/p&gt;\\r\\n&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;created_on&quot;: &quot;2017-03-14 06:02:05&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;blog_image&quot;: &quot;&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;is_deleted&quot;: &quot;No&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; ],<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;<br />\r\n}</p>\r\n', '', 'No'),
(7, 'APPROVED', 'Get Country Listing', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal<br />\r\nget_list = country_list</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;,<br />\r\n&nbsp; &quot;status&quot;: &quot;success&quot;,<br />\r\n&nbsp; &quot;data&quot;: [<br />\r\n&nbsp; &nbsp; &quot;Select option&quot;,<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 3,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Australia&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 14,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Australias&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 1,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;India&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; ]<br />\r\n}</p>\r\n', '', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(8, 'APPROVED', 'Get City Listing', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)<br>\r\ncsrf_job_portal</p>\r\n\r\n<p>currnet_val</p>\r\n\r\n<p>get_list = city_list</p>', '<p>{<br>\r\n  \"tocken\": \"7f44ca129ab253ffd67bfd165a2d4e8a\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": [<br>\r\n    {<br>\r\n      \"state\": \"Abu Dhabi\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 170,<br>\r\n          \"val\": \"Abu Dhabi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 176,<br>\r\n          \"val\": \"Al Ain\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 180,<br>\r\n          \"val\": \"Liwa\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 181,<br>\r\n          \"val\": \"Zayed City\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Ajman\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 172,<br>\r\n          \"val\": \"Ajman\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Alabama\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 264,<br>\r\n          \"val\": \"Birmingham\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 267,<br>\r\n          \"val\": \"Huntsville\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 266,<br>\r\n          \"val\": \"Mobile\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 265,<br>\r\n          \"val\": \"Montgomery\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 268,<br>\r\n          \"val\": \"Tuscaloosa\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Alaska\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 269,<br>\r\n          \"val\": \"Anchorage\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 270,<br>\r\n          \"val\": \"Fairbanks\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 271,<br>\r\n          \"val\": \"Juneau\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 272,<br>\r\n          \"val\": \"Ketchikan\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Alberta\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 16,<br>\r\n          \"val\": \"Airdrie\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 17,<br>\r\n          \"val\": \"Camrose\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Andhra Pradesh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 71,<br>\r\n          \"val\": \"Guntur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 125,<br>\r\n          \"val\": \"Kurnool\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 96,<br>\r\n          \"val\": \"Nellore\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 127,<br>\r\n          \"val\": \"Rajahmundry\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 140,<br>\r\n          \"val\": \"Tirupati\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 64,<br>\r\n          \"val\": \"Vijayawada\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 37,<br>\r\n          \"val\": \"Visakhapatnam\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Arizona\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 276,<br>\r\n          \"val\": \"Glendale\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 275,<br>\r\n          \"val\": \"Mesa\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 273,<br>\r\n          \"val\": \"Phoenix\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 274,<br>\r\n          \"val\": \"Tucson\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Arkansas\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 279,<br>\r\n          \"val\": \"Fayetteville\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 278,<br>\r\n          \"val\": \"Fort Smith\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 277,<br>\r\n          \"val\": \"Little Rock\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Arunachal Pradesh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 161,<br>\r\n          \"val\": \"Itanagar\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Assam\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 162,<br>\r\n          \"val\": \"Dispur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 68,<br>\r\n          \"val\": \"Guwahati\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Auckland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 184,<br>\r\n          \"val\": \"Auckland\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 185,<br>\r\n          \"val\": \"Christchurch\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Balochistan\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 196,<br>\r\n          \"val\": \"Quetta\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 197,<br>\r\n          \"val\": \"Turbat\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Bihar\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 155,<br>\r\n          \"val\": \"Begusarai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 134,<br>\r\n          \"val\": \"Bhagalpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 150,<br>\r\n          \"val\": \"Darbhanga\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 118,<br>\r\n          \"val\": \"Gaya\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 42,<br>\r\n          \"val\": \"Patna\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"California\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 280,<br>\r\n          \"val\": \"Los Angeles\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 281,<br>\r\n          \"val\": \"San Diego\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 283,<br>\r\n          \"val\": \"San Francisco\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 282,<br>\r\n          \"val\": \"San Jose\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Canterbury\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 189,<br>\r\n          \"val\": \"Canterbury\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Chandigarh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 69,<br>\r\n          \"val\": \"Chandigarh\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Chhattisgarh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 72,<br>\r\n          \"val\": \"Bhilai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 145,<br>\r\n          \"val\": \"Bilaspur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 142,<br>\r\n          \"val\": \"Korba\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 66,<br>\r\n          \"val\": \"Raipur\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Colorado\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 286,<br>\r\n          \"val\": \"Aurora\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 285,<br>\r\n          \"val\": \"Colorado Springs\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 284,<br>\r\n          \"val\": \"Denver\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 287,<br>\r\n          \"val\": \"Fort Collins\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 288,<br>\r\n          \"val\": \"Lakewood\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Dubai\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 169,<br>\r\n          \"val\": \"Dubai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 182,<br>\r\n          \"val\": \"Hatta\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 179,<br>\r\n          \"val\": \"Jebel Ali\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Eastern Cape\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 211,<br>\r\n          \"val\": \"East London\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 212,<br>\r\n          \"val\": \"Port Elizabeth\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 213,<br>\r\n          \"val\": \"Queenstown\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"England\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 237,<br>\r\n          \"val\": \"Birmingham\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 239,<br>\r\n          \"val\": \"Bristol\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 260,<br>\r\n          \"val\": \"Cambridge\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 263,<br>\r\n          \"val\": \"Kingston\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 242,<br>\r\n          \"val\": \"Leeds\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 243,<br>\r\n          \"val\": \"Leicester\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 238,<br>\r\n          \"val\": \"Liverpool\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 236,<br>\r\n          \"val\": \"London\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 240,<br>\r\n          \"val\": \"Manchester\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 262,<br>\r\n          \"val\": \"Nottingham\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 241,<br>\r\n          \"val\": \"Sheffield\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 261,<br>\r\n          \"val\": \"York\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Free State\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 214,<br>\r\n          \"val\": \"Bethlehem\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 215,<br>\r\n          \"val\": \"Virginia\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Fujairah\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 177,<br>\r\n          \"val\": \"Dibba\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 173,<br>\r\n          \"val\": \"Fujairah\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Gauteng\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 216,<br>\r\n          \"val\": \"Johannesburg\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 217,<br>\r\n          \"val\": \"Randburg\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 218,<br>\r\n          \"val\": \"Randfontein\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Goa\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 163,<br>\r\n          \"val\": \"Panaji\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Gujarat\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 1,<br>\r\n          \"val\": \"Ahmedabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 23,<br>\r\n          \"val\": \"Bhavnagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 156,<br>\r\n          \"val\": \"Gandhidham\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 24,<br>\r\n          \"val\": \"Gandhinagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 21,<br>\r\n          \"val\": \"Jamnagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 26,<br>\r\n          \"val\": \"Kutch\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 25,<br>\r\n          \"val\": \"Mehsana\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 22,<br>\r\n          \"val\": \"Rajkot\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 3,<br>\r\n          \"val\": \"Surat\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 27,<br>\r\n          \"val\": \"Surendranagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 2,<br>\r\n          \"val\": \"Vadodara\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 9,<br>\r\n          \"val\": \"Valsad\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Haryana\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 49,<br>\r\n          \"val\": \"Faridabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 73,<br>\r\n          \"val\": \"Gurgaon\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 149,<br>\r\n          \"val\": \"Panipat\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 141,<br>\r\n          \"val\": \"Rohtak\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 152,<br>\r\n          \"val\": \"Sonipat\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Hawke&#39;s Bay\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 190,<br>\r\n          \"val\": \"Hawke&#39;s Bay\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Himachal Pradesh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 160,<br>\r\n          \"val\": \"Shimla\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Illinois\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 290,<br>\r\n          \"val\": \"Aurora\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 289,<br>\r\n          \"val\": \"Chicago\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 292,<br>\r\n          \"val\": \"Joliet\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 293,<br>\r\n          \"val\": \"Naperville\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 291,<br>\r\n          \"val\": \"Rockford\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Islamabad Capital Territory\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 198,<br>\r\n          \"val\": \"Islamabad\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Jammu and Kashmir\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 110,<br>\r\n          \"val\": \"Jammu\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 54,<br>\r\n          \"val\": \"Srinagar\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Jharkhand\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 128,<br>\r\n          \"val\": \"Bokaro\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 56,<br>\r\n          \"val\": \"Dhanbad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 74,<br>\r\n          \"val\": \"Jamshedpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 60,<br>\r\n          \"val\": \"Ranchi\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Karnataka\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 114,<br>\r\n          \"val\": \"Belgaum\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 130,<br>\r\n          \"val\": \"Bellary\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 29,<br>\r\n          \"val\": \"Bengaluru\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 147,<br>\r\n          \"val\": \"Bijapur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 122,<br>\r\n          \"val\": \"Davanagere\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 104,<br>\r\n          \"val\": \"Gulbarga\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 75,<br>\r\n          \"val\": \"Hubballi-Dharwad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 112,<br>\r\n          \"val\": \"Mangalore\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 76,<br>\r\n          \"val\": \"Mysore\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Kerala\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 77,<br>\r\n          \"val\": \"Kochi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 123,<br>\r\n          \"val\": \"Kozhikode\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 70,<br>\r\n          \"val\": \"Thiruvananthapuram\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Khyber-Pakhtunkhwa\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 200,<br>\r\n          \"val\": \"Abbottabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 201,<br>\r\n          \"val\": \"Dera Ismail Khan\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 199,<br>\r\n          \"val\": \"Peshawar\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"KwaZulu-Natal\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 219,<br>\r\n          \"val\": \"Durban\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 220,<br>\r\n          \"val\": \"Pietermaritzburg\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 221,<br>\r\n          \"val\": \"Pinetown\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Lakshadweep\",<br>\r\n      \"city\": []<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Limpopo\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 222,<br>\r\n          \"val\": \"Giyani\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 223,<br>\r\n          \"val\": \"Phalaborwa\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Madhya Pradesh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 40,<br>\r\n          \"val\": \"Bhopal\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 63,<br>\r\n          \"val\": \"Gwalior\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 38,<br>\r\n          \"val\": \"Indore\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 62,<br>\r\n          \"val\": \"Jabalpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 153,<br>\r\n          \"val\": \"Ratlam\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 105,<br>\r\n          \"val\": \"Ujjain\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Maharashtra\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 124,<br>\r\n          \"val\": \"Akola\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 81,<br>\r\n          \"val\": \"Amravati\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 55,<br>\r\n          \"val\": \"Aurangabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 80,<br>\r\n          \"val\": \"Bhiwandi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 139,<br>\r\n          \"val\": \"Dhule\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 119,<br>\r\n          \"val\": \"Jalgaon\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 51,<br>\r\n          \"val\": \"Kalyan-Dombivali\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 102,<br>\r\n          \"val\": \"Kolhapur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 138,<br>\r\n          \"val\": \"Latur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 117,<br>\r\n          \"val\": \"Malegaon\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 79,<br>\r\n          \"val\": \"Mira-Bhayandar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 4,<br>\r\n          \"val\": \"Mumbai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 36,<br>\r\n          \"val\": \"Nagpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 101,<br>\r\n          \"val\": \"Nanded\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 48,<br>\r\n          \"val\": \"Nashik\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 58,<br>\r\n          \"val\": \"Navi Mumbai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 41,<br>\r\n          \"val\": \"Pimpri-Chinchwad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 5,<br>\r\n          \"val\": \"Pune\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 111,<br>\r\n          \"val\": \"Sangli-Miraj & Kupwad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 146,<br>\r\n          \"val\": \"Satara\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 78,<br>\r\n          \"val\": \"Solapur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 39,<br>\r\n          \"val\": \"Thane\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 109,<br>\r\n          \"val\": \"Ulhasnagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 52,<br>\r\n          \"val\": \"Vasai-Virar\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Manipur\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 165,<br>\r\n          \"val\": \"Imphal\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Marlborough\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 191,<br>\r\n          \"val\": \"Marlborough\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Massachusetts\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 294,<br>\r\n          \"val\": \"Boston\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 298,<br>\r\n          \"val\": \"Cambridge\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 297,<br>\r\n          \"val\": \"Lowell\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 296,<br>\r\n          \"val\": \"Springfield\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 295,<br>\r\n          \"val\": \"Worcester\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Meghalaya\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 164,<br>\r\n          \"val\": \"Shillong\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Michigan\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 303,<br>\r\n          \"val\": \"Ann Arbor\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 299,<br>\r\n          \"val\": \"Detroit\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 300,<br>\r\n          \"val\": \"Grand Rapids\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 302,<br>\r\n          \"val\": \"Sterling Heights\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 301,<br>\r\n          \"val\": \"Warren\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Mizoram\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 166,<br>\r\n          \"val\": \"Aizawl\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Mpumalanga\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 224,<br>\r\n          \"val\": \"Emalahleni\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 225,<br>\r\n          \"val\": \"Secunda\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Nagaland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 167,<br>\r\n          \"val\": \"Kohima\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"National Capital Territory of Delhi\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 28,<br>\r\n          \"val\": \"Delhi\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Nelson\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 192,<br>\r\n          \"val\": \"Nelson\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"New Plymouth\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 193,<br>\r\n          \"val\": \"New Plymouth\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"New South Wales\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 8,<br>\r\n          \"val\": \"Sydney\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 10,<br>\r\n          \"val\": \"Wollongong\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"New York\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 305,<br>\r\n          \"val\": \"Buffalo\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 304,<br>\r\n          \"val\": \"New York City\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 306,<br>\r\n          \"val\": \"Rochester\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 308,<br>\r\n          \"val\": \"Syracuse\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 307,<br>\r\n          \"val\": \"Yonkers\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"North West\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 226,<br>\r\n          \"val\": \"Klerksdorp\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 227,<br>\r\n          \"val\": \"Mahikeng\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 228,<br>\r\n          \"val\": \"Potchefstroom\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Northern Cape\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 229,<br>\r\n          \"val\": \"Kimberley\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 230,<br>\r\n          \"val\": \"Port Nolloth\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Northern Ireland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 244,<br>\r\n          \"val\": \"Armagh\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 245,<br>\r\n          \"val\": \"Belfast\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 247,<br>\r\n          \"val\": \"Lisburn\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 246,<br>\r\n          \"val\": \"Londonderry\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 248,<br>\r\n          \"val\": \"Newry\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Odisha\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 144,<br>\r\n          \"val\": \"Berhampur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 82,<br>\r\n          \"val\": \"Bhubaneswar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 83,<br>\r\n          \"val\": \"Cuttack\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 100,<br>\r\n          \"val\": \"Rourkela\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Ohio\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 310,<br>\r\n          \"val\": \"Cincinnati\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 309,<br>\r\n          \"val\": \"Cleveland\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 311,<br>\r\n          \"val\": \"Toledo\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Ontario\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 18,<br>\r\n          \"val\": \"Brampton\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 19,<br>\r\n          \"val\": \"Cambridge\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 20,<br>\r\n          \"val\": \"Mississauga\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Otago\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 194,<br>\r\n          \"val\": \"Otago\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Pennsylvania\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 314,<br>\r\n          \"val\": \"Allentown\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 315,<br>\r\n          \"val\": \"Erie\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 312,<br>\r\n          \"val\": \"Philadelphia\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 313,<br>\r\n          \"val\": \"Pittsburgh\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Puducherry\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 157,<br>\r\n          \"val\": \"Puducherry\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Punjab\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 57,<br>\r\n          \"val\": \"Amritsar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 151,<br>\r\n          \"val\": \"Bathinda\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 6,<br>\r\n          \"val\": \"Jalandhar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 44,<br>\r\n          \"val\": \"Ludhiana\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 131,<br>\r\n          \"val\": \"Patiala\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 7,<br>\r\n          \"val\": \"Punjab Kot\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Punjab\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 203,<br>\r\n          \"val\": \"Faisalabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 202,<br>\r\n          \"val\": \"Lahore\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 205,<br>\r\n          \"val\": \"Multan\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 204,<br>\r\n          \"val\": \"Rawalpindi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 206,<br>\r\n          \"val\": \"Sialkot\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Queensland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 11,<br>\r\n          \"val\": \"Brisbane\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Rajasthan\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 103,<br>\r\n          \"val\": \"Ajmer\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 148,<br>\r\n          \"val\": \"Alwar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 154,<br>\r\n          \"val\": \"Bharatpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 143,<br>\r\n          \"val\": \"Bhilwara\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 84,<br>\r\n          \"val\": \"Bikaner\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 33,<br>\r\n          \"val\": \"Jaipur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 65,<br>\r\n          \"val\": \"Jodhpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 67,<br>\r\n          \"val\": \"Kota\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 158,<br>\r\n          \"val\": \"Pali\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 120,<br>\r\n          \"val\": \"Udaipur\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Ras Al Khaimah\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 175,<br>\r\n          \"val\": \"Ras Al Khaimah\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Scotland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 249,<br>\r\n          \"val\": \"Aberdeen\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 250,<br>\r\n          \"val\": \"Dundee\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 251,<br>\r\n          \"val\": \"Edinburgh\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 252,<br>\r\n          \"val\": \"Glasgow\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 253,<br>\r\n          \"val\": \"Inverness\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 254,<br>\r\n          \"val\": \"Stirling\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Sharjah\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 178,<br>\r\n          \"val\": \"Khor Fakkan\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 171,<br>\r\n          \"val\": \"Sharjah\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Sikkim\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 168,<br>\r\n          \"val\": \"Gangtok\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Sindh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 208,<br>\r\n          \"val\": \"Hyderabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 210,<br>\r\n          \"val\": \"Jacobabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 207,<br>\r\n          \"val\": \"Karachi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 209,<br>\r\n          \"val\": \"Mirpur Khas\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"South Australia\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 12,<br>\r\n          \"val\": \"Adelaide\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Southland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 195,<br>\r\n          \"val\": \"Southland\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Tamil Nadu\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 115,<br>\r\n          \"val\": \"Ambattur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 31,<br>\r\n          \"val\": \"Chennai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 45,<br>\r\n          \"val\": \"Coimbatore\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 113,<br>\r\n          \"val\": \"Erode\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 47,<br>\r\n          \"val\": \"Madurai\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 87,<br>\r\n          \"val\": \"Salem\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 85,<br>\r\n          \"val\": \"Tiruchirappalli\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 116,<br>\r\n          \"val\": \"Tirunelveli\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 86,<br>\r\n          \"val\": \"Tiruppur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 159,<br>\r\n          \"val\": \"Vellore\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Tasmania\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 13,<br>\r\n          \"val\": \"Hobart\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Telangana\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 30,<br>\r\n          \"val\": \"Hyderabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 88,<br>\r\n          \"val\": \"Warangal\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Tennessee\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 319,<br>\r\n          \"val\": \"Chattanooga\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 318,<br>\r\n          \"val\": \"Knoxville\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 316,<br>\r\n          \"val\": \"Memphis\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 317,<br>\r\n          \"val\": \"Nashville\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Tripura\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 133,<br>\r\n          \"val\": \"Agartala\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Umm al-Quwain\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 183,<br>\r\n          \"val\": \"Al Salam City\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 174,<br>\r\n          \"val\": \"Umm al-Quwain\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Uttar Pradesh\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 46,<br>\r\n          \"val\": \"Agra\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 91,<br>\r\n          \"val\": \"Aligarh\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 59,<br>\r\n          \"val\": \"Allahabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 89,<br>\r\n          \"val\": \"Bareilly\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 95,<br>\r\n          \"val\": \"Firozabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 43,<br>\r\n          \"val\": \"Ghaziabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 93,<br>\r\n          \"val\": \"Gorakhpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 108,<br>\r\n          \"val\": \"Jhansi\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 35,<br>\r\n          \"val\": \"Kanpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 106,<br>\r\n          \"val\": \"Loni\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 34,<br>\r\n          \"val\": \"Lucknow\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 50,<br>\r\n          \"val\": \"Meerut\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 90,<br>\r\n          \"val\": \"Moradabad\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 135,<br>\r\n          \"val\": \"Muzaffarnagar\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 94,<br>\r\n          \"val\": \"Noida\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 92,<br>\r\n          \"val\": \"Saharanpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 53,<br>\r\n          \"val\": \"Varanasi\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Uttarakhand\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 97,<br>\r\n          \"val\": \"Dehradun\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Victoria\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 14,<br>\r\n          \"val\": \"Melbourne\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Wales\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 255,<br>\r\n          \"val\": \"Bangor\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 256,<br>\r\n          \"val\": \"Cardiff\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 257,<br>\r\n          \"val\": \"Newport\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 258,<br>\r\n          \"val\": \"St Davids\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 259,<br>\r\n          \"val\": \"Swansea\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Washington\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 320,<br>\r\n          \"val\": \"Seattle\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 321,<br>\r\n          \"val\": \"Spokane\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 322,<br>\r\n          \"val\": \"Tacoma\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 323,<br>\r\n          \"val\": \"Vancouver\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Wellington\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 187,<br>\r\n          \"val\": \"Hamilton\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 186,<br>\r\n          \"val\": \"Wellington\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"West Bengal\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 99,<br>\r\n          \"val\": \"Asansol\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 136,<br>\r\n          \"val\": \"Bhatpara\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 98,<br>\r\n          \"val\": \"Durgapur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 132,<br>\r\n          \"val\": \"Gopalpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 61,<br>\r\n          \"val\": \"Howrah\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 32,<br>\r\n          \"val\": \"Kolkata\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 121,<br>\r\n          \"val\": \"Maheshtala\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 137,<br>\r\n          \"val\": \"Panihati\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 126,<br>\r\n          \"val\": \"Rajpur Sonarpur\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 107,<br>\r\n          \"val\": \"Siliguri\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 129,<br>\r\n          \"val\": \"South Dumdum\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Western Australia\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 15,<br>\r\n          \"val\": \"Perth\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Western Cape\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 231,<br>\r\n          \"val\": \"Bellville\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 232,<br>\r\n          \"val\": \"Cape Town\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 233,<br>\r\n          \"val\": \"George\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 234,<br>\r\n          \"val\": \"Paarl\"<br>\r\n        },<br>\r\n        {<br>\r\n          \"id\": 235,<br>\r\n          \"val\": \"Worcester\"<br>\r\n        }<br>\r\n      ]<br>\r\n    },<br>\r\n    {<br>\r\n      \"state\": \"Westland\",<br>\r\n      \"city\": [<br>\r\n        {<br>\r\n          \"id\": 188,<br>\r\n          \"val\": \"Dunedin\"<br>\r\n        }<br>\r\n      ]<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(9, 'APPROVED', 'Get Marital Status Listing', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal<br />\r\nget_list = marital_list</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;,<br />\r\n&nbsp; &quot;status&quot;: &quot;success&quot;,<br />\r\n&nbsp; &quot;data&quot;: [<br />\r\n&nbsp; &nbsp; &quot;Select option&quot;,<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 4,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Divorce&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 2,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Married&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 5,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Seperated&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 1,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Test&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 3,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Unmarried&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; ]<br />\r\n}</p>\r\n', '', 'No'),
(10, 'APPROVED', 'Get Education Listing', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal<br />\r\nget_list = edu_list</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;,<br />\r\n&nbsp; &quot;status&quot;: &quot;success&quot;,<br />\r\n&nbsp; &quot;data&quot;: [<br />\r\n&nbsp; &nbsp; &quot;Select option&quot;,<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 1,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;10th pass&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 4,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;BSC&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 5,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;BSC&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 7,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Diploma&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 8,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;Graduate&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: 6,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;MBA&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; ]<br />\r\n}</p>\r\n', '', 'No'),
(11, 'APPROVED', 'Get Mobile Code', 'http://192.168.1.111/job_portal/common_request/get_mobile_code', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal</p>\r\n', '<p>{<br />\r\n&nbsp; &quot;tocken&quot;: &quot;c49c0c553f709c20e6ca787313fdcca0&quot;,<br />\r\n&nbsp; &quot;status&quot;: &quot;success&quot;,<br />\r\n&nbsp; &quot;data&quot;: [<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;+91&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;+91 (India)&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;+92&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;+92 (Pakistan)&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;+61&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;+61 (Australia)&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;+86&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;+86 (China)&quot;<br />\r\n&nbsp; &nbsp; },<br />\r\n&nbsp; &nbsp; {<br />\r\n&nbsp; &nbsp; &nbsp; &quot;id&quot;: &quot;+81&quot;,<br />\r\n&nbsp; &nbsp; &nbsp; &quot;val&quot;: &quot;+81 (Japan)&quot;<br />\r\n&nbsp; &nbsp; }<br />\r\n&nbsp; ]<br />\r\n}</p>\r\n', '', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(12, 'APPROVED', 'User Register  Personal details ', 'http://192.168.1.111/job_portal/sign_up/add_personal_detail', 'POST', '<p><span style=\"font-family:calibri,sans-serif; font-size:11.0pt\">1.User add his/her personal details .that can be store in resume and his/her profile.&nbsp; User registration is required following actions</span></p>\r\n', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:666px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"height:16px; width:434px\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:16px; width:434px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:17px; width:434px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n				<tbody>\r\n					<tr>\r\n						<td>action</td>\r\n						<td><code>add_personal_detail</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>address</td>\r\n						<td><code>safsgd</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>birthdate</td>\r\n						<td><code>2014-04-17</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>city</td>\r\n						<td><code>1</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>country</td>\r\n						<td><code>1</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>csrf_job_portal</td>\r\n						<td><code>2ae0d20d1507712bef1776d6a71c336b</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>gender</td>\r\n						<td><code>Male</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>marital_status</td>\r\n						<td><code>3</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>mobile</td>\r\n						<td><code>7878787878</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>mobile_c_code</td>\r\n						<td><code>+91</code></td>\r\n					</tr>\r\n					<tr>\r\n						<td>user_agent</td>\r\n						<td><code>NI-WEB</code></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<div class=\"netInfoResponseText netInfoText \">\r\n<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{&quot;token&quot;:&quot;2ae0d20d1507712bef1776d6a71c336b&quot;,&quot;errmessage&quot;:&quot;Personal details added successfully.&quot;,&quot;status&quot;</code><code>:&quot;success&quot;}</code></pre>\r\n</div>\r\n</div>\r\n', '<p>{<br />\r\n&ldquo;status&rdquo; : error,</p>\r\n\r\n<p>&quot;errmessage&quot;: custom error msg,</p>\r\n\r\n<p><span style=\"font-family:calibri,sans-serif; font-size:10.0pt\">&quot;tocken&quot;:&quot;e88bce04f91810f5be8a17a73af4d684&quot;,</span></p>\r\n\r\n<p>}</p>\r\n', 'No'),
(13, 'APPROVED', 'fgh', 'gg', 'POST', '', '', '', '', 'No'),
(14, 'APPROVED', 'GET comman dropdown list from table ', 'http://192.168.1.111/job_portal/common_request/get_data_from_tbl', 'POST', '<p>Get dynamic dropdown list from table.</p>\r\n\r\n<p><strong>Requirenment </strong></p>\r\n\r\n<p><strong>Which table data won&#39;t you get </strong></p>\r\n\r\n<p><strong>marital_status</strong> = for get list of&nbsp; marital status</p>\r\n\r\n<p><strong>industries_master</strong> = for get list of&nbsp; industry</p>\r\n\r\n<p><strong>functional_area_master</strong> =&nbsp; for get list of&nbsp; functional area</p>\r\n\r\n<p><strong>role_master</strong> =&nbsp; for get list of&nbsp; role master</p>\r\n\r\n<p>currency_master =&nbsp; for get list of&nbsp; currency type</p>\r\n\r\n<p><strong>Example</strong></p>\r\n\r\n<p><strong>Note : first function parameter is getting list of data</strong></p>\r\n\r\n<p><strong>http://192.168.1.111/job_portal/common_request/get_data_from_tbl/;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><strong>Which list you wont to get. see above description.</strong></p>\r\n', '<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{&quot;tocken&quot;:&quot;27b4f4585e2a996aba86a18c8499ad2c&quot;,&quot;status&quot;:&quot;success&quot;,&quot;data&quot;:[{&quot;id&quot;:&quot;1&quot;,&quot;status&quot;:&quot;APPROVED&quot;,&quot;marital_status&quot;:&quot;Test&quot;,&quot;is_deleted&quot;:&quot;No&quot;},{&quot;id&quot;:&quot;2&quot;,&quot;status&quot;:&quot;APPROVED&quot;,&quot;marital_status&quot;:&quot;Married&quot;,&quot;is_deleted&quot;:&quot;No&quot;},{&quot;id&quot;:&quot;3&quot;,&quot;status&quot;:&quot;APPROVED&quot;,&quot;marital_status&quot;:&quot;Unmarried&quot;,&quot;is_deleted&quot;:&quot;No&quot;},{&quot;id&quot;:&quot;4&quot;,&quot;status&quot;:&quot;APPROVED&quot;,&quot;marital_status&quot;:&quot;Divorce&quot;,&quot;is_deleted&quot;:&quot;No&quot;},{&quot;id&quot;:&quot;5&quot;,&quot;status&quot;:&quot;APPROVED&quot;,&quot;marital_status&quot;:&quot;Seperated&quot;,&quot;is_deleted&quot;:&quot;No&quot;}]}</span></p>\r\n', '', 'No'),
(15, 'APPROVED', 'User Register/resume education  details', 'http://192.168.1.111/job_portal/sign_up/add_education_detail', 'POST', '<p>1.User add his/her education details .that can be store in resume and his/her education details.  User registration is required following actions</p>\r\n\r\n<p> </p>\r\n\r\n<p>2. education added multiple by seeker.</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>Note : data of post variable are depend on number of education.</strong></p>', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <table cellpadding=\"0\" cellspacing=\"0\" class=\"netInfoPostParamsTable\">\r\n    <tbody>\r\n     <tr>\r\n      <td>action</td>\r\n      <td><code>add_education_detail</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>csrf_job_portal</td>\r\n      <td><code>2ae0d20d1507712bef1776d6a71c336b</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>facebook_url</td>\r\n      <td><code>facebook_url</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>gplus_url</td>\r\n      <td><code>gplus_url</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>institute1</td>\r\n      <td><code>G.P.G</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>linkedin_url</td>\r\n      <td><code>linkedin_url</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>marks1</td>\r\n      <td><code>A+</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>name_facebook_url</td>\r\n      <td><code>https://www.facebook.com/</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>name_gplus_url</td>\r\n      <td><code>https://www.google.co.in</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>name_linkedin_url</td>\r\n      <td><code>https://in.linkedin.com/</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>name_twitter_url</td>\r\n      <td><code>https://twitter.com/login</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>no_of_edu_row</td>\r\n      <td><code>1</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>passing_year1</td>\r\n      <td><code>2008</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>qualification_level1</td>\r\n      <td><code>5</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>specialization1</td>\r\n      <td><code>Com</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>twitter_url</td>\r\n      <td><code>twitter_url</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>user_agent</td>\r\n      <td><code>NI-WEB</code></td>\r\n     </tr>\r\n    </tbody>\r\n   </table>\r\n\r\n   <table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\" xss=removed>\r\n    <tbody>\r\n     <tr>\r\n      <td>cerificate_desc1</td>\r\n      <td><code>this is dummy certi.</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>cerificate_desc2</td>\r\n      <td><code>This is dummy certi 2.</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>cerificate_name1</td>\r\n      <td><code>ketan pharama tech test</code></td>\r\n     </tr>\r\n     <tr>\r\n      <td>cerificate_name2</td>\r\n      <td>\r\n      <p><code>pharma tech pvt ltd by kd 2</code></p>\r\n\r\n      <p> </p>\r\n      </td>\r\n     </tr>\r\n    </tbody>\r\n   </table>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\" xss=removed>\r\n <tbody>\r\n  <tr>\r\n   <td>passing_year_certi1</td>\r\n   <td><code>2011</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\" xss=removed>\r\n    <tbody>\r\n     <tr>\r\n      <td>no_of_certi_row</td>\r\n      <td><code>2</code></td>\r\n     </tr>\r\n    </tbody>\r\n   </table>\r\n   passing_year_certi2</td>\r\n   <td><code>2015</code></td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>', '<p><code>{ \"token\":\"2ae0d20d1507712bef1776d6a71c336b\", \"errmessage\":\"Education details added successfully.\", \"status\"</code><code>:\"success\"}</code></p>', '<p>{<br>\r\n“status” : error,</p>\r\n\r\n<p>\"errmessage\": custom error msg,</p>\r\n\r\n<p>\"tocken\":\"e88bce04f91810f5be8a17a73af4d684\",</p>\r\n\r\n<p>}</p>', 'No'),
(16, 'APPROVED', 'User Register/resume  work experience  details ', 'http://192.168.1.111/job_portal/sign_up/add_work_exp_detail', 'POST', '<p><span style=\"font-family:calibri,sans-serif; font-size:11.0pt\">1.User add his/herwork exprience details .that can be store in resume and his/her profile.&nbsp; User registration is required following actions.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-family:calibri,sans-serif; font-size:11.0pt\">User allow to add mutiple work details.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Anual salary data entry stored as =&nbsp; annual_salary_lacs1-annual_salary_lacs2 (<strong>with - separater</strong>)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Note : data of post variable are depend on number of work experience.</strong></p>\r\n', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n	<tbody>\r\n		<tr>\r\n			<td>achievements1</td>\r\n			<td><code>rtryryr</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>achievements2</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>action</td>\r\n			<td><code>add_work_exp_detail</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>annual_salary_lacs1</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>annual_salary_lacs2</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>annual_salary_thousand1</td>\r\n			<td><code>10</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>annual_salary_thousand2</td>\r\n			<td><code>20</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>company_name1</td>\r\n			<td><code>narjis</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>company_name2</td>\r\n			<td><code>Evershine</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>csrf_job_portal</td>\r\n			<td><code>2ae0d20d1507712bef1776d6a71c336b</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>currency_type1</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>currency_type2</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>functional_area1</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>functional_area2</td>\r\n			<td><code>3</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>industry1</td>\r\n			<td><code>7</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>industry2</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>job_role1</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>job_role2</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>joining_date1</td>\r\n			<td><code>2017-04-09</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>joining_date2</td>\r\n			<td><code>2017-04-09</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>leaving_date1</td>\r\n			<td><code>2017-04-18</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>leaving_date2</td>\r\n			<td><code>2017-04-27</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>no_of_work_exp_row</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>user_agent</td>\r\n			<td><code>NI-WEB</code></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{&quot;token&quot;:&quot;2ae0d20d1507712bef1776d6a71c336b&quot;,&quot;errmessage&quot;:&quot;Work experience details added successfully</code><code>.&quot;,&quot;status&quot;:&quot;success&quot;}</code></pre>\r\n</div>\r\n', '<p>{<br />\r\n&ldquo;status&rdquo; : error,</p>\r\n\r\n<p>&quot;errmessage&quot;: custom error msg,</p>\r\n\r\n<p><span style=\"font-family:calibri,sans-serif; font-size:10.0pt\">&quot;tocken&quot;:&quot;e88bce04f91810f5be8a17a73af4d684&quot;,</span></p>\r\n\r\n<p>}</p>\r\n', 'No'),
(17, 'APPROVED', 'User Register / Resumr other  details ', 'http://192.168.1.111/job_portal/sign_up/js_extra_details', 'POST', '<p>Job seeker other details</p>\r\n\r\n<p>Anual salary data entry stored as =&nbsp; a_salary_lacs-a_salary_thousand (<strong>with - separater</strong>)</p>\r\n\r\n<p>Expected Anual salary data entry stored as =&nbsp; a_salary_lacs-a_salary_thousand (<strong>with - separater</strong>)</p>\r\n\r\n<p>Expected Total experiyance &nbsp; data entry stored as =&nbsp; exp_year-exp_month (<strong>with - separater</strong>)</p>\r\n', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n	<tbody>\r\n		<tr>\r\n			<td>a_salary_lacs</td>\r\n			<td><code>6</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>a_salary_thousand</td>\r\n			<td><code>30</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>action</td>\r\n			<td><code>js_details</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>csrf_job_portal</td>\r\n			<td><code>2ae0d20d1507712bef1776d6a71c336b</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>currency_type</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>desire_job_type</td>\r\n			<td><code>Permanent, Temporary/Contractual</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>employment_type</td>\r\n			<td><code>Part Time, Full Time</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>exp_month</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>exp_salary_currency_type</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>exp_salary_lacs</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>exp_salary_thousand</td>\r\n			<td><code>20</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>exp_year</td>\r\n			<td><code>1</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>functional_area</td>\r\n			<td><code>3</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>home_city</td>\r\n			<td><code>Ahmedabad</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>industry</td>\r\n			<td><code>2</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>job_role</td>\r\n			<td><code>4</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>key_skill</td>\r\n			<td><code>HTML, PHP, Html Developer</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>landline</td>\r\n			<td><code>56545456456</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>pincode</td>\r\n			<td><code>4578</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>prefered_shift</td>\r\n			<td><code>Day, Flexible, Night</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>preferred_city</td>\r\n			<td><code>Surat, Mumbai, Sidney</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>profile_summary</td>\r\n			<td><code>asfdsgdfg</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>resume_headline</td>\r\n			<td><code>dffgdg sdgfdgtdf</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>user_agent</td>\r\n			<td><code>NI-WEB</code></td>\r\n		</tr>\r\n		<tr>\r\n			<td>website</td>\r\n			<td><code>http://test.com</code></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<pre>\r\n<code>{&quot;token&quot;:&quot;2ae0d20d1507712bef1776d6a71c336b&quot;,&quot;errmessage&quot;:&quot;Your details added successfully.&quot;,&quot;status&quot;</code><code>:&quot;success&quot;</code></pre>\r\n', '<p>http://192.168.1.111/job_portal/sign_up/add_personal_detail</p>\r\n', 'No'),
(18, 'APPROVED', 'Job seeker upload profile and resume in register', 'http://192.168.1.111/job_portal/sign_up/upload_profile_pic_resume', 'POST', '', '<p><code>profile_pic</code> :DATA</p>\r\n\r\n<p>action :  <code> </code><code>upload_img</code></p>\r\n\r\n<p><code>user_agent</code>  : <code> </code><code>NI-WEB</code></p>\r\n\r\n<p><code>csrf_job_portal</code></p>\r\n\r\n<p><code>profile_pic_upload</code>  : <code>my.jpg</code></p>\r\n\r\n<p> </p>\r\n\r\n<p><code>resume_file_upload</code> : <code>fortest.pdf</code></p>\r\n\r\n<p>resume_file:DATA</p>', '<p><strong>1. condtion of success (upload both resume and profile picture)</strong></p>\r\n\r\n<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"2ae0d20d1507712bef1776d6a71c336b\",\"profile_pic_success_msg\":\"Profile picture uploded successfuly</code><code>.\",\"resume_success_msg\":\"Resume uploded successfuly.\",\"errmessage\":\"Resume and profile picture uploaded</code><code> successfully.\",\"status\":\"success\"}</code></pre>\r\n</div>\r\n\r\n<p><strong>2. condtion of success (upload  resume)</strong></p>\r\n\r\n<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"2ae0d20d1507712bef1776d6a71c336b\",\"resume_success_msg\":\"Resume uploded successfuly.\"}</code></pre>\r\n</div>\r\n\r\n<p><strong>3. condtion of success (upload  profile picture)</strong></p>\r\n\r\n<div class=\"netInfoResponseText netInfoText \">\r\n<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"2ae0d20d1507712bef1776d6a71c336b\",\"profile_pic_success_msg\":\"Profile picture uploded successfuly</code><code>.\"}</code></pre>\r\n</div>\r\n</div>', '<p><strong>File extenstion format error</strong></p>\r\n\r\n<p> </p>\r\n\r\n<pre>\r\n<code>{\"token\":\"2ae0d20d1507712bef1776d6a71c336b\",\"errmessage_profile_pic\":\"</code></pre>\r\n\r\n<p><code>The filetype you are attempting to upload is not allowed.&lt;\\/p>\",\"errmessage_resume\":\"</code></p>\r\n\r\n<p><code>The filetype you are attempting to upload is not allowed.&lt;\\/p>\"}</code></p>\r\n\r\n<p> </p>\r\n\r\n<p><strong><code>File uploding error</code></strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>{<br>\r\n“status” : error,</p>\r\n\r\n<p>\"errmessage\": custom error msg,</p>\r\n\r\n<p><span xss=removed>\"tocken\":\"e88bce04f91810f5be8a17a73af4d684\",</span></p>\r\n\r\n<p>}</p>', 'No'),
(19, 'APPROVED', 'Get Education qualification Listing', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>\r\n', '<p>user_agent = &nbsp;NI-WEB (for desktop) , &nbsp;NI-IAPP (for &nbsp;IOS) , NI-AAPP (for Android)<br />\r\ncsrf_job_portal</p>\r\n\r\n<p>currnet_val</p>\r\n\r\n<p>get_list = edu_list</p>\r\n', '<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{&quot;tocken&quot;:&quot;fe6fca6ee091401c86bc836c2c45eaf0&quot;,&quot;status&quot;:&quot;success&quot;,&quot;data&quot;:[</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{&quot;id&quot;:&quot;0&quot;,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;Select option&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">},</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;qualification_leval&quot;:&quot;Graduate&quot;,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;edu_name&quot;:[</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;id&quot;:5,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;B.COM&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">},</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;id&quot;:3,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;BA&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">},</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;id&quot;:4,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;BSC&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">}</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">]},</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;qualification_leval&quot;:&quot;Master&quot;,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;edu_name&quot;:[</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;id&quot;:6,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;MBA&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">},</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">{</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;id&quot;:8,</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">&quot;val&quot;:&quot;MCA&quot;</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">}]</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">}</span></p>\r\n\r\n<p><span style=\"color:rgb(0, 0, 0); font-family:times new roman; font-size:medium\">]}</span></p>\r\n', '', 'No'),
(20, 'APPROVED', 'About us api', 'http://192.168.1.111/job_portal/cms/about-us', 'POST', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)<br>\r\ncsrf_job_portal</p>', '<p>{  <br>\r\n   \"status\":\"success\",<br>\r\n   \"data\":{  <br>\r\n      \"page_title\":\"About us\",<br>\r\n      \"page_content\":\"</p>\r\n\r\n<div class=\"\\\\\">\\r\\n\r\n<div class=\"\\\\\">Terms And Conditions\\r\\n\r\n<div class=\"\\\\\">\\r\\n\r\n<div class=\"\\\\\">\\r\\n\r\n<h4>Advantages&lt;\\/h4>\\r\\n&lt;\\/div>\\r\\n\\r\\n</h4>\r\n\r\n<div class=\"\\\\\"> &lt;\\/div>\\r\\n&lt;\\/div>\\r\\n\\r\\n\r\n<p><strong>1.&lt;\\/strong>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"&lt;\\/p>\\r\\n\\r\\n</strong></p>\r\n\r\n<p><strong><strong>2.&lt;\\/strong>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.&lt;\\/p>\\r\\n\\r\\n</strong></strong></p>\r\n\r\n<p><strong><strong><strong>3.&lt;\\/strong>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth.&lt;\\/p>\\r\\n\\r\\n</strong></strong></strong></p>\r\n\r\n<ul>\r\n <li><strong><strong><strong>\\r\\n\\t</strong></strong></strong>\r\n\r\n <ul>\r\n  <li><strong><strong><strong>Executing the Food Service program, including preparing and presenting our wonderful food offerings to hungry customers on the go!&lt;\\/li>\\r\\n\\t</strong></strong></strong></li>\r\n  <li><strong><strong><strong>Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.&lt;\\/li>\\r\\n\\t</strong></strong></strong></li>\r\n  <li><strong><strong><strong>Keeping our Store food area looking terrific and ready for our valued customers by managing product inventory, stocking, cleaning, etc.&lt;\\/li>\\r\\n\\t</strong></strong></strong></li>\r\n  <li><strong><strong><strong>We’re looking for associates who enjoy interacting with people and working in a fast-paced environment!&lt;\\/li>\\r\\n&lt;\\/ul>\\r\\n\\r\\n</strong></strong></strong>\r\n  <div class=\"\\\\\"><strong><strong><strong>\\r\\n</strong></strong></strong>\r\n  <div class=\"\\\\\"><strong><strong><strong>\\r\\n</strong></strong></strong>\r\n  <h4><strong><strong><strong>Lorem ipsum dolor sit amet.&lt;\\/h4>\\r\\n&lt;\\/div>\\r\\n\\r\\n</strong></strong></strong></h4>\r\n\r\n  <div class=\"\\\\\"><strong><strong><strong> &lt;\\/div>\\r\\n&lt;\\/div>\\r\\n\\r\\n</strong></strong></strong>\r\n\r\n  <p><strong><strong><strong><strong>1.&lt;\\/strong>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"&lt;\\/p>\\r\\n\\r\\n</strong></strong></strong></strong></p>\r\n\r\n  <p><strong><strong><strong><strong><strong>2.&lt;\\/strong>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.&lt;\\/p>\\r\\n\\r\\n</strong></strong></strong></strong></strong></p>\r\n\r\n  <p><strong><strong><strong><strong><strong><strong>3.&lt;\\/strong>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth.&lt;\\/p>\\r\\n\\r\\n</strong></strong></strong></strong></strong></strong></p>\r\n\r\n  <h4><strong><strong><strong><strong><strong><strong>voluptatum deleniti&lt;\\/h4>\\r\\n\\r\\n</strong></strong></strong></strong></strong></strong></h4>\r\n\r\n  <ul>\r\n   <li><strong><strong><strong><strong><strong><strong>\\r\\n\\t</strong></strong></strong></strong></strong></strong>\r\n\r\n   <ul>\r\n    <li><strong><strong><strong><strong><strong><strong>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.&lt;\\/li>\\r\\n\\t</strong></strong></strong></strong></strong></strong></li>\r\n    <li><strong><strong><strong><strong><strong><strong>Must be available to work required shifts including weekends, evenings and holidays.&lt;\\/li>\\r\\n\\t</strong></strong></strong></strong></strong></strong></li>\r\n    <li><strong><strong><strong><strong><strong><strong>Must be able to perform repeated bending, standing and reaching.&lt;\\/li>\\r\\n\\t</strong></strong></strong></strong></strong></strong></li>\r\n    <li><strong><strong><strong><strong><strong><strong>Must be able to occasionally lift up to 50 pounds&lt;\\/li>\\r\\n&lt;\\/ul>\\r\\n&lt;\\/div>\\r\\n&lt;\\/div>\\r\\n\"<br>\r\n       },<br>\r\n       \"tocken\":\"1f60b749fb785eaacbbbaedf283862c8\"<br>\r\n    }</strong></strong></strong></strong></strong></strong>\r\n    <p> </p>\r\n    </li>\r\n   </ul>\r\n   </li>\r\n  </ul>\r\n  </div>\r\n  </div>\r\n  </div>\r\n  </li>\r\n </ul>\r\n </li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '', 'No'),
(21, 'APPROVED', 'Employer Account details Register', 'http://192.168.1.111/job_portal/sign_up_employer/signupemp', 'POST', '<p>1.Employer registration first page .</p>\r\n\r\n<p>Following are the parameters required  </p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>fullname<br>\r\n<br>\r\nemailid_confirmation  = Email id of the employer</p>\r\n\r\n<p>emailid = Cofirmation  Email id of the employer</p>\r\n\r\n<p>pass_confirmation = Password of the employer</p>\r\n\r\n<p>pass = Cofirmation  Password id of the employer</p>\r\n\r\n<p>mobile_c_code =  Mobile code</p>\r\n\r\n<p>mobile  =  Mobile number</p>\r\n\r\n<p>country</p>\r\n\r\n<p>city</p>\r\n\r\n<p>address</p>\r\n\r\n<p>pincode</p>\r\n\r\n<p>title</p>\r\n\r\n<p> </p>', '<p>{  <br>\r\n   \"tocken\":\"0d75d445871497c8036deea02c9e074e\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"employer_id_inserted\":\"15\",<br>\r\n   \"stepnoreturn\":\"2\",<br>\r\n   \"successmessage\":\"Your account as an employer has been created successfully.\"<br>\r\n}</p>', '<p>{  <br>\r\n   \"tocken\":\"0d75d445871497c8036deea02c9e074e\",<br>\r\n   \"errormessage\":\"</p>\r\n\r\n<p>The Email Confirmation field does not match the Email field.&lt;\\/p>\\n</p>\r\n\r\n<p>The Password Confirmation field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Password field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Mobile code field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Mobile Number field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Country field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The City field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Address field is required.&lt;\\/p>\\n</p>\r\n\r\n<p>The Pincode field is required.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(22, 'APPROVED', 'Employer Account details Register form 2', 'http://192.168.1.111/job_portal/sign_up_employer/signupemp2', 'POST', '<p>.Employer registration second page .</p>\r\n\r\n<p>Following are the parameters required</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal </p>\r\n\r\n<p>company_name</p>\r\n\r\n<p>company_type</p>\r\n\r\n<p>industry</p>\r\n\r\n<p>user_id = id of the employer that was received in return of submitting form 1 of employer</p>', '<p>{  <br>\r\n   \"tocken\":\"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"employer_id_inserted\":\"18\",<br>\r\n   \"successmessage\":\"Your account as an employer has been created successfully.\"<br>\r\n}</p>', '<p>{  <br>\r\n   \"tocken\":\"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n   \"errormessage\":\"<p>The Company name field is required.&lt;\\/p>\\n<p>The Company type field is required.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(23, 'APPROVED', 'Employer Account details Register form 3', 'http://192.168.1.111/job_portal/sign_up_employer/signupemp3', 'POST', '<p>.Employer registration THIRD page .</p>\r\n\r\n<p>Following are the parameters required</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal </p>\r\n\r\n<p>currentdesignation</p>\r\n\r\n<p>join_month</p>\r\n\r\n<p>join_year</p>\r\n\r\n<p>job_profile</p>\r\n\r\n<p>user_id = id of the employer that was received in return of submitting form 2 of employer</p>', '<p>{  <br>\r\n   \"tocken\":\"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"employer_id_inserted\":\"18\",<br>\r\n   \"successmessage\":\"Your account as an employer has been created successfully.\"<br>\r\n}</p>', '<p>{  <br>\r\n   \"tocken\":\"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n   \"errormessage\":\"<p>The Current designation field is required.&lt;\\/p>\\n<p>The Join month field is required.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(24, 'APPROVED', 'suggestion api', 'http://192.168.1.111/job_portal/sign_up_employer/get_suggestion/\"+request.term', 'POST', '<p>request.term = with eachkey press send the above request and request.term parameter is the key that is pressed</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p> </p>\r\n\r\n<p>example api to call on each key press :-</p>\r\n\r\n<p>If u is typed in the text box you have to call the below api</p>\r\n\r\n<p>http://192.168.1.111/job_portal/sign_up_employer/get_suggestion/u</p>', '<p>request.term = suggestion term</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>', '<pre>\r\n[<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"Project Manager\",\r\n      \"lable\": \"Project Manager\"\r\n   },\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"Developer\",\r\n      \"lable\": \"Developer\"\r\n   },\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"Senior Developer\",\r\n      \"lable\": \"Senior Developer\"\r\n   },\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"Junior Developer\",\r\n      \"lable\": \"Junior Developer\"\r\n   },\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"Product Designer\",\r\n      \"lable\": \"Product Designer\"\r\n   },\r\n   {<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n      \"value\": \"User Experience Designer\",\r\n      \"lable\": \"User Experience Designer\"\r\n   }\r\n]</pre>\r\n\r\n<p> </p>', '<p>If the results are available you will get results otherwise an empty array will return.</p>', 'No'),
(25, 'APPROVED', 'Get designation Listing from functional area', 'http://192.168.1.111/job_portal/common_request/get_list_json', 'POST', '<p>Usinng this web serivce you will get drop downlist option</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p><br>\r\ncsrf_job_portal</p>\r\n\r\n<p><br>\r\nget_list = role_master</p>\r\n\r\n<p>multivar = multi</p>\r\n\r\n<p>currnet_val =1,2,3</p>\r\n\r\n<p>retun_for = json</p>\r\n\r\n<p>.... and so on each selected valule of functional area is to be send in individual  parameter currnet_val[] </p>', '<pre>\r\n{  \r\n   \"tocken\":\"7a40818e986c769511ba0681c49ada70\",\r\n   \"status\":\"success\",\r\n   \"data\":[  \r\n      {  \r\n         \"functional_area\":\"Accounts \\/ Finance \\/ Tax \\/ CS \\/ Audit\",\r\n         \"designation\":[  \r\n            {  \r\n               \"id\":\"1\",\r\n               \"val\":\"Accounts Exec.\\/Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"2\",\r\n               \"val\":\"Cost Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"3\",\r\n               \"val\":\"Taxation(Direct) Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"4\",\r\n               \"val\":\"Taxation(Indirect) Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"5\",\r\n               \"val\":\"Accounts Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"6\",\r\n               \"val\":\"Financial Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"7\",\r\n               \"val\":\"ICWA\"\r\n            },\r\n            {  \r\n               \"id\":\"8\",\r\n               \"val\":\"Chartered Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"9\",\r\n               \"val\":\"Finance Exec.\"\r\n            },\r\n            {  \r\n               \"id\":\"10\",\r\n               \"val\":\"Credit\\/Control Exec.\"\r\n            },\r\n            {  \r\n               \"id\":\"11\",\r\n               \"val\":\"Investor Relationship-Exec.\\/Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"12\",\r\n               \"val\":\"Credit\\/Control Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"13\",\r\n               \"val\":\"Financial Analyst\"\r\n            },\r\n            {  \r\n               \"id\":\"14\",\r\n               \"val\":\"Audit Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"15\",\r\n               \"val\":\"Forex Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"16\",\r\n               \"val\":\"Treasury Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"17\",\r\n               \"val\":\"Finance\\/Budgeting Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"18\",\r\n               \"val\":\"Head\\/VP\\/GM-Finance\\/Audit\"\r\n            },\r\n            {  \r\n               \"id\":\"19\",\r\n               \"val\":\"Head\\/VP\\/GM-Accounts\"\r\n            }\r\n         ]\r\n      },\r\n      {  \r\n         \"functional_area\":\"Agent\",\r\n         \"designation\":[  \r\n            {  \r\n               \"id\":\"1\",\r\n               \"val\":\"Accounts Exec.\\/Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"2\",\r\n               \"val\":\"Cost Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"3\",\r\n               \"val\":\"Taxation(Direct) Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"4\",\r\n               \"val\":\"Taxation(Indirect) Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"5\",\r\n               \"val\":\"Accounts Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"6\",\r\n               \"val\":\"Financial Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"7\",\r\n               \"val\":\"ICWA\"\r\n            },\r\n            {  \r\n               \"id\":\"8\",\r\n               \"val\":\"Chartered Accountant\"\r\n            },\r\n            {  \r\n               \"id\":\"9\",\r\n               \"val\":\"Finance Exec.\"\r\n            },\r\n            {  \r\n               \"id\":\"10\",\r\n               \"val\":\"Credit\\/Control Exec.\"\r\n            },\r\n            {  \r\n               \"id\":\"11\",\r\n               \"val\":\"Investor Relationship-Exec.\\/Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"12\",\r\n               \"val\":\"Credit\\/Control Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"13\",\r\n               \"val\":\"Financial Analyst\"\r\n            },\r\n            {  \r\n               \"id\":\"14\",\r\n               \"val\":\"Audit Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"15\",\r\n               \"val\":\"Forex Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"16\",\r\n               \"val\":\"Treasury Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"17\",\r\n               \"val\":\"Finance\\/Budgeting Mgr\"\r\n            },\r\n            {  \r\n               \"id\":\"18\",\r\n               \"val\":\"Head\\/VP\\/GM-Finance\\/Audit\"\r\n            },\r\n            {  \r\n               \"id\":\"19\",\r\n               \"val\":\"Head\\/VP\\/GM-Accounts\"\r\n            },\r\n            {  \r\n               \"id\":\"20\",\r\n               \"val\":\"DSA\\/Company Rep.\"\r\n            },\r\n            {  \r\n               \"id\":\"21\",\r\n               \"val\":\"Independent Rep.\"\r\n            },\r\n            {  \r\n               \"id\":\"22\",\r\n               \"val\":\"Life-Insurance Agent\"\r\n            },\r\n            {  \r\n               \"id\":\"23\",\r\n               \"val\":\"Non-Life Insurance Agent\"\r\n            },\r\n            {  \r\n               \"id\":\"24\",\r\n               \"val\":\"Real Estate Agent\"\r\n            },\r\n            {  \r\n               \"id\":\"25\",\r\n               \"val\":\"Travel Agent\"\r\n            }\r\n         ]\r\n      }\r\n   ]\r\n}</pre>', '', 'No'),
(26, 'APPROVED', 'Employer Account details Register form 4', 'http://192.168.1.111/job_portal/sign_up_employer/signupemp4', 'POST', '<p>.Employer registration FOURTH page .</p>\r\n\r\n<p>Following are the parameters required</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal </p>\r\n\r\n<p>profile_pic = profile image file of employer</p>\r\n\r\n<p>user_id = id of the employer that was received in return of submitting form 2 of employer</p>', '<pre>\r\n{<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n   \"tocken\": \"9f310b40198a9f6c94d7f7f188582f49\",\r\n   \"status\": \"success\",\r\n   \"employer_id_inserted\": \"18\",\r\n   \"successmessage\": \"Your account as an employer has been created successfully.\"\r\n}</pre>', '<pre>\r\n{<img src=\"http://www.freeformatter.com/2.3/img/minus.gif\">\r\n   \"tocken\": \"9f310b40198a9f6c94d7f7f188582f49\",\r\n   \"status\": \"error\",\r\n   \"errormessage\": \"Invalid file type\"\r\n}</pre>', 'No'),
(27, 'APPROVED', 'Employer Login', 'http://192.168.1.111/job_portal/login_employer/login', 'POST', '<p>Employer login is required following actions</p>', '<p>action= login</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>username = emailof employer</p>\r\n\r\n<p>password</p>\r\n\r\n<p>csrf_job_portal</p>', '<p>{<br>\r\n  \"token\": \"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n  \"user_agent\": \"NI-IAPP\",<br>\r\n  \"errmessage\": \"Login successfull\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '<p>{<br>\r\n  \"token\": \"9f310b40198a9f6c94d7f7f188582f49\",<br>\r\n  \"user_agent\": \"NI-IAPP\",<br>\r\n  \"errmessage\": \"Entered email or password is incorrect .\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>', 'No'),
(28, 'APPROVED', 'Get employer profile data after login', 'http://192.168.1.111/job_portal/common_request/get_profile_details_emp', 'POST', '<p> </p>\r\n\r\n<p>Get all the details of the employer</p>', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>emp_id_profile= id of employer that was obtiainded when the employer was logged in </p>', '<p>{  <br>\r\n   \"tocken\":\"e778b3ee63952654eb15903b139c59f0\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"emp_data\":[  <br>\r\n      {  <br>\r\n         \"id\":\"18\",<br>\r\n         \"email\":\"test1212@test.com\",<br>\r\n         \"title\":\"0\",<br>\r\n         \"fullname\":\"test\",<br>\r\n         \"personal_email\":\"\",<br>\r\n         \"designation\":\"Art Director\\/Sr Art Director, Senior Developer,\",<br>\r\n         \"join_in\":\"April ,1998\",<br>\r\n         \"job_profile\":\"test\\ntest\\ntest\",<br>\r\n         \"email_verified\":\"No\",<br>\r\n         \"status\":\"APPROVED\",<br>\r\n         \"password\":\"$2a$08$LZvLSrAW5aB8BnUSb7aN..fp43gLsD0eJ\\/VnDXU\\/nAiAXPWMCTujG\",<br>\r\n         \"country\":\"1\",<br>\r\n         \"city\":\"1\",<br>\r\n         \"address\":\"fdrtfsd\",<br>\r\n         \"pincode\":\"5654665\",<br>\r\n         \"mobile\":\"+91-45654654654\",<br>\r\n         \"profile_pic\":\"http:\\/\\/192.168.1.111\\/job_portal\\/assets\\/emp_photos\\/ce43b442f34e61d3406337df1ccf6240.jpg\",<br>\r\n         \"profile_pic_approval\":\"UNAPPROVED\",<br>\r\n         \"company_name\":\"test\",<br>\r\n         \"company_type\":\"Company\",<br>\r\n         \"industry\":\"1\",<br>\r\n         \"industry_hire\":null,<br>\r\n         \"function_area_hire\":null,<br>\r\n         \"skill_hire\":null,<br>\r\n         \"register_date\":\"2017-04-13 06:14:36\",<br>\r\n         \"user_agent\":\"NI-AAPP\",<br>\r\n         \"device_id\":\"\",<br>\r\n         \"last_login\":null,<br>\r\n         \"is_deleted\":\"No\",<br>\r\n         \"city_name\":\"Ahmedabad\",<br>\r\n         \"country_name\":\"India\",<br>\r\n         \"personal_titles\":null,<br>\r\n         \"industries_name\":\"IT Industry\"<br>\r\n      }<br>\r\n   ]<br>\r\n}</p>', '<p>{  <br>\r\n   \"tocken\":\"e778b3ee63952654eb15903b139c59f0\",<br>\r\n   \"status\":\"error\",<br>\r\n   \"errorMessage\":\"Data Not Found\"<br>\r\n}</p>', 'No'),
(29, 'APPROVED', 'Employer Account details edit', 'http://192.168.1.111/job_portal/employer_profile/updatepersonaldet', 'POST', '<p>1.Employer edit personal details</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>emp_id  = ID OF THE employer</p>\r\n\r\n<p>title</p>\r\n\r\n<p>fullname</p>\r\n\r\n<p>mobile_c_code</p>\r\n\r\n<p>mobile</p>\r\n\r\n<p>country</p>\r\n\r\n<p>city</p>\r\n\r\n<p>address</p>\r\n\r\n<p>pincode</p>', '<p>{  <br>\r\n   \"tocken\":\"dcd32270c8393ef575cf00d7430c918c\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"successmessage\":\"Your account as an employer has been updated successfully.\"<br>\r\n}</p>', '<p>{  <br>\r\n   \"tocken\":\"dcd32270c8393ef575cf00d7430c918c\",<br>\r\n   \"errormessage\":\"<p>The Pincode field is required.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(30, 'APPROVED', 'Employer Company details edit', 'http://192.168.1.111/job_portal/employer_profile/updatecompanydet', 'POST', '<p>1.Employer edit company details</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>emp_id  = ID OF THE employer</p>\r\n\r\n<p>company_name</p>\r\n\r\n<p>company_type</p>\r\n\r\n<p>industry</p>', '<p>{\"tocken\":\"dcd32270c8393ef575cf00d7430c918c\",\"status\":\"success\",\"successmessage\":\"Your account as an employer has been updated successfully.\"}</p>', '<p>{  <br>\r\n   \"tocken\":\"dcd32270c8393ef575cf00d7430c918c\",<br>\r\n   \"errormessage\":\"</p>\r\n\r\n<p>The Industry field is required.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(31, 'APPROVED', 'Employer Work experiene details edit', 'http://192.168.1.111/job_portal/employer_profile/updatework_expdet', 'POST', '<p>1.Employer edit work experience details</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>emp_id  = ID OF THE employer</p>\r\n\r\n<p>currentdesignation[] = array of id&#39;s of desgnatjion</p>\r\n\r\n<p>join_month = example January</p>\r\n\r\n<p>join_year = example 2015</p>\r\n\r\n<p>job_profile </p>', '<p>{<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"successmessage\":\"Your account as an employer has been updated successfully.\"<br>\r\n}</p>', '<p>{<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\",<br>\r\n   \"errormessage\":\"<p>Please deselect the &#39;select current role&#39; option to proceed further.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No'),
(32, 'APPROVED', 'Employer Secotrs and skill hire for details edit', 'http://192.168.1.111/job_portal/employer_profile/updateskilldet', 'POST', '<p>1.Employer edit  Secotrs and skill hire for</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>emp_id  = ID OF THE employer</p>\r\n\r\n<p>industry_hire = array of id&#39;s of industry</p>\r\n\r\n<p>function_area_hire = array of id&#39;s of functional areas</p>\r\n\r\n<p>skill_hire = array of id&#39;s of skills</p>', '<p>{<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\",<br>\r\n   \"status\":\"success\",<br>\r\n   \"successmessage\":\"Your account as an employer has been updated successfully.\"<br>\r\n}</p>', '<p>{<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\",<br>\r\n   \"errormessage\":\"<p>Please deselect the &#39;select skill&#39; option to proceed further.&lt;\\/p>\\n\",<br>\r\n   \"status\":\"error\"<br>\r\n}</p>', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(33, 'APPROVED', 'Employer profile picture edit', 'http://192.168.1.111/job_portal/employer_profile/uploadprofile', 'POST', '<p>1.Employer edit  profile picture</p>', '<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>csrf_job_portal = csrf token from the token api</p>\r\n\r\n<p>emp_id  = ID OF THE employer</p>\r\n\r\n<p>profile_pic = image file in the format jpg, jpeg or png</p>', '<p>{<br>\r\n   \"final_result\":{<br>\r\n      \"status\":\"success\",<br>\r\n      \"file_data\":{<br>\r\n         \"file_name\":\"094df825733f54e7c62d10239b3c5e96.jpg\",<br>\r\n         \"file_type\":\"image\\/jpeg\",<br>\r\n         \"file_path\":\"D:\\/wamp\\/www\\/job_portal\\/assets\\/emp_photos\\/\",<br>\r\n         \"full_path\":\"D:\\/wamp\\/www\\/job_portal\\/assets\\/emp_photos\\/094df825733f54e7c62d10239b3c5e96.jpg\",<br>\r\n         \"raw_name\":\"094df825733f54e7c62d10239b3c5e96\",<br>\r\n         \"orig_name\":\"Koala.jpg\",<br>\r\n         \"client_name\":\"Koala.jpg\",<br>\r\n         \"file_ext\":\".jpg\",<br>\r\n         \"file_size\":762.53,<br>\r\n         \"is_image\":true,<br>\r\n         \"image_width\":1024,<br>\r\n         \"image_height\":768,<br>\r\n         \"image_type\":\"jpeg\",<br>\r\n         \"image_size_str\":\"width=\\\"1024\\\" height=\\\"768\\\"\"<br>\r\n      }<br>\r\n   },<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\"<br>\r\n}</p>', '<p>{<br>\r\n   \"final_result\":{<br>\r\n      \"status\":\"error\",<br>\r\n      \"error_message\":\"<p>The filetype you are attempting to upload is not allowed.&lt;\\/p>\"<br>\r\n   },<br>\r\n   \"tocken\":\"1cb15ddacded5a1ea16fc4729dc90a9e\"<br>\r\n}</p>', 'No'),
(34, 'APPROVED', 'send message to employer', 'http://192.168.1.111/job_portal/job_seeker_action/send_message', 'POST', '<p>Send mesage to employer by job seeker</p>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>action</td>\r\n   <td><code>send_message</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>content</td>\r\n   <td><code>Third test by ketan</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>fcc746c9fc5a67a6e56f73a66c6d6341</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td><code>11</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>subject</td>\r\n   <td><code>Third test by ketan</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"fcc746c9fc5a67a6e56f73a66c6d6341\",\"errmessage\":\"Message sent successfully\",\"status\":\"success\"</code><code>}</code></pre>\r\n</div>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"fcc746c9fc5a67a6e56f73a66c6d6341\",\"errmessage\":\"Something is not wright\",\"status\":\"success\"</code><code>}</code></pre>\r\n</div>', 'No'),
(35, 'APPROVED', 'Add new job / post new job', 'http://192.168.1.111/job_portal/employer_post_job/save_job', 'POST', '<p>For add new job..</p>\r\n\r\n<p>Note :</p>\r\n\r\n<p>1. job  industry and job functional area listing from employer profile industry and functional area listing</p>\r\n\r\n<p>2. job role is dependent on functional area</p>\r\n\r\n<p>3. salary entry is two type</p>\r\n\r\n<p>job salary is selected as As per company rules and regulations</p>\r\n\r\n<p>then display text box</p>\r\n\r\n<p>otherwise salary is select define then display as below</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>salary_from</td>\r\n   <td><code>1.5</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>salary_to</td>\r\n   <td><code>2.0</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n   </td>\r\n   <td> </td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>action</td>\r\n   <td><code>save_job</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>company_hiring</td>\r\n   <td><code>CE Engg. manager</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>159b9bc3bf6f07853fdadee726b318ef</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>currency_type</td>\r\n   <td><code>INR</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>currently_hiring_status</td>\r\n   <td><code>Yes</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>define_salary</td>\r\n   <td><code>Define</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>desire_candidate_profile</td>\r\n   <td><code>Test by narjis enterprise</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>functional_area</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_description</td>\r\n   <td><code>New POst by ketan for CE Engg</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_education[]</td>\r\n   <td><code>24</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_education[]</td>\r\n   <td><code>10</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_education[]</td>\r\n   <td><code>11</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_education[]</td>\r\n   <td><code>4</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_industry</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_payment</td>\r\n   <td><code>2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_post_role</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_salary_text</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_shift</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title</td>\r\n   <td><code>New POst by ketan for CE Engg</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_type</td>\r\n   <td><code>2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>link_job</td>\r\n   <td><code>http://test.com</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>location_hiring[]</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>location_hiring[]</td>\r\n   <td><code>156</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>location_hiring[]</td>\r\n   <td><code>2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>location_hiring[]</td>\r\n   <td><code>4</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>salary_from</td>\r\n   <td><code>1.5</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>salary_to</td>\r\n   <td><code>2.0</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>skill_keyword</td>\r\n   <td><code>Accounts Exec./Accountant</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>total_requirenment</td>\r\n   <td><code>3</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>work_experience_from</td>\r\n   <td><code>0.6</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>work_experience_to</td>\r\n   <td><code>1.6</code></td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"159b9bc3bf6f07853fdadee726b318ef\",\"errmessage\":\"Job posted successfuly.\",\"status\":\"success\"</code><code>}</code></pre>\r\n</div>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"159b9bc3bf6f07853fdadee726b318ef\",\"errmessage\":\"Custom error.\",\"status\":\"error\"</code><code>}</code></pre>\r\n</div>', 'No'),
(36, 'APPROVED', 'Get employer industry  , functional area , skill from his profile', 'http://192.168.1.111/job_portal/common_request/get_employer_industry_farea', 'POST', '', '<p>---&gt;get_list<br>\r\n1.skill_hire<br>\r\n2.industry_hire<br>\r\n3.function_area_hire</p>\r\n\r\n<p>---&gt;selected_item</p>\r\n\r\n<p>selected item list</p>\r\n\r\n<p>----&gt;tocken_val<br>\r\nvalue 1 for skill hire</p>', '', '', 'No'),
(37, 'APPROVED', 'http://192.168.1.111/job_portal/common_request/get_profile_details_js', 'Get jobseeker profile data after login', 'POST', '<table>\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p>Get all the details of the jobseeker</p>\r\n   </td>\r\n   <td> </td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api</p>\r\n\r\n<p>user_agent =  NI-WEB (for desktop) ,  NI-IAPP (for  IOS) , NI-AAPP (for Android)</p>\r\n\r\n<p>js_id_profile = id of job seeker that was obtiainded when the job seeker was logged in </p>', '<p>{<br>\r\n   \"status\":\"success\",<br>\r\n   \"js_data\":[<br>\r\n      {<br>\r\n         \"id\":\"61\",<br>\r\n         \"facebook_id\":\"\",<br>\r\n         \"gplus_id\":\"\",<br>\r\n         \"status\":\"APPROVED\",<br>\r\n         \"email\":\"jay.narjisenterprise@gmail.com\",<br>\r\n         \"password\":\"$2a$08$r30vpk5NcogmzeIUt\\/QwDuUXSfDoM2DOPmyUfYpouF1HxmYY6gNcO\",<br>\r\n         \"verify_email\":\"No\",<br>\r\n         \"title\":\"0\",<br>\r\n         \"fullname\":\"jay\",<br>\r\n         \"gender\":\"Male\",<br>\r\n         \"birthdate\":\"2017-05-01\",<br>\r\n         \"marital_status\":\"3\",<br>\r\n         \"address\":\"test\",<br>\r\n         \"city\":\"8\",<br>\r\n         \"country\":\"3\",<br>\r\n         \"mobile\":\"+91-98249\",<br>\r\n         \"landline\":\"\",<br>\r\n         \"pincode\":\"111111\",<br>\r\n         \"website\":\"www.google.com\",<br>\r\n         \"profile_pic\":\"http:\\/\\/192.168.1.111\\/job_portal\\/assets\\/js_photos\\/e64eaacea212784478bf76ac179751a1.jpg\",<br>\r\n         \"profile_pic_approval\":\"UNAPPROVED\",<br>\r\n         \"profile_summary\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\",<br>\r\n         \"home_city\":\"Vadodara\",<br>\r\n         \"preferred_city\":\"Vadodara, Jalandhar, Mumbai, Sydney, Pune\",<br>\r\n         \"resume_headline\":\"testing resume headline\",<br>\r\n         \"total_experience\":\"1-0\",<br>\r\n         \"annual_salary\":\"0-15\",<br>\r\n         \"currency_type\":\"INR\",<br>\r\n         \"industry\":\"1\",<br>\r\n         \"functional_area\":\"2\",<br>\r\n         \"job_role\":\"1\",<br>\r\n         \"key_skill\":\"Html Developer, 4th Dimenssion, AccPac\",<br>\r\n         \"email_verification\":\"No\",<br>\r\n         \"resume_file\":\"11b953367388a69bad2b78c181fd5535.txt\",<br>\r\n         \"resume_last_update\":\"2017-05-02 07:23:18\",<br>\r\n         \"resume_verification\":\"\",<br>\r\n         \"desire_job_type\":\"Temporary\\/Contractual\",<br>\r\n         \"employment_type\":\"Part Time, Full Time\",<br>\r\n         \"prefered_shift\":\"Day\",<br>\r\n         \"expected_annual_salary\":\"-\",<br>\r\n         \"exp_salary_currency_type\":\"\",<br>\r\n         \"register_date\":\"2017-04-05 09:49:30\",<br>\r\n         \"last_login\":\"0000-00-00 00:00:00\",<br>\r\n         \"profile_visibility\":\"Yes\",<br>\r\n         \"facebook_url\":\"\",<br>\r\n         \"gplus_url\":\"\",<br>\r\n         \"twitter_url\":\"\",<br>\r\n         \"linkedin_url\":\"\",<br>\r\n         \"device_id\":\"\",<br>\r\n         \"user_agent\":\"NI-WEB\",<br>\r\n         \"user_ip\":\"\",<br>\r\n         \"plan_status\":\"Active\",<br>\r\n         \"is_deleted\":\"No\",<br>\r\n         \"plan_name\":\"\",<br>\r\n         \"personal_titles\":null,<br>\r\n         \"marital_status_val\":\"Unmarried\",<br>\r\n         \"city_name\":\"Sydney\",<br>\r\n         \"country_name\":\"Australia\",<br>\r\n         \"industries_name\":\"Accounting\\/Finance\",<br>\r\n         \"functional_name\":\"Agent\",<br>\r\n         \"role_name\":\"Accounts Exec.\\/Accountant\"<br>\r\n      }<br>\r\n   ]<br>\r\n}</p>', '<p>{<br>\r\n   \"status\":\"error\",<br>\r\n   \"errorMessage\":\"Data Not Found\"<br>\r\n}</p>', 'No'),
(38, 'APPROVED', 'Apply for job', 'http://192.168.1.111/job_portal/job_seeker_action/apply_for_job', 'POST', '<p>Apply for the new  job with email send to posted job employer</p>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>9133a0a6849ac7e6c5aa3cec789315e2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_id</td>\r\n   <td><code>32</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"9133a0a6849ac7e6c5aa3cec789315e2\",\"errmessage\":\"Applcation submitted successfully.\",\"status\"</code><code>:\"success\"}</code></pre>\r\n</div>', '<div class=\"netInfoResponseText netInfoText \">\r\n<pre>\r\n<code>{\"token\":\"9133a0a6849ac7e6c5aa3cec789315e2\",\"errmessage\":\"</code>Something is not right, while applying job<code>.\",\"status\":\"error\"}</code></pre>\r\n</div>\r\n\r\n<p> </p>', 'No'),
(39, 'APPROVED', 'get education details of job seeker', 'http://192.168.1.111/job_portal/my_profile/get_all_detailsfromid/education', 'POST', '<p>get education details of job seeker</p>', '<p>csrf_job_portal= token obtained from api 1.API mandatory token api</p>\r\n\r\n<p>user_agent = NI-AAPP<br>\r\n<br>\r\nuser_id = job seeker id from login</p>', '<p>{<br>\r\n   \"tocken\":\"1024045e9eed53dce343c2421e94d476\",<br>\r\n   \"data\":[<br>\r\n      {<br>\r\n         \"id\":\"1\",<br>\r\n         \"js_id\":\"12\",<br>\r\n         \"passing_year\":\"2014\",<br>\r\n         \"qualification_level\":\"7\",<br>\r\n         \"institute\":\"S.D college of engineering\",<br>\r\n         \"specialization\":\"Master of bakwas\",<br>\r\n         \"marks\":\"-150\",<br>\r\n         \"is_certificate_X_XII\":\"\",<br>\r\n         \"update_on\":\"2017-03-17 00:00:00\"<br>\r\n      },<br>\r\n      {<br>\r\n         \"id\":\"2\",<br>\r\n         \"js_id\":\"12\",<br>\r\n         \"passing_year\":\"2015\",<br>\r\n         \"qualification_level\":\"7\",<br>\r\n         \"institute\":\"S.S collge of engineering\",<br>\r\n         \"specialization\":\"Master of mahabkwaas\",<br>\r\n         \"marks\":\"-900\",<br>\r\n         \"is_certificate_X_XII\":\"\",<br>\r\n         \"update_on\":\"2017-03-17 00:00:00\"<br>\r\n      },<br>\r\n      {<br>\r\n         \"id\":\"3\",<br>\r\n         \"js_id\":\"12\",<br>\r\n         \"passing_year\":\"2015\",<br>\r\n         \"qualification_level\":\"7\",<br>\r\n         \"institute\":\"S.S collge of engineering\",<br>\r\n         \"specialization\":\"master of lolo\",<br>\r\n         \"marks\":\"4444\",<br>\r\n         \"is_certificate_X_XII\":\"\",<br>\r\n         \"update_on\":\"2017-03-17 00:00:00\"<br>\r\n      }<br>\r\n   ]<br>\r\n}</p>', '<p>{\"tocken\":\"1024045e9eed53dce343c2421e94d476\"}</p>', 'No'),
(40, 'APPROVED', 'Job seeker Personal details edit', 'http://192.168.1.111/job_portal/my_profile/editpersonaldet', 'POST', '<p>1.Job seeker edit personal details</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td> </td>\r\n   <td> </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>', '<table cellpadding=\"0\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>address</td>\r\n   <td><code>test</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>birthdate</td>\r\n   <td><code>1990-02-14</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>city</td>\r\n   <td><code>8</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>country</td>\r\n   <td><code>3</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>f7e694a27265d83715cb2a9de8238fef</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>fullname</td>\r\n   <td><code>jay</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>gender</td>\r\n   <td><code>Male</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>home_city</td>\r\n   <td><code>Vadodara</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>landline</td>\r\n   <td><code>7454545</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>marital_status</td>\r\n   <td><code>3</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>mobile</td>\r\n   <td><code>78676767555</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>mobile_c_code</td>\r\n   <td><code>+91</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>pincode</td>\r\n   <td><code>111111</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>resume_headline</td>\r\n   <td><code>resume headline</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>title</td>\r\n   <td><code>1</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td>NI-AAPP</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>user_id</p>', '<p>{<br>\r\n  \"tocken\": \"7f44ca129ab253ffd67bfd165a2d4e8a\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"successmessage\": \"Your account as a job seeker has been updated successfully.\"<br>\r\n}</p>', '<p>{<br>\r\n  \"tocken\": \"7f44ca129ab253ffd67bfd165a2d4e8a\",<br>\r\n  \"errormessage\": \"</p>\r\n\r\n<p>The Pincode field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Mobile field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Full name field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Resume headline field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Gender field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Maital status field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Mobile code field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Birth date field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Country field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The City field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Address field is required.</p>\r\n\r\n<p>\\n</p>\r\n\r\n<p>The Home city field is required.</p>\r\n\r\n<p>\\n\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>\r\n\r\n<p> </p>', 'No'),
(41, 'APPROVED', 'Job seeker other details edit', 'http://192.168.1.111/job_portal/my_profile/editotherdet', 'POST', '<p>1.Job seeker edit other details</p>', '<ol>\r\n <li>\r\n <p>profile_summary:</p>\r\n\r\n <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n </li>\r\n <li>\r\n <p>preferred_city:</p>\r\n\r\n <p>Ahmedabad, Mumbai, Valsad, anand</p>\r\n </li>\r\n <li>\r\n <p>prefered_shift:</p>\r\n\r\n <p>Day, Flexible, yrdyr</p>\r\n </li>\r\n <li>\r\n <p>website:</p>\r\n\r\n <p>www.google.com</p>\r\n </li>\r\n <li>\r\n <p>exp_year:</p>\r\n\r\n <p>1</p>\r\n </li>\r\n <li>\r\n <p>exp_month:</p>\r\n\r\n <p>2</p>\r\n </li>\r\n <li>\r\n <p>currency_type:</p>\r\n\r\n <p>INR</p>\r\n </li>\r\n <li>\r\n <p>a_salary_lacs:</p>\r\n\r\n <p>3</p>\r\n </li>\r\n <li>\r\n <p>a_salary_thousand:</p>\r\n\r\n <p>5</p>\r\n </li>\r\n <li>\r\n <p>industry:</p>\r\n\r\n <p>1</p>\r\n </li>\r\n <li>\r\n <p>functional_area:</p>\r\n\r\n <p>16</p>\r\n </li>\r\n <li>\r\n <p>job_role:</p>\r\n\r\n <p>385</p>\r\n </li>\r\n <li>\r\n <p>key_skill:</p>\r\n\r\n <p>yyugy, 3D Studio Max, Abap4</p>\r\n </li>\r\n <li>\r\n <p>desire_job_type:</p>\r\n\r\n <p>Permanent</p>\r\n </li>\r\n <li>\r\n <p>employment_type:</p>\r\n\r\n <p>Part Time</p>\r\n </li>\r\n <li>\r\n <p>user_agent:</p>\r\n\r\n <p>NI-AAPP</p>\r\n </li>\r\n <li>\r\n <p>csrf_job_portal:</p>\r\n\r\n <p>3b51dcadd0e149f06a33f465061058ca</p>\r\n </li>\r\n</ol>', '<p>{<br>\r\n  \"tocken\": \"89d7673b7d2a856af3804a79e04d8597\",<br>\r\n  \"errormessage\": \"<p>Please provide years of your Experience. Select zero if you are a fresher with no experience.</p>\\n<p>Please provide months of your Experience. Select zero if you are a fresher with no experience.</p>\\n<p>The Preferred city field is required.</p>\\n<p>The Preferred shift field is required.</p>\\n\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>', '', 'No'),
(42, 'APPROVED', 'Job seeker language details edit', 'http://192.168.1.111/job_portal/my_profile/editlangdet', 'POST', '<p>user_agent:NI-IAPP</p>\r\n\r\n<p><span xss=removed>user_id</span> :61</p>\r\n\r\n<p><br>\r\ncsrf_job_portal:ca07c4a04708d830454fb4bca086dec8</p>\r\n\r\n<p>//for update<br>\r\nlang_known_74:English<br>\r\nproficiency_74:Average</p>\r\n\r\n<p><span xss=removed>checkbox</span>_74: reading,writing,speaking</p>\r\n\r\n<p>// for add new</p>\r\n\r\n<p>&lt;!--StartFragment--&gt;<span xss=removed>lang_known_a_2 </span>:English<br>\r\n<span xss=removed>proficiency_a_2</span>:Average<br>\r\n<span xss=removed>checkbox_a_2</span>: reading,writing,speaking&lt;!--EndFragment--&gt;</p>', '<p>user_agent:NI-IAPP<br>\r\ncsrf_job_portal:ca07c4a04708d830454fb4bca086dec8</p>\r\n\r\n<p><span xss=removed>user_id</span> :61</p>\r\n\r\n<p><br>\r\ncsrf_job_portal:ca07c4a04708d830454fb4bca086dec8</p>\r\n\r\n<p>//for update<br>\r\nlang_known_74:English<br>\r\nproficiency_74:Average</p>\r\n\r\n<p><span xss=removed>checkbox</span>_74: reading,writing,speaking</p>\r\n\r\n<p>// for add new</p>\r\n\r\n<p>&lt;!--StartFragment--&gt;<span xss=removed>lang_known_a_2 </span>:English<br>\r\n<span xss=removed>proficiency_a_2</span>:Average<br>\r\n<span xss=removed>checkbox_a_2</span>: reading,writing,speaking</p>', '<p>{<br>\r\n  \"tocken\": \"22f4b0e7563646ddf9b1014e35495c7a\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"successmessage\": \"Your account has been updated successfully.\"<br>\r\n}</p>', '', 'No'),
(43, 'APPROVED', 'Message Read status update', 'http://192.168.1.111/job_portal/my-message/message_readstatus', 'POST', '<p>Call this service for update message status unread to read, when message status unread</p>', '<ol>\r\n <li>\r\n <p>user_agent = NI-AAPP</p>\r\n </li>\r\n <li>\r\n <p>csrf_job_portal: </p>\r\n </li>\r\n <li>\r\n <p>message_id: 71</p>\r\n </li>\r\n</ol>', '<p>{\"token\":\"dfae968897e5c7076ce0e996ace96820\",\"status\":\"success\"}</p>', '', 'No'),
(44, 'APPROVED', 'My Message', 'http://192.168.1.111/job_portal/my-message/index', 'POST', '<p>This service is used for getting messge for emploer and jobseeker, with inbox, sent, and draft also with same Service with different perameter value passed.</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>', '<ul>\r\n <li>user_agent - NI-AAPP</li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n <li>message_type - inbox   (Possible Value -  inbox / sent / draft)</li>\r\n <li>page_number - 1 (as you go scroll down pass page number, You can ignore this default value is 1)</li>\r\n <li>search_message - search  (You can search message by username you can ignore this filed if not searching)</li>\r\n</ul>', '<p>{\"status\":\"success\",\"data\":[{\"id\":\"68\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: Re: Re: new message from emp\",\"content\":\"okr sdrdg dsdfg sddgdf\",\"status\":\"sent\",\"sent_on\":\"2017-05-10 13:05:42\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"67\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: test web sehb f\",\"content\":\"kdf bhdsf bsdjsedf sdfg\",\"status\":\"sent\",\"sent_on\":\"2017-05-10 11:29:47\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"66\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: fgdf\",\"content\":\"gfdgf dgdf\",\"status\":\"sent\",\"sent_on\":\"2017-05-10 11:29:34\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"41\",\"sender\":\"71\",\"receiver\":\"21\",\"subject\":\"yuug safdhg\",\"content\":\"hgj\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 13:26:53\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Read\",\"fullname\":\"Emp test mansuri\",\"email\":\"mtest@gmail.com\",\"user_id\":\"21\"},{\"id\":\"33\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: fgdf\",\"content\":\"sd ds gsdg\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 11:32:02\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"32\",\"sender\":\"71\",\"receiver\":\"21\",\"subject\":\"Re: From employer\",\"content\":\"ds ffdg\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 11:31:47\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Read\",\"fullname\":\"Emp test mansuri\",\"email\":\"mtest@gmail.com\",\"user_id\":\"21\"},{\"id\":\"29\",\"sender\":\"71\",\"receiver\":\"21\",\"subject\":\"terter\",\"content\":\"te rt\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 10:40:10\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"Emp test mansuri\",\"email\":\"mtest@gmail.com\",\"user_id\":\"21\"},{\"id\":\"28\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: Re: Re: new message from emp\",\"content\":\"fgchh\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 10:00:22\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Read\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"26\",\"sender\":\"71\",\"receiver\":\"64\",\"subject\":\"Re: new message from emp\",\"content\":\"ok nop\\r\\ncan you resend me new message\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 09:55:52\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Read\",\"fullname\":\"test Mansuri\",\"email\":\"testbhai@gmail.com\",\"user_id\":\"64\"},{\"id\":\"24\",\"sender\":\"71\",\"receiver\":\"21\",\"subject\":\"Re: From employer\",\"content\":\"Test\",\"status\":\"sent\",\"sent_on\":\"2017-05-09 09:49:29\",\"trash_sender\":\"No\",\"trash_receiver\":\"No\",\"sender_type\":\"job_seeker\",\"read_status\":\"Unread\",\"fullname\":\"Emp test mansuri\",\"email\":\"mtest@gmail.com\",\"user_id\":\"21\"}],\"token\":\"a054217234d211a87fd6df289136293f\"}</p>', '<p>{\"status\":\"error\",\"errorMessage\":\"Data Not Found\",\"token\":\"a054217234d211a87fd6df289136293f\"}</p>', 'No'),
(45, 'APPROVED', 'My Message - Count', 'http://192.168.1.111/job_portal/my-message/get_message_count', 'POST', '<p>This service is used for get count of inbox, sent and draft with all count</p>', '<ul>\r\n <li>user_agent - NI-AAPP</li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n</ul>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": {<br>\r\n    \"inbox\": 10,<br>\r\n    \"sent\": 45,<br>\r\n    \"draft\": 0<br>\r\n  },<br>\r\n  \"token\": \"a054217234d211a87fd6df289136293f\"<br>\r\n}</p>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": {<br>\r\n    \"inbox\": 10,<br>\r\n    \"sent\": 45,<br>\r\n    \"draft\": 0<br>\r\n  },<br>\r\n  \"token\": \"a054217234d211a87fd6df289136293f\"<br>\r\n}</p>', 'No'),
(46, 'APPROVED', 'My Message - draft and send message', 'http://192.168.1.111/job_portal/my-message/send_message', 'POST', '<p>This service is used to send message to user and also for save draft message based on perameter passed,</p>', '<ul>\r\n <li>user_agent - NI-AAPP</li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n <li>to_message - 21 (Job seeker or employer id)</li>\r\n <li>message_action - draft   (Posible value - draft / sent )</li>\r\n <li>draft_id - 92   (Leave blank when new draft, pass id when you edit draft message) or also pass draft_id when you sent message from saved draft</li>\r\n <li>subject -  Subject of message</li>\r\n <li>content - Content of message to send</li>\r\n <li> </li>\r\n</ul>', '<p>{<br>\r\n  \"token\": \"a054217234d211a87fd6df289136293f\",<br>\r\n  \"draft_id\": \"92\",<br>\r\n  \"errmessage\": \"Massage saved successfully\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '<p>{<br>\r\n  \"token\": \"a054217234d211a87fd6df289136293f\",<br>\r\n  \"errmessage\": \"Sorry, You have no credit balance to send message.\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>', 'No'),
(47, 'APPROVED', 'My Message - Delete message', 'http://192.168.1.111/job_portal/my-message/delete_msg', 'POST', '<p>Call this service for delete message</p>', '<ol>\r\n <li>\r\n <p>user_agent = NI-AAPP</p>\r\n </li>\r\n <li>\r\n <p>csrf_job_portal: </p>\r\n </li>\r\n <li>\r\n <p>message_id: 5   (Comma seperated id like 1,2,11,13)</p>\r\n </li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n <li>message_type - inbox   (Possible Value -  inbox / sent / draft)</li>\r\n</ol>', '<p>{\"status\":\"success\",\"message\":\"Message deleted successfully.\",\"token\":\"3e326393971de3e3d67a54f17c6460ca\"}</p>', '<p>{\"status\":\"error\",\"message\":\"Please select atleast one message to delete\",\"token\":\"3e326393971de3e3d67a54f17c6460ca\"}</p>', 'No'),
(48, 'APPROVED', 'My message - Get Job seeker list', 'http://192.168.1.111/job_portal/my-message/get_receiverList', 'POST', '', '<p>user_agent : NI-AAPP</p>\r\n\r\n<p>user_type : job_seeker  (you can also pass employer)</p>\r\n\r\n<p>user_id : 71</p>', '<pre>\r\n{\r\n  \"token\": \"3e326393971de3e3d67a54f17c6460ca\",\r\n  \"status\": \"success\",\r\n  \"data\": [\r\n    {\r\n      \"id\": \"10\",\r\n      \"text\": \"Ketan Parmar12(ketanparmar583@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"61\",\r\n      \"text\": \"jay(jay.narjisenterprise@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"71\",\r\n      \"text\": \"test Mansuri(test@narjisenterprise.com)\"\r\n    },\r\n    {\r\n      \"id\": \"136\",\r\n      \"text\": \"Jay testing(jaytetingreg@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"138\",\r\n      \"text\": \"for plan(for@plan.com)\"\r\n    },\r\n    {\r\n      \"id\": \"139\",\r\n      \"text\": \"for plan(for@plan1.com)\"\r\n    },\r\n    {\r\n      \"id\": \"143\",\r\n      \"text\": \"Tauhid Kovadiya(test@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"159\",\r\n      \"text\": \"test fdfdg(testmansuri@narjis.com)\"\r\n    },\r\n    {\r\n      \"id\": \"181\",\r\n      \"text\": \"Narjis demo(Narjisdemo@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"182\",\r\n      \"text\": \"This is jay(jay.finaltesting@gmail.com)\"\r\n    },\r\n    {\r\n      \"id\": \"196\",\r\n      \"text\": \"test verify by  email(verify@gmail.com)\"\r\n    }\r\n  ]\r\n}</pre>', '', 'No'),
(49, 'APPROVED', 'My message - Get Employer list', 'http://192.168.1.111/job_portal/my-message/get_employerList', 'POST', '', '<p>user_agent : NI-AAPP</p>\r\n\r\n<p>user_type : job_seeker</p>\r\n\r\n<p>user_id : 71</p>', '<p>{<br>\r\n  \"token\": \"3e326393971de3e3d67a54f17c6460ca\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": [<br>\r\n    {<br>\r\n      \"id\": \"7\",<br>\r\n      \"text\": \"test(test)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"11\",<br>\r\n      \"text\": \"jay thakkar(Narjis Enterprise)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"12\",<br>\r\n      \"text\": \"ketan(test test)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"21\",<br>\r\n      \"text\": \"Emp test mansuri(Narjis Infotech pvt limited)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"32\",<br>\r\n      \"text\": \"testing(ters)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"37\",<br>\r\n      \"text\": \"kapil patel(narjis)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"64\",<br>\r\n      \"text\": \"test Mansuri(Narjis Infotech pvt limited)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"70\",<br>\r\n      \"text\": \"test test(Test New empse df)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"71\",<br>\r\n      \"text\": \"Navab(Navab Techy)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"75\",<br>\r\n      \"text\": \"Tohid M Kovadiya1(Baburao Marriage Bureau)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"79\",<br>\r\n      \"text\": \"Riddhish H Pathak(dgdfgdsf gdsfgsdfg)\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'Yes');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(50, 'APPROVED', 'Job listing', 'http://192.168.1.111/job_portal/job-listing/posted-job', 'POST', '', '<p>user_agent : </p>\r\n\r\n<pre>\r\nNI-AAPP</pre>\r\n\r\n<p>csrf_job_portal :  98ba32168ef5f07070dd0531b84cb857</p>', '<p>{<br>\r\n  \"base_url\": \"http://192.168.1.111/job_portal/\",<br>\r\n  \"total_post_count\": 32,<br>\r\n  \"emp_posted_job\": [<br>\r\n    {<br>\r\n      \"id\": \"41\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"job_description\": \"As per company rules and regulations\",<br>\r\n      \"skill_keyword\": \"ICWA, Cost Accountant\",<br>\r\n      \"location_hiring\": \"1,2,3,4,5,6,7,8,9,11\",<br>\r\n      \"company_hiring\": \"Latest test by ketan for demo\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"3\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"21\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"2.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"24,25\",<br>\r\n      \"total_requirenment\": \"2\",<br>\r\n      \"desire_candidate_profile\": \"Latest test by ketan for demo\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Agriculture/Dairy\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Independent Rep.\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"40\",<br>\r\n      \"job_title\": \"Java\",<br>\r\n      \"job_description\": \"Advance Java Web Developer\",<br>\r\n      \"skill_keyword\": \"Data Analyst, Independent Rep., Collections Exec., Financial Analyst\",<br>\r\n      \"location_hiring\": \"1,2,3,4,5,6,7,9\",<br>\r\n      \"company_hiring\": \"dfsdfgsdgsdfgsdfg\",<br>\r\n      \"link_job\": \"http://sdfgd.sdfg\",<br>\r\n      \"job_industry\": \"3\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"21\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"0.6\",<br>\r\n      \"job_salary_from\": \"1.0\",<br>\r\n      \"job_salary_to\": \"1.0\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"24,25,2,3,10,11,4,12,5,6,16,17,18,7,8,9,13,14,15,19,23,22,21,1,26,27,28,29,30,31,32,33,34,35,36,37,38,42,43,44,48,39,45,40,41,47,46\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"ertert yrty rtyr tyrty rtyrt yrty rtyrtyrtrty\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"79\",<br>\r\n      \"posted_on\": \"2017-05-13 13:44:47\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Agriculture/Dairy\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Independent Rep.\",<br>\r\n      \"posted_by_employee\": \"Riddhish H Pathak\",<br>\r\n      \"email\": \"riddhishpathak9@gmail.com\",<br>\r\n      \"profile_pic\": \"50ba3b1190c091cdf807c8ce3a3cb051.jpg\",<br>\r\n      \"company_name\": \"dgdfgdsf gdsfgsdfg\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"dfgdfghfh dg hdfgh dfghdf ghdfg hdfgh fdghdf hdfhx dsfgsdfgdfsg sdfgdsfgd\",<br>\r\n      \"company_website\": \"http://google.com\",<br>\r\n      \"company_email\": \"\",<br>\r\n      \"company_logo\": \"1302e01a4be2ae6ffca835ee355d4613.jpg\",<br>\r\n      \"company_logo_approval\": \"UNAPPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"35\",<br>\r\n      \"job_title\": \"php developer\",<br>\r\n      \"job_description\": \"Location Hiring Location Hiring\\r\\nLocation HiringLocation HiringLocation HiringLocation HiringLocation Hiring\\r\\nLocation HiringLocation HiringLocation HiringLocation Hiring\",<br>\r\n      \"skill_keyword\": \"Accounting, PHP, JavaScript, Ajax jqury, as, Asynch\",<br>\r\n      \"location_hiring\": \"1,4\",<br>\r\n      \"company_hiring\": \"Navab techy\",<br>\r\n      \"link_job\": \"http://www.google.com\",<br>\r\n      \"job_industry\": \"10\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"22\",<br>\r\n      \"work_experience_from\": \"2.0\",<br>\r\n      \"work_experience_to\": \"2.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"6\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"3,12\",<br>\r\n      \"total_requirenment\": \"4\",<br>\r\n      \"desire_candidate_profile\": \"Data updated successfully.Data updated successfully.\\r\\nData updated successfully.Data updated successfully.Data updated successfully.\\r\\nData updated successfully.\\r\\nData updated successfully.Data updated successfully.\\r\\nData updated successfully.Data updated successfully.Data updated successfully.\\r\\nData updated successfully.\\r\\nData updated successfully.\",<br>\r\n      \"job_payment\": \"0\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"71\",<br>\r\n      \"posted_on\": \"2017-05-03 11:07:02\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Brewery / Distillery\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Life-Insurance Agent\",<br>\r\n      \"posted_by_employee\": \"Navab\",<br>\r\n      \"email\": \"navab@facebook.com\",<br>\r\n      \"profile_pic\": \"9d3a669b712c10c386bc51d63380b365.jpg\",<br>\r\n      \"company_name\": \"Navab Techy\",<br>\r\n      \"profile_pic_approval\": \"UNAPPROVED\",<br>\r\n      \"company_profile\": \"ompany Profileompany Profile\\r\\nompany Profileompany Profileompany Profileompany Profile\\r\\nompany Profileompany Profileompany Profileompany Profileompany Profile\\r\\nompany Profileompany Profileompany Profileompany Profileompany Profileompany Profile\",<br>\r\n      \"company_website\": \"http://www.navab.com\",<br>\r\n      \"company_email\": \"info@navab.com\",<br>\r\n      \"company_logo\": \"92ba6180d2cc3caa9000e4d56bc450b7.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"34\",<br>\r\n      \"job_title\": \"njk\",<br>\r\n      \"job_description\": \"fgn\",<br>\r\n      \"skill_keyword\": \"ICWA,Forex Mgr\",<br>\r\n      \"location_hiring\": \"176,180\",<br>\r\n      \"company_hiring\": \"hn\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"3\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"1\",<br>\r\n      \"work_experience_from\": \"2.0\",<br>\r\n      \"work_experience_to\": \"5.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"2.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"2\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"bnkn\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-02 07:06:50\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Agriculture/Dairy\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Accounts Exec./Accountant\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"33\",<br>\r\n      \"job_title\": \"hdbb\",<br>\r\n      \"job_description\": \"vhs\",<br>\r\n      \"skill_keyword\": \"Accounts Exec./Accountant,ICWA\",<br>\r\n      \"location_hiring\": \"1,3,176,181\",<br>\r\n      \"company_hiring\": \"bzb\",<br>\r\n      \"link_job\": \"http://test.com\",<br>\r\n      \"job_industry\": \"4\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"1\",<br>\r\n      \"work_experience_from\": \"2.0\",<br>\r\n      \"work_experience_to\": \"2.6\",<br>\r\n      \"job_salary_from\": \"3.5\",<br>\r\n      \"job_salary_to\": \"3.0\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"25\",<br>\r\n      \"total_requirenment\": \"15\",<br>\r\n      \"desire_candidate_profile\": \"zvbb\",<br>\r\n      \"job_payment\": \"1\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-02 07:01:10\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Animation\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Accounts Exec./Accountant\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"32\",<br>\r\n      \"job_title\": \"networking engg\",<br>\r\n      \"job_description\": \"networking engg\",<br>\r\n      \"skill_keyword\": \"Cost Accountant\",<br>\r\n      \"location_hiring\": \"1,4\",<br>\r\n      \"company_hiring\": \"networking engg\",<br>\r\n      \"link_job\": \"http://test.com\",<br>\r\n      \"job_industry\": \"2\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"5\",<br>\r\n      \"work_experience_from\": \"0.6\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"As per company rules and regulations\",<br>\r\n      \"job_salary_to\": \"\",<br>\r\n      \"currency_type\": \"\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"24,25\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"networking engg\",<br>\r\n      \"job_payment\": \"1\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-02 06:52:39\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Accounts Mgr\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"31\",<br>\r\n      \"job_title\": \"networking engg\",<br>\r\n      \"job_description\": \"networking engg\",<br>\r\n      \"skill_keyword\": \"Cost Accountant\",<br>\r\n      \"location_hiring\": \"1,4\",<br>\r\n      \"company_hiring\": \"networking engg\",<br>\r\n      \"link_job\": \"http://test.com\",<br>\r\n      \"job_industry\": \"2\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"5\",<br>\r\n      \"work_experience_from\": \"0.6\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"As per company rules and regulations\",<br>\r\n      \"job_salary_to\": \"\",<br>\r\n      \"currency_type\": \"\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"24,25\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"networking engg\",<br>\r\n      \"job_payment\": \"1\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-02 06:52:39\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Accounts Mgr\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"30\",<br>\r\n      \"job_title\": \"networking engg\",<br>\r\n      \"job_description\": \"networking engg\",<br>\r\n      \"skill_keyword\": \"Cost Accountant\",<br>\r\n      \"location_hiring\": \"1,4\",<br>\r\n      \"company_hiring\": \"networking engg\",<br>\r\n      \"link_job\": \"http://test.com\",<br>\r\n      \"job_industry\": \"2\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"5\",<br>\r\n      \"work_experience_from\": \"0.6\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"As per company rules and regulations\",<br>\r\n      \"job_salary_to\": \"\",<br>\r\n      \"currency_type\": \"\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"24,25\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"networking engg\",<br>\r\n      \"job_payment\": \"1\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-02 06:52:39\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Accounts Mgr\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"27\",<br>\r\n      \"job_title\": \"New POst by ketan for CE Engg\",<br>\r\n      \"job_description\": \"New POst by ketan for CE Engg\",<br>\r\n      \"skill_keyword\": \"Accounts Exec./Accountant\",<br>\r\n      \"location_hiring\": \"1,156,2,4\",<br>\r\n      \"company_hiring\": \"CE Engg. manager\",<br>\r\n      \"link_job\": \"http://test.com\",<br>\r\n      \"job_industry\": \"1\",<br>\r\n      \"functional_area\": \"1\",<br>\r\n      \"job_post_role\": \"1\",<br>\r\n      \"work_experience_from\": \"0.6\",<br>\r\n      \"work_experience_to\": \"1.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"2.0\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"24,10,11,4\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"Test by narjis enterprise\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-04-26 05:39:30\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Accounting/Finance\",<br>\r\n      \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Accounts Exec./Accountant\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"26\",<br>\r\n      \"job_title\": \"Accountant\",<br>\r\n      \"job_description\": \"Position: PHP Developer\\r\\n\\r\\nLooking for STRONG candidates who have more than 2 Years of hands on experience on the following platforms :\\r\\n\\r\\n1. Proven software development experience in PHP\\r\\n\\r\\n2. Understanding of open source projects like Wordpress, Drupal, Wikis, osCommerce, etc\\r\\n\\r\\n3. Demonstrable knowledge of web technologies including HTML, CSS, jquery, AJAX etc\\r\\n\\r\\n4. Good knowledge of relational databases, version control tools(Github) and of developing web services\\r\\n\\r\\n5. Experience in REST client API, Angular.js\\r\\n\\r\\n6. Experience in common third-party APIs (Amadeus, Sabre, Google, Facebook, Ebay etc)\\r\\n\\r\\n7. Passion for best design and coding practices and a desire to develop new bold ideas\",<br>\r\n      \"skill_keyword\": \"Accounts Exec./Accountant\",<br>\r\n      \"location_hiring\": \"24,58,5,57\",<br>\r\n      \"company_hiring\": \"Accountant manager\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"1\",<br>\r\n      \"functional_area\": \"5\",<br>\r\n      \"job_post_role\": \"106\",<br>\r\n      \"work_experience_from\": \"0.6\",<br>\r\n      \"work_experience_to\": \"1.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"5.5\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"11,20,31,35\",<br>\r\n      \"total_requirenment\": \"4\",<br>\r\n      \"desire_candidate_profile\": \"Accountant manager should have all knowledge\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-04-26 05:39:30\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Accounting/Finance\",<br>\r\n      \"functional_name\": \"Banking / Insurance\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Asset Manager\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(51, 'APPROVED', 'Get Company List', 'http://192.168.1.111/job_portal/sign_up/get_suggestion_list', 'POST', '<p>Get company List</p>', '<p>user_agent:NI-AAPP<br>\r\ncsrf_job_portal:b62f5c8f191f8c92a5e879665073bd9f<br>\r\nget_list:company_list<br>\r\nreturn_val:id</p>', '<p>{<br>\r\n  \"data\": [<br>\r\n    {<br>\r\n      \"id\": \"75\",<br>\r\n      \"val\": \"Baburao Marriage Bureau\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"79\",<br>\r\n      \"val\": \"dgdfgdsf gdsfgsdfg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"37\",<br>\r\n      \"val\": \"narjis\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"11\",<br>\r\n      \"val\": \"Narjis Enterprise\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"21\",<br>\r\n      \"val\": \"Narjis Infotech pvt limited\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"71\",<br>\r\n      \"val\": \"Navab Techy\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"32\",<br>\r\n      \"val\": \"ters\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"7\",<br>\r\n      \"val\": \"test\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"70\",<br>\r\n      \"val\": \"Test New empse df\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"12\",<br>\r\n      \"val\": \"test test\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(52, 'APPROVED', 'view job full details', 'http://192.168.1.111/job_portal/job-listing/view-job-details', 'POST', '', '<p>job_id = 52</p>\r\n\r\n<p>csraf_job_portal</p>\r\n\r\n<p>user_agent = NI-AAP</p>', '<p>{<br>\r\n  \"base_url\": \"http://192.168.1.111/job_portal/\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"job_details\": [<br>\r\n    {<br>\r\n      \"id\": \"52\",<br>\r\n      \"job_title\": \"Test job\",<br>\r\n      \"job_description\": \"Test\",<br>\r\n      \"skill_keyword\": \"Test, 3D Studio Max\",<br>\r\n      \"location_hiring\": \"249\",<br>\r\n      \"company_hiring\": \"Test\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"18\",<br>\r\n      \"functional_area\": \"14\",<br>\r\n      \"job_post_role\": \"308\",<br>\r\n      \"work_experience_from\": \"7.0\",<br>\r\n      \"work_experience_to\": \"4.6\",<br>\r\n      \"job_salary_from\": \"1\",<br>\r\n      \"job_salary_to\": \"4.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"10\",<br>\r\n      \"total_requirenment\": \"16\",<br>\r\n      \"desire_candidate_profile\": \"sdfrrd\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 11:15:24\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Education/Teaching/Training\",<br>\r\n      \"functional_name\": \"Hotels / Restaurants\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Commis\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(53, 'APPROVED', 'Apply Job', 'http://192.168.1.111/job_portal/job_seeker_action/apply_for_job', 'POST', '<p>Applying for Job</p>', '<p>csrf_job_portal     --a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\njob_id     --53<br>\r\nuser_agent    --NI-WEB</p>\r\n\r\n<p>user_id-61</p>', '<p>{<br>\r\n  \"token\": \"3de5cda61f8a66a9166f5c08b9102ea7\",<br>\r\n  \"errmessage\": \"Applcation submitted successfully.\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '', 'No'),
(54, 'APPROVED', 'Save job', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>save_job<br>\r\naction_for    --&gt;job<br>\r\naction_id    --&gt;53</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p><br>\r\n{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"This job is saved in your save job list\",\"status\"<br>\r\n:\"success\"}</p>', '', 'No'),
(55, 'APPROVED', 'Remove from Save job', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>remove_save_job<br>\r\naction_for    --&gt;job<br>\r\naction_id    --&gt;53</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p><br>\r\n{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"This job is saved in your save job list\",\"status\"<br>\r\n:\"success\"}<br>\r\n </p>', '', 'No'),
(56, 'APPROVED', 'Like job & Un Like job', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>like_job   or    unlike_job<br>\r\naction_for    --&gt;job<br>\r\naction_id    --&gt;53</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p>{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"Action for like job successfully performed\"<br>\r\n,\"status\":\"success\"}<br>\r\n </p>', '', 'No'),
(57, 'APPROVED', 'Block emp & UNBlock emp', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>block_emp   or   unblock_emp<br>\r\naction_for    --&gt;Emp<br>\r\naction_id    --&gt;64</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p><br>\r\n{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"Employer removed from  block list \",\"status\"<br>\r\n:\"success\"}</p>', '', 'No'),
(58, 'APPROVED', 'Foollow emp & UnFoollow emp', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>    unfollow_emp   or   follow_emp<br>\r\naction_for    --&gt;Emp<br>\r\naction_id    --&gt;64</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p><br>\r\n{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"This employer removed from following list\"<br>\r\n,\"status\":\"success\"}</p>', '', 'No'),
(59, 'APPROVED', 'LIke emp & UnLIke em', 'http://192.168.1.111/job_portal/job-seeker-action/seeker-action', 'POST', '', '<p>action     --&gt;>         unlike_emp   or  like_emp<br>\r\naction_for    --&gt;Emp<br>\r\naction_id    --&gt;64</p>\r\n\r\n<p>csrf_job_portal     a126ab6e0eb2c2b1d5a594b261f5508a<br>\r\nuser_agent     NI-WEB</p>\r\n\r\n<p>user_id</p>', '<p>{\"token\":\"a126ab6e0eb2c2b1d5a594b261f5508a\",\"errmessage\":\"Now you are following this employer\",\"status\"<br>\r\n:\"success\"}</p>', '', 'No'),
(60, 'APPROVED', 'get View Count', 'Common_request/get_counter', 'POST', '', '<p>counter_id --&gt;> set job id</p>', '', '', 'No'),
(61, 'APPROVED', 'get job action', 'common_request/check_job_seeker_action', 'POST', '', '<p> js_id=userid</p>\r\n\r\n<p>job_id</p>\r\n\r\n<p>emp_id</p>', '', '', 'No'),
(62, 'APPROVED', 'My plan list', 'http://192.168.1.111/job_portal/my-plan/index', 'POST', '<p>To get membership plan</p>', '<ul>\r\n <li>user_agent - NI-AAPP</li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n <li>tocken - tocken </li>\r\n</ul>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": [<br>\r\n    {<br>\r\n      \"id\": \"2\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"plan_name\": \"Start Up\",<br>\r\n      \"plan_type\": \"Paid\",<br>\r\n      \"plan_currency\": \"INR\",<br>\r\n      \"plan_amount\": \"500\",<br>\r\n      \"plan_duration\": \"30\",<br>\r\n      \"message\": \"15\",<br>\r\n      \"highlight_application\": \"No\",<br>\r\n      \"relevant_jobs\": \"No\",<br>\r\n      \"performance_report\": \"Yes\",<br>\r\n      \"contacts\": \"15\",<br>\r\n      \"job_post_notification\": \"No\",<br>\r\n      \"plan_offers\": \"\",<br>\r\n      \"created_on\": \"2017-05-01 08:53:45\",<br>\r\n      \"is_deleted\": \"No\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"3\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"plan_name\": \"Company\",<br>\r\n      \"plan_type\": \"Paid\",<br>\r\n      \"plan_currency\": \"INR\",<br>\r\n      \"plan_amount\": \"1500\",<br>\r\n      \"plan_duration\": \"55\",<br>\r\n      \"message\": \"35\",<br>\r\n      \"highlight_application\": \"Yes\",<br>\r\n      \"relevant_jobs\": \"Yes\",<br>\r\n      \"performance_report\": \"Yes\",<br>\r\n      \"contacts\": \"35\",<br>\r\n      \"job_post_notification\": \"Yes\",<br>\r\n      \"plan_offers\": \"\",<br>\r\n      \"created_on\": \"2017-05-03 11:03:23\",<br>\r\n      \"is_deleted\": \"No\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"4\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"plan_name\": \"Enterprise\",<br>\r\n      \"plan_type\": \"Paid\",<br>\r\n      \"plan_currency\": \"INR\",<br>\r\n      \"plan_amount\": \"3000\",<br>\r\n      \"plan_duration\": \"70\",<br>\r\n      \"message\": \"55\",<br>\r\n      \"highlight_application\": \"Yes\",<br>\r\n      \"relevant_jobs\": \"Yes\",<br>\r\n      \"performance_report\": \"Yes\",<br>\r\n      \"contacts\": \"55\",<br>\r\n      \"job_post_notification\": \"Yes\",<br>\r\n      \"plan_offers\": \"\",<br>\r\n      \"created_on\": \"2017-05-03 11:03:56\",<br>\r\n      \"is_deleted\": \"No\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"5\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"plan_name\": \"Extended\",<br>\r\n      \"plan_type\": \"Paid\",<br>\r\n      \"plan_currency\": \"INR\",<br>\r\n      \"plan_amount\": \"5000\",<br>\r\n      \"plan_duration\": \"85\",<br>\r\n      \"message\": \"75\",<br>\r\n      \"highlight_application\": \"Yes\",<br>\r\n      \"relevant_jobs\": \"Yes\",<br>\r\n      \"performance_report\": \"Yes\",<br>\r\n      \"contacts\": \"75\",<br>\r\n      \"job_post_notification\": \"Yes\",<br>\r\n      \"plan_offers\": \"\",<br>\r\n      \"created_on\": \"2017-05-03 11:04:13\",<br>\r\n      \"is_deleted\": \"No\"<br>\r\n    }<br>\r\n  ],<br>\r\n  \"tocken\": \"7a2b2d1458d44d63303a6487b3cccece\"<br>\r\n}</p>', '<p>{\"status\":\"error\",\"errorMessage\":\"Data Not Found\",\"tocken\":\"7a2b2d1458d44d63303a6487b3cccece\"}</p>', 'No'),
(63, 'APPROVED', 'My plan detail', 'http://192.168.1.111/job_portal/my-plan/current_plan', 'POST', '<p>For get plan detail</p>', '<ul>\r\n <li>user_agent - NI-AAPP</li>\r\n <li>user_type - job_seeker           (You can pass job_seeker / employer as current user login)</li>\r\n <li>user_id - 71    (User id of current logged in user same perameter for both user type)</li>\r\n <li>tocken - tocken </li>\r\n</ul>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"data\": {<br>\r\n    \"id\": \"17\",<br>\r\n    \"js_id\": \"71\",<br>\r\n    \"plan_id\": \"3\",<br>\r\n    \"current_plan\": \"Yes\",<br>\r\n    \"plan_name\": \"Company\",<br>\r\n    \"plan_type\": \"Paid\",<br>\r\n    \"plan_currency\": \"INR\",<br>\r\n    \"plan_amount\": \"150\",<br>\r\n    \"plan_duration\": \"45\",<br>\r\n    \"message\": \"1450\",<br>\r\n    \"message_used\": \"64\",<br>\r\n    \"highlight_application\": \"Yes\",<br>\r\n    \"relevant_jobs\": \"Yes\",<br>\r\n    \"performance_report\": \"Yes\",<br>\r\n    \"contacts\": \"45\",<br>\r\n    \"contacts_used\": \"6\",<br>\r\n    \"job_post_notification\": \"No\",<br>\r\n    \"plan_offers\": \"\",<br>\r\n    \"coupan_code\": \"DISCOUNT15\",<br>\r\n    \"discount_amount\": \"15\",<br>\r\n    \"service_tax\": \"20.25\",<br>\r\n    \"final_amount\": \"155.25\",<br>\r\n    \"activated_on\": \"2017-05-04\",<br>\r\n    \"expired_on\": \"2017-06-03\",<br>\r\n    \"payment_method\": \"Credit Card/ Online Transfer\",<br>\r\n    \"transaction_id\": \"1111186520\",<br>\r\n    \"payment_note\": \"\",<br>\r\n    \"is_deleted\": \"No\"<br>\r\n  },<br>\r\n  \"token\": \"7a2b2d1458d44d63303a6487b3cccece\"<br>\r\n}</p>', '<p>{\"status\":\"error\",\"errorMessage\":\"Data Not Found\",\"token\":\"7a2b2d1458d44d63303a6487b3cccece\"}</p>', 'No'),
(64, 'APPROVED', 'for getting all recruiters of the site', 'http://192.168.1.111/job_portal/recruiters', 'POST', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:67fdcd8e48ccc76c1e424686fe5c80d5<br>\r\npage:1<br>\r\nlimit_per_page:2</p>', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:67fdcd8e48ccc76c1e424686fe5c80d5<br>\r\npage:1<br>\r\nlimit_per_page:2</p>', '<p>{<br>\r\n  \"data\": [<br>\r\n    {<br>\r\n      \"id\": \"79\",<br>\r\n      \"email\": \"riddhishpathak9@gmail.com\",<br>\r\n      \"email_ver_str\": \"VmdxiLKDS5p8mFF\",<br>\r\n      \"email_ver_status\": \"No\",<br>\r\n      \"title\": \"1\",<br>\r\n      \"fullname\": \"Riddhish H Pathak\",<br>\r\n      \"designation\": \"2,3,4\",<br>\r\n      \"functional_area\": \"1,4\",<br>\r\n      \"job_profile\": \"hgj gh jghj gh jghjgfhjfgjghgf dfgh dfghd fghdf hfdh d fdsf gdsfg sdfg dsfgsd sdfg sdf\",<br>\r\n      \"email_verified\": \"No\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"password\": \"$2a$08$MKw28dusa9n73rCUKpQMi.2Cp2pWuaOzfsi8MaV5WMuMklQrYdXH6\",<br>\r\n      \"country\": \"1\",<br>\r\n      \"city\": \"1\",<br>\r\n      \"address\": \"dfg hfg hdfgh dfghfdg hdfgh dfhdfhdfghdfdfg hghdfgh\",<br>\r\n      \"pincode\": \"2147483647\",<br>\r\n      \"mobile\": \"+91-8460402610\",<br>\r\n      \"profile_pic\": \"50ba3b1190c091cdf807c8ce3a3cb051.jpg\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_name\": \"dgdfgdsf gdsfgsdfg\",<br>\r\n      \"company_profile\": \"dfgdfghfh dg hdfgh dfghdf ghdfg hdfgh fdghdf hdfhx dsfgsdfgdfsg sdfgdsfgd\",<br>\r\n      \"company_website\": \"http://google.com\",<br>\r\n      \"company_email\": \"\",<br>\r\n      \"company_type\": \"2\",<br>\r\n      \"company_size\": \"2\",<br>\r\n      \"company_logo\": \"1302e01a4be2ae6ffca835ee355d4613.jpg\",<br>\r\n      \"company_logo_approval\": \"UNAPPROVED\",<br>\r\n      \"industry\": \"16\",<br>\r\n      \"industry_hire\": \"2,3,4\",<br>\r\n      \"function_area_hire\": \"1,2,4\",<br>\r\n      \"skill_hire\": \"26,21,27,59\",<br>\r\n      \"register_date\": \"2017-05-13 11:33:06\",<br>\r\n      \"facebook_url\": \"http://google.com\",<br>\r\n      \"gplus_url\": \"http://google.com\",<br>\r\n      \"twitter_url\": \"http://google.com\",<br>\r\n      \"linkedin_url\": \"http://google.com\",<br>\r\n      \"user_agent\": \"NI-WEB\",<br>\r\n      \"device_id\": \"\",<br>\r\n      \"last_login\": \"2017-05-19 06:50:55\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"plan_status\": \"Active\",<br>\r\n      \"plan_name\": \"\",<br>\r\n      \"city_name\": \"Ahmedabad\",<br>\r\n      \"country_name\": \"India\",<br>\r\n      \"personal_titles\": \"Mr.\",<br>\r\n      \"industries_name\": \"Courier/Transportation/Freight\",<br>\r\n      \"company_type_name\": \"Public Listed\",<br>\r\n      \"company_size_name\": \"151-500 Employee\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"75\",<br>\r\n      \"email\": \"test@gmail.com\",<br>\r\n      \"email_ver_str\": \"vwAPAoMAe5sEbV7\",<br>\r\n      \"email_ver_status\": \"No\",<br>\r\n      \"title\": \"1\",<br>\r\n      \"fullname\": \"Tohid M Kovadiya1\",<br>\r\n      \"designation\": \"21\",<br>\r\n      \"functional_area\": \"2,3,4,5,6\",<br>\r\n      \"job_profile\": \"Nothing to say right now, will tell u later on1\",<br>\r\n      \"email_verified\": \"No\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"password\": \"$2a$08$Jy/CV3NJQGvtCmPW7n5EcOBmIzpYIFe4wdULgeruoMOCmkxSnKbv2\",<br>\r\n      \"country\": \"4\",<br>\r\n      \"city\": \"169\",<br>\r\n      \"address\": \"C/318 Burj Khalifa, Down Town, dubai\",<br>\r\n      \"pincode\": \"123456\",<br>\r\n      \"mobile\": \"+91-9123456781\",<br>\r\n      \"profile_pic\": \"807edb7e73ea478188802a78e68fc29d.jpg\",<br>\r\n      \"profile_pic_approval\": \"UNAPPROVED\",<br>\r\n      \"company_name\": \"Baburao Marriage Bureau\",<br>\r\n      \"company_profile\": \"Come here  and get married especially for Riddhish Bhai... Contact baburao !!!!@@@@\",<br>\r\n      \"company_website\": \"https://ww.baburaomb.com\",<br>\r\n      \"company_email\": \"\",<br>\r\n      \"company_type\": \"3\",<br>\r\n      \"company_size\": \"4\",<br>\r\n      \"company_logo\": \"61a873092b57eb4978177d6599fccc96.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\",<br>\r\n      \"industry\": \"42\",<br>\r\n      \"industry_hire\": \"3,42\",<br>\r\n      \"function_area_hire\": \"4,46\",<br>\r\n      \"skill_hire\": \"390,283,397\",<br>\r\n      \"register_date\": \"2017-05-05 06:01:09\",<br>\r\n      \"facebook_url\": null,<br>\r\n      \"gplus_url\": null,<br>\r\n      \"twitter_url\": null,<br>\r\n      \"linkedin_url\": null,<br>\r\n      \"user_agent\": \"NI-WEB\",<br>\r\n      \"device_id\": \"\",<br>\r\n      \"last_login\": \"2017-05-05 06:24:06\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"plan_status\": \"Active\",<br>\r\n      \"plan_name\": \"\",<br>\r\n      \"city_name\": \"Dubai\",<br>\r\n      \"country_name\": \"United Arab Emirates\",<br>\r\n      \"personal_titles\": \"Mr.\",<br>\r\n      \"industries_name\": \"NGO/Social Services\",<br>\r\n      \"company_type_name\": \"Private Limited\",<br>\r\n      \"company_size_name\": \"0-50 Employee\"<br>\r\n    }<br>\r\n  ],<br>\r\n  \"total_recuiters_count\": 12<br>\r\n}</p>', '', 'No'),
(65, 'APPROVED', 'For getting employers active job count as well as total follwer count', 'http://192.168.1.111/job_portal/recruiters/get_follw_cnt_and_act_jog', 'POST', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:67fdcd8e48ccc76c1e424686fe5c80d5<br>\r\nemploer_id:11</p>', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:67fdcd8e48ccc76c1e424686fe5c80d5<br>\r\nemploer_id:11</p>', '<p>{<br>\r\n  \"active_job\": 28,<br>\r\n  \"follower_count\": 6,<br>\r\n  \"tocken\": \"67fdcd8e48ccc76c1e424686fe5c80d5\"<br>\r\n}</p>', '', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(66, 'APPROVED', 'Browse resume and job respose', 'http://192.168.1.111/job_portal/job-application/manage_job_application', 'POST', '<p>for search application post a below perameter</p>\r\n\r\n<p>for paginatio pass is_ajax =1</p>\r\n\r\n<p>also page, limit_per_page</p>\r\n\r\n<p> </p>\r\n\r\n<p>check is_shortlisted  filed if this field value is No then displyu add to shortlist other wise shortlisted</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>search_applied_user</td>\r\n   <td><code>ketanparmar583@gmail.com</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td>\r\n   <p>44</p>\r\n\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>728fb1cb2d8996a284874f0605bcab1a</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>is_ajax</td>\r\n   <td><code>0</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td>search_applied_user</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td><code>undefined</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>11</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>please see in response for this perameter emp_posted_job_list</p>\r\n\r\n<p>this perametr get the posted job id and job title of job which is posted by emploey.</p>\r\n\r\n<p>this is workful in search dropdown</p>', '<p>{<br>\r\n  \"base_url\": \"http://192.168.1.111/job_portal/\",<br>\r\n  \"apply_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"36\",<br>\r\n      \"job_id\": \"41\",<br>\r\n      \"js_id\": \"182\",<br>\r\n      \"is_shortlisted\": \"Yes\",<br>\r\n      \"applied_on\": \"2017-05-18 09:29:30\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"34\",<br>\r\n      \"job_id\": \"41\",<br>\r\n      \"js_id\": \"61\",<br>\r\n      \"is_shortlisted\": \"Yes\",<br>\r\n      \"applied_on\": \"2017-05-17 12:47:30\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\"<br>\r\n    }<br>\r\n  ],<br>\r\n  \"apply_list_count\": 29,<br>\r\n  \"status\": \"success\",<br>\r\n  \"js_img\": \"assets/js_photos\",<br>\r\n  \"emp_posted_job_list\": [<br>\r\n    {<br>\r\n      \"id\": \"2\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"3\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"4\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"5\",<br>\r\n      \"val\": \"Junior developer  (Java)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"6\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"7\",<br>\r\n      \"val\": \"testing\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"8\",<br>\r\n      \"val\": \"testing\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"9\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"10\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"11\",<br>\r\n      \"val\": \"New develo[er\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"12\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"13\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"14\",<br>\r\n      \"val\": \"Interior Designer/furniture Designer\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"16\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"17\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"18\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"19\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"20\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"21\",<br>\r\n      \"val\": \"Test add\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"22\",<br>\r\n      \"val\": \"test new job\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"25\",<br>\r\n      \"val\": \"Chaged by ketan / network engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"26\",<br>\r\n      \"val\": \"Accountant\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"27\",<br>\r\n      \"val\": \"New POst by ketan for CE Engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"30\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"31\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"32\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"33\",<br>\r\n      \"val\": \"hdbb\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"34\",<br>\r\n      \"val\": \"njk\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"41\",<br>\r\n      \"val\": \"Latest test by ketan for demo\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"43\",<br>\r\n      \"val\": \"MObile oprator\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"44\",<br>\r\n      \"val\": \"MObile oprator 2\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"45\",<br>\r\n      \"val\": \"fdg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"47\",<br>\r\n      \"val\": \"sdafadf\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"48\",<br>\r\n      \"val\": \"php\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"55\",<br>\r\n      \"val\": \"Jay thakkar\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"56\",<br>\r\n      \"val\": \"Senior Php developer /fresher for testing job life\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"57\",<br>\r\n      \"val\": \"uykmgy\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(67, 'APPROVED', 'job seeker profile details for employer', 'http://192.168.1.111/job_portal/employer-profile/view_js_details', 'POST', '', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>728fb1cb2d8996a284874f0605bcab1a</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>js_id</td>\r\n   <td><code>182</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>10</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{<br>\r\n  \"user_data\": {<br>\r\n    \"id\": \"182\",<br>\r\n    \"facebook_id\": \"\",<br>\r\n    \"gplus_id\": \"\",<br>\r\n    \"status\": \"APPROVED\",<br>\r\n    \"email\": \"jay.finaltesting@gmail.com\",<br>\r\n    \"password\": \"$2a$08$j.cTdFU27czMI/ImVdF4Ju10XEOTEbKGjaYu0KoxIGUBDEEOlDO6e\",<br>\r\n    \"email_ver_str\": \"\",<br>\r\n    \"verify_email\": \"No\",<br>\r\n    \"title\": \"1\",<br>\r\n    \"fullname\": \"This is jay\",<br>\r\n    \"gender\": \"Male\",<br>\r\n    \"birthdate\": \"2017-05-01\",<br>\r\n    \"marital_status\": \"3\",<br>\r\n    \"address\": \"terst\",<br>\r\n    \"city\": \"71\",<br>\r\n    \"country\": \"1\",<br>\r\n    \"mobile\": \"+91-67676657667\",<br>\r\n    \"landline\": \"079-32434\",<br>\r\n    \"pincode\": \"111111\",<br>\r\n    \"website\": \"http://192.168.1.111/job_portal/sign-up/add-resume\",<br>\r\n    \"profile_pic\": \"3361db08c790ee3b651162ad835d6e2d.jpg\",<br>\r\n    \"profile_pic_approval\": \"APPROVED\",<br>\r\n    \"profile_summary\": \"dfdfdsf\",<br>\r\n    \"home_city\": \"Mumbai\",<br>\r\n    \"preferred_city\": \"1,2,21\",<br>\r\n    \"resume_headline\": \"dsfddsf\",<br>\r\n    \"total_experience\": \"1-1\",<br>\r\n    \"annual_salary\": \"0-45\",<br>\r\n    \"currency_type\": \"GBP\",<br>\r\n    \"industry\": \"2\",<br>\r\n    \"functional_area\": \"2\",<br>\r\n    \"job_role\": \"22\",<br>\r\n    \"key_skill\": \"Conflict Management\",<br>\r\n    \"email_verification\": \"No\",<br>\r\n    \"resume_file\": \"25267f642dad4db0f758a15b5c27e2a5.docx\",<br>\r\n    \"resume_last_update\": \"2017-05-18 11:33:04\",<br>\r\n    \"resume_verification\": \"APPROVED\",<br>\r\n    \"desire_job_type\": \"Permanent, Temporary/Contractual\",<br>\r\n    \"employment_type\": \"Full Time\",<br>\r\n    \"prefered_shift\": \"Night\",<br>\r\n    \"expected_annual_salary\": \"0-5\",<br>\r\n    \"exp_salary_currency_type\": \"CAD\",<br>\r\n    \"register_date\": \"2017-05-13 10:16:02\",<br>\r\n    \"last_login\": \"0000-00-00 00:00:00\",<br>\r\n    \"profile_visibility\": \"Yes\",<br>\r\n    \"facebook_url\": \"http://192.168.1.111/job_portal/sign-up/add-resume\",<br>\r\n    \"gplus_url\": \"http://192.168.1.111/job_portal/sign-up/add-resume\",<br>\r\n    \"twitter_url\": \"http://192.168.1.111/job_portal/sign-up/add-resume\",<br>\r\n    \"linkedin_url\": \"\",<br>\r\n    \"device_id\": \"\",<br>\r\n    \"user_agent\": \"NI-WEB\",<br>\r\n    \"user_ip\": \"\",<br>\r\n    \"plan_status\": \"Active\",<br>\r\n    \"is_deleted\": \"No\",<br>\r\n    \"plan_name\": \"\",<br>\r\n    \"personal_titles\": \"Mr.\",<br>\r\n    \"marital_status_val\": \"Unmarried\",<br>\r\n    \"city_name\": \"Guntur\",<br>\r\n    \"country_name\": \"India\",<br>\r\n    \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n    \"functional_name\": \"Agent\",<br>\r\n    \"role_name\": \"Life-Insurance Agent\"<br>\r\n  },<br>\r\n  \"js_img\": \"assets/js_photos\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '', 'No'),
(68, 'APPROVED', 'For download resume', 'http://192.168.1.111/job_portal/job_application/download_resume', 'POST', '<p>in success we are return file name with full file path.</p>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>728fb1cb2d8996a284874f0605bcab1a</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>file</td>\r\n   <td><code>MjUyNjdmNjQyZGFkNGRiMGY3NThhMTViNWMyN2UyYTUuZG9jeA==</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>jidpkd</td>\r\n   <td><code>MTgy</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>11</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>jidpkd  this perameter values get him with base64_encode()</p>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"file_download\": \"http://192.168.1.111/job_portal/assets/resume_file/25267f642dad4db0f758a15b5c27e2a5.docx\"<br>\r\n}</p>', '<p>{<br>\r\n  \"errmessage\": \"Please upgrade your membership to access the c.v\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p>this response come user not paid or download resume limi is over</p>', 'No'),
(69, 'APPROVED', 'http://192.168.1.111/job_portal/job_application/add_to_shortlist', 'For add resume in shortlist', 'POST', '<p>for add resume in short list</p>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>app_id</td>\r\n   <td><code>32</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>728fb1cb2d8996a284874f0605bcab1a</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>id</td>\r\n   <td><code>32</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>mode</td>\r\n   <td><code>edit</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>11</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{<br>\r\n  \"token\": \"3564f528016ab673d56f3532fa9917ee\",<br>\r\n  \"errmessage\": \"Application added to shortlist.\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '', 'No'),
(70, 'APPROVED', 'get employer liker , follower and blocker listing', 'http://192.168.1.111/job_portal/employer-profile/my-listing', 'POST', '', '<p><span xss=removed>csrf_job_portal</span>  = <span xss=removed>3564f528016ab673d56f3532fa9917ee</span></p>\r\n\r\n<p><span xss=removed>user_agent</span> = <span xss=removed>NI-AAPP</span></p>\r\n\r\n<p><span xss=removed>page</span> = <span xss=removed>1</span></p>\r\n\r\n<p><span xss=removed>emp_id</span>  = <span xss=removed>11</span></p>\r\n\r\n<p><span xss=removed>get_list</span>  = <span xss=removed>like_emp</span></p>\r\n\r\n<p>limit_per_page</p>\r\n\r\n<p>get_list peramerter for different listing</p>\r\n\r\n<p>for like : <span xss=removed>like_emp</span></p>\r\n\r\n<p>for follow : follow_emp</p>\r\n\r\n<p>for block : block_emp</p>\r\n\r\n<p> </p>', '', '<p>{<br>\r\n  \"js_img\": \"assets/js_photos\",<br>\r\n  \"activity_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"3\",<br>\r\n      \"facebook_id\": \"678493265662943\",<br>\r\n      \"gplus_id\": \"\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"email\": \"ketanparmar583@gmail.com\",<br>\r\n      \"password\": \"$2a$08$gIRPmqR1QV4BJHYYAo.yR.9dXOPh95.b6on8t1WbqbDKnskoNmLpe\",<br>\r\n      \"email_ver_str\": \"\",<br>\r\n      \"verify_email\": \"No\",<br>\r\n      \"title\": \"1\",<br>\r\n      \"fullname\": \"Ketan Parmar12\",<br>\r\n      \"gender\": \"Male\",<br>\r\n      \"birthdate\": \"2016-05-09\",<br>\r\n      \"marital_status\": \"4\",<br>\r\n      \"address\": \"j bbgv\",<br>\r\n      \"city\": \"140\",<br>\r\n      \"country\": \"1\",<br>\r\n      \"mobile\": \"+91-6580055455\",<br>\r\n      \"landline\": \"\",<br>\r\n      \"pincode\": \"2131\",<br>\r\n      \"website\": \"\",<br>\r\n      \"profile_pic\": \"5c56cf5d2aadf9751b7783eba6e65d1a.jpg\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"profile_summary\": \"\",<br>\r\n      \"home_city\": \"ahme\",<br>\r\n      \"preferred_city\": \"1,2,21,22\",<br>\r\n      \"resume_headline\": \"ketaf\",<br>\r\n      \"total_experience\": \"2-5\",<br>\r\n      \"annual_salary\": \"2-10\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"industry\": \"34\",<br>\r\n      \"functional_area\": \"5\",<br>\r\n      \"job_role\": \"94\",<br>\r\n      \"key_skill\": \"php,html,cc,jquery\",<br>\r\n      \"email_verification\": \"No\",<br>\r\n      \"resume_file\": \"a7b8653587052302f0b72bb1077aed98.doc\",<br>\r\n      \"resume_last_update\": \"2017-04-10 12:34:44\",<br>\r\n      \"resume_verification\": \"APPROVED\",<br>\r\n      \"desire_job_type\": \"Temporary/Contractual\",<br>\r\n      \"employment_type\": \"Part Time\",<br>\r\n      \"prefered_shift\": \"Flexible\",<br>\r\n      \"expected_annual_salary\": \"2-70\",<br>\r\n      \"exp_salary_currency_type\": \"INR\",<br>\r\n      \"register_date\": \"2017-03-14 05:45:35\",<br>\r\n      \"last_login\": \"2017-06-02 05:45:54\",<br>\r\n      \"profile_visibility\": \"Yes\",<br>\r\n      \"facebook_url\": \"\",<br>\r\n      \"gplus_url\": \"\",<br>\r\n      \"twitter_url\": \"\",<br>\r\n      \"linkedin_url\": \"\",<br>\r\n      \"device_id\": \"\",<br>\r\n      \"user_agent\": \"NI-WEB\",<br>\r\n      \"user_ip\": \"\",<br>\r\n      \"plan_status\": \"Active\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"plan_name\": \"\",<br>\r\n      \"personal_titles\": \"Mr.\",<br>\r\n      \"marital_status_val\": \"Divorced\",<br>\r\n      \"city_name\": \"Tirupati\",<br>\r\n      \"country_name\": \"India\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"functional_name\": \"Banking / Insurance\",<br>\r\n      \"role_name\": \"Debt Analyst\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"emp_id\": \"11\",<br>\r\n      \"is_block\": \"Yes\",<br>\r\n      \"is_follow\": \"Yes\",<br>\r\n      \"is_liked\": \"Yes\",<br>\r\n      \"liked_on\": \"2017-05-04 08:57:54\",<br>\r\n      \"followed_on\": \"2017-05-04 08:57:58\",<br>\r\n      \"blocked_on\": \"2017-05-04 08:57:58\"<br>\r\n    }<br>\r\n  ],<br>\r\n  \"activity_list_count\": 1,<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', 'No'),
(71, 'APPROVED', 'getting short listed application', 'http://192.168.1.111/job_portal/job-application/shortlisted-application', 'POST', '<p>for search application post a below perameter</p>\r\n\r\n<p>for paginatio pass is_ajax =1</p>\r\n\r\n<p>also page, limit_per_page</p>\r\n\r\n<p> </p>\r\n\r\n<p>check is_shortlisted  filed if this field value is No then displyu add to shortlist other wise shortlisted</p>\r\n\r\n<table cellpadding=\"0\" cellspacing=\"0\" class=\"netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>search_applied_user</td>\r\n   <td><code>ketanparmar583@gmail.com</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td>\r\n   <p>44</p>\r\n\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>please see in response for this perameter emp_posted_job_list</p>\r\n\r\n<p>this perametr get the posted job id and job title of job which is posted by emploey.</p>\r\n\r\n<p>this is workful in search dropdown</p>\r\n\r\n<p> </p>', '<table cellpadding=\"0\" cellspacing=\"0\" class=\"netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>728fb1cb2d8996a284874f0605bcab1a</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>is_ajax</td>\r\n   <td><code>0</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td>search_applied_user</td>\r\n   <td> </td>\r\n  </tr>\r\n  <tr>\r\n   <td>job_title_id</td>\r\n   <td><code>undefined</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>11</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{<br>\r\n  \"js_img\": \"assets/js_photos\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"apply_list_count\": 8,<br>\r\n  \"apply_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"36\",<br>\r\n      \"job_id\": \"41\",<br>\r\n      \"js_id\": \"182\",<br>\r\n      \"is_shortlisted\": \"Yes\",<br>\r\n      \"applied_on\": \"2017-05-18 09:29:30\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"34\",<br>\r\n      \"job_id\": \"41\",<br>\r\n      \"js_id\": \"61\",<br>\r\n      \"is_shortlisted\": \"Yes\",<br>\r\n      \"applied_on\": \"2017-05-17 12:47:30\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\"<br>\r\n    }<br>\r\n  ],<br>\r\n  \"emp_posted_job_list\": [<br>\r\n    {<br>\r\n      \"id\": \"2\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"3\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"4\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"5\",<br>\r\n      \"val\": \"Junior developer  (Java)\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"6\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"7\",<br>\r\n      \"val\": \"testing\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"8\",<br>\r\n      \"val\": \"testing\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"9\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"10\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"11\",<br>\r\n      \"val\": \"New develo[er\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"12\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"13\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"14\",<br>\r\n      \"val\": \"Interior Designer/furniture Designer\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"16\",<br>\r\n      \"val\": \"Senior Php developer /fresher\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"17\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"18\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"19\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"20\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"21\",<br>\r\n      \"val\": \"Test add\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"22\",<br>\r\n      \"val\": \"test new job\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"25\",<br>\r\n      \"val\": \"Chaged by ketan / network engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"26\",<br>\r\n      \"val\": \"Accountant\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"27\",<br>\r\n      \"val\": \"New POst by ketan for CE Engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"30\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"31\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"32\",<br>\r\n      \"val\": \"networking engg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"33\",<br>\r\n      \"val\": \"hdbb\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"34\",<br>\r\n      \"val\": \"njk\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"41\",<br>\r\n      \"val\": \"Latest test by ketan for demo\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"43\",<br>\r\n      \"val\": \"MObile oprator\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"44\",<br>\r\n      \"val\": \"MObile oprator 2\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"45\",<br>\r\n      \"val\": \"fdg\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"47\",<br>\r\n      \"val\": \"sdafadf\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"48\",<br>\r\n      \"val\": \"php\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"55\",<br>\r\n      \"val\": \"Jay thakkar\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"56\",<br>\r\n      \"val\": \"Senior Php developer /fresher for testing job life\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"57\",<br>\r\n      \"val\": \"uykmgy\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '', 'No'),
(72, 'APPROVED', 'Add job li highlight', 'http://192.168.1.111/job_portal/job-listing/add-to-highlight', 'POST', '', '<table cellpadding=\"0\" cellspacing=\"0\" class=\" netInfoPostParamsTable\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>40709f294b52fd78643fb693f51ec035</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>id</td>\r\n   <td><code>57</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>mode</td>\r\n   <td><code>edit</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-WEB</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td>11</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{<br>\r\n  \"token\": \"9b256338bfb08e0709339db869843e5e\",<br>\r\n  \"errmessage\": \"Job added to highlight\",<br>\r\n  \"status\": \"success\"<br>\r\n}</p>', '<p>{<br>\r\n  \"token\": \"9b256338bfb08e0709339db869843e5e\",<br>\r\n  \"errmessage\": \"Sorry ! You do not have credit left to highlight this job.\",<br>\r\n  \"status\": \"error\"<br>\r\n}</p>', 'No'),
(73, 'APPROVED', 'Share and view job seeker profile', 'http://192.168.1.111/job_portal/share_profile/js-profile/', 'POST', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>js_id  = MTk2</p>\r\n\r\n<p> </p>\r\n\r\n<p><span xss=removed>pass js_id wirh base64_encode</span></p>', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>js_id  = MTk2</p>', '<p>{<br>\r\n  \"js_img\": \"assets/js_photos\",<br>\r\n  \"js_details\": {<br>\r\n    \"id\": \"196\",<br>\r\n    \"facebook_id\": \"\",<br>\r\n    \"gplus_id\": \"\",<br>\r\n    \"status\": \"APPROVED\",<br>\r\n    \"email\": \"verify@gmail.com\",<br>\r\n    \"password\": \"$2a$08$KS/kV12J5TG5lwWPVu1zSeMH6iFiHtwuwCrHFJ8OKmk4zebkLEk6.\",<br>\r\n    \"email_ver_str\": \"PPUmVNDQFeelT5j\",<br>\r\n    \"verify_email\": \"Yes\",<br>\r\n    \"title\": \"1\",<br>\r\n    \"fullname\": \"test verify by  email\",<br>\r\n    \"gender\": \"Male\",<br>\r\n    \"birthdate\": \"0000-00-00\",<br>\r\n    \"marital_status\": \"0\",<br>\r\n    \"address\": \"\",<br>\r\n    \"city\": \"0\",<br>\r\n    \"country\": \"0\",<br>\r\n    \"mobile\": \"\",<br>\r\n    \"landline\": \"\",<br>\r\n    \"pincode\": \"\",<br>\r\n    \"website\": \"\",<br>\r\n    \"profile_pic\": \"\",<br>\r\n    \"profile_pic_approval\": \"UNAPPROVED\",<br>\r\n    \"profile_summary\": \"\",<br>\r\n    \"home_city\": \"\",<br>\r\n    \"preferred_city\": \"\",<br>\r\n    \"resume_headline\": \"\",<br>\r\n    \"total_experience\": \"\",<br>\r\n    \"annual_salary\": \"\",<br>\r\n    \"currency_type\": \"\",<br>\r\n    \"industry\": \"0\",<br>\r\n    \"functional_area\": \"0\",<br>\r\n    \"job_role\": \"0\",<br>\r\n    \"key_skill\": \"\",<br>\r\n    \"email_verification\": \"No\",<br>\r\n    \"resume_file\": \"\",<br>\r\n    \"resume_last_update\": \"2017-05-17 14:32:43\",<br>\r\n    \"resume_verification\": \"UNAPPROVED\",<br>\r\n    \"desire_job_type\": \"\",<br>\r\n    \"employment_type\": \"\",<br>\r\n    \"prefered_shift\": \"\",<br>\r\n    \"expected_annual_salary\": \"\",<br>\r\n    \"exp_salary_currency_type\": \"\",<br>\r\n    \"register_date\": \"2017-05-17 09:02:43\",<br>\r\n    \"last_login\": \"0000-00-00 00:00:00\",<br>\r\n    \"profile_visibility\": \"Yes\",<br>\r\n    \"facebook_url\": \"\",<br>\r\n    \"gplus_url\": \"\",<br>\r\n    \"twitter_url\": \"\",<br>\r\n    \"linkedin_url\": \"\",<br>\r\n    \"device_id\": \"\",<br>\r\n    \"user_agent\": \"NI-WEB\",<br>\r\n    \"user_ip\": \"\",<br>\r\n    \"plan_status\": \"Active\",<br>\r\n    \"is_deleted\": \"No\",<br>\r\n    \"plan_name\": \"\",<br>\r\n    \"personal_titles\": \"Mr.\",<br>\r\n    \"marital_status_val\": null,<br>\r\n    \"city_name\": null,<br>\r\n    \"country_name\": null,<br>\r\n    \"industries_name\": null,<br>\r\n    \"functional_name\": null,<br>\r\n    \"role_name\": null<br>\r\n  }<br>\r\n}</p>', '', 'No'),
(74, 'APPROVED', 'Share and view employer  profile', 'http://192.168.1.111/job_portal/share_profile/emp_profile/', 'POST', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>emp_id  = MTE=</p>\r\n\r\n<p> </p>\r\n\r\n<p>pass emp_id wirh base64_encode</p>', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>emp_id  = MTE=</p>', '<p>{<br>\r\n  \"emp_img\": \"assets/emp_photos\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"emp_details\": {<br>\r\n    \"id\": \"11\",<br>\r\n    \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n    \"email_ver_str\": \"pim9db816dJ5VnJ\",<br>\r\n    \"email_ver_status\": \"No\",<br>\r\n    \"title\": \"5\",<br>\r\n    \"fullname\": \"jay thakkar\",<br>\r\n    \"designation\": \"21,23,44,\",<br>\r\n    \"functional_area\": \"2,5\",<br>\r\n    \"job_profile\": \"Hi ! Am The Employer From Narjis Enterprise.\\r\\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\",<br>\r\n    \"email_verified\": \"No\",<br>\r\n    \"status\": \"APPROVED\",<br>\r\n    \"password\": \"$2a$08$UvTLTT4GIYS7Slk/1ipzPerDGJVPTaeG/lL8430OZq1yj2lNraulW\",<br>\r\n    \"country\": \"3\",<br>\r\n    \"city\": \"12\",<br>\r\n    \"address\": \"Shop no F 11 A-Block Sagar Avenue Nr Amber Tower, Sarkhej Rd, Juhapura, Ahmedabad, Gujarat 380055\",<br>\r\n    \"pincode\": \"382350\",<br>\r\n    \"mobile\": \"+91-7043102102\",<br>\r\n    \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n    \"profile_pic_approval\": \"APPROVED\",<br>\r\n    \"company_name\": \"Narjis Enterprise\",<br>\r\n    \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n    \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n    \"company_email\": \"est@testc.o\",<br>\r\n    \"company_type\": \"9\",<br>\r\n    \"company_size\": \"4\",<br>\r\n    \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n    \"company_logo_approval\": \"APPROVED\",<br>\r\n    \"industry\": \"34\",<br>\r\n    \"industry_hire\": \"11,34\",<br>\r\n    \"function_area_hire\": \"23,16\",<br>\r\n    \"skill_hire\": \"26,81,392,2,16\",<br>\r\n    \"register_date\": \"2017-04-03 12:48:10\",<br>\r\n    \"facebook_url\": \"http://fb.com\",<br>\r\n    \"gplus_url\": \"http://www.gplus1.co\",<br>\r\n    \"twitter_url\": \"http://www.twitter.cm\",<br>\r\n    \"linkedin_url\": \"http://www.linkdi1n.om\",<br>\r\n    \"user_agent\": \"NI-WEB\",<br>\r\n    \"device_id\": \"\",<br>\r\n    \"last_login\": \"2017-06-09 04:59:04\",<br>\r\n    \"is_deleted\": \"No\",<br>\r\n    \"plan_status\": \"Paid\",<br>\r\n    \"plan_name\": \"Platinum\",<br>\r\n    \"city_name\": \"Adelaide\",<br>\r\n    \"country_name\": \"Australia\",<br>\r\n    \"personal_titles\": \"Dr.\",<br>\r\n    \"industries_name\": \"IT-Software/Software Services\",<br>\r\n    \"company_type_name\": \"Small and Medium Enterprise\",<br>\r\n    \"company_size_name\": \"0-50 Employee\"<br>\r\n  },<br>\r\n  \"ind_hire_name\": [<br>\r\n    \"IT-Software/Software Services\",<br>\r\n    \"Broadcasting\"<br>\r\n  ],<br>\r\n  \"fn_area_hire_name\": [<br>\r\n    \"IT Software - Application Programming / Maintenance\",<br>\r\n    \"IT Software - Client Server\"<br>\r\n  ],<br>\r\n  \"skill_hire_name\": [<br>\r\n    \"C++\",<br>\r\n    \"C#\",<br>\r\n    \"3D Studio Max\",<br>\r\n    \"Selling\",<br>\r\n    \"Communication\"<br>\r\n  ],<br>\r\n  \"fn_area_name\": [<br>\r\n    \"Banking / Insurance\",<br>\r\n    \"Agent\"<br>\r\n  ],<br>\r\n  \"role_name\": [<br>\r\n    \"Cust. Service Mgr\",<br>\r\n    \"Non-Life Insurance Agent\",<br>\r\n    \"Independent Rep.\"<br>\r\n  ]<br>\r\n}</p>', '<p>{<br>\r\n  \"emp_img\": \"assets/emp_photos\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"status\": \"error\",<br>\r\n  \"emp_details\": \"\",<br>\r\n  \"ind_hire_name\": \"\",<br>\r\n  \"fn_area_hire_name\": \"\",<br>\r\n  \"skill_hire_name\": \"\",<br>\r\n  \"fn_area_name\": \"\",<br>\r\n  \"role_name\": \"\"<br>\r\n}</p>', 'No'),
(75, 'APPROVED', 'get job seeker view , like , saved job list', 'http://192.168.1.111/job_portal/Job_seeker_action/js_activity_list', 'POST', '<p>get_list perameter</p>\r\n\r\n<p> </p>\r\n\r\n<p>for getting list of view job pass : view_job</p>\r\n\r\n<p>for getting list of like job pass : like_job</p>\r\n\r\n<p>for getting list of save job pass : save_job</p>', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>user_id  = 10</p>\r\n\r\n<p>get_list = view_job</p>\r\n\r\n<p>page  =1</p>\r\n\r\n<p> </p>\r\n\r\n<p>limit_per_page = 10</p>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"activity_list_count\": 23,<br>\r\n  \"activity_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"63\",<br>\r\n      \"job_title\": \"Test job\",<br>\r\n      \"job_description\": \"Test\",<br>\r\n      \"skill_keyword\": \"Test, 3D Studio Max\",<br>\r\n      \"location_hiring\": \"249\",<br>\r\n      \"company_hiring\": \"Test\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"18\",<br>\r\n      \"functional_area\": \"3\",<br>\r\n      \"job_post_role\": \"29\",<br>\r\n      \"work_experience_from\": \"7.0\",<br>\r\n      \"work_experience_to\": \"4.6\",<br>\r\n      \"job_salary_from\": \"1\",<br>\r\n      \"job_salary_to\": \"4.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"10\",<br>\r\n      \"total_requirenment\": \"16\",<br>\r\n      \"desire_candidate_profile\": \"sdfrrd\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 11:18:37\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Education/Teaching/Training\",<br>\r\n      \"functional_name\": \"Analytics & Business Intelligence\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Analytics Manager\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"job_id\": \"53\",<br>\r\n      \"update_on\": \"2017-06-09 08:56:52\",<br>\r\n      \"liked_on\": \"2017-06-01 05:54:02\",<br>\r\n      \"saved_on\": \"2017-06-01 05:54:38\",<br>\r\n      \"is_liked\": \"No\",<br>\r\n      \"is_saved\": \"No\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"40\",<br>\r\n      \"job_title\": \"Latest test by ketan for demo\",<br>\r\n      \"job_description\": \"As per company rules and regulations\",<br>\r\n      \"skill_keyword\": \"ICWA, Cost Accountant\",<br>\r\n      \"location_hiring\": \"1,2,3,4,5,6,7,8,9,11\",<br>\r\n      \"company_hiring\": \"Latest test by ketan for demo\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"3\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"21\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"2.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"24,25\",<br>\r\n      \"total_requirenment\": \"2\",<br>\r\n      \"desire_candidate_profile\": \"Latest test by ketan for demo\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"11\",<br>\r\n      \"posted_on\": \"2017-05-13 13:53:22\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"UNAPPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Agriculture/Dairy\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Independent Rep.\",<br>\r\n      \"posted_by_employee\": \"jay thakkar\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"job_id\": \"41\",<br>\r\n      \"update_on\": \"2017-05-17 06:59:36\",<br>\r\n      \"liked_on\": \"0000-00-00 00:00:00\",<br>\r\n      \"saved_on\": \"2017-05-17 06:21:33\",<br>\r\n      \"is_liked\": \"No\",<br>\r\n      \"is_saved\": \"Yes\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '<p>{<br>\r\n  \"status\": \"error\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"activity_list_count\": 0,<br>\r\n  \"activity_list_data\": \"\"<br>\r\n}</p>', 'No'),
(76, 'APPROVED', 'like employer , block employer , follow employer listing for job seeker .... action by job seeker', 'http://192.168.1.111/job_portal//job-seeker-action/js-activity-list-emp', 'POST', '<p>List of job seeker follow , like etc employer</p>\r\n\r\n<p> </p>\r\n\r\n<p>for like employer list get_list = like_emp</p>\r\n\r\n<p>for blocked employer list get_list = block_emp</p>\r\n\r\n<p>for following employer list get_list = follow_emp</p>', '<p>csrf_job_portal  = 3564f528016ab673d56f3532fa9917ee</p>\r\n\r\n<p>user_agent  = NI-AAPP</p>\r\n\r\n<p>user_id  = 10</p>\r\n\r\n<p>get_list  = like_emp</p>\r\n\r\n<p>page = 1</p>\r\n\r\n<p>limit_per_page =5</p>\r\n\r\n<p> </p>\r\n\r\n<p>Different prerameter for get_list</p>\r\n\r\n<p>for like employer list get_list = like_emp</p>\r\n\r\n<p>for blocked employer list get_list = block_emp</p>\r\n\r\n<p>for following employer list get_list = follow_emp</p>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"activity_list_count\": 1,<br>\r\n  \"activity_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"3\",<br>\r\n      \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n      \"email_ver_str\": \"pim9db816dJ5VnJ\",<br>\r\n      \"email_ver_status\": \"No\",<br>\r\n      \"title\": \"5\",<br>\r\n      \"fullname\": \"jay thakkar\",<br>\r\n      \"designation\": \"21,23,44,\",<br>\r\n      \"functional_area\": \"2,5\",<br>\r\n      \"job_profile\": \"Hi ! Am The Employer From Narjis Enterprise.\\r\\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\",<br>\r\n      \"email_verified\": \"No\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"password\": \"$2a$08$UvTLTT4GIYS7Slk/1ipzPerDGJVPTaeG/lL8430OZq1yj2lNraulW\",<br>\r\n      \"country\": \"3\",<br>\r\n      \"city\": \"12\",<br>\r\n      \"address\": \"Shop no F 11 A-Block Sagar Avenue Nr Amber Tower, Sarkhej Rd, Juhapura, Ahmedabad, Gujarat 380055\",<br>\r\n      \"pincode\": \"382350\",<br>\r\n      \"mobile\": \"+91-7043102102\",<br>\r\n      \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_name\": \"Narjis Enterprise\",<br>\r\n      \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"est@testc.o\",<br>\r\n      \"company_type\": \"9\",<br>\r\n      \"company_size\": \"4\",<br>\r\n      \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n      \"company_logo_approval\": \"APPROVED\",<br>\r\n      \"industry\": \"34\",<br>\r\n      \"industry_hire\": \"11,34\",<br>\r\n      \"function_area_hire\": \"23,16\",<br>\r\n      \"skill_hire\": \"26,81,392,2,16\",<br>\r\n      \"register_date\": \"2017-04-03 12:48:10\",<br>\r\n      \"facebook_url\": \"http://fb.com\",<br>\r\n      \"gplus_url\": \"http://www.gplus1.co\",<br>\r\n      \"twitter_url\": \"http://www.twitter.cm\",<br>\r\n      \"linkedin_url\": \"http://www.linkdi1n.om\",<br>\r\n      \"user_agent\": \"NI-WEB\",<br>\r\n      \"device_id\": \"\",<br>\r\n      \"last_login\": \"2017-06-09 08:54:18\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"plan_status\": \"Paid\",<br>\r\n      \"plan_name\": \"Platinum\",<br>\r\n      \"city_name\": \"Adelaide\",<br>\r\n      \"country_name\": \"Australia\",<br>\r\n      \"personal_titles\": \"Dr.\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"company_type_name\": \"Small and Medium Enterprise\",<br>\r\n      \"company_size_name\": \"0-50 Employee\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"emp_id\": \"11\",<br>\r\n      \"is_block\": \"Yes\",<br>\r\n      \"is_follow\": \"Yes\",<br>\r\n      \"is_liked\": \"Yes\",<br>\r\n      \"liked_on\": \"2017-05-04 08:57:54\",<br>\r\n      \"followed_on\": \"2017-05-04 08:57:58\",<br>\r\n      \"blocked_on\": \"2017-05-04 08:57:58\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '<p>{<br>\r\n  \"status\": \"error\",<br>\r\n  \"comp_img\": \"assets/company_logos\",<br>\r\n  \"activity_list_count\": 0,<br>\r\n  \"activity_list_data\": \"\"<br>\r\n}</p>', 'No'),
(77, 'APPROVED', 'Job seeker apllied job list', 'http://192.168.1.111/job_portal/job-seeker-action/js_apply_job_list', 'POST', '', '<p><span xss=removed>csrf_job_portal</span>  = <span xss=removed>3564f528016ab673d56f3532fa9917ee</span></p>\r\n\r\n<p><span xss=removed>user_agent</span>  = <span xss=removed>NI-AAPP</span></p>\r\n\r\n<p><span xss=removed>user_id</span>  = <span xss=removed>10</span></p>\r\n\r\n<p><span xss=removed>limit_per_page</span>  = <span xss=removed>2</span> </p>\r\n\r\n<p><span xss=removed>page</span> = 1</p>', '<p>{<br>\r\n  \"company_logo_img\": \"assets/company_logos\",<br>\r\n  \"status\": \"success\",<br>\r\n  \"apply_list_count\": 18,<br>\r\n  \"apply_list_data\": [<br>\r\n    {<br>\r\n      \"id\": \"40\",<br>\r\n      \"job_title\": \"Test job\",<br>\r\n      \"job_description\": \"Test\",<br>\r\n      \"skill_keyword\": \"Test, 3D Studio Max\",<br>\r\n      \"location_hiring\": \"249\",<br>\r\n      \"company_hiring\": \"Test\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"18\",<br>\r\n      \"functional_area\": \"3\",<br>\r\n      \"job_post_role\": \"29\",<br>\r\n      \"work_experience_from\": \"7.0\",<br>\r\n      \"work_experience_to\": \"4.6\",<br>\r\n      \"job_salary_from\": \"1\",<br>\r\n      \"job_salary_to\": \"4.5\",<br>\r\n      \"currency_type\": \"GBP\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"1\",<br>\r\n      \"job_education\": \"10\",<br>\r\n      \"total_requirenment\": \"16\",<br>\r\n      \"desire_candidate_profile\": \"sdfrrd\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 11:18:37\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Education/Teaching/Training\",<br>\r\n      \"functional_name\": \"Analytics & Business Intelligence\",<br>\r\n      \"job_type_name\": \"Temporary/Contractual\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Analytics Manager\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\",<br>\r\n      \"job_id\": \"53\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"is_shortlisted\": \"No\",<br>\r\n      \"applied_on\": \"2017-06-01 05:14:03\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"33\",<br>\r\n      \"job_title\": \"Java\",<br>\r\n      \"job_description\": \"Advance Java Web Developer\",<br>\r\n      \"skill_keyword\": \"Data Analyst, Independent Rep., Collections Exec., Financial Analyst\",<br>\r\n      \"location_hiring\": \"1,2,3,4,5,6,7,9\",<br>\r\n      \"company_hiring\": \"dfsdfgsdgsdfgsdfg\",<br>\r\n      \"link_job\": \"http://sdfgd.sdfg\",<br>\r\n      \"job_industry\": \"3\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"21\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"0.6\",<br>\r\n      \"job_salary_from\": \"1.0\",<br>\r\n      \"job_salary_to\": \"1.0\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"24,25,2,3,10,11,4,12,5,6,16,17,18,7,8,9,13,14,15,19,23,22,21,1,26,27,28,29,30,31,32,33,34,35,36,37,38,42,43,44,48,39,45,40,41,47,46\",<br>\r\n      \"total_requirenment\": \"3\",<br>\r\n      \"desire_candidate_profile\": \"ertert yrty rtyr tyrty rtyrt yrty rtyrtyrtrty\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"79\",<br>\r\n      \"posted_on\": \"2017-05-13 13:44:47\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"Agriculture/Dairy\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Independent Rep.\",<br>\r\n      \"posted_by_employee\": \"Riddhish H Pathak\",<br>\r\n      \"email\": \"riddhishpathak9@gmail.com\",<br>\r\n      \"profile_pic\": \"50ba3b1190c091cdf807c8ce3a3cb051.jpg\",<br>\r\n      \"company_name\": \"dgdfgdsf gdsfgsdfg\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"dfgdfghfh dg hdfgh dfghdf ghdfg hdfgh fdghdf hdfhx dsfgsdfgdfsg sdfgdsfgd\",<br>\r\n      \"company_website\": \"http://google.com\",<br>\r\n      \"company_email\": \"\",<br>\r\n      \"company_logo\": \"1302e01a4be2ae6ffca835ee355d4613.jpg\",<br>\r\n      \"company_logo_approval\": \"UNAPPROVED\",<br>\r\n      \"job_id\": \"40\",<br>\r\n      \"js_id\": \"10\",<br>\r\n      \"is_shortlisted\": \"No\",<br>\r\n      \"applied_on\": \"2017-05-16 10:05:12\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '<p>{<br>\r\n  \"status\": \"error\",<br>\r\n  \"company_logo_img\": \"assets/company_logos\",<br>\r\n  \"apply_list_count\": 0,<br>\r\n  \"apply_list_data\": \"\"<br>\r\n}</p>', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(78, 'APPROVED', 'suggested job for job seeker', 'http://192.168.1.111/job_portal/job-listing/suggested-job', 'POST', '<p>job list based on job seeker requirenment</p>', '<p><span xss=removed>csrf_job_portal</span>  = <span xss=removed>3564f528016ab673d56f3532fa9917ee</span></p>\r\n\r\n<p><span xss=removed>user_agent</span>  = <span xss=removed>NI-AAPP</span></p>\r\n\r\n<p><span xss=removed>user_id</span> = <span xss=removed>10</span></p>\r\n\r\n<p><span xss=removed>limit_per_page</span>  = 1</p>\r\n\r\n<p>page =1</p>', '<p>{<br>\r\n  \"status\": \"success\",<br>\r\n  \"total_post_count\": 4,<br>\r\n  \"emp_posted_job\": [<br>\r\n    {<br>\r\n      \"id\": \"51\",<br>\r\n      \"job_title\": \"Test Job\",<br>\r\n      \"job_description\": \"Test job\",<br>\r\n      \"skill_keyword\": \"php, html\",<br>\r\n      \"location_hiring\": \"1\",<br>\r\n      \"company_hiring\": \"Test job\",<br>\r\n      \"link_job\": \"http://www.joblink.com\",<br>\r\n      \"job_industry\": \"34\",<br>\r\n      \"functional_area\": \"2\",<br>\r\n      \"job_post_role\": \"20\",<br>\r\n      \"work_experience_from\": \"2.0\",<br>\r\n      \"work_experience_to\": \"4.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"3.5\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"18,46\",<br>\r\n      \"total_requirenment\": \"5\",<br>\r\n      \"desire_candidate_profile\": \"Test Job\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 11:10:46\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"functional_name\": \"Agent\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"DSA/Company Rep.\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"50\",<br>\r\n      \"job_title\": \"Test Job\",<br>\r\n      \"job_description\": \"Test job\",<br>\r\n      \"skill_keyword\": \"php, html\",<br>\r\n      \"location_hiring\": \"1\",<br>\r\n      \"company_hiring\": \"Test job\",<br>\r\n      \"link_job\": \"http://www.joblink.com\",<br>\r\n      \"job_industry\": \"34\",<br>\r\n      \"functional_area\": \"24\",<br>\r\n      \"job_post_role\": \"776\",<br>\r\n      \"work_experience_from\": \"2.0\",<br>\r\n      \"work_experience_to\": \"4.6\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"3.5\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"18,46\",<br>\r\n      \"total_requirenment\": \"5\",<br>\r\n      \"desire_candidate_profile\": \"Test Job\",<br>\r\n      \"job_payment\": \"2\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 11:08:46\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"functional_name\": \"IT Software - DBA / Datawarehousing\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"IT/Technical Content Developer\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"49\",<br>\r\n      \"job_title\": \"Php  Developer\",<br>\r\n      \"job_description\": \"Want a php developer have a good communication skill and development skill\\r\\n\",<br>\r\n      \"skill_keyword\": \"PHP, HTML, css\",<br>\r\n      \"location_hiring\": \"1\",<br>\r\n      \"company_hiring\": \"Php developer\",<br>\r\n      \"link_job\": \"http://www.joblink.com\",<br>\r\n      \"job_industry\": \"34\",<br>\r\n      \"functional_area\": \"21\",<br>\r\n      \"job_post_role\": \"601\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"2.0\",<br>\r\n      \"job_salary_from\": \"1.5\",<br>\r\n      \"job_salary_to\": \"5.0\",<br>\r\n      \"currency_type\": \"INR\",<br>\r\n      \"job_shift\": \"1\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"18,22\",<br>\r\n      \"total_requirenment\": \"4\",<br>\r\n      \"desire_candidate_profile\": \"Php developer have good skill in development and tester\",<br>\r\n      \"job_payment\": \"1\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"64\",<br>\r\n      \"posted_on\": \"2017-05-29 09:34:20\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"Yes\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"functional_name\": \"IT Software - System Programming\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Day\",<br>\r\n      \"job_role_name\": \"Software Developer\",<br>\r\n      \"posted_by_employee\": \"test Mansuri\",<br>\r\n      \"email\": \"testbhai@gmail.com\",<br>\r\n      \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n      \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n      \"company_email\": \"narjis@gmail.com\",<br>\r\n      \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    },<br>\r\n    {<br>\r\n      \"id\": \"24\",<br>\r\n      \"job_title\": \"Posted by test\",<br>\r\n      \"job_description\": \"test\",<br>\r\n      \"skill_keyword\": \"4th Dimenssion, Active Listening,php\",<br>\r\n      \"location_hiring\": \"12\",<br>\r\n      \"company_hiring\": \"test\",<br>\r\n      \"link_job\": \"\",<br>\r\n      \"job_industry\": \"34\",<br>\r\n      \"functional_area\": \"3\",<br>\r\n      \"job_post_role\": \"28\",<br>\r\n      \"work_experience_from\": \"1.0\",<br>\r\n      \"work_experience_to\": \"2.6\",<br>\r\n      \"job_salary_from\": \"1\",<br>\r\n      \"job_salary_to\": \"1.5\",<br>\r\n      \"currency_type\": \"USD\",<br>\r\n      \"job_shift\": \"3\",<br>\r\n      \"job_type\": \"2\",<br>\r\n      \"job_education\": \"11\",<br>\r\n      \"total_requirenment\": \"2\",<br>\r\n      \"desire_candidate_profile\": \"\",<br>\r\n      \"job_payment\": \"0\",<br>\r\n      \"no_of_views\": \"0\",<br>\r\n      \"no_of_like\": \"0\",<br>\r\n      \"no_of_apply\": \"0\",<br>\r\n      \"posted_by\": \"21\",<br>\r\n      \"posted_on\": \"2017-04-24 11:30:41\",<br>\r\n      \"job_life\": \"0\",<br>\r\n      \"status\": \"APPROVED\",<br>\r\n      \"currently_hiring_status\": \"Yes\",<br>\r\n      \"job_highlighted\": \"No\",<br>\r\n      \"is_deleted\": \"No\",<br>\r\n      \"industries_name\": \"IT-Software/Software Services\",<br>\r\n      \"functional_name\": \"Analytics & Business Intelligence\",<br>\r\n      \"job_type_name\": \"Permanent\",<br>\r\n      \"job_shift_type\": \"Flexible\",<br>\r\n      \"job_role_name\": \"Business Analyst\",<br>\r\n      \"posted_by_employee\": \"Emp test mansuri\",<br>\r\n      \"email\": \"mtest@gmail.com\",<br>\r\n      \"profile_pic\": \"c02aa339b56d8e5523fba472c8e99e5a.jpg\",<br>\r\n      \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n      \"profile_pic_approval\": \"APPROVED\",<br>\r\n      \"company_profile\": null,<br>\r\n      \"company_website\": \"\",<br>\r\n      \"company_email\": \"test@mm.com\",<br>\r\n      \"company_logo\": \"a8c9ebff27e9d8759d5dc24b926fb453.gif\",<br>\r\n      \"company_logo_approval\": \"APPROVED\"<br>\r\n    }<br>\r\n  ]<br>\r\n}</p>', '<p>{<br>\r\n  \"status\": \"error\",<br>\r\n  \"total_post_count\": 0,<br>\r\n  \"emp_posted_job\": \"\"<br>\r\n}</p>', 'No'),
(79, 'APPROVED', 'get recent view job by job seeker', 'http://192.168.1.111/job_portal/common_request/recent_view_job', 'POST', '', '<p><span xss=removed>csrf_job_portal</span>  = <span xss=removed>3564f528016ab673d56f3532fa9917ee</span></p>\r\n\r\n<p><span xss=removed>user_agent= </span> <span xss=removed>NI-AAPP</span></p>\r\n\r\n<p><span xss=removed>user_id</span> = <span xss=removed>10</span></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>', '<p>{<br>\r\n  \"data\": {<br>\r\n    \"status\": \"success\",<br>\r\n    \"tocken\": \"9b256338bfb08e0709339db869843e5e\",<br>\r\n    \"view_job_list\": [<br>\r\n      {<br>\r\n        \"id\": \"22\",<br>\r\n        \"job_title\": \"test new job\",<br>\r\n        \"job_description\": \"test new job\",<br>\r\n        \"skill_keyword\": \"Abap4, 4th Dimenssion\",<br>\r\n        \"location_hiring\": \"2\",<br>\r\n        \"company_hiring\": \"test new job\",<br>\r\n        \"link_job\": \"\",<br>\r\n        \"job_industry\": \"1\",<br>\r\n        \"functional_area\": \"1\",<br>\r\n        \"job_post_role\": \"1\",<br>\r\n        \"work_experience_from\": \"1.0\",<br>\r\n        \"work_experience_to\": \"2.0\",<br>\r\n        \"job_salary_from\": \"0.5\",<br>\r\n        \"job_salary_to\": \"1\",<br>\r\n        \"currency_type\": \"USD\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"1\",<br>\r\n        \"job_education\": \"22\",<br>\r\n        \"total_requirenment\": \"5\",<br>\r\n        \"desire_candidate_profile\": \"\",<br>\r\n        \"job_payment\": \"0\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-04-24 09:47:39\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"Yes\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Accounting/Finance\",<br>\r\n        \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n        \"job_type_name\": \"Temporary/Contractual\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Accounts Exec./Accountant\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"53\",<br>\r\n        \"job_title\": \"Test job\",<br>\r\n        \"job_description\": \"Test\",<br>\r\n        \"skill_keyword\": \"Test, 3D Studio Max\",<br>\r\n        \"location_hiring\": \"249\",<br>\r\n        \"company_hiring\": \"Test\",<br>\r\n        \"link_job\": \"\",<br>\r\n        \"job_industry\": \"18\",<br>\r\n        \"functional_area\": \"3\",<br>\r\n        \"job_post_role\": \"29\",<br>\r\n        \"work_experience_from\": \"7.0\",<br>\r\n        \"work_experience_to\": \"4.6\",<br>\r\n        \"job_salary_from\": \"1\",<br>\r\n        \"job_salary_to\": \"4.5\",<br>\r\n        \"currency_type\": \"GBP\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"1\",<br>\r\n        \"job_education\": \"10\",<br>\r\n        \"total_requirenment\": \"16\",<br>\r\n        \"desire_candidate_profile\": \"sdfrrd\",<br>\r\n        \"job_payment\": \"2\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"64\",<br>\r\n        \"posted_on\": \"2017-05-29 11:18:37\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"No\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Education/Teaching/Training\",<br>\r\n        \"functional_name\": \"Analytics & Business Intelligence\",<br>\r\n        \"job_type_name\": \"Temporary/Contractual\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Analytics Manager\",<br>\r\n        \"posted_by_employee\": \"test Mansuri\",<br>\r\n        \"email\": \"testbhai@gmail.com\",<br>\r\n        \"profile_pic\": \"8ac64d326da2ae619f72865e344d5cdb.bmp\",<br>\r\n        \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Narjis enterprice working on software development using core php and codeigniter framework\\r\\nCompany growing on success and also work on seo\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"narjis@gmail.com\",<br>\r\n        \"company_logo\": \"dc6a812d3ff8e4c1b7c7c1f7ca5ba4fa.bmp\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"30\",<br>\r\n        \"job_title\": \"networking engg\",<br>\r\n        \"job_description\": \"networking engg\",<br>\r\n        \"skill_keyword\": \"Cost Accountant\",<br>\r\n        \"location_hiring\": \"1,4\",<br>\r\n        \"company_hiring\": \"networking engg\",<br>\r\n        \"link_job\": \"http://test.com\",<br>\r\n        \"job_industry\": \"2\",<br>\r\n        \"functional_area\": \"1\",<br>\r\n        \"job_post_role\": \"5\",<br>\r\n        \"work_experience_from\": \"0.6\",<br>\r\n        \"work_experience_to\": \"2.0\",<br>\r\n        \"job_salary_from\": \"As per company rules and regulations\",<br>\r\n        \"job_salary_to\": \"\",<br>\r\n        \"currency_type\": \"\",<br>\r\n        \"job_shift\": \"3\",<br>\r\n        \"job_type\": \"1\",<br>\r\n        \"job_education\": \"24,25\",<br>\r\n        \"total_requirenment\": \"3\",<br>\r\n        \"desire_candidate_profile\": \"networking engg\",<br>\r\n        \"job_payment\": \"1\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-05-02 06:52:39\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"Yes\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n        \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n        \"job_type_name\": \"Temporary/Contractual\",<br>\r\n        \"job_shift_type\": \"Flexible\",<br>\r\n        \"job_role_name\": \"Accounts Mgr\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"31\",<br>\r\n        \"job_title\": \"networking engg\",<br>\r\n        \"job_description\": \"networking engg\",<br>\r\n        \"skill_keyword\": \"Cost Accountant\",<br>\r\n        \"location_hiring\": \"1,4\",<br>\r\n        \"company_hiring\": \"networking engg\",<br>\r\n        \"link_job\": \"http://test.com\",<br>\r\n        \"job_industry\": \"2\",<br>\r\n        \"functional_area\": \"1\",<br>\r\n        \"job_post_role\": \"5\",<br>\r\n        \"work_experience_from\": \"0.6\",<br>\r\n        \"work_experience_to\": \"2.0\",<br>\r\n        \"job_salary_from\": \"As per company rules and regulations\",<br>\r\n        \"job_salary_to\": \"\",<br>\r\n        \"currency_type\": \"\",<br>\r\n        \"job_shift\": \"3\",<br>\r\n        \"job_type\": \"1\",<br>\r\n        \"job_education\": \"24,25\",<br>\r\n        \"total_requirenment\": \"3\",<br>\r\n        \"desire_candidate_profile\": \"networking engg\",<br>\r\n        \"job_payment\": \"1\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-05-02 06:52:39\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"Yes\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Advertising/PR/MR/Events\",<br>\r\n        \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n        \"job_type_name\": \"Temporary/Contractual\",<br>\r\n        \"job_shift_type\": \"Flexible\",<br>\r\n        \"job_role_name\": \"Accounts Mgr\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"14\",<br>\r\n        \"job_title\": \"Interior Designer/furniture Designer\",<br>\r\n        \"job_description\": \"Job Description\\r\\n \\r\\nSend me Jobs like this\\r\\n\\r\\n    1. Should be well versed with Auto Cad as well as good designing skills. Knowledge of any other presentation software will be an advantage.\\r\\n\\r\\n    2. Experienced, creative, innovative designer and should have knowledge of detail drawings.\\r\\n\\r\\n    3. The candidate should have the knowledge of taking/ checking measurement.\\r\\n\\r\\n    4 .Ability to work under pressure and meet deadlines. Should be a team player, keeping up to date with new developments in the design industry.\\r\\n\\r\\nSalary: Not Disclosed by Recruiter\\r\\n\\r\\nIndustry: Architecture / Interior Design\\r\\n\\r\\nFunctional Area: Architecture , Interior Design\\r\\n\\r\\nRole Category:Interior Design\\r\\n\\r\\nRole:Interior Designer\",<br>\r\n        \"skill_keyword\": \"HTML, Html Developer\",<br>\r\n        \"location_hiring\": \"1,3,9\",<br>\r\n        \"company_hiring\": \"developer /fresher\",<br>\r\n        \"link_job\": \"\",<br>\r\n        \"job_industry\": \"1\",<br>\r\n        \"functional_area\": \"1\",<br>\r\n        \"job_post_role\": \"0\",<br>\r\n        \"work_experience_from\": \"1.6\",<br>\r\n        \"work_experience_to\": \"2.6\",<br>\r\n        \"job_salary_from\": \"3.8\",<br>\r\n        \"job_salary_to\": \"6.0\",<br>\r\n        \"currency_type\": \"INR\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"5,3,4\",<br>\r\n        \"total_requirenment\": \"2\",<br>\r\n        \"desire_candidate_profile\": \"\",<br>\r\n        \"job_payment\": \"0\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-04-14 10:32:23\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"No\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Accounting/Finance\",<br>\r\n        \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": null,<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"15\",<br>\r\n        \"job_title\": \"Php Developer\",<br>\r\n        \"job_description\": \"Required php developer with good working skill\\r\\nthank you\\r\\nok\",<br>\r\n        \"skill_keyword\": \"\",<br>\r\n        \"location_hiring\": \"1,3,2\",<br>\r\n        \"company_hiring\": \"Software Developer\",<br>\r\n        \"link_job\": \"http://www.phpdeveloper.com\",<br>\r\n        \"job_industry\": \"7\",<br>\r\n        \"functional_area\": \"16\",<br>\r\n        \"job_post_role\": \"382\",<br>\r\n        \"work_experience_from\": \"1.6\",<br>\r\n        \"work_experience_to\": \"5.6\",<br>\r\n        \"job_salary_from\": \"2\",<br>\r\n        \"job_salary_to\": \"2.5\",<br>\r\n        \"currency_type\": \"INR\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"18,22\",<br>\r\n        \"total_requirenment\": \"5\",<br>\r\n        \"desire_candidate_profile\": \"\",<br>\r\n        \"job_payment\": \"0\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"21\",<br>\r\n        \"posted_on\": \"2017-04-21 11:35:20\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"No\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Aviation / Aerospace Firms\",<br>\r\n        \"functional_name\": \"IT Software - Client Server\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Team Lead/Tech Lead\",<br>\r\n        \"posted_by_employee\": \"Emp test mansuri\",<br>\r\n        \"email\": \"mtest@gmail.com\",<br>\r\n        \"profile_pic\": \"c02aa339b56d8e5523fba472c8e99e5a.jpg\",<br>\r\n        \"company_name\": \"Narjis Infotech pvt limited\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": null,<br>\r\n        \"company_website\": \"\",<br>\r\n        \"company_email\": \"test@mm.com\",<br>\r\n        \"company_logo\": \"a8c9ebff27e9d8759d5dc24b926fb453.gif\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"16\",<br>\r\n        \"job_title\": \"Senior Php developer /fresher\",<br>\r\n        \"job_description\": \"Senior Php developer /fresher\",<br>\r\n        \"skill_keyword\": \"PHP, Adobe Photoshop, CSS, HTML, Javascript\",<br>\r\n        \"location_hiring\": \"1,4\",<br>\r\n        \"company_hiring\": \"Senior Php developer /fresher\",<br>\r\n        \"link_job\": \"\",<br>\r\n        \"job_industry\": \"1\",<br>\r\n        \"functional_area\": \"23\",<br>\r\n        \"job_post_role\": \"709\",<br>\r\n        \"work_experience_from\": \"0.0\",<br>\r\n        \"work_experience_to\": \"4.6\",<br>\r\n        \"job_salary_from\": \"0.5\",<br>\r\n        \"job_salary_to\": \"3.5\",<br>\r\n        \"currency_type\": \"USD\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"22,24,25\",<br>\r\n        \"total_requirenment\": \"3\",<br>\r\n        \"desire_candidate_profile\": \"\",<br>\r\n        \"job_payment\": \"0\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-04-24 05:32:21\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"No\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Accounting/Finance\",<br>\r\n        \"functional_name\": \"IT Software - Application Programming / Maintenance\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Business Analyst\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"21\",<br>\r\n        \"job_title\": \"Test add\",<br>\r\n        \"job_description\": \"test add\",<br>\r\n        \"skill_keyword\": \"3D Studio Max\",<br>\r\n        \"location_hiring\": \"1\",<br>\r\n        \"company_hiring\": \"test add\",<br>\r\n        \"link_job\": \"http://www.test.com\",<br>\r\n        \"job_industry\": \"33\",<br>\r\n        \"functional_area\": \"23\",<br>\r\n        \"job_post_role\": \"729\",<br>\r\n        \"work_experience_from\": \"0.0\",<br>\r\n        \"work_experience_to\": \"0.0\",<br>\r\n        \"job_salary_from\": \"0.5\",<br>\r\n        \"job_salary_to\": \"1\",<br>\r\n        \"currency_type\": \"INR\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"18\",<br>\r\n        \"total_requirenment\": \"10\",<br>\r\n        \"desire_candidate_profile\": \"\",<br>\r\n        \"job_payment\": \"0\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"0000-00-00 00:00:00\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"No\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"IT-Hardware & Networking\",<br>\r\n        \"functional_name\": \"IT Software - Application Programming / Maintenance\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Trainee\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"26\",<br>\r\n        \"job_title\": \"Accountant\",<br>\r\n        \"job_description\": \"Position: PHP Developer\\r\\n\\r\\nLooking for STRONG candidates who have more than 2 Years of hands on experience on the following platforms :\\r\\n\\r\\n1. Proven software development experience in PHP\\r\\n\\r\\n2. Understanding of open source projects like Wordpress, Drupal, Wikis, osCommerce, etc\\r\\n\\r\\n3. Demonstrable knowledge of web technologies including HTML, CSS, jquery, AJAX etc\\r\\n\\r\\n4. Good knowledge of relational databases, version control tools(Github) and of developing web services\\r\\n\\r\\n5. Experience in REST client API, Angular.js\\r\\n\\r\\n6. Experience in common third-party APIs (Amadeus, Sabre, Google, Facebook, Ebay etc)\\r\\n\\r\\n7. Passion for best design and coding practices and a desire to develop new bold ideas\",<br>\r\n        \"skill_keyword\": \"Accounts Exec./Accountant\",<br>\r\n        \"location_hiring\": \"24,58,5,57\",<br>\r\n        \"company_hiring\": \"Accountant manager\",<br>\r\n        \"link_job\": \"\",<br>\r\n        \"job_industry\": \"1\",<br>\r\n        \"functional_area\": \"5\",<br>\r\n        \"job_post_role\": \"106\",<br>\r\n        \"work_experience_from\": \"0.6\",<br>\r\n        \"work_experience_to\": \"1.6\",<br>\r\n        \"job_salary_from\": \"1.5\",<br>\r\n        \"job_salary_to\": \"5.5\",<br>\r\n        \"currency_type\": \"INR\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"11,20,31,35\",<br>\r\n        \"total_requirenment\": \"4\",<br>\r\n        \"desire_candidate_profile\": \"Accountant manager should have all knowledge\",<br>\r\n        \"job_payment\": \"2\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-04-26 05:39:30\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"Yes\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Accounting/Finance\",<br>\r\n        \"functional_name\": \"Banking / Insurance\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Asset Manager\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      },<br>\r\n      {<br>\r\n        \"id\": \"27\",<br>\r\n        \"job_title\": \"New POst by ketan for CE Engg\",<br>\r\n        \"job_description\": \"New POst by ketan for CE Engg\",<br>\r\n        \"skill_keyword\": \"Accounts Exec./Accountant\",<br>\r\n        \"location_hiring\": \"1,156,2,4\",<br>\r\n        \"company_hiring\": \"CE Engg. manager\",<br>\r\n        \"link_job\": \"http://test.com\",<br>\r\n        \"job_industry\": \"1\",<br>\r\n        \"functional_area\": \"1\",<br>\r\n        \"job_post_role\": \"1\",<br>\r\n        \"work_experience_from\": \"0.6\",<br>\r\n        \"work_experience_to\": \"1.6\",<br>\r\n        \"job_salary_from\": \"1.5\",<br>\r\n        \"job_salary_to\": \"2.0\",<br>\r\n        \"currency_type\": \"INR\",<br>\r\n        \"job_shift\": \"1\",<br>\r\n        \"job_type\": \"2\",<br>\r\n        \"job_education\": \"24,10,11,4\",<br>\r\n        \"total_requirenment\": \"3\",<br>\r\n        \"desire_candidate_profile\": \"Test by narjis enterprise\",<br>\r\n        \"job_payment\": \"2\",<br>\r\n        \"no_of_views\": \"0\",<br>\r\n        \"no_of_like\": \"0\",<br>\r\n        \"no_of_apply\": \"0\",<br>\r\n        \"posted_by\": \"11\",<br>\r\n        \"posted_on\": \"2017-04-26 05:39:30\",<br>\r\n        \"job_life\": \"0\",<br>\r\n        \"status\": \"APPROVED\",<br>\r\n        \"currently_hiring_status\": \"Yes\",<br>\r\n        \"job_highlighted\": \"Yes\",<br>\r\n        \"is_deleted\": \"No\",<br>\r\n        \"industries_name\": \"Accounting/Finance\",<br>\r\n        \"functional_name\": \"Accounts / Finance / Tax / CS / Audit\",<br>\r\n        \"job_type_name\": \"Permanent\",<br>\r\n        \"job_shift_type\": \"Day\",<br>\r\n        \"job_role_name\": \"Accounts Exec./Accountant\",<br>\r\n        \"posted_by_employee\": \"jay thakkar\",<br>\r\n        \"email\": \"jay.narjisenterprise@gmail.com\",<br>\r\n        \"profile_pic\": \"437990a8fe2fa25950979db1d0094b11.bmp\",<br>\r\n        \"company_name\": \"Narjis Enterprise\",<br>\r\n        \"profile_pic_approval\": \"APPROVED\",<br>\r\n        \"company_profile\": \"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",<br>\r\n        \"company_website\": \"http://www.narjisenterprise.com\",<br>\r\n        \"company_email\": \"est@testc.o\",<br>\r\n        \"company_logo\": \"0d0cf13ab91fad3032909c5bbb1a5c11.png\",<br>\r\n        \"company_logo_approval\": \"APPROVED\"<br>\r\n      }<br>\r\n    ]<br>\r\n  }<br>\r\n}</p>', '<p>{<br>\r\n  \"data\": {<br>\r\n    \"status\": \"error\",<br>\r\n    \"tocken\": \"9b256338bfb08e0709339db869843e5e\",<br>\r\n    \"view_job_list\": \"0\"<br>\r\n  }<br>\r\n}</p>', 'No'),
(80, 'APPROVED', 'send message emp to JS', 'http://192.168.1.111/job_portal/job_application/send_message_js', 'POST', '', '<p>action send_message<br>\r\ncontent  test for api<br>\r\ncsrf_job_portal    <br>\r\ned42c2d3ad0bb910f4299ca236e7ce1f<br>\r\nemail    <br>\r\njay.finaltesting@gmail.com<br>\r\nreceiver_id    <br>\r\n182<br>\r\nsender_id    <br>\r\n11<br>\r\nsubject    <br>\r\ntest for api<br>\r\nuser_agent    <br>\r\nNI-WEB<br>\r\n </p>', '<p>{\"token\":\"ed42c2d3ad0bb910f4299ca236e7ce1f\",\"errmessage\":\"Message sent successfully\",\"status\":\"success\"<br>\r\n}</p>', '', 'No'),
(81, 'APPROVED', 'Forgot password view for job seeker', 'http://192.168.1.111/job_portal/login/forgot-password', 'POST', '', '', '', '', 'No'),
(82, 'APPROVED', 'Sent verifaction to user by email for forgot password', 'http://192.168.1.111/job_portal/login/reset_forgot_password', 'POST', '', '<p>action     = forgot_password<br>\r\ncsrf_job_portal     = ed42c2d3ad0bb910f4299ca236e7ce1f<br>\r\nemail    = ketanparmar583@gmail.com<br>\r\nuser_agent     = NI-WEB</p>\r\n\r\n<p> </p>', '<p>{\"token\":\"ed42c2d3ad0bb910f4299ca236e7ce1f\",\"errmessage\":\"Your password reset verification link sent<br>\r\n to your email, please check your email.\",\"status\":\"success\"}</p>\r\n\r\n<p> </p>', '', 'No'),
(83, 'APPROVED', 'Job seeker reset password view', 'http://192 .168.1.111/job_portal/login/restetpasstonew_js/yrkPGrq58ZMoDqR/ketanparmar583@gmail.com', 'POST', '<p>For the view job seeker change password...</p>', '', '', '', 'No'),
(84, 'APPROVED', 'Job seeker reset new password', 'http://192.168.1.111/job_portal/login/reset_new_password/yrkPGrq58ZMoDqR/ketanparmar583@gmail.com', 'POST', '', '<p>action     = reset_password<br>\r\ncsrf_job_portal     = ed42c2d3ad0bb910f4299ca236e7ce1f<br>\r\npass    = 123456<br>\r\npass_confirmation     = 123456<br>\r\nuser_agent     = NI-WEB</p>\r\n\r\n<p> </p>', '<p>token    \"ed42c2d3ad0bb910f4299ca236e7ce1f\"<br>\r\nerrmessage    \"Your password changed successufully.\"<br>\r\nstatus    \"success\"</p>', '<p>token    \"ed42c2d3ad0bb910f4299ca236e7ce1f\"<br>\r\nerrmessage    \"Unauthorized access.\"<br>\r\nstatus    \"error\"</p>', 'No'),
(85, 'APPROVED', 'search suggestion for home page and browse job search', 'http://192.168.1.111/job_portal/job_listing/get_search_suggestion/', 'POST', '', '<p>csrf_job_portal:3564f528016ab673d56f3532fa9917ee<br>\r\nuser_agent:NI-AAPP<br>\r\nsearch:ph</p>', '<p>{<br>\r\n    \"data\": [<br>\r\n        {<br>\r\n            \"value\": \"R-47\",<br>\r\n            \"label\": \"CRM/Phone/Internet Banking Exec.\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-235\",<br>\r\n            \"label\": \"Bio/Pharma Informatics-Associate/Scientist\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-243\",<br>\r\n            \"label\": \"Pharmacist/Chemist/Bio Chemist\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-389\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-433\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-477\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-521\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-565\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-609\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"R-653\",<br>\r\n            \"label\": \"Graphic/Web Designer\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-Adobe Photoshop\",<br>\r\n            \"label\": \"Adobe Photoshop\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-Delphi\",<br>\r\n            \"label\": \"Delphi\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-Harvard Graphics\",<br>\r\n            \"label\": \"Harvard Graphics\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-PHP\",<br>\r\n            \"label\": \"PHP\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-WebSphere\",<br>\r\n            \"label\": \"WebSphere\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-Graphic Design\",<br>\r\n            \"label\": \"Graphic Design\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"S-iPhone\",<br>\r\n            \"label\": \"iPhone\"<br>\r\n        }<br>\r\n    ]<br>\r\n}</p>', '', 'No'),
(86, 'APPROVED', 'Get city suggestion', 'http://192.168.1.111/job_portal/sign_up/yget_suggestion_cit/', 'POST', '<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p>if u want to return city_name then pass city_name in return</p>', '<p>csrf_job_portal:3564f528016ab673d56f3532fa9917ee<br>\r\nuser_agent:NI-AAPP<br>\r\nsearch:va<br>\r\nreturn:id<br>\r\nreturn_type:array</p>', '<p>{<br>\r\n    \"data\": [<br>\r\n        {<br>\r\n            \"value\": \"2\",<br>\r\n            \"label\": \"Vadodara\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"9\",<br>\r\n            \"label\": \"Valsad\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"51\",<br>\r\n            \"label\": \"Kalyan-Dombivali\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"52\",<br>\r\n            \"label\": \"Vasai-Virar\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"53\",<br>\r\n            \"label\": \"Varanasi\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"70\",<br>\r\n            \"label\": \"Thiruvananthapuram\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"81\",<br>\r\n            \"label\": \"Amravati\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"122\",<br>\r\n            \"label\": \"Davanagere\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"323\",<br>\r\n            \"label\": \"Vancouver\"<br>\r\n        }<br>\r\n    ]<br>\r\n}</p>', '', 'No'),
(87, 'APPROVED', 'Get city suggestion', 'http://192.168.1.111/job_portal/sign_up/get_suggestion_city', 'POST', '<p>if u want to return city name then pass city_name in return pearmeter</p>\r\n\r\n<p> </p>\r\n\r\n<p>see below peramert</p>\r\n\r\n<p> </p>\r\n\r\n<p>return:city_name</p>', '<p>csrf_job_portal:3564f528016ab673d56f3532fa9917ee<br>\r\nuser_agent:NI-AAPP<br>\r\nsearch:va<br>\r\nreturn:id<br>\r\nreturn_type:array</p>', '<p>{<br>\r\n    \"data\": [<br>\r\n        {<br>\r\n            \"value\": \"2\",<br>\r\n            \"label\": \"Vadodara\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"9\",<br>\r\n            \"label\": \"Valsad\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"51\",<br>\r\n            \"label\": \"Kalyan-Dombivali\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"52\",<br>\r\n            \"label\": \"Vasai-Virar\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"53\",<br>\r\n            \"label\": \"Varanasi\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"70\",<br>\r\n            \"label\": \"Thiruvananthapuram\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"81\",<br>\r\n            \"label\": \"Amravati\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"122\",<br>\r\n            \"label\": \"Davanagere\"<br>\r\n        },<br>\r\n        {<br>\r\n            \"value\": \"323\",<br>\r\n            \"label\": \"Vancouver\"<br>\r\n        }<br>\r\n    ]<br>\r\n}</p>', '', 'No'),
(88, 'APPROVED', 'Coupon Code My plan', 'http://192.168.1.111/job_portal/my-plan/check-coupan', 'POST', '<p>To check for discount amount coupon code applied</p>', '<p>user_agent =  NI-AAPP (for Android)<br>\r\ncsrf_job_portal = tocken value</p>\r\n\r\n<p>plan_id = current selected plan id</p>\r\n\r\n<p>couponcode = entered coupon code</p>\r\n\r\n<p>user_type =  as user logged post (job_seeker/employer)</p>\r\n\r\n<p>user_id = current user id</p>', '<p>{\"status\":\"success\",\"tocken\":\"2d461a999f5074ce6c3da9dc00fa5caf\",\"message\":\"Coupan Code applied successfully.\",\"discount_amount\":\"15\"}</p>', '<p>{\"status\":\"error\",\"tocken\":\"2d461a999f5074ce6c3da9dc00fa5caf\",\"message\":\"Invalid Coupan Code\"}</p>', 'No'),
(89, 'APPROVED', 'Update Membership plan', 'http://192.168.1.111/job_portal/my-plan/update_plan_app', 'POST', '<p>For update membership plan to user account</p>', '<p>user_agent - NI-AAPP</p>\r\n\r\n<p>csrf_job_portal - tocken value</p>\r\n\r\n<p>plan_id -  2 (User selected plan id dynamic)</p>\r\n\r\n<p>coupan_code - coupan_code (Leave blank if not applied)</p>\r\n\r\n<p>discount_amount - 15 (if coupan code applied then pass discount amount else leave blank)</p>\r\n\r\n<p>user_type - job_seeker (You can pass job_seeker / employer as current user login)</p>\r\n\r\n<p>user_id - 71    (User id of current logged in user same perameter for both user type)</p>\r\n\r\n<p>payment_method - Credit Card/ Online Transfer</p>\r\n\r\n<p>payment_note - if any else leave blank</p>\r\n\r\n<p>transaction_id - transaction id from payumoney</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>', '<p>{\"tocken\":\"7c78cc1f58ef47b6d29ac92fff46f4bd\",\"status\":\"success\",\"message\":\"Plan Assigned successfully.\"}</p>', '<p>{\"tocken\":\"7c78cc1f58ef47b6d29ac92fff46f4bd\",\"status\":\"error\",\"message\":\"Some error issue ocurred, Please try again.\"}</p>', 'No'),
(90, 'APPROVED', 'payu money payment gateway detail', 'http://192.168.1.111/job_portal/my-plan/get_payudetail', 'POST', '', '<p>user_agent - NI-AAPP</p>\r\n\r\n<p>csrf_job_portal - c49c0c553f709c20e6ca787313fdcca0</p>', '<p>{<br>\r\n    \"status\": \"success\",<br>\r\n    \"marchent_key\": \"EieL94BG\",<br>\r\n    \"salt\": \"tS303T0I6M\",<br>\r\n    \"marchent_id\": \"123456\",<br>\r\n    \"tocken\": \"768692f54b3aa3513b3320c3d7cb9430\"<br>\r\n}</p>', '', 'No'),
(91, 'APPROVED', 'Plan contact validity', 'http://192.168.1.111/job_portal/common_request/give_plan_access_auth', 'POST', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:cee09088f69a644894eb06794d665eed<br>\r\nuser_id/emp_id:61<br>\r\nempid_jsid:10<br>\r\naccess_field:contacts</p>\r\n\r\n<p>user_agent: user agent<br>\r\ncsrf_job_portal: csrf job portal<br>\r\nuser_id: of the job seeker viewing the contact detail of employer or employer viewibng detials of js<br>\r\nempid_jsid: emploer id of the emploeyer of whose details is being viewed<br>\r\naccess_field:contacts</p>', '<p>user_agent:NI-IAAPP<br>\r\ncsrf_job_portal:cee09088f69a644894eb06794d665eed<br>\r\nuser_id:61<br>\r\nempid_jsid:10<br>\r\naccess_field: such as contacts , messages</p>', '<p>{<br>\r\n    \"data\": [],<br>\r\n    \"access\": \"Yes\",<br>\r\n    \"tocken\": \"cee09088f69a644894eb06794d665eed\"<br>\r\n}</p>', '<p>{<br>\r\n    \"data\": [],<br>\r\n    \"access\": \"No\",<br>\r\n    \"tocken\": \"cee09088f69a644894eb06794d665eed\"<br>\r\n}</p>', 'No'),
(92, 'APPROVED', 'Delete Job seeker profile photo', 'http://192.168.1.111/job_portal/my_profile/deleteuploadprofile', 'POST', '<p>For  delete profle photo for job seeker</p>', '<table cellpadding=\"0\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>9133a0a6849ac7e6c5aa3cec789315e2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_id</td>\r\n   <td><code>32</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-</code>AAPP</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{\"tocken\":\"8bea90a01e2b1bf8dc61df77138ccdc7\",\"errormessage\":\"Your Profile image has been deleted successfully.\",\"status\":\"success\"}</p>', '<pre>\r\n{\"tocken\":\"8bea90a01e2b1bf8dc61df77138ccdc7\",\"status\":\"error\",\"errormessage\":\"There was a problem while deleting your profile image.\"}</pre>', 'No'),
(93, 'APPROVED', 'Delete Employed profile photo', 'http://192.168.1.111/job_portal/employer_profile/deleteuploadprofile', 'POST', '<p>For  delete profle photo for job seeker</p>', '<table cellpadding=\"0\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>csrf_job_portal</td>\r\n   <td><code>9133a0a6849ac7e6c5aa3cec789315e2</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>emp_id</td>\r\n   <td><code>32</code></td>\r\n  </tr>\r\n  <tr>\r\n   <td>user_agent</td>\r\n   <td><code>NI-</code>AAPP</td>\r\n  </tr>\r\n </tbody>\r\n</table>', '<p>{\"tocken\":\"8bea90a01e2b1bf8dc61df77138ccdc7\",\"errormessage\":\"Your Profile image has been deleted successfully.\",\"status\":\"success\"}</p>', '<p>{\"tocken\":\"8bea90a01e2b1bf8dc61df77138ccdc7\",\"status\":\"error\",\"errormessage\":\"There was a problem while deleting your profile image.\"}</p>', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(94, 'APPROVED', 'Get Common all data single api', 'http://192.168.1.111/mega_matrimony/common_request/get_common_list_ddr', 'POST', '<p>user_agent:NI-AAPP<br>\r\ncsrf_job_portal:e73122391cc53cf64d5d35fb2a5488fc</p>', '<p>user_agent:NI-AAPP<br>\r\ncsrf_job_portal:e73122391cc53cf64d5d35fb2a5488fc</p>', '<p>{<br>\r\n    \"tocken\": \"fabfc4402cd2905789915a1fbc9d94d9\",<br>\r\n    \"status\": \"success\",<br>\r\n    \"data\": {<br>\r\n        \"marital_status\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Unmarried\",<br>\r\n                \"val\": \"Unmarried\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Widow/Widower\",<br>\r\n                \"val\": \"Widow/Widower\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Divorcee\",<br>\r\n                \"val\": \"Divorcee\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Separated\",<br>\r\n                \"val\": \"Separated\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"diet\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Occasionally Non-Veg\",<br>\r\n                \"val\": \"Occasionally Non-Veg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Veg\",<br>\r\n                \"val\": \"Veg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Eggetarian\",<br>\r\n                \"val\": \"Eggetarian\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Non-Veg\",<br>\r\n                \"val\": \"Non-Veg\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"smoke\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No\",<br>\r\n                \"val\": \"No\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Yes\",<br>\r\n                \"val\": \"Yes\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Occasionally\",<br>\r\n                \"val\": \"Occasionally\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"gender\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Male\",<br>\r\n                \"val\": \"Male\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Female\",<br>\r\n                \"val\": \"Female\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"manglik\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No\",<br>\r\n                \"val\": \"No\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Yes\",<br>\r\n                \"val\": \"Yes\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Maybe\",<br>\r\n                \"val\": \"Maybe\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Anshik\",<br>\r\n                \"val\": \"Anshik\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"drink\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No\",<br>\r\n                \"val\": \"No\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Yes\",<br>\r\n                \"val\": \"Yes\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Occasionally\",<br>\r\n                \"val\": \"Occasionally\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"bodytype\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Slim\",<br>\r\n                \"val\": \"Slim\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Average\",<br>\r\n                \"val\": \"Average\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Athletic\",<br>\r\n                \"val\": \"Athletic\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Heavy\",<br>\r\n                \"val\": \"Heavy\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"complexion\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Wheatish\",<br>\r\n                \"val\": \"Wheatish\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Very Fair\",<br>\r\n                \"val\": \"Very Fair\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Fair\",<br>\r\n                \"val\": \"Fair\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Wheatish Brown\",<br>\r\n                \"val\": \"Wheatish Brown\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Dark\",<br>\r\n                \"val\": \"Dark\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"profileby\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Self\",<br>\r\n                \"val\": \"Self\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Parents\",<br>\r\n                \"val\": \"Parents\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Guardian\",<br>\r\n                \"val\": \"Guardian\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Friends\",<br>\r\n                \"val\": \"Friends\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Sibling\",<br>\r\n                \"val\": \"Sibling\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Relatives\",<br>\r\n                \"val\": \"Relatives\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"reference\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Advertisements\",<br>\r\n                \"val\": \"Advertisements\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Friends\",<br>\r\n                \"val\": \"Friends\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Search Engines\",<br>\r\n                \"val\": \"Search Engines\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Others\",<br>\r\n                \"val\": \"Others\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"blood_group\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"A+\",<br>\r\n                \"val\": \"A+\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"A-\",<br>\r\n                \"val\": \"A-\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"AB+\",<br>\r\n                \"val\": \"AB+\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"AB-\",<br>\r\n                \"val\": \"AB-\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"B+\",<br>\r\n                \"val\": \"B+\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"B-\",<br>\r\n                \"val\": \"B-\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"O+\",<br>\r\n                \"val\": \"O+\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"O-\",<br>\r\n                \"val\": \"O-\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"star_list\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 5,<br>\r\n                \"val\": \"BHARANI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 6,<br>\r\n                \"val\": \"CHITHIRAI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 7,<br>\r\n                \"val\": \"HASTHAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 8,<br>\r\n                \"val\": \"KETTAI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 9,<br>\r\n                \"val\": \"KRITHIGAI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 10,<br>\r\n                \"val\": \"MAHAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 11,<br>\r\n                \"val\": \"MOOLAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 12,<br>\r\n                \"val\": \"MRIGASIRISHAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 13,<br>\r\n                \"val\": \"POOSAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 14,<br>\r\n                \"val\": \"PUNARPUSAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 15,<br>\r\n                \"val\": \"PURADAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 16,<br>\r\n                \"val\": \"PURAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 17,<br>\r\n                \"val\": \"PURATATHI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 18,<br>\r\n                \"val\": \"REVATHI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 19,<br>\r\n                \"val\": \"ROHINI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 20,<br>\r\n                \"val\": \"SADAYAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 21,<br>\r\n                \"val\": \"SWATHI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 22,<br>\r\n                \"val\": \"THIRUVADIRAI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 23,<br>\r\n                \"val\": \"THIRUVONAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 24,<br>\r\n                \"val\": \"UTHRADAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 25,<br>\r\n                \"val\": \"UTHRAM\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 26,<br>\r\n                \"val\": \"UTHRATADHI\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 27,<br>\r\n                \"val\": \"VISAKAM\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"horoscope\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No\",<br>\r\n                \"val\": \"No\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Yes\",<br>\r\n                \"val\": \"Yes\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"moonsign_list\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 11,<br>\r\n                \"val\": \"Kumbha (Aquarious)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 10,<br>\r\n                \"val\": \"Makar (Capricorn)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 12,<br>\r\n                \"val\": \"Meen (Pisces)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 1,<br>\r\n                \"val\": \"Mesh (Aries)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 3,<br>\r\n                \"val\": \"Mithun (Gemini)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 5,<br>\r\n                \"val\": \"Simha (Leo)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 7,<br>\r\n                \"val\": \"Tula (Libra)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 8,<br>\r\n                \"val\": \"Vrischika (Scorpio)\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 2,<br>\r\n                \"val\": \"Vrishabh (Taurus)\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"residence\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Citizen\",<br>\r\n                \"val\": \"Citizen\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Permanent Resident\",<br>\r\n                \"val\": \"Permanent Resident\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Student Visa\",<br>\r\n                \"val\": \"Student Visa\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Temporary Visa\",<br>\r\n                \"val\": \"Temporary Visa\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Work permit\",<br>\r\n                \"val\": \"Work permit\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"employee_in\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Private\",<br>\r\n                \"val\": \"Private\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Government\",<br>\r\n                \"val\": \"Government\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Business\",<br>\r\n                \"val\": \"Business\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Defence\",<br>\r\n                \"val\": \"Defence\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Not Employed in\",<br>\r\n                \"val\": \"Not Employed in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Others\",<br>\r\n                \"val\": \"Others\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"income\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 10,000 - 50,000\",<br>\r\n                \"val\": \"Rs 10,000 - 50,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 50,000 - 1,00,000\",<br>\r\n                \"val\": \"Rs 50,000 - 1,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 1,00,000 - 2,00,000\",<br>\r\n                \"val\": \"Rs 1,00,000 - 2,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 2,00,000 - 5,00,000\",<br>\r\n                \"val\": \"Rs 2,00,000 - 5,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 5,00,000 - 10,00,000\",<br>\r\n                \"val\": \"Rs 5,00,000 - 10,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 10,00,000 - 50,00,000\",<br>\r\n                \"val\": \"Rs 10,00,000 - 50,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rs 50,00,000 - 1,00,00,000\",<br>\r\n                \"val\": \"Rs 50,00,000 - 1,00,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Above Rs 1,00,00,000\",<br>\r\n                \"val\": \"Above Rs 1,00,00,000\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Does not matter\",<br>\r\n                \"val\": \"Does not matter\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"family_type\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Separate Family\",<br>\r\n                \"val\": \"Separate Family\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Joint Family\",<br>\r\n                \"val\": \"Joint Family\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"family_status\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Rich\",<br>\r\n                \"val\": \"Rich\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Upper Middle Class\",<br>\r\n                \"val\": \"Upper Middle Class\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Middle Class\",<br>\r\n                \"val\": \"Middle Class\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Lower Middle Class\",<br>\r\n                \"val\": \"Lower Middle Class\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Poor Family\",<br>\r\n                \"val\": \"Poor Family\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"no_of_brothers\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 0,<br>\r\n                \"val\": \"0\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 1,<br>\r\n                \"val\": \"1\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 2,<br>\r\n                \"val\": \"2\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 3,<br>\r\n                \"val\": \"3\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 4,<br>\r\n                \"val\": \"4\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"4 +\",<br>\r\n                \"val\": \"4 +\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"no_marri_sister\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No married sister\",<br>\r\n                \"val\": \"No married sister\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"One married sister\",<br>\r\n                \"val\": \"One married sister\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Two married sisters\",<br>\r\n                \"val\": \"Two married sisters\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Three married sisters\",<br>\r\n                \"val\": \"Three married sisters\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Four married sisters\",<br>\r\n                \"val\": \"Four married sisters\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Above four married sisters\",<br>\r\n                \"val\": \"Above four married sisters\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"payment_method\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Cash\",<br>\r\n                \"val\": \"Cash\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Credit Card\",<br>\r\n                \"val\": \"Credit Card\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Debit Card\",<br>\r\n                \"val\": \"Debit Card\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Other\",<br>\r\n                \"val\": \"Other\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Cheque\",<br>\r\n                \"val\": \"Cheque\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"mobile_verify_status\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"No\",<br>\r\n                \"val\": \"No\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Yes\",<br>\r\n                \"val\": \"Yes\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"plan_status\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Active\",<br>\r\n                \"val\": \"Active\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Expired\",<br>\r\n                \"val\": \"Expired\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"plan_expired_on\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"today_expire\",<br>\r\n                \"val\": \"Today expire\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"in_week_expire\",<br>\r\n                \"val\": \"In 1 week expire\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"in_month_expire\",<br>\r\n                \"val\": \"In 1 month expire\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"registered_from\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Mobile App\",<br>\r\n                \"val\": \"Mobile App\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Front End\",<br>\r\n                \"val\": \"Front End\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Back end\",<br>\r\n                \"val\": \"Back End\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Other\",<br>\r\n                \"val\": \"Other\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"photo_setting\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"With Photo\",<br>\r\n                \"val\": \"With Photo\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Without Photo\",<br>\r\n                \"val\": \"Without Photo\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"country_code\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+93\",<br>\r\n                \"val\": \"Afghanistan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+263\",<br>\r\n                \"val\": \"Zimbabwe\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+355\",<br>\r\n                \"val\": \"Albania\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+213\",<br>\r\n                \"val\": \"Algeria\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1684\",<br>\r\n                \"val\": \"American Samoa\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+376\",<br>\r\n                \"val\": \"Andorra\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+244\",<br>\r\n                \"val\": \"Angola\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+672\",<br>\r\n                \"val\": \"Antarctica\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1268\",<br>\r\n                \"val\": \"Antigua And Barbuda\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+54\",<br>\r\n                \"val\": \"Argentina\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+374\",<br>\r\n                \"val\": \"Armenia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+297\",<br>\r\n                \"val\": \"Aruba\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+61\",<br>\r\n                \"val\": \"Australia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+43\",<br>\r\n                \"val\": \"Austria\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+994\",<br>\r\n                \"val\": \"Azerbaijan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1242\",<br>\r\n                \"val\": \"Bahamas\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+973\",<br>\r\n                \"val\": \"Bahrain\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+880\",<br>\r\n                \"val\": \"Bangladesh\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1246\",<br>\r\n                \"val\": \"Barbados\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+375\",<br>\r\n                \"val\": \"Belarus\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+32\",<br>\r\n                \"val\": \"Belgium\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+501\",<br>\r\n                \"val\": \"Belize\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+229\",<br>\r\n                \"val\": \"Benin\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1441\",<br>\r\n                \"val\": \"Bermuda\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+975\",<br>\r\n                \"val\": \"Bhutan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+591\",<br>\r\n                \"val\": \"Bolivia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+387\",<br>\r\n                \"val\": \"Bosnia And Herzegovina\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+267\",<br>\r\n                \"val\": \"Botswana\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+47\",<br>\r\n                \"val\": \"Svalbard And Jan Mayen\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+55\",<br>\r\n                \"val\": \"Brazil\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+246\",<br>\r\n                \"val\": \"British Indian Ocean Territory\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+673\",<br>\r\n                \"val\": \"Brunei\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+359\",<br>\r\n                \"val\": \"Bulgaria\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+226\",<br>\r\n                \"val\": \"Burkina Faso\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+257\",<br>\r\n                \"val\": \"Burundi\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+855\",<br>\r\n                \"val\": \"Cambodia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+237\",<br>\r\n                \"val\": \"Cameroon\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1\",<br>\r\n                \"val\": \"United States Minor Outlying Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+238\",<br>\r\n                \"val\": \"Cape Verde\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+236\",<br>\r\n                \"val\": \"Central African Republic\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+235\",<br>\r\n                \"val\": \"Chad\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+56\",<br>\r\n                \"val\": \"Chile\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+86\",<br>\r\n                \"val\": \"China\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+57\",<br>\r\n                \"val\": \"Colombia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+269\",<br>\r\n                \"val\": \"Mayotte\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+506\",<br>\r\n                \"val\": \"Costa Rica\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+385\",<br>\r\n                \"val\": \"Croatia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+53\",<br>\r\n                \"val\": \"Cuba\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+357\",<br>\r\n                \"val\": \"Cyprus\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+420\",<br>\r\n                \"val\": \"Czech Republic\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+242\",<br>\r\n                \"val\": \"Republic Of The Congo\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+45\",<br>\r\n                \"val\": \"Denmark\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+253\",<br>\r\n                \"val\": \"Djibouti\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1767\",<br>\r\n                \"val\": \"Dominica\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1809\",<br>\r\n                \"val\": \"Dominican Republic\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+670\",<br>\r\n                \"val\": \"East Timor\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+593\",<br>\r\n                \"val\": \"Ecuador\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+20\",<br>\r\n                \"val\": \"Egypt\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+503\",<br>\r\n                \"val\": \"El Salvador\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+240\",<br>\r\n                \"val\": \"Equatorial Guinea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+291\",<br>\r\n                \"val\": \"Eritrea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+372\",<br>\r\n                \"val\": \"Estonia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+251\",<br>\r\n                \"val\": \"Ethiopia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+298\",<br>\r\n                \"val\": \"Faroe Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+679\",<br>\r\n                \"val\": \"Fiji\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+358\",<br>\r\n                \"val\": \"Finland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+33\",<br>\r\n                \"val\": \"France\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+594\",<br>\r\n                \"val\": \"French Guiana\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+689\",<br>\r\n                \"val\": \"French Polynesia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+260\",<br>\r\n                \"val\": \"Zambia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+241\",<br>\r\n                \"val\": \"Gabon\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+220\",<br>\r\n                \"val\": \"Gambia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+995\",<br>\r\n                \"val\": \"Georgia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+49\",<br>\r\n                \"val\": \"Germany\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+233\",<br>\r\n                \"val\": \"Ghana\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+30\",<br>\r\n                \"val\": \"Greece\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+299\",<br>\r\n                \"val\": \"Greenland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1473\",<br>\r\n                \"val\": \"Grenada\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+590\",<br>\r\n                \"val\": \"Guadeloupe\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1671\",<br>\r\n                \"val\": \"Guam\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+502\",<br>\r\n                \"val\": \"Guatemala\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+224\",<br>\r\n                \"val\": \"Guinea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+245\",<br>\r\n                \"val\": \"Guinea-Bissau\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+592\",<br>\r\n                \"val\": \"Guyana\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+509\",<br>\r\n                \"val\": \"Haiti\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+504\",<br>\r\n                \"val\": \"Honduras\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+852\",<br>\r\n                \"val\": \"Hong Kong\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+36\",<br>\r\n                \"val\": \"Hungary\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+354\",<br>\r\n                \"val\": \"Iceland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+91\",<br>\r\n                \"val\": \"India\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+62\",<br>\r\n                \"val\": \"Indonesia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+98\",<br>\r\n                \"val\": \"Iran\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+964\",<br>\r\n                \"val\": \"Iraq\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+353\",<br>\r\n                \"val\": \"Ireland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+972\",<br>\r\n                \"val\": \"Israel\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+39\",<br>\r\n                \"val\": \"Italy\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+225\",<br>\r\n                \"val\": \"Ivory Coast\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1876\",<br>\r\n                \"val\": \"Jamaica\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+81\",<br>\r\n                \"val\": \"Japan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+962\",<br>\r\n                \"val\": \"Jordan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+7\",<br>\r\n                \"val\": \"Kazakhstan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+254\",<br>\r\n                \"val\": \"Kenya\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+686\",<br>\r\n                \"val\": \"Kiribati\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+965\",<br>\r\n                \"val\": \"Kuwait\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+996\",<br>\r\n                \"val\": \"Kyrgyzstan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+856\",<br>\r\n                \"val\": \"Laos\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+371\",<br>\r\n                \"val\": \"Latvia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+961\",<br>\r\n                \"val\": \"Lebanon\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+266\",<br>\r\n                \"val\": \"Lesotho\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+231\",<br>\r\n                \"val\": \"Liberia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+218\",<br>\r\n                \"val\": \"Libya\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+423\",<br>\r\n                \"val\": \"Liechtenstein\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+370\",<br>\r\n                \"val\": \"Lithuania\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+352\",<br>\r\n                \"val\": \"Luxembourg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+853\",<br>\r\n                \"val\": \"Macao\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+389\",<br>\r\n                \"val\": \"Macedonia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+261\",<br>\r\n                \"val\": \"Madagascar\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+265\",<br>\r\n                \"val\": \"Malawi\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+60\",<br>\r\n                \"val\": \"Malaysia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+960\",<br>\r\n                \"val\": \"Maldives\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+223\",<br>\r\n                \"val\": \"Mali\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+692\",<br>\r\n                \"val\": \"Marshall Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+596\",<br>\r\n                \"val\": \"Martinique\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+222\",<br>\r\n                \"val\": \"Mauritania\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+230\",<br>\r\n                \"val\": \"Mauritius\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+52\",<br>\r\n                \"val\": \"Mexico\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+691\",<br>\r\n                \"val\": \"Micronesia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+373\",<br>\r\n                \"val\": \"Moldova\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+377\",<br>\r\n                \"val\": \"Monaco\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+976\",<br>\r\n                \"val\": \"Mongolia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1664\",<br>\r\n                \"val\": \"Montserrat\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+212\",<br>\r\n                \"val\": \"Western Sahara\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+258\",<br>\r\n                \"val\": \"Mozambique\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+95\",<br>\r\n                \"val\": \"Myanmar\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+264\",<br>\r\n                \"val\": \"Namibia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+674\",<br>\r\n                \"val\": \"Nauru\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+977\",<br>\r\n                \"val\": \"Nepal\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+31\",<br>\r\n                \"val\": \"Netherlands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+687\",<br>\r\n                \"val\": \"New Caledonia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+64\",<br>\r\n                \"val\": \"New Zealand\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+505\",<br>\r\n                \"val\": \"Nicaragua\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+227\",<br>\r\n                \"val\": \"Niger\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+234\",<br>\r\n                \"val\": \"Nigeria\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+850\",<br>\r\n                \"val\": \"North Korea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1670\",<br>\r\n                \"val\": \"Northern Mariana Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+968\",<br>\r\n                \"val\": \"Oman\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+92\",<br>\r\n                \"val\": \"Pakistan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+680\",<br>\r\n                \"val\": \"Palau\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+970\",<br>\r\n                \"val\": \"Palestinian Territory\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+507\",<br>\r\n                \"val\": \"Panama\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+675\",<br>\r\n                \"val\": \"Papua New Guinea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+595\",<br>\r\n                \"val\": \"Paraguay\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+51\",<br>\r\n                \"val\": \"Peru\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+63\",<br>\r\n                \"val\": \"Philippines\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+48\",<br>\r\n                \"val\": \"Poland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+351\",<br>\r\n                \"val\": \"Portugal\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1787\",<br>\r\n                \"val\": \"Puerto Rico\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+974\",<br>\r\n                \"val\": \"Qatar\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+262\",<br>\r\n                \"val\": \"Reunion\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+40\",<br>\r\n                \"val\": \"Romania\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+70\",<br>\r\n                \"val\": \"Russia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+250\",<br>\r\n                \"val\": \"Rwanda\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+290\",<br>\r\n                \"val\": \"Saint Helena\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1869\",<br>\r\n                \"val\": \"Saint Kitts And Nevis\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1758\",<br>\r\n                \"val\": \"Saint Lucia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+508\",<br>\r\n                \"val\": \"Saint Pierre And Miquelon\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1784\",<br>\r\n                \"val\": \"Saint Vincent And The Grenadines\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+684\",<br>\r\n                \"val\": \"Samoa\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+378\",<br>\r\n                \"val\": \"San Marino\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+239\",<br>\r\n                \"val\": \"Sao Tome And Principe\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+966\",<br>\r\n                \"val\": \"Saudi Arabia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+221\",<br>\r\n                \"val\": \"Senegal\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+248\",<br>\r\n                \"val\": \"Seychelles\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+232\",<br>\r\n                \"val\": \"Sierra Leone\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+65\",<br>\r\n                \"val\": \"Singapore\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+421\",<br>\r\n                \"val\": \"Slovakia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+386\",<br>\r\n                \"val\": \"Slovenia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+677\",<br>\r\n                \"val\": \"Solomon Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+252\",<br>\r\n                \"val\": \"Somalia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+27\",<br>\r\n                \"val\": \"South Africa\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+82\",<br>\r\n                \"val\": \"South Korea\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+34\",<br>\r\n                \"val\": \"Spain\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+94\",<br>\r\n                \"val\": \"Sri Lanka\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+249\",<br>\r\n                \"val\": \"Sudan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+597\",<br>\r\n                \"val\": \"Suriname\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+268\",<br>\r\n                \"val\": \"Swaziland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+46\",<br>\r\n                \"val\": \"Sweden\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+41\",<br>\r\n                \"val\": \"Switzerland\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+963\",<br>\r\n                \"val\": \"Syria\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+886\",<br>\r\n                \"val\": \"Taiwan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+992\",<br>\r\n                \"val\": \"Tajikistan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+255\",<br>\r\n                \"val\": \"Tanzania\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+66\",<br>\r\n                \"val\": \"Thailand\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+228\",<br>\r\n                \"val\": \"Togo\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+690\",<br>\r\n                \"val\": \"Tokelau\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+676\",<br>\r\n                \"val\": \"Tonga\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1868\",<br>\r\n                \"val\": \"Trinidad And Tobago\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+216\",<br>\r\n                \"val\": \"Tunisia\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+90\",<br>\r\n                \"val\": \"Turkey\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+7370\",<br>\r\n                \"val\": \"Turkmenistan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+688\",<br>\r\n                \"val\": \"Tuvalu\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+1340\",<br>\r\n                \"val\": \"U.S. Virgin Islands\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+256\",<br>\r\n                \"val\": \"Uganda\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+380\",<br>\r\n                \"val\": \"Ukraine\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+971\",<br>\r\n                \"val\": \"United Arab Emirates\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+44\",<br>\r\n                \"val\": \"United Kingdom\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+598\",<br>\r\n                \"val\": \"Uruguay\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+998\",<br>\r\n                \"val\": \"Uzbekistan\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+678\",<br>\r\n                \"val\": \"Vanuatu\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+58\",<br>\r\n                \"val\": \"Venezuela\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+84\",<br>\r\n                \"val\": \"Vietnam\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+681\",<br>\r\n                \"val\": \"Wallis And Futuna\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"+967\",<br>\r\n                \"val\": \"Yemen\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"weight_list\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 40,<br>\r\n                \"val\": \"40 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 41,<br>\r\n                \"val\": \"41 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 42,<br>\r\n                \"val\": \"42 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 43,<br>\r\n                \"val\": \"43 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 44,<br>\r\n                \"val\": \"44 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 45,<br>\r\n                \"val\": \"45 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 46,<br>\r\n                \"val\": \"46 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 47,<br>\r\n                \"val\": \"47 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 48,<br>\r\n                \"val\": \"48 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 49,<br>\r\n                \"val\": \"49 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 50,<br>\r\n                \"val\": \"50 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 51,<br>\r\n                \"val\": \"51 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 52,<br>\r\n                \"val\": \"52 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 53,<br>\r\n                \"val\": \"53 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 54,<br>\r\n                \"val\": \"54 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 55,<br>\r\n                \"val\": \"55 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 56,<br>\r\n                \"val\": \"56 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 57,<br>\r\n                \"val\": \"57 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 58,<br>\r\n                \"val\": \"58 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 59,<br>\r\n                \"val\": \"59 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 60,<br>\r\n                \"val\": \"60 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 61,<br>\r\n                \"val\": \"61 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 62,<br>\r\n                \"val\": \"62 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 63,<br>\r\n                \"val\": \"63 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 64,<br>\r\n                \"val\": \"64 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 65,<br>\r\n                \"val\": \"65 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 66,<br>\r\n                \"val\": \"66 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 67,<br>\r\n                \"val\": \"67 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 68,<br>\r\n                \"val\": \"68 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 69,<br>\r\n                \"val\": \"69 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 70,<br>\r\n                \"val\": \"70 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 71,<br>\r\n                \"val\": \"71 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 72,<br>\r\n                \"val\": \"72 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 73,<br>\r\n                \"val\": \"73 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 74,<br>\r\n                \"val\": \"74 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 75,<br>\r\n                \"val\": \"75 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 76,<br>\r\n                \"val\": \"76 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 77,<br>\r\n                \"val\": \"77 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 78,<br>\r\n                \"val\": \"78 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 79,<br>\r\n                \"val\": \"79 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 80,<br>\r\n                \"val\": \"80 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 81,<br>\r\n                \"val\": \"81 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 82,<br>\r\n                \"val\": \"82 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 83,<br>\r\n                \"val\": \"83 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 84,<br>\r\n                \"val\": \"84 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 85,<br>\r\n                \"val\": \"85 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 86,<br>\r\n                \"val\": \"86 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 87,<br>\r\n                \"val\": \"87 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 88,<br>\r\n                \"val\": \"88 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 89,<br>\r\n                \"val\": \"89 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 90,<br>\r\n                \"val\": \"90 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 91,<br>\r\n                \"val\": \"91 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 92,<br>\r\n                \"val\": \"92 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 93,<br>\r\n                \"val\": \"93 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 94,<br>\r\n                \"val\": \"94 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 95,<br>\r\n                \"val\": \"95 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 96,<br>\r\n                \"val\": \"96 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 97,<br>\r\n                \"val\": \"97 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 98,<br>\r\n                \"val\": \"98 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 99,<br>\r\n                \"val\": \"99 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 100,<br>\r\n                \"val\": \"100 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 101,<br>\r\n                \"val\": \"101 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 102,<br>\r\n                \"val\": \"102 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 103,<br>\r\n                \"val\": \"103 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 104,<br>\r\n                \"val\": \"104 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 105,<br>\r\n                \"val\": \"105 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 106,<br>\r\n                \"val\": \"106 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 107,<br>\r\n                \"val\": \"107 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 108,<br>\r\n                \"val\": \"108 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 109,<br>\r\n                \"val\": \"109 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 110,<br>\r\n                \"val\": \"110 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 111,<br>\r\n                \"val\": \"111 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 112,<br>\r\n                \"val\": \"112 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 113,<br>\r\n                \"val\": \"113 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 114,<br>\r\n                \"val\": \"114 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 115,<br>\r\n                \"val\": \"115 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 116,<br>\r\n                \"val\": \"116 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 117,<br>\r\n                \"val\": \"117 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 118,<br>\r\n                \"val\": \"118 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 119,<br>\r\n                \"val\": \"119 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 120,<br>\r\n                \"val\": \"120 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 121,<br>\r\n                \"val\": \"121 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 122,<br>\r\n                \"val\": \"122 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 123,<br>\r\n                \"val\": \"123 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 124,<br>\r\n                \"val\": \"124 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 125,<br>\r\n                \"val\": \"125 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 126,<br>\r\n                \"val\": \"126 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 127,<br>\r\n                \"val\": \"127 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 128,<br>\r\n                \"val\": \"128 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 129,<br>\r\n                \"val\": \"129 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 130,<br>\r\n                \"val\": \"130 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 131,<br>\r\n                \"val\": \"131 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 132,<br>\r\n                \"val\": \"132 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 133,<br>\r\n                \"val\": \"133 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 134,<br>\r\n                \"val\": \"134 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 135,<br>\r\n                \"val\": \"135 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 136,<br>\r\n                \"val\": \"136 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 137,<br>\r\n                \"val\": \"137 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 138,<br>\r\n                \"val\": \"138 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 139,<br>\r\n                \"val\": \"139 Kg\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 140,<br>\r\n                \"val\": \"140 Kg\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"height_list\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 48,<br>\r\n                \"val\": \"Below 4ft\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 54,<br>\r\n                \"val\": \"4ft 6in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 55,<br>\r\n                \"val\": \"4ft 7in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 56,<br>\r\n                \"val\": \"4ft 8in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 57,<br>\r\n                \"val\": \"4ft 9in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 58,<br>\r\n                \"val\": \"4ft 10in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 59,<br>\r\n                \"val\": \"4ft 11in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 60,<br>\r\n                \"val\": \"5ft\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 61,<br>\r\n                \"val\": \"5ft 1in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 62,<br>\r\n                \"val\": \"5ft 2in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 63,<br>\r\n                \"val\": \"5ft 3in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 64,<br>\r\n                \"val\": \"5ft 4in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 65,<br>\r\n                \"val\": \"5ft 5in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 66,<br>\r\n                \"val\": \"5ft 6in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 67,<br>\r\n                \"val\": \"5ft 7in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 68,<br>\r\n                \"val\": \"5ft 8in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 69,<br>\r\n                \"val\": \"5ft 9in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 70,<br>\r\n                \"val\": \"5ft 10in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 71,<br>\r\n                \"val\": \"5ft 11in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 72,<br>\r\n                \"val\": \"6ft\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 73,<br>\r\n                \"val\": \"6ft 1in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 74,<br>\r\n                \"val\": \"6ft 2in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 75,<br>\r\n                \"val\": \"6ft 3in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 76,<br>\r\n                \"val\": \"6ft 4in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 77,<br>\r\n                \"val\": \"6ft 5in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 78,<br>\r\n                \"val\": \"6ft 6in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 79,<br>\r\n                \"val\": \"6ft 7in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 80,<br>\r\n                \"val\": \"6ft 8in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 81,<br>\r\n                \"val\": \"6ft 9in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 82,<br>\r\n                \"val\": \"6ft 10in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 83,<br>\r\n                \"val\": \"6ft 11in\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 84,<br>\r\n                \"val\": \"7ft\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 85,<br>\r\n                \"val\": \"Above 7ft\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"age_rang\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 18,<br>\r\n                \"val\": \"18 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 19,<br>\r\n                \"val\": \"19 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 20,<br>\r\n                \"val\": \"20 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 21,<br>\r\n                \"val\": \"21 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 22,<br>\r\n                \"val\": \"22 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 23,<br>\r\n                \"val\": \"23 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 24,<br>\r\n                \"val\": \"24 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 25,<br>\r\n                \"val\": \"25 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 26,<br>\r\n                \"val\": \"26 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 27,<br>\r\n                \"val\": \"27 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 28,<br>\r\n                \"val\": \"28 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 29,<br>\r\n                \"val\": \"29 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 30,<br>\r\n                \"val\": \"30 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 31,<br>\r\n                \"val\": \"31 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 32,<br>\r\n                \"val\": \"32 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 33,<br>\r\n                \"val\": \"33 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 34,<br>\r\n                \"val\": \"34 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 35,<br>\r\n                \"val\": \"35 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 36,<br>\r\n                \"val\": \"36 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 37,<br>\r\n                \"val\": \"37 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 38,<br>\r\n                \"val\": \"38 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 39,<br>\r\n                \"val\": \"39 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 40,<br>\r\n                \"val\": \"40 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 41,<br>\r\n                \"val\": \"41 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 42,<br>\r\n                \"val\": \"42 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 43,<br>\r\n                \"val\": \"43 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 44,<br>\r\n                \"val\": \"44 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 45,<br>\r\n                \"val\": \"45 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 46,<br>\r\n                \"val\": \"46 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 47,<br>\r\n                \"val\": \"47 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 48,<br>\r\n                \"val\": \"48 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 49,<br>\r\n                \"val\": \"49 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 50,<br>\r\n                \"val\": \"50 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 51,<br>\r\n                \"val\": \"51 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 52,<br>\r\n                \"val\": \"52 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 53,<br>\r\n                \"val\": \"53 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 54,<br>\r\n                \"val\": \"54 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 55,<br>\r\n                \"val\": \"55 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 56,<br>\r\n                \"val\": \"56 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 57,<br>\r\n                \"val\": \"57 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 58,<br>\r\n                \"val\": \"58 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 59,<br>\r\n                \"val\": \"59 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 60,<br>\r\n                \"val\": \"60 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 61,<br>\r\n                \"val\": \"61 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 62,<br>\r\n                \"val\": \"62 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 63,<br>\r\n                \"val\": \"63 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 64,<br>\r\n                \"val\": \"64 Year\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 65,<br>\r\n                \"val\": \"65 Year\"<br>\r\n            }<br>\r\n        ],<br>\r\n        \"total_children\": [<br>\r\n            {<br>\r\n                \"id\": \"\",<br>\r\n                \"val\": \"Select Option\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": 0,<br>\r\n                \"val\": \"None\"<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"One\",<br>\r\n                \"val\": 1<br>\r\n            },<br>\r\n            {<br>\r\n                \"id\": \"Two\",<br>\r\n                \"val\": \"2\"<br>\r\n            },<br>\r\n            {<br>\r\n        ', '', 'No');
INSERT INTO `web_service` (`id`, `status`, `service_name`, `service_url`, `method`, `description`, `perameter`, `success_response`, `error_response`, `is_deleted`) VALUES
(95, 'APPROVED', 'Sent OTP For mobile verification', 'http://192.168.1.111/job_portal/original_script/my_profile/varify_mobile_send_otp', 'POST', '<p>Sent OTP jobseeker or employer for mobile verification</p>', '<p>user_agent:NI-AAPP<br>\r\ncsrf_job_portal:c49c0c553f709c20e6ca787313fdcca0<br>\r\nuser_id:<br>\r\nemp_id:115</p>', '<p>{<br>\r\n    \"status\": \"success\",<br>\r\n    \"error_meessage\": \"Your OTP sent on your mobile(xzgsvedsg-1234899)\",<br>\r\n    \"generate_OTP\": 7747,<br>\r\n    \"tocken\": \"ee0350ce169cb680d51301d8bf7d2c04\"<br>\r\n}</p>', '<p>{<br>\r\n    \"status\": \"error\",<br>\r\n    \"error_meessage\": \"Some error Occured, please try again.\",<br>\r\n    \"tocken\": \"ee0350ce169cb680d51301d8bf7d2c04\"<br>\r\n}</p>', 'No'),
(96, 'APPROVED', 'Verify  OTP For mobile verification', 'http://192.168.1.111/job_portal/original_script/my_profile/varify_mobile_send_otp', 'POST', '<p>Verify OTP jobseeker or employer for mobile verification</p>', '<p>user_agent:NI-AAPP<br>\r\ncsrf_job_portal:c49c0c553f709c20e6ca787313fdcca0<br>\r\nuser_id:<br>\r\nemp_id:115<br>\r\notp_mobile:7747</p>', '<p>{<br>\r\n    \"status\": \"success\",<br>\r\n    \"error_meessage\": \"Your mobile number verifired successfully.\",<br>\r\n    \"tocken\": \"ee0350ce169cb680d51301d8bf7d2c04\"<br>\r\n}</p>', '<p>{<br>\r\n    \"status\": \"error\",<br>\r\n    \"error_meessage\": \"Please enter Valid OTP sent on your mobile number\",<br>\r\n    \"tocken\": \"ee0350ce169cb680d51301d8bf7d2c04\"<br>\r\n}</p>', 'No');

-- --------------------------------------------------------

--
-- Structure for view `delete_employer_view`
--
DROP TABLE IF EXISTS `delete_employer_view`;

CREATE  VIEW `delete_employer_view`  AS  select `dr`.`id` AS `id`,`dr`.`req_id` AS `req_id`,`dr`.`reason_for_del` AS `reason_for_del`,`dr`.`is_deleted` AS `is_deleted`,`employer_master`.`email` AS `email`,`employer_master`.`fullname` AS `fullname` from (`delete_profile_request` `dr` left join `employer_master` on((`dr`.`req_id` = `employer_master`.`id`))) where (`dr`.`user_type` = 'emp') ;

-- --------------------------------------------------------

--
-- Structure for view `delete_job_seekar_view`
--
DROP TABLE IF EXISTS `delete_job_seekar_view`;

CREATE  VIEW `delete_job_seekar_view`  AS  select `dr`.`id` AS `id`,`dr`.`req_id` AS `req_id`,`dr`.`reason_for_del` AS `reason_for_del`,`dr`.`is_deleted` AS `is_deleted`,`jobseeker`.`email` AS `email`,`jobseeker`.`fullname` AS `fullname` from (`delete_profile_request` `dr` left join `jobseeker` on((`dr`.`req_id` = `jobseeker`.`id`))) where (`dr`.`user_type` = 'js') ;

-- --------------------------------------------------------

--
-- Structure for view `employer_master_view`
--
DROP TABLE IF EXISTS `employer_master_view`;

CREATE  VIEW `employer_master_view`  AS  select `emaster`.`id` AS `id`,`emaster`.`email` AS `email`,`emaster`.`email_ver_str` AS `email_ver_str`,`emaster`.`email_ver_status` AS `email_ver_status`,`emaster`.`title` AS `title`,`emaster`.`fullname` AS `fullname`,`emaster`.`designation` AS `designation`,`emaster`.`functional_area` AS `functional_area`,`emaster`.`job_profile` AS `job_profile`,`emaster`.`email_verified` AS `email_verified`,`emaster`.`status` AS `status`,`emaster`.`password` AS `password`,`emaster`.`country` AS `country`,`emaster`.`city` AS `city`,`emaster`.`address` AS `address`,`emaster`.`pincode` AS `pincode`,`emaster`.`mobile` AS `mobile`,`emaster`.`mobile_verified` AS `mobile_verified`,`emaster`.`profile_pic` AS `profile_pic`,`emaster`.`profile_pic_approval` AS `profile_pic_approval`,`emaster`.`company_name` AS `company_name`,`emaster`.`company_profile` AS `company_profile`,`emaster`.`company_website` AS `company_website`,`emaster`.`company_email` AS `company_email`,`emaster`.`company_type` AS `company_type`,`emaster`.`company_size` AS `company_size`,`emaster`.`company_logo` AS `company_logo`,`emaster`.`company_logo_approval` AS `company_logo_approval`,`emaster`.`industry` AS `industry`,`emaster`.`industry_hire` AS `industry_hire`,`emaster`.`function_area_hire` AS `function_area_hire`,`emaster`.`skill_hire` AS `skill_hire`,`emaster`.`register_date` AS `register_date`,`emaster`.`facebook_url` AS `facebook_url`,`emaster`.`gplus_url` AS `gplus_url`,`emaster`.`twitter_url` AS `twitter_url`,`emaster`.`linkedin_url` AS `linkedin_url`,`emaster`.`user_agent` AS `user_agent`,`emaster`.`device_id` AS `device_id`,`emaster`.`last_login` AS `last_login`,`emaster`.`is_deleted` AS `is_deleted`,`emaster`.`plan_status` AS `plan_status`,`emaster`.`plan_name` AS `plan_name`,`emaster`.`plan_expired` AS `plan_expired`,`cm`.`city_name` AS `city_name`,`cmm`.`country_name` AS `country_name`,`pt`.`personal_titles` AS `personal_titles`,`im`.`industries_name` AS `industries_name`,`ctype`.`company_type` AS `company_type_name`,`csize`.`company_size` AS `company_size_name` from ((((((`employer_master` `emaster` left join `personal_titles_master` `pt` on((`emaster`.`title` = `pt`.`id`))) left join `city_master` `cm` on((`emaster`.`city` = `cm`.`id`))) left join `country_master` `cmm` on((`emaster`.`country` = `cmm`.`id`))) left join `industries_master` `im` on((`emaster`.`industry` = `im`.`id`))) left join `company_type_master` `ctype` on((`emaster`.`company_type` = `ctype`.`id`))) left join `company_size_master` `csize` on((`emaster`.`company_size` = `csize`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `fn_area_role_view`
--
DROP TABLE IF EXISTS `fn_area_role_view`;

CREATE  VIEW `fn_area_role_view`  AS  select `rm`.`id` AS `id`,`rm`.`status` AS `status`,`rm`.`functional_area` AS `functional_area`,`rm`.`role_name` AS `role_name`,`rm`.`is_deleted` AS `is_deleted`,`fnarea`.`status` AS `fnarea_status`,`fnarea`.`functional_name` AS `functional_name`,`fnarea`.`is_deleted` AS `fnarea_is_deleted` from (`role_master` `rm` left join `functional_area_master` `fnarea` on((`fnarea`.`id` = `rm`.`functional_area`))) group by `rm`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `jobseeker_view`
--
DROP TABLE IF EXISTS `jobseeker_view`;

CREATE  VIEW `jobseeker_view`  AS  select `js`.`id` AS `id`,`js`.`facebook_id` AS `facebook_id`,`js`.`gplus_id` AS `gplus_id`,`js`.`status` AS `status`,`js`.`email` AS `email`,`js`.`password` AS `password`,`js`.`email_ver_str` AS `email_ver_str`,`js`.`verify_email` AS `verify_email`,`js`.`title` AS `title`,`js`.`fullname` AS `fullname`,`js`.`gender` AS `gender`,`js`.`birthdate` AS `birthdate`,`js`.`marital_status` AS `marital_status`,`js`.`address` AS `address`,`js`.`city` AS `city`,`js`.`country` AS `country`,`js`.`mobile` AS `mobile`,`js`.`mobile_verified` AS `mobile_verified`,`js`.`landline` AS `landline`,`js`.`pincode` AS `pincode`,`js`.`website` AS `website`,`js`.`profile_pic` AS `profile_pic`,`js`.`profile_pic_approval` AS `profile_pic_approval`,`js`.`profile_summary` AS `profile_summary`,`js`.`home_city` AS `home_city`,`js`.`preferred_city` AS `preferred_city`,`js`.`resume_headline` AS `resume_headline`,`js`.`total_experience` AS `total_experience`,`js`.`annual_salary` AS `annual_salary`,`js`.`currency_type` AS `currency_type`,`js`.`industry` AS `industry`,`js`.`functional_area` AS `functional_area`,`js`.`job_role` AS `job_role`,`js`.`key_skill` AS `key_skill`,`js`.`email_verification` AS `email_verification`,`js`.`resume_file` AS `resume_file`,`js`.`resume_last_update` AS `resume_last_update`,`js`.`resume_verification` AS `resume_verification`,`js`.`desire_job_type` AS `desire_job_type`,`js`.`employment_type` AS `employment_type`,`js`.`prefered_shift` AS `prefered_shift`,`js`.`expected_annual_salary` AS `expected_annual_salary`,`js`.`exp_salary_currency_type` AS `exp_salary_currency_type`,`js`.`register_date` AS `register_date`,`js`.`last_login` AS `last_login`,`js`.`profile_visibility` AS `profile_visibility`,`js`.`facebook_url` AS `facebook_url`,`js`.`gplus_url` AS `gplus_url`,`js`.`twitter_url` AS `twitter_url`,`js`.`linkedin_url` AS `linkedin_url`,`js`.`device_id` AS `device_id`,`js`.`user_agent` AS `user_agent`,`js`.`user_ip` AS `user_ip`,`js`.`plan_status` AS `plan_status`,`js`.`is_deleted` AS `is_deleted`,`js`.`plan_name` AS `plan_name`,`js`.`plan_expired` AS `plan_expired`,`pt`.`personal_titles` AS `personal_titles`,`ms`.`marital_status` AS `marital_status_val`,`cm`.`city_name` AS `city_name`,`cmm`.`country_name` AS `country_name`,`im`.`industries_name` AS `industries_name`,`fam`.`functional_name` AS `functional_name`,`rm`.`role_name` AS `role_name`,`sr`.`salary_range` AS `annual_salary_name`,`sre`.`salary_range` AS `ex_annual_salary_name` from (((((((((`jobseeker` `js` left join `personal_titles_master` `pt` on((`js`.`title` = `pt`.`id`))) left join `marital_status_master` `ms` on((`js`.`marital_status` = `ms`.`id`))) left join `city_master` `cm` on((`js`.`city` = `cm`.`id`))) left join `country_master` `cmm` on((`js`.`country` = `cmm`.`id`))) left join `industries_master` `im` on((`js`.`industry` = `im`.`id`))) left join `functional_area_master` `fam` on((`js`.`functional_area` = `fam`.`id`))) left join `role_master` `rm` on((`js`.`job_role` = `rm`.`id`))) left join `salary_range` `sr` on((`js`.`annual_salary` = `sr`.`id`))) left join `salary_range` `sre` on((`js`.`expected_annual_salary` = `sre`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `jobseeker_workhistory_view`
--
DROP TABLE IF EXISTS `jobseeker_workhistory_view`;

CREATE  VIEW `jobseeker_workhistory_view`  AS  select `jw`.`id` AS `id`,`jw`.`js_id` AS `js_id`,`jw`.`company_name` AS `company_name`,`jw`.`joining_date` AS `joining_date`,`jw`.`leaving_date` AS `leaving_date`,`jw`.`industry` AS `industry`,`jw`.`functional_area` AS `functional_area`,`jw`.`job_role` AS `job_role`,`jw`.`annual_salary` AS `annual_salary`,`jw`.`achievements` AS `achievements`,`jw`.`update_on` AS `update_on`,`jw`.`currency_type` AS `currency_type`,`im`.`industries_name` AS `industries_name`,`fam`.`functional_name` AS `functional_name`,`rm`.`role_name` AS `role_name`,`sr`.`salary_range` AS `salary_range` from ((((`jobseeker_workhistory` `jw` left join `industries_master` `im` on((`jw`.`industry` = `im`.`id`))) left join `functional_area_master` `fam` on((`jw`.`functional_area` = `fam`.`id`))) left join `role_master` `rm` on((`jw`.`job_role` = `rm`.`id`))) left join `salary_range` `sr` on((`jw`.`annual_salary` = `sr`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `job_posting_view`
--
DROP TABLE IF EXISTS `job_posting_view`;

CREATE  VIEW `job_posting_view`  AS  select `jp`.`id` AS `id`,`jp`.`job_title` AS `job_title`,`jp`.`job_description` AS `job_description`,`jp`.`skill_keyword` AS `skill_keyword`,`jp`.`location_hiring` AS `location_hiring`,`jp`.`company_hiring` AS `company_hiring`,`jp`.`link_job` AS `link_job`,`jp`.`job_industry` AS `job_industry`,`jp`.`functional_area` AS `functional_area`,`jp`.`job_post_role` AS `job_post_role`,`jp`.`work_experience_from` AS `work_experience_from`,`jp`.`work_experience_to` AS `work_experience_to`,`jp`.`job_salary_from` AS `job_salary_from`,`jp`.`job_salary_to` AS `job_salary_to`,`jp`.`currency_type` AS `currency_type`,`jp`.`job_shift` AS `job_shift`,`jp`.`job_type` AS `job_type`,`jp`.`job_education` AS `job_education`,`jp`.`total_requirenment` AS `total_requirenment`,`jp`.`desire_candidate_profile` AS `desire_candidate_profile`,`jp`.`job_payment` AS `job_payment`,`jp`.`no_of_views` AS `no_of_views`,`jp`.`no_of_like` AS `no_of_like`,`jp`.`no_of_apply` AS `no_of_apply`,`jp`.`posted_by` AS `posted_by`,`jp`.`posted_on` AS `posted_on`,`jp`.`status` AS `status`,`jp`.`currently_hiring_status` AS `currently_hiring_status`,`jp`.`job_highlighted` AS `job_highlighted`,`jp`.`is_deleted` AS `is_deleted`,`jp`.`job_life` AS `job_life`,`jp`.`job_expired_on` AS `job_expired_on`,`im`.`industries_name` AS `industries_name`,`fa`.`functional_name` AS `functional_name`,`jt`.`job_type` AS `job_type_name`,`st`.`shift_type` AS `job_shift_type`,`rm`.`role_name` AS `job_role_name`,`em`.`fullname` AS `posted_by_employee`,`em`.`email` AS `email`,`em`.`profile_pic` AS `profile_pic`,`em`.`company_name` AS `company_name`,`em`.`profile_pic_approval` AS `profile_pic_approval`,`em`.`company_profile` AS `company_profile`,`em`.`company_website` AS `company_website`,`em`.`company_email` AS `company_email`,`em`.`company_logo` AS `company_logo`,`em`.`company_logo_approval` AS `company_logo_approval`,`em`.`is_deleted` AS `employer_delete`,`em`.`status` AS `employer_status` from ((((((`job_posting` `jp` left join `shift_type` `st` on((`jp`.`job_shift` = `st`.`id`))) left join `industries_master` `im` on((`jp`.`job_industry` = `im`.`id`))) left join `functional_area_master` `fa` on((`jp`.`functional_area` = `fa`.`id`))) left join `role_master` `rm` on((`jp`.`job_post_role` = `rm`.`id`))) left join `employer_master` `em` on((`jp`.`posted_by` = `em`.`id`))) left join `job_type_master` `jt` on((`jp`.`job_type` = `jt`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `js_education_view`
--
DROP TABLE IF EXISTS `js_education_view`;

CREATE  VIEW `js_education_view`  AS  select `jse`.`id` AS `id`,`jse`.`js_id` AS `js_id`,`jse`.`passing_year` AS `passing_year`,`jse`.`qualification_level` AS `qualification_level`,`jse`.`institute` AS `institute`,`jse`.`specialization` AS `specialization`,`jse`.`marks` AS `marks`,`jse`.`is_certificate_X_XII` AS `is_certificate_X_XII`,`jse`.`update_on` AS `update_on`,`eqm`.`educational_qualification` AS `educational_qualification`,`jql`.`qualification_name` AS `qualification_name` from ((`jobseeker_education` `jse` left join `educational_qualification_master` `eqm` on((`jse`.`qualification_level` = `eqm`.`id`))) left join `jobseeker_qualification_level` `jql` on((`eqm`.`qualification_level_id` = `jql`.`id`))) group by `jse`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_master`
--
ALTER TABLE `advertisement_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_master`
--
ALTER TABLE `blog_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_master`
--
ALTER TABLE `city_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`,`state_id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `page_url` (`page_url`);

--
-- Indexes for table `company_size_master`
--
ALTER TABLE `company_size_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_type_master`
--
ALTER TABLE `company_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_master`
--
ALTER TABLE `country_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupan_code`
--
ALTER TABLE `coupan_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_plan_employer`
--
ALTER TABLE `credit_plan_employer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_plan_jobseeker`
--
ALTER TABLE `credit_plan_jobseeker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_master`
--
ALTER TABLE `currency_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delete_profile_request`
--
ALTER TABLE `delete_profile_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `educational_qualification_master`
--
ALTER TABLE `educational_qualification_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_name` (`template_name`);

--
-- Indexes for table `employement_type`
--
ALTER TABLE `employement_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer_down_js_resume`
--
ALTER TABLE `employer_down_js_resume`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`,`jobseeker_id`);

--
-- Indexes for table `employer_master`
--
ALTER TABLE `employer_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`,`title`,`country`,`city`,`company_type`,`company_size`,`industry`,`register_date`,`plan_expired`);

--
-- Indexes for table `employer_viewed_js_contact`
--
ALTER TABLE `employer_viewed_js_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`,`jobseeker_id`);

--
-- Indexes for table `functional_area_master`
--
ALTER TABLE `functional_area_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries_master`
--
ALTER TABLE `industries_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobseeker`
--
ALTER TABLE `jobseeker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_title` (`title`),
  ADD KEY `email` (`email`,`title`,`marital_status`,`city`,`country`,`industry`,`functional_area`,`job_role`,`register_date`,`is_deleted`),
  ADD KEY `annual_salary` (`annual_salary`,`currency_type`);

--
-- Indexes for table `jobseeker_access_employer`
--
ALTER TABLE `jobseeker_access_employer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`,`emp_id`),
  ADD KEY `js_id_2` (`js_id`,`emp_id`),
  ADD KEY `liked_on` (`liked_on`,`followed_on`,`blocked_on`);

--
-- Indexes for table `jobseeker_education`
--
ALTER TABLE `jobseeker_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`);

--
-- Indexes for table `jobseeker_language`
--
ALTER TABLE `jobseeker_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`);

--
-- Indexes for table `jobseeker_qualification_level`
--
ALTER TABLE `jobseeker_qualification_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobseeker_viewed_employer_contact`
--
ALTER TABLE `jobseeker_viewed_employer_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`,`employer_id`,`last_viewed_on`);

--
-- Indexes for table `jobseeker_viewed_jobs`
--
ALTER TABLE `jobseeker_viewed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`,`job_id`),
  ADD KEY `js_id_2` (`js_id`,`job_id`,`update_on`,`liked_on`,`saved_on`);

--
-- Indexes for table `jobseeker_workhistory`
--
ALTER TABLE `jobseeker_workhistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`,`joining_date`,`leaving_date`,`industry`,`functional_area`,`job_role`);

--
-- Indexes for table `job_application_history`
--
ALTER TABLE `job_application_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`,`js_id`),
  ADD KEY `job_id_2` (`job_id`,`js_id`,`applied_on`);

--
-- Indexes for table `job_payment_master`
--
ALTER TABLE `job_payment_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_posting`
--
ALTER TABLE `job_posting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_title` (`job_title`,`location_hiring`,`company_hiring`,`job_industry`,`functional_area`,`job_post_role`,`job_shift`,`job_type`,`no_of_views`,`no_of_like`,`no_of_apply`,`posted_by`,`posted_on`,`job_life`,`job_expired_on`);

--
-- Indexes for table `job_type_master`
--
ALTER TABLE `job_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `key_skill_master`
--
ALTER TABLE `key_skill_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `key_skill_name_2` (`key_skill_name`),
  ADD KEY `key_skill_name_3` (`key_skill_name`);

--
-- Indexes for table `key_skill_master_new`
--
ALTER TABLE `key_skill_master_new`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key_skill_name` (`key_skill_name`),
  ADD KEY `key_skill_name_2` (`key_skill_name`),
  ADD KEY `key_skill_name_3` (`key_skill_name`);

--
-- Indexes for table `marital_status_master`
--
ALTER TABLE `marital_status_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender` (`sender`,`receiver`,`sent_on`);

--
-- Indexes for table `personal_titles_master`
--
ALTER TABLE `personal_titles_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_employer`
--
ALTER TABLE `plan_employer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`,`plan_id`,`expired_on`);

--
-- Indexes for table `plan_jobseeker`
--
ALTER TABLE `plan_jobseeker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `js_id` (`js_id`,`plan_id`,`expired_on`);

--
-- Indexes for table `reply_table`
--
ALTER TABLE `reply_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_master`
--
ALTER TABLE `role_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `functional_area` (`functional_area`);

--
-- Indexes for table `salary_range`
--
ALTER TABLE `salary_range`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `send_bulk_email`
--
ALTER TABLE `send_bulk_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_type`
--
ALTER TABLE `shift_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_config`
--
ALTER TABLE `site_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill_language_master`
--
ALTER TABLE `skill_language_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill_level_master`
--
ALTER TABLE `skill_level_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_templates`
--
ALTER TABLE `sms_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_login_master`
--
ALTER TABLE `social_login_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state_master`
--
ALTER TABLE `state_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `ticket_history_reply`
--
ALTER TABLE `ticket_history_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_id` (`ticket_number`,`user_id`,`user_type`);

--
-- Indexes for table `ticket_table`
--
ALTER TABLE `ticket_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_service`
--
ALTER TABLE `web_service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `advertisement_master`
--
ALTER TABLE `advertisement_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_master`
--
ALTER TABLE `blog_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `city_master`
--
ALTER TABLE `city_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `company_size_master`
--
ALTER TABLE `company_size_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `company_type_master`
--
ALTER TABLE `company_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `country_master`
--
ALTER TABLE `country_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `coupan_code`
--
ALTER TABLE `coupan_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credit_plan_employer`
--
ALTER TABLE `credit_plan_employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `credit_plan_jobseeker`
--
ALTER TABLE `credit_plan_jobseeker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `currency_master`
--
ALTER TABLE `currency_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `delete_profile_request`
--
ALTER TABLE `delete_profile_request`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `educational_qualification_master`
--
ALTER TABLE `educational_qualification_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `employement_type`
--
ALTER TABLE `employement_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employer_down_js_resume`
--
ALTER TABLE `employer_down_js_resume`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employer_master`
--
ALTER TABLE `employer_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employer_viewed_js_contact`
--
ALTER TABLE `employer_viewed_js_contact`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `functional_area_master`
--
ALTER TABLE `functional_area_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `industries_master`
--
ALTER TABLE `industries_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `jobseeker`
--
ALTER TABLE `jobseeker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobseeker_access_employer`
--
ALTER TABLE `jobseeker_access_employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobseeker_education`
--
ALTER TABLE `jobseeker_education`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobseeker_language`
--
ALTER TABLE `jobseeker_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobseeker_qualification_level`
--
ALTER TABLE `jobseeker_qualification_level`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jobseeker_viewed_employer_contact`
--
ALTER TABLE `jobseeker_viewed_employer_contact`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobseeker_viewed_jobs`
--
ALTER TABLE `jobseeker_viewed_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobseeker_workhistory`
--
ALTER TABLE `jobseeker_workhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_application_history`
--
ALTER TABLE `job_application_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_payment_master`
--
ALTER TABLE `job_payment_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `job_posting`
--
ALTER TABLE `job_posting`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_type_master`
--
ALTER TABLE `job_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `key_skill_master`
--
ALTER TABLE `key_skill_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=483;

--
-- AUTO_INCREMENT for table `key_skill_master_new`
--
ALTER TABLE `key_skill_master_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marital_status_master`
--
ALTER TABLE `marital_status_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_titles_master`
--
ALTER TABLE `personal_titles_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `plan_employer`
--
ALTER TABLE `plan_employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plan_jobseeker`
--
ALTER TABLE `plan_jobseeker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reply_table`
--
ALTER TABLE `reply_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_master`
--
ALTER TABLE `role_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1640;

--
-- AUTO_INCREMENT for table `salary_range`
--
ALTER TABLE `salary_range`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `send_bulk_email`
--
ALTER TABLE `send_bulk_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shift_type`
--
ALTER TABLE `shift_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `skill_language_master`
--
ALTER TABLE `skill_language_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `skill_level_master`
--
ALTER TABLE `skill_level_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sms_templates`
--
ALTER TABLE `sms_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `social_login_master`
--
ALTER TABLE `social_login_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state_master`
--
ALTER TABLE `state_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `ticket_history_reply`
--
ALTER TABLE `ticket_history_reply`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_table`
--
ALTER TABLE `ticket_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_service`
--
ALTER TABLE `web_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
COMMIT;
