var is_reload_page=0;
function scroll_to_div(div_id)
{
	$('html, body').animate({
		scrollTop: $('#'+div_id).offset().top -55 }, 'slow');
	/*$('html,body').animate({
		scrollTop: $("#"+div_id).offset().top}
	,'slow');*/
}
function show_comm_mask()
{
	var winW = $(window).width();
	var winH = $(window).height();
	var loaderLeft = (winW / 2) - (36 / 2);
	var loaderTop = (winH / 2) - (36 / 2);
	$('#lightbox-panel-mask').css('height', winH + "px");
	$('#lightbox-panel-mask').fadeTo('slow', 0.2);
	$('#lightbox-panel-mask').show();
	$('#lightbox-panel-loader').css({ 'left': loaderLeft + "px", 'top': loaderTop });
	//$("#lightbox-panel-loader").html(""); 
	$('#lightbox-panel-loader').show();
}
function hide_comm_mask()
{
	$('#lightbox-panel-mask').hide();
	$('#lightbox-panel-loader').hide();
}
function Trim(str)
{
	return str.replace(/\s/g,"");
}
var temp_div_content = new Array();

function settimeout_div(div_id,timout)
{
	timout = typeof timout !== 'undefined' ? timout : 10000;
	setTimeout(function(){ $("#"+div_id).slideUp(); }, timout);
}
function remove_element(element,timout)
{
	timout = typeof timout !== 'undefined' ? timout : 10000;
	setTimeout(function(){ $(element).remove(); }, timout);
}
function check_all()
{
	$(".checkbox_val").prop("checked",$("#all").prop("checked"))
}
function check_uncheck_all()
{
	var total_checked = $('input[name="checkbox_val[]"]:checked').length;
	var total = $('input[name="checkbox_val[]"]').length;
	if(total ==total_checked)
	{
		$("#all").prop("checked",true);
	}
	else
	{
		$("#all").prop("checked",false);	
	}
}

function search_change_limit()
{
	get_ajax_search(1);
	return false;
}
function change_order(coloumn_name,order)
{
	//alert(coloumn_name);
	$("#default_order").val(order);
	$("#default_sort").val(coloumn_name);
	get_ajax_search(1);
}
function change_sort_order(order_col)
{
	if(order_col !='')
	{
		var order_arr = order_col.split('-');
		if(order_arr[0] !='' && order_arr[1] !='')
		{
			change_order(order_arr[0],order_arr[1]);
		}
	}
}
function get_ajax_search(page_number)
{
	var base_url = $("#base_url_ajax").val();
	var page_url = base_url + page_number;
	if(page_number == "" || page_number == 0 ||  base_url =='')
	{
		alert("Some issue arise please refress page.");
		return false;
	}
	curr_page_number = page_number;
	var limit_per_page = $("#limit_per_page").val();
	var search_filed = $("#search_filed").val();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var default_sort = $("#default_sort").val();
	var default_order = $("#default_order").val();
	var status_mode = $("#status_mode").val();
	
	show_comm_mask();
	$.ajax({
	   url: page_url,
	   type: "post",
	   data: {'csrf_job_portal':hash_tocken_id,'is_ajax':1,'search_filed':search_filed,'limit_per_page':limit_per_page,'default_order':default_order,'default_sort':default_sort,'status_mode':status_mode},
	   success:function(data){
			$("#main_content_ajax").html(data);
			hide_comm_mask();
			load_pagination_code();
			scroll_to_div("main_content_ajax");
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			/*$("#hash_tocken_id").val($("#hash_tocken_id_temp").val());*/
			is_reload_page = 0;
	   }
	});
	return false;
}

function update_status(status_update)
{
	if(status_update =='')
	{
		alert('Somthing wrong, Please refress page and try again.')
		return false;
	}
	var total_checked = $('input[name="checkbox_val[]"]:checked').length;
	if(total_checked == 0 || total_checked =='')
	{
		alert("Please select atleast one record to proccess");
		return false;
	}
	if(status_update =='DELETE')
	{
		if(!confirm("Are you sure to delete the record?"))
		{
			return false;
		}
	}
	var selected_val = Array();
	var i=0;
	$('input[name="checkbox_val[]"]:checked').each(function() 
	{
		selected_val[i] = this.value;
		i++;
	});
	var page_number = 1;
	var base_url = $("#base_url_ajax").val();
	var page_url = base_url + page_number;
	if(page_number == "" || page_number == 0 ||  base_url =='')
	{
		alert("Some issue arise please refress page.");
		return false;
	}
	curr_page_number = page_number;
	var limit_per_page = $("#limit_per_page").val();
	var search_filed = $("#search_filed").val();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var default_sort = $("#default_sort").val();
	var default_order = $("#default_order").val();
	var status_mode = $("#status_mode").val();
	
	show_comm_mask();
	$.ajax({
	   url: page_url,
	   type: "post",
	   data: {'csrf_job_portal':hash_tocken_id,'is_ajax':1,'search_filed':search_filed,'limit_per_page':limit_per_page,'default_order':default_order,'default_sort':default_sort,'status_mode':status_mode,'selected_val':selected_val,'status_update':status_update},
	   success:function(data){
			$("#main_content_ajax").html(data);
			hide_comm_mask();
			load_pagination_code();
			scroll_to_div("main_content_ajax");
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			/*$("#hash_tocken_id").val($("#hash_tocken_id_temp").val());*/
	   }
	});
	return false;
}
function update_tocken(tocken)
{
	$(".hash_tocken_id").each(function()
	{
	   $(this).val(tocken);
	})
}
function load_pagination_code()
{	
   $("#ajax_pagin_ul li a").click(function()
   {
		var page_number = $(this).attr("data-ci-pagination-page");
		page_number = typeof page_number !== 'undefined' ? page_number : 0;
		if(page_number == 0)
		{
			return false;
		}
		if(page_number != undefined && page_number !='' && page_number != 0)
		{
			get_ajax_search(page_number);
		}
		return false;
   });
   load_checkbo();
}
function load_checkbo()
{
	$('html').checkBo({checkAllButton:"#all_check",checkAllTarget:".checkbox-row",checkAllTextDefault:"Check All",checkAllTextToggle:"Un-check All"});
}
function tog(v)
{
	return v?'addClass':'removeClass';
}
function back_list()
{
	window.history.back();
}
function edit_data_popup(id)
{
	/* new add*/
	var title = $("#title").val();
	$("#model_title").html('Edit '+title);
	/* new add*/
	//$("#model_title").html('Edit Data');
	$("#mode").val('edit');
	$("#reponse_message").slideUp();
	$.each($('#id_'+id).data(), function(i, v) 
	{
		if($("#"+String(i)).length > 0)
		{
			//alert(v);
			$("#"+String(i)).val(v);
		/*	
			var temp_chnage = $('#'+String(i)).attr('onchange');
			if(temp_chnage !='' && temp_chnage != undefined)
			{
				$( "#"+String(i) ).trigger( "change" );
			}
		*/	
		}		
		if(i =='status')
		{
			$("input[name='"+String(i)+"'][value='"+String(v)+"']").prop('checked', true);
		}
		//alert('"' + i + '":"' + v + '",');
	});
	
	return false;
}
function add_data_popup()
{
	/* new add for popup title*/
	var title = $("#title").val();
	$("#model_title").html('Add '+title);
	/*new add for popup title*/
	//$("#model_title").html('Add Data');
	
	$("#mode").val('add');
	$("#id").val('');
	$("#reponse_message").slideUp();
	// $("#common_insert_update").reset();
	//$("#common_insert_update").find('input:radio').removeAttr('checked');
	document.getElementById('common_insert_update').reset();
	$("#APPROVED").attr("checked","checked");
}

function common_submit_fomr()
{
	var form_data = $('#common_insert_update').serialize();
	//alert(form_data);
	var action = $('#common_insert_update').attr('action');
	if(action !='' && form_data !='')
	{
		form_data = form_data+ "&is_ajax=1";
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   dataType:"json",
		   data: form_data,
		   success:function(data)
		   {
			   	$("#reponse_message").html(data.response);
				$("#reponse_message").slideDown();
				update_tocken(data.tocken);
				hide_comm_mask();
				settimeout_div("reponse_message");
				if(data.status == 'success')
				{
					is_reload_page = 1;
				}
				if($("#mode").val() =='add' && data.status == 'success')
				{
					form_reset();
				}
		   }
		});
	}
	else
	{
		alert("Some issue arise please refress page.");
	}
	return false;
}
function form_reset()
{
	var elemet_not_resest = new Array();
	var i = 0;
	$('.not_reset').each(function() 
	{
		elemet_not_resest[i] = this.value;
		i++;
	});
	document.getElementById('common_insert_update').reset();
	i = 0;
	$('.not_reset').each(function()
	{
		this.value = elemet_not_resest[i];
		i++;
	});
}
function close_popup()
{
	if(is_reload_page == 1)
	{
		get_ajax_search(1);
	}
}

function dropdownChange_mul(currnet_id,disp_on,get_list)
{
	var base_url = $("#base_url").val();
	action = base_url+ 'common_request/get_list';
	var hash_tocken_id = $("#hash_tocken_id").val();
	currnet_val = $("#"+currnet_id).val();
	if(currnet_val !='')
	{
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   dataType:"json",
		   data: {'csrf_job_portal':hash_tocken_id,'get_list':get_list,'currnet_val':currnet_val,'multivar':'multi','tocken_val':1},
		   success:function(data)
		   {
				$("#"+disp_on).html(data.dataStr);
				update_tocken(data.tocken);
				$('#'+disp_on).trigger('chosen:updated');
				hide_comm_mask();
		   }
		});
	}
	else
	{
		$("#"+disp_on).html('<option value="">Select Value</option>');
		$('#'+disp_on).trigger('chosen:updated');
	}
}

function dropdownChange(currnet_id,disp_on,get_list)
{
	var base_url = $("#base_url").val();
	action = base_url+ 'common_request/get_list';
	var hash_tocken_id = $("#hash_tocken_id").val();
	currnet_val = $("#"+currnet_id).val();
	if(currnet_val !='')
	{
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   dataType:"json",
		   data: {'csrf_job_portal':hash_tocken_id,'get_list':get_list,'currnet_val':currnet_val},
		   success:function(data)
		   {
				$("#"+disp_on).html(data.dataStr);
				update_tocken(data.tocken);
				hide_comm_mask();
		   }
		});
	}
	else
	{
		$("#"+disp_on).html('<option value="">Select Value</option>');
	}
}
function chnageadvType()
{
	var adv_type = $('.adv_type:checked').val();
	if(adv_type =='Image')
	{
		$(".image_adv").slideDown();
		$(".google_adv").slideUp();
	}
	else
	{
		$(".google_adv").slideDown();
		$(".image_adv").slideUp();
	}
}

function job_seek_address_val()
{
	if($("#form_address_detail").length > 0)
	{
		$("#form_address_detail").validate({
			submitHandler: function(form) 
			{
				edit_profile('address_detail','save');
				return false;
				//return true;
			}
		});
	}
}
function model_search()
{
	var form_data = $('#form_model_search').serialize();
	var action = $('#form_model_search').attr('action')
	var hash_tocken_id = $("#hash_tocken_id").val();
	form_data = form_data+ "&csrf_job_portal="+hash_tocken_id;	
	show_comm_mask();
	$.ajax({
	   url: action,
	   type: "post",
	   dataType:"json",
	   data: form_data,
	   success:function(data)
	   {
		    $('#myModal_filter').modal('hide');
			update_tocken(data.tocken);
			get_ajax_search(1);
	   }
	});
	return false;
}
function clear_model_filter()
{
	var base_url_admin = $("#base_url_admin").val();
	var action = base_url_admin +'/clear_filter';
	var hash_tocken_id = $("#hash_tocken_id").val();
	show_comm_mask();
	$.ajax({
	   url: action,
	   type: "post",
	   dataType:"json",
	   data: {"csrf_job_portal":hash_tocken_id},
	   success:function(data)
	   {
		   document.getElementById('form_model_search').reset();
		   $(".chosen-select").val('').trigger("chosen:updated");
			update_tocken(data.tocken);
			get_ajax_search(1);
	   }
	});
	return false;
}
/* for payment pop up*/
function display_payment(user_id,user_type)
{
	var base_url_admin = $("#base_url_admin").val();
	var action = base_url_admin +'/plan_list';
	var hash_tocken_id = $("#hash_tocken_id").val();
	show_comm_mask();
	$('#model_title_common').html('Plan Assignment');
	$('#model_body_common').html('please wait...');
	$.ajax({
	   url: action,
	   type: "post",
	   data: {"csrf_job_portal":hash_tocken_id,'user_id':user_id,'user_type':user_type},
	   success:function(data)
	   {
		    $('#model_body_common').html(data);
		    update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			$('#model_title_common').html('Plan Assignment');
			$('#myModal_common').modal('show');
			hide_comm_mask();
	   }
	});
	return false;
}
function update_payment_member()
{
	var plan_id = $("#plan_id").val();
	var payment_note = $("#payment_note").val();
	var hidd_user_id = $("#hidd_user_id").val();
	var hidd_user_type = $("#hidd_user_type").val();
	var payment_mode = $("#payment_mode").val();
	var payment_amount = $("#payment_amount_"+plan_id).val();	
	$('#model_body_common_err').html('');
	$('#model_body_common_err').slideUp();
	
	if(plan_id =='')
	{
		alert('Please Select Plan');
		return false;
	}
	if(payment_amount > 0 )
	{
		if(payment_mode =='')
		{
			alert('Please Select Payment mode');
			return false;
		}
	}
	
	var base_url_admin = $("#base_url_admin").val();
	var action = base_url_admin +'/plan_update';
	var hash_tocken_id = $("#hash_tocken_id").val();
	show_comm_mask();
	$.ajax({
	   url: action,
	   type: "post",
	   dataType:"json",
	   data: {"csrf_job_portal":hash_tocken_id,'plan_id':plan_id,'payment_note':payment_note,'user_id':hidd_user_id,'user_type':hidd_user_type,'payment_method':payment_mode},
	   success:function(data)
	   {
		    update_tocken(data.tocken);
			if(data.status == 'success')
			{
				$('#model_body_common').html(data.message);
				get_ajax_search(1);
			}
			else
			{
				$('#model_body_common_err').html(data.message);
				$('#model_body_common_err').slideDown();
			}
			$('#model_title_common').html('Plan Assignment');
			hide_comm_mask();
	   }
	});
	return false;
}
function close_payment_pop()
{
	$('#model_title_common').html('');
	$('#model_body_common').html('');
	$('#myModal_common').modal('hide');
}
/* for payment pop up*/

/* for magnifi glass zoom*/
function OnhoverMove()
{
	var native_width = 0;
	var native_height = 0;
  	var mouse = {x: 0, y: 0};
  	var magnify;
  	var cur_img;
  	var ui = {
    	magniflier: $('.magniflier')
  	};
	if (ui.magniflier.length)
	{
    	var div = document.createElement('div');
	    div.setAttribute('class', 'glass');
    	ui.glass = $(div);
	    $('body').append(div);
  	}
	var mouseMove = function(e)
	{
    	var $el = $(this);
	    var magnify_offset = cur_img.offset();
	    mouse.x = e.pageX - magnify_offset.left;
    	mouse.y = e.pageY - magnify_offset.top;
	    if(
		     mouse.x < cur_img.width() &&
		     mouse.y < cur_img.height() &&
	      	 mouse.x > 0 &&
      		 mouse.y > 0
	    ){
      		magnify(e);
    	}
    	else {
      		ui.glass.fadeOut(100);
    	}
    	return;
  	};
  	var magnify = function(e){
    var rx = Math.round(mouse.x/cur_img.width()*native_width - ui.glass.width()/2)*-1;
    var ry = Math.round(mouse.y/cur_img.height()*native_height - ui.glass.height()/2)*-1;
    var bg_pos = rx + "px " + ry + "px";
    var glass_left = e.pageX - ui.glass.width() / 2;
    var glass_top  = e.pageY - ui.glass.height() / 2;
    ui.glass.css({
      left: glass_left,
      top: glass_top,
      backgroundPosition: bg_pos
    });
    return;
  };
  $('.magniflier').on('mousemove', function()
  {
    ui.glass.fadeIn(100);
    cur_img = $(this);
    var large_img_loaded = cur_img.data('large-img-loaded');
    var src = cur_img.data('large') || cur_img.attr('src');
    if (src) {
      ui.glass.css({
        'background-image': 'url(' + src + ')',
        'background-repeat': 'no-repeat'
      });
    }
    if (!cur_img.data('native_width')){
        var image_object = new Image();
        image_object.onload = function() {
        native_width = image_object.width;
        native_height = image_object.height;
        cur_img.data('native_width', native_width);
        cur_img.data('native_height', native_height);
        mouseMove.apply(this, arguments);
        ui.glass.on('mousemove', mouseMove);
     };
     image_object.src = src;
        return;
     }
	 else 
	 {
        native_width = cur_img.data('native_width');
        native_height = cur_img.data('native_height');
     }
	mouseMove.apply(this, arguments);
    ui.glass.on('mousemove', mouseMove);
  });
  ui.glass.on('mouseout', function() {
    ui.glass.off('mousemove', mouseMove);
  });
}
/* for magnifi glass zoom*/

function title_based_ongender()
{
	var gender =  $("#gender").val();
	if(gender == 'Male')
	{
		$("#Male > option").each(function()
		{
			if(this.value =='Mr.' || this.value =='Mr')
			{
				this.css('display','none');
			}
			else
			{
				this.css('display','');
			}
		});
	}
	else if(gender == 'Female')
	{
		$("#Male > option").each(function()
		{
			if(this.value =='Ms.' || this.value =='ms' || this.value =='mrs' || this.value =='mrs.' || this.value =="ma'am")
			{
				this.css('display','none');
			}
			else
			{
				this.css('display','');
			}
		});
	}
	else
	{
		$("#Male > option").each(function()
		{
			this.css('display','');
		});
	}	
}

function change_gender(gender,mode)
{
	if(mode == 'add')
	{
		$('#title').val('');
	}
	if(gender=='Male')
	{
		$('.gn_classfemale').css('display','none');
		$('.gn_classmale').css('display','block');
	}
	else if(gender=='Female')
	{
		$('.gn_classfemale').css('display','block');
		$('.gn_classmale').css('display','none');
	}
}

/* new edited. for text box only accept characters*/  
/*generate_textbox in common model use in input_type == 'alpha' condition*/

var alpha = "[ A-Za-z]";
//var numeric = "[0-9]"; 
//var alphanumeric = "[ A-Za-z0-9]"; 
function onKeyValidate(e,charVal)
{
    var keynum;
    var keyChars = /[\x00\x08]/;
    var validChars = new RegExp(charVal);
    if(window.event)
    {
        keynum = e.keyCode;
    }
    else if(e.which)
    {
        keynum = e.which;
    }
    var keychar = String.fromCharCode(keynum);
    if (!validChars.test(keychar) && !keyChars.test(keychar))   {
		alert('Please enter characters only');
        return false
    } else{
        return keychar;
    }
}
/* new edited. for text box only accept characters  */
function get_suggestion_list(list_id,label,type,nstatus)
{
	var base_url = $("#base_url").val();
	var action = base_url+ 'common_request/get_list_select2';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: label,
	  ajax: {
		url: action,
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_new_matrimonial: hash_tocken_id,
			list_id:list_id,
			type:type,
			nstatus:nstatus,
		  };
		},
	  }
	});
}
var temp_value_duplicate ='';
function display_comment(user_id)
{
	var base_url_admin = $("#base_url_admin").val();
	var action = base_url_admin +'/view_comment';
	var hash_tocken_id = $("#hash_tocken_id").val();
	show_comm_mask();
	$('#model_title_common').html('View Comment');
	$('#model_body_common').html('please wait...');
	$.ajax({
	   url: action,
	   type: "post",
	   data: {"csrf_job_portal":hash_tocken_id,'user_id':user_id},
	   success:function(data)
	   {
		    $('#model_body_common').html(data);
		    update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			$('#model_title_common').html('View Comment');
			$('#myModal_common').modal('show');
			hide_comm_mask();
	   }
	});
	return false;
}
function display_add_comment(user_id)
{
	var base_url_admin = $("#base_url_admin").val();
	var action = base_url_admin +'/add_comment';
	var hash_tocken_id = $("#hash_tocken_id").val();
	show_comm_mask();
	$('#model_title_common').html('Add Comment');
	$('#model_body_common').html('please wait...');
	$.ajax({
	   url: action,
	   type: "post",
	   data: {"csrf_job_portal":hash_tocken_id,'user_id':user_id},
	   success:function(data)
	   {
		    $('#model_body_common').html(data);
		    update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			$('#model_title_common').html('Add Comment');
			$('#myModal_common').modal('show');
			hide_comm_mask();
	   }
	});
	return false;
}
function save_comment_member()
{
	CKEDITOR.instances['member_comment'].updateElement();
	var form_data = $('#add_comment_form').serialize();
	var action = $('#add_comment_form').attr('action');
	var hash_tocken_id = $("#hash_tocken_id").val();
	if(action !='' && form_data !='')
	{
		form_data = form_data+ "&csrf_job_portal="+hash_tocken_id;	
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   dataType:"json",
		   data: form_data,
		   success:function(data)
		   {
			   	$("#reponse_message_comm").html(data.response);
				$("#reponse_message_comm").slideDown();
				update_tocken(data.tocken);
				$('#myModal_common').scrollTop(0);
				$("#member_comment").val('');
				CKEDITOR.instances.member_comment.setData('');
				hide_comm_mask();
				settimeout_div("reponse_message_comm");
		   }
		});
	}
	else
	{
		alert("Some issue arise please refress page.");
	}
	return false;
}
function check_duplicate(id_field,check_on)
{
	
	var id = $("#id").val();
	var mode = $("#mode").val();
	var field_value = $("#"+id_field).val();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var page_url = base_url+ 'common_request/check_duplicate';
	if(id_field !='' && check_on !=''&& field_value !='' && mode !='' && temp_value_duplicate != field_value)
	{
		$.ajax({
		   url: page_url,
		   type: "post",
		   dataType:"json",
		   data: {'csrf_job_portal':hash_tocken_id,'id':id,'mode':mode,'field_value':field_value,'field_name':id_field,'check_on':check_on},
		   success:function(data)
		   {
				if(data.status == 'success')
				{
					if($("#" + id_field+'-error').length == 0) 
					{
						$( "#"+ id_field ).after( '<label id="'+id_field+'-error" class="error" for="'+id_field+'"></label>' );
					}
					
					$("#"+id_field+'-error').text('Duplicate value found, please enter another one');
					$("#"+id_field+'-error').show();
					$("#"+id_field).addClass('error');
					
					//document.getElementsByClassName("mr10")[0].disabled = true;
				}
				else
				{
					//document.getElementsByClassName("mr10")[0].disabled = false;
					if($("#" + id_field+'-error').length > 0) 
					{
						$("#"+id_field+'-error').hide();
					}
					$("#"+id_field).removeClass('error');
				}
				update_tocken(data.tocken);
		   }
		});
	}
	return false;
}
