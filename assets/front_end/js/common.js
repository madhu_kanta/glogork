var curr_page_number = 1;
function scroll_to_div_new(div_id)
{
	$('html, body').animate({
		scrollTop: $('#'+div_id).offset().top -100 }, 'slow');
}
function scroll_to_div(div_id,topheig)
{
	if(topheig!='' && topheig!='null' && topheig!='undefined' && topheig!=null && topheig!=undefined)
	{
		var topheigt = '+' + topheig;
	}	
	else
	{
		var topheigt = '';
	}
	if(topheigt!='')
	{
		$('html,body').animate({
			/*scrollTop: $("#"+div_id).offset().top + topheig*/
			scrollTop: $("#"+div_id).offset().top + topheigt}
		,'slow', function() {});
	}
	else
	{
		$('html,body').animate({
			scrollTop: $("#"+div_id).offset().top }
		,'slow');
	}
	/*window.scrollBy(0, 0);*/
}
function show_comm_mask()
{
	var winW = $(window).width();
	var winH = $(window).height();
	var loaderLeft = (winW / 2) - (36 / 2);
	var loaderTop = (winH / 2) - (36 / 2);
	$('#lightbox-panel-mask').css('height', winH + "px");
	$('#lightbox-panel-mask').fadeTo('slow', 0.2);
	$('#lightbox-panel-mask').show();
	$('#lightbox-panel-loader').css({ 'left': loaderLeft + "px", 'top': loaderTop });
	//$("#lightbox-panel-loader").html(""); 
	$('#lightbox-panel-loader').show();
}
function hide_comm_mask()
{
	$('#lightbox-panel-mask').hide();
	$('#lightbox-panel-loader').hide();
}

function settimeout_div(div_id,timout)
{
	timout = typeof timout !== 'undefined' ? timout : 10000;
	setTimeout(function(){ $("#"+div_id).slideUp(); }, timout);
}
function load_pagination_code()
{	
   $("#ajax_pagin_ul li a").click(function()
   {    
		var page_number = $(this).attr("data-ci-pagination-page");
		var url_action = $(this).attr('href');
		page_number = typeof page_number !== 'undefined' ? page_number : 0;
		
		if(page_number == 0)
		{
			return false;
		}
		if(page_number != undefined && page_number !='' && page_number != 0 && url_action !='')
		{   
		    var search_form_id = $('#search_from_id').val();
			if(search_form_id!='' && search_form_id!='null' && search_form_id!='undefined'  && search_form_id!=null && search_form_id!=undefined)
			{
				get_ajax_search(url_action,page_number,search_form_id);
			}
			else
			{
				search_form_id = '';
				get_ajax_search(url_action,page_number,search_form_id);
			}
		}
		return false;
   });
}
var in_progress = 0;
function get_ajax_search(url_action,page_number,search_form_id)
{ 
	//var base_url = $("#base_url_ajax").val();
	if(in_progress == 0)
	{
		var page_url = url_action;
		if(page_number == "" || page_number == 0 ||  page_url =='')
		{
			alert("Some issue arise please refress page.");
			return false;
		}
		curr_page_number = page_number;
		var hash_tocken_id = $("#hash_tocken_id").val();
		if(search_form_id!='' && search_form_id!='null' && search_form_id!='undefined'  && search_form_id!=null && search_form_id!=undefined)
		{
			var data_string = $('#'+search_form_id).serialize();
			var text_search = $('#text_search').val();
			var datastring = data_string+'&is_ajax=1'+'&csrf_job_portal='+hash_tocken_id+'&text_search='+text_search;		
		}
		else
		{
			var datastring ='is_ajax=1'+'&csrf_job_portal='+hash_tocken_id;		
		}
		in_progress = 1;
		show_comm_mask();
		$.ajax({
		   url: page_url,
		   type: "post",
		   data: datastring,
		   success:function(data){
				$("#main_content_ajax").html(data);
				hide_comm_mask();
				load_pagination_code();
				update_tocken($("#hash_tocken_id_temp").val());
				$("#hash_tocken_id_temp").remove();
				scroll_to_div("main_content_ajax",-100);
				in_progress = 0;
		   }
		});
	}
	return false;
}
function update_tocken(tocken)
{
	/*$("#hash_tocken_id").each(function()
	{*/
	   //$(this).val(tocken);
	   $("#hash_tocken_id").val(tocken);
	/*})*/
}
/*function scroll_to_div(div_id)
{
	$('html,body').animate({
		scrollTop: $("#"+div_id).offset().top}
	,'slow');
}*/
function dropdownChange(currnet_id,disp_on,get_list,cb,single_multi)
{  
	
	var base_url = $("#base_url").val();
	if(cb!='' && cb!='undefined' && cb!='null' && cb!=null && cb!=undefined)
	{
		var cbfn = cb;
	}
	else
	{
		var cbfn = "default";
	}
	if(single_multi!='' && single_multi!='undefined' && single_multi!='null' && single_multi!=null && single_multi!=undefined )
	{
		var single_multi = single_multi;
		var tocken_val = 1;
	}
	else
	{
		var single_multi = 'single';
		var tocken_val = 0;
	}
	action = base_url+ 'common_request/get_list';	
	var hash_tocken_id = $("#hash_tocken_id").val();
	var currnet_val = $("#"+currnet_id).val();
	show_comm_mask();
	$.ajax({
	   url: action,
	   type: "post",
	   dataType:"json",
	   data: {'csrf_job_portal':hash_tocken_id,'get_list':get_list,'currnet_val':currnet_val,'multivar':single_multi,'tocken_val':tocken_val},
	   success:function(data)
	   {
		   
			$("#"+disp_on).html(data.dataStr);
			update_tocken(data.tocken);
			hide_comm_mask();
			/*if(get_list=='city_list' || get_list=='role_master' || disp_on=='currentdesignation')
			   {
					$('#'+disp_on).trigger('chosen:updated');
			   }*/
			   if($('#'+disp_on).length && disp_on!='' && disp_on!='undefined' && disp_on!='null' && disp_on!=null && disp_on!=undefined )
			   {
				   $('#'+disp_on).trigger('chosen:updated');
			   }
			  
			if(cbfn!='' && cbfn!='null' && cbfn!='undefined' && cbfn!='default' && cbfn!=undefined && cbfn!=null)
				{
					cbfn();
				}
	   }
	});
}


function varify_mobile()
{
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var url_req = base_url+'my_profile/varify_mobile_send_otp';
	show_comm_mask();
	//$("#varify_mobile_pop_up").show('modal');
	
	$.magnificPopup.open({
		items: {
			src: '#varify_mobile_pop_up'
		},
	 	type: 'inline',
		closeBtnInside: true,
	});
	$(".mfp-content").css('width','450px');
	$(".mfp-content").css('max-width','100%');
	//$.ajax({ url: base_url+'assets/front_end/js/jquery.magnific-popup.min.js', dataType: "script" });
	
	var datastring = 'csrf_job_portal='+hash_tocken_id;
	$.ajax({
		url : url_req,
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.tocken);
			if(data.status== 'success')
			{
				$("#error_message_mv").hide();
				$("#success_message_mv").html(data.error_meessage);
				$("#success_message_mv").show();
				$("#resend_link").hide();
				setTimeout(function(){ $("#resend_link").show();},20000);
			}
			else
			{
				$("#success_message_mv").hide();
				$("#error_message_mv").html(data.error_meessage);
				$("#error_message_mv").show();
			}
			hide_comm_mask();
		}
	});
}
function varify_mobile_check()
{
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var url_req = base_url+'my_profile/varify_mobile_check_otp';
	var otp_mobile = $("#otp_mobile").val();
	if(otp_mobile =='')
	{
		$("#error_message_mv").html("Please enter OTP Sent on your mobile.");
		$("#error_message_mv").show();
		$("#success_message_mv").hide();
		return false;
	}
	show_comm_mask();
	$("#error_message_mv").hide();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&otp_mobile='+otp_mobile;
	$.ajax({
		url : url_req,
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.tocken);
			if(data.status== 'success')
			{
				$("#verify_mobile_cont").hide();
				$("#error_message_mv").hide();
				$("#success_message_mv").html(data.error_meessage);
				$("#success_message_mv").show();
				$("#mobile_verifired").html('<label class="alert alert-success" style="width:50px;font-weight:bold;margin-bottom: 0px ;padding: 10px;">Yes</label>');
			}
			else
			{
				$("#success_message_mv").hide();
				$("#error_message_mv").html(data.error_meessage);
				$("#error_message_mv").show();
			}
			hide_comm_mask();
		}
	});
}
// for registraion time user mobile varification
function check_mobile_varify_disp()
{
	setTimeout(function(){
	var mobile_var = 'mobile';
	if($("#mobile").length > 0)
	{
		
	}
	else
	{
		mobile_var = 'mobile_num';
	}
	var mobile = $("#"+mobile_var).val();
	
	var displ_link = 0;
	if(mobile !='')
	{
		if($("#"+mobile_var).hasClass('valid'))
		{
			displ_link = 1;
		}
	}
	if(displ_link == 1)
	{
		$("#varif_mobil_link").show();
		$("#mobile_varifred_message").text('');
		$("#mobile_varifred_message").removeClass('text-success text-danger');
		$("#mobile_varifred_message").hide();
	}
	else
	{
		$("#varif_mobil_link").hide();
	}
	},400);
}
function varify_mobile_reg()
{
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var url_req = base_url+'sign_up/varify_mobile_send_otp';
	show_comm_mask();
	//$("#varify_mobile_pop_up").show('modal');
	
	$.magnificPopup.open({
		items: {
			src: '#varify_mobile_pop_up'
		},
	 	type: 'inline',
		closeBtnInside: true,
	});
	$(".mfp-content").css('width','450px');
	//$.ajax({ url: base_url+'assets/front_end/js/jquery.magnific-popup.min.js', dataType: "script" });
	
	if($("#mobile").length > 0)
	{
		var mobile = $("#mobile").val();	
	}
	else
	{
		var mobile = $("#mobile_num").val()
	}
	var mobile_c_code = $("#mobile_c_code").val();
	$("#verify_mobile_cont").show();
	$("#otp_mobile").val('');
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&mobile='+mobile+'&mobile_code='+mobile_c_code;
	$.ajax({
		url : url_req,
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.tocken);
			if(data.status== 'success')
			{
				$("#error_message_mv").hide();
				$("#success_message_mv").html(data.error_meessage);
				$("#success_message_mv").show();
				$("#resend_link").hide();
				setTimeout(function(){ $("#resend_link").show();},20000);
			}
			else
			{
				$("#success_message_mv").hide();
				$("#error_message_mv").html(data.error_meessage);
				$("#error_message_mv").show();
			}
			hide_comm_mask();
		}
	});
}
function varify_mobile_check_reg()
{
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var url_req = base_url+'sign_up/varify_mobile_check_otp';
	var otp_mobile = $("#otp_mobile").val();
	if(otp_mobile =='')
	{
		$("#error_message_mv").html("Please enter OTP Sent on your mobile.");
		$("#error_message_mv").show();
		$("#success_message_mv").hide();
		return false;
	}
	show_comm_mask();
	$("#error_message_mv").hide();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&otp_mobile='+otp_mobile;
	$.ajax({
		url : url_req,
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.tocken);
			if(data.status== 'success')
			{
				$("#verify_mobile_cont").hide();
				$("#error_message_mv").hide();
				$("#success_message_mv").html(data.error_meessage);
				$("#success_message_mv").show();
				$("#varif_mobil_link").hide();
				$("#mobile_verifired").html('<label class="alert alert-success" style="width:50px; font-weight:bold; margin-bottom: 0px ;padding: 10px;">Yes</label>');
				$("#mobile_varifred_message").show();
				$("#mobile_varifred_message").text('Your Mobile Number Varifired Successfully');
				$("#mobile_varifred_message").removeClass('text-danger');
				$("#mobile_varifred_message").addClass('text-success');
				setTimeout(function(){ 
					$.magnificPopup.close({
						items: {
							src: '#varify_mobile_pop_up'
						},
						type: 'inline',
						closeBtnInside: true,
					});
				},5000);
			}
			else
			{
				$("#success_message_mv").hide();
				$("#error_message_mv").html(data.error_meessage);
				$("#error_message_mv").show();
			}
			hide_comm_mask();
		}
	});
}
// for registraion time user mobile varification

/*var temp_value_duplicate ='';
function check_duplicate(id_field,check_on)
{
	var id = $("#id").val();
	var mode = $("#mode").val();
	var field_value = $("#"+id_field).val();
	if(id_field === "mobile")
	{
		var country_code = $("#mobile_c_code").val();
		var mobile_number = $("#mobile").val();
		field_value = country_code+'-'+mobile_number;
	}
	alert(field_value);
	var hash_tocken_id = $("#hash_tocken_id").val();
	var base_url = $("#base_url").val();
	var page_url = base_url+ 'common_request/check_duplicate';
	if(id_field !='' && check_on !=''&& field_value !='' && mode !='' && temp_value_duplicate != field_value)
	{
		$.ajax({
		   url: page_url,
		   type: "post",
		   dataType:"json",
		   data: {'csrf_job_portal':hash_tocken_id,'mode':mode,'field_value':field_value,'field_name':id_field,'check_on':check_on},
		   success:function(data)
		   {
				if(data.status == 'success')
				{
					alert('success');
					if($("#" + id_field+'-error').length == 0) 
					{
						$( "#"+ id_field ).after( '<span class="help-block form-error" style="color:red;">Duplicate value found, please enter another one</span>' );
					}
					$("#"+id_field+'-error').text('Duplicate value found,please enter another one');
					$("#"+id_field+'-error').show();
					$("#"+id_field).addClass('error');
					
				}
				else
				{
					alert('error');
					if($("#" + id_field+'-error').length > 0) 
					{
						$("#"+id_field+'-error').hide();
					}
					$("#"+id_field).removeClass('error');
				}
				update_tocken(data.tocken);
		   }
		});
	}
	return false;
}*/