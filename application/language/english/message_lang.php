<?php

$lang['welcome_message'] = 'Welcome to all';
/* General variable */
$lang['mandatory_field_msg'] = 'Please fill all mandatory fields.';
$lang['Unauthorized_Access'] = 'Unauthorized access.';
$lang['email_exits'] = 'Email id already exits.';
$lang['mobile_exits'] = 'Mobile number already exits.';
$lang['OR'] = 'OR';
$lang['notavilablevar'] = 'Not Available';
$lang['please_login'] = 'Please login to access this feature';

/* General variable end */



/* All page title */
$lang['forgot_pass_page_title'] = 'Forgot Password';
$lang['add_resume_page_title'] = 'Add Resume';
/* All page title end */

/* All index page title */
$lang['pop_cat_idtit'] = 'Featured Industries';
$lang['brow_cat_idtit'] = 'Browse All Industries';
//$lang['rec_post_indtit'] = 'Recent Posts';
$lang['art_post_indtit'] = 'Articles Posts';
$lang['leg_post_indtit'] = 'Legislazione Posts';
$lang['browse_resume'] = 'Browse Resume';
$lang['rec_job_indtit'] = 'Recent Jobs';
$lang['high_job_indtit'] = 'Highlighted Jobs';
$lang['contac_out_rec_indtit'] = 'Contact with Our Recruiters';
$lang['contac_out_rec_indtit1'] = 'You can directly contact with our Recruiters';
$lang['contac_out_rec_indtit2'] = 'Our Recruiters';
$lang['contac_our_js_indtit1'] = 'You can directly contact with our Registered Jobseekers';
$lang['applied_our_js_indtit1'] = 'You can Directly Contact with Our Applied jobseekers';
$lang['contac_our_js_indtit2'] = 'All Registered Jobseekers';
$lang['contac_our_js_indtit3'] = 'Registered Jobseekers';
$lang['applied_our_js_indtit3'] = 'Applied Jobseekers';
$lang['contac_out_js_indtit2'] = 'Listed Jobseekers';
$lang['recruiters_page_act_since'] = 'Registered Since';
$lang['recruiters_page_act_job'] = 'Active Jobs';
$lang['followers'] = 'Followers';
$lang['contac_out_comp_indtit2'] = 'Listed Companies';
$lang['sub_mes_com'] = 'Subject';
$lang['mes_mes_com'] = 'Message';
$lang['compnay_recruter_det_pg_tit'] = 'Company/Recruiter Details';
$lang['company_det'] = 'Company Details';
$lang['index_page_ban_tit'] = 'We&#x27;ve over <strong>15 000</strong> job offers for you!';
/* All index page title end */

/* Single company detail page */
$lang['company_profil_sin'] = 'Company Profile';
$lang['rec_profil_sin'] = 'About Recruiter';
$lang['our_work_exp_sin'] = 'Recruiter\'s experience & role';
$lang['skils_hire_sin'] = 'Skills that we hire';
/* Single company detail page end */

/* All index page title end */

/* Header */
$lang['new_here'] = 'New Here ?';
$lang['reg_now'] = 'Register Here';
/* Header End */
/* Comman */
$lang['select'] = 'Select';
$lang['select_option'] = 'Select Option';
$lang['country'] = 'Country Name';
$lang['state'] = 'State Name';
$lang['city'] = 'City Name';
$lang['qualification_lvl'] = 'Qualification Level';
$lang['specify'] = 'Specify';
/* Comman end */

/* Verify member text */
$lang['acc_activeted'] = 'Your account has been activated.';
$lang['acc_already_activeted'] = 'Profile is already activated.';
$lang['error_in_activation'] = 'Error in activation.';
/* Verify member text */

/* Facebook  google plus */
$lang['fb_login_lbl'] = 'Log in using Facebook';
$lang['gplus_login_lbl'] = 'Log in using Google Plus';
$lang['fb_signup_lbl'] = 'Sign up using Facebook';
$lang['gplus_signup_lbl'] = 'Sign up  using Google Plus';
$lang['facebook_login_error'] = 'You haven\'t signup with Facebook, Please login with your email.';
$lang['facebook_already_registred'] = 'You are already registered with us, Please sign in with facebook.';
$lang['gplus_already_registred'] = 'You are already registered with us, Please sign in with google plus.';
$lang['gplus_error'] = 'There is a problam while getting details, please try after later.';
/* Facebook  google plus */

/* Sign up variable */
$lang['forgot_password_lbl'] = 'Forgot Password ?';
$lang['reg_tab_login'] = 'Login';
$lang['reg_tab_register'] = 'Register';
$lang['reg_lbl_fullname'] = 'Full Name';
$lang['reg_lbl_pass'] = 'Password';
$lang['reg_lbl_c_pass'] = 'Repeat Password';
$lang['reg_lbl_email'] = 'Email Id';
$lang['reg_lbl_remember'] = 'Remember Me';
$lang['reg_submit_lbl'] = 'Register';
$lang['reg_acc_dtl_success_msg'] = 'Account details added successfully.';
$lang['already_reg_with_fb'] = 'You are already registered with us, Please sign in with Facebook.';
$lang['login_register_lbl'] = 'Login / Registration Job seeker';
$lang['read_more'] = 'Read More';
$lang['register_now'] = 'Register now for  ';
$lang['free'] = 'Free';
$lang['see_all_order'] = 'See all your orders';
$lang['save_favorite'] = 'Save your favorites';
$lang['shipping_free'] = ' Shipping is always free';
$lang['fast_checkout'] = 'Fast checkout';
$lang['get_gift'] = 'Get a gift';
$lang['new_customer'] = 'Only new customers';
$lang['holiday_discount'] = ' Holiday discounts up to 30% off';
$lang['get_started'] = ' Get Started';
$lang['i_agree'] = ' I Agree';
$lang['you_are'] = ' You agree to the ';
$lang['agree_msg_lbl'] = ' Set out by this site, including Cookie Use';
$lang['terms_and_condition'] = 'Terms and Conditions';
$lang['by_clicking'] = 'By clicking';
$lang['accept_terms_msg'] = 'Please accept the terms and conditions';
$lang['captcha_msg'] = 'Please confirm that you are not a robot';
$lang['security_text'] = 'What is the sum of';
$lang['security_question'] = 'security question';
$lang['validate_captcha'] = 'You have not given a correct answer to the security question';
/* Sign up variable end */

/* Forgot password variables */
$lang['forgot_pass_page_text'] = 'You can reset your password here';
$lang['forgot_pass_p_h_text'] = 'Enter your email address';
$lang['forgot_pass_btn_lbl'] = 'Reset password';
$lang['forgot_password_success'] = 'Your password reset verification link sent to your email, please check your email.';
$lang['changed_password_success'] = 'Your password changed successufully.';
$lang['forgot_password_error'] = 'Your have entered wrong email address or your account is deleted.';
$lang['reset_pass_fail_js'] = 'The action you are performing is not allowed';
$lang['reset_pass_check_field'] = 'Please fill all the fields correctly.';
$lang['change_js_pass_lbl'] = 'Enter a strong password';
$lang['change_js_pass_p_lbl'] = 'Enter a strong password';
$lang['cnf_pass_reset_js'] = 'Confirm your password';
$lang['change_pass_confirm_err_msg'] = 'Confirm password correctly';
$lang['change_pass_lbl'] = 'Password';
$lang['change_pass_confirm_lbl'] = 'Confirm password';
/* Forgot password variables end */

/* Forgot password variables for employer */
$lang['forgot_pass_page_text_emp'] = 'You can reset your password here';
$lang['forgot_pass_p_h_text_emp'] = 'Enter your email address';
$lang['forgot_pass_btn_lbl_emp'] = 'Reset password';
$lang['forgot_password_success_emp'] = 'Your password reset link has been sent successfully to your email address, please check your email for the link.';
$lang['forgot_password_error_emp'] = 'You have entered wrong Email Address.';
/* Forgot password variables end  for employer  end */

/* reset password variables for employer */
$lang['reset_pass_page_title'] = 'Reset Password';
$lang['cnf_pass_reset'] = 'Confirm your password';
/* reset password variables for employer */
/* header menu variables */
$lang['header_menu_home_txt'] = 'Home';
$lang['header_menu_foemp'] = 'Employers Section';
$lang['header_menu_foemp_login'] = 'Login';
$lang['header_menu_foemp_reg'] = 'Sign up';
/* header menu variables end */

/* footer menu variables */
$lang['footer_menu_about_txt'] = 'About us';
$lang['footer_menu_careers_txt'] = 'Careers';
$lang['footer_menu_term_txt'] = 'Terms of Service';
$lang['footer_menu_p_policy_txt'] = 'Privacy Policy';
$lang['footer_menu_blog_txt'] = 'Our Blog';
$lang['footer_menu_contact_tit'] = 'Contact';
$lang['footer_menu_contact_tit1'] = 'Customer Support';
$lang['search_by_company_foot'] = 'Search jobs by company';
$lang['search_by_jobseeker_foot'] = 'Search Job seekers';
$lang['message_jobseeker_foot'] = 'Message Employer';
$lang['message_employer_foot'] = 'Message Job seekers';
/* footer menu variables end */

/* footer about  */
$lang['footer_about_txt'] = 'About';
$lang['footer_get_started_btn_txt'] = 'GET STARTED';
/* footer about end */

/* Login variables */
$lang['login_lbl_username'] = 'Username';
$lang['login_username_placeholder'] = 'Enter your email ID';
$lang['login_password_placeholder'] = 'Enter your password';
$lang['login_submit_lbl'] = 'Login';
$lang['wrong_login_detail'] = 'Your email or password is incorrect .';
$lang['inactive_login_account'] = 'YOUR ACCOUNT IS UNDER REVIEW. IT WILL BE ACTIVATED WITHIN 24HRS.';
$lang['login_success_msg'] = 'Login Successful';
/* Login variables end */

/* 404 page variables */
$lang['404page_error'] = 'Error 404';
$lang['404page_error_txt'] = 'Page not found';
$lang['404page_error_msg'] = 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.';
$lang['404page_error_btntxt'] = 'Return to home page';
/* 404 page variables end */

/* blog page title */
$lang['all_blog_page_title'] = 'All Blogs';
$lang['all_blog_page_subtitle'] = 'Keep up to date with the latest news';
$lang['right_contact_widget_title'] = 'Got any questions?';
$lang['right_contact_widget_subtitle'] = 'If you are having any questions, please feel free to ask.';
$lang['right_contact_widget_linktitle'] = 'Drop Us a Line';
$lang['blog_right_tab_title'] = 'Recent';
$lang['blog_right_ad_title'] = 'Featured content'; //Advertisement
$lang['blog_right_social_title'] = 'Social Share';
/* blog page title end */

/* contact page variables */
$lang['contact_page_title'] = 'Contact us';
$lang['contact_page_header_title'] = 'Contact';
$lang['contact_page_bcrum_title'] = 'You are here';
$lang['contact_page_bcrum_link'] = 'Home';
$lang['contact_page_bcrum_link1'] = 'Contact';
$lang['contact_page_above_map_tit'] = 'Our Office';
$lang['contact_page_frm_tit'] = 'Contact Form';
$lang['contact_info_box_info'] = 'Morbi eros bibendum lorem ipsum dolor pellentesque pellentesque bibendum.';
$lang['contact_info_box_info_title'] = 'Information';
$lang['contact_social_title'] = 'Social Media';
$lang['error_msg_on_frm_submit'] = 'Please fill all mandatory fields';
$lang['contact_form_field_title'] = 'Name';
$lang['contact_form_field_title1'] = 'Email';
$lang['contact_form_field_title2'] = 'Message';
$lang['contact_form_field_title3'] = 'Contact Mobile';
$lang['contact_form_successmess'] = 'Your query is sent successfully';
/* contact page variables end */

/* Add resume page variables */
$lang['account_ask_msg'] = 'Have an account ?';
$lang['add_resume_page_title_text'] = 'Add Resume and Find The Jobs';
$lang['account_msg'] = 'If you don’t have an account you can create one below by entering your email address. A password will be automatically emailed to you.';
$lang['personal_info_lbl'] = 'Personal information';
$lang['gender_lbl'] = 'Gender';
$lang['gender_lbl_male'] = 'Male';
$lang['gender_lbl_female'] = 'Female';
$lang['m_status_lbl'] = 'Marital Status';
$lang['dob_lbl'] = 'Date of Birth';
$lang['mobile_lbl'] = 'Mobile Number';
$lang['mobile_palce_h_lbl'] = 'Ex.0987654321';
$lang['country_lbl'] = 'Country';
$lang['city_lbl'] = 'City';
$lang['address_lbl'] = 'Address';
$lang['reg_per_dtl_success_msg'] = 'Personal details added successfully.';

$lang['edu_qualification_lbl'] = 'Education Qualification';
$lang['institute_name'] = 'Institute Name';
$lang['qualification_lvl'] = 'Qualification Level';
$lang['specialization'] = 'Specialization';
$lang['specialization_placeholder'] = 'Ex. Arts, Commerce, Software Engg. etc';
$lang['percentage'] = 'Percentage / Grade';
$lang['passing_year'] = 'Passing Year';
$lang['more_edu_btn_lbl'] = 'More Qualifications';
$lang['url(s)'] = 'Social media URL(s)';
$lang['optional_lbl'] = 'Optional';
$lang['add_url'] = 'Add Social media URL';
$lang['add_url_msg'] = 'Optionally provide links to any of your websites or social network profiles.';
$lang['press'] = 'Press';
$lang['another_field'] = 'to add another form field';
$lang['remove_field'] = 'to remove form field';
$lang['class-X/XII'] = 'class-X/XII';
$lang['class-X'] = 'Add Class X';
$lang['class-XII'] = ' Add Class XII';
$lang['reg_edu_dtl_success_msg'] = 'Education details added successfully.';

$lang['work_experiance'] = 'Work Experience';
$lang['company_name'] = 'Company Name';
$lang['type_industry'] = 'Type of Industry';
$lang['functional_area'] = 'Functional Area';
$lang['joining_date'] = 'Joining Date';
$lang['leaving_date'] = 'Leave Date';
$lang['leaving_date_update'] = '(Leave Date Blank If Currently Working)';
$lang['leaving_date_plc_holder'] = 'Specify Leave Date / Leave blank if present';
$lang['job_role'] = 'Current Role';
$lang['notice_period'] = 'Notice Period';
$lang['annual_salary'] = 'Annual Salary';
$lang['current_annual_salary'] = 'Current Annual Salary';
$lang['skill'] = 'Skill';
$lang['achievements'] = 'Achievements';
$lang['achievements_popoer'] = 'Provide few lines about your achievements there';
$lang['more_exp'] = 'More Experience';
$lang['lacs'] = 'Lacs';
$lang['thousand'] = 'Thousands';
$lang['currency_type'] = 'Currency type';
$lang['reg_workexp_dtl_success_msg'] = 'Work experience details added successfully.';

$lang['js_details'] = 'Job seeker detalis';
$lang['home_city'] = 'Home City';
$lang['preferred_city'] = 'Preferred City';
$lang['landline'] = 'Landline';
$lang['pincode'] = 'Pincode';
$lang['website'] = 'Website Name';
$lang['profile_summary'] = 'Profile Summary';
$lang['resume_headline'] = 'Resume Headline';
$lang['total_experience'] = 'Total Experience';
$lang['year'] = 'Year';
$lang['month'] = 'Month';
$lang['desire_job_type'] = 'Desire Job Type';
$lang['employment_type'] = 'Employment Type';
$lang['prefered_shift'] = 'Preferred Shift';
$lang['expected_annual_salary'] = 'Expected Annual Salary';
$lang['reg_js_dtl_success_msg'] = 'Your Profile is Under review and will be approved shortly. you will receive an email  once approved.';

$lang['upload_resume'] = 'Upload Your Resume';
$lang['upload_resume_pro_pic'] = 'Upload Resume & Profile Picture';
$lang['upload_resume_info'] = 'Please Select PDF, WORD Doc, RTF file';
$lang['upload'] = 'Upload';
$lang['imp_notes'] = 'Important Notes';
$lang['upload_size_info'] = 'The maximum file size for upload is <strong>2 MB</strong>';
$lang['upload_img_frmt_info'] = ' Only PDF or Word files are allowed.';
$lang['upload_pro_pic'] = 'Upload Profile Picture';
$lang['upload_pro_img_frmt_info'] = 'Image files (<strong>JPG, GIF, PNG ,JPEG</strong>) are allowed.';
$lang['upload_proc_pic_info'] = 'Image files JPG, PNG ,JPEG, GIF are allowed. ';
$lang['upload_profile_pic_resume_success_msg'] = 'Resume and profile picture uploaded successfully.';
$lang['profile_pic_upload_success_msg'] = 'Profile picture uploded successfully.';
$lang['resume_upload_success_msg'] = 'Resume uploded successfully.';
$lang['please_select_file'] = 'Please select Image';
$lang['register_success'] = 'Resume added successfully. Your registration process has been completed successfully, please verify by clicking on the link sent to your email id';

$lang['social_name'] = 'Social Media Name';
$lang['social_url_facebook'] = 'Facebook';
$lang['social_url_gplus'] = 'Google plus';
$lang['social_url_twitter'] = 'Twitter ';
$lang['social_url_linkedin'] = 'Linkedin ';
$lang['url_note'] = '.  Note : Url start with http://';
$lang['facebook'] = 'Facebook';
$lang['gplus'] = 'Google Plus';
$lang['twitter'] = 'Twitter';
$lang['linkedin'] = 'Linkedin';
$lang['justurl'] = 'url';

$lang['skip'] = 'Skip';
$lang['previous'] = 'Previous';
$lang['save_continue_lbl'] = 'Save and continue';
$lang['preview'] = 'Preview';
$lang['save'] = 'Save';
/* Add resume page variables end */

/* My profile variables */
$lang['myprofile_no_edu_msg'] = 'No education details added.';
$lang['myprofile_no_lang_msg'] = 'No languages details added.';
$lang['myprofile_no_work_msg'] = 'No work details added.';
$lang['myprofile_no_salary_msg'] = 'Intern with no salary pay.';
$lang['myprofile_personal_det_tit'] = 'Personal Details';
$lang['myprofile_other_det_tit'] = 'Other Details';
$lang['myprofile_social_med_tit'] = 'Social Media Links';
$lang['myprofile_educ_det_tit'] = 'Education Details';
$lang['myprofile_work_det_tit'] = 'Work details';
$lang['myprofile_work_det_more'] = 'Add more Work details';
$lang['myprofile_lang_known_tit'] = 'Languages Known Details';
$lang['myprofile_resume_dwn_tit'] = 'Resume download';
$lang['myprofile_resume_dwn_up_tit'] = 'Resume Upload/Download';
$lang['myprofile_pro_lvl_blak_opt'] = 'Proficiency Level in the language';
$lang['myprofile_lang_blak_opt'] = 'Please select language';
$lang['myprofile_lang_max_exceed'] = 'Sorry! Cannot add more languages. Limit exceed for adding languages.';
$lang['myprofile_work_max_exceed'] = 'Sorry! Cannot add more work details. Limit exceed for adding work.';
$lang['myprofile_lang_one_empty'] = 'Please ! fill the already existing languages to add more languages.';
$lang['myprofile_edu_one_empty'] = 'Please ! fill the already existing education details to add more education.';
$lang['myprofile_certi_one_empty'] = 'Please ! fill the already existing ceritificate details to add more ceritificate.';
$lang['myprofile_work_one_empty'] = 'Please ! fill the already existing work details to add more work details.';
$lang['myprofile_lang_already_added'] = 'Already selected.';
$lang['myprofile_edu_max_exceed'] = 'Sorry! Cannot add more education. Limit exceed for adding education.';
$lang['myprofile_certi_max_exceed'] = 'Sorry! Cannot add more certificates. Limit exceed for adding certificates.';
$lang['myprofile_lang_knwn_place'] = 'Language Known.';
$lang['myprofile_lang_val_mes'] = 'Language number.';
$lang['myprofile_education_val_mes'] = 'Education number.';
$lang['myprofile_workdet_val_mes'] = 'Work detail number.';
$lang['js_edit_title'] = 'Edit Your Profile';
/* My profile variables end */

/* Sign up employer var */
$lang['sign_up_emp_1sttabtitle'] = 'Personal Details';
$lang['sign_up_emp_2ndtabtitle'] = 'Company Details';
$lang['sign_up_emp_3rdtabtitle'] = 'Work Experience';
$lang['sign_up_emp_4thtabtitle'] = 'Upload Photo';
$lang['sign_up_emp_5thtabtitle'] = 'Complete';
$lang['sign_up_emp_2ndtabedittitle'] = 'Edit Company Details';
$lang['sign_up_emp_title'] = 'Employer Sign Up ';
$lang['sign_up_emp_edit_title'] = 'Edit Your Profile';
$lang['sign_up_emp_header_title'] = 'Employer Registration';
$lang['sign_up_emp_1stformtitle_head'] = 'Personal Details';
$lang['sign_up_emp_1stformfullname'] = 'Full Name';
$lang['sign_up_emp_basic_detail'] = 'Employer Basic Details';
$lang['sign_up_emp_1stformfullnameplacehold'] = 'Full Name';
$lang['sign_up_emp_1stformfullnamevalmes'] = 'You did not enter your full name';
$lang['sign_up_emp_1stformlastname'] = 'Last Name';
$lang['sign_up_emp_1stformlastnameplacehold'] = 'Last Name';
$lang['sign_up_emp_1stformlastnamevalmes'] = 'You did not enter your last name';
$lang['sign_up_emp_1stformemail'] = 'Email Address';
$lang['sign_up_emp_1stformabout'] = 'About';
$lang['sign_up_emp_1stformaboutphold'] = 'Tell us something about yourself as an employer.';
$lang['sign_up_emp_1stformemailplacehold'] = 'abc@gmail.com';
$lang['sign_up_emp_1stformemailvalmes'] = 'Enter a valid email';
$lang['sign_up_emp_1stformemailcnf'] = 'Confirm Email address';
$lang['sign_up_emp_1stformemailplaceholdcnf'] = 'abc@gmail.com';
$lang['sign_up_emp_1stformemailvalmescnf'] = 'You did not enter confirmation email correctly';
$lang['sign_up_emp_1stformpass'] = 'Password';
$lang['sign_up_emp_1stformpassvalmes'] = 'Enter a strong password';
$lang['sign_up_emp_1stformpasscnf'] = 'Confirm Password';
$lang['sign_up_emp_1stformpassvalmescnf'] = 'Confirm password correctly';
$lang['sign_up_emp_1stformmob'] = 'Mobile Number';
$lang['sign_up_emp_1stformmobcode'] = 'Mobile Code';
$lang['sign_up_emp_1stformtitle'] = 'Title';
$lang['sign_up_emp_1stformmobcodevalmes'] = 'Please select a mobile code';
$lang['sign_up_emp_1stformtitlevalmes'] = 'Please select a title';
$lang['sign_up_emp_1stformmobnumvalmes'] = 'Please provide a valid mobile/cell number';
$lang['sign_up_emp_1stformmobnumplacehold'] = 'Ex.0987654321';
$lang['sign_up_emp_1stformcnt'] = 'Country';
$lang['sign_up_emp_1stformcntvalmes'] = 'Please select your country';
$lang['sign_up_emp_1stformcity'] = 'City';
$lang['sign_up_emp_1stformvalmescity'] = 'Please select your City';
$lang['sign_up_emp_1stformcitydfltopt'] = 'Please select country first';
$lang['sign_up_emp_1stformaddress'] = 'Address';
$lang['sign_up_emp_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['sign_up_emp_1stformaddressvalmes'] = 'Permanent Address';
$lang['sign_up_emp_1stformaddressvalmessup'] = 'Please Provide your address';
$lang['no_user_agent_sent_in_req'] = 'Sorry you are not allowed to access this by administrator. Please contact admin of this site';
$lang['email_of_employer_already_exits'] = 'That email is already registered as an employer.';
$lang['mobile_of_employer_already_exits'] = 'That mobile is already registered as an employer.';
$lang['sign_up_emp_1stformpincode'] = 'Pincode';
$lang['sign_up_emp_1stformpincodevalmes'] = 'Please Provide a valid pincode';
$lang['sign_up_emp_1stformsave'] = 'Save and continue';
$lang['previous_emp'] = 'Previous';
$lang['sign_up_emp_1stformupdatebtn'] = 'Update';
$lang['sign_up_emp_1stformcancellbtn'] = 'Cancel';
$lang['emp_reg_data_not_ins'] = 'It seems that somethig is not right and your data is not submitted.';
$lang['emp_reg_data_ins_succ'] = 'Your account as an employer has been created successfully.';
$lang['emp_reg_data_ins_succ_final'] = 'Your account as an employer has been created successfully. An email has been sent to the email address provided. This mail contains the confirmation link for confirming your email address.';
$lang['sign_up_emp_companydet'] = 'Company Name';
$lang['urlincorect'] = 'The URL you entered is not correct.';
$lang['sign_up_emp_1stformcompnaylogo'] = 'Upload company logo';
$lang['sign_up_emp_1stformcompnaylogoview'] = 'Company Logo';
$lang['sign_up_emp_1stformcompnaylogoval'] = 'You can not upload images larger than 2MB';
$lang['sign_up_emp_1stformcompnaylogoval1'] = 'You can only upload images of jpeg , jpg, gif and png types';
$lang['example_website'] = 'Ex. https://www.google.co.in';
$lang['sign_up_emp_complete'] = 'Congratulations';
$lang['sign_up_emp_complete_mes'] = 'Registration Completed Successfully...';
$lang['sign_up_emp_reume_upload_succ'] = 'Your resume file has been submitted successfully.';
$lang['sign_up_emp_profile_upload_succ'] = 'Your profile image has been submitted successfully.';
/* Sign up employer var end */

/* employer email verfic */
$lang['acc_activeted_emp'] = 'Your account as an employer has been activated. You can login now.';
$lang['acc_already_activeted_emp'] = 'Your profile as an employer is already activated.';
$lang['error_in_activation_emp'] = 'Error in activation.';
/* employer email verfic end */

/* login up employer var */
$lang['login_emp_title'] = 'Employer Sign Up ';
$lang['login_emp_header_title'] = 'Employer login';
$lang['login_js_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['login_emp_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['wrong_login_detail_employer'] = 'Your email or password is incorrect .';
$lang['wrong_login_detail_employer'] = 'Entered email or password is incorrect .';
$lang['inactive_login_account_employer'] = 'YOUR ACCOUNT IS UNDER REVIEW. IT WILL BE ACTIVATED WITHIN 24HRS.';
$lang['login_success_msg_employer'] = 'Login Successful';
$lang['header_menu_foemp_myprofile'] = 'My Profile';
$lang['captcha_msg_employer'] = 'Please provide a valid captcha';
$lang['reset_passtit_employer'] = 'Reset Password';
$lang['forgot_passtit_employer'] = 'Forgot Password';

$lang['reset_passtit_js'] = 'Reset Password';
$lang['forgot_passtit_js'] = 'Forgot Password';

/* login up employer var end */

/* my profile employer var */
$lang['myprofile_emp_personaltitle'] = 'Personal Details';
$lang['myprofileedit_emp_personaltitle'] = 'Edit Personal Details';
$lang['skill_hire_for_tit'] = 'Sectors that i hire for';
$lang['indu_hire_for_lbl'] = 'Industry that i hire for';
$lang['indu_hire_for_place'] = 'Select industries that your hire for';
$lang['fn_hire_for_lbl'] = 'Functional area that i hire for';
$lang['fn_hire_for_place'] = 'Select Functional area that your hire for';
$lang['skill_hire_for_lbl'] = 'Skills for which I hire';
$lang['skill_hire_for_place'] = 'Select Skills for which your hire';
$lang['myprofileedit_emp_cmpnyval'] = 'You did not enter your company name';
$lang['myprofileedit_emp_cmpnytypeval'] = 'Please select your company type.';
$lang['myprofileedit_emp_cmpnysizeval'] = 'Please select your company size.';
$lang['myprofileedit_emp_cmpnyplace'] = 'Define company name eg. XYZ Enterprise Pvt. Ltd.';
$lang['myprofile_emp_cmptypelbl'] = 'Company Type';
$lang['myprofile_emp_cmpsjizelbl'] = 'Company Size';
$lang['myprofile_emp_cmpweblbl'] = 'Company Website';
$lang['myprofile_emp_cmpprolbl'] = 'Company Profile';
$lang['myprofile_emp_industrylbl'] = 'Industry';
$lang['functional_area_empl_lbl'] = 'Functional Area';
$lang['current_role_empl_lbl'] = 'Role In Organization';
$lang['myprofile_emp_industryvalmes'] = 'Please select your industry';
$lang['myprofile_emp_cmptypeval'] = 'Please select if you are a company or a consultant';
$lang['myprofile_emp_cmpprofiltexz'] = 'Provide few lines about your company';
$lang['myprofile_emp_cmptypedefu'] = 'Select Company Type';
$lang['emp_edit_data_not_ins'] = 'It seems that somethig is not right and your data is not submitted.';
$lang['emp_edit_data_up_succ'] = 'Your account has been updated successfully.';
$lang['emp_edit_data_image_succ'] = 'Your profile image has been submitted successfully.';
$lang['emp_edit_data_image_succ_comp'] = 'Complete (success).';
$lang['myprofile_emp_desemptopt'] = 'Please select your designation';
$lang['sign_up_emp_1stformuploadpictit'] = 'Upload Your Picture';
$lang['sign_up_emp_1stformuploadtypemes'] = 'Note:- Please Select JPG, JPEG, PNG, GIF <br/>Max File Size 2MB.';
$lang['emp_edit_data_image_impnot'] = 'Important Notes';
$lang['emp_edit_data_image_impnot1'] = 'The maximum file size for uploads in this demo is <strong>2 MB</strong> .';
$lang['emp_edit_data_image_impnot2'] = 'The maximum file size for uploads in this demo is <strong>2 MB</strong> .';
$lang['emp_edit_data_image_impnot3'] = 'Doc. files (<strong>Pdf, Word</strong>) are Not allowed in this demo.';
$lang['emp_edit_des_mes'] = 'Please deselect the \'select current role\' option to proceed further.';
$lang['emp_edit_des_mes1'] = 'Please select at least one designation to proceed further.';
$lang['emp_edit_fn_mes'] = 'Please deselect the \'select functional area\' option to proceed further.';
$lang['emp_edit_fn_mes1'] = 'Please select at least one functional area to proceed further.';
$lang['emp_edit_in_hire_mes'] = 'Please deselect the \'select type of industry\' option to proceed further.';
$lang['emp_edit_in_hire_mes1'] = 'Please select at least one industry that your hire for to proceed further.';
$lang['emp_edit_fn_hire_mes'] = 'Please deselect the \'select functional area\' option to proceed further.';
$lang['emp_edit_fn_hire_mes1'] = 'Please select at least one functional area that your hire for to proceed further.';
$lang['emp_edit_skill_hire_mes'] = 'Please deselect the \'select skill\' option to proceed further.';
$lang['emp_edit_skill_hire_mes1'] = 'Please select at least one skill that your hire for to proceed further.';
$lang['sign_up_emp_Edit'] = 'Edit';
$lang['change_pass_emp'] = 'Change Password';
$lang['old_pass_emp'] = 'Old password';
$lang['new_pass_emp'] = 'New password';
$lang['invalid_file_format'] = 'Invalid file format';
$lang['emp_edit_title'] = 'Edit Your Profile';
$lang['emp_edit_pro_tab_tit'] = 'Edit Profile';
$lang['emp_pro_tab_tit'] = 'Profile';
$lang['emp_left_acc_set'] = 'Account Setting';
/* my profile employer var end */

/* Empyoler job posting */
$lang['account_verify'] = 'Verify account';
$lang['account_verify_msg'] = 'You are not authorized to post jobs, please verify your account.';
$lang['post_new_job'] = 'Post new job';
$lang['job_title'] = 'Job Title';
$lang['job_description'] = 'Job description';
$lang['job_title_p_holder'] = 'Specify complete word Eg : Senior PHP Developer';
$lang['skill_keyword'] = 'Job skill';
$lang['search_keyword'] = 'Job search keyword';
$lang['work_experience'] = 'Work experience';
$lang['location_hiring'] = 'Location(s) hiring for';
$lang['company_hiring'] = 'Company hiring for ';
$lang['job_industry'] = 'Job industry';
$lang['job_functional_area'] = 'Job functional area';
$lang['job_post_role'] = 'Role hire for';
$lang['link_job'] = 'Link to Job (Company Webiste)';
$lang['post_new_job'] = 'Post new job';
$lang['post_job_success_msg'] = 'Job posted successfully.';
$lang['edit_job_success_msg'] = 'Edit posted job successfully.';
$lang['define_salary'] = 'Define salary';
$lang['company_rule'] = 'As per company rules';
$lang['job_salary'] = 'Job salary';
$lang['character_left'] = 'characters left';
$lang['company_rule'] = 'As per company rules and regulations';
$lang['job_shift'] = 'Job shift';
$lang['to'] = 'To';
$lang['from'] = 'From';
$lang['total_requirenment'] = 'Current vacancy';
$lang['msg_for_add_job_functional_area'] = 'Please add functional area in your profile';
$lang['msg_for_add_job_industry'] = 'Please add industry in your profile';
$lang['msg_for_add_job_skill'] = 'Please add job skill in your profile';
$lang['desire_candidate_profile'] = 'Describe about candidate profile';
$lang['currently_hiring'] = 'Currently hiring';
$lang['edit'] = 'Edit';
$lang['currently_job_status'] = 'Currently job status';
$lang['edit_job_title'] = 'Edit posted job';
$lang['job_payment'] = 'Job payment';

$lang['add_cerificate'] = 'Add certificate';
$lang['cerificate_name'] = 'Certificate name';
$lang['cerificate_desc'] = 'Certificate description';
$lang['sign_up_js_complete'] = 'Congratulations';
$lang['sign_up_js_complete_mes'] = 'Resume added successfully...';


/* Empyoler job posting end */


/* jobseeker edit */
$lang['js_data_edit_fail'] = 'It seems that something is not right and your data is not submitted.';
$lang['js_data_edit_succ'] = 'Your account has been updated successfully.';

/* jobseeker edit end */

/* employer edit */
$lang['section_personal_det'] = 'Personal details';
$lang['section_compant_det'] = 'Company details';
$lang['section_word_det'] = 'Work details';
/* employer edit end */

/* Job search */
$lang['post_new_job_text'] = 'Post a Job, It’s easy!';
$lang['search_by_location'] = 'Search By Location';
$lang['by_skill'] = 'By Skills';
$lang['sort_by'] = 'Sort by';
$lang['last_30_days'] = 'Last 30 days';
$lang['last_15_days'] = 'Last 15 days';
$lang['last_7_days'] = 'Last 7 days';
$lang['last_1_days'] = 'Last 1 days';
$lang['extra_search'] = 'By Industry, Job Type & Salary ';
$lang['industry'] = 'Industry';
$lang['search_company_lbl'] = 'Search by company';
$lang['job_type'] = 'Job type';
$lang['salary'] = 'Salary';
$lang['job_education'] = 'Job education';
$lang['viewd'] = 'Viewed';
$lang['applied'] = 'Applied';
$lang['salary_search'] = 'Select salary range ';
$lang['browse_job_text'] = 'Browse Jobs';
$lang['total_job_count_msg'] = 'Total job found :';
$lang['search_text_placeholder'] = 'Search with job title, skill, job role, for selection Type something and hit enter';
$lang['search_location_placeholder'] = 'Search with location, for selection Type something and hit enter';
$lang['view_job_title'] = 'View job listing';
$lang['similar_jobs'] = 'Similar jobs';
$lang['more_details'] = 'More Details';
$lang['share'] = 'Share';
$lang['share_twitter'] = 'Click to share  on Twitter';
$lang['share_facebook'] = 'Click to share on Facebook';
$lang['share_gplus'] = 'Click to share on Gplus';
$lang['share_linkedin'] = 'Click to share on Linkedin';
$lang['share_pinterest'] = 'Click to share on Pinterest';
$lang['apply'] = 'Apply';
$lang['save_job'] = 'Save job';
$lang['block'] = 'Block';
$lang['job_open_status'] = 'Open';
$lang['job_closed_status'] = 'Closed';
$lang['add_to_highlighted'] = 'Add to highlight';
$lang['job_highlighted'] = 'Highlighted';
$lang['snd_msg'] = ' Send Message';
$lang['send_application'] = 'Send Application';
$lang['like'] = 'Like';
$lang['liked'] = 'Liked';
$lang['saved'] = 'Saved';
$lang['follow'] = 'Follow';
$lang['following'] = 'Following';
$lang['blocked'] = 'Blocked';
$lang['action'] = 'Action';
$lang['fresher'] = 'Fresher';
$lang['salary'] = 'Salary';
$lang['exprience'] = 'Exprience';
$lang['key_skills'] = 'Keyskills';
$lang['desire_c_profile'] = 'Desire candidate profile';
$lang['view_contact_details'] = 'View contact details';
$lang['recruiter_details'] = 'Recruiter Details';
$lang['recruiter_name'] = 'Recruiter Name';
$lang['web_site'] = 'Web site';
$lang['company_name'] = 'Company name';
$lang['mobile'] = 'Mobile';
$lang['mobile'] = 'Mobile';
$lang['overview'] = 'Overview';
$lang['location'] = 'Location';
$lang['job_details'] = 'Job details';
$lang['posetd_on'] = 'Posted on';
$lang['expired_on'] = 'Expired on';
$lang['job_link'] = 'Job link';
$lang['apply_job'] = 'Apply For This Job';
$lang['upload_cv'] = 'Upload your CV (optional)';
$lang['upload_cv_size_lbl'] = 'Max. file size: 5MB ';
$lang['browse'] = 'Max. file size: 5MB ';
$lang['browse'] = 'Max. file size: 5MB ';
$lang['no_file_selected'] = 'No file selected';
$lang['send_application'] = 'Send Application';
$lang['send_msg'] = 'Send message';
$lang['email'] = 'Email';
$lang['for_search_warning'] = 'Please search something here..';
$lang['confirm_apply_job'] = 'Are you sure to apply for this job ?';
$lang['please_login_apply'] = 'Please login for the apply job';
$lang['apply_job_success_msg'] = 'Applcation submitted successfully.';
$lang['apply_job_err_msg'] = 'Something is not right, while applying job';
$lang['already_apply'] = 'Already applied';
/* Job search End */

/* employer reset password */
$lang['reset_pass_fail_emp'] = 'The action you are performing is not allowed';
$lang['reset_pass_suck_emp'] = 'Congratulation ! your password has been reset successfully. You can now go to employer login and login with the new password entered.';
/* employer reset password end */

/* Job seeker action */
$lang['js_like_job'] = 'Action for like job successfully performed';
$lang['js_unlike_job'] = 'Job removed form like list';
$lang['js_save_job'] = 'This job is saved in your save job list';
$lang['js_remove_save_job'] = 'This job is removed from your save job list';
$lang['js_block_emp'] = 'Employer blocked successfully';
$lang['js_unblock_emp'] = 'Employer removed from  block list ';
$lang['js_follow_emp'] = 'Now you are following this employer';
$lang['js_unfollow_emp'] = 'This employer removed from following list';
$lang['js_like_emp'] = 'Action for like employer successfully performed';
$lang['js_unlike_emp'] = 'Employer removed from like list';
$lang['js_action_err_msg'] = 'Something is not right, while performing action';
$lang['send_mesage_success_msg'] = 'Message sent successfully';
$lang['js_activity_title'] = 'Job seeker activity list';
$lang['job_created_on'] = 'Date created';
$lang['job_update_on'] = 'Update on';
$lang['current_status'] = 'Current status';
$lang['remove_save_list'] = 'Remove from save list';
$lang['remove_like_list'] = 'Remove from like list';
$lang['remove_view_list'] = 'Remove from view list';
$lang['confrim_action_unlike_job'] = ' Are you sure you want to remove job from like list ?';
$lang['confrim_action_remove_save_job'] = 'Are you sure you want to remove job from save list ?';
$lang['confrim_action_remove_view_job'] = 'Are you sure you want to remove job from view list ? Note : This job is removed from like and save list';
$lang['warning_delete'] = 'This records will be deleted permanently.';
$lang['Yes'] = 'Yes';
$lang['No'] = 'No';
$lang['delete'] = 'Delete';
$lang['view_detail'] = 'View detail';
$lang['view_job_list_title'] = 'Viewed job listing';
$lang['save_job_list_title'] = 'Saved job listing';
$lang['like_job_list_title'] = 'Liked job listing';
$lang['home_lbl'] = 'Home';
$lang['js_activity_lbl'] = 'Activity list';
$lang['you_are_here'] = 'You are here';
$lang['js_delete_activity'] = 'Records deleted Permanently';
$lang['js_save_job_left_p_lbl'] = 'Saved job list';
$lang['js_view_job_left_p_lbl'] = 'Viewed Job list';
$lang['js_like_job_left_p_lbl'] = 'Like  Job list';
$lang['like_emp_list_title'] = 'Liked employer listing';
$lang['follow_emp_list_title'] = 'Followed employer listing';
$lang['block_emp_list_title'] = 'Block employer listing';
$lang['js_follow_emp_left_p_lbl'] = 'Following Employer list';
$lang['js_block_emp_left_p_lbl'] = 'Blocked Employer list';
$lang['js_like_emp_left_p_lbl'] = 'Liked Employer list';
$lang['emp_name_lbl'] = 'Employer Name';
$lang['emp_update_on'] = 'Update on';
$lang['confrim_action_block_emp'] = ' Are you sure you want to remove employer from block list ?';
$lang['confrim_action_remove_follow_emp'] = ' Are you sure you want to remove employer from following ?';
$lang['confrim_action_remove_like_emp'] = ' Are you sure you want to remove employer from like list ?';
$lang['close'] = 'Close';
$lang['like_job_on'] = 'Liked on';
$lang['save_job_on'] = 'Saved on';
$lang['view_job_on'] = 'Viewed on';
$lang['block_emp_on'] = 'Blocked On';
$lang['follow_emp_on'] = 'Followed On';
$lang['like_emp_on'] = 'Liked On';
$lang['apply_job_list_title'] = 'Applied job listing';
$lang['apply_job_list'] = 'Applied job list';
$lang['applied_on'] = 'Applied on';
$lang['view_detail_company'] = 'View company detail';
$lang['activity_list'] = 'Activity list';
$lang['finish'] = 'Finish';
$lang['add_url_err_msg'] = 'You can not add more then 4 urls';
$lang['remove_url_warning'] = 'Please remove last add url box';
$lang['specify_present_work_date'] = 'Can keep only 1 leave date blank.';
$lang['warning_for_greater_join_date'] = 'Can not specify joining date  greater than leave date.';
$lang['js_activity_title_for_emp'] = 'My profile actitvity listing';
$lang['js_name_lbl'] = 'Job seeker name';
$lang['js_country_lbl'] = 'Country name';
$lang['js_role_lbl'] = 'Role name';
$lang['follow_emp_left_p_lbl'] = 'Followers listing';
$lang['like_emp_left_p_lbl'] = 'Profile liked by';
$lang['activity_list_lbl_for_emp'] = 'My listing';
$lang['js_basic_detail'] = 'Job seeker basic details';
$lang['js_profile_text_lbl'] = 'Profile text';
$lang['js_email_lbl'] = 'Email';
$lang['js_country_lbl'] = 'Country name';
$lang['js_city_lbl'] = 'City name';
$lang['js_address_lbl'] = 'Address';
$lang['js_mobile_lbl'] = 'Mobile';
$lang['js_pincode_lbl'] = 'Pincode';
$lang['js_website_lbl'] = 'Website';
$lang['js_job_lbl'] = 'Job details';
$lang['js_resume_headline_lbl'] = 'Resume headline';
$lang['js_industry_lbl'] = 'Industry name';
$lang['js_functional_name_lbl'] = 'Functional area name';
$lang['js_role_name_lbl'] = 'Role name';
$lang['js_work_exp_lbl'] = 'Total experience';
$lang['js_annual_salary_lbl'] = 'Annual salary';
$lang['js_exp_salary_lbl'] = 'Expected salary';
$lang['like_emp_list_title'] = 'Liked employer listing';
$lang['follow_emp_list_title_for_emp'] = 'Followers listing';
$lang['block_emp_list_title_for_emp'] = 'Blocked by job seeker listing';
$lang['like_emp_list_title_for_emp'] = 'Liked by job seeker listing';
$lang['manage_job_application_title'] = 'Manage job application';
$lang['manage_application_title'] = 'Manage application';
$lang['download_cv_title'] = 'Download CV';
$lang['show_js_details'] = 'Show job seeker details';
$lang['show_job_details'] = 'Show job details';
$lang['posted_on'] = 'Posted On';
$lang['message'] = 'Message';
$lang['browse_cv'] = 'Browse CV';
$lang['download_cv'] = 'Download CV';
$lang['job_application_lbl'] = 'Job applications list';
$lang['job_application_left_p_lbl'] = 'Applied job seeker list';
$lang['search_with_posted_job_lbl'] = 'Search with your posted jobs';
$lang['js_public_profile_title'] = 'Public profile';
$lang['top_recruiters_lbl'] = 'Top Recruiters';
$lang['top_recruiters_lbl'] = 'Top Recruiters';
$lang['contact_top_recruiters_lbl'] = 'Contact with top Recruiters';
$lang['contact_top_recruiters_di_lbl'] = 'You can directly contact with top Recruiters';
$lang['job_seeker_profile'] = 'Job seeker profile';
$lang['job_seeker_share_profile_about'] = 'Personal information';
$lang['job_seeker_share_profile_education'] = 'Education';
$lang['job_seeker_share_profile_work_exp'] = 'Work experience';
$lang['job_seeker_share_profile_download_resume'] = 'Download resume';
$lang['share_profile_js_info_lbl'] = 'Personal information';
$lang['share_profile_js_name_lbl'] = 'Name';
$lang['share_profile_js_email_lbl'] = 'Email';
$lang['share_profile_js_mobile_lbl'] = 'Mobile';
$lang['share_profile_js_birth_lbl'] = 'Birth Date';
$lang['share_profile_js_address_lbl'] = 'Address';
$lang['share_profile_js_country_lbl'] = 'Country';
$lang['share_profile_js_city_lbl'] = 'City';
$lang['share_profile_js_website_lbl'] = 'Website';
$lang['share_profile_js_pstn_lbl'] = 'Position';
$lang['share_profile_js_work_area_lbl'] = 'Work Area';
$lang['share_profile_js_skill_lbl'] = 'Skill';
$lang['share_profile_ind_lbl'] = 'Industry';
$lang['share_profile_fn_lbl'] = 'Functional area';
$lang['share_profile_role_lbl'] = 'Role name';
$lang['share_profile_desire_lbl'] = 'Desire job type';
$lang['share_profile_emp_type_lbl'] = 'Employment type';
$lang['share_profile_p_s_lbl'] = 'Preferred shift';
$lang['share_profile_exp_salary_lbl'] = 'Expected salary';
$lang['share_profile_profile_summary_lbl'] = 'Profile summary';
$lang['share_profile_certi_lbl'] = 'Certificate details';
$lang['share_profile_certi_name_lbl'] = 'Certificate  name';
$lang['share_profile_certi_d_lbl'] = 'Certificate detail';
$lang['share_profile_passing_lbl'] = 'Passing year';
$lang['share_work_exp_lbl'] = 'Work Experience';
$lang['share_company_lbl'] = 'Company name';
$lang['share_company_lbl'] = 'Company name';
$lang['share_salary_lbl'] = 'Annual salary';
$lang['standard_x_lbl'] = 'Standard X details';
$lang['share_p_institute_name'] = 'Institute name';
$lang['share_p_mark_lbl'] = 'Marks / Percentage ';
$lang['standard_xii_lbl'] = 'Standard XII details ';
$lang['specialization'] = 'Specialization';
$lang['share_profile_lang_lbl'] = 'Language known';
$lang['share_profile_lang_lvl_lbl'] = 'Proficiency level';
$lang['share_profile_lang_read'] = 'Read';
$lang['share_profile_lang_write'] = 'Write';
$lang['share_profile_lang_speak'] = 'Speak';
$lang['emp_public_profile_title'] = 'Public profile';
$lang['emp_profile'] = 'Employer profile';
$lang['emp_share_profile_c_details'] = 'Company information';
$lang['emp_share_profile_sector_for_hire'] = 'Sectors hire by employer ';
/* Job seeker action end */
$lang['indu_hire_for_share_emp_lbl'] = 'Industry for hire';
$lang['fn_area_for_share_emp_lbl'] = 'Functional for hire';
$lang['skill_for_share_emp_lbl'] = 'Skill for hire';

/* browse by industry start */
$lang['browse_by_indus_tit'] = 'Browse by Industries';
$lang['browse_by_indus_subtitle'] = '';
$lang['browse_location_tit'] = 'Browse by Location';
$lang['browse_location_subtitle'] = '';
$lang['browse_location_sort_tit'] = 'Locations sorted alphabetically';
$lang['browse_companies_tit'] = 'Browse Companies';
$lang['browse_companies_subtitle'] = '';
$lang['browse_companies_sort_tit'] = 'Company name sorted alphabetically';
$lang['browse_recruiters_tit'] = 'Browse Recruiters';
$lang['browse_recruiters_subtitle'] = '';
$lang['browse_jobseekers_tit'] = 'Browse Jobseekers';
$lang['browse_jobseekers_subtitle'] = '';
$lang['recent_job_view_lbl'] = 'Recently viewed jobs ';
$lang['more_job_view_lbl'] = 'More Jobs+';
$lang['previous'] = 'Previous';
$lang['next'] = 'Next';
/* browse by industry end */

$lang['delete_req_btn_txt'] = 'Request for profile delete';
$lang['view_detail_lbl'] = 'View details';
$lang['confirm_hightlight_job'] = 'Are you sure make this job as a highlight.';
$lang['add_to_highlight_success'] = 'Job added to highlight';
$lang['add_to_highlight_error'] = 'Something is not write while performing highlight action.';
$lang['suggested_job_l_menu_lbl'] = 'Suggested job.';
$lang['suggested_job_title'] = 'Suggested job listing.';
$lang['add_to_shortlist'] = 'Add to shortlist';
$lang['confirm_add_to_shortlist'] = 'Are you sure, you want to add this application as a shortlist.';
$lang['added_to_shortlist'] = 'Shortlisted';
$lang['add_to_shortlist_success_msg'] = 'Application added to shortlist.';
$lang['remove_from_shortlist'] = 'Remove from shortlist';
$lang['confirm_remove_from_shortlist'] = 'Are you sure, you want to remove this application from shortlist.';
$lang['remove_from_shortlist_success_msg'] = 'Application removed from shortlist.';
$lang['emp_action_err_msg'] = 'Something is not right, while performing action';
$lang['shortlist_application_left_p_lbl'] = 'Shortlisted application';
$lang['shortlist_application_title'] = 'Shortlisted application';
$lang['shortlist_application_lbl'] = 'Shortlisted applications list';
$lang['ask_emp'] = '<strong>Employer ? </strong> Click here';


/* Resume search */
/* $lang['post_new_job_text'] = 'Post a Job, It’s Free!';
  $lang['search_by_location'] = 'Search By Location';
  $lang['by_skill'] = 'By Skills';
  $lang['sort_by'] = 'Sort by';
  $lang['last_30_days'] = 'Last 30 days';
  $lang['last_15_days'] = 'Last 15 days';
  $lang['last_7_days'] = 'Last 7 days';
  $lang['last_1_days'] = 'Last 1 days';
  $lang['extra_search'] = 'By Industry, Job Type & Salary ';
  $lang['industry'] = 'Industry';
  $lang['search_company_lbl']='Search by company';
  $lang['job_type'] = 'Job type';
  $lang['salary'] = 'Salary';
  $lang['job_education'] = 'Job education';
  $lang['viewd'] = 'Viewd';
  $lang['applied'] = 'Applied';
  $lang['salary_search'] = 'Select salary range '; */
$lang['browse_resume_text'] = 'Browse Job seekers';
$lang['search_resume_placeholder'] = 'Search with skills, industry, job role of jobseekers. For selection Type something and hit enter';
/* $lang['total_job_count_msg'] ='Total job found :';
  $lang['search_location_placeholder']='Search with location, for selection Type something and hit enter';
  $lang['view_job_title'] = 'View job listing';
  $lang['similar_jobs']='Similar jobs';
  $lang['more_details'] ='More Details';
  $lang['share'] ='Share';
  $lang['share_twitter'] = 'Click to share  on Twitter';
  $lang['share_facebook'] = 'Click to share on Facebook';
  $lang['share_gplus'] = 'Click to share on Gplus';
  $lang['share_linkedin'] = 'Click to share on Linkedin';
  $lang['share_pinterest'] = 'Click to share on Pinterest';
  $lang['apply'] = 'Apply';
  $lang['save_job'] = 'Save job';
  $lang['block'] = 'Block';
  $lang['job_open_status'] = 'Open';
  $lang['job_closed_status'] = 'Closed';
  $lang['add_to_highlighted'] = 'Add to highlight';
  $lang['job_highlighted'] = 'Highlighted';
  $lang['snd_msg'] = ' Send Message';
  $lang['send_application'] = 'Send Application';
  $lang['like'] = 'Like';
  $lang['liked'] = 'Liked';
  $lang['saved'] = 'Saved';
  $lang['follow'] = 'Follow';
  $lang['following'] = 'Following';
  $lang['blocked'] = 'Blocked';
  $lang['action'] = 'Action';
  $lang['fresher'] ='Fresher';
  $lang['salary'] ='Salary';
  $lang['exprience'] ='Exprience';
  $lang['key_skills'] ='Keyskills';
  $lang['desire_c_profile'] = 'Desire candidate profile';
  $lang['view_contact_details'] = 'View contact details';
  $lang['recruiter_details'] = 'Recruiter Details';
  $lang['recruiter_name'] ='Recruiter Name';
  $lang['web_site'] = 'Web site';
  $lang['company_name'] = 'Company name';
  $lang['mobile'] = 'Mobile';
  $lang['mobile'] = 'Mobile';
  $lang['overview'] = 'Overview';
  $lang['location'] = 'Location';
  $lang['job_details'] ='Job details';
  $lang['posetd_on'] = 'Posetd on';
  $lang['job_link'] = 'Job link';
  $lang['apply_job'] = 'Apply For This Job';
  $lang['upload_cv'] ='Upload your CV (optional)';
  $lang['upload_cv_size_lbl'] ='Max. file size: 5MB ';
  $lang['browse'] ='Max. file size: 5MB ';
  $lang['browse'] ='Max. file size: 5MB ';
  $lang['no_file_selected']='No file selected';
  $lang['send_application']='Send Application';
  $lang['send_msg'] = 'Send message';
  $lang['email'] = 'Email';
  $lang['for_search_warning'] = 'Please search something here..';
  $lang['confirm_apply_job'] = 'Are you sure to apply for this job ?';
  $lang['please_login_apply'] = 'Please login for the apply job';
  $lang['apply_job_success_msg'] = 'Applcation submitted successfully.';
  $lang['apply_job_err_msg'] = 'Something is not right, while applying job';
  $lang['already_apply'] = 'Already applied'; */
/* Resume search End */

$lang['job_life'] = 'Job life (Number of days your post remain live)';
$lang['mobile_charecter_limit_info'] = 'Mobile number should be min 10 and max 13 character allowed';
$lang['upload_pro_pic_social_info'] = 'Your profile picture already uploaded from your facebook/gplus account. If you want to change your profile picture then upload from here.';
?>