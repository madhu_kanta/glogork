<?php
$lang['welcome_message'] = 'सभी का स्वागत हैं';
/*General variable*/
$lang['mandatory_field_msg'] = 'कृपया सभी अनिवार्य फ़ील्ड भरें.';
$lang['Unauthorized_Access'] = 'अनाधिकृत उपयोग.';
$lang['email_exits'] = 'ईमेल आईडी पहले से ही मौजूद है.' ;
$lang['mobile_exits'] = 'मोबाइल नंबर पहले से ही मौजूद है.' ;
$lang['OR'] = 'OR' ;
$lang['notavilablevar'] = 'उपलब्ध नहीं है' ;
$lang['please_login']='कृपया इस सुविधा का उपयोग करने के लिए लॉगिन करें';

/*General variable end*/



/*All page title*/
$lang['forgot_pass_page_title'] = 'पासवर्ड भूल गए';
$lang['add_resume_page_title'] = 'रिज्यूमे जोड़ें';
/*All page title end*/

/*All index page title*/
$lang['pop_cat_idtit'] = 'चुनिंदा उद्योग';
$lang['brow_cat_idtit'] = 'सभी उद्योगों को ब्राउज़ करें';
$lang['rec_post_indtit'] = 'हाल के पोस्ट';
$lang['browse_resume'] = 'फिर से शुरू करें';
$lang['rec_job_indtit'] = 'हाल ही में नौकरियां';
$lang['high_job_indtit'] = 'हाईलाइटेड जॉब्स';
$lang['contac_out_rec_indtit'] = 'हमारे रिक्रूटर्स से संपर्क करें';
$lang['contac_out_rec_indtit1'] = 'आप सीधे हमारे रिक्रूटर्स से संपर्क कर सकते हैं';
$lang['contac_out_rec_indtit2'] = 'हमारे रिक्रूटर्स';
$lang['contac_our_js_indtit1'] ='आप सीधे अपने पंजीकृत नौकरी चाहने वालों से संपर्क कर सकते हैं';
$lang['applied_our_js_indtit1'] = 'आप सीधे हमारे लागू नौकरीपेशा लोगों से संपर्क कर सकते हैं';
$lang['contac_our_js_indtit2'] = 'सभी पंजीकृत जॉबसेकर';
$lang['contac_our_js_indtit3'] = 'पंजीकृत जॉबसेकर';
$lang['applied_our_js_indtit3'] = 'एप्लाइड जॉबसेकर्स';
$lang['contac_out_js_indtit2'] = 'लिस्टेड जोबसेकर';
$lang['recruiters_page_act_since'] = 'कब से पंजीकृत है';
$lang['recruiters_page_act_job'] = 'सक्रिय नौकरियां';
$lang['followers'] = 'समर्थक';
$lang['contac_out_comp_indtit2'] = 'सूचीबद्ध कम्पनियां';
$lang['sub_mes_com'] = 'विषय';
$lang['mes_mes_com'] = 'संदेश';
$lang['compnay_recruter_det_pg_tit'] = 'कंपनी / भर्ती विवरण';
$lang['company_det'] = 'संस्थान के विवरण';
$lang['index_page_ban_tit'] = 'We&#x27;ve over <strong>15 000</strong> job offers for you!';
/*All index page title end*/

/* Single company detail page */
$lang['company_profil_sin'] = 'कंपनी प्रोफाइल';
$lang['rec_profil_sin'] = 'रिक्रूटर के बारे में';
$lang['our_work_exp_sin'] = 'रिक्रूटर के अनुभव और भूमिका';
$lang['skils_hire_sin'] = 'कौशल जो हम किराए पर लेते हैं';
/* Single company detail page end */

/*All index page title end*/

/*Header*/
$lang['new_here'] = 'यहाँ नये ?';
$lang['reg_now'] = 'यहां रजिस्टर करें';
/*Header End*/
/*Comman*/
$lang['select'] = 'चुनें';
$lang['select_option'] = 'विकल्प चुनें';
$lang['country'] = 'देश का नाम';
$lang['state'] = 'राज्य का नाम';
$lang['city'] = 'शहर का नाम';
$lang['qualification_lvl'] = 'योग्यता स्तर';
$lang['specify'] = 'उल्लिखित करना';
/*Comman end*/

/*Verify member text*/
$lang['acc_activeted'] ='आपका खाता सक्रिय हो गया है.';
$lang['acc_already_activeted'] ='प्रोफ़ाइल पहले से सक्रिय है.';
$lang['error_in_activation'] ='सक्रियण में त्रुटि.';
/*Verify member text*/

/*Facebook  google plus*/
$lang['fb_login_lbl'] ='फेसबुक का उपयोग कर लॉगिन करें';
$lang['gplus_login_lbl'] ='गूगल प्लस का उपयोग करके लॉगिन करें';
$lang['fb_signup_lbl'] ='फेसबुक का उपयोग कर साइन अप करें';
$lang['gplus_signup_lbl'] ='गूगल प्लस का उपयोग करके साइन अप करें';
$lang['facebook_login_error'] ='आपने फेसबुक के साथ साइन अप नहीं किया है, कृपया अपने ईमेल से लॉगिन करें.';
$lang['facebook_already_registred'] = 'आप पहले से ही हमारे साथ पंजीकृत हैं, कृपया फेसबुक के साथ साइन इन करें.';
$lang['gplus_already_registred'] = 'आप पहले से ही हमारे साथ पंजीकृत हैं, कृपया गूगल प्लस से साइन इन करें.';
$lang['gplus_error'] ='विवरण प्राप्त करते समय एक समस्या है, कृपया बाद में प्रयास करें.';
/*Facebook  google plus */

/*Sign up variable*/
$lang['forgot_password_lbl'] = 'पासवर्ड भूल गए ?';
$lang['reg_tab_login'] = 'लॉग इन करें';
$lang['reg_tab_register'] = 'रजिस्टर';
$lang['reg_lbl_fullname'] = 'पूरा नाम';
$lang['reg_lbl_pass'] = 'पासवर्ड';
$lang['reg_lbl_c_pass'] = 'पासवर्ड दोहराएं';
$lang['reg_lbl_email'] = 'ईमेल आईडी';
$lang['reg_lbl_remember'] = 'मुझे याद रखना';
$lang['reg_submit_lbl'] = 'रजिस्टर';
$lang['reg_acc_dtl_success_msg'] = 'खाता विवरण सफलतापूर्वक जोड़ा गया.';
$lang['already_reg_with_fb'] = 'आप पहले से ही हमारे साथ पंजीकृत हैं, कृपया फेसबुक के साथ साइन इन करें.';
$lang['login_register_lbl'] = 'लॉग इन / पंजीकरण जॉब सीकर';
$lang['read_more'] = 'और पढो';
$lang['register_now'] = 'अभी के लिए रजिस्टर करें';
$lang['free'] = 'मुक्त';
$lang['see_all_order'] = 'अपने सभी आदेश देखें';
$lang['save_favorite'] = 'अपने पसंदीदा को बचाओ';
$lang['shipping_free'] = 'शिपिंग हमेशा मुफ्त है';
$lang['fast_checkout'] ='फास्ट चेकआउट';
$lang['get_gift'] ='एक उपहार प्राप्त करें';
$lang['new_customer'] ='केवल नए ग्राहक';
$lang['holiday_discount'] ='30% तक की छूट';
$lang['get_started'] ='शुरू हो जाओ';
$lang['i_agree'] ='मैं सहमत हूँ';
$lang['you_are'] ='आप इससे सहमत हैं';
$lang['agree_msg_lbl'] ='कुकी उपयोग सहित, इस साइट द्वारा निर्धारित करें';
$lang['terms_and_condition'] ='नियम और शर्तें';
$lang['by_clicking'] ='क्लिक करने से';
$lang['accept_terms_msg'] = 'कृपया नियम और शर्तें स्वीकार करें';
$lang['captcha_msg'] ='कृपया पुष्टि करें कि आप रोबोट नहीं हैं';
$lang['security_text'] = 'की राशि क्या है';
$lang['security_question'] ='सुरक्षा प्रश्न';
$lang['validate_captcha']='आपने सुरक्षा प्रश्न का सही उत्तर नहीं दिया है';
/*Sign up variable end*/

/*Forgot password variables*/
$lang['forgot_pass_page_text'] = 'आप यहां अपना पासवर्ड रीसेट कर सकते हैं';
$lang['forgot_pass_p_h_text'] = 'अपना ईमेल पता दर्ज करें';
$lang['forgot_pass_btn_lbl'] = 'पासवर्ड रीसेट';
$lang['forgot_password_success'] = 'आपके ईमेल पर भेजा गया आपका पासवर्ड रीसेट सत्यापन लिंक, कृपया अपना ईमेल देखें.';
$lang['changed_password_success'] = 'आपका पासवर्ड सफलतापूर्वक बदल गया.';
$lang['forgot_password_error'] = 'आपका गलत ईमेल पता दर्ज किया गया है या आपका खाता हटा दिया गया है.';
$lang['reset_pass_fail_js'] = 'आपके द्वारा की जा रही कार्रवाई की अनुमति नहीं है';
$lang['reset_pass_check_field'] = 'कृपया सभी फ़ील्ड सही ढंग से भरें.';
$lang['change_js_pass_lbl'] = 'एक मजबूत पासवर्ड दर्ज करें';
$lang['change_js_pass_p_lbl'] = 'एक मजबूत पासवर्ड दर्ज करें';
$lang['cnf_pass_reset_js'] = 'अपने पासवर्ड की पुष्टि करें';
$lang['change_pass_confirm_err_msg'] = 'सही ढंग से पासवर्ड की पुष्टि करें';
$lang['change_pass_lbl'] = 'पासवर्ड';
$lang['change_pass_confirm_lbl'] = 'पासवर्ड की पुष्टि कीजिये';
/*Forgot password variables end*/

/*Forgot password variables for employer */
$lang['forgot_pass_page_text_emp'] = 'आप यहां अपना पासवर्ड रीसेट कर सकते हैं';
$lang['forgot_pass_p_h_text_emp'] = 'अपना ईमेल पता दर्ज करें';
$lang['forgot_pass_btn_lbl_emp'] = 'पासवर्ड रीसेट';
$lang['forgot_password_success_emp'] = 'आपका पासवर्ड रीसेट लिंक आपके ईमेल पते पर सफलतापूर्वक भेज दिया गया है, कृपया लिंक के लिए अपना ईमेल देखें.';
$lang['forgot_password_error_emp'] = 'आपने गलत ईमेल पता दर्ज किया है.';
/*Forgot password variables end  for employer  end */

/*reset password variables for employer */
$lang['reset_pass_page_title'] = 'पासवर्ड रीसेट';
$lang['cnf_pass_reset'] = 'अपने पासवर्ड की पुष्टि करें';
/*reset password variables for employer */
/* header menu variables */
$lang['header_menu_home_txt'] = 'होम';
$lang['header_menu_foemp'] = 'नियोक्ता अनुभाग';
$lang['header_menu_foemp_login'] = 'लॉग इन';
$lang['header_menu_foemp_reg'] = 'साइन अप';
/* header menu variables end */

/* footer menu variables */
$lang['footer_menu_about_txt'] = 'हमारे बारे में';
$lang['footer_menu_careers_txt'] = 'करियर';
$lang['footer_menu_term_txt'] = 'सेवा की शर्तें';
$lang['footer_menu_p_policy_txt'] = 'गोपनीयता नीति';
$lang['footer_menu_blog_txt'] = 'हमारा ब्लॉग';
$lang['footer_menu_contact_tit'] = 'संपर्क';
$lang['footer_menu_contact_tit1'] = 'ग्राहक सहेयता';
$lang['search_by_company_foot'] = 'कंपनी द्वारा नौकरी खोजें';
$lang['search_by_jobseeker_foot'] = 'नौकरी चाहने वालों को खोजें';
$lang['message_jobseeker_foot'] = 'संदेश नियोक्ता';
$lang['message_employer_foot'] = 'संदेश नौकरी चाहने वालों';
/* footer menu variables end */

/* footer about  */
$lang['footer_about_txt'] = 'बारे में';
$lang['footer_get_started_btn_txt'] = 'शुरू हो जाओ';
/* footer about end */

/* Login variables */
$lang['login_lbl_username'] = 'उपयोगकर्ता नाम';
$lang['login_username_placeholder'] = 'अपनी ईमेल आईडी दर्ज करें';
$lang['login_password_placeholder'] = 'अपना पासवर्ड डालें';
$lang['login_submit_lbl'] = 'लॉग इन करें';
$lang['wrong_login_detail'] = 'आपका ईमेल या पासवर्ड गलत है.';
$lang['inactive_login_account'] = 'आपका खाता UNDER REVIEW है। यह 24HRS के साथ सक्रिय किया जाएगा.';
$lang['login_success_msg'] = 'सफलतापूर्ण प्रवेश';
/* Login variables end */

/* 404 page variables */
$lang['404page_error'] = 'Error 404';
$lang['404page_error_txt'] = 'पेज नहीं मिला';
$lang['404page_error_msg'] = 'वह पेज जिसे आप ढूंढ रहे हैं, हटा दिया गया हो सकता है, उसका नाम बदल दिया गया था, या अस्थायी रूप से अनुपलब्ध है.';
$lang['404page_error_btntxt'] = 'होमपेज पर वापस आएं';
/* 404 page variables end */

/* blog page title */
$lang['all_blog_page_title'] = 'सभी ब्लॉग';
$lang['all_blog_page_subtitle'] = 'ताजा खबरों से अपडेट रहें';
$lang['right_contact_widget_title'] = 'कोई प्रश्न मिल गया?';
$lang['right_contact_widget_subtitle'] = 'यदि आप कोई प्रश्न पूछ रहे हैं, तो कृपया बेझिझक पूछें.';
$lang['right_contact_widget_linktitle'] = 'हमारे लिए एक लाइन छोड़ें';
$lang['blog_right_tab_title'] = 'हाल का';
$lang['blog_right_ad_title'] = 'ख़ास पेशकश';//Advertisement
$lang['blog_right_social_title'] = 'सामाजिक हिस्सेदारी';
/* blog page title end */

/* contact page variables */
$lang['contact_page_title'] = 'हमसे संपर्क करें';
$lang['contact_page_header_title'] = 'संपर्क करें';
$lang['contact_page_bcrum_title'] = 'आप यहाँ हैं';
$lang['contact_page_bcrum_link'] = 'होम';
$lang['contact_page_bcrum_link1'] = 'संपर्क करें';
$lang['contact_page_above_map_tit'] = 'हमारा ऑफिस';
$lang['contact_page_frm_tit'] = 'संपर्क करें प्रपत्र';
$lang['contact_info_box_info'] = 'मोरबी एरोस बाइबेंडम लॉरेम इप्सम डोलर पेएलेरसेक पेलेरेसेक बाइबेंडम.';
$lang['contact_info_box_info_title'] = 'जानकारी';
$lang['contact_social_title'] = 'सामाजिक मीडिया';
$lang['error_msg_on_frm_submit'] = 'कृपया सभी अनिवार्य फ़ील्ड भरें';
$lang['contact_form_field_title'] = 'नाम';
$lang['contact_form_field_title1'] = 'ईमेल';
$lang['contact_form_field_title2'] = 'संदेश';
$lang['contact_form_field_title3'] = 'मोबाइल से संपर्क करें';
$lang['contact_form_successmess'] = 'आपकी क्वेरी सफलतापूर्वक भेजी गई है';
/* contact page variables end */

/*Add resume page variables */
$lang['account_ask_msg'] = 'खाता होना ?'; 
$lang['add_resume_page_title_text'] ='रिज्यूमे जोड़ें और नौकरियां खोजें';
$lang['account_msg'] = 'यदि आपके पास कोई खाता नहीं है, तो आप अपना ईमेल पता दर्ज करके नीचे एक बना सकते हैं। एक पासवर्ड स्वचालित रूप से आपको ईमेल कर दिया जाएगा.';
$lang['personal_info_lbl'] = 'व्यक्तिगत जानकारी'; 
$lang['gender_lbl'] = 'जेंडर'; 
$lang['gender_lbl_male'] = 'पुरुष'; 
$lang['gender_lbl_female'] = 'महिला'; 
$lang['m_status_lbl'] = 'वैवाहिक स्थिति'; 
$lang['dob_lbl'] ='जन्म की तारीख';
$lang['mobile_lbl'] ='मोबाइल नंबर';
$lang['mobile_palce_h_lbl'] ='Ex.0987654321';
$lang['country_lbl'] ='देश';
$lang['city_lbl'] ='शहर';
$lang['address_lbl'] ='पता';
$lang['reg_per_dtl_success_msg'] = 'व्यक्तिगत विवरण सफलतापूर्वक जोड़ा गया.';

$lang['edu_qualification_lbl'] = 'शैक्षणिक योग्यता';
$lang['institute_name'] = 'संस्थान का नाम';
$lang['qualification_lvl'] = 'योग्यता स्तर';
$lang['specialization'] = 'विशेषज्ञता';
$lang['specialization_placeholder']='Ex. Arts, Commerce, Software Engg. etc';
$lang['percentage'] = 'प्रतिशत / ग्रेड';
$lang['passing_year'] = 'पासिंग साल';
$lang['more_edu_btn_lbl'] = 'अधिक योग्यता';
$lang['url(s)'] = 'सोशल मीडिया यूआरएल';
$lang['optional_lbl'] = 'ऐच्छिक';
$lang['add_url'] = 'सोशल मीडिया यूआरएल जोड़ें';
$lang['add_url_msg'] = 'वैकल्पिक रूप से अपनी किसी भी वेबसाइट या सोशल नेटवर्क प्रोफाइल के लिंक प्रदान करें.';
$lang['press']='दबाएँ';
$lang['another_field']='किसी अन्य प्रपत्र फ़ील्ड को जोड़ने के लिए';
$lang['remove_field'] ='प्रपत्र फ़ील्ड को निकालने के लिए';
$lang['class-X/XII'] = 'वर्ग एक्स / बारहवीं';
$lang['class-X'] = 'कक्षा एक्स को जोड़ें';
$lang['class-XII'] = 'कक्षा बारहवीं जोड़ें';
$lang['reg_edu_dtl_success_msg'] = 'शिक्षा विवरण सफलतापूर्वक जोड़ा गया.';

$lang['work_experiance'] ='काम का अनुभव';
$lang['company_name'] ='कंपनी का नाम';
$lang['type_industry'] ='उद्योग का प्रकार';
$lang['functional_area'] ='कार्य क्षेत्र';
$lang['joining_date'] ='कार्यग्रहण तिथि';
$lang['leaving_date'] ='तारीख को छोड़ दें';
$lang['leaving_date_update'] ='(यदि वर्तमान में काम कर रहे हैं तो डेट ब्लैंक छोड़ दें)';
$lang['leaving_date_plc_holder'] ='यदि मौजूद हो तो छुट्टी की तारीख / अवकाश निर्दिष्ट करें';
$lang['job_role'] ='वर्तमान भूमिका';
$lang['notice_period'] ='नोटिस की अवधि';
$lang['annual_salary'] ='वार्षिक वेतन';
$lang['current_annual_salary'] ='वर्तमान वार्षिक वेतन';
$lang['skill'] ='स्किल';
$lang['achievements'] ='उपलब्धियां';   
$lang['achievements_popoer'] ='वहाँ अपनी उपलब्धियों के बारे में कुछ पंक्तियाँ प्रदान करें';   
$lang['more_exp'] ='अधिक अनुभव'; 
$lang['lacs'] ='लाख'; 
$lang['thousand'] ='हजार';
$lang['currency_type'] ='मुद्रा प्रकार';
$lang['reg_workexp_dtl_success_msg'] = 'कार्य अनुभव विवरण सफलतापूर्वक जोड़े गए.';

$lang['js_details'] = 'नौकरी चाहने वाला विवरण';
$lang['home_city'] = 'गृह शहर';
$lang['preferred_city'] = 'पसंदीदा शहर';
$lang['landline'] = 'लैंडलाइन';
$lang['pincode'] = 'पिन कोड';
$lang['website'] = 'वेबसाइट का नाम';
$lang['profile_summary'] = 'प्रोफ़ाइल सारांश';
$lang['resume_headline'] = 'संक्षिप्त विवरण का शीर्षक';
$lang['total_experience'] = 'कुल अनुभव';
$lang['year'] = 'साल';
$lang['month'] = 'महीना';
$lang['desire_job_type'] = 'वांछित नौकरी प्रकार';
$lang['employment_type'] = 'रोजगार के प्रकार';
$lang['prefered_shift'] = 'पसंदीदा पारी';
$lang['expected_annual_salary'] = 'अपेक्षित वार्षिक वेतन';
$lang['reg_js_dtl_success_msg'] = 'आपका विवरण सफलतापूर्वक जोड़ा गया.';

$lang['upload_resume'] = 'अपना बायो डाटा अपलोड करें';
$lang['upload_resume_pro_pic'] = 'रिज्यूमे और प्रोफाइल पिक्चर अपलोड करें';
$lang['upload_resume_info'] = 'कृपया पीडीएफ, वर्ड डॉक, आरटीएफ फाइल का चयन करें';
$lang['upload'] = 'अपलोड';
$lang['imp_notes'] ='महत्वपूर्ण लेख';
$lang['upload_size_info'] ='अपलोड के लिए अधिकतम फ़ाइल आकार है <strong>2 MB</strong>';
$lang['upload_img_frmt_info'] ='केवल पीडीएफ या वर्ड फाइल की अनुमति है.';
$lang['upload_pro_pic'] = 'प्रोफ़ाइल चित्र अपलोड करें';
$lang['upload_pro_img_frmt_info'] = 'छवि फ़ाइलों (<strong>जेपीजी, जीआईएफ, पीएनजी, जेपीईजी</strong>) की अनुमति है.';
$lang['upload_proc_pic_info'] = 'छवि फ़ाइलें जेपीजी, पीएनजी, जेपीईजी, जीआईएफ की अनुमति है. ';
$lang['upload_profile_pic_resume_success_msg'] = 'पुनः आरंभ और प्रोफ़ाइल चित्र सफलतापूर्वक अपलोड किया गया.';
$lang['profile_pic_upload_success_msg'] = 'प्रोफ़ाइल चित्र सफलतापूर्वक अपलोड़ किया गया.';
$lang['resume_upload_success_msg'] = 'रिज्यूमे सफलतापूर्वक अपलोड किया गया.';
$lang['please_select_file'] = 'कृपया छवि का चयन करें';
$lang['register_success'] = 'रिज्यूमे सफलतापूर्वक जोड़ा गया। आपकी पंजीकरण प्रक्रिया सफलतापूर्वक पूरी हो गई है, कृपया अपनी ईमेल आईडी पर भेजे गए लिंक पर क्लिक करके सत्यापित करें';

$lang['social_name'] = 'सोशल मीडिया का नाम';
$lang['social_url_facebook'] = 'फेसबुक';
$lang['social_url_gplus'] = 'गूगल प्लस';
$lang['social_url_twitter'] = 'ट्विटर ';
$lang['social_url_linkedin'] = 'Linkedin ';
$lang['url_note'] = '.  Note : Url start with http://';
$lang['facebook'] = 'फेसबुक';
$lang['gplus'] = 'गूगल प्लस';
$lang['twitter'] = 'ट्विटर';
$lang['linkedin'] = 'Linkedin';
$lang['justurl'] = 'यूआरएल';

$lang['skip'] = 'छोड़ें';
$lang['previous'] = 'पिछला';
$lang['save_continue_lbl'] = 'सहेजें और जारी रखें';
$lang['preview'] = 'पूर्वावलोकन';
$lang['save'] = 'बचाना';
/*Add resume page variables end*/

/*My profile variables */
$lang['myprofile_no_edu_msg'] = 'कोई शिक्षा विवरण नहीं जोड़ा गया.';
$lang['myprofile_no_lang_msg'] = 'कोई भाषा विवरण नहीं जोड़ा गया.';
$lang['myprofile_no_work_msg'] = 'कोई कार्य विवरण नहीं जोड़ा गया.';
$lang['myprofile_no_salary_msg'] = 'बिना वेतन के इंटर्न.';
$lang['myprofile_personal_det_tit'] = 'व्यक्तिगत विवरण';
$lang['myprofile_other_det_tit'] = 'अन्य जानकारी';
$lang['myprofile_social_med_tit'] = 'सोशल मीडिया लिंक';
$lang['myprofile_educ_det_tit'] = 'शिक्षा विवरण';
$lang['myprofile_work_det_tit'] = 'काम का विवरण';
$lang['myprofile_work_det_more'] = 'अधिक कार्य विवरण जोड़ें';
$lang['myprofile_lang_known_tit'] = 'भाषाएँ जानी जाती हैं';
$lang['myprofile_resume_dwn_tit'] = 'बायोडाटा डाउनलोड  करें';
$lang['myprofile_resume_dwn_up_tit'] = 'बायोडाटा अपलोड / डाउनलोड  करें';
$lang['myprofile_pro_lvl_blak_opt'] = 'भाषा में प्रवीणता स्तर';
$lang['myprofile_lang_blak_opt'] = 'कृपया भाषा का चयन करें';
$lang['myprofile_lang_max_exceed'] = 'माफ़ कीजिये! अधिक भाषाएँ नहीं जोड़ सकते। भाषाएँ जोड़ने के लिए सीमा पार हो गई.';
$lang['myprofile_work_max_exceed'] = 'माफ़ कीजिये! अधिक कार्य विवरण नहीं जोड़ सकते। काम जोड़ने के लिए सीमा से अधिक है.';
$lang['myprofile_lang_one_empty'] = 'कृप्या ! अधिक भाषाओं को जोड़ने के लिए पहले से मौजूद भाषाओं को भरें.';
$lang['myprofile_edu_one_empty'] = 'कृप्या ! अधिक शिक्षा जोड़ने के लिए पहले से मौजूद शिक्षा विवरण भरें.';
$lang['myprofile_certi_one_empty'] = 'कृप्या ! अधिक प्रमाणपत्र जोड़ने के लिए पहले से मौजूद सेरिटिफ़िकेशन विवरण भरें.';
$lang['myprofile_work_one_empty'] = 'कृप्या ! अधिक कार्य विवरण जोड़ने के लिए पहले से मौजूद कार्य विवरण भरें.';
$lang['myprofile_lang_already_added'] = 'पहले से ही चयनित है.';
$lang['myprofile_edu_max_exceed'] = 'माफ़ कीजिये! अधिक शिक्षा नहीं जोड़ सकते। शिक्षा को जोड़ने के लिए सीमा से अधिक है.';
$lang['myprofile_certi_max_exceed'] = 'माफ़ कीजिये! अधिक प्रमाण पत्र नहीं जोड़ सकते। प्रमाण पत्र जोड़ने के लिए सीमा से अधिक है.';
$lang['myprofile_lang_knwn_place'] = 'भाषा ज्ञात.';
$lang['myprofile_lang_val_mes'] = 'भाषा संख्या.';
$lang['myprofile_education_val_mes'] = 'शिक्षा संख्या.';
$lang['myprofile_workdet_val_mes'] = 'कार्य विस्तार संख्या.';
$lang['js_edit_title'] = 'अपनी प्रोफाइल सम्पादित करें';
/*My profile variables end */

/* Sign up employer var */
$lang['sign_up_emp_1sttabtitle'] = 'व्यक्तिगत विवरण';
$lang['sign_up_emp_2ndtabtitle'] = 'संस्थान के विवरण';
$lang['sign_up_emp_3rdtabtitle'] = 'काम का अनुभव';
$lang['sign_up_emp_4thtabtitle'] = 'फोटो अपलोड करें';
$lang['sign_up_emp_5thtabtitle'] = 'पूर्ण';
$lang['sign_up_emp_2ndtabedittitle'] = 'कंपनी विवरण संपादित करें';
$lang['sign_up_emp_title'] = 'नियोक्ता साइन अप ';
$lang['sign_up_emp_edit_title'] = 'अपनी प्रोफाइल सम्पादित करें';
$lang['sign_up_emp_header_title'] = 'नियोक्ता पंजीकरण';
$lang['sign_up_emp_1stformtitle_head'] = 'व्यक्तिगत विवरण';
$lang['sign_up_emp_1stformfullname'] = 'पूरा नाम';
$lang['sign_up_emp_basic_detail'] = 'नियोक्ता का मूल विवरण';
$lang['sign_up_emp_1stformfullnameplacehold'] = 'पूरा नाम';
$lang['sign_up_emp_1stformfullnamevalmes'] = 'आपने अपना पूरा नाम दर्ज नहीं किया';
$lang['sign_up_emp_1stformlastname'] = 'अंतिम नाम';
$lang['sign_up_emp_1stformlastnameplacehold'] = 'अंतिम नाम';
$lang['sign_up_emp_1stformlastnamevalmes'] = 'आपने अपना अंतिम नाम दर्ज नहीं किया';
$lang['sign_up_emp_1stformemail'] = 'ईमेल पता';
$lang['sign_up_emp_1stformabout'] = 'बारे में';
$lang['sign_up_emp_1stformaboutphold'] = 'एक नियोक्ता के रूप में अपने बारे में कुछ बताएं.';
$lang['sign_up_emp_1stformemailplacehold'] = 'abc@gmail.com';
$lang['sign_up_emp_1stformemailvalmes'] = 'एक वैध ईमेल प्रविष्ट करें';
$lang['sign_up_emp_1stformemailcnf'] = 'ईमेल पते की पुष्टि करें';
$lang['sign_up_emp_1stformemailplaceholdcnf'] = 'abc@gmail.com';
$lang['sign_up_emp_1stformemailvalmescnf'] = 'आपने पुष्टिकरण ईमेल सही से दर्ज नहीं किया है';
$lang['sign_up_emp_1stformpass'] = 'पासवर्ड';
$lang['sign_up_emp_1stformpassvalmes'] = 'एक मजबूत पासवर्ड दर्ज करें';
$lang['sign_up_emp_1stformpasscnf'] = 'पासवर्ड की पुष्टि कीजिये';
$lang['sign_up_emp_1stformpassvalmescnf'] = 'सही ढंग से पासवर्ड की पुष्टि करें';
$lang['sign_up_emp_1stformmob'] = 'मोबाइल नंबर';
$lang['sign_up_emp_1stformmobcode'] = 'मोबाइल कोड';
$lang['sign_up_emp_1stformtitle'] = 'शीर्षक';
$lang['sign_up_emp_1stformmobcodevalmes'] = 'Please select a mobile code';
$lang['sign_up_emp_1stformtitlevalmes'] = 'Please select a title';
$lang['sign_up_emp_1stformmobnumvalmes'] = 'Please provide a valid mobile/cell number';
$lang['sign_up_emp_1stformmobnumplacehold'] = 'Ex.0987654321';
$lang['sign_up_emp_1stformcnt'] = 'Country';
$lang['sign_up_emp_1stformcntvalmes'] = 'Please select your country';
$lang['sign_up_emp_1stformcity'] = 'City';
$lang['sign_up_emp_1stformvalmescity'] = 'Please select your City';
$lang['sign_up_emp_1stformcitydfltopt'] = 'Please select country first';
$lang['sign_up_emp_1stformaddress'] = 'Address';
$lang['sign_up_emp_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['sign_up_emp_1stformaddressvalmes'] = 'Permanent Address';
$lang['sign_up_emp_1stformaddressvalmessup'] = 'Please Provide your address';
$lang['no_user_agent_sent_in_req'] = 'Sorry you are not allowed to access this by administrator. Please contact admin of this site';
$lang['email_of_employer_already_exits'] = 'That email is already registered as an employer.';
$lang['mobile_of_employer_already_exits'] = 'That mobile is already registered as an employer.';
$lang['sign_up_emp_1stformpincode'] = 'Pincode';
$lang['sign_up_emp_1stformpincodevalmes'] = 'Please Provide a valid pincode';
$lang['sign_up_emp_1stformsave'] = 'Save and continue';
$lang['previous_emp'] = 'Previous';
$lang['sign_up_emp_1stformupdatebtn'] = 'Update';
$lang['sign_up_emp_1stformcancellbtn'] = 'Cancel';
$lang['emp_reg_data_not_ins'] = 'It seems that somethig is not right and your data is not submitted.';
$lang['emp_reg_data_ins_succ'] = 'Your account as an employer has been created successfully.';
$lang['emp_reg_data_ins_succ_final'] = 'Your account as an employer has been created successfully. An email has been sent to the email address provided. This mail contains the confirmation link for confirming your email address.';
$lang['sign_up_emp_companydet'] = 'Company Name';
$lang['urlincorect'] = 'The URL you entered is not correct.';
$lang['sign_up_emp_1stformcompnaylogo'] = 'Upload company logo';
$lang['sign_up_emp_1stformcompnaylogoview'] = 'Company Logo';
$lang['sign_up_emp_1stformcompnaylogoval'] = 'You can not upload images larger than 2MB';
$lang['sign_up_emp_1stformcompnaylogoval1'] = 'You can only upload images of jpeg , jpg, gif and png types';
$lang['example_website'] = 'Ex. https://www.google.co.in';
$lang['sign_up_emp_complete'] = 'Congratulations';
$lang['sign_up_emp_complete_mes'] = 'Registration Completed Successfully...';
$lang['sign_up_emp_reume_upload_succ'] = 'Your resume file has been submitted successfully.';
$lang['sign_up_emp_profile_upload_succ'] = 'Your profile image has been submitted successfully.';
/*Sign up employer var end */

/*employer email verfic */
$lang['acc_activeted_emp'] ='Your account as an employer has been activated. You can login now.';
$lang['acc_already_activeted_emp'] ='Your profile as an employer is already activated.';
$lang['error_in_activation_emp'] ='Error in activation.';
/*employer email verfic end */

/* login up employer var */
$lang['login_emp_title'] = 'Employer Sign Up ';
$lang['login_emp_header_title'] = 'Employer login';
$lang['login_js_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['login_emp_form_val_mes'] = 'Please fill all the fields correctly.';
$lang['wrong_login_detail_employer'] = 'Your email or password is incorrect .';
$lang['wrong_login_detail_employer'] = 'Entered email or password is incorrect .';
$lang['inactive_login_account_employer'] = 'YOUR ACCOUNT IS UNDER REVIEW. IT WILL BE ACTIVATED WITHIN 24HRS.';
$lang['login_success_msg_employer'] = 'सफलतापूर्ण प्रवेश';
$lang['header_menu_foemp_myprofile'] = 'My Profile';
$lang['captcha_msg_employer'] = 'Please provide a valid captcha';
$lang['reset_passtit_employer'] = 'Reset Password';
$lang['forgot_passtit_employer'] = '
पासवर्ड भूल गए';

$lang['reset_passtit_js'] = 'Reset Password';
$lang['forgot_passtit_js'] = 'पासवर्ड भूल गए';

/* login up employer var end */

/* my profile employer var */
$lang['myprofile_emp_personaltitle'] = 'Personal Details';
$lang['myprofileedit_emp_personaltitle'] = 'Edit Personal Details';
$lang['skill_hire_for_tit'] ='Sectors that i hire for';
$lang['indu_hire_for_lbl'] ='Industry that i hire for';
$lang['indu_hire_for_place'] ='Select industries that your hire for';
$lang['fn_hire_for_lbl'] ='Functional area that i hire for';
$lang['fn_hire_for_place'] ='Select Functional area that your hire for';
$lang['skill_hire_for_lbl'] ='Skills for which I hire';
$lang['skill_hire_for_place'] ='Select Skills for which your hire';
$lang['myprofileedit_emp_cmpnyval'] = 'You did not enter your company name';
$lang['myprofileedit_emp_cmpnytypeval'] = 'Please select your company type.';
$lang['myprofileedit_emp_cmpnysizeval'] = 'Please select your company size.';
$lang['myprofileedit_emp_cmpnyplace'] = 'Define company name eg. XYZ Enterprise Pvt. Ltd.';
$lang['myprofile_emp_cmptypelbl'] = 'Company Type';
$lang['myprofile_emp_cmpsjizelbl'] = 'Company Size';
$lang['myprofile_emp_cmpweblbl'] = 'Company Website';
$lang['myprofile_emp_cmpprolbl'] = 'Company Profile';
$lang['myprofile_emp_industrylbl'] = 'Industry';
$lang['functional_area_empl_lbl'] ='Functional Area';
$lang['current_role_empl_lbl'] ='Role In Organization';
$lang['myprofile_emp_industryvalmes'] = 'Please select your industry';
$lang['myprofile_emp_cmptypeval'] = 'Please select if you are a company or a consultant';
$lang['myprofile_emp_cmpprofiltexz'] = 'Provide few lines about your company';
$lang['myprofile_emp_cmptypedefu'] = 'Select Company Type';
$lang['emp_edit_data_not_ins'] = 'It seems that somethig is not right and your data is not submitted.';
$lang['emp_edit_data_up_succ'] = 'Your account has been updated successfully.';
$lang['emp_edit_data_image_succ'] = 'Your profile image has been submitted successfully.';
$lang['emp_edit_data_image_succ_comp'] = 'Complete (success).';
$lang['myprofile_emp_desemptopt'] = 'Please select your designation';
$lang['sign_up_emp_1stformuploadpictit'] = 'Upload Your Picture';
$lang['sign_up_emp_1stformuploadtypemes'] = 'Note:- Please Select JPG, JPEG, PNG, GIF <br/>Max File Size 2MB.';
$lang['emp_edit_data_image_impnot'] = 'Important Notes';
$lang['emp_edit_data_image_impnot1'] = 'The maximum file size for uploads in this demo is <strong>2 MB</strong> .';
$lang['emp_edit_data_image_impnot2'] = 'The maximum file size for uploads in this demo is <strong>2 MB</strong> .';
$lang['emp_edit_data_image_impnot3'] = 'Doc. files (<strong>Pdf, Word</strong>) are Not allowed in this demo.';
$lang['emp_edit_des_mes'] = 'Please deselect the \'select current role\' option to proceed further.';
$lang['emp_edit_des_mes1'] = 'Please select at least one designation to proceed further.';
$lang['emp_edit_fn_mes'] = 'Please deselect the \'select functional area\' option to proceed further.';
$lang['emp_edit_fn_mes1'] = 'Please select at least one functional area to proceed further.';
$lang['emp_edit_in_hire_mes'] = 'Please deselect the \'select type of industry\' option to proceed further.';
$lang['emp_edit_in_hire_mes1'] = 'Please select at least one industry that your hire for to proceed further.';
$lang['emp_edit_fn_hire_mes'] = 'Please deselect the \'select functional area\' option to proceed further.';
$lang['emp_edit_fn_hire_mes1'] = 'Please select at least one functional area that your hire for to proceed further.';
$lang['emp_edit_skill_hire_mes'] = 'Please deselect the \'select skill\' option to proceed further.';
$lang['emp_edit_skill_hire_mes1'] = 'Please select at least one skill that your hire for to proceed further.';
$lang['sign_up_emp_Edit'] = 'Edit';
$lang['change_pass_emp'] = 'Change Password';
$lang['old_pass_emp'] = 'Old password';
$lang['new_pass_emp'] = 'New password';
$lang['invalid_file_format'] = 'Invalid file format';
$lang['emp_edit_title'] = 'Edit Your Profile';
$lang['emp_edit_pro_tab_tit'] = 'Edit Profile';
$lang['emp_pro_tab_tit'] = 'Profile';
$lang['emp_left_acc_set'] = 'Account Setting';
/* my profile employer var end */

/*Empyoler job posting*/
$lang['account_verify'] = 'Verify account';
$lang['account_verify_msg'] = 'You are not authorized to post jobs, please verify your account.';
$lang['post_new_job'] = 'Post new job';
$lang['job_title'] = 'Job Title';
$lang['job_description'] = 'Job description';
$lang['job_title_p_holder'] = 'Specify complete word Eg : Senior PHP Developer';
$lang['skill_keyword'] = 'Job skill';
$lang['search_keyword'] = 'Job search keyword';
$lang['work_experience']  = 'Work experience';
$lang['location_hiring'] = 'Location(s) hiring for';
$lang['company_hiring'] = 'Company hiring for ';
$lang['job_industry'] = 'Job industry';
$lang['job_functional_area'] = 'Job functional area';
$lang['job_post_role'] = 'Role hire for';
$lang['link_job'] = 'Link to Job (Company Webiste)';
$lang['post_new_job'] = 'Post new job';
$lang['post_job_success_msg'] = 'Job posted successfully.';
$lang['edit_job_success_msg'] = 'Edit posted job successfully.';
$lang['define_salary'] = 'Define salary';
$lang['company_rule'] = 'As per company rules';
$lang['job_salary'] = 'Job salary';
$lang['character_left'] = 'characters left';
$lang['company_rule'] = 'As per company rules and regulations';
$lang['job_shift'] = 'Job shift';
$lang['to'] = 'To';
$lang['from'] = 'From';
$lang['total_requirenment'] = 'Current vacancy';
$lang['msg_for_add_job_functional_area'] = 'Please add functional area in your profile';
$lang['msg_for_add_job_industry'] = 'Please add industry in your profile';
$lang['msg_for_add_job_skill'] = 'Please add job skill in your profile';
$lang['desire_candidate_profile'] = 'Describe about candidate profile';
$lang['currently_hiring']='Currently hiring';
$lang['edit'] = 'Edit';
$lang['currently_job_status'] = 'Currently job status';
$lang['edit_job_title']='Edit posted job';
$lang['job_payment']='Job payment';

$lang['add_cerificate'] = 'Add certificate';
$lang['cerificate_name'] = 'Certificate name';
$lang['cerificate_desc'] = 'Certificate description';
$lang['sign_up_js_complete'] = 'Congratulations';
$lang['sign_up_js_complete_mes'] = 'Resume added successfully...';


/*Empyoler job posting end*/


/* jobseeker edit */
$lang['js_data_edit_fail'] = 'It seems that something is not right and your data is not submitted.';
$lang['js_data_edit_succ'] = 'Your account has been updated successfully.';

/* jobseeker edit end */

/* employer edit */
$lang['section_personal_det'] = 'Personal details';
$lang['section_compant_det'] = 'Company details';
$lang['section_word_det'] = 'Work details';
/* employer edit end */

/*Job search*/
$lang['post_new_job_text'] = 'नौकरी पोस्ट करें, यह आसान है!';
$lang['search_by_location'] = 'स्थान के आधार पर खोजें';
$lang['by_skill'] = 'कौशल द्वारा'; 
$lang['sort_by'] = 'इसके अनुसार क्रमबद्ध करें'; 
$lang['last_30_days'] = 'Last 30 days';
$lang['last_15_days'] = 'Last 15 days';
$lang['last_7_days'] = 'Last 7 days';
$lang['last_1_days'] = 'Last 1 days';
$lang['extra_search'] = 'उद्योग, नौकरी के प्रकार और वेतन द्वारा ';
$lang['industry'] = 'उद्योग';
$lang['search_company_lbl']='कंपनी द्वारा खोजें';
$lang['job_type'] = 'नौकरी का प्रकार';
$lang['salary'] = 'वेतन';
$lang['job_education'] = 'नौकरी की शिक्षा';
$lang['viewd'] = 'देखा गया';
$lang['applied'] = 'आवेदन किया है';
$lang['salary_search'] = 'वेतन सीमा का चयन करें ';
$lang['browse_job_text'] ='नौकरियां ब्राउज़ करें';
$lang['total_job_count_msg'] ='कुल नौकरी मिली :';
$lang['search_text_placeholder']='चयन के लिए नौकरी शीर्षक, कौशल, नौकरी की भूमिका के साथ खोजें, कुछ टाइप करें और हिट दर्ज करें';
$lang['search_location_placeholder']='स्थान के साथ खोजें, चयन के लिए कुछ टाइप करें और हिट दर्ज करें';
$lang['view_job_title'] = 'नौकरी सूची देखें';
$lang['similar_jobs']='इसी तरह की नौकरियां';
$lang['more_details'] ='अधिक जानकारी';
$lang['share'] ='शेयर';
$lang['share_twitter'] = 'ट्विटर पर साझा करने के लिए क्लिक करें';
$lang['share_facebook'] = 'फेसबुक पर साझा करने के लिए क्लिक करें';
$lang['share_gplus'] = 'Gplus पर साझा करने के लिए क्लिक करें';
$lang['share_linkedin'] = 'Linkedin पर साझा करने के लिए क्लिक करें';
$lang['share_pinterest'] = 'Pinterest पर साझा करने के लिए क्लिक करें';
$lang['apply'] = 'लागू करें';
$lang['save_job'] = 'नौकरी बचाओ';
$lang['block'] = 'ब्लॉक';
$lang['job_open_status'] = 'खुला';
$lang['job_closed_status'] = 'बन्द';
$lang['add_to_highlighted'] = 'हाइलाइट करने के लिए जोड़ें';
$lang['job_highlighted'] = 'हाइलाइट';
$lang['snd_msg'] = 'मेसेज भेजें';
$lang['send_application'] = 'आवेदन भेजें';
$lang['like'] = 'पसंद';
$lang['liked'] = 'पसंद किया';
$lang['saved'] = 'बचाया';
$lang['follow'] = 'पालन करें';
$lang['following'] = 'निम्नलिखित';
$lang['blocked'] = 'ब्लॉक';
$lang['action'] = 'कार्य';
$lang['fresher'] ='नवसिखुआ';
$lang['salary'] ='वेतन';
$lang['exprience'] ='अनुभव';
$lang['key_skills'] ='प्रमुख कौशल';
$lang['desire_c_profile'] = 'अभ्यर्थी का प्रोफाइल';
$lang['view_contact_details'] = 'संपर्क विवरण देखें';
$lang['recruiter_details'] = 'भर्ती विवरण';
$lang['recruiter_name'] ='रिक्रूटर का नाम';
$lang['web_site'] = 'वेबसाइट';
$lang['company_name'] = 'कंपनी का नाम';
$lang['mobile'] = 'मोबाइल';
$lang['mobile'] = 'मोबाइल';
$lang['overview'] = 'अवलोकन';
$lang['location'] = 'स्थान';
$lang['job_details'] ='नौकरी विवरण';
$lang['posetd_on'] = 'पर प्रविष्ट किया';
$lang['expired_on'] = 'समाप्ति तिथि';
$lang['job_link'] = 'नौकरी का लिंक';
$lang['apply_job'] = 'इस नौकरी के लिए आवेदन';
$lang['upload_cv'] ='अपना CV (वैकल्पिक) अपलोड करें';
$lang['upload_cv_size_lbl'] ='मैक्स। फाइल का आकार: 5MB ';
$lang['browse'] ='मैक्स। फाइल का आकार: 5MB ';
$lang['browse'] ='मैक्स। फाइल का आकार: 5MB ';
$lang['no_file_selected']='किसी भी फाइल का चयन नहीं';
$lang['send_application']='Send Application';
$lang['send_msg'] = 'Send message';
$lang['email'] = 'Email';
$lang['for_search_warning'] = 'कृपया यहाँ कुछ खोजे..';
$lang['confirm_apply_job'] = 'Are you sure to apply for this job ?';
$lang['please_login_apply'] = 'Please login for the apply job';
$lang['apply_job_success_msg'] = 'Applcation submitted successfully.'; 
$lang['apply_job_err_msg'] = 'Something is not right, while applying job';
$lang['already_apply'] = 'Already applied'; 
/*Job search End*/

/* employer reset password */
$lang['reset_pass_fail_emp'] = 'The action you are performing is not allowed';
$lang['reset_pass_suck_emp'] = 'Congratulation ! your password has been reset successfully. You can now go to employer login and login with the new password entered.';
/* employer reset password end */

/*Job seeker action*/
$lang['js_like_job'] = 'Action for like job successfully performed';
$lang['js_unlike_job'] = 'Job removed form like list';
$lang['js_save_job'] = 'This job is saved in your save job list';
$lang['js_remove_save_job'] = 'This job is removed from your save job list';
$lang['js_block_emp']='Employer blocked successfully';
$lang['js_unblock_emp']='Employer removed from  block list ';
$lang['js_follow_emp']='Now you are following this employer';
$lang['js_unfollow_emp']='This employer removed from following list';
$lang['js_like_emp']='Action for like employer successfully performed';
$lang['js_unlike_emp']='Employer removed from like list';
$lang['js_action_err_msg']='Something is not right, while performing action';
$lang['send_mesage_success_msg'] = 'Message sent successfully';
$lang['js_activity_title'] = 'Job seeker activity list';
$lang['job_created_on'] = 'Date created';
$lang['job_update_on'] = 'Update on';
$lang['current_status'] = 'Current status';
$lang['remove_save_list'] = 'Remove from save list';
$lang['remove_like_list'] = 'Remove from like list';
$lang['remove_view_list'] = 'Remove from view list';
$lang['confrim_action_unlike_job']=' Are you sure you want to remove job from like list ?';
$lang['confrim_action_remove_save_job']='Are you sure you want to remove job from save list ?';
$lang['confrim_action_remove_view_job']='Are you sure you want to remove job from view list ? Note : This job is removed from like and save list';
$lang['warning_delete'] = 'This records will be deleted permanently.';
$lang['Yes'] = 'Yes';
$lang['No'] = 'No';
$lang['delete'] = 'Delete';
$lang['view_detail'] = 'View detail';
$lang['view_job_list_title'] = 'Viewed job listing';
$lang['save_job_list_title'] = 'Saved job listing';
$lang['like_job_list_title'] = 'Liked job listing';
$lang['home_lbl']='Home';
$lang['js_activity_lbl']='Activity list';
$lang['you_are_here'] = 'You are here';
$lang['js_delete_activity'] = 'Records deleted Permanently';
$lang['js_save_job_left_p_lbl'] = 'Saved job list'; 
$lang['js_view_job_left_p_lbl'] = 'Viewed Job list';
$lang['js_like_job_left_p_lbl'] = 'Like  Job list';
$lang['like_emp_list_title'] = 'Liked employer listing';
$lang['follow_emp_list_title'] = 'Followed employer listing';
$lang['block_emp_list_title'] = 'Block employer listing';
$lang['js_follow_emp_left_p_lbl']= 'Following Employer list';
$lang['js_block_emp_left_p_lbl']= 'Blocked Employer list';
$lang['js_like_emp_left_p_lbl']= 'Liked Employer list';
$lang['emp_name_lbl'] = 'Employer Name';
$lang['emp_update_on'] = 'Update on';
$lang['confrim_action_block_emp']=' Are you sure you want to remove employer from block list ?';
$lang['confrim_action_remove_follow_emp']=' Are you sure you want to remove employer from following ?';
$lang['confrim_action_remove_like_emp']=' Are you sure you want to remove employer from like list ?';
$lang['close'] = 'Close';
$lang['like_job_on']= 'Liked on';
$lang['save_job_on']= 'Saved on';
$lang['view_job_on']= 'Viewed on';
$lang['block_emp_on']='Blocked On';
$lang['follow_emp_on']='Followed On';
$lang['like_emp_on']='Liked On';
$lang['apply_job_list_title'] = 'Applied job listing';
$lang['apply_job_list'] = 'Applied job list';
$lang['applied_on']='Applied on';
$lang['view_detail_company'] = 'View company detail';
$lang['activity_list'] = 'Activity list';
$lang['finish']= 'Finish';
$lang['add_url_err_msg'] = 'You can not add more then 4 urls';
$lang['remove_url_warning'] = 'Please remove last add url box';
$lang['specify_present_work_date'] = 'Can keep only 1 leave date blank.';
$lang['warning_for_greater_join_date'] = 'Can not specify joining date  greater than leave date.';
$lang['js_activity_title_for_emp'] = 'My profile actitvity listing';
$lang['js_name_lbl'] = 'Job seeker name';
$lang['js_country_lbl'] = 'Country name';
$lang['js_role_lbl'] = 'Role name';
$lang['follow_emp_left_p_lbl'] = 'Followers listing';
$lang['like_emp_left_p_lbl'] = 'Profile liked by';
$lang['activity_list_lbl_for_emp']  = 'My listing';
$lang['js_basic_detail'] = 'Job seeker basic details';
$lang['js_profile_text_lbl'] = 'Profile text';
$lang['js_email_lbl'] = 'Email';
$lang['js_country_lbl'] = 'Country name';
$lang['js_city_lbl'] = 'City name';
$lang['js_address_lbl'] = 'Address';
$lang['js_mobile_lbl'] = 'Mobile';
$lang['js_pincode_lbl'] = 'Pincode';
$lang['js_website_lbl'] = 'Website';
$lang['js_job_lbl'] = 'Job details';
$lang['js_resume_headline_lbl'] = 'Resume headline';
$lang['js_industry_lbl'] = 'Industry name';
$lang['js_functional_name_lbl'] = 'Functional area name';
$lang['js_role_name_lbl'] = 'Role name';
$lang['js_work_exp_lbl'] = 'Total experience';
$lang['js_annual_salary_lbl'] = 'Annual salary';
$lang['js_exp_salary_lbl'] = 'Expected salary';
$lang['like_emp_list_title'] = 'Liked employer listing';
$lang['follow_emp_list_title_for_emp'] = 'Followers listing';
$lang['block_emp_list_title_for_emp'] = 'Blocked by job seeker listing';
$lang['like_emp_list_title_for_emp'] = 'Liked by job seeker listing';
$lang['manage_job_application_title'] = 'Manage job application';
$lang['manage_application_title'] = 'Manage application';
$lang['download_cv_title'] = 'Download CV';
$lang['show_js_details'] = 'Show job seeker details';
$lang['show_job_details'] = 'Show job details';
$lang['posted_on'] = 'Posted On';
$lang['message'] = 'Message';
$lang['browse_cv'] = 'Browse CV';
$lang['download_cv'] = 'Download CV';
$lang['job_application_lbl'] ='Job applications list';
$lang['job_application_left_p_lbl'] = 'Applied job seeker list';
$lang['search_with_posted_job_lbl'] = 'Search with your posted jobs';
$lang['js_public_profile_title'] = 'Public profile';
$lang['top_recruiters_lbl'] = 'शीर्ष रिक्रूटर्स';
//$lang['top_recruiters_lbl'] = 'Top Recruiters';
$lang['contact_top_recruiters_lbl'] ='शीर्ष रिक्रूटर्स से संपर्क करें';
$lang['contact_top_recruiters_di_lbl'] ='आप सीधे शीर्ष रिक्रूटर्स से संपर्क कर सकते हैं';
$lang['job_seeker_profile'] = 'Job seeker profile';
$lang['job_seeker_share_profile_about'] ='Personal information';
$lang['job_seeker_share_profile_education'] ='Education';
$lang['job_seeker_share_profile_work_exp'] ='Work experience';
$lang['job_seeker_share_profile_download_resume'] = 'Download resume';
$lang['share_profile_js_info_lbl'] = 'Personal information';
$lang['share_profile_js_name_lbl'] = 'Name';
$lang['share_profile_js_email_lbl'] ='Email';
$lang['share_profile_js_mobile_lbl'] ='Mobile';
$lang['share_profile_js_birth_lbl'] ='Birth Date';
$lang['share_profile_js_address_lbl'] = 'Address';
$lang['share_profile_js_country_lbl'] = 'Country';
$lang['share_profile_js_city_lbl'] = 'City';
$lang['share_profile_js_website_lbl'] ='Website';
$lang['share_profile_js_pstn_lbl'] = 'Position';
$lang['share_profile_js_work_area_lbl'] ='Work Area';
$lang['share_profile_js_skill_lbl'] ='Skill';
$lang['share_profile_ind_lbl'] = 'Industry';
$lang['share_profile_fn_lbl'] = 'Functional area';
$lang['share_profile_role_lbl'] = 'Role name';
$lang['share_profile_desire_lbl'] = 'Desire job type';
$lang['share_profile_emp_type_lbl'] = 'Employment type';
$lang['share_profile_p_s_lbl'] = 'Preferred shift';
$lang['share_profile_exp_salary_lbl'] ='Expected salary';
$lang['share_profile_profile_summary_lbl'] ='Profile summary';
$lang['share_profile_certi_lbl'] ='Certificate details';
$lang['share_profile_certi_name_lbl'] = 'Certificate  name';
$lang['share_profile_certi_d_lbl'] ='Certificate detail';
$lang['share_profile_passing_lbl']  = 'Passing year';
$lang['share_work_exp_lbl']  = 'Work Experience';
$lang['share_company_lbl']  = 'Company name';
$lang['share_company_lbl']  = 'Company name';
$lang['share_salary_lbl'] = 'Annual salary';
$lang['standard_x_lbl'] = 'Standard X details';
$lang['share_p_institute_name'] ='Institute name';
$lang['share_p_mark_lbl'] ='Marks / Percentage ';
$lang['standard_xii_lbl'] = 'Standard XII details ';
$lang['specialization'] = 'Specialization';
$lang['share_profile_lang_lbl'] = 'Language known';
$lang['share_profile_lang_lvl_lbl'] = 'Proficiency level';
$lang['share_profile_lang_read'] = 'Read';
$lang['share_profile_lang_write'] = 'Write';
$lang['share_profile_lang_speak'] = 'Speak';
$lang['emp_public_profile_title'] = 'Public profile';
$lang['emp_profile'] = 'Employer profile';
$lang['emp_share_profile_c_details'] = 'Company information';
$lang['emp_share_profile_sector_for_hire'] = 'Sectors hire by employer ';
/*Job seeker action end*/
$lang['indu_hire_for_share_emp_lbl'] = 'भाड़े के लिए उद्योग';
$lang['fn_area_for_share_emp_lbl'] = 'भाड़े के लिए कार्यात्मक';
$lang['skill_for_share_emp_lbl'] = 'भाड़े के लिए कौशल';

/* browse by industry start */
$lang['browse_by_indus_tit'] = 'उद्योगों द्वारा ब्राउज़ करें';
$lang['browse_by_indus_subtitle'] = '';
$lang['browse_location_tit'] = 'स्थान के आधार पर ब्राउज़ करें';
$lang['browse_location_subtitle'] = '';
$lang['browse_location_sort_tit'] = 'स्थान वर्णानुक्रम में सॉर्ट किए गए';
$lang['browse_companies_tit'] = 'कंपनियों को ब्राउज़ करें';
$lang['browse_companies_subtitle'] = '';
$lang['browse_companies_sort_tit'] = 'कंपनी का नाम वर्णानुक्रम में सॉर्ट किया गया';
$lang['browse_recruiters_tit'] = 'रिक्रूटर्स ब्राउज़ करें';
$lang['browse_recruiters_subtitle'] = '';
$lang['browse_jobseekers_tit'] = 'ब्राउज जॉबसेकर्स';
$lang['browse_jobseekers_subtitle'] = '';
$lang['recent_job_view_lbl'] = 'हाल ही में देखी गई नौकरियां';
$lang['more_job_view_lbl'] ='और अधिक नौकरियां+';
$lang['previous'] ='पिछला';
$lang['next'] ='आगामी';
/* browse by industry end */

$lang['delete_req_btn_txt'] = 'प्रोफ़ाइल हटाने का अनुरोध';
$lang['view_detail_lbl'] = 'विवरण देखें';
$lang['confirm_hightlight_job'] = 'क्या आप सुनिश्चित हैं कि यह नौकरी एक आकर्षण के रूप में है.';
$lang['add_to_highlight_success'] = 'नौकरी पर प्रकाश डाला';
$lang['add_to_highlight_error'] = 'हाइलाइट एक्शन करते समय कुछ लिखा नहीं जाता है.';
$lang['suggested_job_l_menu_lbl'] = 'सुझाई गई नौकरी.';
$lang['suggested_job_title'] = 'सुझाई गई नौकरी की सूची.';
$lang['add_to_shortlist'] = 'शॉर्टलिस्ट में जोड़ें';
$lang['confirm_add_to_shortlist'] = 'क्या आप वाकई इस एप्लिकेशन को एक शॉर्टलिस्ट के रूप में जोड़ना चाहते हैं.';
$lang['added_to_shortlist'] = 'चुने';
$lang['add_to_shortlist_success_msg'] = 'शॉर्टलिस्ट में एप्लिकेशन जोड़ा गया.';
$lang['remove_from_shortlist'] = 'शॉर्टलिस्ट से निकालें';
$lang['confirm_remove_from_shortlist'] = 'क्या आप वाकई इस एप्लिकेशन को शॉर्टलिस्ट से हटाना चाहते हैं.';
$lang['remove_from_shortlist_success_msg'] = 'शॉर्टलिस्ट से निकाला गया आवेदन.';
$lang['emp_action_err_msg']='कार्रवाई करते समय कुछ ठीक नहीं है';
$lang['shortlist_application_left_p_lbl'] = 'शॉर्टलिस्ट किया गया आवेदन';
$lang['shortlist_application_title'] = 'शॉर्टलिस्ट किया गया आवेदन';
$lang['shortlist_application_lbl'] = 'शॉर्टलिस्ट किए गए आवेदकों की सूची';
$lang['ask_emp'] = '<strong>नियोक्ता ? </strong>यहां क्लिक करे';


/*Resume search */
/*$lang['post_new_job_text'] = 'Post a Job, It’s Free!';
$lang['search_by_location'] = 'Search By Location';
$lang['by_skill'] = 'By Skills'; 
$lang['sort_by'] = 'Sort by'; 
$lang['last_30_days'] = 'Last 30 days';
$lang['last_15_days'] = 'Last 15 days';
$lang['last_7_days'] = 'Last 7 days';
$lang['last_1_days'] = 'Last 1 days';
$lang['extra_search'] = 'By Industry, Job Type & Salary ';
$lang['industry'] = 'Industry';
$lang['search_company_lbl']='Search by company';
$lang['job_type'] = 'Job type';
$lang['salary'] = 'Salary';
$lang['job_education'] = 'Job education';
$lang['viewd'] = 'Viewd';
$lang['applied'] = 'Applied';
$lang['salary_search'] = 'Select salary range ';*/
$lang['browse_resume_text'] ='नौकरी चाहने वालों को ब्राउज़ करें';
$lang['search_resume_placeholder']='कौशल, उद्योग, नौकरी करने वालों की नौकरी की भूमिका के साथ खोजें। चयन के लिए कुछ लिखें और हिट दर्ज करें';
/*$lang['total_job_count_msg'] ='Total job found :';
$lang['search_location_placeholder']='Search with location, for selection Type something and hit enter';
$lang['view_job_title'] = 'View job listing';
$lang['similar_jobs']='Similar jobs';
$lang['more_details'] ='More Details';
$lang['share'] ='Share';
$lang['share_twitter'] = 'Click to share  on Twitter';
$lang['share_facebook'] = 'Click to share on Facebook';
$lang['share_gplus'] = 'Click to share on Gplus';
$lang['share_linkedin'] = 'Click to share on Linkedin';
$lang['share_pinterest'] = 'Click to share on Pinterest';
$lang['apply'] = 'Apply';
$lang['save_job'] = 'Save job';
$lang['block'] = 'Block';
$lang['job_open_status'] = 'Open';
$lang['job_closed_status'] = 'Closed';
$lang['add_to_highlighted'] = 'Add to highlight';
$lang['job_highlighted'] = 'Highlighted';
$lang['snd_msg'] = ' Send Message';
$lang['send_application'] = 'Send Application';
$lang['like'] = 'Like';
$lang['liked'] = 'Liked';
$lang['saved'] = 'Saved';
$lang['follow'] = 'Follow';
$lang['following'] = 'Following';
$lang['blocked'] = 'Blocked';
$lang['action'] = 'Action';
$lang['fresher'] ='Fresher';
$lang['salary'] ='Salary';
$lang['exprience'] ='Exprience';
$lang['key_skills'] ='Keyskills';
$lang['desire_c_profile'] = 'Desire candidate profile';
$lang['view_contact_details'] = 'View contact details';
$lang['recruiter_details'] = 'Recruiter Details';
$lang['recruiter_name'] ='Recruiter Name';
$lang['web_site'] = 'Web site';
$lang['company_name'] = 'Company name';
$lang['mobile'] = 'Mobile';
$lang['mobile'] = 'Mobile';
$lang['overview'] = 'Overview';
$lang['location'] = 'Location';
$lang['job_details'] ='Job details';
$lang['posetd_on'] = 'Posetd on';
$lang['job_link'] = 'Job link';
$lang['apply_job'] = 'Apply For This Job';
$lang['upload_cv'] ='Upload your CV (optional)';
$lang['upload_cv_size_lbl'] ='Max. file size: 5MB ';
$lang['browse'] ='Max. file size: 5MB ';
$lang['browse'] ='Max. file size: 5MB ';
$lang['no_file_selected']='No file selected';
$lang['send_application']='Send Application';
$lang['send_msg'] = 'Send message';
$lang['email'] = 'Email';
$lang['for_search_warning'] = 'Please search something here..';
$lang['confirm_apply_job'] = 'Are you sure to apply for this job ?';
$lang['please_login_apply'] = 'Please login for the apply job';
$lang['apply_job_success_msg'] = 'Applcation submitted successfully.'; 
$lang['apply_job_err_msg'] = 'Something is not right, while applying job';
$lang['already_apply'] = 'Already applied'; */
/*Resume search End*/

$lang['job_life'] = 'नौकरी का जीवन (आपकी पोस्ट लाइव रहने के दिनों की संख्या)';
$lang['mobile_charecter_limit_info']='मोबाइल नंबर न्यूनतम 10 और अधिकतम 13 वर्ण होना चाहिए';
$lang['upload_pro_pic_social_info'] = 'आपकी प्रोफ़ाइल तस्वीर पहले से ही आपके फेसबुक / gplus खाते से अपलोड की गई है। अगर आप अपनी प्रोफाइल पिक्चर बदलना चाहते हैं तो यहां से अपलोड करें.';

?>