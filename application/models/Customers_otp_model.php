<?php

class Customers_otp_model extends CI_model {

    public $table_name = "jobseeker";

    function verify_otp($mobile, $otp) {
        if ($this->db->get_where($this->table_name, ["mobile" => $mobile, "otp" => $otp])->row()) {
            $this->db->set("status", 1);
            $this->db->where("mobile", $mobile);
            $this->db->set("otp", null);
            $this->db->set("updated_at", null);
            if ($this->db->update($this->table_name)) {
                $this->db->set("mobile_verified", 1);
                $this->db->where("mobile", $mobile);
                $this->db->update("jobseeker");
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function send_verfication_otp($customers_id) {
        $mobile = $this->db->get_where("jobseeker", ["id" => $customers_id])->row()->mobile;
        //$mobile = $this->db->get_where("jobseeker", ["id" => $customers_id])->row()->mobile;
        //echo $this->db->last_query(); die;
        // $otp = rand(1111, 9999);
        $otp ="1234";
        $is_existed = $this->db->get_where($this->table_name, ["mobile" => $mobile])->row();
        $message = '<#>' . SITE_TITLE . " Verification OTP " . $otp . ' ' . SMS_HASH_OTP_KEY; // rMUtB4EueQ9
        //<#> Hi, {name} \nThank You For Registration. Your Verification OTP is {otp} \n0oz5WPkYVoW
        if ($is_existed) {
            $this->db->set("otp", $otp);
            $this->db->set("updated_at", time());
            $this->db->where("mobile", $mobile);
            $this->db->set("status", 1);
            if ($this->db->update($this->table_name)) {
                send_message($message, $mobile);
            }
        } else {
            $this->db->set("mobile", $mobile);
            $this->db->set("otp", $otp);
            $this->db->set("status", 1);
            $this->db->set("created_at", time());
            if ($this->db->insert($this->table_name)) {
                send_message($message, $mobile);
            }
        }
        return $otp;
    }

    function send_not_registered_customer_verification_code($mobile, $account_id = null, $type = null) {
        $otp = rand(1111, 9999);
        $is_existed = $this->db->get_where($this->table_name, ["mobile" => $mobile])->row();
        $message = SITE_TITLE . " Verification OTP " . $otp;
        if ($is_existed) {
            if ($type == "google") {
                $this->db->set("google_account_id", $account_id);
            } else if ($type == "facebook") {
                $this->db->set("facebook_account_id", $account_id);
            }
            $this->db->set("otp", $otp);
            $this->db->set("updated_at", time());
            $this->db->where("mobile", $mobile);
            $this->db->set("status", 1);
            if ($this->db->update($this->table_name)) {
                send_message($message, $mobile);
            }
        } else {
            if ($type == "google") {
                $this->db->set("google_account_id", $account_id);
            } else if ($type == "facebook") {
                $this->db->set("facebook_account_id", $account_id);
            }
            $this->db->set("mobile", $mobile);
            $this->db->set("otp", $otp);
            $this->db->set("status", 1);
            $this->db->set("created_at", time());
            if ($this->db->insert($this->table_name)) {
                send_message($message, $mobile);
            }
        }
        return $otp;
    }

}
