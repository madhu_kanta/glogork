<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Common_front_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['base_url'] = $this->base_url = base_url();
		$this->data['config_data'] = $this->get_site_config();
		$this->data['custom_lable'] = $this->lang;
		$this->limit_per_page = 3;
		$this->field_duplicate='';
		$this->check_for_update_expired();
		$this->path_js_photos = 'assets/js_photos/';
		$this->path_company_logo = 'assets/company_logos/';
		$this->path_emp_photos = 'assets/emp_photos/';
		$this->path_resume_file = 'assets/resume_file/';
	}
	
	public function set_orgin()
	{
		 header('Access-Control-Allow-Origin: *');
	     header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		 if (isset($_SERVER['HTTP_ORIGIN']))
		 {
			//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');
		 }
     // Access-Control headers are received during OPTIONS requests
 	   	 if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
		 {
        	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");         
 
        	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        	exit(0);
	     }
		//$this->output->set_header('Access-Control-Allow-Origin: *');
	}
	// call every time but run once
	public function check_for_update_expired()
	{
		$config_data = $this->data['config_data'];
		$current_date_site = $config_data['current_date_crone'];
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		if($current_date_site != $current_data)
		{
			// for job seeker
				$where = array('plan_status'=>'Paid'," plan_expired < '$current_data' ",'is_deleted'=>'No');
				
				$this->update_insert_data_common('jobseeker',array('plan_status'=>'Expired'),$where,1,0);
				$this->common_model->last_query();
			// for job seeker
			// for employer
				$this->update_insert_data_common('employer_master',array('plan_status'=>'Expired'),$where,1,0);
				$this->common_model->last_query();
			// for employer	
			// update date in site_config
			$this->update_insert_data_common('site_config',array('current_date_crone'=>$current_data),'',1);
		}
	}
	public function checkLoginfront()
	{
		$returnvar = false;
		if($this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') !="" && is_array($this->session->userdata('jobportal_user')) && count($this->session->userdata('jobportal_user')) > 0 )
		{
				$returnvar = true;
		}
		else if($this->input->post('user_id'))
		{
			$user_id = $this->input->post('user_id');
			if($user_id !='') 
			{
				$returnvar = true;
			}
		}
		return $returnvar;
	}
	public function checkLoginfrontempl()
	{
		$returnvar = false;
		if($this->session->userdata('jobportal_employer') && $this->session->userdata('jobportal_employer') !="" && is_array($this->session->userdata('jobportal_employer')) && count($this->session->userdata('jobportal_employer')) > 0 )
		{
				$returnvar = true;
		}
		return $returnvar;
	}
	public function get_logged_user_typeid()
	{
		$user_data = array('user_type'=>'','user_id'=>'');
		
		$js_data = $this->get_userid();
		if($js_data !='')
		{
			$user_data = array('user_type'=>'job_seeker','user_id'=>$js_data);
		}
		else
		{
			$emp_data = $this->get_empid();
			if($emp_data !='')
			{
				$user_data = array('user_type'=>'employer','user_id'=>$emp_data);
			}
		}
		return $user_data;
	}
	public function get_userid()
	{
		$user_id = '';
		if($this->input->post('user_id'))
		{
			$user_id = $this->input->post('user_id');
		}
		else if($this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') !='' && is_array($this->session->userdata('jobportal_user')) && count($this->session->userdata('jobportal_user'))> 0)
		{
			$jobportal_user = $this->session->userdata('jobportal_user');
			$user_id = $jobportal_user['user_id'];
		}
		return $user_id;
	}
	public function get_empid()
	{
		$emp_id = '';
		if($this->input->post('emp_id') && $this->input->post('emp_id')!='')
		{
			$emp_id = $this->input->post('emp_id');
		}
		else if($this->session->userdata('jobportal_employer') && $this->session->userdata('jobportal_employer') !='' && is_array($this->session->userdata('jobportal_employer')) && count($this->session->userdata('jobportal_employer'))> 0)
		{
			$jobportal_employer = $this->session->userdata('jobportal_employer');
			$emp_id = $jobportal_employer['employer_id'];
		}
		return $emp_id;
	}
	public function get_login_user_data($js_id,$select='*')
	{
		$where = array('id'=>$js_id);
		$login_data = $this->common_front_model->get_count_data_manual("jobseeker_view",$where,1,$select,'','',1);
		return $login_data;						
	}
	public function get_user_data($table,$id,$select='*',$id_f ='id')
	{
		$user_data = '';
		if($table !='' && $id!='' && $id_f !='')
		{
			$where = array($id_f=>$id);
			$user_data = $this->common_front_model->get_count_data_manual($table,$where,1,$select,'','',1);
		}
		return $user_data;
	}
	
	public function redirectLoginfront()
	{
		if($this->input->post('user_id'))
		{
			
		}
		else if(!$this->session->userdata('jobportal_user') || $this->session->userdata('jobportal_user') =="" && count($this->session->userdata('jobportal_user')) ==0 )
		{
			$base_url = $this->base_url;
			redirect($base_url);
		}
		elseif($this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') !="" && count($this->session->userdata('jobportal_user')) > 0 )
		{
			$js_id = $this->session->userdata('jobportal_user');
			$where_arra = array('id'=>$js_id['user_id'],'status'=>'APPROVED','is_deleted'=>'No');
			$check_if_active = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,0,'','','','','');
			if(!($check_if_active > 0))
			{
				$this->session->unset_userdata('jobportal_user');	
				$base_url = $this->base_url;
				redirect($base_url);
			}
		}
	}
	public function redirectLoginfrontempl()
	{
		if(!$this->session->userdata('jobportal_employer') || $this->session->userdata('jobportal_employer') =="" && count($this->session->userdata('jobportal_employer')) ==0 )
		{
			$base_url = $this->base_url;
			redirect($base_url);
		}
		elseif($this->session->userdata('jobportal_employer') && $this->session->userdata('jobportal_employer') !="" && count($this->session->userdata('jobportal_employer')) > 0 )
		{
			$emp_id = $this->session->userdata('jobportal_employer');
			$where_arra = array('id'=>$emp_id['employer_id'],'status'=>'APPROVED','is_deleted'=>'No');
			$check_if_active = $this->get_count_data_manual('employer_master_view',$where_arra,0,'','','','','');
			if(!($check_if_active > 0))
			{
				$this->session->unset_userdata('jobportal_employer');	
				$base_url = $this->base_url;
				redirect($base_url);
			}
		}
		
	}
	function getCurrentDate($dformat='Y-m-d H:i:s')
	{
		//date_default_timezone_set('UTC'); // already set in config
		return date($dformat);
	}
	function callCURL($url='')
	{
		$return_res ='';
		if($url !='')
		{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
			curl_setopt($ch, CURLOPT_TIMEOUT, 4);
			$return_res = curl_exec($ch);
			curl_close($ch);
		}
		return $return_res;
	}
	function displayDate($date = '',$dformat='F j, Y h:i A')// Y-m-d h:i:s
	{
		if($date !='' && $dformat !='')
		{
			$time_zone ='';
			if(!$this->session->userdata('time_zone') || $this->session->userdata('time_zone') =="" && count($this->session->userdata('time_zone')) ==0 )
			{
				$ip = $this->input->ip_address();
				$final_api = 'http://ip-api.com/json/';
				if($ip !='')
				{
					$final_api_call= $final_api.$ip;
					$response_data = $this->callCURL($final_api_call);
					if($response_data !='')
					{
						$response_data = json_decode($response_data);
						if(isset($response_data->status) && $response_data->status == 'fail')
						{
							$response_data = $this->callCURL($final_api);
							if($response_data !='')
							{
								$response_data = json_decode($response_data);
							}
						}
					}
					if(isset($response_data->timezone) && $response_data->timezone != '')
					{
						$time_zone = $response_data->timezone;
						$this->session->set_userdata('time_zone', $time_zone);
					}
					
					//print_r($response_data);
				}
			}
			else
			{
				$time_zone = $this->session->userdata('time_zone');
			}
			if($time_zone =='')
			{
				$time_zone = 'Asia/Kolkata';
			}
			$strtime = strtotime($date);
			date_default_timezone_set($time_zone);
			$date_retuen = date($dformat,$strtime);
			date_default_timezone_set('UTC'); // already set in config
			return $date_retuen;
		}
		else
		{
			return '-';
		}
	}
	public function __load_header($label_page='',$status='')
	{
		$this->label_page = $label_page;
		$page_title = $label_page;
		if($status !='')
		{
			$page_title = $page_title.' - '.$status;
		}
		$this->data['page_title'] = $page_title;
		$this->load->view('front_end/page_part/header',$this->data);
	}
	public function __load_header_employer($label_page='',$status='')
	{
		$this->label_page = $label_page;
		$page_title = $label_page;
		if($status !='')
		{
			$page_title = $page_title.' - '.$status;
		}
		$this->data['page_title'] = $page_title;
		$this->load->view('front_end/page_part/header_employer',$this->data);
	}
	public function __load_footer($model_body='')
	{
		$this->data['model_body'] = $model_body;
		$this->data['model_title'] = 'Add '.$this->label_page;
		$this->load->view('front_end/page_part/footer',$this->data);
	}
	public function __load_footer_employer($model_body='')
	{
		$this->data['model_body'] = $model_body;
		$this->data['model_title'] = 'Add '.$this->label_page;
		$this->load->view('front_end/page_part/footer_employer',$this->data);
	}
	public function get_site_config()
	{
       	$this->db->limit(1);
		$query = $this->db->get_where('site_config', array('id' => 1));
       	return $query->row_array();
	}
	public function get_session_data()
	{
		return $this->session->userdata('jobportal_user');
	}
	public function checkLogin()
	{
		if(!$this->session->userdata('jobportal_user') || $this->session->userdata('jobportal_user') =="" && count($this->session->userdata('jobportal_user')) ==0 )
		{
			$base_url = base_url();
			redirect($base_url);
		}
	}
	public function common_send_email($to_array,$subject,$message,$cc_array= '',$bcc_array ='',$attachment = '')
	{
		return $this->common_model->common_send_email($to_array,$subject,$message,$cc_array,$bcc_array,$attachment);
	}
	public function common_sms_send($mobile,$sms)
	{
		$this->common_model->common_sms_send($mobile,$sms);
	}
	function last_query()
	{
		return $this->db->last_query();
	}
	function get_count_data_manual($table,$where_arra='',$flag_count_data = 0,$select_f ='',$order_by='',$page='',$limit='',$disp_query = "")
	{
		if($table !='')
		{
			if($where_arra !='' && is_array($where_arra) && count($where_arra) >0)
			{
				foreach($where_arra as $key=>$val)
				{
					
					if(is_numeric($key))
					{ 
						$this->db->where($val);
					}
					else
					{ 
						$this->db->where($key,$val);
					}
				}
			}
			else if($where_arra !='')
			{
				$this->db->where($where_arra);
			}
			if(isset($select_f) && $select_f !='')
			{
				$this->db->select($select_f);
			}

			if($flag_count_data == 0)
			{
				//$search_data = $this->db->get_compiled_select($table);
				return $this->db->count_all_results($table);
			}
			else 
			{
				if(isset($order_by) && $order_by !='')
				{
					$this->db->order_by($order_by);
				}
				if($flag_count_data==1)
				{
					$this->db->limit(1);
				}
				else
				{
					/*if($limit =="")
					{
						$limit = $this->limit_per_page;
					}*/
					if($page !='' && $limit !='')
					{
						$start = (($page - 1) * $limit);
						$this->db->limit($limit,$start);
					}
					//$this->db->limit(1);
				}
				
				if($disp_query == 1)
				{
					echo $this->db->get_compiled_select($table);
				}
				
				$query = $this->db->get($table);
				if($flag_count_data == 1)
				{
					if($query->num_rows() == 0)
					{
						return '';
					}
					else
					{
						return $query->row_array();
					}
				}
				else
				{
					if($query->num_rows() == 0)
					{
						return '';	
					}
					else
					{ 
						return $query->result_array();
					}
				}
			}
		}
		else
		{
			return '';
		}
		
	}
	function update_insert_data_common($table='',$data_array='',$where_arra='',$flag_update=1,$limit=1)
	{
		$return = false;
		if($table !='' && $data_array !='')
		{
			if($flag_update == 1)
			{
				if($where_arra !='' && is_array($where_arra) && count($where_arra) >0)
				{
					foreach($where_arra as $key=>$val)
					{
						if(is_numeric($key))
						{
							$this->db->where($val);
						}
						else
						{
							$this->db->where($key,$val);
						}
					}
				}
				else if($where_arra !='')
				{
					$this->db->where($where_arra);
				}

				if($limit == 1)
				{
					$this->db->limit(1);
				}
				$return = $this->db->update($table,$data_array);
			}
			else
			{
				if($flag_update == 2)
				{
					$return = $this->db->insert_batch($table,$data_array);
				}
				else
				{
					$return = $this->db->insert($table,$data_array);
				}
			}
		}
		return $return;
	}
	function data_delete_common($table='',$where_arra='',$limit=0)
	{
		$return = false;
		if($table !='')
		{
			if($where_arra !='' && is_array($where_arra) && count($where_arra) >0)
			{
				foreach($where_arra as $key=>$val)
				{
					$this->db->where($key,$val);
				}
			}
			if($limit == 1)
			{
				$this->db->limit(1);
			}
			$return = $this->db->delete($table);
		}
		return $return;
	}
	function getconfingValue($item_name ='')
	{
		$return = '';
		if($item_name !='')
		{
			$return = $this->config->item($item_name);
		}
		return $return;
	}
	
	public function save_update_data($table_name='', $data_array_custom='',$primary_key='id', $mode ='add', $id ='', $field_duplicate='',$limit=1, $is_delete_fild='is_deleted',$fileupload = 1)
	{
		//error_reporting(E_ALL);
		if($table_name == '' && $primary_key == '')
		{
			return false;
		}
		$response_data = '';
		//print_r($_REQUEST);
		//print_r($_FILES);
		//exit;
		$table_field = $this->db->list_fields($table_name);
		
		if($table_name !='' && $table_field !='' && is_array($table_field) && count($table_field) > 0 )
		{
			$where_arra='';
			$flag_update = 0;
			$data_array = array();
			$data_file_old_array = array();
			$data_file_new_array = array();
			$where_arra_dup = '';
			//$mode = 'add';
			//$id = '';
			$is_duplicate = 0;
			if(isset($_REQUEST['mode']) && $_REQUEST['mode'] !='' && $_REQUEST['mode'] =='edit')
			{
				$mode = 'edit';
			}
			if($this->input->post('id'))
			{
				$id = trim($this->input->post('id'));
			}
			if($id !='' && $mode !='' && $mode =='edit' && $primary_key !='')
			{
				$where_arra = array($primary_key =>$id);
				//$this->db->where($primary_key .' != '.$id); // for duplicate check with id
				$where_arra_dup = "'".$primary_key .' != '.$id."'";
				$flag_update = 1;
			}
			if($field_duplicate !='')
			{
				$filed_dup_check = $field_duplicate;
				if(isset($filed_dup_check) && $filed_dup_check !='' && is_array($filed_dup_check) && count($filed_dup_check) > 0)
				{
					foreach($filed_dup_check as $filed_dup_check_val)
					{
						if($this->input->post($filed_dup_check_val))
						{
							$filed_dup_check_val_fill = $this->input->post($filed_dup_check_val);
							if($filed_dup_check_val_fill !='' && $filed_dup_check_val !='')
							{
								$this->db->where($filed_dup_check_val,$filed_dup_check_val_fill);
							}
						}
					}
					if(in_array($is_delete_fild,$table_field))
					{
						$this->db->where($this->table_name.'.'.$is_delete_fild,'No');
					}
					
					$cound_duplicate = $this->get_count_data_manual($table_name,$where_arra_dup,0,$primary_key);
					if($cound_duplicate > 0)
					{
						$is_duplicate = 1;
						$response_data = 'exists';
						return $response_data;
					}
				}
			}
			if($is_duplicate == 0)
			{
				if($mode =='add')
				{
					$genrate_url='';
					if($this->input->post('genrate_url'))
					{
						$genrate_url = $this->input->post('genrate_url');
						if($genrate_url !='')
						{
							$this->generate_url_comon($genrate_url);
						}
					}
				}
				// for check edit mode
				// for check file upload 
				// set fileupload var to false if you doont want to upload a file
				if($fileupload==1)
				{
					
					if(isset($_FILES) && $_FILES !='' && count($_FILES) > 0)
					{
						
						foreach($_FILES as $key=>$val)
						{

							if(in_array($key,$table_field) && isset($val['name']) && $val['name'] !='' && isset($val['size']) && $val['size'] > 0)
							{
								
								$path_upload = '';
								if(isset($_REQUEST[$key.'_path']) && $_REQUEST[$key.'_path'] !='')
								{
									$path_upload = trim($_REQUEST[$key.'_path']);
								}
								
								$old_upload = '';
								if(isset($_REQUEST[$key.'_val']) && $_REQUEST[$key.'_val'] !='')
								{
									$old_upload = trim($_REQUEST[$key.'_val']);
								}
								
								$allowed_types ='gif|jpg|png|jpeg';
								if(isset($_REQUEST[$key.'_ext']) && $_REQUEST[$key.'_ext'] !='')
								{
									$allowed_types = trim($_REQUEST[$key.'_ext']);
								}
								
								$temp_data_array = array('file_name'=>$key,'upload_path'=>$path_upload,'allowed_types'=>$allowed_types);
								$upload_data = $this->upload_file($temp_data_array);
								//print_r($upload_data);
								//exit;
								if(isset($upload_data) && $upload_data !='' && is_array($upload_data) && count($upload_data) > 0 && isset($upload_data['status']) && $upload_data['status'] =='success')
								{
									$file_data = $upload_data['file_data'];
									$data_array[$key] = $file_data['file_name'];
									$data_file_old_array[] = $path_upload.$old_upload;
									$data_file_new_array[] = $path_upload.$file_data['file_name'];
								}
								else
								{
									$this->delete_file($data_file_new_array);
									$response_data = 'file_upload_error';
									return $response_data;
								}
							}
						}
					}

				}
				// for check file upload
				// for request field and add
				if(isset($_REQUEST) && $_REQUEST !='' && is_array($_REQUEST) && count($_REQUEST) > 0)
				{
					foreach($_REQUEST as $key=>$val)
					{
						if(in_array($key,$table_field) && $key != $primary_key)
						{
							if(is_array($this->security->xss_clean($val)))
							{
							  $val = $this->security->xss_clean($val);
							}
							else
							{
								$val = trim($this->security->xss_clean($val));
							}
							$data_array[$key] = $val;
						}
					}
				}
				// for request field and add
				// for adding custom field in data array
				//print_r($data_array);
				//print_r($data_array_custom);
				if(isset($data_array_custom) && $data_array_custom !='' && is_array($data_array_custom) && count($data_array_custom) > 0)
				{
					foreach($data_array_custom as $key_d=>$val_d)
					{
						if(in_array($key_d,$table_field) && $key_d != $primary_key)
						{
							$data_array[$key_d] = $val_d;
						}
					}
				}
				//print_r($data_array);
				// for adding custom field in data array
				if($data_array !='' && is_array($data_array) && count($data_array) > 0)
				{
					$response = $this->update_insert_data_common($table_name,$data_array,$where_arra,$flag_update,$limit);
					//echo $this->last_query();
					if(isset($response) && $response)
					{
						$this->delete_file($data_file_old_array);
						$response_data = 'success';
					}
					else
					{
						$this->delete_file($data_file_new_array);
						$response_data = 'error';
					}
				}
			}
		}
		return $response_data;
	}
	public function upload_file($upload_data = '')
	{
		$return_message = '';
		if(isset($upload_data) && $upload_data !='' && is_array($upload_data) && count($upload_data) > 0 && isset($upload_data['upload_path']) && $upload_data['upload_path'] !='' && isset($upload_data['file_name']) && $upload_data['file_name'] !='')
		{			
			$config['upload_path']  = $upload_data['upload_path'];
			if(isset($upload_data['allowed_types']) && $upload_data['allowed_types'] !='')
			{
				$config['allowed_types']= $upload_data['allowed_types'];
			}
			//$config['max_size']= 1024 * 10;
			$config['max_size']= 2048;
			if(isset($upload_data['max_size']) && $upload_data['max_size'] !='')
			{
				//$config['max_size']= $upload_data['max_size'];
			}
			if(isset($upload_data['encrypt_name']) && $upload_data['encrypt_name'] !='')
			{
				$config['encrypt_name']= $upload_data['encrypt_name'];
			}
			else
			{
				$config['encrypt_name'] = TRUE;
			}
			$this->load->library('upload'); //  , $config
			$this->upload->initialize($config);
			
			if(!$this->upload->do_upload($upload_data['file_name']))
			{
				$error_message = $this->upload->display_errors();
				if('<p>The file you are attempting to upload is larger than the permitted size.</p>' == $error_message)
				{
					$error_message = 'The file you are attempting to upload is larger than the permitted size, Max file size allow upto 2MB';
				}
				$return_message = array('status'=>'error','error_message'=>$error_message);
				//print_r($return_message);
			}
			else
			{
				$return_message = array('status'=>'success','file_data'=>$this->upload->data());
				//print_r($return_message);
			}
		}
		return $return_message;
	}
	public function delete_file($file_name='')
	{
		if(isset($file_name) && $file_name !='' && is_array($file_name))
		{
			if(count($file_name) > 0)
			{
				foreach($file_name as $file_name_val)
				{
					if(isset($file_name_val) && $file_name_val !='' && file_exists($file_name_val))
					{
						@unlink($file_name_val);
					}	
				}
			}
		}
		else if(isset($file_name) && $file_name !='' && file_exists($file_name))
		{
			@unlink($file_name);
		}
	}
	public function rander_pagination_old($url='',$count=0,$set_limit = '')
	{   
		$return_page = '';
		if($set_limit=='')
		{
			$set_limitvar = $this->limit_per_page;
		}
		else
		{
			$set_limitvar = $set_limit;
		}
		if($url !='' && $count !='' && $count > 0)
		{
			$this->load->library('pagination');
			$config = $this->getconfingValue('config_pag');
			$config['full_tag_open'] = '<ul id="ajax_pagin_ul">';
			$config['cur_tag_open'] = '<li><a href="#" class="current-page">';
			$config['first_tag_open'] = '<li class="prev page ci-pagination-first">';
    		$config['first_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li class="prev page ci-pagination-prev">';
   			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li class="next page ci-pagination-next">';
    		$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="next page ci-pagination-next">';
    		$config['last_tag_close'] = '</li>';
			$config['base_url'] = $this->base_url.$url;
			$config['per_page'] = $set_limitvar;
			$config['total_rows'] = $count;
			$this->pagination->initialize($config);
			$return_page = $this->pagination->create_links();
			$return_page ='<div class="pagination-container">
			<nav class="pagination">'.$return_page.'</nav></div>';
			
		}
		return $return_page;
	}
	public function rander_pagination($url='',$count=0,$set_limit = '')
	{   
		$return_page = '';
		if($set_limit=='')
		{
			$set_limitvar = $this->limit_per_page;
		}
		else
		{
			$set_limitvar = $set_limit;
		}
		if($url !='' && $count !='' && $count > 0)
		{
			$this->load->library('pagination');
			$config = $this->getconfingValue('config_pag');
			$config['full_tag_open'] = '<ul class="pagination pagination-v1" id="ajax_pagin_ul">';
			$config['cur_tag_open'] = '<li><a href="#" class="current-page active">';
			$config['first_tag_open'] = '<li>';
    		$config['first_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
   			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
    		$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
    		$config['last_tag_close'] = '</li>';
			$config['base_url'] = $this->base_url.$url;
			$config['per_page'] = $set_limitvar;
			$config['total_rows'] = $count;
			$config['next_link'] ='<i class="fa fa-angle-double-right"></i>';
			$config['prev_link'] ='<i class="fa fa-angle-double-left"></i>';
			$this->pagination->initialize($config);
			$return_page = $this->pagination->create_links();
			$return_page ='<div class="pagination-wrap">'.$return_page.'</div>';
			
		}
		return $return_page;
	}
	function getfooterabouttxt($pageurl='about-us')
	{
		$returningvar = '';
		$where_arra = array('page_url'=>$pageurl,'status'=>'APPROVED','is_deleted'=>'No');
		$cmsdata = $this->get_count_data_manual('cms_pages',$where_arra,1,'page_content','','');
		if($cmsdata)
		{
			$returningvar = substr(strip_tags($cmsdata['page_content']), 0, 250);
		}
		return $returningvar;
	}
	function getemailtemplate($tempname = '')
	{
		$tempdata = array();
		if($tempname != '')
		{
			$where_arra = array('template_name'=>$tempname,'status'=>'APPROVED','is_deleted'=>'No');
			$tempdata = $this->get_count_data_manual('email_templates',$where_arra,1,'','','','','');
		}
		return $tempdata;
	}
	function getadvertisement($adlevel='Level 1')
	{
		$where_arra = array('level'=>$adlevel,'status'=>'APPROVED','is_deleted'=>'No');
		$addata = $this->get_count_data_manual('advertisement_master',$where_arra,1,'','rand()','','','');
		return $addata;
	}
	function getstringreplaced($actula_content,$array=array())
	{
		$email_template = strtr($actula_content, $array);
		return $email_template;
	}
	
	function get_sms_template($tempname='')
	{
		$tempdata = '';
		if($tempname !='')
		{
			$where_arra = array('template_name'=>$tempname,'status'=>'APPROVED','is_deleted'=>'No');
			$tempdata = $this->get_count_data_manual('sms_templates',$where_arra,1,'','','','','');
		}
		return $tempdata;
	}
	function fileuploadpaths($foldername='',$level=1,$folderpath='')
	{
		$generatedpath = '';
		if($foldername !='')
		{
			if($level==1)
			{
				$generatedpath = './assets/'.$foldername;//$this->base_url.
			}
			if($level!=1)
			{
				$generatedpath = './assets/'.$folderpath.'/'.$foldername;//$this->base_url.
			}
		}
		return $generatedpath;
	}
	function sms_string_replaced($actula_content,$array=array())
	{
		$sms_template = strtr($actula_content, $array);
		return $sms_template;
	}
	
	function dataimage_fullurl($data='',$column_nam='',$datatype = 'multiple')
	{
		$tempdata = array();
		if($data !='' && $column_nam !='' && is_array($column_nam) && count($column_nam) > 0)
		{
			if($datatype!='single')
			{
				foreach($data as $data_val)
				{
					foreach($column_nam as $key=>$val)
					{
						if(isset($data_val[$key]) && $data_val[$key] !='' && file_exists($val.'/'.$data_val[$key]))
						{
							$data_val[$key] = $this->base_url.$val.'/'.$data_val[$key];
						}
						else
						{
							$data_val[$key] = '';
						}
					}
					$tempdata[] = $data_val;
				}
			}
			else
			{
				foreach($column_nam as $key=>$val)
				{
					if(isset($data[$key]) && $data[$key] !='' && file_exists($val.'/'.$data[$key]))
					{
						$data[$key] = $this->base_url.$val.'/'.$data[$key];
					}
					else
					{
						$data[$key] = '';
					}
				}
				$tempdata[] = $data;
			}
		}
		return $tempdata;
	}
	function checkfieldnotnull($filed='')
	{
		$returnvar = false;	
		if($filed!='' && !is_null($filed))
		{
			$returnvar = true;
		}
		return $returnvar;
	}
	public function get_list($get_list='',$return_opt='json',$currnet_val='',$retun_for = 'str',$selected_val ='',$tocken_val  =0)
	{
		$this->tabel_config = array(
			'state_list'=>array('table_name'=>'state_master','pri_key'=>'id','disp_clm'=>'state_name','rel_clm_name'=>'country_id','label'=>$this->lang->line('state')),
			'country_list'=>array('table_name'=>'country_master','pri_key'=>'id','disp_clm'=>'country_name','label'=>$this->lang->line('country'),'rel_clm_name'=>''),
			'marital_list'=>array('table_name'=>'marital_status_master','pri_key'=>'id','disp_clm'=>'marital_status','label'=>$this->lang->line('country'),'rel_clm_name'=>''),
			'city_list'=>array('table_name'=>'city_master','pri_key'=>'id','disp_clm'=>'city_name','label'=>$this->lang->line('city'),'rel_clm_name'=>'state_id'),
			'all_city_list'=>array('table_name'=>'city_master','pri_key'=>'id','disp_clm'=>'city_name','label'=>$this->lang->line('city'),'rel_clm_name'=>'state_id'),
			'edu_list'=>array('table_name'=>'educational_qualification_master','pri_key'=>'id','disp_clm'=>'educational_qualification','label'=>$this->lang->line('qualification_lvl'),'rel_clm_name'=>'qualification_level_id'),
			'industries_master'=>array('table_name'=>'industries_master','val_clm'=>'id','pri_key'=>'id','disp_clm'=>'industries_name','label'=>$this->lang->line('type_industry'),'rel_clm_name'=>''),
			'functional_area_master'=>array('table_name'=>'functional_area_master','val_clm'=>'id','pri_key'=>'id','disp_clm'=>'functional_name','label'=>$this->lang->line('functional_area'),'rel_clm_name'=>''),
			'role_master'=>array('table_name'=>'role_master','val_clm'=>'id','disp_clm'=>'role_name','pri_key'=>'id','label'=>$this->lang->line('job_role'),'rel_clm_name'=>'functional_area'),
			'currency_master'=>array('table_name'=>'currency_master','val_clm'=>'currency_code','disp_clm'=>'currency_code','pri_key'=>'id','label'=>$this->lang->line('currency_type'),'rel_clm_name'=>''),
			'key_skill_master'=>array('table_name'=>'key_skill_master','val_clm'=>'key_skill_name','disp_clm'=>'key_skill_name','pri_key'=>'id','label'=>$this->lang->line('skill'),'rel_clm_name'=>''),  
			'job_type_list'=>array('table_name'=>'job_type_master','pri_key'=>'id','val_clm'=>'job_type','disp_clm'=>'job_type','rel_clm_name'=>'','label'=>'Job Type'),
			'employement_list'=>array('table_name'=>'employement_type','pri_key'=>'id','val_clm'=>'employement_type','disp_clm'=>'employement_type','rel_clm_name'=>'','label'=>'Employment Type'),
			'shift_list'=>array('table_name'=>'shift_type','pri_key'=>'id','val_clm'=>'shift_type','disp_clm'=>'shift_type','rel_clm_name'=>'','label'=>'Prefered Shift'),
			'title_list'=>array('table_name'=>'personal_titles_master','pri_key'=>'id','val_clm'=>'shift_type','disp_clm'=>'personal_titles','rel_clm_name'=>'','label'=>'Title'),
			'job_payment_list'=>array('table_name'=>'job_payment_master','pri_key'=>'id','val_clm'=>'id','disp_clm'=>'job_payment','rel_clm_name'=>'','label'=>$this->lang->line('job_payment')),
			'company_type_master'=>array('table_name'=>'company_type_master','val_clm'=>'id','disp_clm'=>'company_type','pri_key'=>'id','label'=>$this->lang->line('myprofile_emp_cmptypelbl'),'rel_clm_name'=>''),
			'company_size_master'=>array('table_name'=>'company_size_master','val_clm'=>'id','disp_clm'=>'company_size','pri_key'=>'id','label'=>$this->lang->line('myprofile_emp_cmpsjizelbl'),'rel_clm_name'=>''),
			'skill_level_master'=>array('table_name'=>'skill_level_master','val_clm'=>'skill_level','disp_clm'=>'skill_level','pri_key'=>'id','label'=>$this->lang->line('myprofile_pro_lvl_blak_opt'),'rel_clm_name'=>''),
			'skill_language_master'=>array('table_name'=>'skill_language_master','val_clm'=>'id','disp_clm'=>'skill_language','pri_key'=>'id','label'=>$this->lang->line('myprofile_lang_blak_opt'),'rel_clm_name'=>''),
			'salary_range_list'=>array('table_name'=>'salary_range','pri_key'=>'id','val_clm'=>'salary_range','disp_clm'=>'salary_range','rel_clm_name'=>'id','label'=>'Salary Range')
		);
		//
		
		$selected_arr = array();
		if($selected_val !='')
		{
			if(!is_array($selected_val))
			{
				$selected_arr[] = explode(',',$selected_val);
				$selected_arr = $selected_arr[0];
			}
			else
			{
				$selected_arr[] = $selected_val;
				$selected_arr = $selected_arr[0];
			}
		}
		
		$opt_array = array();
		if($this->input->post('get_list') == 'city_list' || $this->input->post('get_list') == 'edu_list')
		{
			//$opt_array[] = array('state'=>$this->lang->line('select_option'),'city'=>'');
		}
		else
		{
			//$opt_array[] = array();
			/*comment for opt_array in app remove select option*/
			//$opt_array[] = array('id'=>'0','val'=>$this->lang->line('select_option'));
		}
		if($this->input->post('get_list'))
		{
			$get_list = trim($this->input->post('get_list'));
		}
		if($this->input->post('currnet_val'))
		{
			$currnet_val = trim($this->input->post('currnet_val'));
		}
		if($this->input->post('tocken_val'))
		{
			$tocken_val = trim($this->input->post('tocken_val'));
		}
		if($get_list  !='')
		{
			if(isset($this->tabel_config[$get_list]) && $this->tabel_config[$get_list] !='' && is_array($this->tabel_config[$get_list]) && count($this->tabel_config[$get_list]) > 0)
			{ 
				$tabel_config = $this->tabel_config[$get_list];
				$label_sele = $this->lang->line('select_option');
				if(isset($tabel_config['label']) && $tabel_config['label'] !='')
				{
					$label_sele = 'Select '.$tabel_config['label'];
				}
				$str_ddr = '<option value="">'.$label_sele.'</option>';	
				$elemt_array = array('relation'=>array('rel_table'=>$tabel_config['table_name'],'key_val'=>$tabel_config['pri_key'],'key_disp'=>$tabel_config['disp_clm'],'rel_col_name'=>$tabel_config['rel_clm_name'],'rel_col_val'=>$currnet_val));
				if($this->tabel_config[$get_list]['table_name'] == 'city_master' || $this->tabel_config[$get_list]['table_name'] == 'educational_qualification_master')
				{
					if($this->tabel_config[$get_list]['table_name'] == 'city_master')
					{
						$elemt_array_state = array('relation'=>array('rel_table'=>'state_master','key_val'=>'id','key_disp'=>'state_name','rel_col_name'=>'country_id','rel_col_val'=>$currnet_val));
					}
					else
					{
						$elemt_array_state = array('relation'=>array('rel_table'=>'jobseeker_qualification_level','key_val'=>'id','key_disp'=>'qualification_name','rel_col_name'=>'','rel_col_val'=>''));
					}
					$data_array = $this->common_model->getRelationDropdown($elemt_array_state);					
					
					if(isset($data_array) && is_array($data_array) && count($data_array) > 0)
					{
						foreach($data_array as $key=>$val)
						{
							$opt_group_name = $val;
							$str_ddr .= "<optgroup label= \"".$val."\" data-optgrpid=\"".$key."\" class=\"".str_replace(' ','',$val)."\">";
							$elemt_array = array('relation'=>array('rel_table'=>$tabel_config['table_name'],'key_val'=>$tabel_config['pri_key'],'key_disp'=>$tabel_config['disp_clm'],'rel_col_name'=>$tabel_config['rel_clm_name'],'rel_col_val'=>$key));	
							$opt_array1 = array();
							$data_array = $this->common_model->getRelationDropdown($elemt_array);
							if(isset($data_array) && is_array($data_array) && count($data_array) > 0)
							{
								foreach($data_array as $key=>$val)
								{
									$selected_val_str ='';
									if(isset($selected_arr) && is_array($selected_arr) && count($selected_arr) > 0 && in_array($key,$selected_arr))
									{
										$selected_val_str =' selected ';
									}
									$str_ddr.= '<option '.$selected_val_str.' data-optval="'.$key.'" value="'.$key.'">'.$val.'</option>';
									$opt_array1[] = array('id'=>$key,'val'=>$val);
								}
							}
							$str_ddr .= "</optgroup>";
							if($this->tabel_config[$get_list]['table_name'] == 'city_master')
							{
							   $opt_array[] = array('state'=>$opt_group_name,'city'=>$opt_array1);
							}
							else
							{
								$opt_array[] = array('qualification_leval'=>$opt_group_name,'edu_name'=>$opt_array1);
							}
						}
					}
				}
				else
				{
					$data_array = $this->common_model->getRelationDropdown($elemt_array);
					$str_ddr = '<option value="">Select '.$tabel_config['label'].'</option>';
					if(isset($data_array) && is_array($data_array) && count($data_array) > 0)
					{
						foreach($data_array as $key=>$val)
						{
							$selected_val_str = '';
							if(isset($selected_arr) && is_array($selected_arr) && count($selected_arr) > 0 && in_array($key,$selected_arr) && ($this->tabel_config[$get_list]['table_name'] != 'currency_master' || $this->tabel_config[$get_list]['table_name'] != 'skill_level_master'))
							{
								$selected_val_str =' selected ';
							}
							elseif(isset($selected_arr) && is_array($selected_arr) && count($selected_arr) > 0 && in_array($val,$selected_arr) && ($this->tabel_config[$get_list]['table_name'] == 'currency_master' || $this->tabel_config[$get_list]['table_name'] == 'skill_level_master'))
							{
								$selected_val_str =' selected ';
							}
							if($this->tabel_config[$get_list]['table_name'] == 'currency_master'  || $this->tabel_config[$get_list]['table_name'] == 'skill_level_master')
							{ 
							  $str_ddr.= '<option '.$selected_val_str.' value="'.$val.'">'.$val.'</option>';
							}
							else
							{
								$str_ddr.= '<option '.$selected_val_str.' value="'.$key.'">'.$val.'</option>';
							}
							
							if($tocken_val  == 1)
							{
								$opt_array[] = array('value'=>$val,'lable'=>$val);
							}
							else
							{
								$opt_array[] = array('id'=>$key,'val'=>$val);
							}
						}
					}
				}
			}
		}
		if($retun_for == 'str')
		{
			return $str_ddr;
		}
		else
		{
		    if($tocken_val  == 1)
			{
				//array_splice($opt_array, 0, 1);
				array_splice($opt_array, 0, 0);
			}
			return $opt_array;
		}
	}
	public function get_list_multiple($get_list='',$return_opt='json',$currnet_val='',$retun_for = 'str',$selected_val ='',$tocken_val  =0,$singmult  ='single')
	{
		$this->tabel_config= array(
			'edu_list'=>array('table_name'=>'educational_qualification_master','pri_key'=>'id','disp_clm'=>'educational_qualification','label'=>$this->lang->line('qualification_lvl'),'rel_clm_name'=>'qualification_level_id'),
			'industries_master'=>array('table_name'=>'industries_master','val_clm'=>'id','pri_key'=>'id','disp_clm'=>'industries_name','label'=>$this->lang->line('type_industry'),'rel_clm_name'=>''),
			'functional_area_master'=>array('table_name'=>'functional_area_master','val_clm'=>'id','pri_key'=>'id','disp_clm'=>'functional_name','label'=>$this->lang->line('functional_area'),'rel_clm_name'=>''),
			'role_master'=>array('table_name'=>'role_master','val_clm'=>'id','disp_clm'=>'role_name','pri_key'=>'id','label'=>$this->lang->line('job_role'),'rel_clm_name'=>'functional_area'),
			'key_skill_master'=>array('table_name'=>'key_skill_master','val_clm'=>'key_skill_name','disp_clm'=>'key_skill_name','pri_key'=>'id','label'=>$this->lang->line('skill'),'rel_clm_name'=>''),
			'company_type_master'=>array('table_name'=>'company_type_master','val_clm'=>'id','disp_clm'=>'company_type','pri_key'=>'id','label'=>$this->lang->line('myprofile_emp_cmptypelbl'),'rel_clm_name'=>''),
			'company_size_master'=>array('table_name'=>'company_size_master','val_clm'=>'id','disp_clm'=>'company_size','pri_key'=>'id','label'=>$this->lang->line('myprofile_emp_cmpsjizelbl'),'rel_clm_name'=>''),
			'city_list'=>array('table_name'=>'city_master','pri_key'=>'id','disp_clm'=>'city_name','label'=>$this->lang->line('city'),'rel_clm_name'=>'country_id'),
			'job_type_list'=>array('table_name'=>'job_type_master','pri_key'=>'id','val_clm'=>'job_type','disp_clm'=>'job_type','rel_clm_name'=>'','label'=>'Job Type'),
		);
		$opt_array = array();
		if(isset($_REQUEST['user_agent']) && $_REQUEST['user_agent'] == 'NI-AAPP')
		{
			$opt_array[] = array('id'=>'','val'=>$this->lang->line('select_option'));
		}
		else
		{
			$opt_array[] = array('id'=>'0','val'=>$this->lang->line('select_option'));
		}
		
		if($this->input->post('get_list'))
		{
			$get_list = trim($this->input->post('get_list'));
		}
		if($this->input->post('multivar'))
		{
			$singmult = trim($this->input->post('multivar'));
		}
		if($this->input->post('retun_for'))
		{
			$retun_for = trim($this->input->post('retun_for'));
		}		
		if($this->input->post('tocken_val'))
		{
			$tocken_val = trim($this->input->post('tocken_val'));
		}
		if($this->input->post('currnet_val'))
		{
			if($singmult=='multi')
			{
				if($retun_for=='json')
				{
					if($this->input->post('currnet_val') && $this->input->post('currnet_val')!='')
					{
						$currnet_val = explode(',',$this->input->post('currnet_val'));		
					}
					else
					{
						$currnet_val = '';
					}
				}
				else
				{
					$currnet_val = $this->input->post('currnet_val');
				}
				
			}
			else
			{
				$currnet_val = trim($this->input->post('currnet_val'));
			}
		}
		
		if($get_list!='')
		{
			if(isset($this->tabel_config[$get_list]) && $this->tabel_config[$get_list] !='' && is_array($this->tabel_config[$get_list]) && count($this->tabel_config[$get_list]) > 0)
			{
				$tabel_config = $this->tabel_config[$get_list];
				$label_sele = $this->lang->line('select_option');
				if(isset($tabel_config['label']) && $tabel_config['label'] !='')
				{
					$label_sele = 'Select '.$tabel_config['label'];
				}
				if($tocken_val==0)
				{
					$str_ddr = '<option value="">'.$label_sele.'</option>';
				}
				$elemt_array = array('relation'=>array('rel_table'=>$tabel_config['table_name'],'key_val'=>$tabel_config['pri_key'],'key_disp'=>$tabel_config['disp_clm'],'rel_col_name'=>$tabel_config['rel_clm_name'],'rel_col_val'=>$currnet_val,'for_multiple_single_check'=>$singmult));
					if($selected_val!='')
					{
						$selected_val_array = explode(",",$selected_val);
					}
					else
					{
						$selected_val_array = array();
					}
					if($singmult=='multi')
					{	
						$data_array = array();
						if($tabel_config['table_name']=='role_master')
						{
							
							$data_array = $this->getdropdown_multi_sing_role($elemt_array,$selected_val_array,$retun_for,$tocken_val);
							
						}
						else
						{
							$data_array = $this->getdropdown_multi_sing($elemt_array);
						}
						
					}
					else
					{
						$data_array = $this->common_model->getRelationDropdown($elemt_array);
					}
					if($tocken_val==0)
					{
						$str_ddr = '<option value="">Select '.$tabel_config['label'].'</option>';
					}
					else
					{
						$str_ddr = '';
					}
					
					if($tabel_config['table_name'] != 'role_master')
					{
						if(isset($data_array) && is_array($data_array) && count($data_array) > 0)
						{
							foreach($data_array as $key=>$val)
							{
								$selected_val_str = '';
								if(isset($selected_val_array) && $selected_val_array!=''  && is_array($selected_val_array) && in_array($key,$selected_val_array))
								{
									$selected_val_str =' selected ';
								}
								
								$str_ddr.= '<option '.$selected_val_str.' value="'.$key.'">'.$val.'</option>';
								if($tocken_val  == 1)
								{
									$opt_array[] = array('value'=>$val,'lable'=>$val);
								}
								else
								{
									$opt_array[] = array('id'=>$key,'val'=>$val);
								}
							}
						}
					}// if conditon for role  master
					else // else conditon for role  master
					{
						if($retun_for=='json')
						{
							$opt_array = $data_array;	
						}
						else
						{
							$str_ddr.= $data_array;	
						}
						
					}
			}
		}
		
		
		if($retun_for == 'str')
		{
		    
			return $str_ddr;
			//echo $this->db->last_query();die;
		}
		else
		{
		    if($tocken_val  == 1)
			{
				array_splice($opt_array, 0, 1);
			}
			return $opt_array;
		}
		
		
	}
	function getdropdown_multi_sing($element_array_val)
	{
		$return_arr = '';
		$value_curr = $this->get_value_mult($element_array_val,'value','');
		$relation_arr = $this->get_value_mult($element_array_val,'relation','');
		if(isset($relation_arr) && $relation_arr !='' && is_array($relation_arr) && count($relation_arr) > 0)
		{
			if(isset($relation_arr['rel_table']) && $relation_arr['rel_table'] !='' && isset($relation_arr['key_val']) && $relation_arr['key_val'] !='' && isset($relation_arr['key_disp']) && $relation_arr['key_disp'] !='' )
			{
				$select_field = $relation_arr['key_disp'].', '.$relation_arr['key_val'];
				
				$where_close = array();
				if($value_curr !='')
				{
					$where_close[] = $relation_arr['key_val']." = '".$value_curr."' ";
				}
				$status_filed = 'status';
				$status_val = 'APPROVED';
				if(isset($relation_arr['status_filed']) && $relation_arr['status_filed'] !='')
				{
					$status_filed = $relation_arr['status_filed'];
				}
				if(isset($relation_arr['status_val']) && $relation_arr['status_val'] !='')
				{
					$status_val = $relation_arr['status_val'];
				}
				if($status_filed !='' && $status_val !='')
				{
					$where_close[] = $status_filed." = '".$status_val."' ";
				}
				if(isset($where_close) && $where_close !='' && count($where_close) > 0 )
				{
					$where_close_str = implode(" OR ",$where_close);
					$this->db->where(" ( $where_close_str ) ");
				}	
				if(isset($relation_arr['rel_col_name']) && $relation_arr['rel_col_name'] !='' && isset($relation_arr['rel_col_val']) && $relation_arr['rel_col_val'] !='' && is_array($relation_arr['rel_col_val']) && count($relation_arr['rel_col_val']) > 0)
				{
					//$this->db->where($relation_arr['rel_col_name'],$relation_arr['rel_col_val']);
					$this->db->where_in($relation_arr['rel_col_name'], $relation_arr['rel_col_val']);
					$row_data = $this->get_count_data_manual($relation_arr['rel_table'],"",2,$select_field,$relation_arr['key_disp'].' ASC ',0,'','');
				}
				$return_arr = array();
				if(isset($row_data) && $row_data !='' && is_array($row_data) && count($row_data) > 0)
				{
					foreach($row_data as $row_data_val)
					{
						$return_arr[$row_data_val[$relation_arr['key_val']]] = $row_data_val[$relation_arr['key_disp']];
					}
				}
			}
		}
		return $return_arr;
	}
	function getdropdown_multi_sing_role($element_array_val,$selectarr = array(),$retun_for='str',$tokenval=0)
	{
		$return_arr = '';
		
		$value_curr = $this->get_value_mult($element_array_val,'value','');
		$relation_arr = $this->get_value_mult($element_array_val,'relation','');
		
		if(isset($relation_arr) && $relation_arr !='' && is_array($relation_arr) && count($relation_arr) > 0)
		{
			if(isset($relation_arr['rel_table']) && $relation_arr['rel_table'] !='' && isset($relation_arr['key_val']) && $relation_arr['key_val'] !='' && isset($relation_arr['key_disp']) && $relation_arr['key_disp'] !='' )
			{
				$select_field = $relation_arr['key_disp'].', '.$relation_arr['key_val'];
				
				$where_close = array();
				if($value_curr !='')
				{
					$where_close[] = $relation_arr['key_val']." = '".$value_curr."' ";
				}
				$status_filed = 'status';
				$status_val = 'APPROVED';
				if(isset($relation_arr['status_filed']) && $relation_arr['status_filed'] !='')
				{
					$status_filed = $relation_arr['status_filed'];
				}
				if(isset($relation_arr['status_val']) && $relation_arr['status_val'] !='')
				{
					$status_val = $relation_arr['status_val'];
				}
				if($status_filed !='' && $status_val !='')
				{
					$where_close[] = $status_filed." = '".$status_val."' ";
				}
				if(isset($where_close) && $where_close !='' && is_array($where_close) && count($where_close) > 0 )
				{
					$where_close_str = implode(" OR ",$where_close);
					$this->db->where(" ( $where_close_str ) ");
				}	
				if(isset($relation_arr['rel_col_name']) && $relation_arr['rel_col_name'] !='' && isset($relation_arr['rel_col_val']) && $relation_arr['rel_col_val'] !='' && is_array($relation_arr['rel_col_val']) && count($relation_arr['rel_col_val']) > 0)
				{
					if($relation_arr['rel_table']=='role_master')
					{
						if($retun_for=='json')
						{
							$this->db->where_in('id', $relation_arr['rel_col_val']);
							$row_data = $this->get_count_data_manual('functional_area_master',"",2,'functional_name,id','id ASC ',0,'','');
						}
						else
						{
							$this->db->where_in('functional_area', $relation_arr['rel_col_val']);
							$row_data = $this->get_count_data_manual('fn_area_role_view',"",2,'functional_area,functional_name,id,role_name','id ASC ',0,'','');
						}
					}
					else
					{
						$this->db->where_in($relation_arr['rel_col_name'], $relation_arr['rel_col_val']);
						$row_data = $this->get_count_data_manual($relation_arr['rel_table'],"",2,$select_field,$relation_arr['key_disp'].' ASC ',0,'','');	
					}
				}
				
				/* new edit */
				if(isset($_REQUEST['user_agent']) && $_REQUEST['user_agent'] == 'NI-AAPP' && $relation_arr['rel_table']=='role_master' && $relation_arr['for_multiple_single_check']=='multi')
				{
					$return_arr = array();
				}/* new edit */
				else if($relation_arr['rel_table']=='role_master')
				{
					$return_arr ='';
					//$return_arr =array();
				}
				else
				{
					$return_arr = array();
				}
				if(isset($row_data) && $row_data !='' && is_array($row_data) && count($row_data) > 0)
				{
					if($relation_arr['rel_table']=='role_master')
					{
						$functional_area_storred = '';
						$custom_count = 1;
						$custom_array = array();
						$customacar  = '';
						$opt_array1 = array();													
						
						/**/
						if($retun_for=='json')
						{
							foreach($row_data as $row_data_val)
							{
								
								$functional_area_storred = $row_data_val['functional_name'];
								$this->db->where('functional_area', $row_data_val['id']);
								$opt_array1 = array();
								$row_data_inner = $this->get_count_data_manual('fn_area_role_view',"",2,'functional_name,id,role_name','id ASC ',0,'','');
								if(isset($row_data_inner) && $row_data_inner!='' && count($row_data_inner) > 0)
								{
									foreach($row_data_inner as $row_data_inner_sing)
									{
										$opt_array1[] = array('id'=>$row_data_inner_sing['id'],'val'=>$row_data_inner_sing['role_name']);
									}
								}
								$return_arr[] = array('functional_area'=>$functional_area_storred,'designation'=>$opt_array1);
							}
						}
						else
						{
							foreach($row_data as $row_data_val)
							{
								$selected_val_str = '';
								if(isset($selectarr) && $selectarr!=''  && is_array($selectarr) && in_array($row_data_val['id'],$selectarr))
								{
									$selected_val_str =' selected ';
								}
								
								if($functional_area_storred!=$row_data_val['functional_area'])
								{
									$return_arr .= '<optgroup label="'.$row_data_val['functional_name'].'">';
									$functional_area_storred = $row_data_val['functional_area'];
								}
									$return_arr .= '<option '.$selected_val_str.' value="'.$row_data_val['id'].'">'.$row_data_val['role_name'].'</option>';
								if($functional_area_storred!=$row_data_val['functional_area'])
								{
									$return_arr .= '</optgroup>';
								}							
							}	
						}
						/**/
					}
					else
					{
						foreach($row_data as $row_data_val)
						{
							$return_arr[$row_data_val[$relation_arr['key_val']]] = $row_data_val[$relation_arr['key_disp']];
						}
					}
				}
			}
		}
		return $return_arr;
	}
	function get_value_mult($element_array_val,$key='value',$defult='')
	{
		$value_curr = $defult;
		if(isset($element_array_val[$key]) && $element_array_val[$key] !='')
		{
			$value_curr = $element_array_val[$key];
		}
		return $value_curr;
	}
	public function get_m_status()
	{
	    $where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		return $this->get_count_data_manual('marital_status_master',$where_arra,2,'','','','',"");
	}
	public function get_salary()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		return $this->get_count_data_manual('salary_range',$where_arra,2,'','','','',"");
	}
	public function get_country_code($return = '')
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"country_code!=''");		
		$data_arr = $this->get_count_data_manual('country_master',$where_arra,2,'country_code,country_name','','','',"");
		if($return == '')
		{
			return $data_arr;
		}
		else
		{
			$opt_array = array();
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				$opt_array[] = array('id'=>'','val'=>$this->lang->line('select_option'));
				foreach($data_arr as $data_arr_val)
				{
					$opt_array[] = array('id'=>$data_arr_val['country_code'],'val'=>$data_arr_val['country_code'].' ('.$data_arr_val['country_name'].')');
				}
			}
			return $opt_array;
		}
	}
	public function get_personal_titles($return = '')
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"personal_titles!=''");		
		$data_arr = $this->get_count_data_manual('personal_titles_master',$where_arra,2,'personal_titles,id','','','','');
		if($return == '')
		{
			return $data_arr;
		}
		else
		{
			$opt_array = array();
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				$opt_array[] = array('id'=>'0','val'=>$this->lang->line('select_option'));
				foreach($data_arr as $data_arr_val)
				{
					$opt_array[] = array('id'=>$data_arr_val['id'],'val'=>$data_arr_val['personal_titles']);
				}
			}
			return $opt_array;
		}
	}
	
	public function get_qualification_name($qua_id)
	{
		$where_arra = array('id'=>$qua_id);
		$edu_name = $this->common_front_model->get_count_data_manual('educational_qualification_master',$where_arra,1,'educational_qualification','','','','');
		return $edu_name['educational_qualification'];
	}
	
	function display_exp_salary($exp = '' , $display_type='salary')
	{
		$explode_index1 = ' Lacs ';
		$explode_index2 = ' Thousand ';
		$to = 'To';
		if($display_type == 'experience' || $display_type == 'work_experience')
		{
			$explode_index1 = ' Year ';
			$explode_index2 =  'Month ';
		}
		
		if($exp !='')
		{
			if(strpos($exp,'-') == TRUE )
			{
				$exp_arr = explode('-',$exp);
				if(isset($exp_arr[0]) && $exp_arr[0] !='' && $exp_arr[0] !=0)
				{
					$ret_exp = $exp_arr[0].' '.$explode_index1;
				}
				if(isset($exp_arr[1]) && $exp_arr[1] !='' && $exp_arr[1] !=0)
				{
					if(isset($ret_exp) &&  $ret_exp !='')
					{
						if($display_type == 'work_experience')
						{
							$ret_exp.= $to .' '. $exp_arr[1].' '.$explode_index1; /*Concat with first index*/
						}
						else
						{
							$ret_exp.=$exp_arr[1].' '.$explode_index2; /*Concat with first index*/
						}
						
					}
					else
					{
						if($display_type == 'work_experience')
						{
							$ret_exp = $exp_arr[1].' '.$explode_index1;/*output of direct second index*/
						}
						else
						{
							$ret_exp = $exp_arr[1].' '.$explode_index2;/*output of direct second index*/
						}
						
					}
				}
				if(($display_type = 'experience' || $display_type = 'work_experience') && $exp_arr[1] == 0 && $exp_arr[0] == 0)
				{
					$ret_exp = ' Fresher ';
				}
			}
			else
			{
				$ret_exp = $exp;
			}
		}
		return $ret_exp;
	}
	function get_featured_list($table='',$id_filed='',$requiredfield='',$setlimit='')
	{	
		$returnvar = array();
		if($table!='' && $id_filed!='')
		{
			$requiredfield = $requiredfield!='' ? "`".$requiredfield."`" : $requiredfield ;
			$id_filed = "`".$id_filed."`" ;
			//$where_arra_id = $id_filed." = ".str_replace(",","','",$id)."";
			//$where_arra = array('status' => 'APPROVED',$where_arra_id);//$id_filed => $id,
			if($setlimit!='')
			{
				$limitvar = $setlimit;
			}
			else
			{
				$limitvar = '';
			}
			if($table=='industries_master')
			{
				
				$where_arra_id = "icon_name is NOT NULL";
				$where_arra = array('status' => 'APPROVED','is_deleted' => 'No','is_featured' => 'Yes','icon_name !=' => '',$where_arra_id);
			}
			elseif($table=='blog_master')
			{
				$where_arra = array('status' => 'APPROVED','is_deleted' => 'No');
			}
			else
			{
				$where_arra = array('status' => 'APPROVED');
			}
			$list_count = $this->get_count_data_manual($table,$where_arra,'0',$requiredfield,$id_filed.'','','','');
			if(isset($list_count) && $list_count > 0 )
			{
				if($table=='blog_master')
				{
					$resultstored = $this->get_count_data_manual($table,$where_arra,2,$requiredfield,'created_on desc','1',$limitvar,'');
				}
				else
				{
					$resultstored = $this->get_count_data_manual($table,$where_arra,2,$requiredfield,$id_filed.' desc','1',$limitvar,'');
				}
				if($requiredfield!='' && is_array($resultstored) && count($resultstored) > 0)
				{
					foreach($resultstored as $resultstored_sing)
					{
						$returnvar[] = $resultstored_sing;
					}
				}
				else
				{
					foreach($resultstored as $resultstored_sing)
					{
						$returnvar[] = $resultstored_sing;
					}
				}
				
			}
		}
		return $returnvar;
	}
	// for android App job name
	function get_job_education_name($job_education)
	{ 
		$j_name_array = array();
		$j_name_list = '';
		$whare = "id in ($job_education)";
		$job_education = $this->common_front_model->get_count_data_manual('educational_qualification_master',$whare,2,'educational_qualification','','','','');
		
		if(isset($job_education) && $job_education !='' && is_array($job_education) && count($job_education) > 0)
		{
			foreach($job_education as $j_name)
			{
				$j_name_array[] =$j_name['educational_qualification'];
			}
			$j_name_list = implode(', ',$j_name_array);
		}
		if($j_name_list =='')
		{
			$j_name_list = $this->common_model->data_not_availabel;
		}
		return $j_name_list;
	}
	function get_key_skill_name($skill_id_array)
	{ 
		$ks_name_array = array();
		$ks_name_list = '';
		$whare = "id in ($skill_id_array)";
		$skill_id_array_1 = $this->common_front_model->get_count_data_manual('key_skill_master',$whare,2,'key_skill_name','','','','');
		
		if(isset($skill_id_array_1) && $skill_id_array_1 !='' && is_array($skill_id_array_1) && count($skill_id_array_1) > 0)
		{
			foreach($skill_id_array_1 as $ks_name)
			{
				$ks_name_array[] =$ks_name['key_skill_name'];
			}
			$ks_name_list = implode(', ',$ks_name_array);
		}
		if($ks_name_list =='')
		{
			$ks_name_list = $this->common_model->data_not_availabel;
		}
		return $ks_name_list ;
	}
	// for android App job name
	function get_location_hiring_name($location_hiring)
	{ 
		$l_name_array = array();
		$l_name_list = '';
		$whare = "id in ($location_hiring)";
		$location_name = $this->common_front_model->get_count_data_manual('city_master',$whare,2,'city_name','','','','');
		if(isset($location_name) && $location_name !='' && is_array($location_name) && count($location_name) > 0)
		{
			foreach($location_name as $l_name)
			{
				$l_name_array[] =$l_name['city_name'];
			}
			$l_name_list = implode(', ',$l_name_array);
		}
		if($l_name_list =='')
		{
			$l_name_list = $this->common_model->data_not_availabel;
		}
		return $l_name_list;
	}
	
	function valueFromId($table_name='',$arry_id='',$clm_value='',$id_clm='id',$return_type = 'str',$delimetor=',')
	{
		$return_arr ='';
		if($table_name !='' && $arry_id !='' && $clm_value !='' && $id_clm !='')
		{
			if(!is_array($arry_id))
			{
				$arry_id = explode($delimetor,$arry_id);
			}
			$this->db->where_in($id_clm,$arry_id);
			$data_arr = $this->get_count_data_manual($table_name,'',2,$clm_value);
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				$temp_arr = array();
				foreach($data_arr as $data_arr_val)
				{
					$temp_arr[] = $data_arr_val[$clm_value];
				}
				if($return_type =='str')
				{
					$return_arr = implode(', ',$temp_arr);
				}
				else
				{
					$return_arr = $temp_arr;
				}
			}
		}
		return $return_arr;
	}
	public function get_employer_listselect2()
	{
		$search = $this->input->post('q');
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No'," (email like '%$search%' or fullname like '%$search%' or company_name like '%$search%') ");
		$data_arr = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,2,'fullname,id,company_name','','1',25,"");
		$opt_array['results'] = array();
		if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
		{
			foreach($data_arr as $data_arr_val)
			{
				$forpushingarray = array("id"=>$data_arr_val['id'],"text"=>$data_arr_val['fullname'].'('.$data_arr_val['company_name'].')');
				array_push($opt_array['results'],$forpushingarray);
			}
		}
		$opt_array['more'] = "false";
		return $opt_array;
	}
	public function get_jobseeker_listselect2()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','profile_visibility'=>'Yes');
		if($this->input->post('q'))
		{
			$search = $this->input->post('q');
			if($search !='')
			{
				$where_arra[] = " (email like '%$search%' or fullname like '%$search%') ";
			}
		}
		$data_arr = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,2,'fullname,id,email','','1',25,"");
		$opt_array['results'] = array();
		if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
		{
			foreach($data_arr as $data_arr_val)
			{
				if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
				{
					if(isset($data_arr_val['email']) && $data_arr_val['email'] !='')
					{
						$data_arr_val['email'] = $this->common_model->email_disp;
					}
				}
				$forpushingarray = array("id"=>$data_arr_val['id'],"text"=>$data_arr_val['fullname'].'('.$data_arr_val['email'].')');
				array_push($opt_array['results'],$forpushingarray);
			}
		}
		$opt_array['more'] = "false";
		return $opt_array;
	}
	function get_plan_detail($user_id='',$user_type='',$return_filed='')
	{
		$return_data = 'No';
		if($user_id !='' && ($user_type =='job_seeker' || $user_type =='employer'))
		{
			$table_name = 'plan_jobseeker';
			$where_data = array('js_id'=>$user_id,'current_plan'=>'Yes','is_deleted'=>'No');
			if($user_type =='employer')
			{
				$table_name = 'plan_employer';
				$where_data = array('emp_id'=>$user_id,'current_plan'=>'Yes','is_deleted'=>'No');
			}
			$today_date = $this->getCurrentDate('Y-m-d');
			$where_data[] = " expired_on >= '$today_date' ";
			$plan_data = $this->common_model->get_count_data_manual($table_name,$where_data,1,' * ','',0,'',0);
			
			if(isset($plan_data) && $plan_data !='' && is_array($plan_data) && count($plan_data) > 0)
			{
				if($return_filed !='' && isset($plan_data[$return_filed]) && $plan_data[$return_filed] !='')
				{
					$return_data = $plan_data[$return_filed];
					if($return_data !='Yes' && $return_data !='No')
					{
						if($plan_data[$return_filed] > $plan_data[$return_filed.'_used'])
						{
							$return_data = 'Yes';
						}
					}
					$plan_data[$return_filed];
				}
				else
				{
					$return_data = $plan_data;
				}
			}
		}
		return $return_data;
	}
	function update_plan_detail($user_id='',$user_type='',$return_filed='')
	{
		if($user_id !='' && ($user_type =='job_seeker' || $user_type =='employer'))
		{
			$table_name = 'plan_jobseeker';
			$where_data = array('js_id'=>$user_id,'current_plan'=>'Yes','is_deleted'=>'No');
			if($user_type =='employer')
			{
				$table_name = 'plan_employer';
				$where_data = array('emp_id'=>$user_id,'current_plan'=>'Yes','is_deleted'=>'No');
			}
			$column_updated = $return_filed.'_used';
			$data_array = array('is_deleted'=>'No');
			$this->db->set($column_updated , " $column_updated + 1", FALSE);
			$this->update_insert_data_common($table_name,$data_array,$where_data,1,1);
		}
	}
	function check_for_plan_update($user_id='',$user_type='',$empid_jsid='',$resumetype='')
	{
		$update_on = $this->common_front_model->getCurrentDate();
		if($user_id !='' && ($user_type =='job_seeker' || $user_type =='employer') && $empid_jsid!='')
		{
			if($user_type =='job_seeker')
			{
				$where_arra_contact = array('js_id'=>$user_id,'employer_id'=>$empid_jsid);
				$get_count_contact = $this->common_front_model->get_count_data_manual('jobseeker_viewed_employer_contact',$where_arra_contact,0,'id','','','','');
				if($get_count_contact=='' || $get_count_contact=='0')
				{
					/* for updating plan details*/
					$this->common_front_model->update_plan_detail($user_id,'job_seeker','contacts');
					/* for updating plan details end */
					/* for inserting that this company details is viewed */
					$data_insert_contact = array('last_viewed_on'=>$update_on,'js_id'=>$user_id,'employer_id'=>$empid_jsid);
					$this->common_front_model->update_insert_data_common('jobseeker_viewed_employer_contact',$data_insert_contact,'',0);
					/* for inserting that this job is viewed end */
				}
				else
				{ 
					/* for updating that this company details is already viewed */
					$data_update_contact = array('last_viewed_on'=>$update_on);
					$this->common_front_model->update_insert_data_common('jobseeker_viewed_employer_contact',$data_update_contact,$where_arra_contact);
					/* for updating that this company details is already viewed end */
				}
			}
			elseif($user_type =='employer')
			{
				
				$where_arra_contact = array('emp_id'=>$user_id,'jobseeker_id'=>$empid_jsid);
				if($resumetype=='resume')
				{
					
					$get_count_contact = $this->common_front_model->get_count_data_manual('employer_down_js_resume',$where_arra_contact,0,'id','','','','');	
				}
				else
				{
					$get_count_contact = $this->common_front_model->get_count_data_manual('employer_viewed_js_contact',$where_arra_contact,0,'id','','','','');
				}
				
				if($get_count_contact=='' || $get_count_contact=='0')
				{ 
					/* for updating plan details*/
					if($resumetype=='resume')
					{
						$this->common_front_model->update_plan_detail($user_id,'employer','cv_access_limit');	
					}
					else
					{
						$this->common_front_model->update_plan_detail($user_id,'employer','contacts');
					}
					/* for updating plan details end */
					/* for inserting that this company details is viewed */
					if($resumetype=='resume')
					{
						$data_insert_contact = array('last_dwn_on'=>$update_on,'emp_id'=>$user_id,'jobseeker_id'=>$empid_jsid);
						$this->common_front_model->update_insert_data_common('employer_down_js_resume',$data_insert_contact,'',0);
					}
					else
					{
						$data_insert_contact = array('last_viewed_on'=>$update_on,'emp_id'=>$user_id,'jobseeker_id'=>$empid_jsid);
						$this->common_front_model->update_insert_data_common('employer_viewed_js_contact',$data_insert_contact,'',0);
					}
					
					/* for inserting that this job is viewed end */
				}
				else
				{ 
					/* for updating that this company details is already viewed */
					if($resumetype=='resume')
					{
						$data_update_contact = array('last_dwn_on'=>$update_on);
						$this->common_front_model->update_insert_data_common('employer_down_js_resume',$data_update_contact,$where_arra_contact);
					}
					else
					{
						$data_update_contact = array('last_viewed_on'=>$update_on);
						$this->common_front_model->update_insert_data_common('employer_viewed_js_contact',$data_update_contact,$where_arra_contact);
					}
					
					/* for updating that this company details is already viewed end */
				}
			}
		}
	}
	function check_for_plan_update_jobs($user_id='')
	{
		$update_on = $this->common_front_model->getCurrentDate();
		if($user_id !='')
		{
			$where_arra_job = array('emp_id'=>$user_id,'jobseeker_id'=>$empid_jsid);
			$get_count_contact = $this->common_front_model->get_count_data_manual('employer_viewed_js_contact',$where_arra_job,0,'id','','','','');				
			if($get_count_contact=='' || $get_count_contact=='0')
			{ 
				/* for updating plan details*/
					$this->common_front_model->update_plan_detail($user_id,'employer','contacts');
				/* for updating plan details end */
				/* for inserting that this company details is viewed */
					$data_insert_contact = array('last_viewed_on'=>$update_on,'emp_id'=>$user_id,'jobseeker_id'=>$empid_jsid);
					$this->common_front_model->update_insert_data_common('employer_viewed_js_contact',$data_insert_contact,'',0);
				/* for inserting that this job is viewed end */
			}
			else
			{ 
				/* for updating that this company details is already viewed */
					$data_update_contact = array('last_viewed_on'=>$update_on);
					$this->common_front_model->update_insert_data_common('employer_viewed_js_contact',$data_update_contact,$where_arra_contact);
				/* for updating that this company details is already viewed end */
			}
		}
	}
	
	function get_counter($counter_name,$counter_id)
	{
		/*
		$counter_name = which action counter you wan't to get. like job viewed , liked , block , employer like , follower also number of job application
		
		$counter_id = for job related counter pass job_id , for employer related counter pass emp_id 
	
		*/
		
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB';
		if($counter_id!='')
		{
		   $array_get_count = array(
			'viewed_job'=>array('tbl_name'=>'jobseeker_viewed_jobs','where'=>"job_id = '".$counter_id."'"),
			'liked_job'=>array('tbl_name'=>'jobseeker_viewed_jobs','where'=>"job_id = '".$counter_id."' and is_liked = 'Yes'"),
			'saved_job'=>array('tbl_name'=>'jobseeker_viewed_jobs','where'=>"job_id = '".$counter_id."' and is_saved = 'Yes'"),
			'applied_job'=>array('tbl_name'=>' job_application_history','where'=>"job_id = '".$counter_id."' "));
		}
		if($counter_name!= '' && $counter_id!='')
		{
			$get_count = $this->common_front_model->get_count_data_manual($array_get_count[$counter_name]['tbl_name'],$array_get_count[$counter_name]['where'],0);
		}
		else if($counter_id!='' && $user_agent !='' && $user_agent !='NI-WEB')
		{
			$viewed_job = $this->common_front_model->get_count_data_manual($array_get_count['viewed_job']['tbl_name'],$array_get_count['viewed_job']['where'],0);
			$liked_job = $this->common_front_model->get_count_data_manual($array_get_count['liked_job']['tbl_name'],$array_get_count['liked_job']['where'],0);
			$saved_job = $this->common_front_model->get_count_data_manual($array_get_count['saved_job']['tbl_name'],$array_get_count['saved_job']['where'],0);
			$applied_job = $this->common_front_model->get_count_data_manual($array_get_count['applied_job']['tbl_name'],$array_get_count['applied_job']['where'],0);
		}
		else
		{
			$get_count = 0;
		}
		if($user_agent =='NI-WEB')
		{
			return $get_count;
		}
		else
		{
			$data['viewed_job'] = $viewed_job; 
			$data['liked_job'] = $liked_job; 
			$data['saved_job'] = $saved_job; 
			$data['applied_job'] = $applied_job;
			return $data; 
		}
	}
	function check_job_seeker_action()
	{
			$js_id = $this->input->post('js_id');
			$job_id = $this->input->post('job_id');
			$emp_id = $this->input->post('emp_id');
		
		    $where_arra = array('js_id'=>$js_id,'job_id'=>$job_id);
			$jobseeker_viewed_jobs = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,1);
			$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$emp_id);
			$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
			$check_already_apply = $this->common_front_model->get_count_data_manual('job_application_history',$where_arra,1,'','','',1);
			
			$data['is_saved'] = 'No';
			$data['is_liked'] = 'No';
			$data['is_block'] = 'No';
			$data['is_follow'] = 'No';
			$data['is_liked_emp'] = 'No';
			$data['is_apply'] = 'No';
			
			if(isset($jobseeker_viewed_jobs) && $jobseeker_viewed_jobs!='' && is_array($jobseeker_viewed_jobs) && count($jobseeker_viewed_jobs) > 0)
			{
				if($jobseeker_viewed_jobs['is_saved']=='Yes')
				{
					$data['is_saved'] = 'Yes';
				}
				if($jobseeker_viewed_jobs['is_liked']=='Yes')
				{
					$data['is_liked'] = 'Yes';
				}
			}
			
			if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) &&  count($jobseeker_access_employer) > 0)
			{
				if($jobseeker_access_employer['is_block']=='Yes')
				{
					$data['is_block'] = 'Yes';
				}
				if($jobseeker_access_employer['is_follow']=='Yes')
				{
					$data['is_follow'] = 'Yes';
				}
				if($jobseeker_access_employer['is_liked']=='Yes')
				{
					$data['is_liked_emp'] = 'Yes';
				}
			}
			
			if(isset($check_already_apply) && $check_already_apply!='' && is_array($check_already_apply) && count($check_already_apply) > 0)
			{
				$data['is_apply'] = 'Yes';
			}
			return $data;
	}
	
	function recent_view_job()
	{
		$js_id = $this->common_front_model->get_userid();
		$where_array = "js_id = '".$js_id."'"; 
	    $old_view_job_list = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_array,2,'','update_on desc','',10);
		
		if($old_view_job_list!='' && is_array($old_view_job_list) && count($old_view_job_list) > 0)
		{
			
			foreach($old_view_job_list as $old_view_job)
			{
				$old_view_job_list_arr[] = $old_view_job['job_id'];
			}
			$old_view_job_list = implode(',',$old_view_job_list_arr);
			
			$where_arra = "id IN (".$old_view_job_list.") AND is_deleted='No'";
		    $recent_view_job_list = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,'','',1,10);
		    $response['status'] = 'success';
			$response['view_job_list'] = $recent_view_job_list ;
		}
		else
		{
			$response['status'] = 'error';
			$response['view_job_list'] = '0';
		}
		return $response;
	}
	public function base_64_photo($post_name='',$dir_name='',$photo_name='')
	{
	    if($post_name != '' && $dir_name !='' && $photo_name !='')
	    {    
            if(isset($_REQUEST[$post_name]) && $_REQUEST[$post_name] !='')
            {
				$photo_base_64 = $_REQUEST[$post_name];
                $photo_base_64 = str_replace('data:image/jpeg;base64,', '', $photo_base_64);
				$photo_base_64 = str_replace('data:image/png;base64,', '', $photo_base_64);
                $photo_base_64 = str_replace(' ', '+', $photo_base_64);
                $data = base64_decode($photo_base_64);
                $path_folder = $this->$dir_name;
                $path_folder.$photo_name;
                $success = file_put_contents($path_folder.$photo_name, $data);
            }
        }
	}
	public function check_duplicate()
	{
		//$id = '';
		$mode = '';
		$field_value = '';
		$field_name = '';
		$check_on = '';
		/*if(isset($_REQUEST['id']) && $_REQUEST['id'] !='')
		{
			$id = $_REQUEST['id'];
		}*/
		if(isset($_REQUEST['mode']) && $_REQUEST['mode'] !='')
		{
			$mode = $_REQUEST['mode'];
		}
		if(isset($_REQUEST['field_value']) && $_REQUEST['field_value'] !='')
		{
			$field_value = $_REQUEST['field_value'];
		}
		if(isset($_REQUEST['field_name']) && $_REQUEST['field_name'] !='')
		{
			$field_name = $_REQUEST['field_name'];
		}
		if(isset($_REQUEST['check_on']) && $_REQUEST['check_on'] !='')
		{
			$check_on = $_REQUEST['check_on'];
		}
		if($check_on !='' && $field_name !='' && $field_value !='' && $mode !='')
		{
			$where_data = array();
			$array_table_field = $this->db->list_fields($check_on);
		
			if(in_array('is_deleted',$array_table_field))
			{
				$where_data = array('is_deleted'=>'No');
			}
			/*if($mode =='edit' && $id != '' )
			{
				$where_data[] = " id != '".$id."' ";
			}*/
			if($field_value !='')
			{
				$where_data[$field_name] = $field_value;
			}
			$count_duplicate = $this->common_front_model->get_count_data_manual($check_on,$where_data,0,'id');
			if(isset($count_duplicate) && $count_duplicate > 0)
			{
				return 'success';
			}
		}
		return 'error';
	}
	public function get_suggestion_list($search_get,$get_list_get='')
	{
		$search = $this->input->post('q');
		$list_id = $this->input->post('list_id');
		$type = $this->input->post('type');
		$nstatus = $this->input->post('nstatus');
		
			$where = " (email like '%$search%' or fullname like '%$search%' or mobile like '%$search%')";
			if($get_list_get =='member_list_email')
			{
				$where.= " and email!='' ";
			}
			if($type=='Job seeker')
			{
				$tbl_name = 'jobseeker';
			}
			else if($type=='Employer')
			{
				$tbl_name = 'employer_master';
			}	
			//$tbl_name = 'register';
			$select = 'id,email,mobile,fullname';
			$display_field = 'fullname';
			
			if($nstatus=='Active')
			{
				$where_arra = array('is_deleted'=>'No',$where,'status'=>'APPROVED');
				
			}else if($nstatus=='Inactive')
			{
				$where_arra = array('is_deleted'=>'No',$where,'status'=>'UNAPPROVED');
				
			}else if($nstatus=='Paid')
			{
				$where_arra = array('is_deleted'=>'No',$where,'status'=>'APPROVED','plan_status'=>'Paid');
			
			}else{
				$where_arra = array('is_deleted'=>'No',$where);
			}
			$return = 'email';
			if($get_list_get =='member_list_email')
			{
				$return = 'email';
				$this->db->group_by('email');
			}
			
			$data_arr = $this->common_model->get_count_data_manual($tbl_name,$where_arra,2,$select,'','1',25,"");
		
			$opt_array['results'] = array();
			$tocken_array = array();
			$app_array = array();
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
					{
						$forpushingarray = array("id"=>$this->common_model->email_disp,"text"=>$data_arr_val['fullname'].'  ('.$this->common_model->email_disp.')');
					}
					else
					{
						$forpushingarray = array("id"=>$data_arr_val[$return],"text"=>$data_arr_val['fullname'].'  ('.$data_arr_val[$return].')');
					}
					
					array_push($opt_array['results'],$forpushingarray);					
				}
			}
		
		return $opt_array;
	}
}

/**/

