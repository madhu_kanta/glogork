<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Employer_sign_up extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function add_employer_detail()
	{
		$datavar = array();
		$this->load->library('encryption');
		$this->load->library('phpass');
		$password = $this->input->post('pass');
		$emailid_confirmation = $this->input->post('emailid_confirmation');
		$hashed_pass = $this->phpass->hash($password);
		$reg_date = $this->common_front_model->getCurrentDate(); // must save date and time on datetime field
		$data_array_custom = array('password'=> $hashed_pass,'register_date'=>$reg_date,'email'=>$emailid_confirmation);
		/* save the data */
		if($this->session->has_userdata('employer_reg'))
		{   
			$allsessionstoredforreg = $this->session->userdata('employer_reg');
			$sessionstoredforreg = end($allsessionstoredforreg);
			$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$sessionstoredforreg['emp_reg_sess_id'],'',1,'is_deleted',1);
				$last_inserted = $sessionstoredforreg['emp_reg_sess_id'];
			}
			else
			{
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','add','','','1','1','is_deleted',2);
				$last_inserted = $this->db->insert_id();	
			}
		}
		else
		{
			$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','add','','','1','1','is_deleted',2);
			$last_inserted = $this->db->insert_id();
		}
		/* save th data end */
		if(isset($last_inserted) && $last_inserted!='' && !is_null($last_inserted))
		{
			$datavar['result'] = "success";
			if($this->input->post('user_agent') && ($this->input->post('user_agent')=='NI-IAPP' || $this->input->post('user_agent')=='NI-AAPP'))
			{
				$datavar['employer_id_inserted'] = $last_inserted;
			}
			
			$step = (isset($sessionstoredforreg) && isset($sessionstoredforreg['step']) && $sessionstoredforreg['step'] > '2') ? $sessionstoredforreg['step'] : '2';
			$newreg = array('emp_reg_sess_id' => $last_inserted,'emp_reg_sess' => $emailid_confirmation,'step' => $step);
			if($this->session->has_userdata('employer_reg'))
			{
				$old_session =  $this->session->userdata('employer_reg');
			}
			else
			{
				$old_session =  array();
			}
			array_push($old_session, $newreg);
			$this->session->set_userdata('employer_reg', $old_session);
			$datavar['stepnoreturn']= $step;
		}
		else
		{
			$datavar['result']="error";
		}
		return $datavar;
	}
	
	public function add_employer_detail2()
	{
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";	
		$datavar = array();
		
		/* save the data */
		if($this->session->has_userdata('employer_reg') || ($this->input->post('user_id') && $this->input->post('user_id')!=''))
		{
				
			if($this->input->post('user_id') && $this->input->post('user_id')!='')
			{
				$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_id($this->input->post('user_id'));
			}
			else
			{
				$allsessionstoredforreg = $this->session->userdata('employer_reg');
				$sessionstoredforreg = end($allsessionstoredforreg);
				$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);	
			}
		
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$mobile_code = $this->input->post('mobile_c_code');
				$mobile_coming = $this->input->post('mobile_num');
				/*echo '<pre>';

				print_r($_REQUEST['']);
				echo '</pre>';*/
				$mobile_update = $mobile_code.'-'.$mobile_coming;
				//$_REQUEST['mobile'] = $mobile_update;
				$data_array_custom = array('mobile'=>$mobile_update);
				
				if(isset($_FILES['company_logo']) && $_FILES['company_logo'])
				{
					$_REQUEST['company_logo_path'] = $this->common_front_model->fileuploadpaths('company_logos',1);
					$_REQUEST['company_logo_ext'] = 'jpg|jpeg|png|gif';
					$upload_path = $this->common_front_model->fileuploadpaths('company_logos',1);
					if(file_exists($upload_path.'/'.$checkifmemberexists['company_logo']))
					{
						$company_logo_var = $upload_path.'/'.$checkifmemberexists['company_logo'];
						$_REQUEST['company_logo_var'] = $company_logo_var;
					}
				}
				else if(isset($_POST['company_logo']) && $_POST['company_logo']!='')
				{
						$company_logo_name = 'company_logo';
						$upload_company_logo_name = time().'-'.$checkifmemberexists['id'].'.jpg';
						$this->common_front_model->base_64_photo($company_logo_name,'path_company_logo',$upload_company_logo_name);
						$_REQUEST['company_logo'] = $upload_company_logo_name;
						$_REQUEST['company_logo_approval'] = 'UNAPPROVED';
						
						$customvar = array('company_logo'=>$upload_company_logo_name,'company_logo_approval'=>'UNAPPROVED');
						$response = $this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$checkifmemberexists['id']);
						
						if($response && $response=='success')
						{
							if(file_exists($this->common_front_model->path_company_logo.$checkifmemberexists['company_logo']))
							{
								 $delete_company_logo = $this->common_front_model->path_company_logo.$checkifmemberexists['company_logo'];
								if(isset($delete_company_logo) && $delete_company_logo!='')
								{
									$this->common_front_model->delete_file($delete_company_logo);
								}
							}
						}
				}
				//print_r($_REQUEST);
				//$data_array_custom = array();
				$step = (isset($sessionstoredforreg) && isset($sessionstoredforreg['step']) && $sessionstoredforreg['step'] > '3') ? $sessionstoredforreg['step'] : '3';
				
				if($this->input->post('user_id') && $this->input->post('user_id')!='')
				{
					//echo '2222';
					$return_value_error = $this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
					$datavar['employer_id_inserted'] = $checkifmemberexists['id'];
				}
				else
				{	
					//echo 'dddd';
					//error_reporting(E_ALL);
					$return_value_error = $this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$sessionstoredforreg['emp_reg_sess_id'],'',1,'is_deleted',1);
					$newreg = array('emp_reg_sess_id' => $sessionstoredforreg['emp_reg_sess_id'],'emp_reg_sess' => $sessionstoredforreg['emp_reg_sess'],'step' => $step);
					$old_session =  $this->session->userdata('employer_reg');
					$removed_item_from_session =  array_pop($old_session);
					array_push($old_session, $newreg);
					$this->session->set_userdata('employer_reg', $old_session);
				}
				
				
				/*$company_logo = (isset($_FILES['company_logo']) && $_FILES['company_logo']!='' && count($_FILES['company_logo']) > 0) ? $_FILES['company_logo'] : "";
				$upload_path = $this->common_front_model->fileuploadpaths('company_logos',1);
				if($company_logo!='' && !empty($company_logo))
				{
					$allowed_file_type ='jpg|jpeg|png';	
					$final_param = array('file_name'=>'company_logo','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type);
					$retrun_mes = $this->common_front_model->upload_file($final_param);
					if($retrun_mes['status']=='success')
					{
						if(file_exists($upload_path.'/'.$checkifmemberexists['company_logo']))
						{
							$this->common_front_model->delete_file($upload_path.'/'.$checkifmemberexists['company_logo']);
						}
						$customvar = array('company_logo'=>$retrun_mes['file_data']['file_name'],'company_logo_approval'=>'UNAPPROVED'); 
						$this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',2);
					}
				}
				*/
				//echo $return_value_error;
				if($return_value_error=='success')
				{
					$datavar['result']="success";
				}
				elseif($return_value_error=='file_upload_error')
				{
					$datavar['result']="file_upload_error";	
				}
				else
				{
					$datavar['result']="error";	
				}
				
				$datavar['stepnoreturn']= $step;
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function add_employer_detail3()
	{
		$datavar = array();
		/* save the data */
		if($this->session->has_userdata('employer_reg') || ($this->input->post('user_id') && $this->input->post('user_id')!=''))
		{
			//$currentdesignation = $this->input->post('currentdesignation');
			//$join_month = $this->input->post('join_month');
			//$join_year = $this->input->post('join_year');
			//$join_in = $join_month.' ,'.$join_year;
			if($this->input->post('user_agent')!='NI-AAPP' && $this->input->post('user_agent')!='NI-IAPP')
			{
				$currentdesignation = (count($this->input->post('currentdesignation')) > 0 && $this->input->post('currentdesignation')!='' && is_array($this->input->post('currentdesignation'))) ? implode(",",$this->input->post('currentdesignation')) : "";
				$functional_area = (count($this->input->post('functional_area')) > 0 && $this->input->post('functional_area')!='' && is_array($this->input->post('functional_area'))) ? implode(",",$this->input->post('functional_area')) : "";
				$industry_hire = (count($this->input->post('industry_hire')) > 0 && $this->input->post('industry_hire')!='' && is_array($this->input->post('industry_hire'))) ? implode(",",$this->input->post('industry_hire')) : "";
				$function_area_hire = (count($this->input->post('function_area_hire')) > 0 && $this->input->post('function_area_hire')!='' && is_array($this->input->post('function_area_hire'))) ? implode(",",$this->input->post('function_area_hire')) : "";
				$skill_hire = (count($this->input->post('skill_hire')) > 0 && $this->input->post('skill_hire')!='' && is_array($this->input->post('skill_hire'))) ? implode(",",$this->input->post('skill_hire')) : "";	
			}
			else
			{
				$currentdesignation = $this->input->post('currentdesignation');
				$functional_area = $this->input->post('functional_area');
				$industry_hire = $this->input->post('industry_hire');
				$function_area_hire = $this->input->post('function_area_hire');
				$skill_hire = $this->input->post('skill_hire');
			}
			if($this->input->post('user_id') && $this->input->post('user_id')!='')
			{
				$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_id($this->input->post('user_id'));
				$datavar['employer_id_inserted'] = $checkifmemberexists['id'];
			}
			else
			{
				$allsessionstoredforreg = $this->session->userdata('employer_reg');
				$sessionstoredforreg = end($allsessionstoredforreg);
				$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
			}
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				//$data_array_custom = array('designation'=> $currentdesignation,'join_in'=>$join_in);
				$data_array_custom = array('designation'=> $currentdesignation,'functional_area'=> $functional_area,'industry_hire'=>$industry_hire,'function_area_hire'=>$function_area_hire,'skill_hire'=> $skill_hire);
				$step = (isset($sessionstoredforreg) && isset($sessionstoredforreg['step']) && $sessionstoredforreg['step'] > '4') ? $sessionstoredforreg['step'] : '4';
				if($this->input->post('user_id') && $this->input->post('user_id')!='')
				{
					$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				}
				else
				{
					$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$sessionstoredforreg['emp_reg_sess_id'],'',1,'is_deleted',1);
					$newreg = array('emp_reg_sess_id' => $sessionstoredforreg['emp_reg_sess_id'],'emp_reg_sess' => $sessionstoredforreg['emp_reg_sess'],'step' => $step);
					$old_session =  $this->session->userdata('employer_reg');
					$removed_item_from_session =  array_pop($old_session);
					array_push($old_session, $newreg);
					$this->session->set_userdata('employer_reg', $old_session);
				}
				
				$datavar['result']="success";
				$datavar['stepnoreturn']= $step;
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	
	public function add_employer_detail4()
	{
		
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		
		$datavar = array();
		/* save the data */
	 if($this->session->has_userdata('employer_reg') || ($this->input->post('user_id') && $this->input->post('user_id')!=''))
		{
			if($this->input->post('user_id') && $this->input->post('user_id')!='')
			{
				$checkifmemberexists = $this->employer_sign_up->get_employerdetail_frm_id($this->input->post('user_id'));
				$datavar['employer_id_inserted'] = $checkifmemberexists['id'];
			}
			else
			{
				$allsessionstoredforreg = $this->session->userdata('employer_reg');
				$sessionstoredforreg = end($allsessionstoredforreg);
				$checkifmemberexists = $this->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
			}
			
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				if(isset($_FILES['profile_pic']) && $_FILES['profile_pic']!='')
				{ 
						$allowed_file_type ='jpg|jpeg|png|gif';
						$upload_path = $this->common_front_model->fileuploadpaths('emp_photos',1);
						$final_param = array('file_name'=>'profile_pic','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type);
						$retrun_mes = $this->common_front_model->upload_file($final_param);
						$step = (isset($sessionstoredforreg) && isset($sessionstoredforreg['step']) && $sessionstoredforreg['step'] > '5') ? $sessionstoredforreg['step'] : '5';
						if(isset($retrun_mes) && $retrun_mes!='' && is_array($retrun_mes) && count($retrun_mes) > 0 && $retrun_mes['status']=='success' )
						{
							if(file_exists($upload_path.'/'.$checkifmemberexists['profile_pic']) && $checkifmemberexists['profile_pic']!='' && !is_null($checkifmemberexists['profile_pic']))
							{
								$this->common_front_model->delete_file($upload_path.'/'.$checkifmemberexists['profile_pic']);
							}
							$reandompass = $this->generateRandomString('15');
							$customvar = array('profile_pic'=>$retrun_mes['file_data']['file_name'],'email_ver_str'=>$reandompass,'email_ver_status'=>'No'); 
							if($this->input->post('user_id') && $this->input->post('user_id')!='')
							{
								$this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',2);
							}
							else
							{
								$this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$sessionstoredforreg['emp_reg_sess_id'],'',1,'is_deleted',2);
								$newreg = array('emp_reg_sess_id' => $sessionstoredforreg['emp_reg_sess_id'],'emp_reg_sess' => $sessionstoredforreg['emp_reg_sess'],'step' => $step);
								$old_session =  $this->session->userdata('employer_reg');
								$removed_item_from_session =  array_pop($old_session);
								array_push($old_session, $newreg);
								$this->session->set_userdata('employer_reg', $old_session);
							}
							/*$data_return['status'] = "success";
							$data_return['successmessage'] = $this->data['custom_lable']->language['emp_reg_data_ins_succ'];		
							$this->output->set_content_type('application/json');
							$data['data'] = $this->output->set_output(json_encode($data_return));*/
							$email_temp_data = $this->common_front_model->getemailtemplate('Employer Registration');
							if(isset($email_temp_data) && $email_temp_data !='' && is_array($email_temp_data) && count($email_temp_data) > 0)
							{
								$config_data = $this->common_front_model->data['config_data'];
								$webfriendlyname = $config_data['web_frienly_name'];
								$subject = $email_temp_data['email_subject'];
								$email_content = $email_temp_data['email_content'];
								$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
								$trans = array("websitename" =>$webfriendlyname,"yourname"=>$checkifmemberexists['fullname'],"email_id"=>$checkifmemberexists['email'],"/cpass"=>$reandompass,"site domain name"=>$this->common_front_model->data['base_url'].'login_employer/verify_member/');
								
								$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
								$this->common_front_model->common_send_email($checkifmemberexists['email'],$subject,$email_template);
							}
							if(isset($checkifmemberexists['mobile']) && $checkifmemberexists['mobile']!='')
							{
								$get_sms_temp = $this->common_front_model->get_sms_template('Registration Employer');
								if(isset($get_sms_temp) && $get_sms_temp!='' && is_array($get_sms_temp) && count($get_sms_temp) > 0)
								{
									$sms_template = htmlspecialchars_decode($get_sms_temp['sms_content'],ENT_QUOTES); 
									$trans = array("#web_name#"=>$webfriendlyname);
									$sms_template = $this->common_front_model->sms_string_replaced($sms_template, $trans);
									$this->common_front_model->common_sms_send($checkifmemberexists['mobile'],$sms_template);
								}
							}
							$this->session->unset_userdata('employer_reg');
						
						//$this->session->unset_userdata('employer_reg');
						$datavar['result']="success";
						$datavar['stepnoreturn']= $step;
					}
					else
					{
						$datavar['result']="error";	
						$datavar['returnmess']="Invalid file type or File size is to large, Max file size is 2MB.";	
					}
				}
				else if(isset($_POST['profile_pic']) && $_POST['profile_pic']!='')
				{
					
					$profile_pic_name = 'profile_pic';
					$upload_profile_pic_name = time().'-'.$checkifmemberexists['id'].'.jpg';
					$this->common_front_model->base_64_photo($profile_pic_name,'path_emp_photos',$upload_profile_pic_name);
					$_REQUEST['profile_pic'] = $upload_profile_pic_name;
					$_REQUEST['profile_pic_approval'] = 'UNAPPROVED';
					$customvar = array('profile_pic'=>$upload_profile_pic_name,'profile_pic_approval'=>'UNAPPROVED');
					$response = $this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$checkifmemberexists['id']);
					
					if($response && $response=='success')
					{
						if(file_exists($this->common_front_model->path_emp_photos.$checkifmemberexists['profile_pic']))
						{
							 $delete_profile_pic = $this->common_front_model->path_emp_photos.$checkifmemberexists['profile_pic'];
							if(isset($delete_profile_pic) && $delete_profile_pic!='')
							{
								$this->common_front_model->delete_file($delete_profile_pic);
							}
						}
					}
					$step = (isset($sessionstoredforreg) && isset($sessionstoredforreg['step']) && $sessionstoredforreg['step'] > '5') ? $sessionstoredforreg['step'] : '5';
					$reandompass = $this->generateRandomString('15');
					
					$email_temp_data = $this->common_front_model->getemailtemplate('Employer Registration');
					if(isset($email_temp_data) && $email_temp_data !='' &&  is_array($email_temp_data) && count($email_temp_data) > 0)
					{
						$config_data = $this->common_front_model->data['config_data'];
						$webfriendlyname = $config_data['web_frienly_name'];
						$subject = $email_temp_data['email_subject'];
						$email_content = $email_temp_data['email_content'];
						$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
						$trans = array("websitename" =>$webfriendlyname,"yourname"=>$checkifmemberexists['fullname'],"email_id"=>$checkifmemberexists['email'],"/cpass"=>$reandompass,"site domain name"=>$this->common_front_model->data['base_url'].'login_employer/verify_member/');
						$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
						$this->common_front_model->common_send_email($checkifmemberexists['email'],$subject,$email_template);
					}
					if(isset($checkifmemberexists['mobile']) && $checkifmemberexists['mobile']!='')
					{
						$get_sms_temp = $this->common_front_model->get_sms_template('Registration Employer');
						if(isset($get_sms_temp) && $get_sms_temp !='' && is_array($get_sms_temp) && count($get_sms_temp) > 0)
						{
							$sms_template = htmlspecialchars_decode($get_sms_temp['sms_content'],ENT_QUOTES); 
							$trans = array("#web_name#"=>$webfriendlyname);
							$sms_template = $this->common_front_model->sms_string_replaced($sms_template, $trans);
							$this->common_front_model->common_sms_send($checkifmemberexists['mobile'],$sms_template);
						}
					}
					$this->session->unset_userdata('employer_reg');
						
						$datavar['result']="success";
						$datavar['stepnoreturn']= $step;
				}
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function get_employerdetail_frm_email($email="")
	{
		$returnvar = array();
		if($email!='')
		{
			$where_arra = array('email'=>$email);
			$returnvar = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'','','','','');
		}
		return $returnvar;
	}
	public function get_employerdetail_frm_id($id="")
	{
		$returnvar = array();
		if($id!='')
		{
			$where_arra = array('id'=>$id);
			$returnvar = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,'','','','','');
		}
		return $returnvar;
	}
	public function get_suggestion($search)
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"role_name like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('role_master',$where_arra,2,'','','','10',"");
		$opt_array = array();
			
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					$opt_array[] = array('value'=>$data_arr_val['role_name'],'lable'=>$data_arr_val['role_name']);
				}
			}
			return $opt_array;
	}
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	public function get_months($search)
	{
		$opt_array = array();
		
		return $opt_array;
	}
}
?>