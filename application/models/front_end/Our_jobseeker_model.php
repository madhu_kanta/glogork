<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Our_jobseeker_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function getactivejobseeker_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','profile_visibility'=>'Yes');
		$getactivejobseeker_count = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,0,'id','','','','');
		return $getactivejobseeker_count;
	}
	public function getjobseekers($page=1,$limit=3,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','profile_visibility'=>'Yes');
		$getjobseekers = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,2,$req_field_comma,'id desc',$page,$limit,'');
		return $getjobseekers;
	}
}
?>