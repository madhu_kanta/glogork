<?php defined('BASEPATH') OR exit('No direct script access allowed');
class My_plan_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		// for payubizz
		$this->marchent_key ='EieL94BG';
		$this->salt ='tS303T0I6M';
		$this->marchent_id ='123456';
		
		$PAYU_BASE_URL = "https://secure.payu.in";
		if(base_url() =='http://192.168.1.111/job_portal/job_portal/' || base_url() =='http://192.168.1.222/original_script/')
		{
			$PAYU_BASE_URL = "https://test.payu.in/_payment";
		}
		$this->pay_url =$PAYU_BASE_URL;
		$this->discount_amount_temp =0;
		// for payubizz
		
		// for payu money
		$this->payu_marchent_key ='xDUFXaSE';
		$this->payu_salt ='yY2uoHvYGK';
		$this->payu_marchent_id ='5707872';
		
		$PAYU_BASE_URL = "https://secure.payu.in";
		if(base_url() =='http://192.168.1.111/job_portal/job_portal/')
		{
			//$PAYU_BASE_URL = "https://test.payu.in/_payment";
		}
		$this->payu_pay_url =$PAYU_BASE_URL;
		// for payu money
		
		//$this->is_active='pay_biz';
		//$this->is_active='payu_money';
		$this->is_active = 'payu_money';
		
		/* For cc avenu payment detail*/
		$this->cc_merchant_id = '164673';
		$this->cc_working_key = 'E16E411EE36E8B33C97EA2A8B96E4AB7';
		$this->cc_access_code = 'AVEW75FA91CM01WEMC';
		/*$this->cc_merchant_id = '146366';
		$this->cc_working_key = 'ABBD3D84392BB2662086B3DA906F6978';
		$this->cc_access_code = 'AVDO72EH83CN66ODNC';*/
		/* For cc avenu payment detail*/
	}
	public  function current_plan_detail()
	{
		$plan_data = array();
		$colmn_id = 'js_id';
		$user_data = $this->common_front_model->get_logged_user_typeid();
		$user_type = $user_data['user_type'];
		$user_id = $user_data['user_id'];
		if($this->input->post('user_type'))
		{
			$user_type_temp = $this->input->post('user_type');
			if($user_type_temp !='')
			{
				$user_type = $user_type_temp;
			}
		}
		if($user_type == 'job_seeker')
		{
			$plan_table_name = 'plan_jobseeker';
			$colmn_id = 'js_id';
		}
		else
		{
			$plan_table_name = 'plan_employer';
			$colmn_id = 'emp_id';
		}
		$plan_data = $this->common_front_model->get_count_data_manual($plan_table_name,array('current_plan'=>'Yes',$colmn_id=>$user_id,'is_deleted'=>'No'),1,' * ','',0,'',0);
		return $plan_data;
	}
	public function plan_list()
	{
		$user_data = $this->common_front_model->get_logged_user_typeid();
		$user_type = $user_data['user_type'];
		if($this->input->post('user_type'))
		{
			$user_type_temp = $this->input->post('user_type');
			if($user_type_temp !='')
			{
				$user_type = $user_type_temp;
			}
		}
		$plan_data = array();
		if($user_type == 'job_seeker')
		{
			$plan_table_name = 'credit_plan_jobseeker';
		}
		else
		{
			$plan_table_name = 'credit_plan_employer';
		}
		$plan_data = $this->common_front_model->get_count_data_manual($plan_table_name,array('status'=>'APPROVED','is_deleted'=>'No'),2,' * ','',0,'',0);
		return $plan_data;
	}
	public function check_copan($plan_id='',$coupancode ='')
	{
		$return = 'Some issue, Please try again';
		if($plan_id !='' && $coupancode !='')
		{
			$user_type = '';
			$user_id = '';
			
			$user_agent = 'NI-WEB';
			if($this->input->post('user_agent'))
			{
				$user_agent = $this->input->post('user_agent');
			}
			if($user_agent =='NI-AAPP' && $this->input->post('user_id') && $this->input->post('user_type'))
			{
				$user_type = $this->input->post('user_type');
				$user_id = $this->input->post('user_id');
			}
			else
			{
				$user_data = $this->common_front_model->get_logged_user_typeid();
				$user_type = $user_data['user_type'];
				$user_id = $user_data['user_id'];			
			}
			
			if($user_type !='' && $user_id !='')
			{
				$plan_data = array();
				if($user_type == 'job_seeker')
				{
					$plan_table_name = 'credit_plan_jobseeker';
				}
				else
				{
					$plan_table_name = 'credit_plan_employer';
				}
				$today_date = $this->common_front_model->getCurrentDate('Y-m-d');
				$plan_data = $this->common_front_model->get_count_data_manual($plan_table_name,array('status'=>'APPROVED','id'=>$plan_id),1,' * ','',0,'',0);
				$plan_amount = 0;
				$total_pay = 0;
				if(isset($plan_data['plan_amount']) && $plan_data['plan_amount'] !='')
				{
					$plan_amount = $plan_data['plan_amount'];
				}
				$discount_amount = 0;
				$coupan_data = $this->common_front_model->get_count_data_manual('coupan_code',array('coupan_code'=>$coupancode,'status'=>'APPROVED'," active_from <= '$today_date' and expired_on >= '$today_date' "),1,' * ','',0,'',0);
				if(isset($coupan_data) && $coupan_data !='' && is_array($coupan_data) && count($coupan_data) > 0)
				{
					$discount_amount = $coupan_data['discount_amount'];
					if($discount_amount > $plan_amount)
					{
						$return = 'Coupon Code cant redeem for this plan';
					}
					else
					{
						$this->discount_amount_temp = $discount_amount;
						$data_array = array('discount_amount'=>$discount_amount,'coupan_code'=>$coupancode,'plan_id'=>$plan_id,'user_type'=>$user_type);
						$this->session->set_userdata('coupan_data_reddem',$data_array);
						$return = 'success';
					}
				}
				else
				{
					$return = 'Invalid Coupon Code';
				}
			}		
		}
		else
		{
			$return = 'Please Enter Coupon Code';
		}
		return $return;
	}
	public function cc_update_payment()
	{
		$workingKey = $this->my_plan_model->cc_working_key;		//Working Key should be provided here.
		$encResponse = $_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString = $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		$order_status = "";
		$decryptValues = explode('&', $rcvdString);
		/* echo '<pre>';
		 print_r($decryptValues);*/
		$tracking_id ='';
		$amount = '';
		$dataSize = sizeof($decryptValues);	
		for($i = 0; $i < $dataSize; $i++)
		{
			$information=explode('=',$decryptValues[$i]);
			if($i==3)	$order_status=$information[1];
			if($i==1)	$tracking_id=$information[1];
			if($i==10)	$amount=$information[1];
		}
		$merchant_data='';
		if($order_status=="Success")
		{
			/*echo 'inn success';
			echo $tracking_id;
			echo '--';
			echo $amount;*/
			
			$user_data = $this->common_front_model->get_logged_user_typeid();
			$user_type = $user_data['user_type'];
			$user_id = $user_data['user_id'];
			
			$_REQUEST['user_id'] = $user_id;
			$_REQUEST['user_type'] = $user_type;
			
			$plan_data_session = $this->session->userdata('plan_data_session');
			
			$_REQUEST['plan_id'] = $plan_data_session['plan_id'];
			//$_REQUEST['payment_note'] = $payment_note;
			
			$_REQUEST['payment_method'] = 'Credit Card/ Online Transfer';
			$_REQUEST['transaction_id'] = $tracking_id;
			
			$_REQUEST['discount_amount'] = $plan_data_session['discount_amount'];
			$_REQUEST['coupan_code'] = $plan_data_session['coupan_code'];
			$update_plan = $this->common_model->update_plan_member_call();
			return $update_plan;
		}
		else
		{
			redirect($this->base_url.'my-plan/payment-cancel');
			exit;
		}
	}
	public function update_payment()
	{
		if($this->is_active =='payu_money')
		{
			$salt		 = $this->payu_salt;
		}
		else
		{
			$salt		 = $this->salt;
		}
		
		$status      = $_POST["status"];
		$firstname   = $_POST["firstname"];
		$amount      = $_POST["amount"];
		$txnid       = $_POST["txnid"];
		$posted_hash = $_POST["hash"];
		$key         = $_POST["key"];
		$productinfo = $_POST["productinfo"];
		$email       = $_POST["email"];
		$payuMoneyId = $_POST["payuMoneyId"];
		/*echo '<pre>';
		print_r($_REQUEST);*/
		
		if (isset($_POST["additionalCharges"]))
		{
			$additionalCharges =$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
			  
	  	}
		else
		{
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
	  	}
		$hash = hash("sha512", $retHashSeq);
		if ($hash != $posted_hash)
		{
			redirect($this->base_url.'my-plan/payment-cancel');
			exit;
		}
		else if($status =='success')
		{
			$user_data = $this->common_front_model->get_logged_user_typeid();
			$user_type = $user_data['user_type'];
			$user_id = $user_data['user_id'];
			
			$_REQUEST['user_id'] = $user_id;
			$_REQUEST['user_type'] = $user_type;
			
			$plan_data_session = $this->session->userdata('plan_data_session');
			
			$_REQUEST['plan_id'] = $plan_data_session['plan_id'];
			//$_REQUEST['payment_note'] = $payment_note;
			
			$_REQUEST['payment_method'] = 'Credit Card/ Online Transfer';
			$_REQUEST['transaction_id'] = $payuMoneyId;
			
			$_REQUEST['discount_amount'] = $plan_data_session['discount_amount'];
			$_REQUEST['coupan_code'] = $plan_data_session['coupan_code'];
			$update_plan = $this->common_model->update_plan_member_call();
			return $update_plan;
		}
		else
		{
			redirect($this->base_url.'my-plan/payment-cancel');
			exit;
		}
	}
	public function update_payment_app()
	{
		$update_plan = $this->common_model->update_plan_member_call();
		return $update_plan;
	}
	
	/* For CC avenu lib*/
	function encrypt($plainText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
		if(mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
		{
			$encryptedText = mcrypt_generic($openMode, $plainPad);
			mcrypt_generic_deinit($openMode);
		} 
		return bin2hex($encryptedText);
	}
	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	mcrypt_generic_deinit($openMode);
		return $decryptedText;
	}
	//*********** Padding Function *********************
	function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}
	//********** Hexadecimal to Binary function for php 4.0 version ********
	function hextobin($hexString) 
	{
    	$length = strlen($hexString); 
        $binString="";   
        $count=0; 
        while($count<$length) 
        {       
            $subString =substr($hexString,$count,2);           
            $packedString = pack("H*",$subString); 
            if ($count==0)
		   	{
				$binString=$packedString;
		    }
		    else 
		    {
				$binString.=$packedString;
		    }
		    $count+=2; 
       	} 
  	    return $binString; 
    }
	/* For CC avenu lib*/
}
?>