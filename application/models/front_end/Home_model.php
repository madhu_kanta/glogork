<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Home_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function getrecent_jobs_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		$getrecent_jobs_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id','','','','');
		
		return $getrecent_jobs_count;
	}
	public function gethighlight_jobs_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','job_highlighted'=>'Yes');
		$getrecent_jobs_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id','','','','');
		
		return $getrecent_jobs_count;
	}
	public function getrecent_jobs($page=1,$limit=3,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		
		//$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','employer_delete'=>'No','employer_status'=>'APPROVED');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		$getrecent_jobs = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,$req_field_comma,'posted_on desc',$page,$limit,'');
		//echo $this->db->last_query();exit;
		return $getrecent_jobs;
	}
	public function gethighlight_jobs($page=1,$limit=3,$req_field=array())
	{
		if(isset($req_field) && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','job_highlighted'=>'Yes');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		$gethighlight_jobs = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,$req_field_comma,'RAND()',$page,$limit,'');
		return $gethighlight_jobs;
	}
}
?>