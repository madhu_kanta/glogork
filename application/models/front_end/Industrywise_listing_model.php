<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Industrywise_listing_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function gettotaljobs_groupbyind()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		$this->db->group_by('job_industry');
		$this->db->group_by('functional_area');
		$totaljobs_groupbyindcount = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,0,'id','','','','');
		return $totaljobs_groupbyindcount;
	}
	public function getindustriesdata($where_arra=array(),$page=1,$limit=5,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$this->db->group_by('job_industry');
		$this->db->group_by('functional_area');
		//$totaljobs_groupbyinddata = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,2,$req_field_comma,'',$page,$limit,'');
		$totaljobs_groupbyinddata = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,2,$req_field_comma,'','','','');
		return $totaljobs_groupbyinddata;
	}
}
?>