<?php defined('BASEPATH') OR exit('No direct script access allowed');
class My_profile_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	function getagefrmbdate($birthdate='')
	{
		$returnvar = "Not available";	
		if($birthdate!='' && !is_null($birthdate) && $birthdate!='0000-00-00')
		{
			$returnvar = floor((time() - strtotime($birthdate))/31556926).' Years ';
		}
		return $returnvar;
	}
	function geteducationfromid($id='')
	{
		$returnvar = array();
		if($id!='')
		{
			$where_arra = array('js_id'=>$id);
			$education_count = $this->common_front_model->get_count_data_manual('js_education_view',$where_arra,0,'id','id desc','','','');
			if($education_count !='' && $education_count > 0)
			{
				$returnvar = $this->common_front_model->get_count_data_manual('js_education_view',$where_arra,2,'','id desc','','','');
			}
		}
		return $returnvar;
	}
	function getlangknownfromid($id='')
	{
		$returnvar = array();
		if($id!='')
		{
			$where_arra = array('js_id'=>$id);
			$education_count = $this->common_front_model->get_count_data_manual('jobseeker_language',$where_arra,0,'id','id desc','','','');
			if($education_count  !='' && $education_count > 0)
			{
				$returnvar = $this->common_front_model->get_count_data_manual('jobseeker_language',$where_arra,2,'','id desc','','','');
			}
		}
		return $returnvar;
	}
	function getdetailsfromid($table='',$id_filed='',$id='',$requiredfield='',$dataorcount='data',$records='single',$finalarryforsingle='single')
	{
		$returnvar = array();
		if($id!='' && $table!='' && $id_filed!='')
		{
			$requiredfield = $requiredfield!='' ? "`".$requiredfield."`" : $requiredfield ;
			$id_filed = "`".$id_filed."`" ;
			if($table=='jobseeker_education' || $table=='js_education_view' || $table=='jobseeker_language' || $table=='jobseeker_workhistory_view')
			{
				$where_arra = array($id_filed => $id);
			}
			else
			{
				$where_arra = array($id_filed => $id,'status' => 'APPROVED','is_deleted' => 'No');
			}
			
			$list_count = $this->common_front_model->get_count_data_manual($table,$where_arra,'0',$requiredfield,$id_filed.' desc','','','');
			if($dataorcount=='count')
			{
				return $list_count;
			}
			if($list_count !='' && $list_count  > 0)
			{
				if($records=='multiple')
				{
					if($table=='js_education_view' || $table=='jobseeker_education')
					{
						$this->db->order_by("FIELD(is_certificate_X_XII,'Certi'),FIELD(is_certificate_X_XII,'Edu'),FIELD(is_certificate_X_XII,'XII'),FIELD(is_certificate_X_XII,'X')");
					}
					$returnvar = $this->common_front_model->get_count_data_manual($table,$where_arra,2,$requiredfield,$id_filed.' desc','','','');
				}
				else
				{
					if($finalarryforsingle=='arraytype')
					{
						$returnvar[] = $this->common_front_model->get_count_data_manual($table,$where_arra,1,$requiredfield,$id_filed.' desc','','','');
					}
					else
					{
						$returnvar = $this->common_front_model->get_count_data_manual($table,$where_arra,1,$requiredfield,$id_filed.' desc','','','');
					}
				}
			}
		}
		return $returnvar;
	}
	function getresumefromid($id='')
	{
		$returnvar = array();
		if($id!='')
		{
			$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1);
			$where_arra = array('id'=>$id);
			$resume = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'resume_file','id','','','');
			if($resume['resume_file'] != '' && !is_null($resume['resume_file']) && file_exists($upload_path.'/'.$resume['resume_file']))
			{
				$this->load->helper('file');
				$string = read_file($upload_path.'/'.$resume['resume_file']);
				$returnvar = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arra,1,'','id desc','','','');
			}
		}
		return $returnvar;
	}
	public function editpersonaldet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$mobile_c_code = $this->input->post('mobile_c_code');
				$mobile = $this->input->post('mobile');
				$mobile_no = $mobile_c_code.'-'.$mobile;
				
				$mobile_num_old = $this->input->post('mobile_num_old');
				if($mobile_num_old != $mobile_no)
				{
					$_REQUEST['mobile_verified'] = 'No';
				}
				
				$brithday = $this->input->post('birthdate');
				$data_array_custom = array('mobile'=> $mobile_no,'birthdate'=>$brithday);
				$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editsocialdet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$facebookurl = $this->input->post('facebookurl');
				$gplusurl = $this->input->post('gplusurl');
				$twitterurl = $this->input->post('twitterurl');
				$linkdinurl = $this->input->post('linkdinurl');
				
				$data_array_custom = array('facebook_url'=> $facebookurl,'gplus_url'=>$gplusurl,'twitter_url'=>$twitterurl,'linkedin_url'=>$linkdinurl);
				$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editlangdet_mod($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				foreach($this->input->post() as $postsingle_key=>$postsingle_value)
				{
					if(!is_array($postsingle_value) && !is_array($postsingle_key))
					{
						$postsingle_lang_chk = substr($postsingle_key, 0, 10);
						if($postsingle_lang_chk=='lang_known')
						{
							$postsingle_key_arr = explode("_",$postsingle_key);
							if(in_array("a",$postsingle_key_arr))
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
								{
									$checkbox_reading = ($this->input->post('checkbox_a_'.$postsingle_key_arr[3]) && in_array('reading',$this->input->post('checkbox_a_'.$postsingle_key_arr[3]))) ? 'Yes' : 'No';
								$checkbox_writing = ($this->input->post('checkbox_a_'.$postsingle_key_arr[3]) && in_array('writing',$this->input->post('checkbox_a_'.$postsingle_key_arr[3]))) ? 'Yes' : 'No';
								$checkbox_speaking = ($this->input->post('checkbox_a_'.$postsingle_key_arr[3]) && in_array('speaking',$this->input->post('checkbox_a_'.$postsingle_key_arr[3]))) ? 'Yes' : 'No';	
								}
								else
								{
									if($this->input->post('checkbox_a_'.$postsingle_key_arr[3]) && $this->input->post('checkbox_a_'.$postsingle_key_arr[3])!='')
									{
										$appchekbox = rtrim($this->input->post('checkbox_a_'.$postsingle_key_arr[3]),",");
										$appchekbox_arr = explode(',',$appchekbox);
										if(is_array($appchekbox_arr))
										{
											$checkbox_reading = (in_array('reading',$appchekbox_arr)) ? 'Yes' : 'No';
											$checkbox_writing = (in_array('writing',$appchekbox_arr)) ? 'Yes' : 'No';
											$checkbox_speaking = (in_array('speaking',$appchekbox_arr)) ? 'Yes' : 'No';	
										}
									}
									else
									{
										$checkbox_reading = 'No';
										$checkbox_writing = 'No';
										$checkbox_speaking = 'No';	
									}
								}
								
								$proficiency = ($this->input->post('proficiency_a_'.$postsingle_key_arr[3]) && $this->input->post('proficiency_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('proficiency_a_'.$postsingle_key_arr[3]) : '';
								$data_array_custom = array('js_id'=> $idofemp,'language'=>$postsingle_value,'proficiency_level'=>$proficiency,'speaking'=>$checkbox_speaking,'writing'=>$checkbox_writing,'reading'=>$checkbox_reading,'update_on'=>$current_datetime);
								$this->common_front_model->save_update_data('jobseeker_language',$data_array_custom,'id','add','','','1','1','is_deleted',2);
							}
							else
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
								{
									$checkbox_reading = ($this->input->post('checkbox_'.$postsingle_key_arr[2]) && in_array('reading',$this->input->post('checkbox_'.$postsingle_key_arr[2]))) ? 'Yes' : 'No';
									$checkbox_writing = ($this->input->post('checkbox_'.$postsingle_key_arr[2]) && in_array('writing',$this->input->post('checkbox_'.$postsingle_key_arr[2]))) ? 'Yes' : 'No';
									$checkbox_speaking = ($this->input->post('checkbox_'.$postsingle_key_arr[2]) && in_array('speaking',$this->input->post('checkbox_'.$postsingle_key_arr[2]))) ? 'Yes' : 'No';
								}
								else
								{
									if($this->input->post('checkbox_'.$postsingle_key_arr[2]) && $this->input->post('checkbox_'.$postsingle_key_arr[2])!='')
									{
										$appchekbox = rtrim($this->input->post('checkbox_'.$postsingle_key_arr[2]),",");
										$appchekbox_arr = explode(',',$appchekbox);
										if(is_array($appchekbox_arr))
										{
											$checkbox_reading = (in_array('reading',$appchekbox_arr)) ? 'Yes' : 'No';
											$checkbox_writing = (in_array('writing',$appchekbox_arr)) ? 'Yes' : 'No';
											$checkbox_speaking = (in_array('speaking',$appchekbox_arr)) ? 'Yes' : 'No';	
										}
									}
									else
									{
										$checkbox_reading = 'No';
										$checkbox_writing = 'No';
										$checkbox_speaking = 'No';	
									}
								}
								$proficiency = ($this->input->post('proficiency_'.$postsingle_key_arr[2]) && $this->input->post('proficiency_'.$postsingle_key_arr[2])!='') ? $this->input->post('proficiency_'.$postsingle_key_arr[2]) : '';
								$data_array_custom = array('js_id'=> $idofemp,'language'=>$postsingle_value,'proficiency_level'=>$proficiency,'speaking'=>$checkbox_speaking,'writing'=>$checkbox_writing,'reading'=>$checkbox_reading,'update_on'=>$current_datetime);
								$data_array_custom = array('language'=>$postsingle_value,'proficiency_level'=>$proficiency,'speaking'=>$checkbox_speaking,'writing'=>$checkbox_writing,'reading'=>$checkbox_reading,'update_on'=>$current_datetime);
								$where_array = array('js_id'=> $idofemp,'id'=>$postsingle_key_arr[2]);
								$this->common_front_model->update_insert_data_common('jobseeker_language',$data_array_custom,$where_array);
							}
						}
					}
				}
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editotherdet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$exp_year = $this->input->post('exp_year');
				$exp_month = $this->input->post('exp_month');
				//$total_exp = $exp_year.'-'.$exp_month;
				$total_exp = $exp_year.'.'.$exp_month;
				
				/*$a_salary_lacs = $this->input->post('a_salary_lacs');
				$a_salary_thousand = $this->input->post('a_salary_thousand');
				//$annual_salary = $a_salary_lacs.'-'.$a_salary_thousand;
				$annual_salary = $a_salary_lacs.'.'.$a_salary_thousand;*/
				$annual_salary = $this->input->post('annual_salary');
				
				/*$exp_salary_lacs = $this->input->post('exp_salary_lacs');
				$exp_salary_thousand = $this->input->post('exp_salary_thousand');
				//$exp_annual_salary = $exp_salary_lacs.'-'.$exp_salary_thousand;
				$exp_annual_salary = $exp_salary_lacs.'.'.$exp_salary_thousand;*/
				
				$exp_annual_salary = $this->input->post('expected_annual_salary');
				
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
				{
					$preferred_city = implode(',',$this->input->post('preferred_city[]'));
				}
				else
				{
					$preferred_city = rtrim($this->input->post('preferred_city'),",");
				}
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
				{
					$prefered_shift = $this->input->post('prefered_shift');
				}
				else
				{
					$prefered_shift = rtrim($this->input->post('prefered_shift'),",");
				}
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
				{
					$key_skill = $this->input->post('key_skill');
				}
				else
				{
					$key_skill = rtrim($this->input->post('key_skill'),",");
				}
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
				{
					$desire_job_type = $this->input->post('desire_job_type');
				}
				else
				{
					$desire_job_type = rtrim($this->input->post('desire_job_type'),",");
				}
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-WEB')
				{
					$employment_type = $this->input->post('employment_type');
				}
				else
				{
					$employment_type = rtrim($this->input->post('employment_type'),",");
				} 
				
				$data_array_custom = array('total_experience'=> $total_exp,'annual_salary'=>$annual_salary,'expected_annual_salary'=>$exp_annual_salary,'preferred_city'=>$preferred_city,'prefered_shift'=>$prefered_shift,'desire_job_type'=>$desire_job_type,'key_skill'=>$key_skill,'employment_type'=>$employment_type);
				$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editedudet_mod($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0 && count($this->input->post()) > 0)
			{
				foreach($this->input->post() as $postsingle_key=>$postsingle_value)
				{
					if(!is_array($postsingle_value) && !is_array($postsingle_key))
					{
						$postsingle_lang_chk = substr($postsingle_key, 0, 19);
						if($postsingle_lang_chk=='qualification_level')
						{
							$postsingle_key_arr = explode("_",$postsingle_key);
							if(in_array("a",$postsingle_key_arr))
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								$qualification_level = ($this->input->post('qualification_level_a_'.$postsingle_key_arr[3]) && $this->input->post('qualification_level_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('qualification_level_a_'.$postsingle_key_arr[3]) : '';
								$institute_name = ($this->input->post('institute_name_a_'.$postsingle_key_arr[3]) && $this->input->post('institute_name_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('institute_name_a_'.$postsingle_key_arr[3]) : '';
								$specialization = ($this->input->post('specialization_a_'.$postsingle_key_arr[3]) && $this->input->post('specialization_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('specialization_a_'.$postsingle_key_arr[3]) : '';
								$percentage = ($this->input->post('percentage_a_'.$postsingle_key_arr[3]) && $this->input->post('percentage_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('percentage_a_'.$postsingle_key_arr[3]) : '';
								$passing_year = ($this->input->post('passing_year_a_'.$postsingle_key_arr[3]) && $this->input->post('passing_year_a_'.$postsingle_key_arr[3])!='') ? $this->input->post('passing_year_a_'.$postsingle_key_arr[3]) : '';
								
								$data_array_custom = array('js_id'=> $idofemp,'passing_year'=>$passing_year,'qualification_level'=>$qualification_level,'institute'=>$institute_name,'specialization'=>$specialization,'marks'=>$percentage,'is_certificate_X_XII'=>'Edu','update_on'=>$current_datetime);
								$this->common_front_model->save_update_data('jobseeker_education',$data_array_custom,'id','add','','','1','1','is_deleted',2);
							}
							else
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								$qualification_level = ($this->input->post('qualification_level_'.$postsingle_key_arr[2]) && $this->input->post('qualification_level_'.$postsingle_key_arr[2])!='') ? $this->input->post('qualification_level_'.$postsingle_key_arr[2]) : '';
								$institute_name = ($this->input->post('institute_name_'.$postsingle_key_arr[2]) && $this->input->post('institute_name_'.$postsingle_key_arr[2])!='') ? $this->input->post('institute_name_'.$postsingle_key_arr[2]) : '';
								$specialization = ($this->input->post('specialization_'.$postsingle_key_arr[2]) && $this->input->post('specialization_'.$postsingle_key_arr[2])!='') ? $this->input->post('specialization_'.$postsingle_key_arr[2]) : '';
								$percentage = ($this->input->post('percentage_'.$postsingle_key_arr[2]) && $this->input->post('percentage_'.$postsingle_key_arr[2])!='') ? $this->input->post('percentage_'.$postsingle_key_arr[2]) : '';
								$passing_year = ($this->input->post('passing_year_'.$postsingle_key_arr[2]) && $this->input->post('passing_year_'.$postsingle_key_arr[2])!='') ? $this->input->post('passing_year_'.$postsingle_key_arr[2]) : '';
								
								$data_array_custom = array('passing_year'=>$passing_year,'qualification_level'=>$qualification_level,'institute'=>$institute_name,'specialization'=>$specialization,'marks'=>$percentage,'is_certificate_X_XII'=>'Edu','update_on'=>$current_datetime);
								$where_array = array('js_id'=> $idofemp,'id'=>$postsingle_key_arr[2]);
								$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom,$where_array);
							}
						}
					}
				}
				foreach($this->input->post() as $postsingle_key=>$postsingle_value)
				{
					if(!is_array($postsingle_value) && !is_array($postsingle_key))
					{
						$postsingle_lang_chk = substr($postsingle_key, 0, 6);
						
						if($postsingle_lang_chk=='certid')
						{
							$postsingle_key_arr = explode("_",$postsingle_key);
							if(in_array("a",$postsingle_key_arr))
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								$certid = ($this->input->post('certid_a_'.$postsingle_key_arr[2]) && $this->input->post('certid_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('certid_a_'.$postsingle_key_arr[2]) : '';
								$certiu = ($this->input->post('certiu_a_'.$postsingle_key_arr[2]) && $this->input->post('certiu_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('certiu_a_'.$postsingle_key_arr[2]) : '';
								$certiy = ($this->input->post('certiy_a_'.$postsingle_key_arr[2]) && $this->input->post('certiy_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('certiy_a_'.$postsingle_key_arr[2]) : '';
								
								$data_array_custom = array('js_id'=> $idofemp,'passing_year'=>$certiy,'qualification_level'=>'0','institute'=>$certiu,'specialization'=>$certid,'marks'=>'0','is_certificate_X_XII'=>'Certi','update_on'=>$current_datetime);
								$this->common_front_model->save_update_data('jobseeker_education',$data_array_custom,'id','add','','','1','1','is_deleted',2);
							}
							else
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								$certid = ($this->input->post('certid_'.$postsingle_key_arr[1]) && $this->input->post('certid_'.$postsingle_key_arr[1])!='') ? $this->input->post('certid_'.$postsingle_key_arr[1]) : '';
								$certiu = ($this->input->post('certiu_'.$postsingle_key_arr[1]) && $this->input->post('certiu_'.$postsingle_key_arr[1])!='') ? $this->input->post('certiu_'.$postsingle_key_arr[1]) : '';
								$certiy = ($this->input->post('certiy_'.$postsingle_key_arr[1]) && $this->input->post('certiy_'.$postsingle_key_arr[1])!='') ? $this->input->post('certiy_'.$postsingle_key_arr[1]) : '';
								$data_array_custom = array('passing_year'=>$certiy,'qualification_level'=>'0','institute'=>$certiu,'specialization'=>$certid,'marks'=>'0','is_certificate_X_XII'=>'Certi','update_on'=>$current_datetime);
								
								$where_array = array('js_id'=> $idofemp,'id'=>$postsingle_key_arr[1]);
								$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom,$where_array);
							}
						}
					}
				}
				if($this->input->post('tenthhid') && $this->input->post('tenthhid')!='')
				{
					$current_datetime_x = $this->common_front_model->getCurrentDate();
					$institute_name_X = ($this->input->post('tenthu') && $this->input->post('tenthu')!='') ? $this->input->post('tenthu') : '';
					$passyr_X = ($this->input->post('tenthy') && $this->input->post('tenthy')!='') ? $this->input->post('tenthy') : '';
					$passm_X = ($this->input->post('tenthm') && $this->input->post('tenthm')!='') ? $this->input->post('tenthm') : '';
					$data_array_custom_x = array('passing_year'=>$passyr_X,'qualification_level'=>'0','institute'=>$institute_name_X,'marks'=>$passm_X,'is_certificate_X_XII'=>'X','update_on'=>$current_datetime_x);
					$where_array_x = array('js_id'=> $idofemp,'id'=>$this->input->post('tenthhid'));
					$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom_x,$where_array_x);
				}
				elseif($this->input->post('tenthu')!='' && $this->input->post('tenthy')!='' && $this->input->post('tenthm')!='')
				{
					$current_datetime_x = $this->common_front_model->getCurrentDate();
					$institute_name_X = ($this->input->post('tenthu') && $this->input->post('tenthu')!='') ? $this->input->post('tenthu') : '';
					$passyr_X = ($this->input->post('tenthy') && $this->input->post('tenthy')!='') ? $this->input->post('tenthy') : '';
					$passm_X = ($this->input->post('tenthm') && $this->input->post('tenthm')!='') ? $this->input->post('tenthm') : '';
					
					if($this->checkifadded('10')==false)
					{
						$edu_id_str_10 = $this->checkifadded('10','data');
						$data_array_custom_x = array('passing_year'=>$passyr_X,'qualification_level'=>'0','institute'=>$institute_name_X,'marks'=>$passm_X,'is_certificate_X_XII'=>'X','update_on'=>$current_datetime_x);
						$where_array_x = array('js_id'=> $idofemp,'id'=>$edu_id_str_10['id']);
						$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom_x,$where_array_x);
						
					}
					else
					{
						$data_array_custom_x = array('js_id'=> $idofemp,'passing_year'=>$passyr_X,'qualification_level'=>'0','institute'=>$institute_name_X,'marks'=>$passm_X,'is_certificate_X_XII'=>'X','update_on'=>$current_datetime_x);
						$this->common_front_model->save_update_data('jobseeker_education',$data_array_custom_x,'id','add','','','1','1','is_deleted',2);
					}
				}
				if($this->input->post('twelvehid') && $this->input->post('twelvehid')!='')
				{
					$current_datetime_x_I = $this->common_front_model->getCurrentDate();
					$institute_name_X_I = ($this->input->post('twelveu') && $this->input->post('twelveu')!='') ? $this->input->post('twelveu') : '';
					$passyr_X_I = ($this->input->post('twelvey') && $this->input->post('twelvey')!='') ? $this->input->post('twelvey') : '';
					$passm_X_I = ($this->input->post('twelvem') && $this->input->post('twelvem')!='') ? $this->input->post('twelvem') : '';
					$data_array_custom_x_I = array('passing_year'=>$passyr_X_I,'qualification_level'=>'0','institute'=>$institute_name_X_I,'marks'=>$passm_X_I,'is_certificate_X_XII'=>'XII','update_on'=>$current_datetime_x_I);
					$where_array_x_I = array('js_id'=> $idofemp,'id'=>$this->input->post('twelvehid'));
					$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom_x_I,$where_array_x_I);
				}
				elseif($this->input->post('twelveu')!='' && $this->input->post('twelvem')!='' && $this->input->post('twelvey')!='')
				{
					$current_datetime_x_I = $this->common_front_model->getCurrentDate();
					$institute_name_X_I = ($this->input->post('twelveu') && $this->input->post('twelveu')!='') ? $this->input->post('twelveu') : '';
					$passyr_X_I = ($this->input->post('twelvey') && $this->input->post('twelvey')!='') ? $this->input->post('twelvey') : '';
					$passm_X_I = ($this->input->post('twelvem') && $this->input->post('twelvem')!='') ? $this->input->post('twelvem') : '';
					if($this->checkifadded('12')==false)
					{
						$edu_id_str = $this->checkifadded('12','data');
						$data_array_custom_x_I = array('passing_year'=>$passyr_X_I,'qualification_level'=>'0','institute'=>$institute_name_X_I,'marks'=>$passm_X_I,'is_certificate_X_XII'=>'XII','update_on'=>$current_datetime_x_I);
						$where_array_x_I = array('js_id'=> $idofemp,'id'=>$edu_id_str['id']);
						$this->common_front_model->update_insert_data_common('jobseeker_education',$data_array_custom_x_I,$where_array_x_I);
					}
					else
					{
						$data_array_custom_x_I = array('js_id'=> $idofemp,'passing_year'=>$passyr_X_I,'qualification_level'=>'0','institute'=>$institute_name_X_I,'marks'=>$passm_X_I,'is_certificate_X_XII'=>'XII','update_on'=>$current_datetime_x_I);
						$this->common_front_model->save_update_data('jobseeker_education',$data_array_custom_x_I,'id','add','','','1','1','is_deleted',2);
					}
				}
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editworkdet_mod($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0 && count($this->input->post()) > 0)
			{
				foreach($this->input->post() as $postsingle_key=>$postsingle_value)
				{
					if(!is_array($postsingle_value) && !is_array($postsingle_key))
					{
						$postsingle_lang_chk = substr($postsingle_key, 0, 11);
						if($postsingle_lang_chk=='companyname')
						{
							$postsingle_key_arr = explode("_",$postsingle_key);
							if(in_array("a",$postsingle_key_arr))
							{
								$current_datetime = $this->common_front_model->getCurrentDate();
								$companyname = ($this->input->post('companyname_a_'.$postsingle_key_arr[2]) && $this->input->post('companyname_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('companyname_a_'.$postsingle_key_arr[2]) : '';
								$industry = ($this->input->post('industry_a_'.$postsingle_key_arr[2]) && $this->input->post('industry_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('industry_a_'.$postsingle_key_arr[2]) : '';
								$functional_area = ($this->input->post('functional_area_a_'.$postsingle_key_arr[2]) && $this->input->post('functional_area_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('functional_area_a_'.$postsingle_key_arr[2]) : '';
								$job_role = ($this->input->post('job_role_a_'.$postsingle_key_arr[2]) && $this->input->post('job_role_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('job_role_a_'.$postsingle_key_arr[2]) : '';
								$joining_date = ($this->input->post('joining_date_a_'.$postsingle_key_arr[2]) && $this->input->post('joining_date_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('joining_date_a_'.$postsingle_key_arr[2]) : '';
								$leaving_date = ($this->input->post('leaving_date_a_'.$postsingle_key_arr[2]) && $this->input->post('leaving_date_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('leaving_date_a_'.$postsingle_key_arr[2]) : '';
								$currency_type = ($this->input->post('currency_type_a_'.$postsingle_key_arr[2]) && $this->input->post('currency_type_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('currency_type_a_'.$postsingle_key_arr[2]) : '';
								$achievements = ($this->input->post('achievements_a_'.$postsingle_key_arr[2]) && $this->input->post('achievements_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('achievements_a_'.$postsingle_key_arr[2]) : '';
								/*$a_salary_lacs = ($this->input->post('a_salary_lacs_a_'.$postsingle_key_arr[2]) && $this->input->post('a_salary_lacs_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('a_salary_lacs_a_'.$postsingle_key_arr[2]) : '0';
								$a_salary_thousand = ($this->input->post('a_salary_thousand_a_'.$postsingle_key_arr[2]) && $this->input->post('a_salary_thousand_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('a_salary_thousand_a_'.$postsingle_key_arr[2]) : '0';
								//$a_salary_concat = $a_salary_lacs.'-'.$a_salary_thousand;
								$a_salary_concat = $a_salary_lacs.'.'.$a_salary_thousand;*/
							
								
								$a_salary_concat = ($this->input->post('annual_salary_a_'.$postsingle_key_arr[2]) && $this->input->post('annual_salary_a_'.$postsingle_key_arr[2])!='') ? $this->input->post('annual_salary_a_'.$postsingle_key_arr[2]) : '0';
								
								$data_array_custom = array('js_id'=> $idofemp,'company_name'=>$companyname,'joining_date'=>$joining_date,'leaving_date'=>$leaving_date,'industry'=>$industry,'functional_area'=>$functional_area,'job_role'=>$job_role,'annual_salary'=>$a_salary_concat,'currency_type'=>$currency_type,'achievements'=>$achievements,'update_on'=>$current_datetime);
								$this->common_front_model->save_update_data('jobseeker_workhistory',$data_array_custom,'id','add','','','1','1','is_deleted',2);
							}
							else
							{
								
								$current_datetime = $this->common_front_model->getCurrentDate();
								$companyname = ($this->input->post('companyname_'.$postsingle_key_arr[1]) && $this->input->post('companyname_'.$postsingle_key_arr[1])!='') ? $this->input->post('companyname_'.$postsingle_key_arr[1]) : '';
								$industry = ($this->input->post('industry_'.$postsingle_key_arr[1]) && $this->input->post('industry_'.$postsingle_key_arr[1])!='') ? $this->input->post('industry_'.$postsingle_key_arr[1]) : '';
								$functional_area = ($this->input->post('functional_area_'.$postsingle_key_arr[1]) && $this->input->post('functional_area_'.$postsingle_key_arr[1])!='') ? $this->input->post('functional_area_'.$postsingle_key_arr[1]) : '';
								$job_role = ($this->input->post('job_role_'.$postsingle_key_arr[1]) && $this->input->post('job_role_'.$postsingle_key_arr[1])!='') ? $this->input->post('job_role_'.$postsingle_key_arr[1]) : '';
								$joining_date = ($this->input->post('joining_date_'.$postsingle_key_arr[1]) && $this->input->post('joining_date_'.$postsingle_key_arr[1])!='') ? $this->input->post('joining_date_'.$postsingle_key_arr[1]) : '';
								$leaving_date = ($this->input->post('leaving_date_'.$postsingle_key_arr[1]) && $this->input->post('leaving_date_'.$postsingle_key_arr[1])!='') ? $this->input->post('leaving_date_'.$postsingle_key_arr[1]) : '';
								$currency_type = ($this->input->post('currency_type_'.$postsingle_key_arr[1]) && $this->input->post('currency_type_'.$postsingle_key_arr[1])!='') ? $this->input->post('currency_type_'.$postsingle_key_arr[1]) : '';
								$achievements = ($this->input->post('achievements_'.$postsingle_key_arr[1]) && $this->input->post('achievements_'.$postsingle_key_arr[1])!='') ? $this->input->post('achievements_'.$postsingle_key_arr[1]) : '';
								/*$a_salary_lacs = ($this->input->post('a_salary_lacs_'.$postsingle_key_arr[1]) && $this->input->post('a_salary_lacs_'.$postsingle_key_arr[1])!='') ? $this->input->post('a_salary_lacs_'.$postsingle_key_arr[1]) : '0';
								$a_salary_thousand = ($this->input->post('a_salary_thousand_'.$postsingle_key_arr[1]) && $this->input->post('a_salary_thousand_'.$postsingle_key_arr[1])!='') ? $this->input->post('a_salary_thousand_'.$postsingle_key_arr[1]) : '0';
								//$a_salary_concat = $a_salary_lacs.'-'.$a_salary_thousand;
								$a_salary_concat = $a_salary_lacs.'.'.$a_salary_thousand;*/
								
								$a_salary_concat = ($this->input->post('annual_salary_'.$postsingle_key_arr[1]) && $this->input->post('annual_salary_'.$postsingle_key_arr[1])!='') ? $this->input->post('annual_salary_'.$postsingle_key_arr[1]) : '0';
								
								//$data_array_custom = array('js_id'=> $idofemp,'company_name'=>$companyname,'joining_date'=>$joining_date,'leaving_date'=>$leaving_date,'industry'=>$industry,'functional_area'=>$functional_area,'job_role'=>$job_role,'annual_salary'=>$a_salary_concat,'currency_type'=>$currency_type,'achievements'=>$achievements,'update_on'=>$current_datetime);
								
								$data_array_custom = array('company_name'=>$companyname,'joining_date'=>$joining_date,'leaving_date'=>$leaving_date,'industry'=>$industry,'functional_area'=>$functional_area,'job_role'=>$job_role,'annual_salary'=>$a_salary_concat,'currency_type'=>$currency_type,'achievements'=>$achievements,'update_on'=>$current_datetime);
								
								$where_array = array('js_id'=> $idofemp,'id'=>$postsingle_key_arr[1]);
								$this->common_front_model->update_insert_data_common('jobseeker_workhistory',$data_array_custom,$where_array);
							}
						}
					}
				}
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function get_userdetail_frm_id($id="")
	{
		$returnvar = array();
		if($id!='')
		{
			$where_arra = array('id'=>$id);
			$returnvar = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
		}
		return $returnvar;
	}
	public function delete_info($id="")
	{
		$user_id_from_sess = $this->common_front_model->get_userid();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "NI-WEB"; 
		if($id!='')
		{
			$section = $this->input->post('section');
			$id = $this->input->post('id');
			$count = $this->input->post('count');
			if($section=='edu')
			{
				$tabel = 'jobseeker_education';
			}
			if($section=='work')
			{
				$tabel = 'jobseeker_workhistory';
			}
			if($section=='lang')
			{
				$tabel = 'jobseeker_language';
			}
			if($section=='resume')
			{
				$tabel = 'jobseeker';
			}
			if($section=='resume')
			{
				//$this->load->helper('file');
				$base_url = $this->common_front_model->base_url;
				$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1);
				if(file_exists($upload_path.'/'.$id))
				{
					$delete_file = unlink($upload_path.'/'.$id);
				}
				else
				{
					$where_arra = array('id'=>$user_id_from_sess,'resume_file!='=>'');//'id='.$user_id_from_sess.' and  resume_file!=""';
					$check_if_in_tab_exists = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,0,'','','','','');
					if($check_if_in_tab_exists=='1')
					{
						$delete_file = true;
					}
				}
				if(isset($delete_file) && $delete_file && isset($tabel))
				{
					$data_array_custom = array('resume_file'=>'');
					//$this->common_front_model->save_update_data($tabel,$data_array_custom,'id','add','','','1','1','is_deleted',2);
					$where_array = array('id'=> $user_id_from_sess,'resume_file'=>$id);
					$this->common_front_model->update_insert_data_common($tabel,$data_array_custom,$where_array);
					//echo $this->db->last_query();
					
				}
				else
				{
					return 'error';
				}
			}
			else
			{
				$where_arra = $this->db->where('id', $id);
				$this->db->delete($tabel); 
			}
			
		}
		return 'success';
	}
	public function checkifadded($tentwilve="10",$get_acutual='tf')
	{
		$returnvar = true;
		$posted_id = $this->common_front_model->get_userid();
		
		if($tentwilve=='10')
		{
			$where_arra = array('js_id'=>$posted_id,'TRIM(is_certificate_X_XII)'=>'X');
		}
		if($tentwilve=='12')
		{
			$where_arra = array('js_id'=>$posted_id,'TRIM(is_certificate_X_XII)'=>'XII');
		}
		if($tentwilve=='Certi')
		{
			$where_arra = array('js_id'=>$posted_id,'TRIM(is_certificate_X_XII)'=>'Certi');
		}
		if($tentwilve=='Edu')
		{
			$where_arra = array('js_id'=>$posted_id,'TRIM(is_certificate_X_XII)'=>'Edu');
		}
		$data_arr = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arra,0,'id','','',"",'');
		if($data_arr > 0)
		{
			$returnvar = false;
		}
		if($get_acutual=='tf')
		{
			return $returnvar;	
		}
		elseif($get_acutual=='data')
		{
			$data_arr_data = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arra,1,'id','','',"",'');
			return $data_arr_data;
		}
		else
		{
			//$data_arr_data = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arra,1,'id','','',"",'');
			return $data_arr;
		}
		
	}
	public function delete_pro_req_chk($id='')
	{
		$del_req_count = 0;
		if($id!='')
		{
			$where_arra = array('req_id'=>$id,'user_type'=>'js');
			$del_req_count = $this->common_front_model->get_count_data_manual('delete_profile_request',$where_arra,0,'id','id desc','','','');
		}
		return $del_req_count;
	}
	public function delete_pro_req($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_userdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$reason_for_del = ($this->input->post('message')) ? $this->input->post('message') : ""; 
				$check_if_req_exitst = $this->delete_pro_req_chk($idofemp);
				if(!($check_if_req_exitst > 0))
				{
					$data_array_custom = array('req_id'=> $idofemp,'user_type'=>'js','reason_for_del'=>$reason_for_del);
					$this->common_front_model->save_update_data('delete_profile_request',$data_array_custom,'id','add','','','1','1','is_deleted',2);
					$datavar['result']="success";
				}
				else
				{
					$datavar['result']="already_there";	
				}
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function checkif_certi_added()
	{
		$posted_id = $this->common_front_model->get_userid();
		$where_arra = array('js_id'=>$posted_id,'TRIM(is_certificate_X_XII)'=>'Certi');
		$data_arr = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arra,0,'id','','',"",'');
		return $data_arr;
	}
	public function get_suggestion_lang($search,$return)
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"skill_language like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('skill_language_master',$where_arra,2,'skill_language,id','','1',10,"");
		//echo $this->common_front_model->last_query(); 
		$opt_array = array();
			
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					$opt_array[] = array('value'=>$data_arr_val[$return],'label'=>$data_arr_val['skill_language']);
				}
			}
			return $opt_array;
	}
	public function get_suggestion_lang_select2()
	{
		$search = $this->input->post('q');
		$return='skill_language';
		
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"skill_language like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('skill_language_master',$where_arra,2,'skill_language,id','','1',10,"");
		//$opt_array = array();
		$opt_array['results'] = array();
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					//$opt_array[] = array('value'=>$data_arr_val[$return],'label'=>$data_arr_val['skill_language']);
					//$opt_array['results'] = array('id'=>$data_arr_val[$return],'text'=>$data_arr_val['skill_language']);
					$forpushingarray = array("id"=>$data_arr_val[$return],"text"=>$data_arr_val['skill_language']);
					array_push($opt_array['results'],$forpushingarray);
					
				}
			}
			$opt_array['more'] = "false";
			return $opt_array;
	}
	function getprofile_completeness($id='')
	{
		$returnvar = 0;
		$tstvar = 0;
		$field_and_per = array(
		'email'=>'5',
		//'verify_email'=>'5',
		'title'=>'2',
		'fullname'=>'4',
		'gender'=>'4',
		'birthdate'=>'2',
		'marital_status'=>'2',
		'address'=>'2',
		'city'=>'3',
		'country'=>'3',
		'mobile'=>'5',
		'pincode'=>'2',
		'profile_pic'=>'7',
		'profile_summary'=>'5',
		'preferred_city'=>'3',
		'resume_headline'=>'2',
		'total_experience'=>'4',
		'annual_salary'=>'2',
		'industry'=>'3',
		'functional_area'=>'3',
		'job_role'=>'3',
		'key_skill'=>'3',
		'email_verification'=>'5',
		'resume_file'=>'4',
		'resume_verification'=>'4',
		'desire_job_type'=>'3',
		'prefered_shift'=>'2',
		'expected_annual_salary'=>'4',
		'facebook_url'=>'1',
		'gplus_url'=>'1',
		'twitter_url'=>'1',
		'linkedin_url'=>'1'
		); 
		
		$education_count = $this->my_profile_model->getdetailsfromid('js_education_view','js_id',$id,'id','count');
		if($education_count > 0)
		{
				$returnvar = $returnvar + 5;// for setting wiightage of education
				$tstvar = $tstvar + 5;// testvar increment
		}
		$language_count = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$id,'id','count');	
		if($language_count > 0)
		{
				$returnvar = $returnvar + 5;// for setting wiightage of education
				$tstvar = $tstvar + 5;// testvar increment
		}
		/* for checking the value  */
		/*foreach($field_and_per as $single_field_and_per)
		{
			$tstvar = $tstvar + $single_field_and_per;
		}
		echo $tstvar;*/
		/* for checking the value end */
		if($id!='')
		{
			$where_arra = array('id'=>$id);
			$jobseeker_count = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,0,'id','id desc','','','');
			if($jobseeker_count > 0 && $jobseeker_count > 0)
			{
				$jobseeker_details = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,1,'','id desc','','','');
				foreach($jobseeker_details as $single_field=>$single_value)
				{
					if (array_key_exists($single_field,$field_and_per))
					{
						if(isset($jobseeker_details[$single_field]) && $jobseeker_details[$single_field]!='' && !is_null($jobseeker_details[$single_field]) && $jobseeker_details[$single_field]!='0')
						{
							if($single_field=='verify_email' || $single_field=='email_verification')
							{
								if($jobseeker_details[$single_field]=='Yes')
								{
									$returnvar = $returnvar + $field_and_per[$single_field];	
								}
							}
							else
							{
								$returnvar = $returnvar + $field_and_per[$single_field];	
							}
							unset($field_and_per[$single_field]);
						}
					}
				}
			}
		}
		if($returnvar > 100)
		{
			return 100;
		}
		else
		{
			return $returnvar;
		}
	}
	
	function send_top_varify()
	{
		$user_agent =$this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		$response = 'error';
		$otp = $this->session->userdata('otp_varify');
		if($otp =='')
		{
			$otp = rand(1000,9999);
		}
		$table_name = 'jobseeker';
		$user_id = $this->common_front_model->get_userid();
		if($user_id =='')
		{
			$user_id = $this->common_front_model->get_empid();
			$table_name = 'employer_master';
		}
		if($user_id !='' && $table_name !='')
		{
			$user_data = $this->common_front_model->get_user_data($table_name,$user_id,'mobile,mobile_verified',$id_f ='id');
			if(isset($user_data['mobile_verified']) && $user_data['mobile_verified'] =='No' && isset($user_data['mobile']) && $user_data['mobile'] !='')
			{
				$this->session->set_userdata('otp_varify',$otp);
				$sms = "Your OTP $otp for verify your mobile, Please verify your mobile number on ".base_url();
				$this->common_front_model->common_sms_send($user_data['mobile'],$sms);
				$response = 'success';
			}
		}
		if($response == 'success')
		{
			$returnvar['status'] = 'success';
			$returnvar['error_meessage'] = 'Your OTP sent on your mobile('.$user_data['mobile'].')';
			if($user_agent=='NI-AAPP')
			{
				$returnvar['generate_OTP'] = $otp;
			}
		}
		else
		{
			$returnvar['status'] = 'error';
			$returnvar['error_meessage'] = 'Some error Occured, please try again.';
		}
		$returnvar['tocken'] = $this->security->get_csrf_hash();
		return $returnvar;
	}
	function varify_mobile_otp()
	{
		$response = 'Please try again.';
		$otp_varify = $this->session->userdata('otp_varify');
		$otp_mobile = ($this->input->post('otp_mobile')) ? $this->input->post('otp_mobile') : "";
		if($otp_mobile =='')
		{
			$response = 'Please enter OTP sent on your mobile number';
		}
		else if($otp_mobile !='' && $otp_mobile != $otp_varify)
		{
			$response = 'Please enter Valid OTP sent on your mobile number';
		}
		else
		{
			$data_array_custom = array('mobile_verified'=>'Yes');
			$table_name = 'jobseeker';
			$user_id = $this->common_front_model->get_userid();
			if($user_id =='')
			{
				$user_id = $this->common_front_model->get_empid();
				$table_name = 'employer_master';
			}
			if($user_id !='' && $table_name !='')
			{
				$this->common_front_model->update_insert_data_common($table_name,$data_array_custom,array('id'=>$user_id));
				$response = 'success';
				$this->session->unset_userdata('otp_varify');
			}
		}
		return $response;
	}
}
?>