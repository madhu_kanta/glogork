<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function facebook_gplus_login_for_android()
	{
		$login_dt = $this->common_front_model->getCurrentDate(); /* must save date and time on datetime field*/
		$login_by = $this->input->post('login_by'); /* check login by facebbok or gplus*/
		$return_id = $this->input->post('return_id'); /* Facebook id or gplus id*/
		$email = $this->input->post('email');
		
		if($login_by!='' && ($return_id!='' || $email!=''))
		{
			  if($email!='')
			   {
					$chkemail=" or email='".$email."'";
			   }
			   else
			   {
					$chkemail= "";   
			   }
				 if($login_by=='facebook')
				 {
					 $check_id = "facebook_id='".$return_id."'";
				 }
				 else
				 {
					 $check_id = "gplus_id='".$return_id."'";
				 }
		   $where = "($check_id $chkemail) AND is_deleted!='Yes'";
		   $this->db->where($where);
		   $query = $this->db->get('jobseeker');
		   $reg_data = $query->row_array();
		   if($query->num_rows() > 0 && is_array($reg_data) && count($reg_data) > 0)
         	{
				if($reg_data['status']!='UNAPPROVED')
		     	{
					$data['status'] = 'success';
					$data['err_message'] = 'Login successfull.';
					$data['user_id'] = $reg_data['id'];
					
					$this->db->set('last_login', $login_dt);
					$this->db->where('id', $reg_data['id']);
					$this->db->limit(1);
					$this->db->update('jobseeker');
				
				}
				else
				{
					$data['status'] = 'error';
					$data['err_message'] = $this->lang->line('inactive_login_account');
				}
			}
			else
			{
				$data['status'] = 'error';
				$data['err_message'] = 'This email id is not registered with us.';
			}
		
		}
		else
		{
			$data['status'] = 'error';
			$data['err_message'] = 'This email id is not registered with us.';
		}
		return $data;
	}
	
	public function login()
	{
		$login_dt = $this->common_front_model->getCurrentDate(); // must save date and time on datetime field
		$this->load->library('encryption');
        $this->load->library('phpass');
		$username = trim($this->input->post('username'));
		$password = trim($this->input->post('password'));
		$remember_details = $this->input->post('rememberme'); 
		$where = array("email"=>$username,"is_deleted"=>'No');
		if($remember_details!='' && $remember_details == 'Yes')
		{
			if($this->encryption->decrypt($password) && $this->encryption->decrypt($password)!='')
			{
		      $password =  $this->encryption->decrypt($password);
			}
			else
			{
				$password = $password;
			}
		}

		$count = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','id');
	
		if($count !='' && $count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','id,email,fullname,gender,status,password');
			$user_hashed_pass = $user_data['password'];
			
           if($this->phpass->check($password,$user_hashed_pass))
			{
				if(isset($user_data['status']) && $user_data['status'] =='APPROVED')
				{
					$this->session->unset_userdata('login_attempt');
					$user_data_array = array(
						 'user_id'	=> $user_data['id'],
						 'full_name'	=> $user_data['fullname'],
						 'email'		=> $user_data['email'],
						 'gender'	=> $user_data['gender'],
						 );
					$this->session->set_userdata('jobportal_user', $user_data_array);		 
					$data['errmessage'] =  $this->lang->line('login_success_msg');
					$data['status'] =  'success';
					$this->session->unset_userdata('jobportal_employer');
					if($this->input->post('device_id') && $this->input->post('device_id')!='')
					{
						$this->db->set('device_id', $this->input->post('device_id'));	
					}
					$this->db->set('last_login', $login_dt);
					$this->db->where('id', $user_data['id']);
					$this->db->limit(1);
					$this->db->update('jobseeker');
				
					if($remember_details!='' && $remember_details == 'Yes')
					{
						// set user login details cookie
						$data_arr = array("username"=>$username,"password"=>$this->encryption->encrypt($password));
						$data_arr = json_encode($data_arr);
						$cookie = array(
							'name'   => 'js_login_detail',
							'value'  => $data_arr,
							'expire' => '864000'
						);
						delete_cookie('js_login_detail');
						$this->input->set_cookie($cookie);
						// user login details cookie  end
					}
				}
				else
				{
					 $data['errmessage'] = $this->lang->line('inactive_login_account');
					 $data['status'] =  'error';
				}
			}
			else
			{
				$data['errmessage'] = $this->lang->line('wrong_login_detail');
			    $data['status'] =  'error';
				
				if($this->session->userdata('login_attempt')!='')
				{
					$login_attempt = $this->session->userdata('login_attempt');
					$new_attempt = array('login_attempt' => $login_attempt + 1);
					$this->session->set_userdata($new_attempt);
				}
				else
				{
					$login_attempt = array('login_attempt' => 1);
					$this->session->set_userdata($login_attempt);
				}
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('wrong_login_detail');
			$data['status'] =  'error';
			if($this->session->userdata('login_attempt')!='')
			{
				$login_attempt = $this->session->userdata('login_attempt');
				$new_attempt = array('login_attempt' => $login_attempt + 1);
				$this->session->set_userdata($new_attempt);
			}
			else
			{
				$login_attempt = array('login_attempt' => 1);
				$this->session->set_userdata($login_attempt);
			}
		}
		$data['login_attempt'] =  $this->session->userdata('login_attempt');
		return $response = $data;
	}
	public function verify_member($cpass='',$email='')
	{
		if($email!='' && $cpass!='')
		{
			/*$this->load->library('encryption');
            $this->load->library('phpass');
			*/
			$where = array('email'=>$email,'email_ver_str'=>$cpass);
			$check_data = $this->common_front_model->get_count_data_manual("jobseeker",$where,1,"id,email,mobile,status,password");						
			
			
			/*if($this->encryption->decrypt($cpass) && $this->encryption->decrypt($cpass)!='')
			{
		      $password =  $this->encryption->decrypt($cpass);
			}*/
			
			if(isset($check_data)  && $check_data!='' && is_array($check_data) &&  count($check_data) > 0)
			{
				/*$user_hashed_pass = $check_data['password'];
				&& $this->phpass->check($password,$user_hashed_pass)*/
				if(count($check_data) > 0 )
				{  
					$status = $check_data['status'];
					if($status=='UNAPPROVED')
						{ 
							$update = array(
											'verify_email'=>'Yes','status'=>'APPROVED'
											);
							//$where = array("email"=>$email);	
							$where = array("id"=>$check_data['id'],"email"=>$email);
							$this->common_model->update_insert_data_common("jobseeker",$update,$where,1);
							$this->session->set_flashdata('user_log_out', $this->lang->line('acc_activeted'));
							if($check_data['mobile']!='')
							{
								$get_sms_temp = $this->common_front_model->get_sms_template('Account activated');
								if(isset($get_sms_temp) && $get_sms_temp !='' && is_array($get_sms_temp) && count($get_sms_temp) > 0)
								{
									$sms_template = htmlspecialchars_decode($get_sms_temp['sms_content'],ENT_QUOTES); 
									$trans = array("xxxx"=>$check_data['email']);
									$sms_template = $this->common_front_model->sms_string_replaced($sms_template, $trans);
									$this->common_front_model->common_sms_send($check_data['mobile'],$sms_template);	
								}
							}
							redirect($this->common_front_model->base_url.'sign-up/login');
						}	
						else
						{
							$this->session->set_flashdata('user_log_out', $this->lang->line('acc_already_activeted'));
							redirect($this->common_front_model->base_url.'sign-up/login');
						}
				}
				else
				{
					$this->session->set_flashdata('user_log_error',$this->lang->line('error_in_activation'));
					redirect($this->common_front_model->base_url.'sign-up/login');
				}
			}
			else
			{
				$this->session->set_flashdata('user_log_error',$this->lang->line('error_in_activation'));
				redirect($this->common_front_model->base_url.'sign-up/login');
			}
		}
		else
		{
			$this->session->set_flashdata('user_log_error',$this->lang->line('error_in_activation'));
			redirect($this->common_front_model->base_url.'sign-up/login');
		}
	}
	public function reset_forgot_password()
	{
		$email = $this->input->post('email');
		$where = array('email'=>$email,'is_deleted'=>'No');
		$count = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','id');
		if($count !='' && $count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','id,email,fullname,status,password');
			$reandompass = $this->generateRandomString('15');
			$data_array = array('email_ver_str'=>$reandompass);
			
			$this->common_front_model->update_insert_data_common('jobseeker',$data_array,$where);
		//	echo $this->db->last_query();
			$get_email = $this->common_front_model->getemailtemplate('Jobseeker password reset');
			if(isset($get_email) && $get_email !='' && is_array($get_email) && count($get_email) > 0)
			{
				$config_data = $this->common_front_model->data['config_data'];
				$webfriendlyname = $config_data['web_frienly_name'];
				$subject = $get_email['email_subject'];
				$email_content= $get_email['email_content'];
				$trans_sub = array("webfriendlyname" =>$webfriendlyname);
				$subject = $this->common_front_model->getstringreplaced($subject, $trans_sub);
					
				$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES);
				$trans = array("webfriendlyname" =>$webfriendlyname,"websitename" =>$webfriendlyname,"yourname"=>$user_data['fullname'],"email_id"=>$user_data['email'],"/cpass"=>$reandompass,"site domain name"=>$this->common_front_model->data['base_url'].'login/restetpasstonew_js/');
				$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);	
			  
			  // $this->common_front_model->common_send_email('ketan@narjisenterprise.com',$subject,$email_template);
				$this->common_front_model->common_send_email($email,$subject,$email_template);
			}
		   $status = 'success';
		   $return_message = $this->lang->line('forgot_password_success');
		}
		else
		{
			$status = 'error';
			$return_message = $this->lang->line('forgot_password_error');
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message,'token'=>$this->security->get_csrf_hash());
		return json_encode($return_arr);
	}
	
	public function reset_new_password($cpass='',$email='')
	{
		$where = array('email'=>$email,'is_deleted'=>'No','status'=>'APPROVED','email_ver_str'=>$cpass);
		$count = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','id','','','','');
		
		if($count!='' && $count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','id,email,fullname,status,password');
			$this->load->library('phpass');
			$this->load->helper('string');
			$pass = $this->input->post('pass');
			$password = $this->phpass->hash($pass);
			$reandompass = $this->generateRandomString('15');
			$update = array('password'=>$password,'email_ver_str'=>$reandompass);
			$where = array("email"=>$email,'is_deleted'=>'No');
			$this->common_model->update_insert_data_common("jobseeker",$update,$where,1);
			//echo $this->db->last_query();
			$email_temp_data = $this->common_front_model->getemailtemplate('Job seeker password reset successfull');
			if(isset($email_temp_data) && $email_temp_data !='' && is_array($email_temp_data) && count($email_temp_data) > 0)
			{
				$config_data = $this->common_front_model->data['config_data'];
				$webfriendlyname = $config_data['web_frienly_name'];
				$subject = $email_temp_data['email_subject'];
				$email_content = $email_temp_data['email_content'];
				$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
				$trans = array("webfriendlyname" =>$webfriendlyname,"yourname"=>$user_data['fullname'],"websitename" =>$webfriendlyname);
				$trans_sub = array("webfriendlyname" =>$webfriendlyname);
				$subject = $this->common_front_model->getstringreplaced($subject, $trans_sub);
				$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
				$this->common_front_model->common_send_email($user_data['email'],$subject,$email_template);
			}
			$status = 'success';
		   	$return_message = $this->data['custom_lable']->language['changed_password_success'];
		}
		else
		{
			$status = 'error';
			$return_message = $this->data['custom_lable']->language['reset_pass_fail_js'];
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message);
		return $return_arr;
	}
	
	}
?>