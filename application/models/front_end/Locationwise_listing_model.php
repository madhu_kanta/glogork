<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Locationwise_listing_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function getcity_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$getcity_count = $this->common_front_model->get_count_data_manual('city_master',$where_arra,0,'id','','','','');
		/*$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$this->db->group_by('job_industry');
		$this->db->group_by('functional_area');
		$totaljobs_groupbyindcount = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,0,'id','','','','');*/
		return $getcity_count;
	}
	public function getcitydata($where_arra=array(),$page=1,$limit=5,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$getcitydata = $this->common_front_model->get_count_data_manual('city_master',$where_arra,2,$req_field_comma,'city_name asc',$page,$limit,'');
		//$getcompaniesdata = $this->common_front_model->get_count_data_manual('city_master',$where_arra,2,$req_field_comma,'','','','');
		return $getcitydata;
	}
	public function getallcitywherejobposted()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		$getallcitywherejobposted = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,2,'location_hiring','','','','');
		return $getallcitywherejobposted;
	}
	public function getcityactivejob_count($idofcity = '')
	{
		$where_arra = array();
		$getcityactivejob_count = 0;
		if($idofcity != '' && $idofcity!='0' && !is_null($idofcity))
		{
			$where_arra[] = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
			$current_data = $this->common_model->getCurrentDate('Y-m-d');
			$where_arra[] = " job_expired_on >= '$current_data' ";
			$where_arra[] = "FIND_IN_SET (".$idofcity.",location_hiring) ";
			$getcityactivejob_count = $this->common_front_model->get_count_data_manual('job_posting',$where_arra,0,'id','','','','');
		}
		return $getcityactivejob_count;
	}
	public function getnameidandpostjobcount($idofcity = '')
	{
		$getnameidandpostjobcount = array();
		if($idofcity!='')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','id'=>$idofcity);
			$getnameidandpostjobcount = $this->common_front_model->get_count_data_manual('city_master',$where_arra,1,'city_name,id','','','','');
			if(isset($getnameidandpostjobcount) && $getnameidandpostjobcount !='' && is_array($getnameidandpostjobcount))
			{
				$getnameidandpostjobcount['job_count_of_city'] = $this->getcityactivejob_count($idofcity);
			}
		}
		return $getnameidandpostjobcount;
	}
}
?>