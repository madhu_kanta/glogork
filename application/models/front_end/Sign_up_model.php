<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sign_up_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
       
	public function add_account_detail()
	{
		$this->load->library('encryption');
		$this->load->library('phpass');
		$password = $this->input->post('password');
		$email= $this->input->post('email');
		$fullname= $this->input->post('fullname');
		$hashed_pass = $this->phpass->hash($password);
		$reg_date = $this->common_front_model->getCurrentDate(); // must save date and time on datetime field
		$reandompass = $this->generateRandomString('15');
		$social_image_name = trim($this->input->post('social_image_name'));
		$data_array_custom = array('password'=> $hashed_pass,'register_date'=>$reg_date,'email_ver_str'=>$reandompass,'verify_email'=>'No','profile_pic'=>$social_image_name);
		$this->common_front_model->save_update_data('jobseeker',$data_array_custom);
		$newreg = array('reg_index_id' => $this->db->insert_id());
		$this->session->set_userdata($newreg);
		
		
		$get_email = $this->common_front_model->getemailtemplate('Job Seeker Registration');
		///print_r($get_email);
		if($get_email!='' && is_array($get_email) && count($get_email) > 0 )
		{  //echo $email;
		   $config_data = $this->common_front_model->data['config_data'];
		   $webfriendlyname = $config_data['web_frienly_name'];
		   $subject = $get_email['email_subject'];
		   $email_content= $get_email['email_content'];
		   $email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
		   $trans = array("websitename" =>$webfriendlyname,"yourname"=>$fullname,"email_id"=>$email,"/cpass"=>$reandompass.'',"site domain name"=>$this->common_front_model->data['base_url'].'login/verify_member/');
		   $email_template = $this->common_front_model->getstringreplaced($email_template, $trans);	
		   //echo $email_template;
		  //$this->common_front_model->common_send_email('ketanparmar583@gmail.com',$subject,$email_template);	
		   $this->common_front_model->common_send_email($email,$subject,$email_template);
		}
		
		return	"success";
	}
	
	public function add_personal_detail()
	{
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		else
		{ 
			$js_id = '10'; /*Just for test*/
		}
		$mobile_c_code = $this->input->post('mobile_c_code');
		$mobile = $this->input->post('mobile');
		$mobile_no = $mobile_c_code.'-'.$mobile;
		///$dob = explode('/', $this->input->post('birthdate'));
		//$brithday = $dob['2'].'-'.$dob['1'].'-'.$dob['0']; 
		$brithday = $this->input->post('birthdate');
		$data_array_custom = array('mobile'=> $mobile_no,'birthdate'=>$brithday);
		$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$js_id);
		return	"success";
	}
	
	public function add_education_detail()
	{
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		else
		{ 
			$js_id = '10'; /*Just for test*/
		}

		$response_added_work_array  = array();
		$form_certi_id = array();
		$form_edu_id = array();
		$update_on = $this->common_front_model->getCurrentDate(); 
		$no_of_edu_row = 10; //$this->input->post('no_of_edu_row');
		for($i=1;$i <= $no_of_edu_row ; $i++)
		{ 
		     if(isset($_POST['institute'.$i]))
			  {
				$is_certificate_X_XII = 'Edu';
				$institute = $this->input->post('institute'.$i.''); 
				$qualification_level = $this->input->post('qualification_level'.$i.'');
				if($qualification_level=='class-X' || $qualification_level=='class-XII')
				{ 
					if($qualification_level=='class-X')
					{
						$is_certificate_X_XII = 'X';
					}
					else if($qualification_level=='class-XII')
					{
						$is_certificate_X_XII = 'XII';
					}
					$qualification_level = '0';	
				}
				
				$specialization = $this->input->post('specialization'.$i.'');
				$marks = $this->input->post('marks'.$i.'');
				$passing_year = $this->input->post('passing_year'.$i.'');
				$data_array = array(
									'js_id'=>$js_id,
									'passing_year'=>$passing_year,
									'qualification_level'=>$qualification_level,
									'institute'=>$institute,
									'specialization'=>$specialization,
									'marks'=>$marks,
									'is_certificate_X_XII'=>$is_certificate_X_XII, 
									'update_on'=>$update_on,
									);
			  if($this->input->post('form_edu_id'.$i.'')!='')
			  {
				  $where_arr = array('id'=>$this->input->post('form_edu_id'.$i.''));
				  $this->common_front_model->update_insert_data_common('jobseeker_education',$data_array,$where_arr,1);
				  $form_edu_id[$i] = $this->input->post('form_edu_id'.$i.'');  
			  }
			  else
			  {
				  $this->common_front_model->update_insert_data_common('jobseeker_education',$data_array,'',0);
				  $form_edu_id[$i] = $this->db->insert_id();  
			  }
			}
		}
		
		$no_of_certi_row = 10; //$this->input->post('no_of_certi_row');
		if($no_of_certi_row > 0 && $no_of_certi_row!='')
		{
			for($i=1;$i <= $no_of_certi_row ; $i++)
			{ 
				if(isset($_POST['cerificate_name'.$i]))
				{
					
					
			 	$cerificate_name = $this->input->post('cerificate_name'.$i.''); 
			    $cerificate_desc = $this->input->post('cerificate_desc'.$i.'');
			    $passing_year_certi = $this->input->post('passing_year_certi'.$i.'');
			    $data_array = array(
								'js_id'=>$js_id,
								'passing_year'=>$passing_year_certi,
								'institute'=>$cerificate_name,
								'specialization'=>$cerificate_desc,
								'is_certificate_X_XII'=>'Certi',
								'update_on'=>$update_on,
								);
			if($this->input->post('form_certi_id'.$i.'')!='')
		     {
				  $where_arr = array('id'=>$this->input->post('form_certi_id'.$i.''));
				  $this->common_front_model->update_insert_data_common('jobseeker_education',$data_array,$where_arr,1);
				  $form_certi_id[$i] = $this->input->post('form_certi_id'.$i.'');  
			  }	
			  else
			  {
				  $this->common_front_model->update_insert_data_common('jobseeker_education',$data_array,'',0);
				  $form_certi_id[$i] = $this->db->insert_id();  
			  }			
				
				}
		   }
		}
	
	$facebook_url = '';
	$gplus_url = '';
	$twitter_url = '';
	$linkedin_url = '';
	if($this->input->post('name_facebook_url')!='')
	{	
	  $facebook_url = $this->input->post('name_facebook_url');
	}
	if($this->input->post('name_gplus_url')!='')
	{	
	  $gplus_url = $this->input->post('name_gplus_url');
	}
	if($this->input->post('name_twitter_url')!='')
	{	
	  $twitter_url = $this->input->post('name_twitter_url');
	}
	if($this->input->post('name_linkedin_url')!='')
	{	
	  $linkedin_url = $this->input->post('name_linkedin_url');
	}
	
	 $social_array = array(
						'facebook_url' => $facebook_url,
						'gplus_url'=>$gplus_url,
						'twitter_url'=>$twitter_url,
						'linkedin_url'=>$linkedin_url
						);
	$where = array('id'=>$js_id);
	$this->common_front_model->update_insert_data_common('jobseeker',$social_array,$where,1);	
		$response_added_work_array['status'] ='success';
		$response_added_work_array['form_edu_id'] = $form_edu_id;
		$response_added_work_array['form_certi_id'] = $form_certi_id;
		//$response_added_work_array['form_edu_id'] = $form_edu_id; 
		return $response_added_work_array;	
	}
	
	public function add_work_exp_detail()
	{
		//$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		else
		{ 
			$js_id = '10'; /*Just for test*/
		}
		$response_work=array();
		$no_of_work_exp_row = $this->input->post('no_of_work_exp_row');//$this->input->post('no_of_work_exp_row');
		$year =array();
		$month =array();
		$exp_year = '';
		$exp_month = '';
		//$form_work_id = '';
		for($i=1;$i <= $no_of_work_exp_row ; $i++)
		{// echo $this->input->post('company_name'.$i.'');
		
		  /* if($this->input->post('company_name'.$i) && $this->input->post('industry'.$i) && $this->input->post('functional_area'.$i) && $this->input->post('leaving_date'.$i))*/
		  
		 
		  $company_name1231 = $this->input->post('company_name'.$i); 
		  $industry1231 = $this->input->post('industry'.$i); 
		  $functional_area1231 = $this->input->post('functional_area'.$i); 
		  $leaving_date1231 = $this->input->post('leaving_date'.$i); 
		  
		  if(isset($company_name1231) && isset($industry1231) && isset($functional_area1231))
		  {

			$company_name = $this->input->post('company_name'.$i.''); 
			$industry = $this->input->post('industry'.$i.'');
			$functional_area = $this->input->post('functional_area'.$i.'');
			$joining_date = $this->input->post('joining_date'.$i.'');
			$leaving_date = $this->input->post('leaving_date'.$i.'');
			$job_role = $this->input->post('job_role'.$i.'');
			/*$currency_type = $this->input->post('currency_type'.$i.'');
			$annual_salary_lacs = $this->input->post('annual_salary_lacs'.$i.'');
			$annual_salary_thousand = $this->input->post('annual_salary_thousand'.$i.'');
			//$salary = $annual_salary_lacs .'-'.$annual_salary_thousand;
			//$salary = $annual_salary_lacs .'.'.$annual_salary_thousand;*/
			$salary = $this->input->post('annual_salary'.$i.'');
			$achievements = $this->input->post('achievements'.$i.'');
			if(isset($achievements) && $achievements!=''){
				$achievements1= $achievements; 
			}else{
				$achievements1 = '';
			}
			$update_on = $this->common_front_model->getCurrentDate(); 
			$data_array = array(
								'js_id'=>$js_id,
								'company_name'=>$company_name,
								'joining_date'=>$joining_date,
								'leaving_date'=>$leaving_date,
								'industry'=>$industry,
								'functional_area'=>$functional_area,
								'job_role'=>$job_role,
								'annual_salary'=>$salary,
								//'currency_type'=>$currency_type,
								'achievements'=>$achievements1,
								'update_on'=>$update_on,
								); 
				
					
				$tests = $this->input->post('form_work_id'.$i.'');
				
			if(isset($tests) && $tests!='')
		     {
				  $where_arr = array('id'=>$this->input->post('form_work_id'.$i.''));
				  $this->common_front_model->update_insert_data_common('jobseeker_workhistory',$data_array,$where_arr,1);
				  
				  $form_work_id[$i] = $this->input->post('form_work_id'.$i.'');  
			  }	
			  else
			  {
				$this->common_front_model->update_insert_data_common('jobseeker_workhistory',$data_array,'',0);
				 $form_work_id[$i] = $this->db->insert_id();
			
			  }
		   $leave_date_now = date('Y-m-d');	  						
		   $join_date = new DateTime($joining_date);
		   $leave_date = new DateTime($leave_date_now);
		   if($leaving_date!='')
		   { 
		   	 $leave_date = new DateTime($leaving_date);
		   }
		   
		   $diff = $join_date->diff($leave_date);
		   $year[] =  $diff->y;
		   $month[] =  $diff->m;
			
		   }
		}
		$exp_year = array_sum($year);
		$exp_month = array_sum($month);
		if($exp_month > 11)
		{
			$exp_month_new = $exp_month - 11;
			$exp_year_new = $exp_year + 1;
			 if($exp_month_new > 11)
			 {
				$exp_month = $exp_month_new - 11;
			    $exp_year = $exp_year_new + 1;
			 }
			 else
			 {
				 $exp_month = $exp_month_new;
				 $exp_year = $exp_year_new;
			 }
		}
		$response_work['status'] ='success';
		$response_work['form_work_id'] = $form_work_id;
		$response_work['exp_year'] = $exp_year;
		$response_work['exp_month'] = $exp_month;
		return $response_work;
	}
	
	public function js_extra_details()
	{
		
		$user_agent =$this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		else
		{ 
			$js_id = '10'; /*Just for test*/
		}
		$exp_year = $this->input->post('exp_year');
		$exp_month = $this->input->post('exp_year');
		//$total_exp = $exp_year.'-'.$exp_month;
		$total_exp = $exp_year.'.'.$exp_month;
		
		/*$a_salary_lacs = $this->input->post('a_salary_lacs');
		$a_salary_thousand = $this->input->post('a_salary_thousand');
		//$annual_salary = $a_salary_lacs.'-'.$a_salary_thousand;
		$annual_salary = $a_salary_lacs.'.'.$a_salary_thousand;*/
		$annual_salary = $this->input->post('annual_salary');
		
		/*$exp_salary_lacs = $this->input->post('exp_salary_lacs');
		$exp_salary_thousand = $this->input->post('exp_salary_thousand');
		//$exp_annual_salary = $exp_salary_lacs.'-'.$exp_salary_thousand;
		$exp_annual_salary = $exp_salary_lacs.'.'.$exp_salary_thousand;*/
		$exp_annual_salary = $this->input->post('expected_annual_salary');
		
		if($user_agent == 'NI-WEB')
		{
			$preferred_city = implode(',',$this->input->post('preferred_city[]'));
		}
		else
		{
			$preferred_city = $this->input->post('preferred_city');
		}
		$data_array_custom = array('total_experience'=> $total_exp,'annual_salary'=>$annual_salary,'expected_annual_salary'=>$exp_annual_salary,'preferred_city'=>$preferred_city);
		$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$js_id);
		return	"success";
	}
	
	public function upload_profile_pic_resume($upload_data,$upload_file_name)
	{
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		else
		{ 
			$js_id = '10'; /*Just for test*/
		}

		$this->common_front_model->save_update_data('jobseeker',$upload_data,'id','edit',$js_id,'',1,'is_deleted',2);
		return	"success";
	}
	public function edu_qualification_html()
	{
		 $html ='';
		 $field_row = $this->input->post('field_row');
		 if($field_row == '')
		 {
			 $field_row = '1';
		 }
		
		 $total_edu = 10;
		 $check_count = 0;
		 for($c=1;$c <= $total_edu ; $c++)
		 {  
			 if($check_count == 0)
			 {  
				if(isset($_POST['institute'.$c]))
				{
				}
				else
				{   
					$field_row = $c;
					$check_count = 1;
				}
			 }
		 }
		 $current_year = date('Y');
		 
		 
		 $html  .= 
		'
		 <div class="form-group removeclass'.$field_row.'">
		<div class="row"><div class="col-sm-4 nopadding"><h6>'.$this->lang->line('institute_name').': <span class="red-only">*</span></h6><div class="form-group"><input type="text" class="form-control"  data-validation="required" id="institute" name="institute'.$field_row.'" value="" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('institute_name').'" style="border-radius:0px;" /></div></div>
		 
<div class="col-sm-4 nopadding">
<h6>'.$this->lang->line('qualification_lvl').': <span class="red-only">*</span></h6><div class="form-group"><select  class="form-control qualification_lvl'.$field_row.'" style="border-radius:0px;"  data-validation="required" name="qualification_level'.$field_row.'"  id="qualification_level'.$field_row.'">'. $this->common_front_model->get_list('edu_list','str').'
<optgroup class="class-X/XII"  label= '.$this->lang->line('class-X/XII').' >
<option value="class-X" > '.$this->lang->line('class-X').' </option>
<option value="class-XII"> '.$this->lang->line('class-XII').'</option>
</optgroup>
 </select></div></div><div class="col-sm-4 nopadding specialization'.$field_row.'">
										  <h6>'.$this->lang->line('specialization').': <span class="red-only red-only-sp'.$field_row.'">*</span></h6>
										  <div class="form-group">
											<input type="text" class="form-control" data-validation="required,custom"  data-validation-regexp="^([a-zA-Z]+)$" id="specialization'.$field_row.'" name="specialization'.$field_row.'" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('specialization_placeholder').'"  style="border-radius:0px;" />
										  </div>
										</div>
										</div>
										<div class="row">
										<div class="col-sm-4 nopadding">
										  <h6>'. $this->lang->line('percentage').': <span class="red-only">*</span></h6>
										  <div class="form-group">
											<input type="text" class="form-control"  data-validation="required" id="marks" name="marks'.$field_row.'" value="" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('percentage').'"  style="border-radius:0px;" />
										  </div>
										</div>
										<div class="col-md-6 nopadding">
										  <h6>'.$this->lang->line('passing_year').': <span class="red-only">*</span></h6>
										  <div class="form-group">
											<div class="input-group" style="width:65%;">
											  <select class="form-control" id="passing_year" data-validation="required" name="passing_year'.$field_row.'" style="">
												<option value="">'.$this->lang->line('passing_year').'</option>';
												
												for($i=1980;$i < $current_year ; $i++)
												 { 
												 	
													$html.='<option value="'.$i.'">'.$i.'</option>';
												 } 	
												 
										$html .='	</select>
											</div>
										  </div>
										</div>
										
										<input type="hidden" id="form_edu_id'.$field_row.'" name="form_edu_id'.$field_row.'" value="">
										';

																				
	if($field_row !='1')
	{
	$html .='<div class="margin-bottom-20"></div><center><button class="btn btn-danger" type="button" onclick="remove_education_fields('.$field_row.')"> 
	<i class="fa fa-trash-o margin-right-5" aria-hidden="true" style="font-size:18px;"></i> Remove</button></center>
	</div>
	<div class="margin-bottom-20"></div><hr><div class="margin-bottom-20"></div> </div> </div>';
	}
	
	
	
	return $html;						
	}
	
	function certificate_html()
	{
		
		 $html ='';
		 $field_row = $this->input->post('field_row');
		 if($field_row == '')
		 {
			 $field_row = '1';
		 }
		 
		 $total_certi = 10;
		 $check_count = 0;
		 for($c=1;$c <= $total_certi ; $c++)
		 {  
			 if($check_count == 0)
			 {  
				if(isset($_POST['cerificate_name'.$c]))
				{
				}
				else
				{   
					$field_row = $c;
					$check_count = 1;
				}
			 }
		 }
		
		 $current_year = date('Y');
		 $html  .= '
		 <div class="form-group removeclasscerti'.$field_row.'">
		 <div class="row">

<div class="col-sm-4 nopadding"><h6>'.$this->lang->line('cerificate_name').': <span class="red-only">*</span></h6><div class="form-group"><input type="text" class="form-control"  data-validation="required" id="cerificate_name" name="cerificate_name'.$field_row.'" value="" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('cerificate_name').'" style="border-radius:0px;" /></div></div>
		 
									<div class="col-sm-4 nopadding">
										  <h6>'.$this->lang->line('cerificate_desc').': <span class="red-only">*</span></h6>
										  <div class="form-group">
											<input type="text" class="form-control"  data-validation="required" id="cerificate_desc" name="cerificate_desc'.$field_row.'" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('cerificate_desc').'" value="" placeholder="" style="border-radius:0px;" />
										  </div>
										</div>
										
										<div class="col-md-4 nopadding">
										  <h6>'.$this->lang->line('passing_year').': <span class="red-only">*</span></h6>
										 
											<div class="form-group" >
											  <select class="form-control"   id="passing_year_certi" data-validation="required" name="passing_year_certi'.$field_row.'" style="">
												<option value="">'.$this->lang->line('passing_year').'</option>';
												
												for($i=1980;$i < $current_year ; $i++)
												 { 
													$html.='<option value="'.$i.'">'.$i.'</option>';
												 } 	
												 
										$html .='	</select>
											
										  </div>
										</div>
										
										
										<input type="hidden" id="form_certi_id'.$field_row.'" name="form_certi_id'.$field_row.'" value="">
										';
																				
	
	$html .='<div class="margin-bottom-20"></div><center><button class="btn btn-danger" type="button" onclick="remove_certi_fields('.$field_row.')"> 
	<i class="fa fa-trash-o margin-right-5" aria-hidden="true" style="font-size:18px;"></i> Remove certificate</button></center>
	</div>
	<div class="margin-bottom-20"></div><hr><div class="margin-bottom-20"></div>  </div>';
	
	return $html;						
	
	}
	public function work_exp_html()
	{
		 $functional_area = $this->common_front_model->get_list('functional_area_master','str');
		 $industries_master = $this->common_front_model->get_list('industries_master','str');
		 $role_master = $this->common_front_model->get_list('role_master','str');
		 $currency_master = $this->common_front_model->get_list('currency_master','str');
		 $salary_range = $this->common_front_model->get_list('salary_range_list','str');
		 $html_exp ='';
		 $field_row = $this->input->post('field_row');
		 if($field_row == '')
		 {
			 $field_row = '1';
		 }
		$total_work = 10;
		 $check_count = 0;
		 for($c=1;$c <= $total_work ; $c++)
		 {  
			 if($check_count == 0)
			 {  
				if(isset($_POST['company_name'.$c]))
				{
				}
				else
				{   
					$field_row = $c;
					$check_count = 1;
				}
			 }
		 } 
		 
		 $html_exp .='
		  <div class="form-group removeclasswork'.$field_row.'">
		 <div class="margin-top-20"></div>
		 							<div class="row">
		 							<div class="col-sm-4 nopadding">
									  <h6>'.$this->lang->line('company_name').'<span class="red-only"> *</span></h6>
									  <div class="form-group">
										<input type="text" class="form-control" data-validation="required" id="company_name" name="company_name'.$field_row.'" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('company_name').'" value="" style="border-radius:0px;">
									  </div>
									</div>
									
									<div class="col-sm-4 nopadding">
									  <h6>'.$this->lang->line('type_industry').'<span class="red-only"> *</span></h6>
									  <div class="form-group">
										<select name="industry'.$field_row.'" id="industry'.$field_row.'"  data-placeholder="'.$this->lang->line('select_option').'" class="form-control"  data-validation="required" style="border-radius:0px;">';
										
                                         $html_exp.= $industries_master; 
										 $html_exp .= '</select>
									  </div>
									</div>
									
									<div class="col-sm-4 nopadding">
									  <h6>'.$this->lang->line('functional_area').'<span class="red-only"> *</span></h6>
									  <div class="form-group">
                                      <select name="functional_area'.$field_row.'" id="functional_area'.$field_row.'"  data-placeholder="'.$this->lang->line('select_option').'" class="form-control"  data-validation="required"  onChange="dropdownChange('."'functional_area$field_row'".','."'job_role$field_row'".','."'role_master'".');" style="border-radius:0px;">';
                                              $html_exp.=  $functional_area; ;
										      $html_exp .= '</select>
									 </div>
									</div>
									</div>
									
									<div class="margin-top-20"></div>
									<div class="row">
								<div class="col-sm-4 nopadding">
								<div class="form-group">
										<h6>'.$this->lang->line('joining_date').'<span class="red-only"> *</span></h6>
										<div class="input-group">
										  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										  <input type="text" class="form-control datepicker datepicker-join" data-validation="required" id="joining_date'.$field_row.'" name="joining_date'.$field_row.'" onkeydown="return false" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('joining_date').'" data-date-format="yyyy-mm-dd" style="border-radius:0px;">
										</div>
										</div>
									</div>	

									<div class="col-sm-4 nopadding">
									<div class="form-group">
										<h6>'.$this->lang->line('leaving_date').' '.$this->lang->line('leaving_date_update').'</h6>
										<div class="input-group">
										  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										  <input type="text" class="form-control datepicker datepicker-leave"  id="ldate'.$field_row.'" name="leaving_date'.$field_row.'" data-date-format="yyyy-mm-dd" onkeydown="return false"  placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('leaving_date_plc_holder').'" style="border-radius:0px;">
										</div>
										</div>
									</div>	
									<div class="col-sm-4 nopadding">
										<h6>'.$this->lang->line('job_role').'<span class="red-only"> *</span></h6>
										<div class="form-group">
										  
										<select name="job_role'.$field_row.'"  id="job_role'.$field_row.'"  data-placeholder="'.$this->lang->line('select_option').'" class="form-control"  data-validation="required" style="border-radius:0px;">';
										
                                        $html_exp.= $role_master;
										$html_exp .= '</select>
										  
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-sm-4 nopadding">
									  <h6>'.$this->lang->line('annual_salary').'<span class="red-only"> *</span></h6>
									   <div class="form-group">
											<select name="annual_salary'.$field_row.'" id="annual_salary'.$field_row.'"  data-placeholder="'.$this->lang->line('select_option').'" class="form-control"  data-validation="required" style="border-radius:0px;">';
                                         $html_exp.= $salary_range; 
										 $html_exp .= '</select>
									   </div>
									</div>
								
									<div class="col-sm-8 nopadding margin-bottom-20">
									  <h6>'.$this->lang->line('achievements').'</h6>
									  <div class="form-group">
										<textarea class="form-control" id="achievements"  name="achievements'.$field_row.'"  style="border-radius:0;" placeholder="'.$this->lang->line('specify') .' '.$this->lang->line('achievements').'"></textarea>
									  </div>
									</div>
									</div>
									<input type="hidden" id="form_work_id'.$field_row.'" name="form_work_id'.$field_row.'" value="">
									';
									
	if($field_row !='1')
	{
	$html_exp .='<div class="margin-bottom-20"></div><center><button class="btn btn-danger" type="button" onclick="remove_work_fields('.$field_row.')"> 
	<i class="fa fa-trash-o margin-right-5" aria-hidden="true" style="font-size:18px;"></i> Remove</button></center>
	</div>
	<div class="margin-bottom-20"></div><div class="margin-bottom-20"></div> </div> </div> ';	
	}
	return $html_exp;
	}
	
	public function get_suggestion_city($search,$return,$return_type)
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($user_agent !='NI-WEB')
		{
			$search = $this->input->post('search') ? $this->input->post('search') : '';
			$return = $this->input->post('return') ? $this->input->post('return') : 'city_name';
			$return_type = $this->input->post('return_type') ? $this->input->post('return_type') : 'array';
		}
		if($search=='')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','country_id'=>'1',"city_name like '%$search%' ");	
		}
		else
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"city_name like '%$search%' ");
		}
		$data_arr = $this->common_front_model->get_count_data_manual('city_master',$where_arra,2,'city_name,id','','1',25,"");
		//echo $this->common_front_model->last_query(); 
		$opt_array = array();
		$opt_str = '';	
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					$opt_array[] = array('value'=>$data_arr_val[$return],'label'=>$data_arr_val['city_name']);
					$opt_str.='<option value="'.$data_arr_val[$return].'">'.$data_arr_val['city_name'].'</option>';
				}
			}
			if($return_type == 'array')
			{
			  return $opt_array;
			}
			else
			{
				return $opt_str;
			}
	}
	
	
	
	public function get_suggestion_list($search_get,$get_list_get)
	{
		$search = $this->input->post('q');
		$return = $this->input->post('return_val');
		$get_list = $this->input->post('get_list');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		//echo $get_list_get;
		if($get_list == 'skill_list' || ($get_list_get!='' && $get_list_get=='skill_list' )	)
		{
			if($get_list_get!='')
			{
				$search =  $search_get; 
				$return ='key_skill_name';
			}
			$where = "key_skill_name like '%$search%' ";
			$tbl_name = 'key_skill_master';
			$select = 'key_skill_name,id';
			$display_field = 'key_skill_name';
		}
		else if($get_list == 'city_list')
		{
			$where = "city_name like '%$search%' ";
			$tbl_name = 'city_master';
			$select = 'city_name,id';
			$display_field = 'city_name';
		}
		else if($get_list == 'shift_list')
		{
			$where = "shift_type like '%$search%' ";
			$tbl_name = 'shift_list';
			$select = 'shift_type,id';
			$display_field = 'shift_type';
		}
		else if($get_list == 'company_list')
		{
			$where = "company_name like '%$search%' ";
			$tbl_name = 'employer_master';
			$select = 'company_name,id';
			$display_field = 'company_name';
			$this->db->group_by('company_name'); 
       }
		
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',$where);		
		$data_arr = $this->common_front_model->get_count_data_manual($tbl_name,$where_arra,2,$select,'','1',25,"");
		//echo $this->db->last_query();
		$opt_array['results'] = array();
		$tocken_array = array();
		$app_array = array();
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)
				{
					$forpushingarray = array("id"=>$data_arr_val[$return],"text"=>$data_arr_val[$display_field]);
					array_push($opt_array['results'],$forpushingarray);
					$tocken_array[] = array('value'=>$data_arr_val[$return],'label'=>$data_arr_val[$display_field]);
					$app_array[] = array('id'=>$data_arr_val[$return],'val'=>$data_arr_val[$display_field]);
				}
			}
			if($get_list_get!='' && $user_agent=='NI-WEB')
			{
				return $tocken_array;
				
			}
			else if($user_agent=='NI-AAPP' || $user_agent=='NI-IAPP')
			{
				return  $app_array;;
			}
			else
			{
				$opt_array['more'] = "false";
			    return $opt_array;
			}
			
	}
	
}
?>