<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model_employer extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function login()
	{
		$this->load->library('encryption');
        $this->load->library('phpass');
		//$this->load->library('phppass/Passwordhash');
		$username = trim($this->input->post('username'));
		$password = trim($this->input->post('password'));
		$remember_details = $this->input->post('rememberme'); 
		$where = array("email"=>$username,"is_deleted"=>'No');
		
		if($remember_details!='' && $remember_details == 'Yes')
		{
			if($this->encryption->decrypt($password) && $this->encryption->decrypt($password)!='')
			{
		      $password =  $this->encryption->decrypt($password);
			}
			else
			{
				$password = $password;
			}
		}
		$count = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','id');
		if($count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('employer_master',$where,'1','id,email,fullname,password,status');
			$user_hashed_pass = $user_data['password'];
			if($this->phpass->check($password,$user_hashed_pass))
			{
				if(isset($user_data['status']) && $user_data['status'] =='APPROVED')
				{
					$user_data_array = array(
						 'employer_id'	=> $user_data['id'],
						 'fullname'	=> $user_data['fullname'],
						 'email'		=> $user_data['email']
						 );
					$this->session->set_userdata('jobportal_employer', $user_data_array);		 
					$data['errmessage'] =  $this->lang->line('login_success_msg');
					$data['status'] =  'success';
					$data['login_user_id'] =  $user_data['id'];
					$current_date_time = $this->common_front_model->getCurrentDate(); 
					
					$update_login_time = array('last_login'=>$current_date_time);
					if($this->input->post('device_id') && $this->input->post('device_id')!='')
					{
						$update_login_time['device_id'] = $this->input->post('device_id');
					}
					$where_login_time = array("id"=>$user_data['id']);
					$this->common_model->update_insert_data_common("employer_master",$update_login_time,$where_login_time,1);
					if($this->session->has_userdata('jobportal_user'))
					{
						$this->session->unset_userdata('jobportal_user');
					}
					if($remember_details!='' && $remember_details == 'Yes')
					{
						// set user login details cookie
						$this->load->helper('cookie');
						$data_arr = array("username"=>$username,"password"=>$this->encryption->encrypt($password));
						$data_arr = json_encode($data_arr);
						$cookie = array(
							'name'   => 'employer_login_detail',
							'value'  => $data_arr,
							'expire' => '864000'
						);
						delete_cookie('employer_login_detail');
						$this->input->set_cookie($cookie);
						// user login details cookie  end
					}
				}
				else
				{
					 $data['errmessage'] = $this->lang->line('inactive_login_account_employer');
					 $data['status'] =  'error';
				}
			}
			else
			{
				$data['errmessage'] = $this->lang->line('wrong_login_detail_employer');
			    $data['status'] =  'error';
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('wrong_login_detail_employer');
			$data['status'] =  'error';
		}
		return $response = $data;
	}
	public function verify_member($cpass='',$email='')
	{
		if($email!='' && $cpass!='')
		{
			$where = array('email'=>$email,'email_ver_str'=>$cpass);
			$check_data = $this->common_front_model->get_count_data_manual("employer_master",$where,1,"id,email,mobile,status,password");
			if($check_data!='' && is_array($check_data) && count($check_data) > 0)
			{
					$status = $check_data['status'];
					if($status=='UNAPPROVED')
						{ 
							$reandompass = $this->generateRandomString('15');
							$update = array(
											'email_ver_status'=>'Yes',
											'status'=>'APPROVED',
											'email_ver_str'=>$reandompass,
											'email_verified'=>'Yes'
											);
							//$where = array("email"=>$email);
							$where = array("id"=>$check_data['id'],"email"=>$email);
							$this->common_model->update_insert_data_common("employer_master",$update,$where,1);
							$this->session->set_flashdata('emp_ver_corect', $this->data['custom_lable']->language['acc_activeted_emp']);
							if(isset($check_data['mobile']) && $check_data['mobile']!='')
							{
								$get_sms_temp = $this->common_front_model->get_sms_template('Employer account activated');
								if(isset($get_sms_temp) && $get_sms_temp !='' && is_array($get_sms_temp) && count($get_sms_temp) > 0)
								{
									$sms_template = htmlspecialchars_decode($get_sms_temp['sms_content'],ENT_QUOTES); 
									$trans = array("xxxx"=>$check_data['email']);
									$sms_template = $this->common_front_model->sms_string_replaced($sms_template, $trans);
									$this->common_front_model->common_sms_send($check_data['mobile'],$sms_template);
								}
							}
							redirect($this->base_url.'login-employer');
						}	
						else
						{
							$this->session->set_flashdata('emp_ver_incorect', $this->data['custom_lable']->language['acc_already_activeted_emp']);
							redirect($this->base_url.'login-employer');
						}
			}
			else
			{
				$this->session->set_flashdata('emp_ver_incorect',$this->data['custom_lable']->language['acc_already_activeted_emp']);
				//$this->data['custom_lable']->language['error_in_activation_emp']
				redirect($this->base_url.'login-employer');
			}
		}
		else
		{
			$this->session->set_flashdata('emp_ver_incorect',$this->data['custom_lable']->language['error_in_activation_emp']);
			redirect($this->base_url.'login-employer');
		}
	}
	public function reset_forgot_password()
	{
		$email = $this->input->post('email');
		$where = array('email'=>$email,'is_deleted'=>'No','status'=>'APPROVED');
		$count = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','id','','','','');
		if($count !='' && $count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('employer_master',$where,'1','id,email,fullname,status,password');
			$reandompass = $this->generateRandomString('15');
			$update = array('email_ver_str'=>$reandompass);
			$where = array("email"=>$email,'is_deleted'=>'No');
			$this->common_model->update_insert_data_common("employer_master",$update,$where,1);
			$email_temp_data = $this->common_front_model->getemailtemplate('Employer password reset');
			if(isset($email_temp_data) && is_array($email_temp_data) && count($email_temp_data) > 0)
			{
				$config_data = $this->common_front_model->data['config_data'];
				$webfriendlyname = $config_data['web_frienly_name'];
				$subject = $email_temp_data['email_subject'];
				$email_content = $email_temp_data['email_content'];
				$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
				$trans = array("websitename" =>$webfriendlyname,"yourname"=>$user_data['fullname'],"email_id"=>$user_data['email'],"/cpass"=>$reandompass,"site domain name"=>$this->common_front_model->data['base_url'].'login_employer/restetpasstonew/');
				$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
				$this->common_front_model->common_send_email($user_data['email'],$subject,$email_template);
			}
			$status = 'success';
		   	$return_message = $this->data['custom_lable']->language['forgot_password_success_emp'];
		}
		else
		{
			$status = 'error';
			$return_message = $this->lang->line('forgot_password_error_emp');
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message);
		return $return_arr;
	}
	public function reset_new_password($cpass='',$email='')
	{
		$where = array('email'=>$email,'is_deleted'=>'No','status'=>'APPROVED','email_ver_str'=>$cpass);
		$count = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','id','','','','');
		if($count!='' && $count > 0)
		{
			$user_data = $this->common_front_model->get_count_data_manual('employer_master',$where,'1','id,email,fullname,status,password');
			$this->load->library('phpass');
			$this->load->helper('string');
			$pass = $this->input->post('pass');
			$password = $this->phpass->hash($pass);
			$reandompass = $this->generateRandomString('15');
			$update = array('password'=>$password,'email_ver_str'=>$reandompass);
			$where = array("email"=>$email,'is_deleted'=>'No');
			$this->common_model->update_insert_data_common("employer_master",$update,$where,1);
			$email_temp_data = $this->common_front_model->getemailtemplate('Employer password reset successfull');
			if(isset($email_temp_data) && $email_temp_data  !='' && is_array($email_temp_data) && count($email_temp_data) > 0)
			{
				$config_data = $this->common_front_model->data['config_data'];
				$webfriendlyname = $config_data['web_frienly_name'];
				$subject = $email_temp_data['email_subject'];
				$email_content = $email_temp_data['email_content'];
				$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
				$trans = array("websitename" =>$webfriendlyname,"yourname"=>$user_data['fullname']);
				$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
				$this->common_front_model->common_send_email($user_data['email'],$subject,$email_template);
			}
			$status = 'success';
		   	$return_message = $this->data['custom_lable']->language['reset_pass_suck_emp'];
		}
		else
		{
			$status = 'error';
			$return_message = $this->data['custom_lable']->language['reset_pass_fail_emp'];
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message);
		return $return_arr;
	}
	function generateRandomString($length = 10) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
?>