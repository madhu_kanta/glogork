<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Employer_post_job_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	function process_job_posted_data($data = '')
	{
		$data_retrn = array();
		if(isset($data) && $data !='' && is_array($data) && count($data) > 0)
		{
			foreach($data as $data_val)
			{
				$data_val['location_hiring_name'] = $this->common_model->valueFromId('city_master',$data_val['location_hiring'],'city_name');
				$data_val['job_salary_from_name'] = $this->common_model->valueFromId('salary_range',$data_val['job_salary_from'],'salary_range');
				$data_val['job_education_name'] = $this->common_model->valueFromId('educational_qualification_master',$data_val['job_education'],'educational_qualification');
				$data_val['job_counter'] = $this->common_front_model->get_counter('',$data_val['id']);
				$data_retrn[] = $data_val;
			}
		}
		return $data_retrn;
	}
	function save_job()
	{
		$employer_id = $this->common_front_model->get_empid();
		$user_agent = $this->input->post('user_agent');
		if($user_agent!='')
		{
			if($user_agent=='NI-WEB')
			{
				$location_hiring = implode(',',$this->input->post('location_hiring[]'));
				$job_education = implode(',',$this->input->post('job_education[]'));
			}
			else
			{
				$location_hiring = $this->input->post('location_hiring');
				$job_education = $this->input->post('job_education');
			}
		}
		$job_type = $this->input->post('job_type');
		$mode = $this->input->post('mode') ?  $this->input->post('mode') : '';
		//$work_experience_month = $this->input->post('work_experience_month');
		//$work_exp = $work_experience_year .'-'.$work_experience_month;
		$posted_on = $this->common_front_model->getCurrentDate();
		if($this->input->post('define_salary') == 'Define')
		{
			/*$salary_from = $this->input->post('salary_from');
			$salary_to = $this->input->post('salary_to');
			//$job_salary = $salary_from .'-'.$salary_to;*/
			$salary_from = $this->input->post('salary_from');
		}
		else
		{
			$salary_from = $this->input->post('job_salary_text');
			$salary_to = '';
		}
		
		
		$job_description = htmlspecialchars($this->input->post('job_description'),ENT_QUOTES);
		$desire_candidate_profile = htmlspecialchars($this->input->post('desire_candidate_profile'),ENT_QUOTES);
		$data_array_custom = array('location_hiring'=> $location_hiring,'posted_by'=>$employer_id,'job_type'=>$job_type,'job_education'=>$job_education,'job_salary_from'=>$salary_from,'job_description'=>$job_description,'desire_candidate_profile'=>$desire_candidate_profile,'status'=>'UNAPPROVED')
;         
		$job_life = $this->common_model->get_plan_detail($employer_id,'employer','job_life');
     	if(isset($mode) && $mode!='edit')
		{
			$data_array_custom['posted_on'] =$posted_on;
			$current_data_str = strtotime($posted_on);
			$date = strtotime("+$job_life day", $current_data_str);
			$job_expired_on = date('Y-m-d',$date);
			$data_array_custom['job_expired_on'] = $job_expired_on;
		}
		$data_array_custom['job_life'] = $job_life;
		$this->common_front_model->save_update_data('job_posting',$data_array_custom);
		//echo $this->db->last_query();
		return 'success';
		
	}
	
	function edit_posted_job($job_id)
	{
		$where_arra = array('id'=>$job_id);
		$job_data = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,1);
		if($job_data!='' && is_array($job_data) && count($job_data) > 0)
		{
			$data['status'] = 'success';
			$data['edit_job_data'] = $job_data; 
		}
		else
		{
			$data['status'] = 'error';
		}
		return $data;
	}
	
	function view_job_details($job_id)
	{
		$where_arra = array('id'=>$job_id);
		$job_data = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,1);
		if($job_data!='' && is_array($job_data) && count($job_data) > 0 )
		{
			$data['status'] = 'success';
			$data['job_details'] = $job_data;
		}
		else
		{
			$data['status'] = 'error';
		}
		return $data;
	}
	function get_employer_industry_farea($get_data,$return_type='',$selected_item='',$tocken_val =0)
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB';
		if($this->input->post('get_list'))
		{
			$get_data = $this->input->post('get_list');
		}
		
		if($this->input->post('selected_item'))
		{
			$selected_item = trim($this->input->post('selected_item'));
		}
		if($this->input->post('tocken_val'))
		{
			$tocken_val = trim($this->input->post('tocken_val'));
		}
		$selected_arr = array();
		if($selected_item !='')
		{
			if(!is_array($selected_item))
			{
				$selected_arr[] = explode(',',$selected_item);
				$selected_arr = $selected_arr[0];
			}
			else
			{
				$selected_arr[] = $selected_item;
				$selected_arr = $selected_arr[0];
			}
		}
		
		if($get_data == 'function_area_hire')
		{
			$lable = $this->lang->line('job_functional_area');
			$table ='functional_area_master';
			$get_select_field = 'functional_name';
			$lable_add = $this->lang->line('msg_for_add_job_functional_area');
		}
		else if($get_data == 'industry_hire')
		{
			$lable = $this->lang->line('job_industry');
			$table ='industries_master';
			$get_select_field = 'industries_name';
			$lable_add =  $this->lang->line('msg_for_add_job_industry');
		}
		else
		{
			$lable = $this->lang->line('skill_keyword');
			$table ='key_skill_master';
			$get_select_field = 'key_skill_name';
			$lable_add =  $this->lang->line('msg_for_add_job_skill');
		}
		
		
		$employer_id = $this->common_front_model->get_empid();
		$where_arra = array('id'=>$employer_id,'status'=>'APPROVED','is_deleted'=>'No');
		$get_list = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,$get_data,'');
		
		//echo $this->db->last_query();
		$get_id =  $get_list[$get_data];
		if($selected_arr!='' && is_array($selected_arr) && count($selected_arr) > 0  && $get_data!='skill_hire' )
		{
		  $get_id = $get_id .','.implode(',',$selected_arr);
		}
		$list_array = array();
		if($get_id!='')
		{
			$str_ddr = '<option  value="">'.$lable.'</option>';
			$where = 'id IN ('.$get_id.')';
			$get_list_name = $this->common_front_model->get_count_data_manual($table,$where,2,'');//id,'.$get_select_field.'	
			//echo $this->db->last_query();
			if(isset($get_list_name) && $get_list_name !='' && is_array($get_list_name) && count($get_list_name) >0)
			{
				foreach($get_list_name as $list)
				{
					$selected_val_str ='';
					if(isset($selected_arr) && count($selected_arr) > 0 && in_array($list['id'],$selected_arr))
					{
						$selected_val_str =' selected ';
					}
					$str_ddr.= '<option  '.$selected_val_str.' value="'.$list['id'].'">'.$list[$get_select_field].'</option>';
					if($tocken_val == 1)
					{
						$list_array[] =  array('value'=>$list[$get_select_field],'label'=>$list[$get_select_field]);
					}
					else
					{
						$list_array[] =  array('id'=>$list['id'],'val'=>$list[$get_select_field]);
					}
				}
			}
		}
		else
		{
			$str_ddr = '<option  value="">'.$lable_add.'</option>';
		}
		
		if($get_data=='skill_hire' && $user_agent!='NI-WEB')
		{
			if(isset($selected_arr) && $selected_arr !='' && is_array($selected_arr) && count($selected_arr) > 0)
			{
				foreach($selected_arr as $selected_skill)
				{
					
					$list_array[] =  array('value'=>$selected_skill,'label'=>$selected_skill);
				}
			}
		}
		
		if($return_type=='array')
		{
			return $list_array;
		}
		else
		{
			return $str_ddr;
		}
	}
	
	function posted_job($page,$limit='')
	{
		   $search_text_arr = '';
		   $search_text_skill_arr = '';
		   $search_text_role_arr='';
		   if($this->input->post('page'))
			{
				$page = $this->input->post('page');
			}
			if($this->input->post('limit_per_page'))
			{
				$limit = $this->input->post('limit_per_page');
			}
			
			$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
			//'status'=>'APPROVED',
			//$where_arra = array('is_deleted'=>'No');
			$where_arra = array('is_deleted'=>'No','employer_delete'=>'No','employer_status'=>'APPROVED');
			$emp_id = $this->common_front_model->get_empid();
			if($emp_id!='')
			{
				$where_arra[] = "posted_by = '".$emp_id."'"; 
			}
			else
			{
				$where_arra[] = "currently_hiring_status = 'Yes'"; 
				$current_data = $this->common_model->getCurrentDate('Y-m-d');
				$where_arra[] = " job_expired_on >= '$current_data' ";
				$where_arra[] = "status = 'APPROVED'"; 
			}
			
			if($this->input->post('search_location') && $this->input->post('search_location')!='' && $this->input->post('search_location')!=0)
			{
				if(is_array($this->input->post('search_location')))
				{
					$location_arr = $this->input->post('search_location[]');
				}
				else
				{
					$location_arr = explode(',',$this->input->post('search_location'));
				}
				foreach($location_arr as $location)
				{
					$location_hiring_new_arr[] = 'FIND_IN_SET("'.$location.'",location_hiring)';
				}
				$location_search = "( ".implode(' OR ',$location_hiring_new_arr) ." ) "; 
				$where_arra[] = $location_search;
				//$where_arra[] ='FIND_IN_SET("'.$location.'",location_hiring) !=0 ';
			}
			
			if($this->input->post('search_sort_by_posted') && $this->input->post('search_sort_by_posted')!='' && $this->input->post('search_sort_by_posted')!='0')
			{
				 $today=$this->common_front_model->getCurrentDate();
			 	 $days_past = $this->input->post('search_sort_by_posted'); //number of days 
				 $past_stamp = time() - $days_past*24*60*60;
				 $past_day = date('Y-m-d h:i:s', $past_stamp);
				 $search_sort_by_posted = "posted_on between  '$past_day' and '$today' ";
				 $where_arra[] = $search_sort_by_posted;
			}
			if($this->input->post('search_skill') && $this->input->post('search_skill')!='')
			{
				if(is_array($this->input->post('search_skill')))
				{
					$search_skill_arr = str_replace(' ','',$this->input->post('search_skill[]'));
				}
				else
				{
					$search_skill_arr = explode(',',$this->input->post('search_skill'));
				}
				
				/*new added */
				$search_skill_new_arr = array();
				$search_text_skill_arr = array();
				/*new added */
				
				foreach($search_skill_arr as $skill_arr)
				{
					$skill_arr = str_replace(' ','',$skill_arr);
					$search_skill_new_arr[] = "FIND_IN_SET('".$skill_arr."',replace(skill_keyword, ' ', ''))";
					$search_text_skill_arr[] = "FIND_IN_SET('".$skill_arr."',replace(skill_keyword, ' ', ''))";
					
				}
				if($this->input->post('text_search')=='')
			    {
					$search_skill = "( ".implode(' OR ',$search_skill_new_arr) ." ) "; 
					$where_arra[] = $search_skill;
					
			    }
				
			}
			if($this->input->post('search_industry') && $this->input->post('search_industry')!='')
			{
				if(is_array($this->input->post('search_industry')))
				{
					$search_industry_arr = $this->input->post('search_industry[]');
					$ind_list = implode(',',$search_industry_arr);
				}
				else
				{
					$ind_list = $this->input->post('search_industry');
				}
				
				
				$industry = "job_industry IN (".$ind_list.")"; 
				$where_arra[] = $industry;
			}
			if($this->input->post('search_company') && $this->input->post('search_company')!='')
			{
				if(is_array($this->input->post('search_company')))
				{
					$search_company_arr = $this->input->post('search_company[]');
					$cmp_list = implode(',',$search_company_arr);
				}
				else
				{
					$cmp_list = $this->input->post('search_company');
				}
				
				
				$cmp_list = str_replace(',',"','",$cmp_list);
				$company = "company_name IN ('".$cmp_list."')"; 
				$where_arra[] = $company;
			}
			
			if($this->input->post('search_fn_area') && $this->input->post('search_fn_area')!='')
			{
				if(is_array($this->input->post('search_fn_area')))
				{
					$search_fn_area_arr = $this->input->post('search_fn_area[]');
					$fnarea_list = implode(',',$search_fn_area_arr);
				}
				else
				{
					$fnarea_list = $this->input->post('search_fn_area');
				}
				
				
				$fnarea = "functional_area IN (".$fnarea_list.")"; 
				$where_arra[] = $fnarea;
			}
			if($this->input->post('search_job_type') && $this->input->post('search_job_type')!='')
			{
				if(is_array($this->input->post('search_job_type')))
				{
					$search_job_type_list = $this->input->post('search_job_type[]');
					$job_list = implode(',',$search_job_type_list);
				}
				else
				{
					$job_list =$this->input->post('search_job_type');
				}
				
				
				$job_list = str_replace(',',"','",$job_list);
				$job_list_search = "job_type IN ('".$job_list."')"; 
				$where_arra[] = $job_list_search;
			}
			/*if($this->input->post('search_salary') && $this->input->post('search_salary')!='' && $this->input->post('search_salary')!=0)
			{
				$search_salary_exp = explode('/',$this->input->post('search_salary'));
				$search_salary_from = $search_salary_exp[0];
				$search_salary_to = $search_salary_exp[1];
			    $salary = "( (job_salary_from between '".$search_salary_from."' and '".$search_salary_to."' ) or job_salary_to between '".$search_salary_from."' and '".$search_salary_to."' )";
				$where_arra[] = $salary;
			}*/
			if($this->input->post('search_salary') && $this->input->post('search_salary')!='')
			{
				if(is_array($this->input->post('search_salary[]')))
				{
					$search_salary_arr = $this->input->post('search_salary[]');
					$salary_list = implode(',',$search_salary_arr);
				}
				else
				{
					$salary_list = $this->input->post('search_salary');
				}
				$salary = "job_salary_from IN (".$salary_list.")"; 
				$where_arra[] = $salary;
			}
			if($this->input->post('job_edu') && $this->input->post('job_edu')!='')
			{
				
				if(is_array($this->input->post('job_edu')))
				{
					$search_job_edu_arr = $this->input->post('job_edu[]');
				}
				else
				{
					$search_job_edu_arr = explode(',',$this->input->post('job_edu'));
				}
				
				foreach($search_job_edu_arr as $edu_arr)
				{
					$edu_new_arr[] = 'FIND_IN_SET("'.$edu_arr.'",job_education)';
				}
				$search_edu = "( ".implode(' OR ',$edu_new_arr) ." ) "; 
				$where_arra[] = $search_edu;
			}
			if($this->input->post('text_search') && $this->input->post('text_search')!='')
			{
				
				$search_text_search_arr = explode(',',$this->input->post('text_search'));
				/*new added */
				$search_text_skill_arr = array();
				$search_text_role_arr = array();
				$search_text_arr = array();
				/*new added */
				if($search_text_search_arr !='' && is_array($search_text_search_arr) && count($search_text_search_arr)> 0)
				{
					foreach($search_text_search_arr as $search_text_new_arr)
					{   
						$text_search_exp = explode('-',trim($search_text_new_arr));
						
						if($text_search_exp[0]=='S')
						{ 
							$search_text_skill = str_replace(' ','',$text_search_exp[1]); 
							$search_text_skill_arr[] = "FIND_IN_SET('".$search_text_skill."',replace(skill_keyword, ' ', ''))";
							
						}
						else if($text_search_exp[0]=='R')
						{ 
							$search_text_role = $text_search_exp[1]; 
							$search_text_role_arr[] = "FIND_IN_SET(".$search_text_role.",job_post_role)";
						}
						else 
						{   
							$search_text = str_replace(' ','',$text_search_exp[0]); 
							$search_text_arr[] = "replace(replace(job_role_name,' ',''),'/','') like '%$search_text%' or replace(job_title, ' ', '') like '%$search_text%' or FIND_IN_SET('".$search_text."',replace(skill_keyword, ' ', '')) ";
						}
						
					}
				}
				$search_all_array = array();
				if($search_text_skill_arr!='' && is_array($search_text_skill_arr) && count($search_text_skill_arr) > 0)
				{
					$search_text_skill = "( ".implode(' OR ',$search_text_skill_arr) ." ) "; 
					$search_all_array[] = $search_text_skill;
				}
				if($search_text_role_arr!='' && is_array($search_text_role_arr) && count($search_text_role_arr) > 0)
				{
					$search_text_role = "( ".implode(' OR ',$search_text_role_arr) ." ) "; 
					$search_all_array[] = $search_text_role;
				}
				if($search_text_arr!='' && is_array($search_text_arr) && count($search_text_arr) > 0)
				{
					$search_text = "( ".implode(' OR ',$search_text_arr) ." ) "; 
					$search_all_array[] = $search_text;
				}
				$search_text_all = "( ".implode(' OR ',$search_all_array) ." ) ";
				$where_arra[] = $search_text_all;
			}
		
		
		 $emp_post_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id');
	     
		 $emp_post_data =  $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,'','id desc',$page,$limit,'');
		 //echo $this->db->last_query();
		 $data['total_post_count'] = $emp_post_count;
		 $data['emp_posted_job'] =  $emp_post_data;
		 return $response = $data;
 	}
	function suggested_job($page,$limit='')
	{
		   if($this->input->post('page'))
			{
				$page = $this->input->post('page');
			}
			if($this->input->post('limit_per_page'))
			{
				$limit = $this->input->post('limit_per_page');
			}
			
			$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
			
			$get_userid = $this->common_front_model->get_userid();
			$where_for_js = array('id'=>$get_userid);
			$get_js_job_requirement = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_for_js,1,'id,preferred_city,key_skill,industry');//,functional_area,job_role
			$where_match = array('status'=>'APPROVED','is_deleted'=>'No','employer_delete'=>'No','employer_status'=>'APPROVED');
			if($get_js_job_requirement['preferred_city'] != '' && $get_js_job_requirement['preferred_city'] != 0)
			{
				$where_city = "id IN (".$get_js_job_requirement['preferred_city'].")";
				$p_city_list = $this->common_front_model->get_count_data_manual('city_master',$where_city,1,'id,city_name');
				if(isset($p_city_list) && $p_city_list !='' && is_array($p_city_list) && count($p_city_list) > 0)
				{
					foreach($p_city_list as $city_list)
					{
						if( isset($city_list['id']) && $city_list['id'] != '')
						{
							$location_match_arr[] = 'FIND_IN_SET("'.$get_city_id['id'].'",location_hiring)';
						}
					}
				}
				if(isset($location_match_arr) && $location_match_arr!='' && is_array($location_match_arr) && count($location_match_arr) > 0)
				{
					$location_match = "( ".implode(' OR ',$location_match_arr) ." ) "; 
					$where_match[] = $location_match;
				}
			}
			
			if( isset($get_js_job_requirement['key_skill']) &&  $get_js_job_requirement['key_skill'] != '')
			{
				$key_skill = explode(',',$get_js_job_requirement['key_skill']);
				foreach($key_skill as $skill)
				{
					$skill_arr = str_replace(' ','',$skill);
					$match_skill_new_arr[] = "FIND_IN_SET('".$skill_arr."',replace(skill_keyword, ' ', ''))";
				}
				$match_skill = "( ".implode(' OR ',$match_skill_new_arr) ." ) "; 
				$where_match[] = $match_skill;
			}
			
			if( isset($get_js_job_requirement['industry']) && $get_js_job_requirement['industry'] != '' && $get_js_job_requirement['industry'] != 0)
			{
				$match_ind = "job_industry = '".$get_js_job_requirement['industry']."'"; 
				$where_match[] = $match_ind;
			}
		
		   $emp_post_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_match,0,'id');
		   $emp_post_data =  $this->common_front_model->get_count_data_manual('job_posting_view',$where_match,2,'','id desc',$page,$limit,'');
		// echo $this->db->last_query();
		 $data['total_post_count'] = $emp_post_count;
		 $data['emp_posted_job'] =  $emp_post_data;
		 return $response = $data;
 	}
	
	function add_to_highlight()
	{
		$employer_id = $this->common_front_model->get_empid();
		$return_data_plan_high = $this->common_front_model->get_plan_detail($employer_id,'employer','highlight_job_limit');
		$job_id = $this->input->post('id');
		if($job_id!='')
		{
			if($return_data_plan_high == 'Yes')	
			{
				$data_array_custom = array('job_highlighted'=>'Yes');
				$this->common_front_model->save_update_data('job_posting',$data_array_custom);
				//echo $this->db->last_query();
				$data['status'] = 'success';
				$this->common_model->update_plan_detail($employer_id,'employer','highlight_job_limit');
			}
			else
			{
				$data['status'] = 'error_plan';
				
			}
		}
		else
		{
			$data['status'] = 'error';
		}
		return $data;
	}
	
	function get_search_suggestion($search)
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($user_agent!='NI-WEB')
		{
			$search = $this->input->post('search') ? $this->input->post('search') : '';
		}
		if(trim($search)!='')
		{
		$search = str_replace('%20','',$search);
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"TRIM(replace(role_name, ' ', '')) like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('role_master',$where_arra,2,'role_name,id','','1',10,"");
		
		$opt_array = array();
			
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)	
				{
					$opt_array[] = array('value'=>'R-'.$data_arr_val['id'],'label'=>$data_arr_val['role_name']);
				}
			}
			
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"TRIM(replace(key_skill_name, ' ', '')) like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('key_skill_master',$where_arra,2,'key_skill_name,id','','1',10,"");
		
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)	
				{
					$opt_array[] = array('value'=>'S-'.$data_arr_val['key_skill_name'],'label'=>$data_arr_val['key_skill_name']);
				}
			}
			
			return $opt_array;	
		}
			
	}
	function location_hiring_city_name($data = '')
	{
		$data_retrn = array();
		if(isset($data) && $data !='' && is_array($data) && count($data) > 0)
		{
			foreach($data as $data_val)
			{
				$data_val['location_hiring_name'] = $this->common_front_model->get_location_hiring_name($data_val['location_hiring']);
				$data_retrn[] = $data_val;
			}
		}
		return $data_retrn;
	}
	function job_education_name($data = '')
	{
		$data_retrn = array();
		if(isset($data) && $data !='' && is_array($data) && count($data) > 0)
		{
			foreach($data as $data_val)
			{
				$data_val['job_education_name'] = $this->common_front_model->get_job_education_name($data_val['job_education']);
				$data_retrn[] = $data_val;
			}
		}
		return $data_retrn;
	}
}
?>