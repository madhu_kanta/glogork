<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Our_recruiters_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function getrecruiters_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$getrecruiters_count = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,0,'id','','','','');
		return $getrecruiters_count;
	}
	public function getcompaniess_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$this->db->group_by('LOWER(TRIM(replace(company_name, \' \', \'\')))');
		$getcompaniess_count = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,0,'id','','','','');
		return $getcompaniess_count;
	}
	public function getrecruitersactivejob_count($idofrecruiter = '')
	{
		$getrecruitersactivejob_count = 0;
		if($idofrecruiter != '')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','posted_by'=>$idofrecruiter);
			$current_data = $this->common_model->getCurrentDate('Y-m-d');
			$where_arra[] = " job_expired_on >= '$current_data' ";
			$getrecruitersactivejob_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id','','','','');
		}
		return $getrecruitersactivejob_count;
	}
	public function getrecruitersactivejob_count_name($nameofrecruiter = '')/*$idofrecruiter = ''*/
	{
		$getrecruitersactivejob_count = 0;
		if($nameofrecruiter != '')
		{
			$nameofrecruiter = str_replace(" ","",strtolower(trim($nameofrecruiter)));
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','LOWER(TRIM(replace(company_name, \' \', \'\'))) ='=>$nameofrecruiter);
			$current_data = $this->common_model->getCurrentDate('Y-m-d');
			$where_arra[] = " job_expired_on >= '$current_data' ";
			$getrecruitersactivejob_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id','','','','');
		}
		return $getrecruitersactivejob_count;
	}
	public function getrecruitersalljob_count($idofrecruiter = '')
	{
		$getrecruitersalljob_count = 0;
		if($idofrecruiter != '')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','posted_by'=>$idofrecruiter);//,'currently_hiring_status'=>'Yes'
			$current_data = $this->common_model->getCurrentDate('Y-m-d');
			$where_arra[] = " job_expired_on >= '$current_data' ";
			$getrecruitersalljob_count = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,0,'id','','','','');
		}
		return $getrecruitersalljob_count;
	}
	public function getrecruitersalljob($employer_id='',$page=1,$limit=3,$req_field=array())
	{
		$getrecruitersalljob = array();
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		if($this->input->post('emp_id'))
		{
			$employer_id = $this->input->post('emp_id');
		}
		//echo $employer_id;
		if($employer_id!='')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes','posted_by'=>$employer_id);
			$current_data = $this->common_model->getCurrentDate('Y-m-d');
			$where_arra[] = " job_expired_on >= '$current_data' ";
			$getrecruitersalljob = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,$req_field_comma,'posted_on desc',$page,$limit,'');
		}
		return $getrecruitersalljob;
	}
	public function getrecruiterstotal_follower_count($idofrecruiter = '')
	{
		$getrecruiterstotal_follower_count = 0;
		if($idofrecruiter != '')
		{
			$where_arra = array('emp_id'=>$idofrecruiter,'is_follow'=>'Yes');
			$getrecruiterstotal_follower_count = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra,0,'id','','','','');	
		}
		return $getrecruiterstotal_follower_count;
	}
	public function getrecruiters($page=1,$limit=3,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$getrecruiters = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,2,$req_field_comma,'id desc',$page,$limit,'');
		return $getrecruiters;
	}
	public function getcompanies($page=1,$limit=3,$req_field=array())
	{
		if($req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$this->db->group_by('LOWER(TRIM(replace(company_name, \' \', \'\')))');
		$getcompanies = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,2,$req_field_comma,'id desc',$page,$limit,'');
		return $getcompanies;
	}
	public function getcompany_logos($data_or_count = 'count')
	{
		$getcompany_logos = '';
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','company_logo !='=>'','company_logo_approval'=>'APPROVED');
		if($data_or_count == 'count')
		{
			$getcompany_logos = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,0,'company_logo','','','8','1');	
		}
		else
		{
			$getcompany_logos = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,2,'company_logo','RAND()','1','10','');
		}
		return $getcompany_logos;
	}
	function view_company_details($company_id)
	{
		$company_data = array();
		if($company_id != '')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','id'=>$company_id);
			$company_data = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'','','','','');
		}
		return $company_data;
	}
	function recruiters_companies_count_follower_for_app($data = '')
	{
		$data_retrn = array();
		if(isset($data) && $data !='' && is_array($data) && count($data) > 0)
		{
			foreach($data as $data_val)
			{
				$data_val['active_job'] = $this->getrecruitersactivejob_count($data_val['id']);	
				$data_val['follower_count'] = $this->getrecruiterstotal_follower_count($data_val['id']);
				$data_val['skill_hire_name'] = $this->common_model->valueFromId('key_skill_master',$data_val['skill_hire'],'key_skill_name');
				$data_retrn[] = $data_val;
			}
		}
		return $data_retrn;
	}
}
?>