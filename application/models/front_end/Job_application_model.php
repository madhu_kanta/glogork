<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_application_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	
	function manage_job_application($page,$limit='')
	{
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$emp_id = $this->common_front_model->get_empid();
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$where_arr = array('posted_by'=>$emp_id);
		if($this->input->post('job_title_id') && $this->input->post('job_title_id')!='')
		{
			$where_arr[] = "jp.id = '".$this->input->post('job_title_id')."' ";
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$search_parameter_str = $this->input->post('search_applied_user');
			$where_arr[] = "(js_id like '%".trim($search_parameter_str)."%' or  LOWER(TRIM(js.email)) like '%".strtolower(trim($search_parameter_str))."%' or  LOWER(TRIM(js.mobile)) like '%".strtolower(trim($search_parameter_str))."%' or  Replace(LOWER(TRIM(js.fullname)),' ','') like '%".str_replace(' ', '',strtolower(trim($search_parameter_str)))."%' ) ";
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		$apply_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$this->db->join('jobseeker_view jv', 'jah.js_id = jv.id', 'inner');
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		$apply_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'jah.*,jv.email as js_email,jv.fullname as js_fullname,jv.resume_file as js_resume_file,jv.resume_verification as js_resume_verification,jv.personal_titles as js_personal_title,jv.profile_pic as js_profile_pic,jv.profile_pic_approval as js_profile_pic_approval,jv.id as job_id,jv.is_deleted as js_is_deleted,jp.posted_by,jp.company_name,jp.job_title,jp.posted_on','jah.id desc',$page,$limit);
		 //echo $this->db->last_query();
		 $data['apply_list_count'] = $apply_list_count;
		 $data['apply_list_data'] =  $apply_list_data;
		 $data['emp_posted_job_list'] = $this->get_employed_posted_job_id_name();
		 $data['status'] = 'success';
		 return $data;
	}
	
	function shortlist_application($page,$limit='')
	{
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$emp_id = $this->common_front_model->get_empid();
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$where_arr = array('posted_by'=>$emp_id,'is_shortlisted'=>'Yes');
		if($this->input->post('job_title_id') && $this->input->post('job_title_id')!='')
		{
			$where_arr[] = "jp.id = '".$this->input->post('job_title_id')."' ";
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$where_arr[] = "(js_id like '%".$this->input->post('search_applied_user')."%' or  js.email like '%".$this->input->post('search_applied_user')."%' or  js.mobile like '%".$this->input->post('search_applied_user')."%' ) ";
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		/*$apply_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		$apply_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'jah.*,jp.id as job_id,jp.posted_by,jp.company_name,jp.job_title,jp.posted_on','jah.id desc',$page,$limit);*/
		
		$apply_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$this->db->join('jobseeker_view jv', 'jah.js_id = jv.id', 'inner');
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		$apply_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'jah.*,jv.email as js_email,jv.fullname as js_fullname,jv.resume_file as js_resume_file,jv.resume_verification as js_resume_verification,jv.personal_titles as js_personal_title,jv.profile_pic as js_profile_pic,jv.profile_pic_approval as js_profile_pic_approval,jv.id as job_id,jv.is_deleted as js_is_deleted,jp.posted_by,jp.company_name,jp.job_title,jp.posted_on','jah.id desc',$page,$limit);
		// echo $this->db->last_query();
		 $data['apply_list_count'] = $apply_list_count;
		 $data['apply_list_data'] =  $apply_list_data;
		 $data['emp_posted_job_list'] = $this->get_employed_posted_job_id_name();
		 $data['status'] = 'success';
		 return $data;
	}
	
	function send_message_js()
	{
		if($this->input->post('sender_id') && $this->input->post('sender_id')!='' && $this->input->post('receiver_id')!='') 
		{
				$sender = $this->input->post('sender_id');
				$receiver = $this->input->post('receiver_id');
				$sent_on = $this->common_front_model->getCurrentDate();
				
				$where_arr_block_employer = array('js_id'=>$receiver,'emp_id'=>$sender,'is_block'=>'Yes'); 
				$get_block_employer = $this->common_front_model->get_count_data_manual(' jobseeker_access_employer',$where_arr_block_employer,2,'*');
				if($get_block_employer!='' && is_array($get_block_employer) && count($get_block_employer) > 0)
				{
					return "error_block";
				}
				else
				{
					$data_array_custom = array('sender'=>$sender,'receiver'=>$receiver,'sent_on'=>$sent_on,'status'=>'sent','sender_type'=>'employer');
					//$this->common_front_model->save_update_data('message',$data_array_custom);
					$return_data_plan_msg = $this->common_front_model->get_plan_detail($sender,'employer','message');
				
					if($return_data_plan_msg=='Yes')	
					{
						$this->common_front_model->save_update_data('message',$data_array_custom);
						$get_email = $this->common_front_model->getemailtemplate('Send message to jobseeker');
						if($get_email!='' && is_array($get_email) && count($get_email) > 0 )
						{
						   $emp_id = $this->common_front_model->get_empid();	
						   $login_user_details = $this->common_front_model->get_user_data('employer_master',$emp_id,'fullname');
						   $email = $this->input->post('email');// js email (for sending mail)
						   $config_data = $this->common_front_model->data['config_data'];
						   $webfriendlyname = $config_data['web_frienly_name'];
						   $subject = $get_email['email_subject'];
						   $email_content= $get_email['email_content'];
						   $email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
						   $trans = array("websitename" =>$webfriendlyname,"sender_name"=>$login_user_details['fullname']);
						   $email_template = $this->common_front_model->getstringreplaced($email_template, $trans);	
						   $this->common_front_model->common_send_email($email,$subject,$email_template);
						}
						$this->common_model->update_plan_detail($sender,'employer','message');
						return "success";
					}
					else
					{
						return "error_plan";		
					}
				}
			}
			else
			{
				return "error";
			}
		
	}
	function add_to_shortlist()
	{
		if($this->input->post('app_id') && $this->input->post('app_id')!='') 
		{
				$app_id = $this->input->post('app_id');
				$data_array_custom = array('is_shortlisted'=>'Yes');
				$this->common_front_model->save_update_data('job_application_history',$data_array_custom);
				return "success";
		}
		else
		{
			return "error";
		}
	}
	
	function remove_from_shortlist()
	{
		if($this->input->post('app_id') && $this->input->post('app_id')!='') 
		{
			$app_id = $this->input->post('app_id');
			$data_array_custom = array('is_shortlisted'=>'No');
			$this->common_front_model->save_update_data('job_application_history',$data_array_custom);
			return "success";
		}
		else
		{
			return "error";
		}
	}
	
	function get_employed_posted_job_id_name()
	{
		$emp_id = $this->common_front_model->get_empid();
		$where_arr = array('posted_by'=>$emp_id);
		$get_posted_job = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arr,2,'id,job_title');
		$emp_job_array = array();
		if($get_posted_job!='' && is_array($get_posted_job) && count($get_posted_job) > 0)
		{
			foreach($get_posted_job as $emp_job)
			{ 
				$emp_job_array[] = array('id'=>$emp_job['id'],'val'=>$emp_job['job_title']);
			}
		}
		return $emp_job_array;
	}
	function download_cv($page,$limit='')
	{
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$emp_id = $this->common_front_model->get_empid();
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$where_arr = array('posted_by'=>$emp_id);
		if($this->input->post('job_title_id') && $this->input->post('job_title_id')!='')
		{
			$where_arr[] = "jp.id = '".$this->input->post('job_title_id')."' ";
			
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$search_parameter_str = $this->input->post('search_applied_user');
			$where_arr[] = "(js_id like '%".trim($search_parameter_str)."%' or  LOWER(TRIM(js.email)) like '%".strtolower(trim($search_parameter_str))."%' or  LOWER(TRIM(js.mobile)) like '%".strtolower(trim($search_parameter_str))."%' or  Replace(LOWER(TRIM(js.fullname)),' ','') like '%".str_replace(' ', '',strtolower(trim($search_parameter_str)))."%' ) ";
		}
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
		}
		$apply_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$this->db->join('jobseeker_view jv', 'jah.js_id = jv.id', 'inner');
		if($this->input->post('search_applied_user') && $this->input->post('search_applied_user')!='')
		{
			$this->db->join('jobseeker js', 'jah.js_id = js.id', 'inner');
			$where_arr[] = "jv.resume_verification = 'APPROVED' ";
			
		}
		$apply_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'jah.*,jv.email as js_email,jv.fullname as js_fullname,jv.resume_file as js_resume_file,jv.resume_verification as js_resume_verification,jv.personal_titles as js_personal_title,jv.profile_pic as js_profile_pic,jv.profile_pic_approval as js_profile_pic_approval,jv.id as job_id,jp.posted_by,jp.company_name,jp.job_title,jp.posted_on','jah.id desc',$page,$limit);
		 $this->db->last_query();
		 $data['apply_list_count'] = $apply_list_count;
		 $data['apply_list_data'] =  $apply_list_data;
		 $data['emp_posted_job_list'] = $this->get_employed_posted_job_id_name();
		 $data['status'] = 'success';
		 return $data;
	}
	
}
?>