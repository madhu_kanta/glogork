<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Resume_list_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	function view_job_details($job_id)
	{
		$where_arra = array('id'=>$job_id);
		$job_data = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,1);
		if( $job_data!='' && is_array($job_data) && count($job_data) > 0)
		{
			$data['status'] = 'success';
			$data['job_details'] = $job_data; 
		}
		else
		{
			$data['status'] = 'error';
		}
		return $data;
	}
	
	function get_employer_industry_farea($get_data,$return_type='',$selected_item='',$tocken_val =0)
	{
		
		if($this->input->post('get_list'))
		{
			$get_data = $this->input->post('get_list');
		}
		
		if($this->input->post('selected_item'))
		{
			$selected_item = trim($this->input->post('selected_item'));
		}
		if($this->input->post('tocken_val'))
		{
			$tocken_val = trim($this->input->post('tocken_val'));
		}
		$selected_arr = array();
		if($selected_item !='')
		{
			if(!is_array($selected_item))
			{
				$selected_arr[] = explode(',',$selected_item);
				$selected_arr = $selected_arr[0];
			}
			else
			{
				$selected_arr[] = $selected_item;
				$selected_arr = $selected_arr[0];
			}
		}
		
		if($get_data == 'function_area_hire')
		{
			$lable = $this->lang->line('job_functional_area');
			$table ='functional_area_master';
			$get_select_field = 'functional_name';
			$lable_add = $this->lang->line('msg_for_add_job_functional_area');
		}
		else if($get_data == 'industry_hire')
		{
			$lable = $this->lang->line('job_industry');
			$table ='industries_master';
			$get_select_field = 'industries_name';
			$lable_add =  $this->lang->line('msg_for_add_job_industry');
		}
		else
		{
			$lable = $this->lang->line('skill_keyword');
			$table ='role_master';
			$get_select_field = 'role_name';
			$lable_add =  $this->lang->line('msg_for_add_job_skill');
		}
		
		
		$employer_id = $this->common_front_model->get_empid();
		$where_arra = array('id'=>$employer_id,'status'=>'APPROVED','is_deleted'=>'No');
		$get_list = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,$get_data,'');
		
		//echo $this->db->last_query();
		$get_id =  $get_list[$get_data];
		if($selected_arr!='' && is_array($selected_arr) && count($selected_arr) > 0)
		{
		  $get_id = $get_id .','.implode(',',$selected_arr);
		}
		$list_array = array();
		if($get_id!='')
		{
			$str_ddr = '<option  value="">'.$lable.'</option>';
			$where = 'id IN ('.$get_id.')';
			$get_list_name = $this->common_front_model->get_count_data_manual($table,$where,2,'');//id,'.$get_select_field.'	
			foreach($get_list_name as $list)
			{
				$selected_val_str ='';
				if(isset($selected_arr) && is_array($selected_arr) && count($selected_arr) > 0 && in_array($list['id'],$selected_arr))
				{
					$selected_val_str =' selected ';
				}
				$str_ddr.= '<option  '.$selected_val_str.' value="'.$list['id'].'">'.$list[$get_select_field].'</option>';
				if($tocken_val == 1)
				{
					$list_array[] =  array('value'=>$list[$get_select_field],'label'=>$list[$get_select_field]);
				}
				else
				{
					$list_array[] =  array('id'=>$list['id'],'val'=>$list[$get_select_field]);
				}
			}
		}
		else
		{
			$str_ddr = '<option  value="">'.$lable_add.'</option>';
		}
		
		if($return_type=='array')
		{
			return $list_array;
		}
		else
		{
			return $str_ddr;
		}
		
	}
	
	function listed_resumes($page,$limit='')
	{
		   $search_text_arr = '';
		   $search_text_skill_arr = '';
		   $search_text_role_arr='';
		   $where_arra[] = array('status'=>'APPROVED','is_deleted'=>'No','profile_visibility'=>'Yes');
		   if($this->input->post('page'))
			{
				$page = $this->input->post('page');
			}
			$limit = (isset($limit) && $limit!='') ? $limit : $this->common_front_model->limit_per_page;
			if($this->input->post('search_location') && $this->input->post('search_location')!='')
			{
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-AAPP')
				{
					$location_arr_comma = $this->input->post('search_location');
					$location_arr_comma = rtrim($location_arr_comma,",");
					$location_arr_comma = ltrim($location_arr_comma,",");
					$location_arr = explode(',',$location_arr_comma);	
				}
				else
				{
					$location_arr = $this->input->post('search_location[]');	
				}
				
				$location_list = implode(',',$location_arr);
				$loation_search = "city IN (".$location_list.")"; 
				$where_arra[] = $loation_search;
			}
			if($this->input->post('search_skill') && $this->input->post('search_skill')!='')
			{
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-AAPP')
				{
					$search_skill_arr_comma = $this->input->post('search_skill');
					$search_skill_arr_comma = rtrim($search_skill_arr_comma,",");
					$search_skill_arr_comma = ltrim($search_skill_arr_comma,",");
					$search_skill_arr = explode(',',$search_skill_arr_comma);
				}
				else
				{
					$search_skill_arr = $this->input->post('search_skill[]');
				}
				if($search_skill_arr !='' && is_array($search_skill_arr) && count($search_skill_arr) > 0)
				{
					foreach($search_skill_arr as $skill_arr)
					{
						$skill_arr = str_replace(' ','',strtolower(trim($skill_arr)));
						$search_skill_new_arr[] = "FIND_IN_SET('".$skill_arr."',replace(LOWER(TRIM(key_skill)), ' ', ''))";
						$search_text_skill_arr[] = "FIND_IN_SET('".$skill_arr."',replace(LOWER(TRIM(key_skill)), ' ', ''))";
					}
					$search_skill = "( ".implode(' OR ',$search_skill_new_arr) ." ) "; 
					$search_skill_not_blank = "key_skill !=''"; 
					$where_arra[] = $search_skill_not_blank;
					$where_arra[] = $search_skill;
				}
				/*if($this->input->post('text_search')=='')
			    {
					$search_skill = "( ".implode(' OR ',$search_skill_new_arr) ." ) "; 
					$search_skill_not_blank = "key_skill !=''"; 
					$where_arra[] = $search_skill_not_blank;
					$where_arra[] = $search_skill;
			    }*/
			}
			if($this->input->post('search_industry') && $this->input->post('search_industry')!='')
			{
				if($this->input->post('user_agent') && $this->input->post('user_agent')=='NI-AAPP')
				{
					$search_industry_comma = $this->input->post('search_industry');	
					$search_industry_comma = rtrim($search_industry_comma,",");
					$search_industry_comma = ltrim($search_industry_comma,",");
					$search_industry_arr = explode(',',$search_industry_comma);
				}
				else
				{
					$search_industry_arr = $this->input->post('search_industry[]');	
				}
				
				$ind_list = implode(',',$search_industry_arr);
				$industry = "industry IN (".$ind_list.")"; 
				$where_arra[] = $industry;
			}
		 	//$total_resume_count = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,0,'id','','','','');
			$total_resume_count = $this->getjobseekers_count($where_arra);
			$total_resume_data =  $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,2,'','id desc',$page,$limit,'');
		// echo $this->db->last_query();
		if($this->input->post('user_agent') == 'NI-IAAPP' || $this->input->post('user_agent') == 'NI-AAPP')
		{
			//$data =  $total_resume_data;
			$data['total_resume_count'] = $total_resume_count;
		 	$data['resume_data'] =  $total_resume_data;		
		}
		else
		{
			$data['total_resume_count'] = $total_resume_count;
		 	$data['resume_data'] =  $total_resume_data;		
		}
		 
		 return $response = $data;
 	}
	public function getjobseekers_count($where_arra='')
	{
		if($where_arra=='')
		{
			$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','profile_visibility'=>'Yes');	
		}
		$getrecruiters_count = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,0,'id','','','','');
		return $getrecruiters_count;
	}
	function get_search_suggestion($search)
	{
		if(trim($search)!='')
		{
		$search = str_replace('%20','',$search);
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"TRIM(replace(role_name, ' ', '')) like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('role_master',$where_arra,2,'role_name,id','','1',10,"");
		
		$opt_array = array();
			
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)	
				{
					$opt_array[] = array('value'=>'R-'.$data_arr_val['id'],'label'=>$data_arr_val['role_name']);
				}
			}
			
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"TRIM(replace(key_skill_name, ' ', '')) like '%$search%' ");		
		$data_arr = $this->common_front_model->get_count_data_manual('key_skill_master',$where_arra,2,'key_skill_name,id','','1',10,"");
		
			if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
			{
				foreach($data_arr as $data_arr_val)	
				{
					$opt_array[] = array('value'=>'S-'.$data_arr_val['key_skill_name'],'label'=>$data_arr_val['key_skill_name']);
				}
			}
			
			return $opt_array;	
		}
			
	}
}
?>