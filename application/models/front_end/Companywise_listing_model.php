<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Companywise_listing_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function gettotaljobs_groupbycompany_count()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','company_name !='=>'');
		$this->db->group_by('LOWER(TRIM(replace(company_name, \' \', \'\')))');
		$gettotaljobs_groupbycompany_count = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,0,'id','','','','');
		return $gettotaljobs_groupbycompany_count;
	}
	public function getcompaniesdata($where_arra=array(),$page=1,$limit=5,$req_field=array())
	{
		if(isset($req_field) && $req_field !='' && is_array($req_field) && count($req_field) > 0)
		{
			$req_field_comma = implode(',',$req_field);
		}
		else
		{
			$req_field_comma = '';
		}
		$this->db->group_by('LOWER(TRIM(replace(company_name, \' \', \'\')))');
		$getcompaniesdata = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,2,$req_field_comma,'company_name asc',$page,$limit,'');
		//$getcompaniesdata = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,2,$req_field_comma,'','','','');
		return $getcompaniesdata;
	}
}
?>