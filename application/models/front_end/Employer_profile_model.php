<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Employer_profile_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function updatepersonaldet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$this->common_front_model->save_update_data('employer_master','','id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function editsocialdet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$facebookurl = $this->input->post('facebookurl');
				$gplusurl = $this->input->post('gplusurl');
				$twitterurl = $this->input->post('twitterurl');
				$linkdinurl = $this->input->post('linkdinurl');
				
				$data_array_custom = array('facebook_url'=> $facebookurl,'gplus_url'=>$gplusurl,'twitter_url'=>$twitterurl,'linkedin_url'=>$linkdinurl);
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function updatecompanydet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				/**/
				$mobile_code = $this->input->post('mobile_c_code');
				$mobile_coming = $this->input->post('mobile_num');
				$mobile_num_old = $this->input->post('mobile_num_old');
				
				/*
					echo '<pre>';
					print_r($_REQUEST['']);
					echo '</pre>';
				*/
				$mobile_update = $mobile_code.'-'.$mobile_coming;
				if($mobile_num_old != $mobile_update)
				{
					$_REQUEST['mobile_verified'] = 'No';
				}
				$mobile_update = $mobile_code.'-'.$mobile_coming;
				//$_REQUEST['mobile'] = $mobile_update;
				$data_array_custom = array('mobile'=>$mobile_update);
				
				if(isset($_FILES['company_logo']) && $_FILES['company_logo'])
				{
					$_REQUEST['company_logo_path'] = $this->common_front_model->fileuploadpaths('company_logos',1);
					$_REQUEST['company_logo_ext'] = 'jpg|jpeg|png|gif';
					$upload_path = $this->common_front_model->fileuploadpaths('company_logos',1);
					if(file_exists($upload_path.'/'.$checkifmemberexists['company_logo']))
					{
						$company_logo_var = $upload_path.'/'.$checkifmemberexists['company_logo'];
						$_REQUEST['company_logo_var'] = $company_logo_var;
					}
				}
				else if(isset($_POST['company_logo']) && $_POST['company_logo']!='')
				{
						$company_logo_name = 'company_logo';
						$upload_company_logo_name = time().'-'.$checkifmemberexists['id'].'.jpg';
						$this->common_front_model->base_64_photo($company_logo_name,'path_company_logo',$upload_company_logo_name);
						$_REQUEST['company_logo'] = $upload_company_logo_name;
						$_REQUEST['company_logo_approval'] = 'UNAPPROVED';
						
						$customvar = array('company_logo'=>$upload_company_logo_name,'company_logo_approval'=>'UNAPPROVED');
						$response = $this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$checkifmemberexists['id']);
						
						if($response && $response=='success')
						{
							if(file_exists($this->common_front_model->path_company_logo.$checkifmemberexists['company_logo']))
							{
								 $delete_company_logo = $this->common_front_model->path_company_logo.$checkifmemberexists['company_logo'];
								if(isset($delete_company_logo) && $delete_company_logo!='')
								{
									$this->common_front_model->delete_file($delete_company_logo);
								}
							}
						}
				}	
				/**/
				$response_func = $this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				if($response_func == 'file_upload_error')
				{
					$datavar['result']="file_upload_error";
				}
				else
				{
					$comoany_logo_arr = $this->common_front_model->get_count_data_manual('employer_master',array('id'=>$checkifmemberexists['id']),1,'company_logo');
					if($comoany_logo_arr['company_logo'] != $checkifmemberexists['company_logo'])
					{
						$data_array_custom = array('company_logo_approval'=>'UNAPPROVED');
						$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
					}
				
				//$this->common_front_model->save_update_data('employer_master','','id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
					$datavar['result']="success";
				}
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function updatework_expdet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				if($this->input->post('user_agent')!='NI-IAPP')
				{
					$currentdesignation = (count($this->input->post('currentdesignation')) > 0 && $this->input->post('currentdesignation')!='' && is_array($this->input->post('currentdesignation'))) ? implode(",",$this->input->post('currentdesignation')) : "";
					$functional_area = (count($this->input->post('functional_area')) > 0 && $this->input->post('functional_area')!='' && is_array($this->input->post('functional_area'))) ? implode(",",$this->input->post('functional_area')) : "";
				}
				else
				{
					$currentdesignation = $this->input->post('currentdesignation');
					$functional_area = $this->input->post('functional_area');
				}
				//$join_month = $this->input->post('join_month');
				//$join_year = $this->input->post('join_year');
				//$join_in = $join_month.' ,'.$join_year;
				//$data_array_custom = array('designation'=> $currentdesignation,'join_in'=>$join_in,'functional_area'=> $functional_area);
				$data_array_custom = array('designation'=> $currentdesignation,'functional_area'=> $functional_area);
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function updateskilldet($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				if($this->input->post('user_agent')!='NI-IAPP')
				{
					$industry_hire = (count($this->input->post('industry_hire')) > 0 && $this->input->post('industry_hire')!='' && is_array($this->input->post('industry_hire'))) ? implode(",",$this->input->post('industry_hire')) : "";
					$function_area_hire = (count($this->input->post('function_area_hire')) > 0 && $this->input->post('function_area_hire')!='' && is_array($this->input->post('function_area_hire'))) ? implode(",",$this->input->post('function_area_hire')) : "";
					$skill_hire = (count($this->input->post('skill_hire')) > 0 && $this->input->post('skill_hire')!='' && is_array($this->input->post('skill_hire'))) ? implode(",",$this->input->post('skill_hire')) : "";	
				}
				else
				{
					$industry_hire = $this->input->post('industry_hire');
					$function_area_hire = $this->input->post('function_area_hire');
					$skill_hire = $this->input->post('skill_hire');
				}
				$data_array_custom = array('industry_hire'=>$industry_hire,'function_area_hire'=>$function_area_hire,'skill_hire'=> $skill_hire);
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$datavar['result']="success";
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
	public function get_employerdetail_frm_email($email="")
	{
		$returnvar = array();
		if($email!='')
		{
			$where_arra = array('email'=>$email);
			$returnvar = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,'','','','','');
		}
		return $returnvar;
	}
	public function get_employerdetail_frm_id($id="")
	{
		$returnvar = array();
		if($id!='')
		{
			$where_arra = array('id'=>$id);
			$returnvar = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,'','','','','');
		}
		return $returnvar;
	}
	function getdetailsfromids($table='',$id_filed='',$id='',$requiredfield='')
	{
		$returnvar = array();
		if($id!='' && $table!='' && $id_filed!='')
		{
			$requiredfield = $requiredfield!='' ? "`".$requiredfield."`" : $requiredfield ;
			
			$id_filed = "`".$id_filed."`" ;
			$where_arra_id = $id_filed." in ( '".str_replace(",","','",$id)."' )";
			$where_arra = array('status' => 'APPROVED',$where_arra_id);//$id_filed => $id,
			$list_count = $this->common_front_model->get_count_data_manual($table,$where_arra,'0',$requiredfield,$id_filed.' desc','','','');
			if($list_count!='' && $list_count > 0)
			{
				$resultstored = $this->common_front_model->get_count_data_manual($table,$where_arra,2,$requiredfield,$id_filed.' desc','','','');
				if($requiredfield!='')
				{
					foreach($resultstored as $resultstored_sing)
					{
						$returnvar[] = $resultstored_sing[str_replace( '`', "", $requiredfield )];
					}	
				}
				else
				{
					foreach($resultstored as $resultstored_sing)
					{
						$returnvar[] = $resultstored_sing;
					}
				}
				
			}
		}
		return $returnvar;
	}
	function getdetailsfromidssingle($table='',$id_filed='',$id='',$requiredfield='')
	{
		
		$returnvar = array();
		if($id!='' && $table!='' && $id_filed!='')
		{
			$requiredfield = $requiredfield!='' ? "`".$requiredfield."`" : $requiredfield ;
			
			$id_filed = "`".$id_filed."`" ;
			$where_arra_id = $id_filed." = ".str_replace(",","','",$id)."";
			$where_arra = array('status' => 'APPROVED',$where_arra_id);//$id_filed => $id,
			$list_count = $this->common_front_model->get_count_data_manual($table,$where_arra,'0',$requiredfield,$id_filed.' desc','','','');
			if(isset($list_count) && $list_count !='' && $list_count  > 0)
			{
				$resultstored = $this->common_front_model->get_count_data_manual($table,$where_arra,1,$requiredfield,$id_filed.' desc','','','');
				if($requiredfield!='')
				{
					$returnvar[str_replace( '`', "", $requiredfield )] = $resultstored[str_replace( '`', "", $requiredfield )];
				}
				else
				{
					$returnvar[] = $resultstored_sing;
				}
			}
		}	
		//print_r($returnvar);
		return $returnvar;
	}
	
	function activity_list_for_emp($page,$limit=2,$get_list)
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB';
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		if($this->input->post('get_list'))
		{
			$get_list = $this->input->post('get_list');
		}
		
		$limit = isset($limit) ? $limit : $this->common_front_model->limit_per_page;
		$emp_id = $this->common_front_model->get_empid();
		
		$this->db->join('jobseeker_access_employer je', 'je.js_id = js.id', 'inner');
		$where_arr = array('is_liked'=>'Yes','emp_id'=>$emp_id,'js.profile_visibility'=>'Yes'); // Default For like employer list  
		if($get_list == 'block_emp')
		{
		   $where_arr = array('is_block'=>'Yes','emp_id'=>$emp_id,'js.profile_visibility'=>'Yes'); // For block employer list
		}
		if($get_list == 'follow_emp')
		{
		   $where_arr = array('is_follow'=>'Yes','emp_id'=>$emp_id,'js.profile_visibility'=>'Yes'); // For follow employer list
		}
		
    	$activity_list_count = $this->common_front_model->get_count_data_manual('jobseeker_view js',$where_arr,0);
		//echo $this->db->last_query();
		$this->db->join('jobseeker_access_employer je', 'je.js_id = js.id', 'inner');  
		$activity_list_data = $this->common_front_model->get_count_data_manual('jobseeker_view js',$where_arr,2,'','je.id desc',$page,$limit);
		//echo $this->db->last_query();
		
		 $data['activity_list_count'] = $activity_list_count;
		 $data['activity_list_data'] =  $activity_list_data;
		 $data['status'] = 'success';
		 return $data;
	}
	
	function view_js_details()
	{
		if($this->input->post('js_id') && $this->input->post('js_id')!='')
			{
				$js_id = $this->input->post('js_id');
				$where_arr = array('id'=>$js_id);
				$get_js_data = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arr,1);
				$data['status'] = 'success';
				$data['js_data'] = $get_js_data;
			}
			else
			{
				$data['errmessage'] = $this->lang->line('js_action_err_msg');
				$data['status'] = 'error';
			}
			return $data;
	}
	public function delete_pro_req_chk($id='')
	{
		$del_req_count = 0;
		if($id!='')
		{
			$where_arra = array('req_id'=>$id,'user_type'=>'emp');
			$del_req_count = $this->common_front_model->get_count_data_manual('delete_profile_request',$where_arra,0,'id','id desc','','','');
		}
		return $del_req_count;
	}
	public function delete_pro_req($idofemp =  "")
	{
		$datavar = array();
		/* save the data */
		if($idofemp!="")
		{
			$checkifmemberexists = $this->get_employerdetail_frm_id($idofemp);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				$reason_for_del = ($this->input->post('message')) ? $this->input->post('message') : ""; 
				$check_if_req_exitst = $this->delete_pro_req_chk($idofemp);
				if(!($check_if_req_exitst > 0))
				{
					$data_array_custom = array('req_id'=> $idofemp,'user_type'=>'emp','reason_for_del'=>$reason_for_del);
					$this->common_front_model->save_update_data('delete_profile_request',$data_array_custom,'id','add','','','1','1','is_deleted',2);
					$datavar['result']="success";
				}
				else
				{
					$datavar['result']="already_there";	
				}
			}
			else
			{
				$datavar['result']="error";
			}
		}
		else
		{
			$datavar['result']="error";
		}
		/* save th data end */
		return $datavar;
	}
}
?>