<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Blog_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function gettotalblogcount()
	{
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$blogcount = $this->common_front_model->get_count_data_manual('blog_master',$where_arra,0,'id','created_on desc','');
		return $blogcount;
	}
	public function getblogdata($where_arra=array(),$page=1,$limit=5)
	{		
		$blogdata = $this->common_front_model->get_count_data_manual('blog_master',$where_arra,2,'','created_on desc',$page,$limit);
		return $blogdata;
	}
}
?>