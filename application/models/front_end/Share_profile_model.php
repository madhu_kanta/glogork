<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Share_profile_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}

	function get_js_details($id)
	{
		$where_arr = array('id'=>$id,"is_deleted"=>'No');
		$js_data = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arr,1);
		return $js_data;	
	}
	
	function get_emp_details($id)
	{
		$where_arr = array('id'=>$id,"is_deleted"=>'No');
		$js_data = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arr,1);
		return $js_data;	
	}
	
	function get_emp_hiring_sector_name($table,$ids,$get_name)
	{
		$list_name = $this->employer_profile_model->getdetailsfromids($table,'id',$ids,$get_name);
		return $list_name;
	}
	
}