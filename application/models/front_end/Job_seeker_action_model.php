<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_seeker_action_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	function process_js_activity_list($data = '')
	{
		$data_retrn = array();
		if(isset($data) && $data !='' && is_array($data) && count($data) > 0)
		{
			foreach($data as $data_val)
			{
				$data_val['location_hiring_name'] = $this->common_model->valueFromId('city_master',$data_val['location_hiring'],'city_name');
				//$data_val['job_education_name'] = $this->common_model->valueFromId('educational_qualification_master',$data_val['job_education'],'educational_qualification');
				//$data_val['job_counter'] = $this->common_front_model->get_counter('',$data_val['id']);
				$data_retrn[] = $data_val;
			}
		}
		return $data_retrn;
	}
	function get_emp_email($get_id,$depend_on)
	{
		$where = array('id'=>$get_id);
		if($depend_on=='Job')
		{
			$get_emp_email = $this->common_front_model->get_count_data_manual("job_posting_view",$where,1,'email','','',1);
		}
		else
		{
			$get_emp_email = $this->common_front_model->get_count_data_manual("employer_master",$where,1,'email','','',1);
			//echo $this->db->last_query();
		}
		return $get_emp_email;
	}
	function seeker_action()
	{
		if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
			{
				$js_id = $this->common_front_model->get_userid();
			}
			
		$action = $this->input->post('action');
		$action_for = $this->input->post('action_for');		
		$action_id = $this->input->post('action_id');	
		$update_on = $this->common_front_model->getCurrentDate();
		
		$action_array[] = array(
		'like_job'=>array("tbl_name"=>'jobseeker_viewed_jobs','where'=>array('js_id'=>$js_id,'job_id'=>$action_id),'data_insert'=>array('is_liked'=>'Yes','js_id'=>$js_id,'job_id'=>$action_id,'liked_on'=>$update_on),'data_update'=>array('is_liked'=>'Yes','liked_on'=>$update_on)),
		'unlike_job'=>array("tbl_name"=>'jobseeker_viewed_jobs','where'=>array('js_id'=>$js_id,'job_id'=>$action_id),'data_update'=>array('is_liked'=>'No','liked_on'=>$update_on)),
		'save_job'=>array("tbl_name"=>'jobseeker_viewed_jobs','where'=>array('js_id'=>$js_id,'job_id'=>$action_id),'data_insert'=>array('is_saved'=>'Yes','js_id'=>$js_id,'job_id'=>$action_id,'saved_on'=>$update_on),'data_update'=>array('is_saved'=>'Yes','saved_on'=>$update_on)),
		'remove_save_job'=>array("tbl_name"=>'jobseeker_viewed_jobs','where'=>array('js_id'=>$js_id,'job_id'=>$action_id),'data_update'=>array('is_saved'=>'No','saved_on'=>$update_on)),
		'block_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_insert'=>array('is_block'=>'Yes','js_id'=>$js_id,'emp_id'=>$action_id,'blocked_on'=>$update_on),'data_update'=>array('is_block'=>'Yes','blocked_on'=>$update_on)),
		'unblock_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_update'=>array('is_block'=>'No','blocked_on'=>$update_on)),
		'follow_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_insert'=>array('is_follow'=>'Yes','js_id'=>$js_id,'emp_id'=>$action_id,'followed_on'=>$update_on),'data_update'=>array('is_follow'=>'Yes','followed_on'=>$update_on)),
		'unfollow_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_update'=>array('is_follow'=>'No','followed_on'=>$update_on)),
		'like_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_insert'=>array('is_liked'=>'Yes','js_id'=>$js_id,'emp_id'=>$action_id,'liked_on'=>$update_on),'data_update'=>array('is_liked'=>'Yes','liked_on'=>$update_on)),
		'unlike_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id),'data_update'=>array('is_liked'=>'No','liked_on'=>$update_on)),
		'delete'=>array("tbl_name"=>'jobseeker_viewed_jobs','where'=>array('js_id'=>$js_id,'job_id'=>$action_id)),
		'delete_emp'=>array("tbl_name"=>'jobseeker_access_employer','where'=>array('js_id'=>$js_id,'emp_id'=>$action_id)),
		);
		$check_action_array = $action_array[0]; 
		
		if($js_id!='')
		{
			if($action == 'delete' || $action == 'delete_emp')
			{
				 $this->common_front_model->data_delete_common($check_action_array[$action]['tbl_name'],$check_action_array[$action]['where'],1);
			}
			else
			{
			   $get_count = $this->common_front_model->get_count_data_manual($check_action_array[$action]['tbl_name'],$check_action_array[$action]['where'],0);
			   
			   if($get_count==0)
			   { 
				   $this->common_front_model->update_insert_data_common($check_action_array[$action]['tbl_name'],$check_action_array[$action]['data_insert'],'',0);
				}
			   else
			   { 
				   $this->common_front_model->update_insert_data_common($check_action_array[$action]['tbl_name'],$check_action_array[$action]['data_update'],$check_action_array[$action]['where']);
			   }
			}
		 return "success";  
		}
		else
		{
			return "error";
		}
		
		
	}
	
	function send_message()
	{
		if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='' && $this->input->post('emp_id')!='')
			{
				$sender = $this->common_front_model->get_userid();
				$receiver = $this->input->post('emp_id');
				$sent_on = $this->common_front_model->getCurrentDate();
				$data_array_custom = array('sender'=>$sender,'receiver'=>$receiver,'sent_on'=>$sent_on,'status'=>'sent');
				if($sender!='')
				{
					$return_data_plan_msg = $this->common_front_model->get_plan_detail($sender,'job_seeker','message');
					if($return_data_plan_msg=='Yes')	
					{
						$this->common_front_model->save_update_data('message',$data_array_custom);
				
						$get_email = $this->common_front_model->getemailtemplate('Send message to employer');
						if($get_email!='' && is_array($get_email) && count($get_email) > 0 )
						{
						   $js_id = $this->common_front_model->get_userid();	
						   $login_user_details = $this->common_front_model->get_login_user_data($js_id,'fullname');
						   $get_emp_email = $this->get_emp_email($receiver,'Emp');
						   $email = $get_emp_email['email']; // Employer email (for sending mail)
						   $config_data = $this->common_front_model->data['config_data'];
						   $webfriendlyname = $config_data['web_frienly_name'];
						   $subject = $get_email['email_subject'];
						   $email_content= $get_email['email_content'];
						   $email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
						   $trans = array("websitename" =>$webfriendlyname,"sender_name"=>$login_user_details['fullname']);
						   $email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
						   $this->common_front_model->common_send_email($email,$subject,$email_template);
						}
						$this->common_model->update_plan_detail($sender,'job_seeker','message');
						return "success";
					}
					else
					{
						return "error_plan";		
					}
				}
			}
			else
			{
				return "error";
			}
		
	}
	
	function apply_for_job()
	{
		if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='' && $this->input->post('job_id')!=''){
				$job_id = $this->input->post('job_id');
				$where_arra = array('id'=>$job_id,'currently_hiring_status'=>'Yes');
				$job_data = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,1);
				if($job_data!='' && is_array($job_data) && count($job_data) > 0)
				{
					$js_id = $this->common_front_model->get_userid();
					$applied_on = $this->common_front_model->getCurrentDate();
					$data_array_custom = array('js_id'=>$js_id,'job_id'=>$job_id,'applied_on'=>$applied_on);
					$this->common_front_model->save_update_data('job_application_history',$data_array_custom);
					$get_emp_email = $this->get_emp_email($job_id,'Job');
					$email = $get_emp_email['email']; // Employer email (for sending mail)
					$login_user_details = $this->common_front_model->get_login_user_data($js_id,'fullname');
					$get_email = $this->common_front_model->getemailtemplate('Apply for job');
					if($get_email!='' && is_array($get_email) && count($get_email) > 0 )
					{
					   //$js_id = $this->common_front_model->get_userid();	
					   //$login_user_details = $this->common_front_model->get_login_user_data($js_id,'fullname');
					   $config_data = $this->common_front_model->data['config_data'];
					   $webfriendlyname = $config_data['web_frienly_name'];
					   $subject = $get_email['email_subject'];
					   $email_content= $get_email['email_content'];
					   $email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
					   $trans = array("websitename" =>$webfriendlyname,"sender_name"=>$login_user_details['fullname']);
					   $email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
					   $this->common_front_model->common_send_email($email,$subject,$email_template);
					}	
					 
					 $data['errmessage'] =  $this->lang->line('apply_job_success_msg');
					 $data['status'] = 'success';
					//return "success";
				}
				else
				{
					$data['errmessage'] =  'This job status is currently close.Try for another job.';
					$data['status'] = 'error';
					//return "error close";
				}
			}
			else
			{
				$data['errmessage'] =  $this->lang->line('apply_job_err_msg');
				$data['status'] = 'error';
				//return "error";
			}
			return $data;
	}
	
	function js_activity_list($page,$limit='',$get_list)
	{
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		if($this->input->post('get_list'))
		{
			$get_list = $this->input->post('get_list');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$js_id = $this->common_front_model->get_userid();
		$where_arr = array('js_id'=>$js_id); // default for view job list
		$this->db->join('jobseeker_viewed_jobs jv', 'jv.job_id = jp.id', 'inner');  
		if($get_list == 'like_job')
		{
		   $where_arr = array('is_liked'=>'Yes','js_id'=>$js_id); // For like job list
		}
		if($get_list == 'save_job')
		{
		   $where_arr = array('is_saved'=>'Yes','js_id'=>$js_id); // For saved job list
		}
		
    	$activity_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		$this->db->join('jobseeker_viewed_jobs jv', 'jv.job_id = jp.id', 'inner');  
		$activity_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'','jv.id desc',$page,$limit);
	     $data['query'] = $this->db->last_query();
		
		 $data['activity_list_count'] = $activity_list_count;
		 $data['activity_list_data'] =  $activity_list_data;
		 $data['status'] = 'success';
		 return $data;
	}
	
	function js_activity_list_emp($page,$limit='',$get_list)
	{
		
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		if($this->input->post('get_list'))
		{
			$get_list = $this->input->post('get_list');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$js_id = $this->common_front_model->get_userid();
		
		$this->db->join('jobseeker_access_employer je', 'je.emp_id = emp.id', 'inner');
		$where_arr = array('is_liked'=>'Yes','js_id'=>$js_id); // Default For like employer list  
		$order_by = "je.liked_on desc";
		if($get_list == 'block_emp')
		{
		   $where_arr = array('is_block'=>'Yes','js_id'=>$js_id); // For block employer list
		   $order_by = "je.blocked_on desc";
		}
		if($get_list == 'follow_emp')
		{
		   $where_arr = array('is_follow'=>'Yes','js_id'=>$js_id); // For follow employer list
		    $order_by = "je.followed_on desc";
		}
		
    	$activity_list_count = $this->common_front_model->get_count_data_manual('employer_master_view emp',$where_arr,0);
		//$this->db->order_by("je.id", "desc");
		$this->db->join('jobseeker_access_employer je', 'je.emp_id = emp.id', 'inner');  
		$activity_list_data = $this->common_front_model->get_count_data_manual('employer_master_view emp',$where_arr,2,'',$order_by,$page,$limit);
		//echo $this->db->last_query();
		
		 $data['activity_list_count'] = $activity_list_count;
		 $data['activity_list_data'] =  $activity_list_data;
		 $data['status'] = 'success';
		 return $data;
	}
	
	function js_apply_job_list($page,$limit='')
	{
		
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
		if($this->input->post('limit_per_page'))
		{
			$limit = $this->input->post('limit_per_page');
		}
		
		$limit = isset($limit) && $limit!='' ? $limit : $this->common_front_model->limit_per_page;
		$js_id = $this->common_front_model->get_userid();
		
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$where_arr = array('js_id'=>$js_id); 
		$apply_list_count = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,0);
		//echo $this->db->last_query();
		$this->db->join('job_application_history jah', 'jah.job_id = jp.id', 'inner');
		$apply_list_data = $this->common_front_model->get_count_data_manual('job_posting_view jp',$where_arr,2,'','jah.id desc',$page,$limit);
		 //echo $this->db->last_query();
		 $data['apply_list_count'] = $apply_list_count;
		 $data['apply_list_data'] =  $apply_list_data;
		 $data['status'] = 'success';
		 return $data;
	
	}
	
	function view_emp_details()
	{
		if($this->input->post('emp_id'))
			{
				$emp_id = $this->input->post('emp_id');
				$where_arr = array('id'=>$emp_id);
				$get_emp_data = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arr,1);
				$data['status'] = 'success';
				$data['emp_data'] = $get_emp_data;
			}
			else
			{
				$data['errmessage'] = $this->lang->line('js_action_err_msg');
				$data['status'] = 'error';
			}
			return $data;
	}
	
}