<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	function get_data($id)
	{
		$data = '';
		if($id !='')
		{
			$where_arra = array('id'=>$id);
			$data = $this->common_model->get_count_data_manual('job_posting_view',$where_arra,1,'');
		}
		return $data;
	}
	function job_list_model($status ='ALL', $page =1)
	{				
		$total_exp= '<div class="col-sm-5 col-lg-5 pl0">
	<select name="work_experience_from" id="work_experience_from" class="form-control">
		<option selected value="" >From</option>';
		for($i=0;$i <= 25 ; $i=$i+0.5)
		{
			$explode = explode('.',$i);
			$m_val = '0';
			if(isset($explode[1]) && $explode[1]!='')
			{
				$m_val = $explode[1] + 1;
			}
			$val = $explode[0].'.'.$m_val;
			$disp_val = '';
			if($explode[0]=='0' && $m_val=='0')
			{
				$disp_val = "Fresher";
			} 
			else
			{ 
				$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
			}
			$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
		}
$total_exp.='</select>
	</div>
	<div class="col-sm-1 col-lg-1">To</div>
	<div class="col-sm-5 col-lg-5 pr0">
	<select name="work_experience_to" id="work_experience_to" class="form-control">
		<option selected value="" >To</option>';
		for($i=0;$i <= 25 ; $i=$i+0.5)
		{
			$explode = explode('.',$i);
			$m_val = '0';
			if(isset($explode[1]) && $explode[1]!='')
			{
				$m_val = $explode[1] + 1;
			}
			$val = $explode[0].'.'.$m_val;
			$disp_val = '';
			if($explode[0]=='0' && $m_val=='0')
			{
				$disp_val = "Fresher";
			} 
			else
			{ 
				$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
			}
			$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
		}
		$total_exp.='</select></div>';
		$where_curr = " ( is_deleted ='No' )";
		$currnecy_list_arr = $this->common_model->get_count_data_manual('currency_master',$where_curr,2,'id,currency_name,currency_code','','','','');
		
		$salary_list_arr = $this->common_model->get_count_data_manual('salary_range',$where_curr,2,'*','','','','');
		
		$exp_anual_salary_str= '<div class="col-sm-12 col-lg-12 pl0">
			<select name="job_salary" id="job_salary" class="form-control" required >
				<option value="">Select Salary range</option>';
				if(isset($salary_list_arr) && $salary_list_arr !='' && is_array($salary_list_arr) && count($salary_list_arr) > 0)
				{
					foreach($salary_list_arr as $salary_list_val)
					{
						$exp_anual_salary_str.= '<option value='.$salary_list_val['id'].'>'.$salary_list_val['salary_range'].'</option>';
					}
				}
			$exp_anual_salary_str.='</select>
			</div>';
		/*$exp_anual_salary_str= '<div class="col-sm-4 col-lg-4 pl0">
			<select name="currency_type" id="currency_type" class="form-control" required >
				<option value="">Select Currency</option>';
				foreach($currnecy_list_arr as $currnecy_list_val)
				{
					$exp_anual_salary_str.= '<option value='.$currnecy_list_val['currency_code'].'>'.$currnecy_list_val['currency_code'].'</option>';
				}
			$exp_anual_salary_str.='</select>
			</div>
			<div class="col-sm-4 col-lg-4 ">
				<select name="job_salary_from" required id="job_salary_from" class="form-control">
				<option selected value="" >From</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$exp_anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$exp_anual_salary_str.='</select>
			</div>
			<div class="col-sm-4 col-lg-4 pr0">
				<select name="job_salary_to" required id="job_salary_to" class="form-control">
				<option selected value="" >To</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$exp_anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$exp_anual_salary_str.='</select>
			</div>';*/
			$temp_arr = range(1,100);
			$temp_arr_tot = array_combine($temp_arr,$temp_arr);
		$ele_array = array(
			'posted_by'=>array('is_required'=>'required'),
			'job_title'=>array('is_required'=>'required'),
			'job_description'=>array('type'=>'textarea','is_required'=>'required'),
			'skill_keyword'=>array('is_required'=>'required'),
			'work_experience'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Work Experience</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$total_exp.'
			  </div>
			</div>'),
			'anual_salary'=>array('type'=>'dropdown','value_arr'=>array('1'=>'Define Salary','2'=>'As per company rules and regulations'),'value'=>'1','onchange'=>'display_salary_ddr()','display_placeholder'=>'no','label'=>'Annual Salary'),
			'job_salary_text'=>array('is_required'=>'required','label'=>'Description for salary','form_group_class'=>'salary_1','value'=>''),
			'job_salary'=>array('type'=>'manual','code'=>'
			<div class="form-group salary_2">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Job Salary</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$exp_anual_salary_str.'
			  </div>
			</div>'),
			/*'job_salary'=>array('type'=>'manual','code'=>'
			<div class="form-group salary_2">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Job Salary</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$exp_anual_salary_str.'
			  </div>
			</div>'),*/
			'location_hiring'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'city_master','key_val'=>'id','key_disp'=>'city_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'company_hiring'=>array('is_required'=>'required'),
			'link_job'=>array('input_type'=>'url'),
			'job_industry'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name')),
			'functional_area'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'onchange'=>"dropdownChange('functional_area','job_post_role','role_master')"),
			'job_post_role'=>array('is_required'=>'required','type'=>'dropdown'),
			'job_shift'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'shift_type','key_val'=>'id','key_disp'=>'shift_type')),
			'job_type'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'job_type_master','key_val'=>'id','key_disp'=>'job_type')),
			'job_payment'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'job_payment_master','key_val'=>'id','key_disp'=>'job_payment')),
			'job_education'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'educational_qualification_master','key_val'=>'id','key_disp'=>'educational_qualification'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'total_requirenment'=>array('is_required'=>'required','type'=>'dropdown','value_arr'=>$temp_arr_tot),
			'desire_candidate_profile'=>array('type'=>'textarea'),
			'currently_hiring_status'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No'),'value'=>'Yes'),
			'job_highlighted'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No'),'value'=>'No'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->extra_css[] = 'vendor/chosen_v1.4.0/chosen.min.css';
		$this->common_model->extra_css[] = 'vendor/jquery-tag-type/tokenfield-typeahead.css';
		$this->common_model->extra_css[] = 'vendor/jquery-tag-type/bootstrap-tokenfield.css';
		$this->common_model->extra_css[] = 'vendor/jquery-tag-type/jquery-ui.css';

		$this->common_model->extra_js[] = 'vendor/chosen_v1.4.0/chosen.jquery.min.js';
		$this->common_model->extra_js[] = 'vendor/jquery-tag-type/jquery-ui.js';
		$this->common_model->extra_js[] = 'vendor/jquery-tag-type/bootstrap-tokenfield.js';
		$this->common_model->extra_js[] = 'vendor/typeahead/typeahead.jquery.min.js';
	
		$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','',1); 
		$key_skill_data = json_encode($key_skill);
		
		$this->common_model->js_extra_code.= "
			function display_salary_ddr()
			{
				var anual_salary = $('#anual_salary').val();
				if(anual_salary == 1)
				{
					$('.salary_2').show();
					$('.salary_1').hide();
				}
				else
				{
					$('.salary_1').show();
					$('.salary_2').hide();
				}
			} 
			$('#posted_by').tokenfield({
				autocomplete: {
				  source: function (request, response) {
					  $.get('".base_url()."common_request/get_suggestion_compnay/'+request.term, {
					  }, function (data) {
						 response(data);
					  });
				  },
				  delay: 100
				},
				showAutocompleteOnFocus: true
			});
			$('#posted_by').on('tokenfield:createtoken', function (event) {
				var existingTokens = $(this).tokenfield('getTokens');
				 if (existingTokens != '')
				 {
					 event.preventDefault();
				 }
			});
		$(function(){
			display_salary_ddr();
			$('#skill_keyword').tokenfield({
				autocomplete: {
				source: ".$key_skill_data." ,
				delay: 100,
			  },
			  showAutocompleteOnFocus: true,
			});
			$('#skill_keyword').on('tokenfield:createtoken', function (event) {
				var existingTokens = $(this).tokenfield('getTokens');
				$.each(existingTokens, function(index, token) {
					if (token.value === event.attrs.value)
						event.preventDefault();
				});
			});
			
			var config = {
			'.chosen-select': {},
			'.chosen-select-deselect': { allow_single_deselect: true },
			'.chosen-select-no-single': { disable_search_threshold: 10 },
			'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
			'.chosen-select-width': { width: '100%' }			
			};
			$('#location_hiring').chosen({placeholder_text_multiple:'Select Location Hiring'});
			$('#job_education').chosen({placeholder_text_multiple:'Select Education Required'});
		});";
		
		$data_table = array(
			'title_disp'=>'job_title',
			'disp_column_array'=> array('company_name','posted_by_employee','skill_keyword','location_hiring','company_hiring','industries_name','functional_name','job_role_name','job_shift_type','job_type_name','work_experience_from','total_requirenment','posted_on','job_expired_on')
		);
		// pass #id# it will replace with table primary key value in url
		$btn_arr = array(
			array('url'=>'job/job_detail/#id#/edit','class'=>'info','label'=>'Edit Job','target'=>'_blank'),
			array('url'=>'job/job_detail/#id#','class'=>'primary','label'=>'View Detail','target'=>'_blank')
		);
		$other_config = array('load_member'=>'yes','data_table_mem'=>$data_table,'data_tab_btn'=>$btn_arr,'default_order'=>'desc','action'=>'job/save_new_job','sort_column'=>array('posted_on'=>'Latest','job_title'=>'Job Title'),'display_image'=>'company_logo','company_logo'=>'assets/company_logos/'); // load member for data table display member listing not table
		$this->display_filter_form();
		
		$this->common_model->label_col = 2;
		$this->common_model->form_control_col =7;
		$this->common_model->addPopup = 0;
		$this->common_model->common_rander('job_posting_view', $status, $page , 'Job Listing',$ele_array,'posted_on',0,$other_config);
	}
	function display_filter_form()
	{	
		$this->common_model->extra_css[] = 'vendor/chosen_v1.4.0/chosen.min.css';
		$this->common_model->extra_js[] = 'vendor/chosen_v1.4.0/chosen.jquery.min.js';
		$url_city = base_url()."sign_up/get_suggestion_city/id/";
		$this->common_model->js_extra_code.= " var config = {
			'.chosen-select': {},
			'.chosen-select-deselect': { allow_single_deselect: true },
			'.chosen-select-no-single': { disable_search_threshold: 10 },
			'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
			'.chosen-select-width': { width: '100%' }			
			};
			$('#industry_search').chosen({placeholder_text_multiple:'Select Industry'});
			$('#functional_area_search').chosen({placeholder_text_multiple:'Select Functional Area'});
			$('#job_role_search').chosen({placeholder_text_multiple:'Select Job Role'});
			$('#job_shift_search').chosen({placeholder_text_multiple:'Select Job Shift'});
			$('#job_type_search').chosen({placeholder_text_multiple:'Select Job Type'});
			
			$('#job_location_search').tokenfield({
				autocomplete: {
				  source: function (request, response) {
					  
					  $.get('".base_url()."sign_up/get_suggestion_city/id/'+request.term+'', {
					  }, function (data) {
						 response(data);
					  });
				  },
				  delay: 100,
				},
				showAutocompleteOnFocus: true
			});
			/*$('#job_location_search').on('tokenfield:createtoken', function (event) {
				var existingTokens = $(this).tokenfield('getTokens');
				 if (existingTokens != '')
				 {
					 event.preventDefault();
				 }
			}); */
		";

		$this->common_model->label_col = 3;
		$this->common_model->form_control_col =9;
		$this->common_model->addPopup = 1;
		$total_exp= '<div class="col-sm-5 col-lg-5 pl0">
			<select name="total_exp_from" id="total_exp_from" class="form-control">
				<option selected value="" >From</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  	$val = $explode[0].'.'.$m_val;
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
					$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>
			<div class="col-sm-5 col-lg-5 pr0">
			<select name="total_exp_to" id="total_exp_to" class="form-control">
				<option selected value="" >To</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  	$val = $explode[0].'.'.$m_val;
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
					$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='
		</select></div>';
		$anual_salary_str= '
			<div class="col-sm-5 col-lg-5 pl0">
				<select name="annual_salary_from" id="annual_salary_from" class="form-control">
				<option selected value="" >From</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$anual_salary_str.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>	
			<div class="col-sm-5 col-lg-5 pr0">
				<select name="annual_salary_to" id="annual_salary_to" class="form-control">
				<option selected value="" >To</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$anual_salary_str.='</select>
			</div>';
			
			
	/*	'industry'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name')), */
		$ele_array = array(
			'keyword'=>array('placeholder'=>'Search with Job Title,Company Name, Employee name, Skill Keyword, Industries Name, Company Hiring, Key Skill..'),
			'industry_search'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'functional_area_search'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','onchange'=>"dropdownChange_mul('functional_area_search','job_role_search','role_master')"),
			'job_role_search'=>array('type'=>'dropdown','is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'job_location_search'=>array(),
			'total_experience'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Work Experience</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$total_exp.'			  
			  </div>
			</div>'),
			/*'annual_salary'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Job Salary</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$anual_salary_str.'
			  </div>
			</div>'),*/
			'annual_salary'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range'),'label'=>'Job Salary'),
			'job_shift_search'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'shift_type','key_val'=>'id','key_disp'=>'shift_type'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'job_type_search'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'job_type_master','key_val'=>'id','key_disp'=>'job_type'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
		);
		$other_config = array('mode'=>'add','id'=>'','action'=>'job/search_model','form_id'=>'form_model_search');
		$this->common_model->set_table_name('jobseeker');
		$data = $this->common_model->generate_form_main($ele_array,$other_config);
		$this->common_model->data['model_title_fil'] = 'Filter Data';
		$this->common_model->data['model_body_fil'] = $data;
	}
	function save_session_search()
	{
		$search_array = array(
			'keyword'=>'',
			'industry_search'=>'',
			'functional_area_search'=>'',
			'job_role_search'=>'',
			'total_exp_from'=>'',
			'total_exp_to'=>'',
			'annual_salary'=>'',
			/*'annual_salary_from'=>'',
			'annual_salary_to'=>'',*/
			'job_location_search'=>'',
			'job_shift_search'=>'',
			'job_type_search'=>''
		);
		if($this->input->post('keyword') && $this->input->post('keyword') !='')
		{
			$keyword = $this->input->post('keyword');
			$search_array['keyword'] = $keyword;
		}
		if($this->input->post('industry_search') && $this->input->post('industry_search') !='')
		{
			$industry = $this->input->post('industry_search');
			$search_array['industry_search'] = $industry;
		}
		if($this->input->post('functional_area_search') && $this->input->post('functional_area_search') !='')
		{
			$functional_area = $this->input->post('functional_area_search');
			$search_array['functional_area_search'] = $functional_area;
		}
		if($this->input->post('job_role_search') && $this->input->post('job_role_search') !='')
		{
			$job_role = $this->input->post('job_role_search');
			$search_array['job_role_search'] = $job_role;
		}
		
		if($this->input->post('total_exp_from') && $this->input->post('total_exp_from') !='')
		{
			$total_exp_from = $this->input->post('total_exp_from');
			$search_array['total_exp_from'] = $total_exp_from;
		}
		if($this->input->post('total_exp_to') && $this->input->post('total_exp_to') !='')
		{
			$total_exp_to = $this->input->post('total_exp_to');
			$search_array['total_exp_to'] = $total_exp_to;
		}
		
		if($this->input->post('job_location_search') && $this->input->post('job_location_search') !='')
		{
			$total_exp_to = $this->input->post('job_location_search');
			$total_exp_to_arr = explode(',',$total_exp_to);
			$total_exp_to_arr_temp = array();
			foreach($total_exp_to_arr as $total_exp_to_arr_val)
			{
				$total_exp_to_arr_temp[] = trim($total_exp_to_arr_val);
			}
			$search_array['job_location_search'] = $total_exp_to_arr_temp;
		}
		if($this->input->post('job_shift_search') && $this->input->post('job_shift_search') !='')
		{
			$total_exp_to = $this->input->post('job_shift_search');
			$search_array['job_shift_search'] = $total_exp_to;
		}
		if($this->input->post('job_type_search') && $this->input->post('job_type_search') !='')
		{
			$total_exp_to = $this->input->post('job_type_search');
			$search_array['job_type_search'] = $total_exp_to;
		}
		if($this->input->post('annual_salary') && $this->input->post('annual_salary') !='')
		{
			$annual_salary = $this->input->post('annual_salary');
			$search_array['annual_salary'] = $annual_salary;
		}
		/*if($this->input->post('annual_salary_from') && $this->input->post('annual_salary_from') !='')
		{
			$annual_salary_from = $this->input->post('annual_salary_from');
			$annual_salary_from = number_format((float)$annual_salary_from, 2, '.', '');
			$search_array['annual_salary_from'] = str_replace('.','-',$annual_salary_from);
		}
		if($this->input->post('annual_salary_to') && $this->input->post('annual_salary_to') !='')
		{
			$annual_salary_to = $this->input->post('annual_salary_to');
			$annual_salary_to = number_format((float)$annual_salary_to, 2, '.', '');
			$search_array['annual_salary_to'] = str_replace('.','-',$annual_salary_to);
		}
		
		if($this->input->post('exp_annual_salary_from') && $this->input->post('exp_annual_salary_from') !='')
		{
			$exp_annual_salary_from = $this->input->post('exp_annual_salary_from');
			$exp_annual_salary_from = number_format((float)$exp_annual_salary_from, 2, '.', '');
			$search_array['exp_annual_salary_from'] = str_replace('.','-',$exp_annual_salary_from);
		}
		if($this->input->post('exp_annual_salary_to') && $this->input->post('exp_annual_salary_to') !='')
		{
			$exp_annual_salary_to = $this->input->post('exp_annual_salary_to');
			$exp_annual_salary_to = number_format((float)$exp_annual_salary_to, 2, '.', '');
			$search_array['exp_annual_salary_to'] = str_replace('.','-',$exp_annual_salary_to);
		}*/
		$this->session->set_userdata('job_save_search',$search_array);
		$this->common_model->return_tocken_clear();
	}
	function save_new_job()
	{
		if(isset($_REQUEST['location_hiring']) && $_REQUEST['location_hiring'] !='')
		{
			$val_req = $_REQUEST['location_hiring'];
			$tmp_val = implode(',',$val_req);
			$_REQUEST['location_hiring'] = $tmp_val;
		}
		if(isset($_REQUEST['job_education']) && $_REQUEST['job_education'] !='')
		{
			$val_req = $_REQUEST['job_education'];
			$tmp_val = implode(',',$val_req);
			$_REQUEST['job_education'] = $tmp_val;
		}
		if(isset($_REQUEST['posted_by']) && $_REQUEST['posted_by'] !='')
		{
			$posted_by = $_REQUEST['posted_by'];
		}
		if(isset($_REQUEST['anual_salary']) && $_REQUEST['anual_salary'] !='')
		{
			if(isset($_REQUEST['anual_salary']) && $_REQUEST['anual_salary'] ==1)
			{
				$_REQUEST['job_salary_from'] = $_REQUEST['job_salary'];
			}else{
				$_REQUEST['job_salary_from'] = $_REQUEST['job_salary_text'];
			}
		}
		
		$plan_avail = $this->common_model->get_plan_detail($posted_by,'employer','job_post_limit');
		
		$job_life = $this->common_model->get_plan_detail($posted_by,'employer','job_life');
		$_REQUEST['job_life'] = $job_life;
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$current_data_str = strtotime($current_data);
		$date = strtotime("+$job_life day", $current_data_str);
		$job_expired_on = date('Y-m-d',$date);
		$_REQUEST['job_expired_on'] = $job_expired_on;
		if($plan_avail =='Yes')
		{
			if(isset($_REQUEST['job_highlighted']) && $_REQUEST['job_highlighted'] !='')
			{
				$job_highlighted = $_REQUEST['job_highlighted'];
			}
			if(isset($job_highlighted) && $job_highlighted  !='' && $job_highlighted =='Yes')
			{
				$highlight_job_limit = $this->common_model->get_plan_detail($posted_by,'employer','highlight_job_limit');
				if($highlight_job_limit =='Yes')
				{
					$_REQUEST['job_highlighted'] ='Yes';
				}
				else
				{
					$_REQUEST['job_highlighted'] ='No';
				}
			}

			$_REQUEST['posted_on'] = $this->common_model->getCurrentDate();
			$this->common_model->set_table_name('job_posting');
			$data = $this->common_model->save_update_data(0,1);	
			
			$data = json_decode($data);
			$this->session->flashdata('success_message');
			//print_r($data);
			//exit;
			if(isset($data->status) && $data->status !='' && $data->status =='success')
			{
				$this->session->set_userdata('success_message_js',$data->response);
				$insert_id = $this->db->insert_id();
				if(isset($_REQUEST['job_highlighted']) && $_REQUEST['job_highlighted'] =='Yes')
				{
					$this->common_model->update_plan_detail($posted_by,'employer','highlight_job_limit');	
				}
				$this->common_model->update_plan_detail($posted_by,'employer','job_post_limit');
				return $insert_id;
			}
			else
			{
				$this->session->set_flashdata('error_message_js',$data->response);
				return '';
			}
		}
		else
		{
			$this->session->set_flashdata('error_message_js','<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>Selected employer have no credit limit to post job</a></div>');
			return '';
		}
	}
	function update_job_detail()
	{
		if(isset($_REQUEST['location_hiring']) && $_REQUEST['location_hiring'] !='')
		{
			$val_req = $_REQUEST['location_hiring'];
			$tmp_val = implode(',',$val_req);
			$_REQUEST['location_hiring'] = $tmp_val;
		}
		if(isset($_REQUEST['job_education']) && $_REQUEST['job_education'] !='')
		{
			$val_req = $_REQUEST['job_education'];
			$tmp_val = implode(',',$val_req);
			$_REQUEST['job_education'] = $tmp_val;
		}
		if(isset($_REQUEST['anual_salary']) && $_REQUEST['anual_salary'] !='' && $_REQUEST['anual_salary'] ==2)
		{
			$_REQUEST['job_salary_from'] = $_REQUEST['job_salary_text'];
			//$_REQUEST['job_salary_to'] = '';
		}
		if(isset($_REQUEST['anual_salary']) && $_REQUEST['anual_salary'] !='' && $_REQUEST['anual_salary'] ==1)
		{
			$_REQUEST['job_salary_from'] = $_REQUEST['job_salary'];
			//$_REQUEST['job_salary_to'] = '';
		}

		$this->common_model->set_table_name('job_posting');
		$data = $this->common_model->save_update_data(0);
		return $data;
	}
}