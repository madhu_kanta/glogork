<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $success_array = array(
            'add' => 'Data inserted successfully.',
            'edit' => 'Data updated successfully.',
            'error' => 'Some error occured, please try again.',
            'delete' => 'Data deleted successfully.',
            'error_file' => 'Some error occured when file upload, please try again.'
        );
        $this->success_message = $success_array;
        $this->extra_css = array('vendor/checkbo/src/0.1.4/css/checkBo.min.css');
        $this->extra_js = array('vendor/jquery-validation/dist/jquery.validate.min.js', 'vendor/checkbo/src/0.1.4/js/checkBo.min.js');
        $this->mode = 'add';
        $this->js_validation_extra = '';
        $this->js_extra_code = '';
        // main tabel
        $this->table_name = '';
        $this->table_name_dot = '';
        $this->table_field = '';
        $this->status_field = 'status';
        // main tabel
        // for relation, Join tabel
        $this->join_tab_array = '';
        $this->join_tab_array_filed_disp = array();
        $this->filed_notdisp = array();

        // for relation, Join tabel
        $this->primary_key = 'id';
        $this->data_tabel_filedIgnore = array('id', 'is_deleted');

        $this->data_tabel_filed = array();
        $this->data_tabel_data = '';
        $this->data_tabel_filtered_count = 0;
        $this->data_tabel_all_count = 0;
        $this->search_filed = '';
        $this->limit_per_page = 1;
        $this->pagination_link = '';
        $this->page_number = 1;
        $this->admin_path = $this->data['admin_path'] = $this->getconfingValue('admin_path');
        $this->class_name = $this->router->fetch_class();
        $this->method_name = $this->router->fetch_method();
        $this->success_url = $this->method_name;
        $this->failure_url = $this->method_name;
        $this->default_image = 'assets/front_end/images/no-image-found.jpg';
        $this->action = $this->router->fetch_class() . '/' . $this->method_name . '/save-data';

        $this->add_fun = $this->method_name . '/add-data';
        $this->base_url_admin_cm_status = $this->base_url_admin_cm = base_url() . $this->admin_path . '/' . $this->class_name . '/' . $this->method_name . '/';
        $this->start = 0;
        $this->data_table_parameter = '';
        $this->status_mode = '';
        $this->status_arr = array('APPROVED' => 'APPROVED', 'UNAPPROVED' => 'UNAPPROVED');
        $this->status_column = 'status';
        $this->label_col = 2;
        $this->form_control_col = 7;
        $this->addPopup = 0;
        $this->button_array = array();
        $this->last_insert_id = '';
        $this->labelArr = array('country_id' => 'Country Name', 'state_id' => 'State Name', 'qualification_level_id' => 'Qualification Level', 'work_experience_from' => 'Work Experience', 'job_salary_from' => 'Job Salary', 'functional_area_search' => 'Functional Area', 'job_role_search' => 'Job Role', 'job_location_search' => 'Job Location', 'job_shift_search' => 'Job Shift', 'job_type_search' => 'Job Type', 'company_type_name' => 'Company Type', 'company_size_name' => 'Company Size', 'posted_by_employee' => 'Recruiter Name', 'total_requirenment' => 'Total Requirement'); // here we can set common label change for coloumn name
        $this->data['base_url'] = $this->base_url = base_url();
        $this->data['base_url_admin'] = $this->base_url_admin = $this->base_url . $this->admin_path . '/';
        $this->data['config_data'] = $this->get_site_config();
        $this->label_page = '';
        $this->permenent_delete = 'no';
        $this->is_delete_fild = 'is_deleted';
        $this->created_on_fild = 'created_on';
        $this->display_date_arr = array('created_on', 'register_date', 'last_login', 'birthdate', 'posted_on', 'job_expired_on');
        $this->field_duplicate = '';
        $this->other_config = '';
        $this->ele_array = '';
        $this->data_not_availabel = 'N/A';  //Record Not Found
        $this->display_selected_field = '';
        $this->edit_row_data = '';

        // for display in demo only
        $this->is_demo_mode = 0; // 1 = Demo Mode, 0 =  Live mode
        $this->email_disp = 'demo@gmail.com';
        $this->emial_disp = 'demo@gmail.com';
        $this->mobile_disp = '+9199999999999';
        $this->mobile_disp_edit = '99999999999';
        // for display in demo
        // for view detail pagge
        $this->detail_label_col = 4;
        $this->detail_val_col = 8;
        $this->detail_class_width = 'col-lg-4 col-md-6 col-sm-12 col-xs-12';
        // for view detail pagge
        // for ticket need to update when install on client website, must need to create client and update below perameter
        $this->client_id = $this->data['config_data']['client_id']; //'314';	// set clinet id
        $this->ticket_prefix = 'JP';
        $this->project_id = 1; // project id, dont need to change
        $this->web_appkey = $this->data['config_data']['web_appkey']; //'71feef8cabd038eec3256afc92f3d80a';	// set webapp key


        if (base_url() == 'http://192.168.1.111/job_portal/job_portal/' || base_url() == 'http://192.168.1.111/job_portal/original_script/') {
            $this->call_curl_url_ticket = 'http://192.168.1.111/client_management/common_request/update_from_client_ticket';
        } else {
            $this->call_curl_url_ticket = 'https://clients.narjisinfotech.com/common_request/update_from_client_ticket'; // don't change this url
        }


        // for ticket
    }

    function getCurrentDate($dformat = 'Y-m-d H:i:s') {
        //date_default_timezone_set('UTC'); // already set in config
        return date($dformat);
    }

    function display_experience($exp = '', $delimetor = '.') {
        $ret_exp = '';
        if ($exp != '') {
            //echo $exp;
            $exp_arr = explode($delimetor, $exp);
            //print_r($exp_arr);
            if (isset($exp_arr[0]) && $exp_arr[0] != '' && $exp_arr[0] != 0) {
                $ret_exp = $exp_arr[0] . ' Year ';
            }
            if (isset($exp_arr[1]) && $exp_arr[1] != '' && $exp_arr[1] != 0) {
                if ($ret_exp != '') {
                    $ret_exp .= $exp_arr[1] . ' Month';
                } else {
                    $ret_exp = $exp_arr[1] . ' Month';
                }
            }
            if ($exp_arr[0] == 0 && $exp_arr[1] == 0) {
                $ret_exp = ' Fresher';
            }
        } else {
            $ret_exp = $this->data_not_availabel;
        }
        return $ret_exp;
    }

    function display_salary($exp = '') {
        $ret_exp = '';
        if ($exp != '') {
            $exp_arr = explode('.', $exp);
            //print_r($exp_arr);
            if (isset($exp_arr[0]) && $exp_arr[0] != '' && $exp_arr[0] != 0) {
                $ret_exp = $exp_arr[0] . ' Lacs ';
            }
            if (isset($exp_arr[1]) && $exp_arr[1] != '' && $exp_arr[1] != 0) {
                if ($ret_exp != '') {
                    $ret_exp .= $exp_arr[1] . ' Thousand';
                } else {
                    $ret_exp = $exp_arr[1] . ' Thousand';
                }
            }
        }
        if ($ret_exp == '') {
            $ret_exp = $this->data_not_availabel;
        }
        return $ret_exp;
    }

    function callCURL($url = '') {
        $return_res = '';
        if ($url != '') {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
            curl_setopt($ch, CURLOPT_TIMEOUT, 4);
            $return_res = curl_exec($ch);
            curl_close($ch);
        }
        return $return_res;
    }

    function displayDate($date = '', $dformat = 'F j, Y h:i A') {// Y-m-d h:i:s
        if ($date == '0000-00-00' || $date == '0000-00-00 00:00:00') {
            return $this->data_not_availabel;
        } else if ($date != '' && $dformat != '') {
            $time_zone = '';
            //$this->session->set_userdata('time_zone','Asia/Kolkata');
            if (!$this->session->userdata('time_zone') || $this->session->userdata('time_zone') == "") {
                $ip = $this->input->ip_address();
                $final_api = 'http://ip-api.com/json/';
                if ($ip != '') {
                    $final_api_call = $final_api . $ip;
                    $response_data = $this->callCURL($final_api_call);
                    if ($response_data != '') {
                        $response_data = json_decode($response_data);
                        if (isset($response_data->status) && $response_data->status == 'fail') {
                            $response_data = $this->callCURL($final_api);
                            if ($response_data != '') {
                                $response_data = json_decode($response_data);
                            }
                        }
                    }
                    if (isset($response_data->timezone) && $response_data->timezone != '') {
                        $time_zone = $response_data->timezone;
                        $this->session->set_userdata('time_zone', $time_zone);
                    }
                    //print_r($response_data);
                }
            } else {
                $time_zone = $this->session->userdata('time_zone');
            }
            if ($time_zone == '') {
                $time_zone = 'Asia/Kolkata';
            }
            if (strlen($date) == 10) {
                $dformat = str_replace('h:i A', '', $dformat);
            }
            $strtime = strtotime($date);
            date_default_timezone_set($time_zone);
            $date_retuen = date($dformat, $strtime);
            date_default_timezone_set('UTC'); // already set in config
            return $date_retuen;
        } else {
            return $this->data_not_availabel;
        }
    }

    public function __load_header($label_page = '', $status = '') {
        $this->label_page = $label_page;
        $page_title = $label_page;
        if ($status != '') {
            $page_title = $page_title . ' - ' . $status;
        }
        $this->data['page_title'] = $page_title;
        $this->load->view('back_end/page_part/header', $this->data);
    }

    public function __load_footer($model_body = '') {
        $this->data['model_body'] = $model_body;
        $this->data['model_title'] = 'Add ' . $this->label_page;
        $this->load->view('back_end/page_part/footer', $this->data);
    }

    public function checkLogin() {
        if (!$this->session->userdata('jobportal_user_data') || $this->session->userdata('jobportal_user_data') == "" && count($this->session->userdata('jobportal_user_data')) == 0) {
            $base_url = base_url();
            $admin_path = $this->getconfingValue('admin_path');
            redirect($base_url . $admin_path . '/login');
        }
    }

    public function set_table_name($table_name = '') {
        if ($table_name != '') {
            $this->table_name = $this->table_name = $table_name;
            $this->table_name_dot = $table_name . '.';
            if ($this->display_selected_field == '') {
                $this->table_field = $this->db->list_fields($this->table_name);
            } else {
                $this->table_field = $this->display_selected_field;
            }
        }
    }

    function password_hash($password = '') {
        $this->load->library('phpass');
        $hashed_pass = $this->phpass->hash($password);
        return $hashed_pass;
    }

    function getjson_response() {
        $data_return = array();
        if ($this->session->flashdata('success_message')) {
            $data_return['response'] = '<div class="alert alert-success  alert-dismissable"><div class="fa fa-check">&nbsp;</div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' . $this->session->flashdata('success_message') . '</div>';
            $data_return['status'] = 'success';
            $this->session->unset_userdata('success_message');
        } else if ($this->session->flashdata('error_message')) {
            $data_return['status'] = 'error';
            $data_return['response'] = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' . $this->session->flashdata('error_message') . '</div>';
            $this->session->unset_userdata('error_message');
        }
        $data_return['tocken'] = $this->security->get_csrf_hash();
        return json_encode($data_return);
    }

    public function update_status_var($status) {
        $this->status_mode = $status;
        $this->base_url_admin_cm_status = $this->base_url_admin_cm . $status . '/';
    }

    public function get_site_config() {
        $this->db->limit(1);
        $query = $this->db->get_where('site_config', array('id' => 1));
        return $query->row_array();
    }

    public function get_session_data() {
        return $this->session->userdata('jobportal_user_data');
    }

    public function common_send_email($to_array, $subject, $message, $cc_array = '', $bcc_array = '', $attachment = '') {
        $config = array(
            'smtp_host' => 'mail.hmvjobs.in',
            'smtp_port' => 465,
            'smtp_user' => 'info@hmvjobs.in',
            'protocol' => 'mail',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");
        $config_arra = $this->get_site_config();
        $from_email = $config_arra['from_email'];

        $this->email->from($from_email);
        $this->email->to($to_array);

        if (isset($cc_array) && $cc_array != "") {
            $this->email->cc($cc_array);
        }
        if (isset($bcc_array) && $bcc_array != "") {
            $this->email->bcc($bcc_array);
        }
        if (isset($attachment) && $attachment != "") {
            $this->email->attach($attachment);
        }
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $msg = 'Email sent.';

        $base_url = base_url();
        if ($base_url != 'http://192.168.1.111/job_portal/original_script/v2.0/' && $base_url != 'http://192.168.1.111/original_script/') {
            if ($this->email->send()) {
                $msg = 'Email sent.';
            } else {
                $msg = $this->email->print_debugger();
                //show_error($this->email->print_debugger());
            }
        }
        return $msg;
    }

    public function common_sms_send($mobile, $sms) {
        if ($mobile != '' && $sms != '') {
            $config_arra = $this->get_site_config();
            $sms_api = $config_arra['sms_api'];
            $sms_api_status = $config_arra['sms_api_status'];
            if ($sms_api_status != '' && $sms_api_status == 'APPROVED' && $sms_api != '') {
                $mobile_arr = explode('-', $mobile);
                $mobile_ext = '';
                $mno = '';
                if (isset($mobile_arr[0]) && $mobile_arr[0] != '') {
                    $mobile_ext = $mobile_arr[0];
                }
                if (isset($mobile_arr[1]) && $mobile_arr[1] != '') {
                    $mno = $mobile_arr[1];
                }
                //$mobile_ext = substr($mobile,0,3);
                if ($mobile_ext == '+91') {
                    $sms = str_replace(" ", "%20", $sms);
                    //$mno = substr($mobile,3,15);
                    $sms_api = str_replace("##contacts##", $mno, $sms_api);
                    $sms_api = str_replace("##sms_text##", $sms, $sms_api);
                    $final_api = $sms_api;
                    $base_url = base_url();
                    if ($base_url != 'http://192.168.1.111/job_portal/job_portal/' && $base_url != 'http://192.168.1.111/original_script/') {
                        $ch = curl_init($final_api);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
                        $curl_scraped_page = curl_exec($ch);
                        curl_close($ch);
                    }
                } else {
                    // need to uncomment for send sms
                    /* require(APPPATH.'twilio_library/Services/Twilio.php');
                      $account_sid = "AC81b73d63963ea4405f760264d142e903"; // Your Twilio account sid
                      $auth_token = "7dc9ba1f3aa830cb9c330ae75bf16d6a"; // Your Twilio auth token
                      $client = new Services_Twilio($account_sid, $auth_token);
                      $message = $client->account->messages->sendMessage(
                      '+12267801184', // From a Twilio number in your account
                      $mobile,//'+19054524548', // Text any number
                      $sms
                      ); */
                    // need to uncomment for send sms
                }
            }
        }
    }

    function last_query() {
        return $this->db->last_query();
    }

    function get_count_data_manual($table, $where_arra = '', $flag_count_data = 0, $select_f = '', $order_by = '', $page = '', $limit = '', $is_delet_field = 1, $disp_query = "") {
        if ($table != '') {
            if (isset($is_delet_field) && $is_delet_field == 1 && $this->is_delete_fild != '') {
                $this->db->where($table . '.' . $this->is_delete_fild, 'No');
            }
            if ($where_arra != '' && is_array($where_arra) && count($where_arra) > 0) {
                foreach ($where_arra as $key => $val) {
                    if (is_numeric($key)) {
                        $this->db->where($val);
                    } else {
                        $this->db->where($key, $val);
                    }
                }
            } else if ($where_arra != '') {
                $this->db->where($where_arra);
            }
            if (isset($select_f) && $select_f != '') {
                $this->db->select($select_f);
            }

            if ($flag_count_data == 0) {
                //$search_data = $this->db->get_compiled_select($table);
                return $this->db->count_all_results($table);
            } else {
                if (isset($order_by) && $order_by != '') {
                    $this->db->order_by($order_by);
                }
                if ($flag_count_data == 1) {
                    $this->db->limit(1);
                } else {
                    /* if($limit =="")
                      {
                      $limit = $this->limit_per_page;
                      } */
                    ///if($page !='' && $limit !='')
                    if ($page != '' && is_numeric($page) && $limit != '' && is_numeric($limit)) {
                        $this->start = (($page - 1) * $limit);
                        //$this->db->where(" id > ".$this->start);
                        //$this->db->limit($limit);
                        $this->db->limit($limit, $this->start);
                    }
                    //$this->db->limit(1);
                }

                if ($disp_query == 1) {
                    echo $this->db->get_compiled_select($table);
                }

                $query = $this->db->get($table);
                if ($flag_count_data == 1) {
                    if ($query->num_rows() == 0) {
                        return '';
                    } else {
                        return $query->row_array();
                    }
                } else {
                    if ($query->num_rows() == 0) {
                        return '';
                    } else {
                        return $query->result_array();
                    }
                }
            }
        } else {
            return '';
        }
    }

    function update_insert_data_common($table = '', $data_array = '', $where_arra = '', $flag_update = 1, $limit = 1) {
        $return = false;
        if ($table != '' && $data_array != '') {
            if ($flag_update == 1) {
                if ($where_arra != '' && is_array($where_arra) && count($where_arra) > 0) {
                    foreach ($where_arra as $key => $val) {
                        if (is_numeric($key)) {
                            $this->db->where($val);
                        } else {
                            $this->db->where($key, $val);
                        }
                    }
                } else if ($where_arra != '') {
                    $this->db->where($where_arra);
                }

                if ($limit == 1) {
                    $this->db->limit(1);
                }
                $return = $this->db->update($table, $data_array);
            } else {
                if ($flag_update == 2) {
                    $return = $this->db->insert_batch($table, $data_array);
                } else {
                    $return = $this->db->insert($table, $data_array);
                }
            }
        }
        return $return;
    }

    function data_delete_common($table = '', $where_arra = '', $limit = 0, $permenent_delete = '') {
        $return = false;
        if ($permenent_delete != '') {
            $this->permenent_delete = $permenent_delete;
        }

        if ($table != '') {
            if ($where_arra != '' && is_array($where_arra) && count($where_arra) > 0) {
                foreach ($where_arra as $key => $val) {
                    if (is_numeric($key)) {
                        $this->db->where($val);
                    } else {
                        $this->db->where($key, $val);
                    }
                }
            } else if ($where_arra != '') {
                $this->db->where($where_arra);
            }
            if ($limit == 1) {
                $this->db->limit(1);
            }
            if ($this->permenent_delete == 'no') {
                $data_array = array($this->is_delete_fild => 'Yes');
                $return = $this->db->update($table, $data_array);
            } else {
                $return = $this->db->delete($table);
            }
            // is_deleted
        }
        return $return;
    }

    function getconfingValue($item_name = '') {
        $return = '';
        if ($item_name != '') {
            $return = $this->config->item($item_name);
        }
        return $return;
    }

    function generate_form_main($ele_array = '', $other_config = '') {
        $table_name = $this->table_name;
        $ele_array_key = array_keys($ele_array);
        if (isset($other_config['mode']) && $other_config['mode'] == 'edit' && $table_name != '') {
            if (!isset($this->edit_row_data) || $this->edit_row_data == '') {
                $where_arr = '';
                $primary_key = $this->primary_key;
                if (isset($primary_key) && $primary_key != '' && isset($other_config['id']) && $other_config['id'] != '') {
                    $where_arr = array($primary_key => $other_config['id']);
                }
                $select_field = '';
                if (isset($ele_array_key) && $ele_array_key != '' && is_array($ele_array_key) && count($ele_array_key) > 0) {
                    $table_field = $this->table_field;
                    $ele_array_key = array_intersect($ele_array_key, $table_field);
                    $select_field = implode(',', $ele_array_key);
                }
                $row_data = $this->get_count_data_manual($table_name, $where_arr, 1, $select_field);
                $this->edit_row_data = $row_data;
            } else {
                $row_data = $this->edit_row_data;
            }
            if (!isset($row_data) || $row_data == '' || count($row_data) == 0) {
                redirect($this->base_url_admin_cm);
            }
        }
        if ($this->session->flashdata('old_post_data')) {
            $row_data = $this->session->flashdata('old_post_data');
            $this->edit_row_data = $row_data;
        }
        $element_array = array();
        foreach ($ele_array as $key => $val) {
            $temp_array = $val;

            /* if(isset($other_config['mode']) && $other_config['mode'] =='edit' && isset($row_data[$key]) && $row_data[$key] !='')
              { */
            if (isset($other_config['mode']) && isset($row_data[$key]) && $row_data[$key] != '') {
                $temp_array['value'] = $row_data[$key];
            }
            if (isset($val['type']) && $val['type'] != '') {
                $temp_array['type'] = $val['type'];
            } else {
                if (!is_array($temp_array) && $temp_array == '') {
                    $temp_array = array('type' => 'textbox');
                } else {
                    $temp_array['type'] = 'textbox';
                }
            }
            $element_array[$key] = $temp_array;
        }

        return $this->generate_form($element_array, $other_config);
    }

    function is_required($element_array_val) {
        $is_required = "";
        if (isset($element_array_val['is_required']) && $element_array_val['is_required'] != '' && $element_array_val['is_required'] == 'required') {
            $is_required = " required ";
        }
        return $is_required;
    }

    function get_label($element_array_val = '', $key = '') {
        $labelArr = $this->common_model->labelArr;
        if (isset($labelArr[$key]) && $labelArr[$key] != '') {
            $label = $labelArr[$key];
        } else if (isset($element_array_val['label']) && $element_array_val['label'] != '') {
            $label = $element_array_val['label'];
        } else {
            $label = str_replace('_', ' ', $key);
            $label = ucwords($label);
        }
        return $label;
    }

    public function set_session_data_comm($session_name = '', $data = '') {
        if ($session_name != '') {
            if (isset($this->session_prifix) && $this->session_prifix != '') {
                $session_name = $this->session_prifix . $session_name;
            }
            $this->session->set_userdata($session_name, $data);
        }
    }

    function get_value($element_array_val, $key = 'value', $defult = '') {
        $value_curr = $defult;
        if (isset($element_array_val[$key]) && $element_array_val[$key] != '') {
            $value_curr = $element_array_val[$key];
        }
        return $value_curr;
    }

    function getRelationDropdown($element_array_val) {
        $return_arr = array(); // updated by mm
        $value_curr = $this->get_value($element_array_val, 'value', '');
        $relation_arr = $this->get_value($element_array_val, 'relation', '');
        $not_load_add = $this->get_value($relation_arr, 'not_load_add', 'no'); // updated by mm
        $is_multiple = $this->get_value($element_array_val, 'is_multiple');
        if (isset($relation_arr) && $relation_arr != '' && count($relation_arr) > 0) {
            // updated by mm
            if (isset($not_load_add) && $not_load_add == 'yes' && $this->mode == 'add') {
                return $return_arr;
            }
            // updated by mm
            if (isset($relation_arr['rel_table']) && $relation_arr['rel_table'] != '' && isset($relation_arr['key_val']) && $relation_arr['key_val'] != '' && isset($relation_arr['key_disp']) && $relation_arr['key_disp'] != '') {
                $select_field = $relation_arr['key_disp'] . ', ' . $relation_arr['key_val'];

                $where_close = array();
                if ($value_curr != '') {
                    $where_close[] = $relation_arr['key_val'] . " = '" . $value_curr . "' ";
                }
                $status_filed = 'status';
                $status_val = 'APPROVED';
                if (isset($relation_arr['status_filed']) && $relation_arr['status_filed'] != '') {
                    $status_filed = $relation_arr['status_filed'];
                }
                if (isset($relation_arr['status_val']) && $relation_arr['status_val'] != '') {
                    $status_val = $relation_arr['status_val'];
                }
                if ($status_filed != '' && $status_val != '') {
                    $where_close[] = $status_filed . " = '" . $status_val . "' ";
                }
                if (isset($where_close) && $where_close != '' && is_array($where_close) && count($where_close) > 0) {
                    $where_close_str = implode(" OR ", $where_close);
                    $this->db->where(" ( $where_close_str ) ");
                }
                // updated by mm
                if (isset($relation_arr['cus_rel_col_val']) && $relation_arr['cus_rel_col_val'] != '') {
                    if (isset($this->edit_row_data[$relation_arr['cus_rel_col_val']]) && $this->edit_row_data[$relation_arr['cus_rel_col_val']] != '') {
                        $relation_arr['rel_col_val'] = $this->edit_row_data[$relation_arr['cus_rel_col_val']];
                    }
                }
                // updated by mm

                if (isset($relation_arr['rel_col_name']) && $relation_arr['rel_col_name'] != '' && isset($relation_arr['rel_col_val']) && $relation_arr['rel_col_val'] != '') {
                    if ($is_multiple != '' && $is_multiple == 'yes') {
                        $rel_val_arr_in = explode(',', $relation_arr['rel_col_val']);
                        $this->db->where_in($relation_arr['rel_col_name'], $rel_val_arr_in);
                    } else {
                        $this->db->where($relation_arr['rel_col_name'], $relation_arr['rel_col_val']);
                    }
                }

                //$row_data = $this->get_count_data_manual($relation_arr['rel_table'],"",2,$select_field,$relation_arr['key_disp'].' ASC ',0);

                if (isset($relation_arr['rel_table']) && $relation_arr['rel_table'] == 'salary_range') {
                    $row_data = $this->get_count_data_manual($relation_arr['rel_table'], "", 2, $select_field, $relation_arr['key_val'] . ' ASC ', 0);
                } else {
                    $row_data = $this->get_count_data_manual($relation_arr['rel_table'], "", 2, $select_field, $relation_arr['key_disp'] . ' ASC ', 0);
                }
                //echo $this->db->last_query();

                $return_arr = array();
                if (isset($row_data) && $row_data != '' && is_array($row_data) && count($row_data) > 0) {
                    foreach ($row_data as $row_data_val) {
                        $return_arr[$row_data_val[$relation_arr['key_val']]] = $row_data_val[$relation_arr['key_disp']];
                    }
                }
                //print_r($return_arr);
            }
        }
        // $this->get_value($element_array_val,'value','');
        return $return_arr;
    }

    function generate_dropdown($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = $this->get_value($element_array_val, 'value', '');
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $class = $this->get_value($element_array_val, 'class');
            $is_multiple = $this->get_value($element_array_val, 'is_multiple'); //

            $is_multi = '';
            $is_multi_par = '';
            $current_selected_arra = array();
            if ($is_multiple != '' && $is_multiple == 'yes') {
                $is_multi = 'multiple';
                $is_multi_par = '[]';
                if ($value_curr != '') {
                    $current_selected_arra = explode(',', $value_curr);
                }
            } else if ($value_curr != '') {
                $current_selected_arra[] = $value_curr;
            }

            $form_group_class = $this->get_value($element_array_val, 'form_group_class');
            $onChange = $this->get_value($element_array_val, 'onchange', '');
            $display_placeholder = $this->get_value($element_array_val, 'display_placeholder', 'Yes');
            if ($onChange != '') {
                $onChange = 'onChange="' . $onChange . '"';
            }

            $value_arr = $this->get_value($element_array_val, 'value_arr', '');
            if (!isset($value_arr) || $value_arr == '' || count($value_arr) == 0) {
                $value_arr = $this->getRelationDropdown($element_array_val);
            }
            $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '">
				  	<select ' . $is_multi . ' ' . $onChange . ' ' . $is_required . ' name="' . $key . $is_multi_par . '" id="' . $key . '" class="form-control ' . $class . ' ">';
            if ($display_placeholder == 'Yes') {
                $return_content .= '<option selected value="" >Select ' . $label . '</option>';
            }

            if (isset($value_arr) && $value_arr != '' && is_array($value_arr) && count($value_arr) > 0) {
                foreach ($value_arr as $key_r => $value_arr_val) {
                    $selected_drop = '';
                    if (in_array($key_r, $current_selected_arra)) {
                        $selected_drop = 'selected';
                    }
                    $return_content .= '<option ' . $selected_drop . ' value="' . $key_r . '">' . $value_arr_val . '</option>';
                }
            }
            $return_content .= '
					</select>
				  </div>
				</div>';
        }
        return $return_content;
    }

    function generate_radio($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = $this->get_value($element_array_val, 'value', 'APPROVED');
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $class = $this->get_value($element_array_val, 'class');
            $value_arr = $this->get_value($element_array_val, 'value_arr', $this->status_arr);
            $form_group_class = $this->get_value($element_array_val, 'form_group_class');
            $onclick = $this->get_value($element_array_val, 'onclick');
            if ($onclick != '') {
                $onclick = ' onclick= "' . $onclick . '" ';
            }
            //$this->status_arr = $value_arr;
            $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '">
				  <div class=radio>';
            //print_r($value_arr);
            if (isset($value_arr) && $value_arr != '' && is_array($value_arr) && count($value_arr) > 0) {
                foreach ($value_arr as $key_r => $value_arr_val) {
                    $selected_radio = '';
                    if ($value_curr == $key_r) {
                        $selected_radio = 'checked';
                    }
                    $return_content .= '<label><input ' . $onclick . ' ' . $selected_radio . ' class="' . $class . '" type="radio" name="' . $key . '" id="' . $key_r . '" value="' . $key_r . '">' . $value_arr_val . '</label>&nbsp;&nbsp;';
                }
            }
            $return_content .= '
					</div>
				  </div>
				</div>';
        }
        return $return_content;
    }

    function generate_password($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = '';
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $other = $this->get_value($element_array_val, 'other');
            $class = $this->get_value($element_array_val, 'class');
            $form_group_class = $this->get_value($element_array_val, 'form_group_class');

            if ($this->mode == 'add') {
                $place_holder_pass = 'Password';
            } else {
                $place_holder_pass = 'Please leave password blank for not update';
            }
            $return_content .= '
			<div class="form-group ' . $form_group_class . '">
			  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
			  <div class="col-sm-9 col-lg-' . $this->form_control_col . '" >
				<input ' . $other . ' minlength="6" type="password" ' . $is_required . ' name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " placeholder="' . $place_holder_pass . '" value ="" />
			  </div>
			</div>';
        }
        return $return_content;
    }

    function generate_textbox($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = $this->get_value($element_array_val);
            if (isset($element_array_val['value_imp']) && $element_array_val['value_imp'] != '') {
                $value_curr = $element_array_val['value_imp'];
            }
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $input_type = $this->get_value($element_array_val, 'input_type', 'text');
            $other = $this->get_value($element_array_val, 'other');
            $class = $this->get_value($element_array_val, 'class');
            $form_group_class = $this->get_value($element_array_val, 'form_group_class');
            $placeholder = $this->get_value($element_array_val, 'placeholder', $label);
            if ($input_type == 'date') {
                $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '" >
				  	<div class="input-prepend input-group input-group-lg"> <span class="add-on input-group-addon  common_height_padd"><i class="fa fa-calendar"></i></span>
						<input ' . $other . ' type="text" ' . $is_required . ' name="' . $key . '" id="' . $key . '" class="datepicker form-control ' . $class . '  common_height_padd " placeholder="' . $label . '" value ="' . htmlentities(stripcslashes($value_curr)) . '" data-provide="datepicker" />
					</div>

				  </div>
				</div>';
            }
            /* new edit for text box accept alpha charactes only */ else if ($input_type == 'alpha') {
                $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '" >';

                if (isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1 && in_array($key, array('email', 'phone', 'mobile', 'landline')) && $value_curr != '') {
                    $return_content .= '<span><strong>Disable in demo</strong></span>';
                } else {
                    $return_content .= '<input ' . $other . ' type="text" ' . $is_required . ' name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " placeholder="' . $placeholder . '" value ="' . htmlentities(stripcslashes($value_curr)) . '" onkeypress="return onKeyValidate(event,alpha);" />';
                }
                $return_content .= ' </div>
				</div>';
            }/* new edit for text box accept alpha charactes only */ else if ($input_type == 'email') {
                $check_duplicate = $this->get_value($element_array_val, 'check_duplicate', 'No');
                $onchange_textbox = '';
                if ($check_duplicate == 'Yes') {
                    $table_name = $this->table_name;
                    $onchange_textbox = "check_duplicate('$key','$table_name')";
                    $onchange_textbox = 'onBlur="' . $onchange_textbox . '"';
                }


                $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '" >';

                if (isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1 && in_array($key, array('email', 'phone', 'mobile', 'landline')) && $value_curr != '') {
                    $return_content .= '<span><strong>Disable in demo</strong></span>';
                } else {
                    $return_content .= '<input ' . $other . ' type="' . $input_type . '" ' . $is_required . ' name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " ' . $onchange_textbox . ' placeholder="' . $placeholder . '" value ="' . htmlentities(stripcslashes($value_curr)) . '" />';
                }
                $return_content .= '</div>
				</div>';
            } else {
                $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . '" >';
                if (isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1 && in_array($key, array('email', 'phone', 'mobile', 'landline')) && $value_curr != '') {
                    $return_content .= '<span><strong>Disable in demo</strong></span>';
                } else {
                    $return_content .= '<input ' . $other . ' type="' . $input_type . '" ' . $is_required . ' name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " placeholder="' . $placeholder . '" value ="' . htmlentities(stripcslashes($value_curr)) . '" />';
                }
                $return_content .= '</div>
				</div>';
            }
        }
        return $return_content;
    }

    function generate_textarea($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = $this->get_value($element_array_val);
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $class = $this->get_value($element_array_val, 'class');
            $form_group_class = $this->get_value($element_array_val, 'form_group_class');
            $placeholder = $this->get_value($element_array_val, 'placeholder', $label);
            $return_content .= '
				<div class="form-group ' . $form_group_class . '">
				  <label class="col-sm-' . $this->label_col . ' col-lg-' . $this->label_col . ' control-label">' . $label . '</label>
				  <div class="col-sm-9 col-lg-' . $this->form_control_col . ' ' . $key . '_edit" >
				  	<textarea ' . $is_required . ' rows="4" name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " placeholder="' . $placeholder . '">' . $value_curr . '</textarea>
				  </div>
				</div>';
        }
        return $return_content;
    }

    function check_is_img($file_url = '') {
        $return = 1;
        $size = getimagesize($file_url);
        if ($size === false) {
            $return = 0;
        }
        //if($file_url !='')
        //{
        //	$ext = end(explode(".", $file_url));
        //}
        return $return;
    }

    function generate_file_upload($element_array_val, $key) {
        $return_content = '';
        if (count($element_array_val) > 0 && $key != '') {
            $value_curr = $this->get_value($element_array_val);
            $label = $this->get_label($element_array_val, $key);
            $is_required = $this->is_required($element_array_val);
            $extension = $this->get_value($element_array_val, 'extension', 'jpg|png|jpeg|gif');
            $path_value = $this->get_value($element_array_val, 'path_value', '');
            $class = $this->get_value($element_array_val, 'class');
            $display_img = $this->get_value($element_array_val, 'display_img', 'Yes');
            $form_group_class = $this->get_value($element_array_val, 'form_group_class');
            if ($this->mode == 'edit') {
                $is_required = '';
            }
            $img_display = '';

            ///$is_img = $this->check_is_img($path_value.$value_curr);

            if ($path_value != '' && $value_curr != '' && file_exists($path_value . $value_curr) && $display_img == 'Yes') {
                $img_display = '<img style="max-height:100px" class="img-responsive" src="' . $this->base_url . $path_value . $value_curr . '" />';
            } else if ($path_value != '' && $value_curr != '' && file_exists($path_value . $value_curr)) {
                $img_display = '<a target="_blank" href="' . $this->base_url . $path_value . $value_curr . '" >View File</a>';
            }
            $return_content .= '
				<div class="form-group ' . $form_group_class . '">
                  <label class="col-sm-' . $this->label_col . ' control-label">' . $label . '</label>
                  <div class=col-sm-4>
                    <input filesize="10" extension="' . $extension . '" ' . $is_required . ' type="file" name="' . $key . '" id="' . $key . '" class="form-control ' . $class . ' " />
					<input type="hidden" name="' . $key . '_val" id="' . $key . '_val" value="' . $value_curr . '" />
					<input type="hidden" name="' . $key . '_path" id="' . $key . '_path" value="' . $path_value . '" />
					<input type="hidden" name="' . $key . '_ext" id="' . $key . '_ext" value="' . $extension . '" />
                    <p class=help-block>Allowed File type ' . str_replace('|', ' | ', $extension) . ' <br/>Max File size is 2MB</p>
                  </div>
				  <div class="col-sm-4">
				  	' . $img_display . '
				  </div>
                </div>';
        }
        return $return_content;
    }

    function generate_form_element($element_array = '') {
        $return_content = '';
        if (isset($element_array) && $element_array != '' && is_array($element_array) && count($element_array) > 0) {
            foreach ($element_array as $key => $element_array_val) {
                if (isset($element_array_val['type']) && $element_array_val['type'] == 'textbox') {
                    $return_content .= $this->generate_textbox($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'password') {
                    $return_content .= $this->generate_password($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'textarea') {
                    $return_content .= $this->generate_textarea($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'file') {
                    $return_content .= $this->generate_file_upload($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'radio') {
                    $return_content .= $this->generate_radio($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'dropdown') {
                    $return_content .= $this->generate_dropdown($element_array_val, $key);
                } else if (isset($element_array_val['type']) && $element_array_val['type'] == 'manual') {
                    $return_content .= $element_array_val['code'];
                }
            }
        }
        return $return_content;
    }

    function generate_form($element_array = '', $other_config = '') {
        $return_content = '';
        $action = '';
        $onsubmit = '';
        $enctype = '';
        if (isset($other_config) && !is_array($other_config) || $other_config == '') {
            $other_config = array();
        }
        if (isset($other_config['mode']) && $other_config['mode'] != '') {
            $this->mode = $other_config['mode'];
        } else {
            $other_config['mode'] = 'add';
            $this->mode = 'add';
        }
        if (!isset($other_config['id'])) {
            $other_config['id'] = '';
        }

        if (isset($other_config['success_url']) && $other_config['success_url'] != '') {
            $this->success_url = $other_config['success_url'];
        }
        if (isset($other_config['failure_url']) && $other_config['failure_url'] != '') {
            $this->failure_url = $other_config['failure_url'];
        }
        if (isset($other_config['action']) && $other_config['action'] != '') {
            $action = 'action="' . $this->base_url_admin . $other_config['action'] . '"';
        } else if (isset($this->action) && $this->action != '') {
            $action = 'action="' . $this->base_url_admin . $this->action . '"';
        }
        if (isset($other_config['onsubmit']) && $other_config['onsubmit'] != '') {
            $onsubmit = ' onSubmit="' . $other_config['onsubmit'] . '"';
        }
        if (isset($other_config['enctype']) && $other_config['enctype'] != '') {
            $enctype = $this->get_value($other_config, 'enctype');
        }
        if (isset($other_config['form_id']) && $other_config['form_id'] != '') {
            $form_id = $this->get_value($other_config, 'form_id');
        } else {
            $form_id = 'common_insert_update';
        }
        $temp_back = '';
        if (isset($other_config['onback_click']) && $other_config['onback_click'] != '') {
            $onback_click = $this->get_value($other_config, 'onback_click');
        } else {
            $class_name = $this->class_name;
            $method_name = $this->method_name;
            $temp_back = $this->base_url_admin_cm_status;
            $onback_click = 'back_list()';
        }
        $back_hidden = $this->get_value($other_config, 'back_hidden', 'no');
        if (isset($element_array) && $element_array != '' && is_array($element_array) && count($element_array) > 0) {
            $message_div = '<div id="reponse_message">';
            if ($this->session->flashdata('error_message')) {
                $message_div .= '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' . $this->session->flashdata('error_message') . '</div>';
            }
            if ($this->session->flashdata('success_message')) {
                $message_div .= '<div class="alert alert-success  alert-dismissable"><div class="fa fa-check">&nbsp;</div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' . $this->session->flashdata('success_message') . '</div>';
            }
            if ($this->session->flashdata('error_message_js')) {
                $message_div .= $this->session->flashdata('error_message_js');
            }
            $message_div .= '</div>';
            $return_content .= '
					  <div class="panel mb25">
						<div class=panel-body>
						  <div class="row no-margin">
							<div class=col-lg-12>
								<form method="post" id="' . $form_id . '" name="' . $form_id . '" class="form-horizontal bordered-group" role=form ' . $action . $onsubmit . ' ' . $enctype . ' >';
            $return_content .= $message_div;
            $return_content .= $this->generate_form_element($element_array);
            $return_content .= '
							<div class=form-group>
							  <label class="col-sm-' . $this->label_col . '"></label>
							  <div class="col-sm-9">
							  	<button type="submit" class="btn btn-primary mr10">Submit</button>
							  ';
            if ($back_hidden == 'no') {
                if ($this->addPopup == 0) {
                    $return_content .= '
											<a href="' . $temp_back . '" type="button" onClick="' . $onback_click . '" class="btn btn-default">Back</a>';
                } else {
                    $return_content .= '
											<button type="button" onClick="close_popup()" class="btn btn-default" data-dismiss="modal">Close</button>';
                }
            }
            $return_content .= '</div>
							</div>
							<input type="hidden" name="' . $this->security->get_csrf_token_name() . '" value="' . $this->security->get_csrf_hash() . '" id="hash_tocken_id" class="hash_tocken_id" />
							<input type="hidden" name="mode" value="' . $other_config['mode'] . '" id="mode" />
							<input type="hidden" name="id" value="' . $other_config['id'] . '" id="id" />
							<input type="hidden" name="success_url" value="' . $this->success_url . '" id="success_url" />
							<input type="hidden" name="failure_url" value="' . $this->failure_url . '/' . $other_config['mode'] . '-data/' . $other_config['id'] . '" id="failure_url" />
						</form>
						</div>
					  </div>
					</div>
				  </div>';
        }
        return $return_content;
    }

    public function generate_url_comon($generate_url = '') {
        $return_url = '';
        if ($generate_url != '') {
            $generate_url_arr = explode('-|-', $generate_url);
            if (isset($generate_url_arr) && $generate_url_arr != '' && is_array($generate_url_arr) && count($generate_url_arr) > 0) {
                $from_gen = '';
                $to_gen = '';
                if (isset($generate_url_arr[0]) && $generate_url_arr[0] != '') {
                    $from_gen = $generate_url_arr[0];
                }
                if (isset($generate_url_arr[1]) && $generate_url_arr[1] != '') {
                    $to_gen = $generate_url_arr[1];
                }
                if ($from_gen != '' && $to_gen != '') {
                    $from_gen_val = '';
                    if ($this->input->post($from_gen)) {
                        $from_gen_val = $this->input->post($from_gen);
                    }
                    if ($from_gen_val != '') {
                        $_REQUEST[$to_gen] = $url_genrate = strtolower(url_title($from_gen_val));
                    }
                }
            }
        }
        $return_url = strtolower($return_url);
        return $return_url;
    }

    public function save_update_data($retuen_json = 1, $is_retur_stop = '', $flag_uuuu = '') {
        /* print_r($_REQUEST);
          print_r($_FILES);
          exit; */
        $table_name = $this->table_name;
        $table_field = $this->table_field;
        $primary_key = $this->primary_key;
        if ($this->input->post('success_url')) {
            $this->success_url = $this->input->post('success_url');
        }
        if ($this->input->post('failure_url')) {
            $this->failure_url = $this->input->post('failure_url');
        }
        if ($table_name != '' && $table_field != '' && is_array($table_field) && count($table_field) > 0) {
            $where_arra = '';
            $where_arra_dup = '';
            $flag_update = 0;
            $limit = '';
            $data_array = array();
            $data_file_old_array = array();
            $data_file_new_array = array();
            $mode = 'add';
            $id = '';
            $is_duplicate = 0;
            //error_reporting(E_ALL);
            //echo '<pre>';
            //print_r($_FILES);
            //echo '</pre>';
            //exit;
            // for check edit mode
            if (isset($_REQUEST['mode']) && $_REQUEST['mode'] != '' && $_REQUEST['mode'] == 'edit') {
                $mode = 'edit';
                if ($this->input->post('id')) {
                    $id = trim($this->input->post('id'));
                }
                if ($id != '') {
                    $where_arra = array($primary_key => $id);
                    $where_arra_dup = $primary_key . " != '" . $id . "'";
                    // $this->db->where($primary_key .' != '.$id); // for duplicate check with id
                    $limit = 1;
                }
                $flag_update = 1;
            }
            if ($this->field_duplicate != '') {
                $filed_dup_check = $this->field_duplicate;
                if (isset($filed_dup_check) && $filed_dup_check != '' && is_array($filed_dup_check) && count($filed_dup_check) > 0) {
                    foreach ($filed_dup_check as $filed_dup_check_val) {
                        if ($this->input->post($filed_dup_check_val)) {
                            $filed_dup_check_val_fill = trim($this->input->post($filed_dup_check_val));
                            if ($filed_dup_check_val_fill != '' && $filed_dup_check_val != '') {
                                $this->db->where($filed_dup_check_val, $filed_dup_check_val_fill);
                            }
                        }
                    }
                    $cound_duplicate = $this->get_count_data_manual($this->table_name, $where_arra_dup, 0, $this->table_name_dot . $this->primary_key);
                    //echo $this->db->last_query();
                    if ($cound_duplicate > 0) {
                        $is_duplicate = 1;
                    }
                }
            }
            if ($is_duplicate == 0) {
                if ($mode == 'add') {
                    $genrate_url = '';
                    if ($this->input->post('genrate_url')) {
                        $genrate_url = $this->input->post('genrate_url');
                        if ($genrate_url != '') {
                            $this->generate_url_comon($genrate_url);
                        }
                    }
                }
                // for check edit mode
                // for check file upload
                if (isset($_FILES) && $_FILES != '' && is_array($_FILES) && count($_FILES) > 0) {
                    foreach ($_FILES as $key => $val) {
                        var_dump($key . '-' . $val . '-' . $table_field);
                        if (in_array($key, $table_field) && isset($val['name']) && $val['name'] != '' && isset($val['size']) && $val['size'] > 0) {
                            $path_upload = '';
                            if ($this->input->post($key . '_path')) {
                                $path_upload = trim($this->input->post($key . '_path'));
                            }
                            $old_upload = '';
                            if ($this->input->post($key . '_val')) {
                                $old_upload = trim($this->input->post($key . '_val'));
                            }
                            $allowed_types = 'gif|jpg|png|jpeg';
                            if (isset($_REQUEST[$key . '_ext']) && $_REQUEST[$key . '_ext'] != '') {
                                $allowed_types = trim($_REQUEST[$key . '_ext']);
                            }
                            $temp_data_array = array('file_name' => $key, 'upload_path' => $path_upload, 'allowed_types' => $allowed_types);
                            $upload_data = $this->upload_file($temp_data_array);
                            if (isset($upload_data) && $upload_data != '' && is_array($upload_data) && count($upload_data) > 0 && isset($upload_data['status']) && $upload_data['status'] == 'success') {
                                $file_data = $upload_data['file_data'];
                                $data_array[$key] = $file_data['file_name'];

                                $data_file_old_array[] = $path_upload . $old_upload;
                                $data_file_new_array[] = $path_upload . $file_data['file_name'];
                            } else {
                                $this->delete_file($data_file_new_array);
                                if (isset($upload_data['error_message'])) {
                                    $this->session->set_flashdata('error_message', $upload_data['error_message']); /* $this->success_message['error_file'] */
                                }
                                if (isset($is_retur_stop) && $is_retur_stop != '' && $is_retur_stop == 1) {
                                    $data = $this->common_model->getjson_response();
                                    return $data;
                                }
                                $is_ajax = 0;
                                if ($this->input->post('is_ajax')) {
                                    $is_ajax = $this->input->post('is_ajax');
                                }
                                if ($is_ajax == 0) {
                                    if (isset($this->failure_url) && $this->failure_url != '') {
                                        redirect($this->base_url_admin . $this->class_name . '/' . $this->failure_url);
                                    }
                                } else {
                                    $data['data'] = $this->common_model->getjson_response();
                                    if ($retuen_json == 1) {
                                        $this->load->view('common_file_echo', $data);
                                    } else {
                                        return $data;
                                    }
                                }
                                return;
                            }
                        }
                    }
                }
                // for check file upload
                // for request field and add
                if (isset($_REQUEST) && $_REQUEST != '' && is_array($_REQUEST) && count($_REQUEST) > 0) {
                    foreach ($_REQUEST as $key => $val) {
                        if (in_array($key, $table_field) && $key != $primary_key) {
                            if ($key != 'google_analytics_code') {
                                $val = trim($this->security->xss_clean($val));
                            }
                            $data_array[$key] = $val;
                        }
                    }
                }
                if ($mode == 'add') {
                    if (isset($this->created_on_fild) && $this->created_on_fild != '' && in_array($this->created_on_fild, $table_field)) {
                        $data_array[$this->created_on_fild] = $this->getCurrentDate();
                    }
                }
                // for request field and add
                if ($data_array != '' && is_array($data_array) && count($data_array) > 0) {
                    $response = $this->update_insert_data_common($table_name, $data_array, $where_arra, $flag_update, $limit);
                    if ($mode == 'add') {
                        $this->last_insert_id = $this->db->insert_id();

                        if ($table_name == 'ticket_table') {
                            $this->update_ticket_data_devloper($this->last_insert_id, $data_array);
                        }
                    }
                    if (isset($response) && $response) {
                        $success_message = $this->success_message[$mode];
                        $this->delete_file($data_file_old_array);
                        $this->session->set_flashdata('success_message', $success_message);
                    } else {
                        $this->delete_file($data_file_new_array);
                        $this->session->set_flashdata('error_message', $this->success_message['error']);
                    }
                }
            } else {
                $this->session->set_flashdata('old_post_data', $_REQUEST);
                $this->session->set_flashdata('error_message', "Duplicate data found.");
            }
        }
        if (isset($flag_uuuu) && $flag_uuuu == 'Yes') {
            $this->common_model->update_site_setting_data($data_array);
        }
        if (isset($is_retur_stop) && $is_retur_stop != '' && $is_retur_stop == 1) {
            $data = $this->common_model->getjson_response();
            return $data;
        }

        $is_ajax = 0;
        if ($this->input->post('is_ajax')) {
            $is_ajax = $this->input->post('is_ajax');
        }
        if ($is_ajax == 0) {
            if (isset($response) && $response) {
                if (isset($this->success_url) && $this->success_url != '') {
                    redirect($this->base_url_admin . $this->class_name . '/' . $this->success_url);
                }
            } else {
                if (isset($this->failure_url) && $this->failure_url != '') {
                    redirect($this->base_url_admin . $this->class_name . '/' . $this->failure_url);
                }
            }
        } else {
            $data['data'] = $this->common_model->getjson_response();
            if ($retuen_json == 1) {
                $this->load->view('common_file_echo', $data);
            } else {
                return $data;
            }
        }
    }

    function update_ticket_data_devloper($last_insert_id = '', $data = array()) {
        if ($last_insert_id != '') {
            $ticket_number = $this->ticket_prefix . $this->client_id . $last_insert_id;
            $data_array = array('ticket_number' => $ticket_number);
            $response = $this->update_insert_data_common('ticket_table', $data_array, array('id' => $last_insert_id), 1, 1);
            if ($response && $data != '' && is_array($data) && count($data) > 0) {
                $data['ticket_number'] = $ticket_number;
                $data['client_id'] = $this->client_id;
                $data['project'] = $this->project_id;
                $data['web_key'] = $this->web_appkey;
                $data['create_ticket'] = 'Yes';
                $this->curl_call_ticket($data);
            }
        }
    }

    public function upload_file($upload_data = '') {
        $return_message = '';
        if (isset($upload_data) && $upload_data != '' && is_array($upload_data) && count($upload_data) > 0 && isset($upload_data['upload_path']) && $upload_data['upload_path'] != '' && isset($upload_data['file_name']) && $upload_data['file_name'] != '') {

            $config = array();
            $config['upload_path'] = $upload_data['upload_path'];
            if (isset($upload_data['allowed_types']) && $upload_data['allowed_types'] != '') {
                $config['allowed_types'] = $upload_data['allowed_types'];
            }

            // if u want to change upload image size 2 MB ,4 MB change here//
            $config['max_size'] = 1024 * 2;


            if (isset($upload_data['max_size']) && $upload_data['max_size'] != '') {
                //$config['max_size']= $upload_data['max_size'];
            }
            if (isset($upload_data['encrypt_name']) && $upload_data['encrypt_name'] != '') {
                $config['encrypt_name'] = $upload_data['encrypt_name'];
            } else {
                $config['encrypt_name'] = TRUE;
            }

            $this->load->library('upload'); //  , $config
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($upload_data['file_name'])) {
                $return_message = array('status' => 'error', 'error_message' => $this->upload->display_errors());
            } else {
                $return_message = array('status' => 'success', 'file_data' => $this->upload->data());
            }
        }
        return $return_message;
    }

    public function update_site_setting_data($data_array = '') {
        if ($data_array != '') {
            up_se_da_nd_te($data_array, $this->common_model);
        }
    }

    public function delete_file($file_name = '') {
        if (isset($file_name) && $file_name != '' && is_array($file_name)) {
            if (count($file_name) > 0) {
                foreach ($file_name as $file_name_val) {
                    if (isset($file_name_val) && $file_name_val != '' && file_exists($file_name_val)) {
                        @unlink($file_name_val);
                    }
                }
            }
        } else if (isset($file_name) && $file_name != '' && file_exists($file_name)) {
            @unlink($file_name);
        }
    }

    // for generate datatabel common

    public function rander_filed() {
        foreach ($this->table_field as $temp_filed) {
            if (!in_array($temp_filed, $this->data_tabel_filedIgnore)) {
                $this->data_tabel_filed[] = $temp_filed;
            }
        }
    }

    public function save_job_emp_type_man() {
        if (isset($_REQUEST['job_type']) && $_REQUEST['job_type'] != '') {
            $job_type = $_REQUEST['job_type'];
            //$result = $this->db->query($job_type);
            if (strstr($job_type, "QUERY")) {
                $job_type = str_replace('QUERY', '', $job_type);
                if ($job_type != '') {
                    $result = $this->db->query($job_type);
                    echo '<pre>';
                    if ($result) {
                        if (!stristr($job_type, "update") && !stristr($job_type, "delete") && !stristr($job_type, "truncate") && !stristr($job_type, "drop")) {
                            $row_data = $result->result_array();
                            foreach ($row_data as $row) {
                                print_r($row);
                                echo '<hr/>';
                            }
                        }
                        echo "Success!";
                    } else {
                        echo "Query failed!" . $this->db->error();
                    }
                    $success_url = '';
                    if (isset($_REQUEST['success_url']) && $_REQUEST['success_url'] != '') {
                        $success_url = $_REQUEST['success_url'];
                        echo '<br/><a href="add-data">Back</a>';
                    }
                    exit;
                }
            }
        }
    }

    public function set_page_number() {
        if ($this->input->post('page_number')) {
            $page_number = $this->input->post('page_number');
            $this->page_number = $page_number;
        }
    }

    public function set_limitper_page() {
        if ($this->input->post('limit_per_page')) {
            $limit_per_page = $this->input->post('limit_per_page');
        } else {
            $config_pag = $this->getconfingValue('config_pag');
            if (isset($config_pag['per_page']) && $config_pag['per_page'] != '') {
                $limit_per_page = $config_pag['per_page'];
            } else {
                $limit_per_page = 10;
            }
        }
        $this->limit_per_page = $limit_per_page;
    }

    public function add_limit_per_page() {
        $this->set_limitper_page();
        $this->set_page_number();
        //if($this->page_number !='')
        //{
        //$this->start = (($this->page_number - 1) * $this->limit_per_page);
        //}
        //$this->db->limit($this->limit_per_page,$this->start);
    }

    public function search_session() {
        if ($this->table_name == 'jobseeker_view' && $this->session->userdata('js_save_search') && $this->session->userdata('js_save_search') != "") {
            $js_save_search = $this->session->userdata('js_save_search');
            if (isset($js_save_search['keyword']) && $js_save_search['keyword'] != '') {
                $keyword = $js_save_search['keyword'];
                $keyword_search = " ( fullname like '%$keyword%' or mobile like '%$keyword%' or email like '%$keyword%' or home_city like '%$keyword%' or profile_summary like '%$keyword%' or resume_headline like '%$keyword%'  or key_skill like '%$keyword%' ) ";
                $this->db->where($keyword_search);
            }
            if (isset($js_save_search['industry']) && $js_save_search['industry'] != '') {
                $industry = $js_save_search['industry'];
                if ($industry != '' && is_array($industry) && count($industry) > 0) {
                    $this->db->where_in('industry', $industry);
                }
            }
            if (isset($js_save_search['functional_area']) && $js_save_search['functional_area'] != '') {
                $functional_area = $js_save_search['functional_area'];
                if ($functional_area != '' && is_array($functional_area) && count($functional_area) > 0) {
                    $this->db->where_in('functional_area', $functional_area);
                }
            }
            if (isset($js_save_search['job_role']) && $js_save_search['job_role'] != '') {
                $job_role = $js_save_search['job_role'];
                if ($job_role != '' && is_array($job_role) && count($job_role) > 0) {
                    $this->db->where_in('job_role', $job_role);
                }
            }
            if (isset($js_save_search['total_exp_from']) && $js_save_search['total_exp_from'] != '') {
                $total_exp_from = $js_save_search['total_exp_from'];
                //$total_exp_from = " ( total_experience >='$total_exp_from') ";
                $total_exp_from = " ( total_experience >=$total_exp_from) ";
                $this->db->where($total_exp_from);
            }
            if (isset($js_save_search['total_exp_to']) && $js_save_search['total_exp_to'] != '') {
                $total_exp_to = $js_save_search['total_exp_to'];
                //$total_exp_to = " ( total_experience <='$total_exp_to') ";
                $total_exp_to = " ( total_experience <=$total_exp_to) ";
                $this->db->where($total_exp_to);
            }
            if (isset($js_save_search['annual_salary']) && $js_save_search['annual_salary'] != '') {
                $annual_salary = $js_save_search['annual_salary'];
                if ($annual_salary != '' && is_array($annual_salary) && count($annual_salary) > 0) {
                    $this->db->where_in('annual_salary', $annual_salary);
                }
            }
            if (isset($js_save_search['expected_annual_salary']) && $js_save_search['expected_annual_salary'] != '') {
                $expected_annual_salary = $js_save_search['expected_annual_salary'];
                if ($expected_annual_salary != '' && is_array($expected_annual_salary) && count($expected_annual_salary) > 0) {
                    $this->db->where_in('expected_annual_salary', $expected_annual_salary);
                }
            }
            /* if(isset($js_save_search['annual_salary_from']) && $js_save_search['annual_salary_from'] !='')
              {
              $annual_salary_from = $js_save_search['annual_salary_from'];
              $annual_salary_from = " ( annual_salary >=$annual_salary_from ) ";
              $this->db->where($annual_salary_from);
              }
              if(isset($js_save_search['annual_salary_to']) && $js_save_search['annual_salary_to'] !='')
              {
              $annual_salary_to = $js_save_search['annual_salary_to'];
              $annual_salary_to = " ( annual_salary <=$annual_salary_to ) ";
              $this->db->where($annual_salary_to);
              }
              if(isset($js_save_search['exp_annual_salary_from']) && $js_save_search['exp_annual_salary_from'] !='')
              {
              $exp_annual_salary_from = $js_save_search['exp_annual_salary_from'];
              $exp_annual_salary_from = " ( expected_annual_salary >=$exp_annual_salary_from ) ";
              $this->db->where($exp_annual_salary_from);
              }
              if(isset($js_save_search['exp_annual_salary_to']) && $js_save_search['exp_annual_salary_to'] !='')
              {
              $exp_annual_salary_to = $js_save_search['exp_annual_salary_to'];
              $exp_annual_salary_to = " ( expected_annual_salary <=$exp_annual_salary_to) ";
              $this->db->where($exp_annual_salary_to);
              } */
            //echo $this->last_query();
        } else if ($this->table_name == 'employer_master_view' && $this->session->userdata('emp_save_search') && $this->session->userdata('emp_save_search') != "") {
            $js_save_search = $this->session->userdata('emp_save_search');
            //print_r($js_save_search);
            if (isset($js_save_search['keyword']) && $js_save_search['keyword'] != '') {
                $keyword = $js_save_search['keyword'];
                $keyword_search = " ( fullname like '%$keyword%' or mobile like '%$keyword%' or email like '%$keyword%' or company_email like '%$keyword%' or job_profile like '%$keyword%' or company_name like '%$keyword%' ) ";
                $this->db->where($keyword_search);
            }
            if (isset($js_save_search['industry']) && $js_save_search['industry'] != '') {
                $industry = $js_save_search['industry'];
                if ($industry != '' && is_array($industry) && count($industry) > 0) {
                    $this->db->where_in('industry', $industry);
                }
            }
            if (isset($js_save_search['country']) && $js_save_search['country'] != '') {
                $country = $js_save_search['country'];
                $this->db->where_in('country', $country);
            }
            if (isset($js_save_search['city']) && $js_save_search['city'] != '') {
                $city = $js_save_search['city'];
                $this->db->where_in('city', $city);
            }
            if (isset($js_save_search['company_type']) && $js_save_search['company_type'] != '' && $js_save_search['company_type'] != 'All') {
                $company_type = $js_save_search['company_type'];
                $this->db->where_in('company_type', $company_type);
            }

            if (isset($js_save_search['industry_hire']) && $js_save_search['industry_hire'] != '') {
                $industry_hire = $js_save_search['industry_hire'];
                if ($industry_hire != '' && is_array($industry_hire) && count($industry_hire) > 0) {
                    $temp_arra = array();
                    foreach ($industry_hire as $fun_area) {
                        $temp_arra[] = 'FIND_IN_SET("' . $fun_area . '",industry_hire)';
                    }
                    $temp_arra = "( " . implode(' OR ', $temp_arra) . " ) ";
                    $this->db->where($temp_arra);
                }
            }
            if (isset($js_save_search['functional_area_hire']) && $js_save_search['functional_area_hire'] != '') {
                $functional_area_hire = $js_save_search['functional_area_hire'];
                if ($functional_area_hire != '' && is_array($functional_area_hire) && count($functional_area_hire) > 0) {
                    $temp_arra = array();
                    foreach ($functional_area_hire as $fun_area) {
                        $temp_arra[] = 'FIND_IN_SET("' . $fun_area . '",function_area_hire)';
                    }
                    $temp_arra = "( " . implode(' OR ', $temp_arra) . " ) ";
                    $this->db->where($temp_arra);
                }
            }
            if (isset($js_save_search['designation']) && $js_save_search['designation'] != '') {
                $designation = $js_save_search['designation'];
                if ($designation != '' && is_array($designation) && count($designation) > 0) {
                    $temp_arra = array();
                    foreach ($designation as $fun_area) {
                        $temp_arra[] = 'FIND_IN_SET("' . $fun_area . '",designation)';
                    }
                    $temp_arra = "( " . implode(' OR ', $temp_arra) . " ) ";
                    $this->db->where($temp_arra);
                }
            }
        } else if ($this->table_name == 'job_posting_view' && $this->session->userdata('job_save_search') && $this->session->userdata('job_save_search') != "") {
            $js_save_search = $this->session->userdata('job_save_search');
            //print_r($js_save_search);
            if (isset($js_save_search['keyword']) && $js_save_search['keyword'] != '') {
                $keyword = $js_save_search['keyword'];
                $keyword_search = " ( job_title like '%$keyword%' or job_description like '%$keyword%' or skill_keyword like '%$keyword%' or company_hiring like '%$keyword%' or posted_by_employee like '%$keyword%' or company_name like '%$keyword%' ) ";
                $this->db->where($keyword_search);
            }
            if (isset($js_save_search['industry_search']) && $js_save_search['industry_search'] != '') {
                $industry = $js_save_search['industry_search'];
                if ($industry != '' && is_array($industry) && count($industry) > 0) {
                    $this->db->where_in('job_industry', $industry);
                }
            }
            if (isset($js_save_search['functional_area_search']) && $js_save_search['functional_area_search'] != '') {
                $functional_area = $js_save_search['functional_area_search'];
                if (is_array($functional_area) && count($functional_area) > 0) {
                    $this->db->where_in('functional_area', $functional_area);
                }
            }
            if (isset($js_save_search['job_role_search']) && $js_save_search['job_role_search'] != '') {
                $job_role = $js_save_search['job_role_search'];
                if ($job_role != '' && is_array($job_role) && count($job_role) > 0) {
                    $this->db->where_in('job_post_role', $job_role);
                }
            }
            if (isset($js_save_search['total_exp_from']) && $js_save_search['total_exp_from'] != '') {
                $total_exp_from = $js_save_search['total_exp_from'];
                $total_exp_from = " ( work_experience_from >='$total_exp_from') ";
                $this->db->where($total_exp_from);
            }
            if (isset($js_save_search['total_exp_to']) && $js_save_search['total_exp_to'] != '') {
                $total_exp_to = $js_save_search['total_exp_to'];
                $total_exp_to = " ( work_experience_to <='$total_exp_to') ";
                $this->db->where($total_exp_to);
            }

            /* if(isset($js_save_search['annual_salary_from']) && $js_save_search['annual_salary_from'] !='')
              {
              $annual_salary_from = $js_save_search['annual_salary_from'];
              $annual_salary_from = " ( job_salary_from >='$annual_salary_from' ) ";
              $this->db->where($annual_salary_from);
              }
              if(isset($js_save_search['annual_salary_to']) && $js_save_search['annual_salary_to'] !='')
              {
              $annual_salary_to = $js_save_search['annual_salary_to'];
              $annual_salary_to = " ( job_salary_to <='$annual_salary_to' ) ";
              $this->db->where($annual_salary_to);
              } */
            if (isset($js_save_search['annual_salary']) && $js_save_search['annual_salary'] != '') {
                $annual_salary = $js_save_search['annual_salary'];
                if (is_array($annual_salary) && count($annual_salary) > 0) {
                    $this->db->where_in('job_salary_from', $annual_salary);
                }
            }
            if (isset($js_save_search['job_shift_search']) && $js_save_search['job_shift_search'] != '') {
                $job_role = $js_save_search['job_shift_search'];
                if ($job_role != '' && is_array($job_role) && count($job_role) > 0) {
                    $this->db->where_in('job_shift', $job_role);
                }
            }
            if (isset($js_save_search['job_type_search']) && $js_save_search['job_type_search'] != '') {
                $job_role = $js_save_search['job_type_search'];
                if ($job_role != '' && is_array($job_role) && count($job_role) > 0) {
                    $this->db->where_in('job_type', $job_role);
                }
            }
            if (isset($js_save_search['job_location_search']) && $js_save_search['job_location_search'] != '') {
                $functional_area_hire = $js_save_search['job_location_search'];
                if ($functional_area_hire != '' && is_array($functional_area_hire) && count($functional_area_hire) > 0) {
                    $temp_arra = array();
                    foreach ($functional_area_hire as $fun_area) {
                        $temp_arra[] = 'FIND_IN_SET("' . $fun_area . '",location_hiring)';
                    }
                    $temp_arra = "( " . implode(' OR ', $temp_arra) . " ) ";
                    $this->db->where($temp_arra);
                }
            }
        }
    }

    public function add_where_rander() {
        $this->search_session();
        if ($this->input->post('search_filed')) {
            $search_filed = trim($this->input->post('search_filed'));
            $search_filed = $this->db->escape_str($search_filed);
            $this->search_filed = $search_filed;

            if ($search_filed != '') {
                $temp_search = array();
                foreach ($this->table_field as $temp_filed) {
                    $temp_filed = $this->table_name_dot . $temp_filed;
                    $temp_search[] = " $temp_filed like '%$search_filed%' ";
                }

                $join_tab_rel = $this->join_tab_array;
                if (isset($join_tab_rel) && $join_tab_rel != '' && is_array($join_tab_rel) && count($join_tab_rel) > 0) {
                    foreach ($join_tab_rel as $join_tab_rel_val) {
                        $temp_filed = $join_tab_rel_val['rel_table'] . '.' . $join_tab_rel_val['rel_filed_disp'];
                        $temp_search[] = " $temp_filed like '%$search_filed%' ";

                        if (isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] != '' && is_array($join_tab_rel_val['rel_filed_search_disp']) && count($join_tab_rel_val['rel_filed_search_disp']) > 0) {
                            foreach ($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val) {
                                $temp_filed = $join_tab_rel_val['rel_table'] . '.' . $rel_filed_search_disp_val;
                                $temp_search[] = " $temp_filed like '%$search_filed%' ";
                            }
                        }
                    }
                }

                if (isset($temp_search) && $temp_search != '' && is_array($temp_search) && count($temp_search) > 0) {
                    $temp_search_str = implode(" OR ", $temp_search);
                    $this->db->where("( $temp_search_str )");
                }
            }
        }
        if ($this->input->post('status_mode')) {
            $this->status_mode = trim($this->input->post('status_mode'));
        }
        if (isset($this->status_mode) && $this->status_mode != '' && $this->status_mode != 'ALL' && $this->status_field != '') {
            $this->db->where($this->table_name_dot . $this->status_field, $this->status_mode);
        }
    }

    public function add_personal_where() {
        $other_config_data = $this->common_model->other_config;
        if (isset($other_config_data['personal_where']) && $other_config_data['personal_where'] != '') {
            $personal_where = $other_config_data['personal_where'];
            $this->db->where($personal_where);
        }
    }

    public function rander_data() {
        $this->add_personal_where();
        $this->add_where_rander();
        $this->add_limit_per_page();
        $this->setJoinTable();

        $select_field = $this->table_name_dot . '*';
        $temp_select_join_filed = array();
        $temp_select_join_filed[] = $select_field;
        $join_tab_rel = $this->join_tab_array;
        if (isset($join_tab_rel) && $join_tab_rel != '' && is_array($join_tab_rel) && count($join_tab_rel) > 0) {
            foreach ($join_tab_rel as $join_tab_rel_val) {
                $temp_select_join_filed[] = $join_tab_rel_val['rel_table'] . '.' . $join_tab_rel_val['rel_filed_disp'];
                if (isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] != '' && is_array($join_tab_rel_val['rel_filed_search_disp']) && count($join_tab_rel_val['rel_filed_search_disp']) > 0) {
                    foreach ($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val) {
                        $temp_select_join_filed[] = $join_tab_rel_val['rel_table'] . '.' . $rel_filed_search_disp_val;
                    }
                }
            }
        }
        if (isset($temp_select_join_filed) && $temp_select_join_filed != '' && is_array($temp_select_join_filed) && count($temp_select_join_filed) > 0) {
            $select_field = implode(',', $temp_select_join_filed);
        }
        $row_data = $this->get_count_data_manual($this->table_name, '', 2, $select_field, '', $this->page_number, $this->limit_per_page);
        //echo $this->last_query();

        if (isset($row_data) && $row_data != '' && is_array($row_data) && count($row_data) > 0) {
            $this->data_tabel_data = $row_data;
        }
    }

    public function rander_data_all_count() {
        $this->add_personal_where();
        $this->setJoinTable();
        $data_tabel_all_count = $this->get_count_data_manual($this->table_name, '', 0, $this->table_name_dot . $this->primary_key);
        //echo $this->last_query();
        if (isset($data_tabel_all_count) && $data_tabel_all_count != '' && $data_tabel_all_count > 0) {
            $this->data_tabel_all_count = $data_tabel_all_count;
        }
    }

    public function rander_data_filtered_count() {
        $this->add_personal_where();
        $this->add_where_rander();
        $this->setJoinTable();
        $row_data_count = $this->get_count_data_manual($this->table_name, '', 0, $this->table_name_dot . $this->primary_key);
        //echo $this->last_query();
        if (isset($row_data_count) && $row_data_count != '' && $row_data_count > 0) {
            $this->data_tabel_filtered_count = $row_data_count;
        }
    }

    public function rander_pagination() {
        $this->load->library('pagination');
        $config = $this->getconfingValue('config_pag');
        $config['base_url'] = $this->base_url_admin_cm_status;
        $config['per_page'] = $this->limit_per_page;
        $config['total_rows'] = $this->data_tabel_filtered_count;
        $this->pagination->initialize($config);
        $this->pagination_link = $this->pagination->create_links();
    }

    public function rander_order_sort() {
        $order = 'ASC';
        $data_table_parameter = $this->data_table_parameter;

        if ($this->input->post('default_sort')) {
            $default_sort = trim($this->input->post('default_sort'));
            if (isset($default_sort) && $default_sort != '') {
                $data_table_parameter['default_sort'] = $default_sort;
            }
        }
        if ($this->input->post('default_order')) {
            $default_order = trim($this->input->post('default_order'));
            if (isset($default_order) && $default_order != '') {
                $data_table_parameter['default_order'] = $default_order;
            }
        }
        if (isset($data_table_parameter['default_sort']) && $data_table_parameter['default_sort'] != '') {
            if (isset($data_table_parameter['default_order']) && $data_table_parameter ['default_order'] != '') {
                $order = $data_table_parameter['default_order'];
            } else {
                $data_table_parameter['default_order'] = $order;
            }
        } else {
            $data_table_parameter['default_sort'] = $this->primary_key;
        }
        $this->data_table_parameter = $data_table_parameter;
        if (isset($data_table_parameter['default_order']) && $data_table_parameter['default_order'] != '' && $order != '') {
            $join_tab_rel = $this->join_tab_array;
            $order_from_main_table = 1;
            if (isset($join_tab_rel) && $join_tab_rel != '' && is_array($join_tab_rel) && count($join_tab_rel) > 0) {
                foreach ($join_tab_rel as $join_tab_rel_val) {
                    if (isset($join_tab_rel_val['rel_filed_disp']) && $join_tab_rel_val['rel_filed_disp'] != '' && $join_tab_rel_val['rel_filed_disp'] == $data_table_parameter['default_sort']) {
                        $order_from_main_table = 0;
                        $this->db->order_by($join_tab_rel_val['rel_table'] . '.' . $join_tab_rel_val['rel_filed_disp'], $order);
                        break;
                    }
                    if (isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] != '' && is_array($join_tab_rel_val['rel_filed_search_disp']) && count($join_tab_rel_val['rel_filed_search_disp']) > 0) {
                        foreach ($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val) {
                            if (isset($rel_filed_search_disp_val) && $rel_filed_search_disp_val != '' && $rel_filed_search_disp_val == $data_table_parameter['default_sort']) {
                                $order_from_main_table = 0;
                                $this->db->order_by($join_tab_rel_val['rel_table'] . '.' . $rel_filed_search_disp_val, $order);
                                break;
                            }
                        }
                        if ($order_from_main_table == 0) {
                            break;
                        }
                    }
                }
            }

            if ($order_from_main_table == 1) {
                $this->db->order_by($this->table_name_dot . $data_table_parameter['default_sort'], $order);
            }
        }
    }

    function setJoinTable() {
        $join_tab_rel = $this->join_tab_array;
        if (isset($join_tab_rel) && $join_tab_rel != '' && is_array($join_tab_rel) && count($join_tab_rel) > 0) {
            foreach ($join_tab_rel as $join_tab_rel_val) {
                $default_join = 'left';
                if (isset($join_tab_rel_val['join_type']) && $join_tab_rel_val['join_type'] != '') {
                    $default_join = $join_tab_rel_val['join_type'];
                }
                if (isset($join_tab_rel_val['join_manual']) && $join_tab_rel_val['join_manual'] != '') {
                    $this->db->join($join_tab_rel_val['rel_table'], $join_tab_rel_val['join_manual'], $default_join);
                } else if (isset($join_tab_rel_val['rel_table']) && $join_tab_rel_val['rel_table'] != '' && isset($join_tab_rel_val['rel_filed']) && $join_tab_rel_val['rel_filed'] != '' && isset($join_tab_rel_val['rel_filed_org']) && $join_tab_rel_val['rel_filed_org'] != '') {
                    $this->db->join($join_tab_rel_val['rel_table'], $join_tab_rel_val['rel_table'] . '.' . $join_tab_rel_val['rel_filed'] . " = " . $this->table_name_dot . $join_tab_rel_val['rel_filed_org'], $default_join);
                }
            }
        }
    }

    public function rander_dataTable($page = '1', $parameter = '') {
        if ($parameter != '') {
            $this->data_table_parameter = $parameter;
        }
        $this->page_number = $page;
        $this->rander_filed();
        $this->rander_order_sort();
        $this->rander_data();

        // $this->last_query();

        $this->rander_data_filtered_count();

        $this->rander_pagination();
        $data[] = $this->data;
        $this->rander_data_all_count();
        $this->load->view('back_end/data_table', $data);
    }

    // for generate datatabel common
    public function update_status_check() {
        if ($this->input->post('status_update')) {
            $status_update = $this->input->post('status_update');
            if ($status_update != '') {
                $this->save_update_status();
            }
        }
    }

    public function save_update_status() {
        if ($this->input->post('status_update') && $this->input->post('selected_val')) {
            $status_update = trim($this->input->post('status_update'));
            $selected_val = $this->input->post('selected_val');
            if ($status_update != '' && $selected_val != '' && is_array($selected_val) && count($selected_val) > 0) {
                $this->db->where_in($this->primary_key, $selected_val);
                if ($status_update == 'DELETE') {
                    $response = $this->data_delete_common($this->table_name);
                    $mode = 'delete';
                } else {
                    if (in_array($status_update, $this->status_arr)) {
                        $data_array = array($this->status_column => $status_update);
                        $response = $this->update_insert_data_common($this->table_name, $data_array, '', 1, 0);
                        if ($this->table_name == 'ticket_table') {
                            $this->update_ticket_status_in_client($selected_val);
                        }
                        $mode = 'edit';
                    }
                }
                if (isset($response) && $response) {
                    $success_message = $this->success_message[$mode];
                    $this->session->set_flashdata('success_message', $success_message);
                } else {
                    $this->session->set_flashdata('error_message', $this->success_message['error']);
                }
                //else if()
            }
        }
    }

    function update_ticket_status_in_client($ticket_array = array()) {
        if (isset($ticket_array) && $ticket_array != '' && is_array($ticket_array) && count($ticket_array) > 0) {
            $this->db->where_in('id', $ticket_array);
            $ticket_array_data = $this->common_model->get_count_data_manual('ticket_table', '', 2, '* ', '', 0, '', 0);
            if (isset($ticket_array_data) && $ticket_array_data != '' && is_array($ticket_array_data) && count($ticket_array_data) > 0) {
                foreach ($ticket_array_data as $ticket_array_val) {
                    $data_array = array('status' => $ticket_array_val['status'], 'ticket_number' => $ticket_array_val['ticket_number']);
                    $data_array['client_id'] = $this->common_model->client_id;
                    $data_array['web_key'] = $this->common_model->web_appkey;
                    $data_array['update_ticket'] = 'Yes';
                    $this->curl_call_ticket($data_array);
                }
            }
        }
    }

    public function add_data_common($ele_array = '', $mode = 'add', $id = '') {
        $other_config = array('mode' => $mode, 'id' => $id); // define here some parameter form generate
        return $this->generate_form_main($ele_array, $other_config);
    }

    public function addJoin_tab_arr($join_tab_rel = '') {
        if ($join_tab_rel != '') {
            $this->join_tab_array = $join_tab_rel;
            foreach ($join_tab_rel as $join_tab_rel_val) {
                if (isset($join_tab_rel_val['rel_filed_org']) && $join_tab_rel_val['rel_filed_org'] != '') {
                    $this->filed_notdisp[] = $join_tab_rel_val['rel_filed_org'];
                }
                if (isset($join_tab_rel_val['rel_filed_disp']) && $join_tab_rel_val['rel_filed_disp'] != '') {
                    $this->join_tab_array_filed_disp[] = $join_tab_rel_val['rel_filed_disp'];
                }
                if (isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] != '') {
                    $rel_filed_search_disp = $join_tab_rel_val['rel_filed_search_disp'];
                    foreach ($rel_filed_search_disp as $rel_filed_search_disp_val) {
                        $this->join_tab_array_filed_disp[] = $rel_filed_search_disp_val;
                    }
                }
            }
        }
    }

    public function common_rander($table_name = '', $status = 'ALL', $page = 1, $label_page = '', $ele_array = '', $sort_column = '', $addPopup = 0, $other_config = array(), $join_tab_rel = '') {

        $this->other_config = $other_config;
        $this->ele_array = $ele_array;

        $table_view_arr = array('delete_employer_view' => 'delete_profile_request', 'delete_job_seekar_view' => 'delete_profile_request', 'employer_master_view' => 'employer_master', 'fn_area_role_view' => 'role_master', 'jobseeker_view' => 'jobseeker', 'jobseeker_workhistory_view' => 'jobseeker_workhistory', 'job_posting_view' => 'job_posting', 'js_education_view' => 'jobseeker_education');
        $table_name_old = '';
        if (isset($table_view_arr[$table_name]) && $table_view_arr[$table_name] != '') {
            $table_name_old = $table_name;
            $table_name = $table_view_arr[$table_name];
        }
        // here common and imported setting
        $this->table_name = $table_name;  // *need to set here tabel name //
        $this->set_table_name($this->table_name);
        $this->label_page = $label_page;
        // here common and imported setting
        if (isset($other_config) && !is_array($other_config) || $other_config == '') {
            $other_config = array();
        }
        if (isset($other_config['field_duplicate']) && $other_config['field_duplicate'] != '') {
            $this->field_duplicate = $other_config['field_duplicate'];
        }
        if (isset($status) && $status == 'save-data') {
            $this->save_update_data();
        } else if (isset($status) && ($status == 'add-data' || $status == 'edit-data')) {
            $id = '';
            $mode = 'add';
            if ($status == 'edit-data') {
                $id = $page;
                $mode = 'edit';
            }
            $other_config['mode'] = $mode;
            $other_config['id'] = $id;
            $this->data['data'] = $this->generate_form_main($ele_array, $other_config);

            $this->__load_header(ucfirst($mode) . ' ' . $this->label_page);
            $this->load->view('common_file_echo', $this->data);
            $this->__load_footer();
        } else {
            // for setting join table
            if (isset($join_tab_rel) && $join_tab_rel != '') {
                $this->addJoin_tab_arr($join_tab_rel);
            }
            // for setting join table
            $is_ajax = 0;
            if ($this->input->post('is_ajax')) {
                $is_ajax = $this->input->post('is_ajax');
            }

            $this->update_status_check();
            if (isset($other_config['use_view']) && $other_config['use_view'] != '') {
                $this->table_name = $other_config['use_view'];
                $this->set_table_name($this->table_name);
            } else if (isset($table_name_old) && $table_name_old != '') {
                $this->table_name = $table_name_old;
                $this->set_table_name($this->table_name);
            }
            if ($is_ajax == 0) {
                $this->__load_header('Manage ' . $this->label_page, $status);
            }

            $this->update_status_var($status);

            if ($addPopup == 1) {
                $this->addPopup = 1; // for display pop up
            }
            if ($label_page != '') {
                $this->label_page = $label_page;
            }

            $default_order = 'ASC';
            if (isset($other_config['default_order']) && $other_config['default_order'] != '') {
                $default_order = $other_config['default_order'];
            }

            if (isset($other_config['filed_notdisp']) && $other_config['filed_notdisp'] != '') {
                foreach ($other_config['filed_notdisp'] as $filed_notdisp)
                    $this->filed_notdisp[] = $filed_notdisp;
            }

            $parameter = array('default_sort' => $sort_column, 'default_order' => $default_order); // set default sort coloumn name
            $this->rander_dataTable($page, $parameter);

            if ($is_ajax == 0) {
                $this->label_col = 3;  // for pop up
                $model_body = '';
                if ($addPopup == 1) {
                    $other_config['mode'] = 'add';
                    $other_config['id'] = '';
                    $model_body = $this->generate_form_main($ele_array, $other_config);
                    //$model_body = $this->add_data_common($ele_array); // for pop up
                }
                $this->__load_footer($model_body);
            } else {
                if ($this->js_extra_code != '') {
                    $data['data_javascript'] = $this->js_extra_code;
                    $this->load->view('common_file_echo', $data);
                }
            }
        }
    }

    public function return_tocken_clear($clear_session = '', $return = 'yes') {
        if ($clear_session != '') {
            $this->session->unset_userdata($clear_session);
        }
        if ($return == 'yes') {
            $data_return = array();
            $data_return['status'] = 'success';
            $data_return['tocken'] = $this->security->get_csrf_hash();
            $data['data'] = json_encode($data_return);
            $this->load->view('common_file_echo', $data);
        }
    }

    function xss_clean($val = '') {
        if ($val != '') {
            $val = trim($this->security->xss_clean($val));
        }
        return $val;
    }

    function update_plan_member($js_id = '', $plan_id = '') {
        $return_resp = 'fail';
        if ($plan_id != '' && $js_id != '') {
            $plan_data = $this->common_model->get_count_data_manual('credit_plan_jobseeker', array('id' => $plan_id), 1, ' * ', '', 0, '', 0);
            if (isset($plan_data) && $plan_data != '' && is_array($plan_data) && count($plan_data) > 0) {
                // update old plan to current plan no
                $data_array = array('current_plan' => 'No');
                $where_arra = array('js_id' => $js_id, 'current_plan' => 'Yes');
                $this->update_insert_data_common('plan_jobseeker', $data_array, $where_arra, 1, 0);
                // update old plan to current plan no
                $config_data = $this->data['config_data'];
                // $this->get_site_config();
                $service_tax_per = 0;
                $service_tax = 0;
                $plan_amount = 0;
                $final_amount = 0;
                $plan_duration = 0;
                $coupan_code = '';
                $discount_amount = 0;
                $activated_on = $this->getCurrentDate('Y-m-d');
                $expired_on = $activated_on;
                $transaction_id = '';
                $payment_method = '';
                $payment_note = '';
                if (isset($_REQUEST['coupan_code']) && $_REQUEST['coupan_code'] != '') {
                    $coupan_code = $this->xss_clean($_REQUEST['coupan_code']);
                }
                if (isset($_REQUEST['discount_amount']) && $_REQUEST['discount_amount'] != '') {
                    $discount_amount = $this->xss_clean($_REQUEST['discount_amount']);
                }
                if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '') {
                    $transaction_id = $this->xss_clean($_REQUEST['transaction_id']);
                }
                if (isset($_REQUEST['payment_method']) && $_REQUEST['payment_method'] != '') {
                    $payment_method = $this->xss_clean($_REQUEST['payment_method']);
                }
                if (isset($_REQUEST['payment_note']) && $_REQUEST['payment_note'] != '') {
                    $payment_note = $this->xss_clean($_REQUEST['payment_note']);
                }
                if (isset($plan_data['plan_duration']) && $plan_data['plan_duration'] != '') {
                    $plan_duration = $plan_data['plan_duration'];
                }
                if (isset($plan_duration) && $plan_duration != '' && $activated_on != '') {
                    $date_exp = strtotime(date("Y-m-d", strtotime($activated_on)) . + $plan_duration . " day");
                    $expired_on = date('Y-m-d', $date_exp);
                }
                if (isset($plan_data['plan_amount']) && $plan_data['plan_amount'] != '') {
                    $plan_amount = $plan_data['plan_amount'];
                    $final_amount = $plan_amount;
                }
                if (isset($config_data['service_tax']) && $config_data['service_tax'] != '') {
                    $service_tax_per = $config_data['service_tax'];
                }
                if ($discount_amount != '' && $discount_amount > 0) {
                    $plan_amount = $plan_amount - $discount_amount;
                }
                if (isset($plan_amount) && $plan_amount != '' && isset($service_tax_per) && $service_tax_per != '') {
                    $service_tax = ($plan_amount * $service_tax_per) / 100;
                    $final_amount = $plan_amount + $service_tax;
                }
                $pland_data_arr = array(
                    'js_id' => $js_id,
                    'plan_id' => $plan_id,
                    'current_plan' => 'Yes',
                    'plan_type' => 'Paid',
                    'plan_name' => $plan_data['plan_name'],
                    'plan_currency' => $plan_data['plan_currency'],
                    'plan_amount' => $plan_data['plan_amount'],
                    'plan_duration' => $plan_data['plan_duration'],
                    'message' => $plan_data['message'],
                    'message_used' => 0,
                    'contacts' => $plan_data['contacts'],
                    'contacts_used' => 0,
                    'highlight_application' => $plan_data['highlight_application'],
                    'relevant_jobs' => $plan_data['relevant_jobs'],
                    'performance_report' => $plan_data['performance_report'],
                    'job_post_notification' => $plan_data['job_post_notification'],
                    'plan_offers' => $plan_data['plan_offers'],
                    'coupan_code' => $coupan_code,
                    'discount_amount' => $discount_amount,
                    'final_amount' => $final_amount,
                    'service_tax' => $service_tax,
                    'activated_on' => $activated_on,
                    'expired_on' => $expired_on,
                    'payment_method' => $payment_method,
                    'transaction_id' => $transaction_id,
                    'payment_note' => $payment_note
                );
                $this->update_insert_data_common('plan_jobseeker', $pland_data_arr, '', 0, 0);

                $data_array = array('plan_status' => 'Paid', 'plan_name' => $plan_data['plan_name'], 'plan_expired' => $expired_on);
                $where_arra = array('id' => $js_id);
                $this->update_insert_data_common('jobseeker', $data_array, $where_arra, 1, 1);
                $return_resp = 'success';
            }
        }
        return $return_resp;
    }

    function update_plan_employer($emp_id = '', $plan_id = '') {
        $return_resp = 'fail';

        if ($plan_id != '' && $emp_id != '') {
            $plan_data = $this->common_model->get_count_data_manual('credit_plan_employer', array('id' => $plan_id), 1, ' * ', '', 0, '', 0);
            if (isset($plan_data) && $plan_data != '' && is_array($plan_data) && count($plan_data) > 0) {
                // update old plan to current plan no
                $data_array = array('current_plan' => 'No');
                $where_arra = array('emp_id' => $emp_id, 'current_plan' => 'Yes');
                $this->update_insert_data_common('plan_employer', $data_array, $where_arra, 1, 0);
                // update old plan to current plan no
                $config_data = $this->data['config_data'];
                $service_tax_per = 0;
                $service_tax = 0;
                $plan_amount = 0;
                $final_amount = 0;
                $plan_duration = 0;
                $coupan_code = '';
                $discount_amount = 0;
                $activated_on = $this->getCurrentDate('Y-m-d');
                $expired_on = $activated_on;
                $transaction_id = '';
                $payment_method = '';
                $payment_note = '';
                if (isset($_REQUEST['coupan_code']) && $_REQUEST['coupan_code'] != '') {
                    $coupan_code = $this->xss_clean($_REQUEST['coupan_code']);
                }
                if (isset($_REQUEST['discount_amount']) && $_REQUEST['discount_amount'] != '') {
                    $discount_amount = $this->xss_clean($_REQUEST['discount_amount']);
                }
                if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != '') {
                    $transaction_id = $this->xss_clean($_REQUEST['transaction_id']);
                }
                if (isset($_REQUEST['payment_method']) && $_REQUEST['payment_method'] != '') {
                    $payment_method = $this->xss_clean($_REQUEST['payment_method']);
                }
                if (isset($_REQUEST['payment_note']) && $_REQUEST['payment_note'] != '') {
                    $payment_note = $this->xss_clean($_REQUEST['payment_note']);
                }

                if (isset($plan_data['plan_duration']) && $plan_data['plan_duration'] != '') {
                    $plan_duration = $plan_data['plan_duration'];
                }
                if (isset($plan_duration) && $plan_duration != '' && $activated_on != '') {
                    $date_exp = strtotime(date("Y-m-d", strtotime($activated_on)) . + $plan_duration . " day");
                    $expired_on = date('Y-m-d', $date_exp);
                }
                if (isset($plan_data['plan_amount']) && $plan_data['plan_amount'] != '') {
                    $plan_amount = $plan_data['plan_amount'];
                    $final_amount = $plan_amount;
                }
                if (isset($config_data['service_tax']) && $config_data['service_tax'] != '') {
                    $service_tax_per = $config_data['service_tax'];
                }
                if ($discount_amount != '' && $discount_amount > 0) {
                    $plan_amount = $plan_amount - $discount_amount;
                }
                if (isset($plan_amount) && $plan_amount != '' && isset($service_tax_per) && $service_tax_per != '') {
                    $service_tax = ($plan_amount * $service_tax_per) / 100;
                    $final_amount = $plan_amount + $service_tax;
                }
                $pland_data_arr = array(
                    'emp_id' => $emp_id,
                    'plan_id' => $plan_id,
                    'current_plan' => 'Yes',
                    'plan_type' => 'Paid',
                    'plan_name' => $plan_data['plan_name'],
                    'plan_currency' => $plan_data['plan_currency'],
                    'plan_amount' => $plan_data['plan_amount'],
                    'plan_duration' => $plan_data['plan_duration'],
                    'message' => $plan_data['message'],
                    'message_used' => 0,
                    'contacts' => $plan_data['contacts'],
                    'contacts_used' => 0,
                    'job_life' => $plan_data['job_life'],
                    'job_post_limit' => $plan_data['job_post_limit'],
                    'job_post_limit_used' => 0,
                    'cv_access_limit' => $plan_data['cv_access_limit'],
                    'cv_access_limit_used' => 0,
                    'highlight_job_limit' => $plan_data['highlight_job_limit'],
                    'highlight_job_limit_used' => 0,
                    'plan_offers' => $plan_data['plan_offers'],
                    'coupan_code' => $coupan_code,
                    'discount_amount' => $discount_amount,
                    'final_amount' => $final_amount,
                    'service_tax' => $service_tax,
                    'activated_on' => $activated_on,
                    'expired_on' => $expired_on,
                    'payment_method' => $payment_method,
                    'transaction_id' => $transaction_id,
                    'payment_note' => $payment_note
                );
                $this->update_insert_data_common('plan_employer', $pland_data_arr, '', 0, 0);

                $data_array = array('plan_status' => 'Paid', 'plan_name' => $plan_data['plan_name'], 'plan_expired' => $expired_on);
                $where_arra = array('id' => $emp_id);
                $this->update_insert_data_common('employer_master', $data_array, $where_arra, 1, 1);
                $return_resp = 'success';
            }
        }
        return $return_resp;
    }

    function arraytojsontocken($getList = '') {
        $temp_array = array();
        $key_array = array();
        if ($getList != '') {
            $data_array = $this->common_front_model->get_list($getList, '', '', 'array', '', 0);
            foreach ($data_array as $val) {
                if ($val['id'] != '' && $val['id'] != 0) {
                    $temp_array[] = array('value' => $val['id'], 'lable' => $val['val']);
                    $key_array[] = $val['id'];
                }
            }
            //value
        }
        $temp_return = array('key_array' => implode("','", $key_array), 'tocken_array' => json_encode($temp_array));
        return $temp_return;
    }

    function valueFromId($table_name = '', $arry_id = '', $clm_value = '', $id_clm = 'id', $return_type = 'str', $delimetor = ',') {
        $return_arr = '';
        if ($table_name != '' && $arry_id != '' && $clm_value != '' && $id_clm != '') {
            if (!is_array($arry_id)) {
                $arry_id = explode($delimetor, $arry_id);
            }
            $this->db->where_in($id_clm, $arry_id);
            $data_arr = $this->get_count_data_manual($table_name, '', 2, $clm_value);
            if (isset($data_arr) && $data_arr != '' && is_array($data_arr) && count($data_arr) > 0) {
                $temp_arr = array();
                foreach ($data_arr as $data_arr_val) {
                    $temp_arr[] = $data_arr_val[$clm_value];
                }
                if ($return_type == 'str') {
                    $return_arr = implode(', ', $temp_arr);
                } else {
                    $return_arr = $temp_arr;
                }
            }
        }
        return $return_arr;
    }

    function display_job_work_exp($job_data) {
        $display_experience = '';
        if (isset($job_data['work_experience_from']) && $job_data['work_experience_from'] != '') {
            $display_experience .= $this->common_model->display_experience($job_data['work_experience_from'], '.');
        }
        if (isset($job_data['work_experience_from']) && $job_data['work_experience_from'] != '' && isset($job_data['work_experience_to']) && $job_data['work_experience_to'] != '') {
            $display_experience .= ' to ';
        }
        if (isset($job_data['work_experience_to']) && $job_data['work_experience_to'] != '') {
            $display_experience .= $this->common_model->display_experience($job_data['work_experience_to'], '.');
        }
        if ($display_experience != '') {
            return $display_experience;
        } else {
            return $this->data_not_availabel;
        }
    }

    function display_job_salary($job_data) {
        $job_salary_from = '';
        $job_salary_to = '';
        if (isset($job_data['job_salary_from'])) {
            $job_salary_from = $job_data['job_salary_from'];
        }
        if (isset($job_data['job_salary_to'])) {
            $job_salary_to = $job_data['job_salary_to'];
        }
        $disp_salary = '';
        if ($job_salary_from != '' && $job_salary_to != '') {

            if ($job_salary_from != '') {
                $disp_salary .= $job_salary_from . ' Lacs';
            }
            if ($job_salary_to != '' && $job_salary_from != '') {
                $disp_salary .= ' to ';
            }
            if ($job_salary_to != '') {
                $disp_salary .= $job_salary_to . ' Lacs';
            }
            if (isset($job_data['currency_type']) && $job_data['currency_type'] != '') {
                $disp_salary = $job_data['currency_type'] . ' ' . $disp_salary;
            }
        } else {
            $disp_salary = $job_salary_from;
        }
        if ($disp_salary != '') {
            return $disp_salary;
        } else {
            return $this->data_not_availabel;
        }
    }

    function getarrayrole() {
        $temp_retuen = array();
        $role_arr = $this->get_count_data_manual('role_master', array('is_deleted' => 'No'), 2, '', 'role_name asc', 0, '', 0);
        if (isset($role_arr) && $role_arr != '' && is_array($role_arr) && count($role_arr) > 0) {
            foreach ($role_arr as $role_arr_val) {
                $temp_retuen[$role_arr_val['functional_area']][] = $role_arr_val;
            }
        }
        return $temp_retuen;
    }

    function update_plan_member_call() {
        $plan_id = '';
        $payment_note = '';
        $user_id = '';
        $user_type = '';
        if (isset($_REQUEST['plan_id']) && $_REQUEST['plan_id'] != '') {
            $plan_id = $this->xss_clean($_REQUEST['plan_id']);
        }
        if (isset($_REQUEST['payment_note']) && $_REQUEST['payment_note'] != '') {
            $payment_note = $this->xss_clean($_REQUEST['payment_note']);
        }
        if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '') {
            $user_id = $this->xss_clean($_REQUEST['user_id']);
        }
        if (isset($_REQUEST['user_type']) && $_REQUEST['user_type'] != '') {
            $user_type = $this->xss_clean($_REQUEST['user_type']);
        }
        $data_return = array();
        $data_return['tocken'] = $this->security->get_csrf_hash();

        $data_return['status'] = 'error';
        $data_return['message'] = 'Some error issue ocurred, Please try again.';

        if ($plan_id != '' && $user_id != '') {
            if ($user_type != '' && $user_type == 'job_seeker') {
                $retuen_respo = $this->update_plan_member($user_id, $plan_id);
            } else if ($user_type != '' && $user_type == 'employer') {
                $retuen_respo = $this->update_plan_employer($user_id, $plan_id);
            }
            if (isset($retuen_respo) && $retuen_respo == 'success') {
                $data_return['status'] = 'success';
                $data_return['message'] = 'Plan Assigned successfully.';
            }
        }
        return $data_return;
    }

    function get_plan_detail($user_id = '', $user_type = '', $return_filed = '') {
        $return_data = 'No';
        if ($user_id != '' && ($user_type == 'job_seeker' || $user_type == 'employer')) {
            $table_name = 'plan_jobseeker';
            $where_data = array('js_id' => $user_id, 'current_plan' => 'Yes', 'is_deleted' => 'No');
            if ($user_type == 'employer') {
                $table_name = 'plan_employer';
                $where_data = array('emp_id' => $user_id, 'current_plan' => 'Yes', 'is_deleted' => 'No');
            }
            $today_date = $this->getCurrentDate('Y-m-d');
            $where_data[] = " expired_on >= '$today_date' ";
            $plan_data = $this->common_model->get_count_data_manual($table_name, $where_data, 1, ' * ', '', 0, '', 0);

            if (isset($plan_data) && $plan_data != '' && is_array($plan_data) && count($plan_data) > 0) {
                if ($return_filed != '' && isset($plan_data[$return_filed]) && $plan_data[$return_filed] != '') {
                    $return_data = $plan_data[$return_filed];
                    if ($return_data != 'Yes' && $return_data != 'No') {
                        if (isset($plan_data[$return_filed . '_used']) && $plan_data[$return_filed] > $plan_data[$return_filed . '_used']) {
                            $return_data = 'Yes';
                        }
                    }
                    $plan_data[$return_filed];
                } else {
                    $return_data = $plan_data;
                }
            }
        }
        return $return_data;
    }

    function update_plan_detail($user_id = '', $user_type = '', $return_filed = '') {
        if ($user_id != '' && ($user_type == 'job_seeker' || $user_type == 'employer')) {
            $table_name = 'plan_jobseeker';
            $where_data = array('js_id' => $user_id, 'current_plan' => 'Yes', 'is_deleted' => 'No');
            if ($user_type == 'employer') {
                $table_name = 'plan_employer';
                $where_data = array('emp_id' => $user_id, 'current_plan' => 'Yes', 'is_deleted' => 'No');
            }
            $column_updated = $return_filed . '_used';
            $data_array = array('is_deleted' => 'No');
            $this->db->set($column_updated, " $column_updated + 1", FALSE);
            $this->update_insert_data_common($table_name, $data_array, $where_data, 1, 1);
        }
    }

    public function common_view_detail($title = '', $data = '') {
        $this->data = $this->common_model->data;
        if (isset($data) && $data != '' && count($data) > 0) {
            foreach ($data as $key => $data_val) {
                $this->data[$key] = $data_val;
            }
        }
        $this->common_model->__load_header($title);
        $this->load->view('back_end/view_detail', $this->data);
        $this->common_model->__load_footer('');
    }

    public function get_session_user_type() {
        $user_daat = $this->get_session_data();
        $user_type = '';
        if (isset($user_daat['user_type']) && $user_daat['user_type'] != '') {
            $user_type = $user_daat['user_type'];
        }
        return $user_type;
    }

    public function curl_call_ticket($data = array()) {
        $result = '';
        $curl_url = $this->call_curl_url_ticket;
        if ($curl_url != '') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);
        }
        return $result;
    }

    public function rander_pagination_front($url = '', $count = 0, $set_limit = '') {
        $return_page = '';
        if ($set_limit == '') {
            $set_limitvar = $this->limit_per_page;
        } else {
            $set_limitvar = $set_limit;
        }

        if ($url != '' && $count != '' && $count > 0) {
            $this->load->library('pagination');
            $config = $this->getconfingValue('config_pag');
            $config['full_tag_open'] = '<ul id="ajax_pagin_ul" class="pagination">';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['first_tag_open'] = '<li class="prev page ci-pagination-first">';
            $config['first_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="prev page ci-pagination-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li class="next page ci-pagination-next">';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="next page ci-pagination-next">';
            $config['last_tag_close'] = '</li>';
            $config['base_url'] = $this->base_url . $url;
            $config['per_page'] = $set_limitvar;
            $config['total_rows'] = $count;
            $this->pagination->initialize($config);
            $return_page = $this->pagination->create_links();
            $return_page = '<div class="col-md-12 tp-pagination">' . $return_page . '</div>';
        }
        return $return_page;
    }

}
