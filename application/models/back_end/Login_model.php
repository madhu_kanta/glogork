<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	public function check_login()
	{
		
		$username = trim($this->input->post('username'));
		$password = trim($this->input->post('password'));
		$password_md5 = md5($password);
		$where_arra = array(
			'email'=>$username,
			'password	'=>$password_md5
		);
		$row_data = $this->common_model->get_count_data_manual('admin_user',$where_arra,1);
		$return_message = "";
		$status = 'error';
		
		if(isset($row_data) && $row_data !='' && is_array($row_data) && count($row_data) > 0)
		{
			$login_dt = $this->common_model->getCurrentDate();
			$status  = 'success';
			$this->db->set('last_login', $login_dt);
			$ip = $this->input->ip_address();
			$where_arra = array(
				'id'=>$row_data['id']
			);
			$data_array = array('last_login'=>$login_dt,'ip_address'=>$ip);
			$row_data1 = $this->common_model->update_insert_data_common('admin_user', $data_array, $where_arra);
			$this->session->set_userdata('jobportal_user_data', $row_data);
		}
		else
		{
			$return_message = "Your username and password is wrong. Please try again...";
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message,'token'=>$this->security->get_csrf_hash());
		return $return_arr;
	}
		public function check_reset_forgot_password()
	{
		$username = trim($this->input->post('username'));
		$username_where = " ( email ='$username') ";
		$this->db->select('*');
		$this->db->where($username_where);
		$this->db->where('is_deleted','No');
		$this->db->limit(1);
		$query = $this->db->get('admin_user');
		$return_message = "";
		$status = 'error';
		if($query->num_rows() == 1)
		{
			$this->load->helper('string');
			$password = random_string('alnum', 8);
			$row_data = $query->row_array();
			$password_md5 = md5($password);

			$this->db->set('c_password', $password_md5);
			$this->db->where('id', $row_data['id']);
			$this->db->update('admin_user');
			$status = 'success';
			$data_email = array(
				'email'=>$row_data['email'],
				'username'=>$row_data['username'],
				'c_password'=>$password_md5,
			);
			$this->forgot_email_send($data_email);
			$return_message = 'New Password linki has been sent to your email id, Please check your Email and reset your password.';
		}
		else
		{
			$return_message = "Your have enter Wrong Email Address. Please enter valid email address and Try again.";
		}
		$return_arr = array('status'=>$status,'errmessage'=>$return_message,'token'=>$this->security->get_csrf_hash());
		return json_encode($return_arr);
	}
	public function forgot_email_send($data_email)
	{
		$config_arra = $this->common_model->get_site_config();
		$web_name = $config_arra['web_name'];
		$webfriendlyname = $config_arra['web_frienly_name'];
		$username = $data_email['username'];
		$c_password = $data_email['c_password'];
		$email = $data_email['email'];
		
		$forgot_password_link = $web_name.$this->admin_path."/login/reset-password/$c_password/$email";
		$forgot_password_link = '<a target="_blank" href="'.$forgot_password_link.'">'.$forgot_password_link.'</a>';
		$message = "<html>
					<head>
					<title><h1>Your new password</h1></title>
					</head>
					<body>
						<p>Dear admin,</p>
						<p><strong>This is the mail regarding the password change request for your account, please update your password via below link.</strong></p>
						<p><br />
						<strong>Please click the below link and set the new password for your account at&nbsp;websitename.<br />
						Password reset Link :&nbsp;forgot_password_link</strong></p>
						<p>Regards ,</p>
						<p>webfriendlyname</p>
					</body>
					</html>";
		$array_repla = array('webfriendlyname'=>$webfriendlyname,'forgot_password_link'=>$forgot_password_link,'websitename'=>$webfriendlyname);
		$message_email = $this->common_front_model->getstringreplaced($message,$array_repla);
		$to_email = $email;
		$subject  = 'Password reset for your account on webfriendlyname';
		$subject = $this->common_front_model->getstringreplaced($subject,$array_repla);
		
		if($to_email !="" && $message !="")
		{
			$msg = $this->common_model->common_send_email($to_email,$subject,$message_email);
		}
	}
	public function check_reset_link($password,$email)
	{
		$password = trim($password);
		$email = trim($email);
		$username_where = " ( c_password ='$password' and email ='$email') ";
		$this->db->select('*');
		$this->db->where($username_where);
		$this->db->where('is_deleted','No');
		$this->db->limit(1);
		$query = $this->db->get('admin_user');
		$return_message = "";
		$status = 'error';
		if($query->num_rows() == 1)
		{
			$row_data = $query->row_array();
			$status = 'success';
			$this->session->set_userdata('email_reset', $row_data['email']);
			$status = 'success';
		}
		else
		{
			$status = 'error';
		}
		return $status;
	}
	public function reset_update_pass()
	{
		$email_reset = $this->session->userdata('email_reset');
		$return ='error';
		$return_message = "Some please try again.";
		if($email_reset !='')
		{
			$password = trim($this->input->post('password'));
			$password_md5 = md5($password);
			$where_arra = array(
				'email'=>$email_reset
			);
			$data_array = array('password'=>$password_md5,'c_password'=>'');
			$row_data1 = $this->common_model->update_insert_data_common('admin_user', $data_array, $where_arra);
			if($row_data1)
			{
				$return ='success';
				$return_message = "Your password has been successfully reset, please login with new password.";
			}
		}
		else
		{
			$return_message = "Your have already reset your password, please login with new password.";
		}
		$return_arr = array('status'=>$return,'errmessage'=>$return_message,'token'=>$this->security->get_csrf_hash());
		return $return_arr;
	}
	
}