<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Jobseeker_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	function get_data($id)
	{
		$data = '';
		if($id !='')
		{
			$where_arra = array('id'=>$id);
			$data = $this->common_model->get_count_data_manual('jobseeker_view',$where_arra,1,'');
		}
		return $data;
	}
	function seeker_list_model($status ='ALL', $page =1,$personal_where='')
	{
		$mobile_num ='';
		$country_code_old='';
		$title_old='';
		if($this->session->flashdata('old_post_data'))
		{    
			$old_post_data = $this->session->flashdata('old_post_data');
			if(isset($old_post_data ) && $old_post_data!='')
			{
				
			   if(isset($old_post_data['title']) && $old_post_data['title']!='')
			   {
				  $title_old = $old_post_data['title'];
			   }
			   
			   if(isset($old_post_data['mobile_num']) && $old_post_data['mobile_num']!='')
			   {
				  $mobile_num = $old_post_data['mobile_num'];
			   }
			   if(isset($old_post_data['country_code']) && $old_post_data['country_code']!='')
			   {
				  $country_code_old = $old_post_data['country_code'];
			   }
			   /*echo '<pre>';
			    print_r($old_post_data);
			   echo '</pre>';
			   exit;*/
			}
		}
		
		$where_country_code= " ( is_deleted ='No' )";
		$country_code_arr = $this->common_model->get_count_data_manual('country_master',$where_country_code,2,'country_code,country_name','','','',"");
		
		$mobile_ddr= '<div class="col-sm-6 col-lg-6 pl0">
			<select name="country_code" id="country_code" required class="form-control" >
			<option value="">Select Country Code</option>';
			if(isset($country_code_arr) && $country_code_arr !='' && is_array($country_code_arr) && count($country_code_arr) >0)
			{
				foreach($country_code_arr as $country_code_arr)
				{		
					if(isset($country_code_old) && $country_code_old!='' && $country_code_old==$country_code_arr['country_code']){
						$selected = 'selected';
					}else{$selected ='';}
					
					$mobile_ddr.= '<option value='.$country_code_arr['country_code'].' '.$selected.'>'.$country_code_arr['country_code'].' ('.$country_code_arr['country_name'].')'.'</option>';
				}
			}
		$mobile_ddr.='</select>
			</div>
			<div class="col-sm-6 col-lg-6 ">
				<input type="number" required name="mobile_num" id="mobile_num" class="form-control" placeholder="Mobile Number" value ="'.$mobile_num.'"  minlength="8" maxlength="13" />
			</div>';
		$title_arr =  $this->common_front_model->get_personal_titles();
		$title_code = '<div class="col-sm-12 col-lg-12 pl0">
			<select name="title" id="title" required class="form-control" >
			<option value="">Select Title</option>';
			if(isset($title_arr) && $title_arr !='' && is_array($title_arr) && count($title_arr) >0)
			{
				foreach($title_arr as $title_arr_val)
				{	
					$gn_class = '';
					if(strtolower(trim($title_arr_val['personal_titles'])) == 'mr.' || strtolower(trim($title_arr_val['personal_titles'])) == 'mr')
					{
						$gn_class = 'gn_classmale';
					}
					else if(strtolower(trim($title_arr_val['personal_titles'])) == 'ms.' || strtolower(trim($title_arr_val['personal_titles'])) == 'ms' || strtolower(trim($title_arr_val['personal_titles'])) == 'mrs' || strtolower(trim($title_arr_val['personal_titles'])) == 'mrs.' || strtolower(trim($title_arr_val['personal_titles'])) == "ma'am" || strtolower(trim($title_arr_val['personal_titles'])) == "ma'am.")
					{
						$gn_class = 'gn_classfemale';
					}	
					$title_code.= '<option class="'.$gn_class.'" value='.$title_arr_val['id'].'>'.$title_arr_val['personal_titles'].'</option>';
				}
			}
		$title_code.='</select>
			</div>';
		$this->common_model->js_extra_code.=' $(function(){change_gender("Male","add") });';
		$ele_array = array(
			'gender'=>array('is_required'=>'required','type'=>'radio','value_arr'=>array('Male'=>'Male','Female'=>'Female'),'value'=>'Male','onclick'=>"change_gender(this.value,'add')"),
			/*'title'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'personal_titles_master','key_val'=>'id','key_disp'=>'personal_titles'),'value'=>1),*/
			'title'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Title</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$title_code.'
			  </div>
			</div>'),
			'fullname'=>array('is_required'=>'required','label'=>'Full Name'),
			'mobile'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Mobile</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$mobile_ddr.'
			  <input type="hidden" name="mobile" id="mobile" value="" />
			  <input type="hidden" name="is_ajax" id="is_ajax" value="1" />
			  </div>
			</div>'),
			'email'=>array('is_required'=>'required','input_type'=>'email','check_duplicate'=>'Yes'),
			'password'=>array('type'=>'password','is_required'=>'required'),
			'landline'=>array('input_type'=>'number',),
			'status'=>array('type'=>'radio'),
			'birthdate'=>array('is_required'=>'required'),
			'marital_status'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'marital_status_master','key_val'=>'id','key_disp'=>'marital_status')),						
			'birthdate'=>array('is_required'=>'required','input_type'=>'date'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->extra_css[] = 'vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';
		$this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
		$this->common_model->extra_js[] = 'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js';
		$this->common_model->labelArr['annual_salary_name'] = 'Annual Salary';
		$data_table = array(
			'title_disp'=>'fullname',
			'disp_column_array'=> array('email','mobile','gender','birthdate','city_name','country_name','industries_name','functional_name','role_name','total_experience','annual_salary_name','plan_status','plan_name','register_date','last_login')
		);
		
		// pass #id# it will replace with table primary key value in url
		$this->common_model->button_array[] = array('url'=>'job-seeker/seeker_detail/#id#/edit','class'=>'info','label'=>'Edit Profile','target'=>'_blank');
		$this->common_model->button_array[] = array('url'=>'job-seeker/seeker_detail/#id#','class'=>'primary','label'=>'View Profile','target'=>'_blank');
		/*$btn_arr = array(
			array('url'=>'job-seeker/seeker_detail/#id#/edit','class'=>'info','label'=>'Edit Profile','target'=>'_blank'),
			array('url'=>'job-seeker/seeker_detail/#id#','class'=>'primary','label'=>'View Profile','target'=>'_blank')
		);*/
		
		$other_config = array('load_member'=>'yes','data_table_mem'=>$data_table,'data_tab_btn'=>$this->common_model->button_array,'default_order'=>'desc','action'=>'job-seeker/save_new_js','field_duplicate'=>array('email','mobile'),'sort_column'=>array('register_date'=>'Latest','last_login'=>'Last Login','fullname'=>'Name')); // load member for data table display member listing not table
	
		
		$label_disp = 'Job Seeker';
		if(isset($personal_where) && $personal_where !='' && is_array($personal_where) && count($personal_where))
		{
			if(isset($personal_where['where_per']) && $personal_where['where_per'] !='')
			{
				$other_config['personal_where'] = $personal_where['where_per'];
			}
			if(isset($personal_where['label_disp']) && $personal_where['label_disp'] !='')
			{
				$label_disp = $personal_where['label_disp'];
			}
		}
		$this->display_filter_form();
		
		$this->common_model->label_col = 2;
		$this->common_model->form_control_col =7;
		$this->common_model->addPopup = 0;
		$other_config['use_view'] = 'jobseeker_view';
		$data_table = $this->common_model->common_rander('jobseeker', $status, $page ,$label_disp ,$ele_array,'register_date',0,$other_config);
		
	}
	function display_filter_form()
	{	
		$this->common_model->extra_css[] = 'vendor/chosen_v1.4.0/chosen.min.css';
		$this->common_model->extra_js[] = 'vendor/chosen_v1.4.0/chosen.jquery.min.js';
		$this->common_model->js_extra_code.= " var config = {
			'.chosen-select': {},
			'.chosen-select-deselect': { allow_single_deselect: true },
			'.chosen-select-no-single': { disable_search_threshold: 10 },
			'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
			'.chosen-select-width': { width: '100%' }			
			};
			$('#industry').chosen({placeholder_text_multiple:'Select Industry'});
			$('#functional_area').chosen({placeholder_text_multiple:'Select Functional Area'});
			$('#job_role').chosen({placeholder_text_multiple:'Select Job Role'});  ";

		$this->common_model->label_col = 3;
		$this->common_model->form_control_col =9;
		$this->common_model->addPopup = 1;
		$total_exp= '<div class="col-sm-5 col-lg-5 pl0">
			<select name="total_exp_from" id="total_exp_from" class="form-control">
				<option selected value="" >From</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  //$val = $explode[0].'-'.$m_val;
					$val = $explode[0].'.'.$m_val;
					
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
					$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>
			<div class="col-sm-5 col-lg-5 pr0">
			<select name="total_exp_to" id="total_exp_to" class="form-control">
				<option selected value="" >To</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  	//$val = $explode[0].'-'.$m_val;
					$val = $explode[0].'.'.$m_val;
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
					$total_exp.= '<option value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='
		</select></div>';
		/*$anual_salary_str= '
			<div class="col-sm-5 col-lg-5 pl0">
				<select name="annual_salary_from" id="annual_salary_from" class="form-control">
				<option selected value="" >From</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$anual_salary_str.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>	
			<div class="col-sm-5 col-lg-5 pr0">
				<select name="annual_salary_to" id="annual_salary_to" class="form-control">
				<option selected value="" >To</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$anual_salary_str.='</select>
			</div>';
			
			$exp_anual_salary_str= '			
			<div class="col-sm-5 col-lg-55  pl0">
				<select name="exp_annual_salary_from" id="exp_annual_salary_from" class="form-control">
				<option selected value="" >From</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$exp_anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$exp_anual_salary_str.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>	
			<div class="col-sm-5 col-lg-5 pr0">
				<select name="exp_annual_salary_to" id="exp_annual_salary_to" class="form-control">
				<option selected value="" >To</option>';
				for($ij=0;$ij<=99;$ij=$ij+0.5)
				{
					$exp_anual_salary_str.= '<option value='.$ij.'>'.$ij.' Lacs</option>';
				}
				$exp_anual_salary_str.='</select>
			</div>';*/
	/*	'industry'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name')), */
		$ele_array = array(
			'keyword'=>array('placeholder'=>'Search with Full name, Email, Mobile, Resume Headline, Key Skill..'),
			'industry'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'functional_area'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','onchange'=>"dropdownChange_mul('functional_area','job_role','role_master')"),
			'job_role'=>array('type'=>'dropdown','is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select'),
			'total_experience'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Total Experience</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$total_exp.'			  
			  </div>
			</div>'),
			'annual_salary'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range')),
			'expected_annual_salary'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range'),'label'=>'Expected Annual Sal.'),
			/*'annual_salary'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Annual Salary</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$anual_salary_str.'
			  </div>
			</div>'),
			'expected_annual_salary'=>array('type'=>'manual','code'=>'
			<div class="form-group">
			  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Expected Annual Sal.</label>
			  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
			  '.$exp_anual_salary_str.'			  
			  </div>
			</div>'),*/
		);
		$other_config = array('mode'=>'add','id'=>'','action'=>'job-seeker/search_model','form_id'=>'form_model_search');
		$this->common_model->set_table_name('jobseeker');
		$data = $this->common_model->generate_form_main($ele_array,$other_config);
		$this->common_model->data['model_title_fil'] = 'Filter Data';
		$this->common_model->data['model_body_fil'] = $data;
	}
	function save_session_search()
	{
		$search_array = array(
			'keyword'=>'',
			'industry'=>'',
			'functional_area'=>'',
			'job_role'=>'',
			'total_exp_from'=>'',
			'total_exp_to'=>'',
			'annual_salary'=>'',
			'expected_annual_salary'=>'',
			/*'annual_salary_from'=>'',
			'annual_salary_to'=>'',
			'exp_annual_salary_from'=>'',
			'exp_annual_salary_to'=>''*/
		);
		if($this->input->post('keyword') && $this->input->post('keyword') !='')
		{
			$keyword = $this->input->post('keyword');
			$search_array['keyword'] = $keyword;
		}
		if($this->input->post('industry') && $this->input->post('industry') !='')
		{
			$industry = $this->input->post('industry');
			$search_array['industry'] = $industry;
		}
		if($this->input->post('functional_area') && $this->input->post('functional_area') !='')
		{
			$functional_area = $this->input->post('functional_area');
			$search_array['functional_area'] = $functional_area;
		}
		if($this->input->post('job_role') && $this->input->post('job_role') !='')
		{
			$job_role = $this->input->post('job_role');
			$search_array['job_role'] = $job_role;
		}
		
		if($this->input->post('total_exp_from') && $this->input->post('total_exp_from') !='')
		{
			$total_exp_from = $this->input->post('total_exp_from');
			$search_array['total_exp_from'] = $total_exp_from;
		}
		if($this->input->post('total_exp_to') && $this->input->post('total_exp_to') !='')
		{
			$total_exp_to = $this->input->post('total_exp_to');
			$search_array['total_exp_to'] = $total_exp_to;
		}
		if($this->input->post('annual_salary') && $this->input->post('annual_salary')!='')
		{
			$annual_salary = $this->input->post('annual_salary');
			$search_array['annual_salary'] = $annual_salary;
		}
		if($this->input->post('expected_annual_salary') && $this->input->post('expected_annual_salary')!='')
		{
			$expected_annual_salary = $this->input->post('expected_annual_salary');
			$search_array['expected_annual_salary'] = $expected_annual_salary;
		}
		/*if($this->input->post('annual_salary_from') && $this->input->post('annual_salary_from') !='')
		{
			 $annual_salary_from = $this->input->post('annual_salary_from');
			$annual_salary_from = number_format((float)$annual_salary_from, 1, '-', '');
			$search_array['annual_salary_from'] = str_replace('-','.',$annual_salary_from);
			//$annual_salary_from = number_format((float)$annual_salary_from, 2, '.', '');
			//$search_array['annual_salary_from'] = str_replace('.','-',$annual_salary_from);
		}
		if($this->input->post('annual_salary_to') && $this->input->post('annual_salary_to') !='')
		{
			$annual_salary_to = $this->input->post('annual_salary_to');
			$annual_salary_to = number_format((float)$annual_salary_to, 1, '-', '');
			$search_array['annual_salary_to'] = str_replace('-','.',$annual_salary_to);
			//$annual_salary_to = number_format((float)$annual_salary_to, 2, '.', '');
			//$search_array['annual_salary_to'] = str_replace('.','-',$annual_salary_to);
		}
		
		if($this->input->post('exp_annual_salary_from') && $this->input->post('exp_annual_salary_from') !='')
		{
			$exp_annual_salary_from = $this->input->post('exp_annual_salary_from');
			$exp_annual_salary_from = number_format((float)$exp_annual_salary_from, 1, '-', '');
			$search_array['exp_annual_salary_from'] = str_replace('-','.',$exp_annual_salary_from);
			//$exp_annual_salary_from = number_format((float)$exp_annual_salary_from, 2, '.', '');
			//$search_array['exp_annual_salary_from'] = str_replace('.','-',$exp_annual_salary_from);
		}
		if($this->input->post('exp_annual_salary_to') && $this->input->post('exp_annual_salary_to') !='')
		{
			$exp_annual_salary_to = $this->input->post('exp_annual_salary_to');
			$exp_annual_salary_to = number_format((float)$exp_annual_salary_to, 1, '-', '');
			$search_array['exp_annual_salary_to'] = str_replace('-','.',$exp_annual_salary_to);
			//$exp_annual_salary_to = number_format((float)$exp_annual_salary_to, 2, '.', '');
			//$search_array['exp_annual_salary_to'] = str_replace('.','-',$exp_annual_salary_to);
		}*/
		/*$total_exp_year = '';
		$total_exp_month = '';
		$total_experience='';
		if($this->input->post('total_exp_year') && $this->input->post('total_exp_year') !='')
		{
			$total_exp_year = $this->input->post('total_exp_year');
		}
		if($this->input->post('total_exp_month') && $this->input->post('total_exp_month') !='')
		{
			$total_exp_month = $this->input->post('total_exp_month');
		}
		if($total_exp_year !='' || $total_exp_month !='')
		{
			if($total_exp_year =='')
			{
				$total_exp_year = '0';
			}
			if($total_exp_month =='')
			{
				$total_exp_month = '0';
			}
			$search_array['total_experience'] = $total_exp_year.'-'.$total_exp_month;
		}		
		$annual_sal_lacs = '';
		$annual_sal_tho = '';
		$annual_salary ='';
		if($this->input->post('annual_sal_lacs') && $this->input->post('annual_sal_lacs') !='')
		{
			$annual_sal_lacs = $this->input->post('annual_sal_lacs');
		}
		if($this->input->post('annual_sal_tho') && $this->input->post('annual_sal_tho') !='')
		{
			$annual_sal_tho = $this->input->post('annual_sal_tho');
		}
		if($annual_sal_lacs !='' || $annual_sal_tho !='')
		{
			if($annual_sal_lacs =='')
			{
				$annual_sal_lacs = '0';
			}
			if($annual_sal_tho =='')
			{
				$annual_sal_tho = '0';
			}
			$search_array['annual_salary'] = $annual_sal_lacs.'-'.$annual_sal_tho;
		}
		
		$exp_annual_sal_lacs = '';
		$exp_annual_sal_tho = '';
		$exp_annual_salary ='';
		if($this->input->post('exp_annual_sal_lacs') && $this->input->post('exp_annual_sal_lacs') !='')
		{
			$exp_annual_sal_lacs = $this->input->post('exp_annual_sal_lacs');
		}
		if($this->input->post('exp_annual_sal_tho') && $this->input->post('exp_annual_sal_tho') !='')
		{
			$exp_annual_sal_tho = $this->input->post('exp_annual_sal_tho');
		}
		if($exp_annual_sal_lacs !='' || $exp_annual_sal_tho !='')
		{
			if($exp_annual_sal_lacs =='')
			{
				$exp_annual_sal_lacs = '0';
			}
			if($exp_annual_sal_tho =='')
			{
				$exp_annual_sal_tho = '0';
			}
			$search_array['exp_annual_salary'] = $exp_annual_sal_lacs.'-'.$exp_annual_sal_tho;
		}*/
		$this->session->set_userdata('js_save_search',$search_array);
		$this->common_model->return_tocken_clear();
	}
	function save_new_js()
	{
		$checkifmobile_exits = 0;
		if(isset($_REQUEST['mobile_num']) && $_REQUEST['mobile_num'] !='')
		{
			$mobile_num = '';
			$country_code = '';
			if(isset($_REQUEST['mobile_num']) && $_REQUEST['mobile_num'] !='')
			{
				$mobile_num = $_REQUEST['mobile_num'];
			}
			if(isset($_REQUEST['country_code']) && $_REQUEST['country_code'] !='')
			{
				$country_code = $_REQUEST['country_code'];
			}
			$mobile = $country_code.'-'.$mobile_num;
			$where = array("mobile='".$mobile."' and is_deleted='No'");
			$checkifmobile_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
			
			$_REQUEST['mobile'] = $mobile;
		}
		$_REQUEST['is_ajax'] = 1;
		$this->common_model->created_on_fild = 'register_date';
		if($this->input->post('password') && $this->input->post('password') !='')
		{
			$password = $this->input->post('password');
			$hashed_pass = $this->common_model->password_hash($password);
			$_REQUEST['password'] = $hashed_pass;
		}
		else if(isset($_REQUEST['password']))
		{
			unset($_REQUEST['password']);
		}
		$_REQUEST['user_agent'] ='NI-WEB - Admin';
		
		if(isset($_REQUEST['email']) && isset($_REQUEST['mobile']))
		{
			$this->common_model->field_duplicate = array('email','mobile');
		}
		
		if($checkifmobile_exits==0)
		{
			$this->common_model->set_table_name('jobseeker');
			$data = $this->common_model->save_update_data(0,1);
			$data = json_decode($data);
			$this->session->flashdata('success_message');
			//print_r($data);
			//exit;
			if(isset($data->status) && $data->status !='' && $data->status =='success')
			{
				$this->session->set_userdata('success_message_js',$data->response);
				// $insert_id = $this->db->insert_id();
				$insert_id = $this->common_model->last_insert_id;
				// $this->common_model->update_plan_employer($insert_id,1);
				return $insert_id;
			}
			else
			{
				//$this->session->set_flashdata('error_message',$data->response);
				$this->session->set_flashdata('error_message_js',$data->response);
				return '';
			}
		}
		else
		{
			/*set old data set in session when display error message*/
			$this->session->set_flashdata('old_post_data', $_REQUEST);
			/*set old data set in session when display error message*/
			
			$data->response = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>Mobile number already exits</div>';
			$this->session->set_flashdata('error_message_js',$data->response);
			return '';
		}
	}
	function save_edu_data($id)
	{
		$edu_id = '';
		$is_certificate_X_XII = '';
		$qualification_level = '';
		$specialization = '';
		$passing_year = '';
		$marks = '';
		$edu_total_count = 0;
		$certi_total_count = 0;
		if($this->input->post('edu_id') && $this->input->post('edu_id') !='')
		{
			$edu_id = $this->input->post('edu_id');
		}
		if($this->input->post('is_certificate_X_XII') && $this->input->post('is_certificate_X_XII') !='')
		{
			$is_certificate_X_XII = $this->input->post('is_certificate_X_XII');
		}
		if($this->input->post('qualification_level') && $this->input->post('qualification_level') !='')
		{
			$qualification_level = $this->input->post('qualification_level');
		}
		if($this->input->post('specialization') && $this->input->post('specialization') !='')
		{
			$specialization = $this->input->post('specialization');
		}
		if($this->input->post('institute') && $this->input->post('institute') !='')
		{
			$institute = $this->input->post('institute');
		}
		if($this->input->post('passing_year') && $this->input->post('passing_year') !='')
		{
			$passing_year = $this->input->post('passing_year');
		}
		if($this->input->post('marks') && $this->input->post('marks') !='')
		{
			$marks = $this->input->post('marks');
		}
		if($this->input->post('edu_total_count') && $this->input->post('edu_total_count') !='')
		{
			$edu_total_count = $this->input->post('edu_total_count');
		}
		if($this->input->post('certi_total_count') && $this->input->post('certi_total_count') !='')
		{
			$certi_total_count = $this->input->post('certi_total_count');
		}
		$i_total = 0;
		if(isset($edu_id) && $edu_id !='' && is_array($edu_id) && count($edu_id) > 0)
		{
			$i_total = count($edu_id);
		}
		$update_on = $this->common_model->getCurrentDate();
		for($i=0;$i<$i_total;$i++)
		{
			$mode = 'add';
			$edu_id_curr = '';
			$passing_year_curr = '';
			$qualification_level_curr = '';
			$institute_curr = '';
			$specialization_curr = '';
			$marks_curr = '';
			$is_certificate_X_XII_curr = '';
			if(isset($edu_id[$i]) && $edu_id[$i] !='')
			{
				$mode ='edit';
				$edu_id_curr = $edu_id[$i];
			}
			if(isset($passing_year[$i]) && $passing_year[$i] !='')
			{
				$passing_year_curr = $passing_year[$i];
			}
			if(isset($qualification_level[$i]) && $qualification_level[$i] !='')
			{
				$qualification_level_curr = $qualification_level[$i];
			}
			if(isset($institute[$i]) && $institute[$i] !='')
			{
				$institute_curr = $institute[$i];
			}
			if(isset($specialization[$i]) && $specialization[$i] !='')
			{
				$specialization_curr = $specialization[$i];
			}
			if(isset($marks[$i]) && $marks[$i] !='')
			{
				$marks_curr = $marks[$i];
			}
			if(isset($is_certificate_X_XII[$i]) && $is_certificate_X_XII[$i] !='')
			{
				$is_certificate_X_XII_curr = $is_certificate_X_XII[$i];
			}
			if(isset($passing_year_curr) && $passing_year_curr !='')
			{
				$data_array = array(
					'js_id'=>$id,
					'passing_year'=>$passing_year_curr,
					'qualification_level'=>$qualification_level_curr,
					'institute'=>$institute_curr,
					'specialization'=>$specialization_curr,
					'marks'=>$marks_curr,
					'is_certificate_X_XII'=>$is_certificate_X_XII_curr,
					'update_on'=>$update_on
				);
				if($mode =='edit' && $edu_id_curr !='')
				{
					$where_arra = array('id'=>$edu_id_curr);
					$this->common_model->update_insert_data_common('jobseeker_education',$data_array,$where_arra,1,1);
				}
				else
				{
					$this->common_model->update_insert_data_common('jobseeker_education',$data_array,'',0);
				}
			}
		}
		$this->session->set_flashdata('success_message',$this->common_model->success_message['edit']);
		$data['data'] =  $this->common_model->getjson_response();
		return $data;
		/*echo '<pre>';
		print_r($_REQUEST);
		echo '</pre>';*/
	}
	function save_job_work($id)
	{
		$update_on = date('Y-m-d h:i:s');
		for($i=1;$i<=10;$i++)
	    {
			$work_id = '';
			$company_name='';
			$joining_date='';
			$leaving_date='';
			$industry='';
			$functional_area='';
			
			$job_role='';
			$currency_type='';
			$achievements='';
			$annual_salary='';
			/*$annual_sal_tho='';
			$annual_sal_lacs = '';*/
			
			$mode = 'add';
			if(isset($_REQUEST['work_id_'.$i]) && $_REQUEST['work_id_'.$i] !='')
			{
				$work_id = $_REQUEST['work_id_'.$i];
				$mode = 'edit';
			}
			if(isset($_REQUEST['company_name_'.$i]) && $_REQUEST['company_name_'.$i] !='')
			{
				$company_name = $_REQUEST['company_name_'.$i];
			}
			else
			{
				continue;
			}
			if(isset($_REQUEST['joining_date_'.$i]) && $_REQUEST['joining_date_'.$i] !='')
			{
				$joining_date = $_REQUEST['joining_date_'.$i];
			}
			if(isset($_REQUEST['leaving_date_'.$i]) && $_REQUEST['leaving_date_'.$i] !='')
			{
				$leaving_date = $_REQUEST['leaving_date_'.$i];
			}
			if(isset($_REQUEST['joining_date_'.$i]) && $_REQUEST['joining_date_'.$i] !='')
			{
				$joining_date = $_REQUEST['joining_date_'.$i];
			}
			if(isset($_REQUEST['industry_'.$i]) && $_REQUEST['industry_'.$i] !='')
			{
				$industry = $_REQUEST['industry_'.$i];
			}
			if(isset($_REQUEST['functional_area_'.$i]) && $_REQUEST['functional_area_'.$i] !='')
			{
				$functional_area = $_REQUEST['functional_area_'.$i];
			}
			if(isset($_REQUEST['job_role_'.$i]) && $_REQUEST['job_role_'.$i] !='')
			{
				$job_role = $_REQUEST['job_role_'.$i];
			}
			if(isset($_REQUEST['currency_type_'.$i]) && $_REQUEST['currency_type_'.$i] !='')
			{
				$currency_type = $_REQUEST['currency_type_'.$i];
			}
			/*if(isset($_REQUEST['annual_sal_lacs_'.$i]) && $_REQUEST['annual_sal_lacs_'.$i] !='')
			{
				$annual_sal_lacs = $_REQUEST['annual_sal_lacs_'.$i];
			}
			if(isset($_REQUEST['annual_sal_tho_'.$i]) && $_REQUEST['annual_sal_tho_'.$i] !='')
			{
				$annual_sal_tho = $_REQUEST['annual_sal_tho_'.$i];
			}
			//$annual_salary = $annual_sal_lacs.'-'.$annual_sal_tho;
			$annual_salary = $annual_sal_lacs.'.'.$annual_sal_tho;*/
			
			if(isset($_REQUEST['annual_salary_'.$i]) && $_REQUEST['annual_salary_'.$i] !='')
			{
				$annual_salary = $_REQUEST['annual_salary_'.$i];
			}
			
			if(isset($_REQUEST['achievements_'.$i]) && $_REQUEST['achievements_'.$i] !='')
			{
				$achievements = $_REQUEST['achievements_'.$i];
			}
			$data_array = array(
				'js_id'=>$id,
				'company_name'=>$company_name,
				'joining_date'=>$joining_date,
				'leaving_date'=>$leaving_date,
				'industry'=>$industry,
				'functional_area'=>$functional_area,
				'job_role'=>$job_role,
				'annual_salary'=>$annual_salary,
				'currency_type'=>$currency_type,
				'achievements'=>$achievements,
				'update_on'=>$update_on
			);
		//	print_r($data_array);
			if($mode =='add')
			{
				$response = $this->common_model->update_insert_data_common("jobseeker_workhistory",$data_array,'',0);
			}
			else
			{
				$response = $this->common_model->update_insert_data_common("jobseeker_workhistory",$data_array,array('id'=>$work_id),1,1);
			}
		}
		$this->session->set_flashdata('success_message',$this->common_model->success_message['edit']);
		$data['data'] =  $this->common_model->getjson_response();
		return $data;
	}
	function save_job_lan($id)
	{
		$update_on = date('Y-m-d h:i:s');
		for($i=1;$i<=5;$i++)
	    {
			$langu_id = '';
			$langu_name='';
			$proficiency='';
			$reading='No';
			$writing='No';
			$speaking='No';
			$mode = 'add';
			if(isset($_REQUEST['langu_id_'.$i]) && $_REQUEST['langu_id_'.$i] !='')
			{
				$langu_id = $_REQUEST['langu_id_'.$i];
				$mode = 'edit';
			}
			if(isset($_REQUEST['langu_name_'.$i]) && $_REQUEST['langu_name_'.$i] !='')
			{
				$langu_name = $_REQUEST['langu_name_'.$i];
			}
			else
			{
				continue;
			}
			if(isset($_REQUEST['proficiency_'.$i]) && $_REQUEST['proficiency_'.$i] !='')
			{
				$proficiency = $_REQUEST['proficiency_'.$i];
			}
			if(isset($_REQUEST['reading_'.$i]) && $_REQUEST['reading_'.$i] !='')
			{
				$reading = 'Yes';
			}
			if(isset($_REQUEST['writing_'.$i]) && $_REQUEST['writing_'.$i] !='')
			{
				$writing = 'Yes';
			}
			if(isset($_REQUEST['speaking_'.$i]) && $_REQUEST['speaking_'.$i] !='')
			{
				$speaking = 'Yes';
			}
			$data_array = array(
				'js_id'=>$id,
				'language'=>$langu_name,
				'proficiency_level'=>$proficiency,
				'reading'=>$reading,
				'writing'=>$writing,
				'speaking'=>$speaking,
				'update_on'=>$update_on
			);
			if($mode =='add')
			{
				$response = $this->common_model->update_insert_data_common("jobseeker_language",$data_array,'',0);
			}
			else
			{
				$response = $this->common_model->update_insert_data_common("jobseeker_language",$data_array,array('id'=>$langu_id),1,1);
			}
		}
		$this->session->set_flashdata('success_message',$this->common_model->success_message['edit']);
		$data['data'] =  $this->common_model->getjson_response();
		return $data;
	}
	function delete_job_lan($l_id)
	{
		$this->common_model->data_delete_common('jobseeker_language',array('id'=>$l_id),1,'yes');
		$this->session->set_flashdata('success_message', 'language data deleted successfully');
	}
	function delete_job_work($l_id)
	{
		$this->common_model->data_delete_common('jobseeker_workhistory',array('id'=>$l_id),1,'yes');
		$this->session->set_flashdata('success_message', 'Work History data deleted successfully');
	}
	function delete_edu_work($l_id)
	{
		$this->common_model->data_delete_common('jobseeker_education',array('id'=>$l_id),1,'yes');
		$this->session->set_flashdata('success_message', 'Education data deleted successfully');
	}
	function update_job_detail()
	{
		if(isset($_REQUEST['preferred_city']) && $_REQUEST['preferred_city'] !='')
		{
			$val_req = $_REQUEST['preferred_city'];
			$tmp_val = implode(',',$val_req);
			$_REQUEST['preferred_city'] = $tmp_val;
		}
		if($this->input->post('password') && $this->input->post('password') !='')
		{
			$password = $this->input->post('password');
			$hashed_pass = $this->common_model->password_hash($password);
			$_REQUEST['password'] = $hashed_pass;
		}
		else if(isset($_REQUEST['password']))
		{
			unset($_REQUEST['password']);
		}
		
		if(isset($_REQUEST['country_code']) && $_REQUEST['country_code'] !='')
		{
			$country_code = $_REQUEST['country_code'];
		}
		else
		{
			$country_code = '';
		}
		if(isset($_REQUEST['mobile_num']) && $_REQUEST['mobile_num'] !='')
		{
			$mobile_num = $_REQUEST['mobile_num'];
		}
		else
		{
			$mobile_num = '';	
		}
		if(isset($_REQUEST['email']) && $_REQUEST['email'] !='')
		{
			$email = $_REQUEST['email'];
		}
		else
		{
			$email = '';	
		}
		if(isset($_REQUEST['id']) && $_REQUEST['id'] !='')
		{
			$js_id = $_REQUEST['id'];
		}
		else
		{
			$js_id='';	
		}
		$mobile = $country_code.'-'.$mobile_num;
		if(isset($mobile) && $mobile!='' && $mobile_num !='')
		{
			//$where = array("mobile='".$mobile."' and is_deleted='No'");
			$where = array('mobile'=>$mobile,'id!='=>$js_id,'is_deleted'=>'No');
			$checkifmobile_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
		}
		else
		{
			 $checkifmobile_exits = 0 ;
		}
		
		if(isset($email) && $email!='')
		{
			//$where = array("mobile='".$mobile."' and is_deleted='No'");
			$where = array('email'=>$email,'id!='=>$js_id,'is_deleted'=>'No');
			$checkifemail_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
		}
		else
		{
			 $checkifemail_exits = 0 ;
		}
		
		if($checkifmobile_exits==0 && $checkifemail_exits == 0)
		{
			$this->common_model->set_table_name('jobseeker');
			$data = $this->common_model->save_update_data(0);
			//echo  $this->db->last_query();
			return $data;
		}
		else if($checkifmobile_exits!=0 && $checkifemail_exits !=0)
		{
			$this->session->set_flashdata('error_message','Mobile number and email are already exists.');
			$data['data'] =  $this->common_model->getjson_response();
			return $data;
			
		}
		else if($checkifmobile_exits==0 && $checkifemail_exits !=0)
		{
			$this->session->set_flashdata('error_message','Email already exists.');
			$data['data'] =  $this->common_model->getjson_response();
			return $data;
		}
		else if($checkifmobile_exits!=0 && $checkifemail_exits ==0)
		{
			$this->session->set_flashdata('error_message','Mobile number already exists. Please use a different mobile phone number.');
			$data['data'] =  $this->common_model->getjson_response();
			return $data;
		}
	}	
}