<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Common_model extends CI_Model {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$success_array = array(
			'add' =>'Data inserted successfully.',
			'edit' =>'Data updated successfully.',
			'error' =>'Some error occured, please try again.',
			'delete'=>'Data deleted successfully.',
			'error_file' =>'Some error occured when file upload, please try again.'
		);
		$this->success_message = $success_array;
		$this->extra_css = array('vendor/checkbo/src/0.1.4/css/checkBo.min.css');
		$this->extra_js = array('vendor/jquery-validation/dist/jquery.validate.min.js','vendor/checkbo/src/0.1.4/js/checkBo.min.js');
		$this->mode = 'add';
		$this->js_validation_extra ='';
		$this->js_extra_code ='';
	// main tabel	
		$this->table_name ='';
		$this->table_name_dot='';
		$this->table_field ='';
		$this->status_field = 'status';
	// main tabel
	// for relation, Join tabel
		$this->join_tab_array = '';
		$this->join_tab_array_filed_disp = '';
		$this->filed_notdisp = array();
	// for relation, Join tabel
		$this->primary_key ='id';
		$this->data_tabel_filedIgnore = array('id','is_deleted');

		$this->data_tabel_filed = '';
		$this->data_tabel_data = '';
		$this->data_tabel_filtered_count = 0;
		$this->data_tabel_all_count = 0;
		$this->search_filed = '';
		$this->limit_per_page =10;
		$this->pagination_link ='';
		$this->page_number=1;
		$this->admin_path = $this->data['admin_path'] = $this->getconfingValue('admin_path');
		$this->class_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->success_url = $this->method_name;
		
		$this->action = $this->router->fetch_class().'/'.$this->method_name.'/save-data';
		
		$this->add_fun = $this->method_name.'/add-data';
		$this->base_url_admin_cm_status = $this->base_url_admin_cm = base_url().$this->admin_path.'/'.$this->class_name.'/'.$this->method_name.'/';
		$this->start = 0;
		$this->data_table_parameter = '';
		$this->status_mode ='';
		$this->status_arr = array('APPROVED'=>'APPROVED','UNAPPROVED'=>'UNAPPROVED');
		$this->status_column = 'status';
		$this->label_col = 2;
		$this->form_control_col = 7;
		$this->addPopup = 0;
		$this->labelArr = array('country_id'=>'Country Name','state_id'=>'State Name'); // here we can set common label change for coloumn name
		$this->data['base_url'] = $this->base_url = base_url();
		$this->data['base_url_admin'] = $this->base_url_admin = $this->base_url.$this->admin_path.'/';
		$this->data['config_data'] = $this->get_site_config();
		$this->label_page='';
		$this->permenent_delete = 'no';
		$this->is_delete_fild = 'is_deleted';
	}
	public function __load_header($label_page='',$status='')
	{
		$this->label_page = $label_page;
		$page_title = $label_page;
		if($status !='')
		{
			$page_title = $page_title.' - '.$status;
		}
		$this->data['page_title'] = $page_title;
		$this->load->view('back_end/page_part/header',$this->data);
	}
	public function __load_footer($model_body='')
	{
		$this->data['model_body'] = $model_body;
		$this->data['model_title'] = 'Add '.$this->label_page;
		$this->load->view('back_end/page_part/footer',$this->data);
	}
	public function checkLogin()
	{
		if(!$this->session->userdata('jobportal_user_data') || $this->session->userdata('jobportal_user_data') =="" && count($this->session->userdata('jobportal_user_data')) ==0 )
		{
			$base_url = base_url();
			$admin_path = $this->getconfingValue('admin_path');
			redirect($base_url.$admin_path.'/login');
		}
	}
	public function set_table_name($table_name='')
	{
		if($table_name !='')
		{
			$this->table_name = $this->table_name = $table_name;
			$this->table_name_dot = $table_name.'.';
			$this->table_field = $this->db->list_fields($this->table_name);
		}
	}
	function getjson_response()
	{
		$data_return = array();
		if($this->session->flashdata('success_message'))
		{
			$data_return['response'] = '<div class="alert alert-success  alert-dismissable"><div class="fa fa-check">&nbsp;</div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('success_message').'</div>';
			$data_return['status']   = 'success';
			$this->session->unset_userdata('success_message');
		}
		else if($this->session->flashdata('error_message'))
		{
			$data_return['status']   = 'error';
			$data_return['response'] = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
			$this->session->unset_userdata('error_message');
		}
		$data_return['tocken'] = $this->security->get_csrf_hash();
		return json_encode($data_return);
	}
	public function update_status_var($status)
	{
		$this->status_mode = $status;
		$this->base_url_admin_cm_status = $this->base_url_admin_cm.$status.'/';
	}
	public function get_site_config()
	{
       	$this->db->limit(1);
		$query = $this->db->get_where('site_config', array('id' => 1));
       	return $query->row_array();
	}
	public function get_session_data()
	{
		return $this->session->userdata('jobportal_user_data');
	}
	public function common_send_email($to_array,$subject,$message,$cc_array= '',$bcc_array ='',$attachment = '')
	{
		$config = array(
		  	'protocol' => 'mail',
		  	'smtp_host' => 'smtp host',	// need to set 
		  	'smtp_port' => 465,
		  	'smtp_user' => 'smtp user', // change it to yours
		  	'mailtype' => 'html',
		  	'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		/* 'smtp_pass' => 'smtp password', // change it to yours */
		
        $this->load->library( 'email' );// , $config //for authenticate email
		$this->email->set_newline("\r\n");
		$config_arra = $this->get_site_config();
		$from_email = $config_arra['from_email'];
		
		$this->email->from($from_email);
		$this->email->to($to_array);
		
		if(isset($cc_array) && $cc_array !="")
		{
			$this->email->cc($cc_array);
		}
		if(isset($bcc_array) && $bcc_array !="")
		{
			$this->email->bcc($bcc_array);
		}
		if(isset($attachment) && $attachment !="")
		{
			$this->email->attach($attachment);
		}
		$this->email->subject($subject);
		$this->email->message($message);
		$msg = 'Email sent.';
		
		$base_url = base_url();
		if($base_url !='http://192.168.1.111/job_portal/')
		{
			if($this->email->send())
			{
				$msg = 'Email sent.';
			}
			else
			{
				//$msg = $this->email->print_debugger();
				//show_error($this->email->print_debugger());
			}
		}
		return $msg;
	}
	public function common_sms_send($mobile,$sms)
	{
		if($mobile !='' && $sms !='')
		{
			$mobile_ext = substr($mobile,0,3);
			if($mobile_ext == '+91')
			{
				$sms = str_replace(" ","%20",$sms);
				$mno = substr($mobile,3,15);
				$final_api="SMS api url and ";
				/*
					$ch = curl_init($final_api);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
					curl_setopt($ch, CURLOPT_TIMEOUT, 3);
					$curl_scraped_page = curl_exec($ch);
					curl_close($ch);
				*/
			}
			else
			{
				// need to uncomment for send sms
				/*require(APPPATH.'twilio_library/Services/Twilio.php');
				$account_sid = "AC81b73d63963ea4405f760264d142e903"; // Your Twilio account sid
				$auth_token = "7dc9ba1f3aa830cb9c330ae75bf16d6a"; // Your Twilio auth token
				$client = new Services_Twilio($account_sid, $auth_token);
				$message = $client->account->messages->sendMessage(
				  '+12267801184', // From a Twilio number in your account
				  $mobile,//'+19054524548', // Text any number
				  $sms
				);*/
					// need to uncomment for send sms
			}
		}
	}
	function last_query()
	{
		return $this->db->last_query();
	}
	function get_count_data_manual($table,$where_arra='',$flag_count_data = 0,$select_f ='',$order_by='',$disp_query = "")
	{
		if($table !='')
		{
			$this->db->where($table.'.'.$this->is_delete_fild,'No');
			if($where_arra !='' && count($where_arra) >0)
			{
				foreach($where_arra as $key=>$val)
				{
					$this->db->where($key,$val);
				}
			}			
			if(isset($select_f) && $select_f !='')
			{
				$this->db->select($select_f);
			}

			if($flag_count_data == 0)
			{
				//$search_data = $this->db->get_compiled_select($table);
								
				return $this->db->count_all_results($table);
			}
			else 
			{
				if(isset($order_by) && $order_by !='')
				{
					$this->db->order_by($order_by);
				}
				if($flag_count_data==1)
				{
					$this->db->limit(1);
				}
				
				if($disp_query ==1)
				{
					echo $this->db->get_compiled_select($table);
				}
				
				$query = $this->db->get($table);
				if($flag_count_data == 1)
				{
					if($query->num_rows() == 0)
					{
						return '';	
					}
					else
					{
						return $query->row_array();
					}
				}
				else
				{
					if($query->num_rows() == 0)
					{
						return '';	
					}
					else
					{
						return $query->result_array();
					}
				}
			}
		}
		else
		{
			return '';
		}
	}
	function update_insert_data_common($table='',$data_array='',$where_arra='',$flag_update=1,$limit=1)
	{
		$return = false;
		if($table !='' && $data_array !='')
		{
			if($flag_update == 1)
			{
				if($where_arra !='' && count($where_arra) >0)
				{
					foreach($where_arra as $key=>$val)
					{
						$this->db->where($key,$val);
					}
				}
				if($limit == 1)
				{
					$this->db->limit(1);
				}
				$return = $this->db->update($table,$data_array);
			}
			else
			{
				if($flag_update == 2)
				{
					$return = $this->db->insert_batch($table,$data_array);
				}
				else
				{
					$return = $this->db->insert($table,$data_array);
				}
			}
		}
		return $return;
	}
	function data_delete_common($table='',$where_arra='',$limit=0)
	{
		$return = false;
		if($table !='')
		{
			if($where_arra !='' && count($where_arra) >0)
			{
				foreach($where_arra as $key=>$val)
				{
					$this->db->where($key,$val);
				}
			}
			if($limit == 1)
			{
				$this->db->limit(1);
			}
			if($this->permenent_delete =='no')
			{
				$data_array = array($this->is_delete_fild =>'Yes');
				$return = $this->db->update($table,$data_array);
			}
			else
			{
				$return = $this->db->delete($table);
			}
			// is_deleted
		}
		return $return;
	}
	function getconfingValue($item_name ='')
	{
		$return = '';
		if($item_name !='')
		{
			$return = $this->config->item($item_name);
		}
		return $return;
	}
	function generate_form_main($ele_array = '',$other_config='')
	{
		$table_name = $this->table_name;
		$ele_array_key = array_keys($ele_array);
		if(isset($other_config['mode']) && $other_config['mode'] =='edit' && $table_name !='')
		{
			$where_arr = '';
			$primary_key = $this->primary_key;
			if(isset($primary_key) && $primary_key !='' && isset($other_config['id']) && $other_config['id'] !='')
			{
				$where_arr = array($primary_key => $other_config['id']);
			}
			$select_field = '';
			if(isset($ele_array_key) && $ele_array_key !='' && count($ele_array_key) > 0)
			{
				$table_field = $this->table_field;
				$ele_array_key = array_intersect($ele_array_key,$table_field);
				
				$select_field = implode(',',$ele_array_key);
			}
			$row_data = $this->get_count_data_manual($table_name,$where_arr,1,$select_field);
			if(!isset($row_data) || $row_data =='' || count($row_data) ==0 )
			{
				redirect($this->base_url_admin_cm);
			}
		}
		$element_array = array();		
		foreach($ele_array as $key=>$val)
		{
			$temp_array = $val;
			if(isset($other_config['mode']) && $other_config['mode'] =='edit' && isset($row_data[$key]) && $row_data[$key] !='')
			{
				$temp_array['value'] = $row_data[$key];
			}
			if(isset($val['type']) && $val['type'] !='')
			{
				$temp_array['type'] = $val['type'];
			}
			else
			{
				$temp_array['type'] = 'textbox';
			}
			$element_array[$key] = $temp_array;
		}
		return $this->generate_form($element_array,$other_config);
	}
	function is_required($element_array_val)
	{
		$is_required = "";
		if(isset($element_array_val['is_required']) && $element_array_val['is_required'] !='' && $element_array_val['is_required'] =='required')
		{
			$is_required = " required ";
		}
		return $is_required;
	}
	function get_label($element_array_val='',$key='')
	{
		$labelArr = $this->common_model->labelArr;
		if(isset($labelArr[$key]) && $labelArr[$key] !='')
		{
			$label = $labelArr[$key];
		}
		else if(isset($element_array_val['label']) && $element_array_val['label'] !='')
		{
			$label = $element_array_val['label'];
		}
		else
		{
			$label = str_replace('_',' ',$key);
			$label = ucwords($label);
		}
		return $label;
	}
	function get_value($element_array_val,$key='value',$defult='')
	{
		$value_curr = $defult;
		if(isset($element_array_val[$key]) && $element_array_val[$key] !='')
		{
			$value_curr = $element_array_val[$key];
		}
		return $value_curr;
	}
	function getRelationDropdown($element_array_val)
	{
		$return_arr = '';
		$value_curr = $this->get_value($element_array_val,'value','');
		$relation_arr = $this->get_value($element_array_val,'relation','');
		if(isset($relation_arr) && $relation_arr !='' && count($relation_arr) > 0)
		{
			
			if(isset($relation_arr['rel_table']) && $relation_arr['rel_table'] !='' && isset($relation_arr['key_val']) && $relation_arr['key_val'] !='' && isset($relation_arr['key_disp']) && $relation_arr['key_disp'] !='' )
			{
				$select_field = $relation_arr['key_disp'].', '.$relation_arr['key_val'];
				
				$where_close = array();
				if($value_curr !='')
				{
					$where_close[] = $relation_arr['key_val']." = '".$value_curr."' ";
				}
				$status_filed = 'status';
				$status_val = 'APPROVED';
				if(isset($relation_arr['status_filed']) && $relation_arr['status_filed'] !='')
				{
					$status_filed = $relation_arr['status_filed'];
				}
				if(isset($relation_arr['status_val']) && $relation_arr['status_val'] !='')
				{
					$status_val = $relation_arr['status_val'];
				}
				if($status_filed !='' && $status_val !='')
				{
					$where_close[] = $status_filed." = '".$status_val."' ";
				}
				if(isset($where_close) && $where_close !='' && count($where_close) > 0 )
				{
					$where_close_str = implode(" OR ",$where_close);
					$this->db->where(" ( $where_close_str ) ");
				}
				if(isset($relation_arr['rel_col_name']) && $relation_arr['rel_col_name'] !='' && isset($relation_arr['rel_col_val']) && $relation_arr['rel_col_val'] !='')
				{
					$this->db->where($relation_arr['rel_col_name'],$relation_arr['rel_col_val']);
				}
				$row_data = $this->get_count_data_manual($relation_arr['rel_table'],"",2,$select_field,$relation_arr['key_disp'].' ASC ',0);
				//print_r($row_data);
				$return_arr = array();
				if(isset($row_data) && $row_data !='' && count($row_data) > 0)
				{
					foreach($row_data as $row_data_val)
					{
						$return_arr[$row_data_val[$relation_arr['key_val']]] = $row_data_val[$relation_arr['key_disp']];
					}
				}
				//print_r($return_arr);
			}
		}
		// $this->get_value($element_array_val,'value','');
		return $return_arr;
	}
	function generate_dropdown($element_array_val,$key)
	{
		$return_content='';
		if(count($element_array_val) > 0 && $key !='')
		{
			$value_curr = $this->get_value($element_array_val,'value','');
			$label = $this->get_label($element_array_val,$key);
			$is_required = $this->is_required($element_array_val);
			$class = $this->get_value($element_array_val,'class');

			$onChange = $this->get_value($element_array_val,'onchange','');
			if($onChange !='')
			{
				$onChange = 'onChange="'.$onChange.'"';
			}
			
			$value_arr = $this->get_value($element_array_val,'value_arr','');
			if(!isset($value_arr) || $value_arr =='' || count($value_arr) ==0)
			{
				$value_arr = $this->getRelationDropdown($element_array_val);
			}
			$return_content.='
				<div class=form-group>
				  <label class="col-sm-'.$this->label_col.' col-lg-'.$this->label_col.' control-label">'.$label.'</label>
				  <div class="col-sm-9 col-lg-'.$this->form_control_col.'">
				  	<select '.$onChange.' '.$is_required.' name="'.$key.'" id="'.$key.'" class="form-control '.$class.' ">
						<option selected value="" >Select '.$label.'</option>';
				  		
					if(isset($value_arr) && $value_arr !='' && count($value_arr) > 0)
					{
						foreach($value_arr as $key_r=>$value_arr_val)
						{
							$selected_drop = '';
							if($value_curr == $key_r)
							{
								$selected_drop = 'selected';
							}
							$return_content.='<option '.$selected_drop.' value="'.$key_r.'">'.$value_arr_val.'</option>';
						}
					}
			$return_content.='
					</select>
				  </div>
				</div>';
		}
		return $return_content;
	}
	function generate_radio($element_array_val,$key)
	{
		$return_content='';
		if(count($element_array_val) > 0 && $key !='')
		{
			$value_curr = $this->get_value($element_array_val,'value','APPROVED');
			$label = $this->get_label($element_array_val,$key);
			$is_required = $this->is_required($element_array_val);
			
			$value_arr = $this->get_value($element_array_val,'value_arr',$this->status_arr);
			$this->status_arr = $value_arr;
			$return_content.='
				<div class=form-group>
				  <label class="col-sm-'.$this->label_col.' col-lg-'.$this->label_col.' control-label">'.$label.'</label>
				  <div class="col-sm-9 col-lg-'.$this->form_control_col.'">
				  <div class=radio>';
			//print_r($value_arr);
			if(isset($value_arr) && $value_arr !='' && count($value_arr) > 0)
			{
				foreach($value_arr as $key_r=>$value_arr_val)
				{
					$selected_radio = '';
					if($value_curr == $key_r)
					{
						$selected_radio = 'checked';
					}
					$return_content.='<label><input '.$selected_radio.' type="radio" name="'.$key.'" id="'.$key_r.'" value="'.$key_r.'">'.$value_arr_val.'</label>&nbsp;&nbsp;';
				}
			}
			$return_content.='
					</div>
				  </div>
				</div>';
		}
		return $return_content;
	}
	function generate_textbox($element_array_val,$key)
	{
		$return_content='';
		if(count($element_array_val) > 0 && $key !='')
		{
			$value_curr = $this->get_value($element_array_val);
			$label = $this->get_label($element_array_val,$key);
			$is_required = $this->is_required($element_array_val);
			$input_type = $this->get_value($element_array_val,'input_type','text');
			$other = $this->get_value($element_array_val,'other');
			$class = $this->get_value($element_array_val,'class');
			
			$return_content.='
				<div class=form-group>
				  <label class="col-sm-'.$this->label_col.' col-lg-'.$this->label_col.' control-label">'.$label.'</label>
				  <div class="col-sm-9 col-lg-'.$this->form_control_col.'" >
					<input '.$other.' type="'.$input_type.'" '.$is_required.' name="'.$key.'" id="'.$key.'" class="form-control '.$class.' " placeholder="'.$label.'" value ="'.htmlentities(stripcslashes($value_curr)).'" />
				  </div>
				</div>';
		}
		return $return_content;
	}
	function generate_textarea($element_array_val,$key)
	{
		$return_content='';
		if(count($element_array_val) > 0 && $key !='')
		{
			$value_curr = $this->get_value($element_array_val);
			$label = $this->get_label($element_array_val,$key);
			$is_required = $this->is_required($element_array_val);
			$class = $this->get_value($element_array_val,'class');
			$return_content.='
				<div class=form-group>
				  <label class="col-sm-'.$this->label_col.' col-lg-'.$this->label_col.' control-label">'.$label.'</label>
				  <div class="col-sm-9 col-lg-'.$this->form_control_col.' '.$key.'_edit" >
				  	<textarea '.$is_required.' rows="4" name="'.$key.'" id="'.$key.'" class="form-control '.$class.' " placeholder="'.$label.'">'.$value_curr.'</textarea>
				  </div>
				</div>';
		}
		return $return_content;
	}
	function generate_file_upload($element_array_val,$key)
	{
		$return_content='';
		if(count($element_array_val) > 0 && $key !='')
		{
			$value_curr = $this->get_value($element_array_val);
			$label = $this->get_label($element_array_val,$key);
			$is_required = $this->is_required($element_array_val);
			$extension = $this->get_value($element_array_val,'extension','jpg|png|jpeg|gif');
			$path_value = $this->get_value($element_array_val,'path_value','');
			$class = $this->get_value($element_array_val,'class');
			if($this->mode == 'edit')
			{
				$is_required = '';	
			}
			$img_display = '';
			if($path_value !='' && $value_curr !='' && file_exists($path_value.$value_curr))
			{
				$img_display = '<img style="max-height:100px" class="img-responsive" src="'.$this->base_url.$path_value.$value_curr.'" />';
			}
			$return_content.='
				<div class=form-group>
                  <label class="col-sm-'.$this->label_col.' control-label">'.$label.'</label>
                  <div class=col-sm-4>
                    <input filesize="10" extension="'.$extension.'" '.$is_required.' type="file" name="'.$key.'" id="'.$key.'" class="form-control '.$class.' " />
					<input type="hidden" name="'.$key.'_val" id="'.$key.'_val" value="'.$value_curr.'" />
					<input type="hidden" name="'.$key.'_path" id="'.$key.'_path" value="'.$path_value.'" />
					<input type="hidden" name="'.$key.'_ext" id="'.$key.'_ext" value="'.$extension.'" />
                    <p class=help-block>Allowed File type '.str_replace('|', ' | ',$extension).'.</p>
                  </div>
				  <div class="col-sm-4">
				  	'.$img_display.'
				  </div>
                </div>';
		}
		return $return_content;
	}
	function generate_form_element($element_array = '')
	{
		$return_content='';
		if(isset($element_array) && $element_array !='' && count($element_array) > 0)
		{
			foreach($element_array as $key=>$element_array_val)
			{
				if(isset($element_array_val['type']) && $element_array_val['type'] =='textbox')
				{
					$return_content.=$this->generate_textbox($element_array_val,$key);
				}
				else if(isset($element_array_val['type']) && $element_array_val['type'] =='textarea')
				{
					$return_content.=$this->generate_textarea($element_array_val,$key);
				}
				else if(isset($element_array_val['type']) && $element_array_val['type'] =='file')
				{
					$return_content.=$this->generate_file_upload($element_array_val,$key);
				}
				else if(isset($element_array_val['type']) && $element_array_val['type'] =='radio')
				{
					$return_content.=$this->generate_radio($element_array_val,$key);
				}
				else if(isset($element_array_val['type']) && $element_array_val['type'] =='dropdown')
				{
					$return_content.=$this->generate_dropdown($element_array_val,$key);
				}
				else if(isset($element_array_val['type']) && $element_array_val['type'] =='manual')
				{
					$return_content.=$element_array_val['code'];
				}
			}
		}
		return $return_content;
	}
	function generate_form($element_array = '',$other_config='')
	{
		$return_content='';
		$action='';
		$onsubmit='';
		$enctype ='';
		$this->mode = $other_config['mode'];

		if(isset($other_config['success_url']) && $other_config['success_url'] !='')
		{
			$this->success_url = $other_config['success_url'];
		}
		if(isset($other_config['action']) && $other_config['action'] !='')
		{
			$action='action="'.$this->base_url_admin.$other_config['action'].'"';
		}
		else if(isset($this->action) && $this->action !='')
		{
			$action='action="'.$this->base_url_admin.$this->action.'"';
		}
		if(isset($other_config['onsubmit']) && $other_config['onsubmit'] !='')
		{
			$onsubmit=' onSubmit="'.$other_config['onsubmit'].'"';
		}
		if(isset($other_config['enctype']) && $other_config['enctype'] !='')
		{
			$enctype = $this->get_value($other_config,'enctype');
		}
		if(isset($element_array) && $element_array !='' && count($element_array) > 0)
		{
			$message_div = '<div id="reponse_message">';
			if($this->session->flashdata('error_message'))
			{
				$message_div.='<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
			}
			if($this->session->flashdata('success_message'))
			{
				$message_div.='<div class="alert alert-success  alert-dismissable"><div class="fa fa-check">&nbsp;</div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('success_message').'</div>';
			}
			$message_div.='</div>';
			$return_content.='
					  <div class="panel mb25">        
						<div class=panel-body>
						  <div class="row no-margin">
							<div class=col-lg-12>
								<form method="post" id="common_insert_update" name="common_insert_update" class="form-horizontal bordered-group" role=form '.$action.$onsubmit.' '.$enctype.' >';
			$return_content.=$message_div;
			$return_content.=$this->generate_form_element($element_array);
			$return_content.='
							<div class=form-group>
							  <label class="col-sm-'.$this->label_col.'"></label>
							  <div class="col-sm-9">
							  	<button type="submit" class="btn btn-primary mr10">Submit</button>
							  ';
								if($this->addPopup == 0)
								{
									$return_content.='
										<button type="button" onClick="back_list()" class="btn btn-default">Back</button>';
								}
								else
								{
									$return_content.='
										<button type="button" onClick="close_popup()" class="btn btn-default" data-dismiss="modal">Close</button>';
								}
							  $return_content.='</div>
							</div>
							<input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'" id="hash_tocken_id" class="hash_tocken_id" />
							<input type="hidden" name="mode" value="'.$other_config['mode'].'" id="mode" />
							<input type="hidden" name="id" value="'.$other_config['id'].'" id="id" />
							<input type="hidden" name="success_url" value="'.$this->success_url.'" id="success_url" />
						</form>
						</div>
					  </div>
					</div>
				  </div>';
		}
		return $return_content;
	}
	public function generate_url_comon($generate_url='')
	{
		$return_url='';
		if($generate_url !='')
		{
			$generate_url_arr = explode('-|-',$generate_url);
			if(isset($generate_url_arr) && $generate_url_arr !='' && count($generate_url_arr) >0)
			{
				$from_gen = '';
				$to_gen = '';
				if(isset($generate_url_arr[0]) && $generate_url_arr[0] !='')
				{
					$from_gen = $generate_url_arr[0];
				}
				if(isset($generate_url_arr[1]) && $generate_url_arr[1] !='')
				{
					$to_gen = $generate_url_arr[1];
				}
				if($from_gen !='' && $to_gen !='')
				{
					$from_gen_val='';
					if($this->input->post($from_gen))
					{
						$from_gen_val = $this->input->post($from_gen);
					}
					if($from_gen_val !='')
					{
						$_REQUEST[$to_gen] = $url_genrate = strtolower(url_title($from_gen_val));
					}
				}
			}
		}
		$return_url = strtolower($return_url);
		return $return_url;
	}
	public function save_update_data()
	{
		/*print_r($_REQUEST);
		print_r($_FILES);
		exit;*/
		$table_name = $this->table_name;
		$table_field = $this->table_field;
		$primary_key = $this->primary_key;
		if($this->input->post('success_url'))
		{
			$this->success_url = $this->input->post('success_url');
		}
		if($table_name !='' && $table_field !='' && count($table_field) > 0 )
		{
			$where_arra='';
			$flag_update = 0;
			$limit = '';
			$data_array = array();
			$data_file_old_array = array();
			$data_file_new_array = array();
			$mode = 'add';
			//error_reporting(E_ALL);
			//echo '<pre>';
			//print_r($_FILES);
			//echo '</pre>';
			//exit;
			// for check edit mode

			if(isset($_REQUEST['mode']) && $_REQUEST['mode'] !='' && $_REQUEST['mode'] =='edit')
			{				
				$id = '';
				$mode = 'edit';
				if($this->input->post('id'))
				{
					$id = trim($this->input->post('id'));
				}
				if($id !='')
				{
					$where_arra = array($primary_key =>$id);
					$limit = 1;
				}
				$flag_update = 1;
			}
			else
			{
				$genrate_url='';
				if($this->input->post('genrate_url'))
				{
					$genrate_url = $this->input->post('genrate_url');
					if($genrate_url !='')
					{
						$this->generate_url_comon($genrate_url);
					}
				}
			}
			// for check edit mode
			// for check file upload
			if(isset($_FILES) && $_FILES !='' && count($_FILES) > 0)
			{
				foreach($_FILES as $key=>$val)
				{
					if(in_array($key,$table_field) && isset($val['name']) && $val['name'] !='' && isset($val['size']) && $val['size'] > 0)
					{
						$path_upload = '';
						if($this->input->post($key.'_path'))
						{
							$path_upload = trim($this->input->post($key.'_path'));
						}
						$old_upload = '';
						if($this->input->post($key.'_val'))
						{
							$old_upload = trim($this->input->post($key.'_val'));
						}
						$allowed_types ='gif|jpg|png|jpeg';
						if($this->input->post($key.'_ext'))
						{
							$allowed_types = trim($this->input->post($key.'_ext'));
						}
						
						$temp_data_array = array('file_name'=>$key,'upload_path'=>$path_upload,'allowed_types'=>$allowed_types);
						$upload_data = $this->upload_file($temp_data_array);
						//print_r($upload_data);
						//exit;
						if(isset($upload_data) && $upload_data !='' && count($upload_data) > 0 && isset($upload_data['status']) && $upload_data['status'] =='success')
						{
							$file_data = $upload_data['file_data'];
							$data_array[$key] = $file_data['file_name'];
							$data_file_old_array[] = $path_upload.$old_upload;
							$data_file_new_array[] = $path_upload.$file_data['file_name'];
						}
						else
						{
							$this->delete_file($data_file_new_array);
							$this->session->set_flashdata('error_message', $this->success_message['error_file']);
							return;
						}
					}
				}
			}
			// for check file upload
			// for request field and add
			if(isset($_REQUEST) && $_REQUEST !='' && count($_REQUEST) > 0)
			{
				foreach($_REQUEST as $key=>$val)
				{
					if(in_array($key,$table_field))
					{
						$data_array[$key] = $val;
					}
				}
			}
			// for request field and add
			if($data_array !='' && count($data_array) > 0)
			{				
				$response = $this->update_insert_data_common($table_name,$data_array,$where_arra,$flag_update,$limit);
				if($response)
				{
					$success_message = $this->success_message[$mode];
					$this->delete_file($data_file_old_array);
					$this->session->set_flashdata('success_message', $success_message);
				}
				else
				{
					$this->delete_file($data_file_new_array);
					$this->session->set_flashdata('error_message', $this->success_message['error']);
				}
			}
		}
		
		
		$is_ajax = 0;
		if($this->input->post('is_ajax'))
		{
			$is_ajax = $this->input->post('is_ajax');
		}
		if($is_ajax == 0)
		{
			if(isset($this->success_url) && $this->success_url !='')
			{
				redirect($this->base_url_admin.$this->class_name.'/'.$this->success_url);
			}
		}
		else
		{
			$data['data'] = $this->common_model->getjson_response();
			$this->load->view('common_file_echo',$data);
		}
	}
	public function upload_file($upload_data = '')
	{
		$return_message = '';
		if(isset($upload_data) && $upload_data !='' && count($upload_data) > 0 && isset($upload_data['upload_path']) && $upload_data['upload_path'] !='' && isset($upload_data['file_name']) && $upload_data['file_name'] !='')
		{			
			$config['upload_path']  = $upload_data['upload_path'];
			if(isset($upload_data['allowed_types']) && $upload_data['allowed_types'] !='')
			{
				$config['allowed_types']= $upload_data['allowed_types'];
			}
			if(isset($upload_data['max_size']) && $upload_data['max_size'] !='')
			{
				$config['max_size']= $upload_data['max_size'];
			}
			if(isset($upload_data['encrypt_name']) && $upload_data['encrypt_name'] !='')
			{
				$config['encrypt_name']= $upload_data['encrypt_name'];
			}
			else
			{
				$config['encrypt_name'] = TRUE;
			}
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload($upload_data['file_name']))
			{
				$return_message = array('status'=>'error','error_message'=>$this->upload->display_errors());
			}
			else
			{
				$return_message = array('status'=>'success','file_data'=>$this->upload->data());
			}
		}
		return $return_message;
	}
	public function delete_file($file_name='')
	{
		if(isset($file_name) && $file_name !='' && is_array($file_name))
		{
			foreach($file_name as $file_name_val)
			{
				if(isset($file_name_val) && $file_name_val !='' && file_exists($file_name_val))
				{
					@unlink($file_name_val);
				}	
			}
		}
		else if(isset($file_name) && $file_name !='' && file_exists($file_name))
		{
			@unlink($file_name);
		}
	}
	// for generate datatabel common
	
	public function rander_filed()
	{
		foreach($this->table_field as $temp_filed)
		{
			if(!in_array($temp_filed,$this->data_tabel_filedIgnore))
			{
				$this->data_tabel_filed[] = $temp_filed;
			}
		}
	}
	
	public function set_page_number()
	{
		if($this->input->post('page_number'))
		{
			$page_number = $this->input->post('page_number');
			$this->page_number = $page_number;
		}
	}
	public function set_limitper_page()
	{
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		else
		{
			$config_pag = $this->getconfingValue('config_pag');
			if(isset($config_pag['per_page']) && $config_pag['per_page'] !='')
			{
				$limit_per_page = $config_pag['per_page'];
			}
			else
			{
				$limit_per_page = 10;
			}
		}
		$this->limit_per_page = $limit_per_page;
	}
	public function add_limit_per_page()
	{
		$this->set_limitper_page();
		$this->set_page_number();
		if($this->page_number !='')
		{
			$this->start = (($this->page_number - 1) * $this->limit_per_page);
		}
		$this->db->limit($this->limit_per_page,$this->start);
	}
	public function add_where_rander()
	{
		if($this->input->post('search_filed'))
		{
			$search_filed = trim($this->input->post('search_filed'));
			$search_filed = $this->db->escape_str($search_filed);
			$this->search_filed = $search_filed;
			
			if($search_filed !='')
			{
				$temp_search = array();
				foreach($this->table_field as $temp_filed)
				{
					$temp_filed = $this->table_name_dot.$temp_filed;
					$temp_search[] = " $temp_filed like '%$search_filed%' ";
				}
				
				$join_tab_rel = $this->join_tab_array;
				if(isset($join_tab_rel) && $join_tab_rel !='' && count($join_tab_rel) > 0)
				{
					foreach($join_tab_rel as $join_tab_rel_val)
					{
						
						$temp_filed = $join_tab_rel_val['rel_table'].'.'.$join_tab_rel_val['rel_filed_disp'];
						$temp_search[] = " $temp_filed like '%$search_filed%' ";
						
						if(isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] !='' && count($join_tab_rel_val['rel_filed_search_disp']) > 0)
						{
							foreach($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val)
							{
								$temp_filed = $join_tab_rel_val['rel_table'].'.'.$rel_filed_search_disp_val;
								$temp_search[] = " $temp_filed like '%$search_filed%' ";
							}
						}
					}
				}
				
				if(isset($temp_search) && $temp_search !='' && count($temp_search)>0)
				{
					$temp_search_str = implode(" OR ",$temp_search);
					$this->db->where("( $temp_search_str )");
				}
			}
		}
		if($this->input->post('status_mode'))
		{
			$this->status_mode = trim($this->input->post('status_mode'));
		}
		if(isset($this->status_mode) && $this->status_mode !='' && $this->status_mode !='ALL' && $this->status_field !='')
		{
			$this->db->where($this->table_name_dot.$this->status_field,$this->status_mode);
		}
	}
	public function rander_data()
	{
		$this->add_where_rander();
		$this->add_limit_per_page();
		$this->setJoinTable();
		
		$select_field = $this->table_name_dot.'*';
		$temp_select_join_filed = array();
		$temp_select_join_filed[] = $select_field;
		$join_tab_rel = $this->join_tab_array;
		if(isset($join_tab_rel) && $join_tab_rel !='' && count($join_tab_rel) > 0)
		{
			foreach($join_tab_rel as $join_tab_rel_val)
			{
				$temp_select_join_filed[] = $join_tab_rel_val['rel_table'].'.'.$join_tab_rel_val['rel_filed_disp'];
				if(isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] !='' && count($join_tab_rel_val['rel_filed_search_disp']) > 0)
				{
					foreach($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val)
					{
						$temp_select_join_filed[] = $join_tab_rel_val['rel_table'].'.'.$rel_filed_search_disp_val;
					}
				}
			}
		}
		if(isset($temp_select_join_filed) && $temp_select_join_filed !='' && count($temp_select_join_filed) > 0)
		{
			$select_field = implode(',',$temp_select_join_filed);
		}
		$row_data = $this->get_count_data_manual($this->table_name,'',2,$select_field);
		//echo $this->last_query();
		
		if(isset($row_data) && $row_data !='' && count($row_data) > 0)
		{
			$this->data_tabel_data = $row_data;
		}
	}
	public function rander_data_all_count()
	{
		$this->setJoinTable();
		$data_tabel_all_count = $this->get_count_data_manual($this->table_name,'',0,$this->table_name_dot.$this->primary_key);
		//echo $this->last_query();
		if(isset($data_tabel_all_count) && $data_tabel_all_count !='' && $data_tabel_all_count > 0)
		{
			$this->data_tabel_all_count = $data_tabel_all_count;
		}
	}
	public function rander_data_filtered_count()
	{
		$this->add_where_rander();
		$this->setJoinTable();
		$row_data_count = $this->get_count_data_manual($this->table_name,'',0,$this->table_name_dot.$this->primary_key);
		//echo $this->last_query();
		if(isset($row_data_count) && $row_data_count !='' && $row_data_count > 0)
		{
			$this->data_tabel_filtered_count = $row_data_count;
		}
	}
	public function rander_pagination()
	{
		$this->load->library('pagination');
		$config = $this->getconfingValue('config_pag');
		$config['base_url'] = $this->base_url_admin_cm_status;
		$config['per_page'] = $this->limit_per_page;
		$config['total_rows'] = $this->data_tabel_filtered_count;
		$this->pagination->initialize($config);
		$this->pagination_link = $this->pagination->create_links();
	}
	public function rander_order_sort()
	{
		$order = 'ASC';
		$data_table_parameter = $this->data_table_parameter;
		
		if($this->input->post('default_sort'))
		{
			$default_sort = trim($this->input->post('default_sort'));
			if(isset($default_sort) && $default_sort !='')
			{
				$data_table_parameter['default_sort'] = $default_sort;
			}
		}		
		if($this->input->post('default_order'))
		{
			$default_order = trim($this->input->post('default_order'));
			if(isset($default_order) && $default_order !='')
			{
				$data_table_parameter['default_order'] = $default_order;
			}
		}
		if(isset($data_table_parameter['default_sort']) && $data_table_parameter['default_sort'] !='')
		{
			if(isset($data_table_parameter['default_order']) && $data_table_parameter ['default_order'] !='')
			{
				$order = $data_table_parameter['default_order'];
			}
			else
			{
				$data_table_parameter['default_order'] = $order;
			}
		}
		else
		{
			$data_table_parameter['default_sort'] = $this->primary_key;
		}
		$this->data_table_parameter = $data_table_parameter;
		if(isset($data_table_parameter['default_order']) && $data_table_parameter['default_order'] !='' && $order !='')
		{
			$join_tab_rel = $this->join_tab_array;
			$order_from_main_table = 1;
			if(isset($join_tab_rel) && $join_tab_rel !='' && count($join_tab_rel) > 0)
			{
				foreach($join_tab_rel as $join_tab_rel_val)
				{
					if(isset($join_tab_rel_val['rel_filed_disp']) && $join_tab_rel_val['rel_filed_disp'] !='' && $join_tab_rel_val['rel_filed_disp'] == $data_table_parameter['default_sort'])
					{
						$order_from_main_table = 0;
						$this->db->order_by($join_tab_rel_val['rel_table'].'.'.$join_tab_rel_val['rel_filed_disp'], $order);
						break;
					}
					if(isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] !='' && count($join_tab_rel_val['rel_filed_search_disp']) > 0)
					{
						foreach($join_tab_rel_val['rel_filed_search_disp'] as $rel_filed_search_disp_val)
						{
							if(isset($rel_filed_search_disp_val) && $rel_filed_search_disp_val !='' && $rel_filed_search_disp_val == $data_table_parameter['default_sort'])
							{
								$order_from_main_table = 0;
								$this->db->order_by($join_tab_rel_val['rel_table'].'.'.$rel_filed_search_disp_val, $order);
								break;
							}
						}
						if($order_from_main_table == 0)
						{
							break;
						}
					}
				}
			}
			
			if($order_from_main_table == 1 )
			{
				$this->db->order_by($this->table_name_dot.$data_table_parameter['default_sort'], $order);
			}
		}
	}
	function setJoinTable()
	{
		$join_tab_rel = $this->join_tab_array;
		if(isset($join_tab_rel) && $join_tab_rel !='' && count($join_tab_rel) > 0)
		{
			foreach($join_tab_rel as $join_tab_rel_val)
			{
				$default_join = 'left';
				if(isset($join_tab_rel_val['join_type']) && $join_tab_rel_val['join_type'] !='')
				{
					$default_join = $join_tab_rel_val['join_type'];
				}
				if(isset($join_tab_rel_val['join_manual']) && $join_tab_rel_val['join_manual'] !='' )
				{
					$this->db->join($join_tab_rel_val['rel_table'],$join_tab_rel_val['join_manual'], $default_join);
				}
				else if(isset($join_tab_rel_val['rel_table']) && $join_tab_rel_val['rel_table'] !='' && isset($join_tab_rel_val['rel_filed']) && $join_tab_rel_val['rel_filed'] !='' && isset($join_tab_rel_val['rel_filed_org']) && $join_tab_rel_val['rel_filed_org'] !='')
				{
					$this->db->join($join_tab_rel_val['rel_table'], $join_tab_rel_val['rel_table'].'.'.$join_tab_rel_val['rel_filed']." = ".$this->table_name_dot.$join_tab_rel_val['rel_filed_org'], $default_join);
				}
			}
		}
	}
	public function rander_dataTable($page='1',$parameter = '')
	{
		if($parameter !='')
		{
			$this->data_table_parameter = $parameter;
		}		
		$this->page_number = $page;
		$this->rander_filed();
		$this->rander_order_sort();
		$this->rander_data();
		
			// $this->last_query();
			
		$this->rander_data_filtered_count();
		
		$this->rander_pagination();
		$data[] = $this->data;
		$this->rander_data_all_count();
		$this->load->view('back_end/data_table',$data);
	}
	// for generate datatabel common
	public function update_status_check()
	{
		if($this->input->post('status_update'))
		{
			$status_update = $this->input->post('status_update');
			if($status_update !='')
			{
				$this->save_update_status();
			}
		}
	}
	public function save_update_status()
	{
		if($this->input->post('status_update') && $this->input->post('selected_val'))
		{
			$status_update = trim($this->input->post('status_update'));
			$selected_val = $this->input->post('selected_val');
			if($status_update !='' && $selected_val !='' && count($selected_val) > 0 )
			{
				$this->db->where_in($this->primary_key, $selected_val);
				if($status_update =='DELETE')
				{
					$response = $this->data_delete_common($this->table_name);
					$mode= 'delete';
				}
				else
				{
					if(in_array($status_update,$this->status_arr))
					{
						$data_array = array($this->status_column=>$status_update);
						$response = $this->update_insert_data_common($this->table_name,$data_array,'',1,0);
						$mode= 'edit';
					}
				}
				if($response)
				{
					$success_message = $this->success_message[$mode];
					$this->session->set_flashdata('success_message', $success_message);
				}
				else
				{
					$this->session->set_flashdata('error_message', $this->success_message['error']);
				}
				//else if()
			}
		}
		
	}
	
	public function add_data_common($ele_array='',$mode = 'add', $id='')
	{
		$other_config = array('mode'=>$mode,'id'=>$id); // define here some parameter form generate
		return $this->generate_form_main($ele_array,$other_config);
	}
	public function addJoin_tab_arr($join_tab_rel='')
	{
		if($join_tab_rel !='')
		{
			$this->join_tab_array = $join_tab_rel;
			foreach($join_tab_rel as $join_tab_rel_val)
			{
				if(isset($join_tab_rel_val['rel_filed_org']) && $join_tab_rel_val['rel_filed_org'] !='')
				{
					$this->filed_notdisp[] = $join_tab_rel_val['rel_filed_org'];
				}
				if(isset($join_tab_rel_val['rel_filed_disp']) && $join_tab_rel_val['rel_filed_disp'] !='')
				{
					$this->join_tab_array_filed_disp[] = $join_tab_rel_val['rel_filed_disp'];
				}
				if(isset($join_tab_rel_val['rel_filed_search_disp']) && $join_tab_rel_val['rel_filed_search_disp'] !='')
				{
					$rel_filed_search_disp = $join_tab_rel_val['rel_filed_search_disp'];
					foreach($rel_filed_search_disp as $rel_filed_search_disp_val)
					{
						$this->join_tab_array_filed_disp[] = $rel_filed_search_disp_val;
					}
				}
			}
		}
	}
	public function common_rander($table_name='',$status ='ALL', $page =1,$label_page='',$ele_array='',$sort_column='',$addPopup=0,$join_tab_rel = '',$other_config='')
	{
		// here common and imported setting
			$this->table_name = $table_name; 	// *need to set here tabel name //
			$this->set_table_name($this->table_name);
			$this->label_page = $label_page;
		// here common and imported setting

		if(isset($status) && $status == 'save-data')
		{
			$this->save_update_data();
		}
		else if(isset($status) && ($status == 'add-data' || $status == 'edit-data'))
		{
			$id='';
			$mode ='add';
			if($status == 'edit-data')
			{
				$id = $page;
				$mode ='edit';
			}
			$this->data['data'] = $this->add_data_common($ele_array,$mode,$id);
			$this->__load_header(ucfirst($mode).' '.$this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->__load_footer();
		}
		else
		{
		// for setting join table
			if(isset($join_tab_rel) && $join_tab_rel !='')
			{
				$this->addJoin_tab_arr($join_tab_rel);
			}
		// for setting join table
			$is_ajax = 0;
			if($this->input->post('is_ajax'))
			{
				$is_ajax = $this->input->post('is_ajax');
			}

			$this->update_status_check();
			
			if($is_ajax == 0)
			{
				$this->__load_header('Manage '.$this->label_page,$status);
			}
			
			$this->update_status_var($status);
			
			if($addPopup == 1)
			{
				$this->addPopup = 1; // for display pop up
			}
			
			$default_order = 'ASC';
			if(isset($other_config['default_order']) && $other_config['default_order'] !='')
			{
				$default_order = $other_config['default_order'];
			}
			if(isset($other_config['filed_notdisp']) && $other_config['filed_notdisp'] !='')
			{
				foreach($other_config['filed_notdisp'] as $filed_notdisp)
				$this->filed_notdisp[] = $filed_notdisp;
			}
			
			$parameter = array('default_sort'=>$sort_column,'default_order'=>$default_order); // set default sort coloumn name
			
			$this->rander_dataTable($page,$parameter);
			
			if($is_ajax == 0)
			{
				$this->label_col = 3;	 // for pop up
				$model_body = '';
				if($addPopup == 1)
				{
					$model_body = $this->add_data_common($ele_array); // for pop up
				}
				$this->__load_footer($model_body);
			}
		}
	}

}