<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cms extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function index($cms_name = '')
	{
		$this->common_front_model->set_orgin();  
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('cms_name'))
		{
			$cms_name = $this->input->post('cms_name');
		}
		$where_arra = array('page_url'=>$cms_name,'status'=>'APPROVED','is_deleted'=>'No');
		$cms_data = $this->common_front_model->get_count_data_manual('cms_pages',$where_arra,1,'page_title,page_content');
		if($user_agent == 'NI-WEB')
		{
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$this->data['cms_data'] = $cms_data;
				$page_title = $cms_data['page_title'];
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/cms_view',$this->data);
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/404_view',$this->data);
			}
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_footer_employer();
			}
			else
			{
				$this->common_front_model->__load_footer();
			}
			
			
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			/*$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);*/
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}
?>