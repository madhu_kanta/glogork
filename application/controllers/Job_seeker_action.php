<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Job_seeker_action extends CI_Controller
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['base_url'] = $this->base_url = base_url();
		$this->load->model('front_end/job_seeker_action_model');
	}
	
	function seeker_action()
	{
		$this->common_front_model->set_orgin(); 
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='')
		{
			$response = $this->job_seeker_action_model->seeker_action();
			if($response == 'success')
			{
				$msg_array = array('like_job'=>$this->lang->line('js_like_job'),'unlike_job'=>$this->lang->line('js_unlike_job'),'save_job'=>$this->lang->line('js_save_job'),'remove_save_job'=>$this->lang->line('js_remove_save_job'),'block_emp'=>$this->lang->line('js_block_emp'),'unblock_emp'=>$this->lang->line('js_unblock_emp'),'follow_emp'=>$this->lang->line('js_follow_emp'),'unfollow_emp'=>$this->lang->line('js_unfollow_emp'),'like_emp'=>$this->lang->line('js_like_emp'),'unlike_emp'=>$this->lang->line('js_unlike_emp'),'delete'=>$this->lang->line('js_delete_activity') );
				
				$data['errmessage'] = $msg_array[$action];
				$data['status'] = 'success';
				
			}
			else
			{/*$this->lang->line('edit_job_success_msg')*/
				$data['status'] = 'error';
				$data['errmessage'] = $this->lang->line('js_action_err_msg');
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	function send_message()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='send_message')
		{
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				// $subject = $this->input->post('subject');			   
			    // $content = $this->input->post('content');
				 $this->load->library('form_validation');
				 $this->form_validation->set_rules('subject', 'Subject ', 'required');
			     $this->form_validation->set_rules('content', "Message contect ", 'required');
			}
			if($user_agent=='NI-WEB'  &&  $this->form_validation->run() == FALSE)
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->job_seeker_action_model->send_message();
				if($response == 'success')
				{
					$data['errmessage'] =  $this->lang->line('send_mesage_success_msg');
				    $data['status'] =  'success';
				}
				elseif($response == 'error_plan')
				{
					$data['errmessage'] =  'Sorry ! You do not have credit left to message.';
				    $data['status'] =  'error';
				}
				else
				{
					$data['errmessage'] =  $this->lang->line('js_action_err_msg');
				    $data['status'] =  'error';
				}
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function apply_for_job()
	{
		$user_agent = $this->input->post('user_agent');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='')
		{
			$response = $this->job_seeker_action_model->apply_for_job();
			
				if(isset($response['status'])  && $response['status'] == 'success')
				{
					$data['errmessage'] =  $response['errmessage'];
				    $data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] =  $response['errmessage'];
				    $data['status'] =  'error';
				}
		/*$response = $this->job_seeker_action_model->apply_for_job();
			if($response == 'success')
			{
				$data['errmessage'] =  $this->lang->line('apply_job_success_msg');
				$data['status'] =  'success';
			}
			else
			{
				$data['errmessage'] =  $this->lang->line('apply_job_err_msg');
				$data['status'] =  'error';
			}*/
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	} 
	
	function js_activity_list($page=1,$get_list='')
	{
		$this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$get_list = base64_decode($get_list);
		if($user_agent=='NI-WEB')
			{
				$get_list_data = array('js_activity_list'=>$get_list);
				if((!$this->session->userdata('js_activity_list') && $this->session->userdata('js_activity_list')=='') || ($this->session->userdata('js_activity_list') && $this->session->userdata('js_activity_list')!=$get_list && $get_list!=''))
				{
					$this->session->set_userdata($get_list_data);
				}
				
				$get_list = $this->session->userdata('js_activity_list');
			}
		$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;		
		$page_title = $this->lang->line('js_activity_title');
		if($this->ajax_search == 0)
		{
			$page = base64_decode($page);
		}
		
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('get_list'))
		{
			$get_list = $this->input->post('get_list');
		}
		
		
		if(is_numeric($page) && $page!='' && $get_list!='')
		{
			//$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
			$js_action_details = $this->job_seeker_action_model->js_activity_list($page,$limit='',$get_list);
			//print_r($job_dtails); exit();
			if($js_action_details['status'] == 'success')
			{
				$this->data['activity_list_data'] = $js_action_details['activity_list_data'];
				$this->data['activity_list_count'] = $js_action_details['activity_list_count'];
				$this->data['get_list'] = $get_list;
				$this->data['custom_lable'] = $this->lang;
				$this->data['status'] = 'success';
				if($user_agent == 'NI-WEB')
				{
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header($page_title);
							$this->load->view('front_end/js_job_activity_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/js_job_activity_result_view',$this->data);
						}
					
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer();
						}
				}
				
			}
			else
			{
				$this->data['status'] = 'error';
				$this->common_front_model->__load_header($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			$this->data['status'] = 'error';
			$this->common_front_model->__load_header($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer();
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['status'] = 'error';
			if($js_action_details['activity_list_count']!='' && $js_action_details['activity_list_count'] > 0 )
			{
				$data['status'] = 'success';
			}
			$data['comp_img'] = 'assets/company_logos';
			$data['activity_list_count'] = $js_action_details['activity_list_count'];
			//$data['activity_list_data'] = $js_action_details['activity_list_data'];
			$data['activity_list_data'] = $this->job_seeker_action_model->process_js_activity_list($js_action_details['activity_list_data']);
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
		
	}
	
	function js_activity_list_emp($page=1,$get_list='')
	{
		    $this->common_front_model->set_orgin();  
			$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;	
		    
			if($user_agent == 'NI-WEB')
			{
				$get_list = base64_decode($get_list);
				$get_list_data = array('js_activity_list_emp'=>$get_list);
				if((!$this->session->userdata('js_activity_list_emp') && $this->session->userdata('js_activity_list_emp')=='') || ($this->session->userdata('js_activity_list_emp') && $this->session->userdata('js_activity_list_emp')!=$get_list && $get_list!=''))
				{
					$this->session->set_userdata($get_list_data);
				}
				
				$get_list = $this->session->userdata('js_activity_list_emp');
				$page_title = $this->lang->line('js_activity_title');
			}
			$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
			if($this->ajax_search == 0)
			{
				$page = base64_decode($page);
			}
			
			if($this->input->post('page'))
			{
				$page = $this->input->post('page');
			}
		
			
			if($this->input->post('get_list'))
			{
				$get_list = $this->input->post('get_list');
			}
		
		if(is_numeric($page) && $page!='' && $get_list!='')
		{
			
			$js_action_details = $this->job_seeker_action_model->js_activity_list_emp($page,$limit='',$get_list);
			//print_r($job_dtails); exit();
			if($js_action_details['status'] == 'success')
			{
				$this->data['activity_list_data'] = $js_action_details['activity_list_data'];
				$this->data['activity_list_count'] = $js_action_details['activity_list_count'];
				$this->data['get_list'] = $get_list;
				$this->data['custom_lable'] = $this->lang;
				$this->data['status'] = 'success';
				if($user_agent == 'NI-WEB')
				{
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header($page_title);
							$this->load->view('front_end/js_emp_activity_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/js_emp_activity_result_view',$this->data);
						}
					
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				$this->common_front_model->__load_header($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			$this->data['status'] = 'error';
			$this->common_front_model->__load_header($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer();
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['status'] = 'error';
			if($js_action_details['activity_list_count']!='' && $js_action_details['activity_list_count'] > 0)
			{
				$data['status'] = 'success';
			}
			$data['comp_img'] = 'assets/company_logos';
			$data['activity_list_count'] = $js_action_details['activity_list_count'];
			$data['activity_list_data'] = $js_action_details['activity_list_data'];
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
	}
	
	function js_apply_job_list($page=1)
	{
		$this->common_front_model->set_orgin();   
	    $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
		$page_title = $this->lang->line('js_activity_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		
	  if(is_numeric($page) && $page!='')
		{
			$js_action_details = $this->job_seeker_action_model->js_apply_job_list($page,$limit='');
			if($js_action_details['status'] == 'success')
			{
				$this->data['apply_list_data'] = $js_action_details['apply_list_data'];
				$this->data['apply_list_count'] = $js_action_details['apply_list_count'];
				$this->data['custom_lable'] = $this->lang;
				$this->data['status'] = 'success';
				if($user_agent == 'NI-WEB')
				{
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header($page_title);
							$this->load->view('front_end/js_apply_job_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/js_apply_job_result_view',$this->data);
						}
					
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				$this->common_front_model->__load_header($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			$this->data['status'] = 'error';
			$this->common_front_model->__load_header($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer();
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['status'] = 'error';
			if($js_action_details['apply_list_count']!='' && $js_action_details['apply_list_count'] > 0)
			{
				$data['status'] = 'success';
			}
			$data['company_logo_img'] = 'assets/company_logos';
			$data['apply_list_count'] = $js_action_details['apply_list_count'];
			//$data['apply_list_data'] = $js_action_details['apply_list_data'];
			$data['apply_list_data'] = $this->job_seeker_action_model->process_js_activity_list($js_action_details['apply_list_data']);
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
	}
	
	function view_emp_details()
	{
		    $user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
			$get_emp_data = $this->job_seeker_action_model->view_emp_details();
			$this->data['user_data'] =  $get_emp_data;
			$this->data['status'] =  $get_emp_data['status'];
			if($user_agent == 'NI-WEB')
				{
					if($get_emp_data['status'] == 'success')
					{
						$this->load->view('front_end/page_part/emp_profile_for_js_view',$this->data);
					}
					else
					{
						$this->load->view('front_end/404_view',$this->data);
					}
				}
				else
				{
					$this->data['user_data'] =  $get_emp_data['emp_data'];
					$this->data['company_logo_img'] = 'assets/company_logos';
					$this->output->set_content_type('application/json');
		   			$this->output->set_output(json_encode($data));
				}
	
	}
}
?>