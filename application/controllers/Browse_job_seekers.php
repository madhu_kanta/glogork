<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Browse_job_seekers extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->ajax_search = 0;
		$this->data['base_url'] = $this->base_url;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->load->model('front_end/our_jobseeker_model');
		//$this->load->model('front_end/my_profile_model');	
	}
	
	public function index($page=1)
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 6;//$this->common_front_model->limit_per_page;
		$this->data['limit_per_page'] = $limit_per_page;
		$jobseekers_count = $this->our_jobseeker_model->getactivejobseeker_count();
		$this->data['jobseekers_count'] = ($jobseekers_count!='' && $jobseekers_count!=0 && $jobseekers_count > 0 ) ? $jobseekers_count : 0;
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		if($user_agent == 'NI-WEB')
		{
			if($jobseekers_count!='' && $jobseekers_count > 0)
			{
				$req_field = array('id,email,fullname,personal_titles
,industries_name,industry,profile_pic,profile_pic_approval,register_date,city,city_name,total_experience');
				$jobseeker_data = $this->our_jobseeker_model->getjobseekers($page,$limit_per_page,$req_field);
				$this->data['jobseeker_data'] = $jobseeker_data;
				$page_title = "Listed Jobseekers";//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					if($this->common_front_model->checkLoginfrontempl())
					{
						$this->common_front_model->__load_header_employer($page_title);
					}
					else
					{
						$this->common_front_model->__load_header($page_title);
					}
					$this->load->view('front_end/jobseekers_list',$this->data);
				}
				else
				{
					$this->load->view('front_end/jobseekers_list_ajax',$this->data);
				}	
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_footer_employer();
				}
				else
				{
					$this->common_front_model->__load_footer();
				}
			}
		}
		else
		{
			//for user agent androif and ohter
		}
	}
	public function companies($page=1)
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 6;
		$this->data['limit_per_page'] = $limit_per_page;
		$companies_count = $this->our_recruiters_model->getcompaniess_count();
		$this->data['companies_count'] = ($companies_count!='' && $companies_count!=0 && $companies_count > 0 ) ? $companies_count : 0;
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		if($user_agent == 'NI-WEB')
		{
			if($companies_count!='' && $companies_count > 0)
			{
				$req_field = array('id,email,fullname,personal_titles,industries_name,industry,company_name,skill_hire,register_date,company_logo,company_logo_approval,city,city_name');
				$companies_data = $this->our_recruiters_model->getcompanies($page,$limit_per_page,$req_field);
				$this->data['companies_data'] = $companies_data;
				$page_title = "Our Companies";//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					if($this->common_front_model->checkLoginfrontempl())
					{
						$this->common_front_model->__load_header_employer($page_title);
					}
					else
					{
						$this->common_front_model->__load_header($page_title);
					}
					$this->load->view('front_end/companies_list',$this->data);
				}
				else
				{
					$this->load->view('front_end/companies_list_ajax',$this->data);
				}	
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_footer_employer();
				}
				else
				{
					$this->common_front_model->__load_footer();
				}
			}
		}
		else
		{
			//for user agent androif and ohter
		}
	}
	public function company_jobs($employer_id='',$page=1)
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 3;
		$this->data['limit_per_page'] = $limit_per_page;

		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		if($this->input->post('emp_id'))
		{
			$employer_id = $this->input->post('emp_id');
		}
		if($employer_id!='')
		{
			$employer_id = base64_decode($employer_id);
		}
		$company_job_count = $this->our_recruiters_model->getrecruitersalljob_count($employer_id);
		$this->data['total_post_count'] = ($company_job_count!='' && $company_job_count!=0 && $company_job_count > 0 ) ? $company_job_count : 0;
		if($user_agent == 'NI-WEB')
		{
			if($company_job_count!='' && $company_job_count > 0)
			{
				//$req_field = array('id,email,fullname,personal_titles,industries_name,industry,company_name,skill_hire,register_date,company_logo,company_logo_approval,city,city_name');
				$req_field = array();
				$emp_posted_job = $this->our_recruiters_model->getrecruitersalljob($employer_id,$page,$limit_per_page,$req_field);
				$this->data['emp_posted_job'] = $emp_posted_job;
				$this->data['company_id_pass'] = $employer_id;
				//$page_title = $emp_posted_job['company_name'];//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					/*$this->common_front_model->__load_header($page_title);
					$this->load->view('front_end/companies_list',$this->data);*/
				}
				else
				{
					$this->load->view('front_end/page_part/job_search_result_view_company',$this->data);
				}	
			}
			else
			{
				/*$page_title = 'Data Not found';
				$this->common_front_model->__load_header($page_title);*/
				//$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_footer_employer();
				}
				else
				{
					$this->common_front_model->__load_footer();
				}
			}
		}
		else
		{
			//for user agent androif and ohter
		}
	}
	public function company($company_id = '')
	{
		$company_id = base64_decode($company_id);
		$page_title = $this->lang->line('compnay_recruter_det_pg_tit');
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		$companydata = $this->our_recruiters_model->view_company_details($company_id);
		if($user_agent == 'NI-WEB')
		{
			if($companydata !='' && is_array($companydata) && count($companydata) > 0)
			{
				$this->data['companydata'] = $companydata;
				$page_title = ($this->common_front_model->checkfieldnotnull($companydata['company_name'])) ? $companydata['company_name'] :  $this->data['custom_lable']->language['compnay_recruter_det_pg_tit'];
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/company_details',$this->data);
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/404_view',$this->data);
			}
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_footer_employer();
				}
				else
				{
					$this->common_front_model->__load_footer();
				}
		}
		else
		{
			/*
			$parampass = array('blog_image'=>'assets/blog_image');
			$blogdata = $this->common_front_model->dataimage_fullurl($blogdata,$parampass,'single');
			$data_return['data']   = $blogdata;
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);*/
		}
	}
	/*function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}*/
}