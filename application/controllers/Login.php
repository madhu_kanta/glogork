<?php header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST");
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('front_end/login_model');
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	
	public function facebook_gplus_login_for_android()
	{
		$response = $this->login_model->facebook_gplus_login_for_android();
		$response['data'] = $response; 
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	public function login()
	{
		//$this->common_front_model->set_orgin();
		$captcha_flag =0;
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		$data['user_agent'] = $user_agent;
		$login_attepmt_value = $this->input->post('login_attepmt_value');
		if($user_agent!='' && $action=='login')
		{ 
		    $this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				//|| ($this->session->userdata('captcha_header_login') && $this->session->userdata('captcha_header_login')!='' ) 
				if(($this->session->userdata('captcha_main_login') && $this->session->userdata('captcha_main_login')!='' ) && $login_attepmt_value >=2)
				{
					
					if($this->input->post('validate_captcha') && $this->input->post('validate_captcha')!='')
					{
						$captcha_list_main = $this->session->userdata('captcha_main_login');
						if($this->input->post('validate_captcha') != $captcha_list_main[0] + $captcha_list_main[1] )
						{
						  $captcha_flag = 1;
						}
					}
					/*else
					{
						if($this->input->post('validate_captcha_header') && $this->input->post('validate_captcha_header')!='')
						
						{
							$captcha_header_login = $this->session->userdata('captcha_header_login');
							if($this->input->post('validate_captcha_header') != $captcha_header_login[0] + $captcha_header_login[1] )
							{
							  $captcha_flag = 1;
							}
						}
					}*/
				}
				$username = $this->input->post('username');
			    $password = $this->input->post('password');
				$this->form_validation->set_rules('username', 'Username ', 'required');
			    $this->form_validation->set_rules('password', "Password ", 'required');
			}
			
			if(($user_agent=='NI-WEB' && $this->form_validation->run() == FALSE ) || ($user_agent=='NI-WEB' && $captcha_flag ==1))
			{
				$data['errmessage'] = validation_errors();
				if($captcha_flag == 1)
				{
					$data['errmessage']  = $this->lang->line('validate_captcha');
				}
				
				$data['status'] =  'error';
			}
			else
			{
				$this->common_front_model->set_orgin();
				$response = $this->login_model->login();
				$data['login_attempt'] = $response['login_attempt'];
				if($response['status'] == 'success')
				{
					$data['errmessage'] = $response['errmessage'];
					$data['status'] =  'success';
					$data['login_user_id'] = $this->common_front_model->get_userid();
				}
				else
				{
					$data['errmessage'] =  $response['errmessage'];
					$data['status'] =  'error';
				}	
			}
		}
		else
		{
			$data['errmessage'] = 'abcdef'. $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		
		//header('Content-type: application/json');
		$this->output->set_content_type('application/json');
		//$data1['data'] = json_encode($data);
		$this->output->set_output(json_encode($data));
		//$this->load->view('common_file_echo',$data1);
	}
	public function verify_member($cpass='',$email='')
	{ 
		$this->login_model->verify_member($cpass,$email);
	}
	public function forgot_password()
	{
		$this->common_front_model->__load_header("Forgot password");
		$this->load->view('front_end/forgot_password',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	public function restetpasstonew_js($cpass='',$email='')
	{
	    $this->data['email_get'] = $email;
		$this->data['query_get'] = $cpass;
		$this->common_front_model->__load_header($this->lang->line('reset_passtit_js'));
		$this->load->view('front_end/reset_password_js',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	public function reset_forgot_password()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='forgot_password')
		{
			$this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email',
					array('is_unique' => $this->lang->line('email_exits'))
					);
			}
			
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->login_model->reset_forgot_password();
				$response = json_decode($response);
				if($response->status == 'success')
				{
					$data['errmessage'] =  $response->errmessage;
					$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] = $response->errmessage;
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] =  $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		//$data1['data'] = json_encode($data);
		//$this->load->view('common_file_echo',$data1);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function reset_new_password($cpass='',$email='')
	{
		
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='reset_password')
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'pass_confirmation', 
                     'label'   => $this->data['custom_lable']->language['change_pass_lbl'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'pass', 
                     'label'   => $this->data['custom_lable']->language['change_pass_confirm_lbl'],
                     'rules'   => 'trim|required|matches[pass_confirmation]'
                  )
            );
			
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->login_model->reset_new_password($cpass,$email);
				if($response['status']== 'success')
				{
					$data['errmessage'] =  $response['errmessage'];
					$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] = $response['errmessage'];
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->data['custom_lable']->language['Unauthorized_Access'];
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function log_out()
	{
		if($this->session->userdata('jobportal_user'))
		{
			$this->session->unset_userdata('jobportal_user');
		}
		if($this->session->userdata('return_after_login_url'))
		{
			$this->session->unset_userdata('return_after_login_url');
		}
		if($this->session->userdata('js_fb_data'))
		{
			$this->session->unset_userdata('js_fb_data');
		}
		if($this->session->userdata('js_gplus_data'))
		{
			$this->session->unset_userdata('js_gplus_data');
		}
		if($this->session->userdata('js_gplus_data'))
		{
			$this->session->unset_userdata('js_gplus_data');
		}
		redirect($this->common_front_model->base_url);
	}
}