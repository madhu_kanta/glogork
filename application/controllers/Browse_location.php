<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Browse_location extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->ajax_search = 0;
		$this->data['base_url'] = $this->base_url;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->load->model('front_end/locationwise_listing_model');
		$this->load->model('front_end/our_recruiters_model');
	}
	public function index($page=1)
	{
		$this->common_front_model->set_orgin();
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$all_city_id_stored = "";
		$city_count = 0;
		$city_data = array();
		$test_count = $this->locationwise_listing_model->getallcitywherejobposted();
		
		if(isset($test_count) && $test_count !='' && is_array($test_count) && count($test_count) > 0)
		{
			foreach($test_count as $single_city)
			{
				if(is_array($single_city) && count($single_city) > 0 && $single_city['location_hiring'] != '')
				{
					/*echo "<pre>";*/
					if($all_city_id_stored!='')
					{
						$all_city_id_stored .= ','.$single_city['location_hiring'];	
					}
					else
					{
						$all_city_id_stored .= $single_city['location_hiring'];	
					}
					
					/*print_r($single_city).'<br>';	
					echo "</pre>";*/
				}
			}
		}
		
		if($all_city_id_stored !='')
		{
			$all_city_id_stored_arr = explode(',',$all_city_id_stored);
			$all_city_id_stored_arr_uni = array_unique($all_city_id_stored_arr);
			if(is_array($all_city_id_stored_arr_uni) && count($all_city_id_stored_arr_uni) > 0)
			{
				$city_count = count($all_city_id_stored_arr_uni);
				if($city_count!='' && $city_count > 0 )
				{
					foreach($all_city_id_stored_arr_uni as $all_city_id_stored_arr_uni_single)
					{
						$city_data_ret = $this->locationwise_listing_model->getnameidandpostjobcount($all_city_id_stored_arr_uni_single);
						if(is_array($city_data_ret) && count($city_data_ret) > 0)
						{
							$city_data[] = $city_data_ret;
						}
						/*echo "<pre>";
						print_r($city_data_ret);
						echo "</pre>";*/
					}
				}
				
			}
			else
			{
				$city_count = $this->locationwise_listing_model->getcity_count();
				if($city_count!='' && $city_count > 0)
				{
					$req_field = array('city_name,id');
					$city_data = $this->locationwise_listing_model->getcitydata($where_arra,$page,$limit_per_page,$req_field);	
				}
			}
		}
		/*echo "<pre>";
		print_r($all_city_id_stored_arr_uni);
		echo "</pre>";*/
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 24;
		$this->data['limit_per_page'] = $limit_per_page;
		//$city_count = $this->locationwise_listing_model->getcity_count();
		$this->data['city_count'] = ($city_count!='' && $city_count!=0 && $city_count > 0 ) ? $city_count : 0;
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		if($user_agent == 'NI-WEB')
		{
			if($city_count!='' && $city_count > 0)
			{
				
				$this->data['city_data'] = $city_data;
				$page_title = "Browse Location";//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					$this->common_front_model->__load_header($page_title);
					$this->load->view('front_end/browse_location_view',$this->data);
				}
				else
				{
					$this->load->view('front_end/location_all_result',$this->data);
				}
				
			}
			else
			{
				$page_title = 'Page Not found';
				$this->common_front_model->__load_header($page_title);
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			//for user agent androif and ohter
			$return_var['data'] = array();
			$return_var['total_location_having_active_job_count'] = $city_count;
			$return_var['tocken'] = $this->security->get_csrf_hash();
			if($city_count!='' && $city_count > 0)
			{
				$return_var['data'] = $city_data;
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($return_var));
		}
	}
	
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}