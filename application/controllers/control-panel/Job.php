<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
		$this->load->model('back_end/Job_model','job_model');
	}
	public function index()
	{
		$this->job_list();
	}
	public function job_list($status ='ALL', $page =1,$clear_search='no')
	{
		if($clear_search =='yes')
		{
			$this->clear_filter('no');
		}
		$this->job_model->job_list_model($status,$page);
	}
	public function job_detail($id = '',$mode='view')
	{
		if($id =='')
		{
			redirect($this->common_model->base_url_admin.'job/job-list');
			exit;
		}
		
		$this->common_model->__load_header('Job Detail');
		$this->data = $this->common_model->data;
		$this->data['view_edit_mode'] = $mode;
		$this->data['job_data'] = $this->job_model->get_data($id);
		$this->data['id'] = $id;
		$this->load->view('back_end/job_detail',$this->data);
		$this->common_model->__load_footer('');
	}
	public function save_new_job()
	{
		$id = $this->job_model->save_new_job();
		if($id !='')
		{
			redirect($this->common_model->base_url_admin.'job/job_detail/'.$id);
		}
		else
		{
			redirect($this->common_model->base_url_admin.'job/job-list');
		}
	}
	public function view_detail($id = '',$mode='',$disp_mode = 'view')
	{
		if($id !='' && $mode != '')
		{
			$data['id'] = $id;
			$data['disp_mode'] = $disp_mode;
			$this->load->view('back_end/job_'.$mode,$data);
		}
	}

	public function save_detail($id = '',$mode='')
	{
		if($this->input->post('success_url') && $this->input->post('success_url') !='')
		{
			$data['respones'] = $this->job_model->update_job_detail();
		}
		if($id !='' && $mode != '')
		{
			$data['id'] = $id;
			$this->load->view('back_end/job_'.$mode,$data);
		}
	}
	public function search_model()
	{
		$this->job_model->save_session_search();
	}
	public function clear_filter($return='yes')
	{
		$this->common_model->return_tocken_clear('job_save_search',$return);
	}
	public function applied_job_seeker($page=1,$id='')
	{
		$id = $_REQUEST['id'];
		if($id !='' && $page !='')
		{
			$data['id'] = $id;
			$data['page'] = $page;
			$this->load->view('back_end/job_applied_jobseeker',$data);
		}
	}
}