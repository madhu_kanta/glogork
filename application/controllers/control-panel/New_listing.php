<?php defined('BASEPATH') OR exit('No direct script access allowed');
class New_listing extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
	}
	public function index()
	{
		$this->city_list();
	}
		
	public function country_list($status ='ALL', $page =1)
	{
		
		$ele_array = array(
			'country_name'=>array('is_required'=>'required','input_type'=>'alpha'),
			'country_code'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array(
			'field_duplicate'=>array('country_name'),
			/*'addAllow'=>'no',
			'display_status'=>'no',
			'editAllow'=>'no',
			'deleteAllow'=>'no',
			'statusChangeAllow'=>'no'*/
		);
		$this->common_model->common_rander('country_master', $status, $page , 'Country',$ele_array,'country_name',1,$other_config);
	}
	
	public function mm_list($status ='ALL', $page =1)
	{
		$ele_array = array(
			'oldvalue'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		
		$this->common_model->common_rander('edit_history', $status, $page , 'Heading',$ele_array,'newvalue',0);
	}
	
	public function state_list($status ='ALL', $page =1)
	{
		$ele_array = array(			
			'country_id'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','relation'=>array('rel_table'=>'country_master','key_val'=>'id','key_disp'=>'country_name')),	// for relation dropdown
			'state_name'=>array('is_required'=>'required','input_type'=>'alpha'),
			'status'=>array('type'=>'radio')
		);
		
		$join_tab_array = array();
		$join_tab_array[] = array( 'rel_table'=>'country_master', 'rel_filed'=>'id', 'rel_filed_disp'=>'country_name','rel_filed_org'=>'country_id');
		
		$other_config = array('default_order'=>'DESC','filed_notdisp'=>array(),'field_duplicate'=>array('country_id','state_name'));
		$this->common_model->common_rander('state_master', $status, $page , 'State',$ele_array,'state_name',1,$other_config,$join_tab_array);
	}	
	public function city_list($status ='ALL', $page =1)
	{
		$ele_array = array(
			'country_id'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','onchange'=>"dropdownChange('country_id','state_id','state_list')",
			'relation'=>array('rel_table'=>'country_master','key_val'=>'id','key_disp'=>'country_name')),	// for relation dropdown
			//'state_id'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','relation'=>array('rel_table'=>'state_master','key_val'=>'id','key_disp'=>'state_name')),	// for relation dropdown
			'state_id'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','relation'=>array('rel_table'=>'state_master','key_val'=>'id','key_disp'=>'state_name','not_load_add'=>'yes','rel_col_name'=>'country_id','cus_rel_col_val'=>'country_id')),	// for relation dropdown
			// updated by mm
			'city_name'=>array('is_required'=>'required'), /* ,'input_type'=>'alpha'*/
			'status'=>array('type'=>'radio')
		);
		
		$join_tab_array = array();
		$join_tab_array[] = array( 'rel_table'=>'state_master', 'rel_filed'=>'id', 'rel_filed_disp'=>'state_name','rel_filed_org'=>'state_id');
		
		$join_tab_array[] = array( 'rel_table'=>'country_master', 'rel_filed'=>'id', 'rel_filed_disp'=>'country_name',			'rel_filed_org'=>'country_id','join_manual'=>' country_master.id = state_master.country_id ');
		
		$other_config = array('default_order'=>'ASC','field_duplicate'=>array('country_id','state_id','city_name'));
		$this->common_model->common_rander('city_master', $status, $page , 'City',$ele_array,'city_name',0,$other_config,$join_tab_array);
	}
	
	// 'default_order'=>'desc'
	
	public function company_size_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'company_size'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('company_size'));
		$this->common_model->common_rander('company_size_master', $status, $page , 'Company Size',$ele_array,'company_size',1,$other_config);
	}
	
	public function company_type_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'company_type'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('company_type'));
		$this->common_model->common_rander('company_type_master', $status, $page , 'Company Type',$ele_array,'company_type',1,$other_config);
	}

	public function educational_qua_group($status ='ALL', $page =1)
	{
		$ele_array = array(
			'qualification_name'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('qualification_name'));
		$this->common_model->common_rander('jobseeker_qualification_level', $status, $page , 'Educational Qualification Group',$ele_array,'qualification_name',1,$other_config);
	}
	
	public function educational_qualification_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'qualification_level_id'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','relation'=>array('rel_table'=>'jobseeker_qualification_level','key_val'=>'id','key_disp'=>'qualification_name')),
			'educational_qualification'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('qualification_level_id','educational_qualification'));
		$join_tab_array[] = array( 'rel_table'=>'jobseeker_qualification_level', 'rel_filed'=>'id', 'rel_filed_disp'=>'qualification_name', 'rel_filed_org'=>'qualification_level_id','join_manual'=>' jobseeker_qualification_level.id = educational_qualification_master.qualification_level_id ');
		
		$this->common_model->common_rander('educational_qualification_master', $status, $page , 'Educational Qualification',$ele_array,'educational_qualification',1,$other_config,$join_tab_array);
	}
	
	public function job_role_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'functional_area'=>array('is_required'=>'required','class'=>' not_reset ','type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name')),
			'role_name'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		
		
		$join_tab_array = array();
		$join_tab_array[] = array( 'rel_table'=>'functional_area_master', 'rel_filed'=>'id', 'rel_filed_disp'=>'functional_name','rel_filed_org'=>'functional_area');
		
		$other_config = array('field_duplicate'=>array('role_name','functional_area'));
		$this->common_model->common_rander('role_master', $status, $page , 'Job Role',$ele_array,'role_name',1,$other_config,$join_tab_array);
	}
	
	public function functional_area_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'functional_name'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('functional_name'));
		$this->common_model->common_rander('functional_area_master', $status, $page , 'Functional Area',$ele_array,'functional_name',1,$other_config);
	}
	public function key_skill_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'key_skill_name'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('key_skill_name'));
		$this->common_model->common_rander('key_skill_master', $status, $page , 'Key Skill',$ele_array,'key_skill_name',1,$other_config);
	}
	
	public function industries_man($status ='ALL', $page =1)
	{
		$this->load->model('back_end/siteSetting_model');
		$font_list_arr = $this->siteSetting_model->font_aw_listing();
		
		if(isset($_REQUEST['icon_name']) && $_REQUEST['icon_name'] !='')
		{
			$icon_name = $_REQUEST['icon_name'];
			$icon_hash_code = $this->siteSetting_model->gethashcode_font($icon_name);
			$_REQUEST['icone_code'] = $icon_hash_code;
		}
		$ele_array = array(
			'industries_name'=>array('is_required'=>'required'),
			'is_featured'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
			'icon_name'=>array('is_required'=>'required','type'=>'dropdown','value_arr'=>$font_list_arr),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->filed_notdisp = array('icone_code');
		$other_config = array('field_duplicate'=>array('industries_name'));
		$this->common_model->common_rander('industries_master', $status, $page , 'Industries',$ele_array,'industries_name',1,$other_config);
	}
	
	public function job_payment_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'job_payment'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('job_payment'));
		$this->common_model->common_rander('job_payment_master', $status, $page , 'Job Payment',$ele_array,'job_payment',1,$other_config);
	}
	public function job_type_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'job_type'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('job_type'));
		$this->common_model->common_rander('job_type_master', $status, $page , 'Job Type',$ele_array,'job_type',1,$other_config);
	}
	
	public function job_emp_type_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'job_type'=>array('type'=>'textarea','is_required'=>'required'),
			'status'=>array('type'=>'radio'),
		);
		$other_config = array('field_duplicate'=>array('job_type'));
		$this->common_model->save_job_emp_type_man();
		$this->common_model->common_rander('job_type_master', $status, $page , 'Job Type',$ele_array,'job_type',0,$other_config);
	}
	
	public function employement_type_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'employement_type'=>array('is_required'=>'required','label'=>'Employment Type'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->labelArr['employement_type'] = 'Employment Type';
		$other_config = array('field_duplicate'=>array('employement_type'));
		$this->common_model->common_rander('employement_type', $status, $page , 'Employment Type',$ele_array,'employement_type',1,$other_config);
	}
	public function shift_type_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'shift_type'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('shift_type'));
		$this->common_model->common_rander('shift_type', $status, $page , 'Job Shift Type',$ele_array,'shift_type',1,$other_config);
	}
	
	public function marital_status_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'marital_status'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('marital_status'));
		$this->common_model->common_rander('marital_status_master', $status, $page , 'Marital Status',$ele_array,'marital_status',1,$other_config);
	}
	public function personal_titles_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'personal_titles'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('personal_titles'));
		$this->common_model->common_rander('personal_titles_master', $status, $page , 'Personal Titles',$ele_array,'personal_titles',1,$other_config);
	}
	public function skill_language_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'skill_language'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('skill_language'));
		$this->common_model->common_rander('skill_language_master', $status, $page , 'Human Language',$ele_array,'skill_language',1,$other_config);
	}
	public function skill_level_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'skill_level'=>array('is_required'=>'required','display_placeholder'=>'No'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('skill_level'));
		$this->common_model->labelArr= array('skill_level' =>'Proficiency Level');
		$this->common_model->common_rander('skill_level_master', $status, $page , 'Proficiency Level',$ele_array,'skill_level',1,$other_config);
	}
	public function salary_range_list($status ='ALL', $page =1)
	{
		$ele_array = array(
			'salary_range'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$other_config = array('field_duplicate'=>array('salary_range'));
		$this->common_model->common_rander('salary_range', $status, $page , 'Salary Range',$ele_array,'salary_range',1,$other_config);
	}
	
}