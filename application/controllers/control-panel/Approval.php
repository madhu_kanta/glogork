<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Approval extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
	}
	public function index()
	{
		$this->job_seeker_photo();
	}
		
	public function job_seeker_photo($status ='ALL', $page =1)
	{
		$ele_array = array(
		'profile_pic'=>array('path_value'=>'assets/js_photos/')
		);
		$other_config = array(
			'addAllow'=>'no',
			'editAllow'=>'no',
			'deleteAllow'=>'no',
			'display_image'=>array('profile_pic'),
			'personal_where'=>"profile_pic!=''"
		);
		$this->common_model->js_extra_code.= ' if($(".magniflier").length > 0){OnhoverMove();}';
		$this->common_model->status_column = 'profile_pic_approval';
		$this->common_model->status_field = 'profile_pic_approval';
		$this->common_model->display_selected_field = array('id','profile_pic_approval','profile_pic','fullname','email');
		$this->common_model->common_rander('jobseeker', $status, $page , 'Job Seeker Photo',$ele_array,'register_date',1,$other_config);
	}
	
	public function job_seeker_resume($status ='ALL', $page =1)
	{
		$ele_array = array(
		'resume_file'=>array('path_value'=>'assets/resume_file/')
		);
		$other_config = array(
			'addAllow'=>'no',
			'editAllow'=>'no',
			'deleteAllow'=>'no',
			'display_image'=>array('resume_file'),
			'personal_where'=>"resume_file!=''"
		);
		$this->common_model->status_column = 'resume_verification';
		$this->common_model->status_field = 'resume_verification';
		$this->common_model->display_selected_field = array('id','resume_verification','resume_file','fullname','email');
		$this->common_model->common_rander('jobseeker', $status, $page , 'Job Seeker Resume',$ele_array,'register_date',1,$other_config);
	}
	
	public function employer_photo($status ='ALL', $page =1)
	{
		$ele_array = array(
		'profile_pic'=>array('path_value'=>'assets/emp_photos/')
		);
		$other_config = array(
			'addAllow'=>'no',
			'editAllow'=>'no',
			'deleteAllow'=>'no',
			'display_image'=>array('profile_pic'),
			'personal_where'=>"profile_pic!=''"
		);
		$this->common_model->js_extra_code.= ' if($(".magniflier").length > 0){OnhoverMove();}';
		$this->common_model->status_column = 'profile_pic_approval';
		$this->common_model->status_field = 'profile_pic_approval';
		$this->common_model->display_selected_field = array('id','profile_pic_approval','profile_pic','fullname','email');
		$this->common_model->common_rander('employer_master', $status, $page , 'Employer Photo',$ele_array,'register_date',1,$other_config);
	}
	
	public function company_logos($status ='ALL', $page =1)
	{
		$ele_array = array(
		'company_logo'=>array('path_value'=>'assets/company_logos/')
		);
		$other_config = array(
			'addAllow'=>'no',
			'editAllow'=>'no',
			'deleteAllow'=>'no',
			'display_image'=>array('company_logo'),
			'personal_where'=>"company_logo!=''"
		);
		$this->common_model->js_extra_code.= ' if($(".magniflier").length > 0){OnhoverMove();}';
		//$this->common_model->js_extra_code.= 'OnhoverMove(); ';
		$this->common_model->status_column = 'company_logo_approval';
		$this->common_model->status_field = 'company_logo_approval';
		$this->common_model->display_selected_field = array('id','company_logo_approval','company_logo','company_name','fullname','email');
		$this->common_model->common_rander('employer_master', $status, $page , 'Company Logo',$ele_array,'register_date',1,$other_config);
	}
}