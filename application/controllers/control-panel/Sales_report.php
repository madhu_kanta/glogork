<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sales_report extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
		//$this->load->model('back_end/sales_report_model','sales_report_model');
	}
	public function job_seeker($status ='ALL', $page =1)
	{
		$btn_arr = array(
			array('url'=>'sales-report/view-invoice-job-seeker/#id#','class'=>'info','label'=>'Invoice','target'=>'_blank'),
		);
		
		$data_table = array(
			'title_disp'=>'fullname',
			'disp_column_array'=> array('plan_name','payment_method','activated_on','expired_on','plan_duration','plan_currency','plan_amount','message','highlight_application','relevant_jobs','performance_report','contacts','job_post_notification','current_plan'),
			'disp_status'=>'no',
		);
		$other_config = array(
			'hide_display_image'=>'No',
			'load_member'=>'yes',
			'data_table_mem'=>$data_table,
			'data_tab_btn'=>$btn_arr,
			'default_order'=>'DESC',
			'addAllow'=>'no',
			'statusChangeAllow'=>'no',
			'display_status'=>'no',
			'deleteAllow'=>'no'
			);
			
		$join_tab_array = array();
		$join_tab_array[] = array( 'rel_table'=>'jobseeker', 'rel_filed'=>'id','rel_filed_disp'=>'fullname','rel_filed_org'=>'js_id','join_manual'=>' jobseeker.id = plan_jobseeker.js_id ');
		//$join_tab_array_filed_disp[] = array('fullname','email');
		
		$this->common_model->common_rander('plan_jobseeker',$status, $page ,'Job Seeker Sales Report','id','',0,$other_config,$join_tab_array);	
	}
	public function employer($status ='ALL', $page =1)
	{
		$btn_arr = array(
			array('url'=>'sales-report/view-invoice-employer/#id#','class'=>'info','label'=>'Invoice','target'=>'_blank'),
		);
		
		$data_table = array(
			'title_disp'=>'fullname',
			'disp_column_array'=> array('plan_name','payment_method','activated_on','expired_on','plan_duration','plan_currency','plan_amount','message','highlight_application','relevant_jobs','performance_report','contacts','job_post_notification','current_plan'),
			'disp_status'=>'no',
		);
		$other_config = array(
			'hide_display_image'=>'No',
			'load_member'=>'yes',
			'data_table_mem'=>$data_table,
			'data_tab_btn'=>$btn_arr,
			'default_order'=>'DESC',
			'field_duplicate'=>array('plan_name'), 
			'addAllow'=>'no',
			'statusChangeAllow'=>'no',
			'display_status'=>'no',
			'deleteAllow'=>'no'
			);
			
		$join_tab_array = array();
		$join_tab_array[] = array( 'rel_table'=>'employer_master', 'rel_filed'=>'emp_id','rel_filed_disp'=>'fullname','rel_filed_org'=>'emp_id','join_manual'=>'employer_master.id = plan_employer.emp_id ');
		
		$this->common_model->common_rander('plan_employer',$status, $page ,'Employer Sales Report','id','',0,$other_config,$join_tab_array);	
		
	}
	public function view_invoice_job_seeker($pay_id ='')
	{
		if($pay_id !='')
		{
			$this->data = $this->common_model->data;
			$this->db->join('jobseeker','plan_jobseeker.js_id = jobseeker.id','left');
			$payment_data = $this->common_model->get_count_data_manual('plan_jobseeker',array('plan_jobseeker.id'=>$pay_id),1,'plan_jobseeker.*,jobseeker.mobile,jobseeker.fullname,jobseeker.address,jobseeker.email');

			if($payment_data !='' && count($payment_data) > 0)
			{
				$this->data['payment_data'] = $payment_data;
				$this->common_model->__load_header('View Invoice');
				$this->data['data'] = 'test';
				$this->load->view('back_end/jobseeker_invoice',$this->data);
				$this->common_model->__load_footer();
			}
			else
			{
				redirect($this->common_model->base_url_admin.'sales-report/all-list');
			}
		}
	}
	public function view_invoice_employer($pay_id ='')
	{
		if($pay_id !='')
		{
			$this->data = $this->common_model->data;
			$this->db->join('employer_master','plan_employer.emp_id = employer_master.id','left');
			$payment_data = $this->common_model->get_count_data_manual('plan_employer',array('plan_employer.id'=>$pay_id),1,'plan_employer.*,employer_master.mobile,employer_master.fullname,employer_master.address,employer_master.email');

			if($payment_data !='' && count($payment_data) > 0)
			{
				$this->data['payment_data'] = $payment_data;
				$this->common_model->__load_header('View Invoice');
				$this->data['data'] = 'test';
				$this->load->view('back_end/employer_invoice',$this->data);
				$this->common_model->__load_footer();
			}
			else
			{
				redirect($this->common_model->base_url_admin.'sales-report/all-list');
			}
		}
	}
	
}