<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Bulk_email extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
		//$access_perm = $this->common_model->check_permission('send_mail','redirect');
	}
	public function index()
	{
		$this->send_email('add-data');
	}
	public function send_email($status ='ALL', $page =1)
	{		
		$status = 'add-data';
		$ele_array = array(
			'type'=>array('is_required'=>'required','placeholder'=>'','type'=>'dropdown','value_arr'=>array('Job seeker'=>'Job seeker','Employer'=>'Employer'),'onchange'=>'change_all()'),
			'status'=>array('is_required'=>'required','placeholder'=>'','type'=>'dropdown','value_arr'=>array('All'=>'All','Active'=>'Active','Inactive'=>'Inactive','Paid'=>'Paid'),'onchange'=>'change_after_staus()'),
			'all_single'=>array('is_required'=>'required','label'=>'All or single','placeholder'=>'','type'=>'dropdown','value_arr'=>array('All'=>'All','Single'=>'Single'),'onchange'=>'get_email_list()',),
			'member_list_email'=>array('is_required'=>'required','label'=>'Select Multiple Email','placeholder'=>'Search Member by Email','type'=>'dropdown','is_multiple'=>'yes','display_placeholder'=>'no','form_group_class'=>'member_list_email'),
			'email_subject'=>array('is_required'=>'required'),
			'email_content'=>array('is_required'=>'required','type'=>'textarea'),
		);
		$this->common_model->extra_css[] = 'vendor/select2/select2.min.css';
		$this->common_model->extra_js[] = 'vendor/select2/select2.min.js';
		$this->common_model->extra_js[] = 'vendor/ckeditor/ckeditor.js';
		$this->common_model->js_extra_code.= "
			$(document).ready(function(){ 

			/*var type = $('#type').val();
			var status = $('#status').val();
			get_suggestion_list('member_list_email','Select Member',type,status)*/ });
			
			if($('#email_content').length > 0) {  $('.email_content_edit').removeClass(' col-lg-7 ');
			$('.email_content_edit').addClass(' col-lg-10 ');
			CKEDITOR.replace( 'email_content' ); } 
			
			var all_single = $('#all_single').val();
			if(all_single == 'Single')
			{
				$('.member_list_email').show();
			}
			else
			{
				$('.member_list_email').hide();
				$('#member_list_email').val('');
			}
			function get_email_list()
			{	
				var type = $('#type').val();
			    var status = $('#status').val();
				
				if(type=='' && status=='')
				{
					alert('Please Select Type and Status');
					$('#all_single').val('');
					
				}else if(type=='')
				{
					alert('Please Select Type');
					$('#all_single').val('');
					
				}else if(status=='')
				{
					alert('Please Select Status');
					$('#all_single').val('');
					//$('#all_single').removeAttr('selected');
					
				}else{
					
			        get_suggestion_list('member_list_email','Select Member',type,status);
				}
				var all_single = $('#all_single').val();
				if(all_single == 'Single')
				{
					
					$('.member_list_email').show();
				}
				else
				{
					$('.member_list_email').hide();
					$('#member_list_email').val('');
				}
			} 
			function change_all()
			{
				$('#status').val('');
				$('#all_single').val('');
				$('.member_list_email').hide();
				$('#member_list_email').val('');
			}
			function change_after_staus()
			{
				$('#all_single').val('');
				$('.member_list_email').hide();
				$('#member_list_email').val('');
			}
			";
		$other_conf = array('action'=>'bulk-email/send-email-save');
		$this->common_model->common_rander('sms_templates', $status, $page , 'Send Bulk Email To Job seeker Or Employer',$ele_array,'template_name',0,$other_conf);
	}
	public function send_email_save()
	{
		if(isset($_REQUEST) && $_REQUEST!='')
		{
			if(isset($_REQUEST['type']) && $_REQUEST['type']!='')
			{
			  $type = $_REQUEST['type'];
			}
			if(isset($_REQUEST['status']) && $_REQUEST['status']!='')
			{
			   $status = $_REQUEST['status'];
			}
			if(isset($_REQUEST['email_subject']) && $_REQUEST['email_subject']!='')
			{
			  $email_subject = $_REQUEST['email_subject'];
			}
			if(isset($_REQUEST['email_content']) && $_REQUEST['email_content']!='')
			{
			  $email_content = $_REQUEST['email_content'];
			}		
					
	    }
		if($_REQUEST['all_single']=='All')
		{
			if(isset($type) && $type!='')
			{
				if(isset($status) && $status!='' && $status=='All')
				{
					$where = array("is_deleted"=>"No");
					
				}else if(isset($status) && $status!='' && $status=='Active')
				{
				   $where = array("is_deleted"=>"No","status"=>"APPROVED");;
				}else if(isset($status) && $status!='' && $status=='Inactive')
				{
				   $where = array("is_deleted"=>"No","status"=>"UNAPPROVED");;
				}
				else if(isset($status) && $status!='' && $status=='Paid')
				{
				   $where = array("is_deleted"=>"No","plan_status"=>"Paid");;
				}
				if($type=='Job seeker')
				{
					$email_list_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'2','email');
				}
				if($type=='Employer')
				{
					$email_list_data = $this->common_front_model->get_count_data_manual('employer_master',$where,'2','email');
				}
				$email_list = array_map('current',$email_list_data);
				
				if(isset($email_list) && $email_list !='')
				{

					$config_arra = $this->common_model->get_site_config();
					$to_email = $config_arra['contact_email'];	
					if(isset($email_list) && $email_list !='' && is_array($email_list) && count($email_list) > 0)
					{
						foreach($email_list as $email)
						{
							$data_array_custom = array('email'=>$email,'email_subject'=>$email_subject,'email_content'=>$email_content);
							$this->common_front_model->save_update_data('send_bulk_email',$data_array_custom,'id','add','','','1','1');
						}
						$this->session->set_flashdata('success_message','Email Sent Successfully.');
					}
				}
			}
		}
		if($_REQUEST['all_single']=='Single')
		{
			if(isset($_REQUEST['member_list_email']) && $_REQUEST['member_list_email']!='')
			{
				$member_list_email = $_REQUEST['member_list_email'];
				if($member_list_email !='' && is_array($member_list_email) && count($member_list_email) > 0)
				{
					foreach($member_list_email as $email)
					{
						$data_array_custom = array('email'=>$email,'email_subject'=>$email_subject,'email_content'=>$email_content);
						$this->common_front_model->save_update_data('send_bulk_email',$data_array_custom,'id','add','','','1','1');
					}
				}
				$this->session->set_flashdata('success_message','Email Sent Successfully.');
			}
		}
		redirect($this->common_model->base_url_admin.'bulk-email/send-email/add-data');
	}
	/*public function email_sent()
	{	
		$email_data =  $this->common_model->get_count_data_manual('send_bulk_email','',2,'*','',1,50);
		if(isset($email_data) && $email_data!='' && is_array($email_data) && count($email_data)>0)
		{
			foreach($email_data as $email)
			{
				$config_arra = $this->common_model->get_site_config();
				$to_email = $config_arra['contact_email'];	
				$email_subject = $email['email_subject'];
				$email_content =$email['email_content'];
				$this->common_model->common_send_email($to_email,$email_subject,$email_content,'',$email);
				
				if(isset($email['id']) && $email['email_content']!='')
				{
					$this->db->where('id', $email['id']);
					$this->db->delete('send_bulk_email'); 
				}
			}
		}
	}*/
}