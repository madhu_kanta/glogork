<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Site_setting extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
		$this->load->model('back_end/SiteSetting_model','SiteSetting_model');
		$this->table_name = 'site_config'; 	// *need to set here tabel name //
		$this->common_model->set_table_name($this->table_name);
	}
	public function index()
	{
		$this->city_list();
	}
	public function logo_favicon($status ='')
	{
		$this->label_page = 'Update Logo & Favicon';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data(1,'','Yes');
			redirect($this->common_model->data['base_url_admin'].'site-setting/logo-favicon');
		}
		else
		{
			$this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
			$ele_array = array(
				'upload_logo'=>array('type'=>'file','path_value'=>'assets/logo/'),
				'upload_favicon'=>array('type'=>'file','path_value'=>'assets/logo/')
			);
			$other_config = array('mode'=>'edit','id'=>'1','enctype'=>'enctype="multipart/form-data"');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	/*public function job_prefix($status ='')
	{
		$this->label_page = 'Update Job Prefix';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$ele_array = array(
				'job_prefix'=>array('is_required'=>'required')
			);
			$other_config = array('mode'=>'edit','id'=>'1');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}*/
	
	public function color_change($status ='')
	{
		$this->label_page = 'Update Site Color';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data(1,'','Yes');
		}
		else
		{
			$ele_array = array(
				'colour_name'=>array('is_required'=>'required','input_type'=>'color')
			);
			$other_config = array('mode'=>'edit','id'=>'1','action'=>'site-setting/update_color');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	public function update_color()
	{
		$this->SiteSetting_model->update_color();
		$this->common_model->save_update_data(1,'','Yes');
		redirect($this->common_model->base_url_admin.'site-setting/color_change');
	}
	
	public function update_email($status ='')
	{
		$this->label_page = 'Update Email';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data(1,'','Yes');
		}
		else
		{
			$ele_array = array(
				'from_email'=>array('is_required'=>'required','input_type'=>'email'),
				/*'to_email'=>array('is_required'=>'required','input_type'=>'eimal'),
				'feedback_email'=>array('is_required'=>'required','input_type'=>'email'),*/
				'contact_email'=>array('is_required'=>'required','input_type'=>'email','label'=>'To Email')
			);
			$other_config = array('mode'=>'edit','id'=>'1');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	
	public function update_key($status ='')
	{
		$this->label_page = 'Update Key';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$ele_array = array(
				'client_id'=>array('is_required'=>'required'),
				'web_appkey'=>array('is_required'=>'required')
			);
			$other_config = array('mode'=>'edit','id'=>'1');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	public function change_password($status ='')
	{
		$this->label_page = 'Change Password';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$extra_js = $this->common_model->extra_js;
			$extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
			$this->common_model->extra_js = $extra_js;
			$this->common_model->js_validation_extra = " rules: 
			  {
				confirm_password:
				{
					equalTo:'#new_password'
				},
			  },";
			$ele_array = array(
				'password'=>array('is_required'=>'required','other'=>'minlength="3" ','input_type'=>'password'),
				'new_password'=>array('is_required'=>'required','other'=>'minlength="3" ','input_type'=>'password'),
				'confirm_password'=>array('is_required'=>'required','other'=>'minlength="3" ','input_type'=>'password')
			);
			
			$other_config = array('mode'=>'edit','id'=>'1','action'=>'site-setting/save-change-password');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	public function save_change_password()
	{
		$this->SiteSetting_model->save_change_password();		
		redirect($this->common_model->base_url_admin.'site-setting/change-password');
	}
	
	public function basic_setting($status ='')
	{
		$this->label_page = 'Update Basic Site Settings';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data(1,'','Yes');
		}
		else
		{
			$ele_array = array(
				'web_name'=>array('is_required'=>'required','input_type'=>'url'),
				'web_frienly_name'=>array('is_required'=>'required','label'=>'Web Friendly Name'),
				'website_title'=>array('is_required'=>'required'),
				'website_description'=>array('type'=>'textarea','is_required'=>'required'),
				'footer_text'=>array('is_required'=>'required'),
				'contact_no'=>array('is_required'=>'required','input_type'=>'number'),
				'website_keywords'=>array('type'=>'textarea','is_required'=>'required'),
				'default_currency'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'currency_master','key_val'=>'currency_code','key_disp'=>'currency_name')),	// for relation dropdown
				'full_address'=>array('type'=>'textarea','is_required'=>'required'),
				'map_address'=>array('type'=>'textarea','is_required'=>'required'),
				'map_tooltip'=>array('is_required'=>'required'),
				'service_tax'=>array('is_required'=>'required','label'=>'Service Tax / GST'),
			);
			$other_config = array('mode'=>'edit','id'=>'1');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	public function social_site_setting($status ='')
	{
		$this->label_page = 'Update Social Site Link';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$ele_array = array(
				'facebook_link'=>array('input_type'=>'url'),
				'twitter_link'=>array('input_type'=>'url'),
				'linkedin_link'=>array('input_type'=>'url'),
				'google_link'=>array('input_type'=>'url')
			);
			$other_config = array('mode'=>'edit','id'=>'1','addAllow'=>'no','deleteAllow'=>'no');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	
	public function analytics_code_setting($status ='')
	{
		$this->label_page = 'Update Google Analytics Code';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$ele_array = array(
				'google_analytics_code'=>array('type'=>'textarea')
			);
			$other_config = array('mode'=>'edit','id'=>'1');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
	public function social_app_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'social_name'=>array('is_required'=>'required'),
			'client_key'=>'',
			'client_secret'=>'',
			'status'=>array('type'=>'radio')
		);
		$other_confing = array('addAllow'=>'no','deleteAllow'=>'no');
		
		$this->common_model->common_rander('social_login_master', $status, $page , 'Social Application',$ele_array,'social_name',1,$other_confing);
	}
	
	public function currency_man($status ='ALL', $page =1)
	{
		$ele_array = array(
			'currency_name'=>array('is_required'=>'required'),
			'currency_code'=>array('is_required'=>'required'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->common_rander('currency_master', $status, $page , 'Manage Currency',$ele_array,'currency_name',1);
	}
}