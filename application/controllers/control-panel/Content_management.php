<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Content_management extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
	}
	public function index()
	{
		$this->cms_pages();
	}
	public function cms_pages($status ='ALL', $page =1)
	{
		$ele_array = array(
			'page_title'=>array('is_required'=>'required'),
			'page_content'=>array('type'=>'textarea'),
			'page_url'=>array('type'=>'hidden'),
			'genrate_url'=>array('type'=>'manual','code'=>'<input type="hidden" value="page_title-|-page_url" name="genrate_url" />'), // for generate url from page title title 
			'status'=>array('type'=>'radio')
		);
		$this->common_model->extra_js[] = 'vendor/ckeditor/ckeditor.js';
			$this->common_model->js_extra_code = " if($('#page_content').length > 0) {  $('.page_content_edit').removeClass(' col-lg-7 ');
			$('.page_content_edit').addClass(' col-lg-10 ');
			CKEDITOR.replace( 'page_content' ); } 
			/*$(document).ready(function(){
				CKEDITOR.instances['page_content'].on('change',function(){
					var page_content = CKEDITOR.instances['page_content'].getData();
					page_content = page_content.split(' ');
					if(page_content.length>3){
						CKEDITOR.instances['page_content'].updateElement();
						alert();
					}
				})
			});*/
			";
		$this->common_model->common_rander('cms_pages', $status, $page , 'CMS Page',$ele_array,'page_title',0);
	}
}