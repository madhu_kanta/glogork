<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Email_templates extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
	}
	public function index()
	{
		$this->email_templates();
	}
	public function email_templates($status ='ALL', $page =1)
	{
		$ele_array = array(
			'template_name'=>array('is_required'=>'required'),
			'email_subject'=>array('is_required'=>'required'),
			'email_content'=>array('type'=>'textarea'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->extra_js[] = 'vendor/ckeditor/ckeditor.js';
			$this->common_model->js_extra_code = " if($('#email_content').length > 0) {  $('.email_content_edit').removeClass(' col-lg-7 ');
			$('.email_content_edit').addClass(' col-lg-10 ');
			CKEDITOR.replace( 'email_content' ); } ";
		$this->common_model->common_rander('email_templates', $status, $page , 'Email Templates',$ele_array,'template_name',0);
	}
}