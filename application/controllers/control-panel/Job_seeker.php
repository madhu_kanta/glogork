<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_seeker extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->common_model->checkLogin(); // here check for login or not
		$this->load->model('back_end/Jobseeker_model','jobseeker_model');
	}
	public function index()
	{
		$this->seeker_list();
	}
	public function seeker_list($status ='ALL', $page =1,$clear_search='no')
	{
		if($clear_search =='yes')
		{
			$this->clear_filter('no');
		}
		$this->jobseeker_model->seeker_list_model($status,$page);
	}
	public function active_seeker_list($status ='ALL', $page =1,$clear_search='no')
	{
		if($clear_search =='yes')
		{
			$this->clear_filter('no');
		}
		$this->common_model->button_array[] = array('onClick'=>"return display_payment(#id#,'job_seeker')",'class'=>'success','label'=>'Approve as Paid');
		
		$personal_where = array();
		$personal_where['where_per'] = " plan_status = 'Active' and status ='APPROVED' ";
		$personal_where['label_disp'] = "Active Job Seeker";
		$this->jobseeker_model->seeker_list_model($status,$page,$personal_where);
	}
	public function paid_seeker_list($status ='ALL', $page =1,$clear_search='no')
	{
		if($clear_search =='yes')
		{
			$this->clear_filter('no');
		}
		$this->common_model->button_array[] = array('onClick'=>"return display_payment(#id#,'job_seeker')",'class'=>'success','label'=>'Upgrade Downgrade Plan','btn_leg_class'=>'col-lg-3');
		$personal_where = array();
		$personal_where['where_per'] = " plan_status ='Paid' ";
		$personal_where['label_disp'] = "Paid Job Seeker";
		$this->jobseeker_model->seeker_list_model($status,$page,$personal_where);
	}
	public function expired_seeker_list($status ='ALL', $page =1,$clear_search='no')
	{
		if($clear_search =='yes')
		{
			$this->clear_filter('no');
		}
		$this->common_model->button_array[] = array('onClick'=>"return display_payment(#id#,'job_seeker')",'class'=>'success','label'=>'Renew Plan');
		$personal_where = array();
		$personal_where['where_per'] = " plan_status ='Expired' ";
		$personal_where['label_disp'] = "Expired Job Seeker";
		$this->jobseeker_model->seeker_list_model($status,$page,$personal_where);
	}
	public function seeker_detail($id = '',$mode='view')
	{
		if($id =='')
		{
			redirect($this->common_model->base_url_admin.'job-seeker/seeker-list');
			exit;
		}
		$this->common_model->__load_header('Job Seeker Detail');
		$this->data = $this->common_model->data;
		$this->data['view_edit_mode'] = $mode;
		$this->data['id'] = $id;
		$this->db->join("educational_qualification_master as eqm","je.qualification_level = eqm.id",'LEFT');
		$this->data['edu_data'] = $this->common_model->get_count_data_manual('jobseeker_education as je',array('je.js_id'=>$id),2,'je.*, eqm.educational_qualification','',0,'',0);
		$this->data['plan_data'] = $this->common_model->get_count_data_manual('plan_jobseeker',array('js_id'=>$id,'is_deleted'=>'No','current_plan'=>'Yes'),1,'','',0,'',0);
		$this->data['lang_data'] = $this->common_model->get_count_data_manual('jobseeker_language',array('js_id'=>$id),2,'','',0,'',0);
		$this->data['work_hist_data'] = $this->common_model->get_count_data_manual('jobseeker_workhistory_view',array('js_id'=>$id),2,'','',0,'',0);
		$this->data['job_seeker_data'] = $this->jobseeker_model->get_data($id);
		$this->load->view('back_end/job_seeker_detail',$this->data);
		$this->common_model->__load_footer('');
	}
	public function save_new_js()
	{
		$id = $this->jobseeker_model->save_new_js();
		if($id !='')
		{
			redirect($this->common_model->base_url_admin.'job-seeker/seeker_detail/'.$id.'/edit');
		}
		else
		{
			//redirect($this->common_model->base_url_admin.'job-seeker/seeker-list');
			redirect($this->common_model->base_url_admin.'job-seeker/seeker-list/add-data');
		}
	}
	public function view_detail($id = '',$mode='',$disp_mode = 'view')
	{
		if($id !='' && $mode != '')
		{
			$data['id'] = $id;
			$data['disp_mode'] = $disp_mode;
			$this->load->view('back_end/job_seeker_'.$mode,$data);
		}
	}

	public function save_detail($id = '',$mode='')
	{
		if($this->input->post('lan_total_count') && $this->input->post('lan_total_count') !='')
		{
			$data['respones'] = $this->jobseeker_model->save_job_lan($id);
		}
		else if($this->input->post('work_total_count') && $this->input->post('work_total_count') !='')
		{
			$data['respones'] = $this->jobseeker_model->save_job_work($id);
		}
		else if($this->input->post('edu_total_count') && $this->input->post('edu_total_count') !='')
		{
			$data['respones'] = $this->jobseeker_model->save_edu_data($id);
		}
		else if($this->input->post('success_url') && $this->input->post('success_url') !='')
		{
			$data['respones'] = $this->jobseeker_model->update_job_detail();
		}
		if($id !='' && $mode != '')
		{
			$data['id'] = $id;
			$this->load->view('back_end/job_seeker_'.$mode,$data);
		}
	}
	public function delete_js_data($id = '',$mode='',$l_id='')
	{
		if($l_id !='' && $id !='' && $mode =='lang_detail')
		{
			$this->jobseeker_model->delete_job_lan($l_id);
		}
		else if($l_id !='' && $id !='' && $mode =='work_history')
		{
			$this->jobseeker_model->delete_job_work($l_id);
		}
		else if($l_id !='' && $id !='' && $mode =='education_detail')
		{
			$this->jobseeker_model->delete_edu_work($l_id);
		}
		
		if($id !='' && $mode != '')
		{
			$data['id'] = $id;
			$this->load->view('back_end/job_seeker_'.$mode,$data);
		}
	}
	public function plan_list()
	{
		$data['base_url'] = $this->common_model->base_url;
		$this->load->view('back_end/payment_display',$data);
	}
	public function plan_update()
	{
		$data_return = $this->common_model->update_plan_member_call();
		$data['data'] =  json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}
	
	public function search_model()
	{
		$this->jobseeker_model->save_session_search();
	}
	public function clear_filter($return='yes')
	{
		$this->common_model->return_tocken_clear('js_save_search',$return);
	}
	public function delete_request($status ='ALL', $page =1)
	{
		// delete job seekeer sprofile process
		if($this->input->post('selected_val') && $this->input->post('selected_val')!='')
		{
			$delete_profile = $this->input->post('selected_val');
			if(isset($delete_profile) && is_array($delete_profile) && count($delete_profile) > 0)
			{
				foreach($delete_profile as $delete_id)
				{
					$where_arra = array('id'=>$delete_id,'is_deleted'=>'No','user_type'=>'js');
					$delete_data = $this->common_model->get_count_data_manual('delete_profile_request',$where_arra,1,'req_id');
					$where_arra_delete_jobseeker = array("id"=>$delete_data['req_id']);
					$data_array = array("is_deleted"=>'Yes');
					$this->common_model->update_insert_data_common('jobseeker',$data_array,$where_arra_delete_jobseeker);
				}
			}
		}	
		// delete profile process
		$ele_array = array(
			'status'=>array('type'=>'radio')
		);
		$other_confing = array('addAllow'=>'no','editAllow'=>'no','statusChangeAllow'=>'no','display_status'=>'no');
		$this->common_model->data_tabel_filedIgnore = array('req_id','id','is_deleted');
		//$this->common_model->labelArr['reason_for_del'] = 'Reason For Deleting';
		$this->common_model->labelArr= array('reason_for_del' =>'Reason For Deleting','fullname'=>'Full Name');
		$this->common_model->common_rander('delete_job_seekar_view', $status, $page , 'Delete Job Seeker Request',$ele_array,'id',1,$other_confing);
		
	}
}