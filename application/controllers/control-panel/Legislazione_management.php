<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Legislazione_management extends CI_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->common_model->checkLogin(); // here check for login or not
    }

    public function index() {
        $this->legislazione_pages();
    }

    public function legislazione_list($status = 'ALL', $page = 1) {
        $ele_array = array(
            'title' => array('is_required' => 'required'),
            'content' => array('type' => 'textarea'),
            'alias' => array('type' => 'hidden'),
            'genrate_url' => array('type' => 'manual', 'code' => '<input type="hidden" value="title-|-alias" name="genrate_url" />'), // for generate url from page title title ,
            'legislazione_image' => array('type' => 'file', 'path_value' => 'assets/legislazione_image/'),
            'status' => array('type' => 'radio')
        );
        $this->common_model->extra_js[] = 'vendor/ckeditor/ckeditor.js';
        $this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';

        $this->common_model->js_extra_code = " if($('#content').length > 0) { $('.page_content_edit').removeClass(' col-lg-7 ');
			$('.page_content_edit').addClass(' col-lg-10 ');
			CKEDITOR.replace( 'content' ); }";
        $other_config = array('enctype' => 'enctype="multipart/form-data"', 'display_image' => array('legislazione_image'));
        $this->common_model->common_rander('legislazione_master', $status, $page, 'Legislazione List', $ele_array, 'title', 0, $other_config);
    }

}
