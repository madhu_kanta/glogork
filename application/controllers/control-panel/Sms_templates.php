<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sms_templates extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_model->checkLogin(); // here check for login or not
	}
	public function index()
	{
		$this->sms_templates();
	}

	public function sms_templates($status ='ALL', $page =1)
	{
		$ele_array = array(
			'template_name'=>array('is_required'=>'required'),
			'sms_content'=>array('type'=>'textarea'),
			'status'=>array('type'=>'radio')
		);
		$this->common_model->common_rander('sms_templates', $status, $page , 'SMS Templates',$ele_array,'template_name',1);
	}
	
	public function sms_configuration($status ='')
	{
		$this->table_name = 'site_config'; 	// *need to set here tabel name //
		$this->common_model->set_table_name($this->table_name);
		
		$this->label_page = 'Update Sms Configuration';
		if(isset($status) && $status == 'save-data')
		{
			$this->common_model->save_update_data();
		}
		else
		{
			$ele_array = array(
				'sms_api'=>array('is_required'=>'required','type'=>'textarea','placeholder'=>'Add ##contacts## for mobile number and ##sms_text## for message in your api url'),
				'sms_api_status'=>array('type'=>'radio')
			);
			$other_config = array('mode'=>'edit','id'=>'1','back_hidden'=>'yes');
			$this->data['data'] = $this->common_model->generate_form_main($ele_array,$other_config);

			$this->common_model->__load_header($this->label_page);
			$this->load->view('common_file_echo',$this->data);
			$this->common_model->__load_footer();
		}
	}
}