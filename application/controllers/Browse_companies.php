<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Browse_companies extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->ajax_search = 0;
		$this->data['base_url'] = $this->base_url;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->load->model('front_end/companywise_listing_model');
		$this->load->model('front_end/our_recruiters_model');
	}
	
	public function index($page=1)
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 12;
		$this->data['limit_per_page'] = $limit_per_page;
		$company_count = $this->companywise_listing_model->gettotaljobs_groupbycompany_count();
		$this->data['company_count'] = ($company_count!='' && $company_count!=0 && $company_count > 0 ) ? $company_count : 0;
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','company_name !='=>'');
		if($user_agent == 'NI-WEB')
		{
			if($company_count!='' && $company_count> 0 )
			{
				$req_field = array('company_name,id');
				$company_data = $this->companywise_listing_model->getcompaniesdata($where_arra,$page,$limit_per_page,$req_field);
				$this->data['company_data'] = $company_data;
				/*echo "<pre>";
				print_r($this->data['company_data']);*/
				$page_title = "Browse Companies";//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					$this->common_front_model->__load_header($page_title);
					$this->load->view('front_end/browse_companies_view',$this->data);
				}
				else
				{
					$this->load->view('front_end/companies_all_result',$this->data);
				}
				
			}
			else
			{
				$page_title = 'Page Not found';
				$this->common_front_model->__load_header($page_title);
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			//for user agent androif and ohter
			$return_var['data'] = array();
			$return_var['unique_companu_count'] = $company_count;
			$return_var['tocken'] = $this->security->get_csrf_hash();
			if($company_count!='' && $company_count > 0 )
			{
				$req_field = array('company_name,id');
				$company_data = $this->companywise_listing_model->getcompaniesdata($where_arra,$page,$limit_per_page,$req_field);
				$return_var['data'] = $company_data;	
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($return_var));
		}
	}
	public function give_active_job_recruiter()
	{
		$return_var['tocken'] = $this->security->get_csrf_hash();
		$return_var['acitve_jobs_by_company'] = 0;
		$employer_id = $this->input->post('company_name') ? $this->input->post('company_name') : "";
		if($employer_id != '')
		{
			$return_var['acitve_jobs_by_company'] = $this->our_recruiters_model->getrecruitersactivejob_count_name($employer_id);
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($return_var));
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}