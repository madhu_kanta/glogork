<?php defined('BASEPATH') OR exit('No direct script access allowed');
class My_plan extends CI_Controller 
{
	public $data = array();
	public $user_data = array();
	public function __construct()
	{
		parent::__construct();
		
		//$this->common_front_model->redirectLoginfront();
		$user_data = $this->common_front_model->get_logged_user_typeid();
		$this->user_type = $user_data['user_type'];
		$user_id = $user_data['user_id'];
		$this->base_url = $this->common_front_model->data['base_url'];
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($user_agent =='NI-AAPP')
		{
			$this->user_type = $this->input->post('user_type');
			$user_id = $this->input->post('user_id');
		}
		if(($this->user_type =='' || $user_id =='') && $user_agent == 'NI-WEB')
		{
			redirect($this->base_url);
		}
		$this->load->model("front_end/my_plan_model");
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->my_plan_model->user_type = $this->user_type;
		$this->my_plan_model->user_id = $user_id;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function get_payudetail()
	{
		$data_return = array();		
		$data_return['status'] = 'success';
		if($this->my_plan_model->is_active =='pay_biz')	// use for payu bizz payment gateway
		{
			$data_return['marchent_key'] = $this->my_plan_model->marchent_key;
			$data_return['salt'] = $this->my_plan_model->salt;
			$data_return['marchent_id'] = $this->my_plan_model->marchent_id;
		}
		else
		{
			$data_return['marchent_key'] = $this->my_plan_model->payu_marchent_key;
			$data_return['salt'] = $this->my_plan_model->payu_salt;
			$data_return['marchent_id'] = $this->my_plan_model->payu_marchent_id;
		}
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);	
	}
	public function load_header($page_title ='')
	{
		if($this->user_type !='' && $this->user_type =='job_seeker')
		{
			$this->common_front_model->__load_header($page_title);
		}
		else
		{
			$this->common_front_model->__load_header_employer($page_title);
		}	
	}
	public function load_footer()
	{
		if($this->user_type !='' && $this->user_type =='job_seeker')
		{
			$this->common_front_model->__load_footer();
		}
		else
		{
			$this->common_front_model->__load_footer_employer();
		}
	}
	public function index()
	{
		$this->common_front_model->set_orgin();
		if($this->session->userdata('coupan_data_reddem'))
		{
			$this->session->unset_userdata('coupan_data_reddem');
		}
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		
		if($user_agent == 'NI-WEB')
		{
			$page_title = 'My Membership Plan';
			$this->load_header($page_title);
			$this->load->view('front_end/plan_list_view',$this->data);
			$this->load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			$cms_data = $this->my_plan_model->plan_list();
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
	public function buy_now()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		$plan_id = '';
		if($this->input->post('plan_id'))
		{
			$plan_id = $this->input->post('plan_id');
		}
		if($plan_id == '')
		{
			redirect($this->base_url.'my-plan');
			exit;
		}
		if($user_agent == 'NI-WEB')
		{
			$page_title = 'My Membership Plan';
			$this->load_header($page_title);
			$this->data['plan_id'] = $plan_id;
			$this->load->view('front_end/plan_detail_view',$this->data);
			$this->load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
	function check_coupan()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		$plan_id = '';
		$couponcode ='';
		if($this->input->post('plan_id'))
		{
			$plan_id = $this->input->post('plan_id');
		}
		if($this->input->post('couponcode'))
		{
			$couponcode = $this->input->post('couponcode');
		}
		
		$data_return = array();
		$data_return['status']   = 'error';
		$data_return['tocken'] = $this->security->get_csrf_hash();
		if($plan_id !='' && $couponcode !='')
		{
			$return = $this->my_plan_model->check_copan($plan_id,$couponcode);
			if($return == 'success')
			{
				$data_return['status'] = 'success';
				$this->data['plan_id'] = $plan_id;
				$this->data['base_url'] = $this->base_url;
				if($user_agent == 'NI-WEB')
				{
					$data_return['message'] = $this->load->view('front_end/plan_detail_view',$this->data,true);
				}
				else
				{
					$data_return['message'] = 'Coupan Code applied successfully.';
					$data_return['discount_amount'] = $this->my_plan_model->discount_amount_temp;
				}
			}
			else
			{
				$data_return['message'] = $return;
			}
		}
		else
		{	
			$data_return['message'] = 'Please enter Coupan Coode';
		}
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}
	public function payment_process() 
	{
		if($this->session->userdata('plan_data_session'))
		{
			$this->data['base_url'] = $this->base_url;
			$this->data['plan_data'] = $this->session->userdata('plan_data_session');
			if($this->my_plan_model->is_active =='pay_biz')	// use for payu bizz payment gateway
			{
				$this->load->view('front_end/payment_process',$this->data);	
			}
			else if($this->my_plan_model->is_active =='payu_money')	// use for payu money payment gateway
			{
				$this->load->view('front_end/payu_payment_process',$this->data);
			}
			else if($this->my_plan_model->is_active =='cc_avenu')	// use for cc avenu bizz payment gateway
			{
				$this->load->view('front_end/cc_payment_process',$this->data);
			}
		}
		else
		{
			redirect($this->base_url.'my-plan');
			exit;
		}
	}
	/*public function cc_payment_process() // use for ccavenu payment gateway
	{
		if($this->session->userdata('plan_data_session'))
		{
			$this->data['base_url'] = $this->base_url;
			$this->data['plan_data'] = $this->session->userdata('plan_data_session');
			$this->load->view('front_end/cc_payment_process',$this->data);
		}
		else
		{
			redirect($this->base_url.'my-plan');
			exit;
		}
	}
	public function payu_payment_process() // use for payu money payment gateway
	{
		if($this->session->userdata('plan_data_session'))
		{
			$this->data['base_url'] = $this->base_url;
			$this->data['plan_data'] = $this->session->userdata('plan_data_session');
			$this->load->view('front_end/payu_payment_process',$this->data);
		}
		else
		{
			redirect($this->base_url.'my-plan');
			exit;
		}
	}*/
	public function payment_success($session_id ='')
	{
		if($session_id !='')
		{
			//$session_id = base64_decode($session_id);
			//session_id($session_id);
			//session_start();
		}
		/*echo '<pre>';
		print_r($_REQUEST);
		echo '</pre>';*/
		if($this->my_plan_model->is_active =='cc_avenu')	// use for cc avenu bizz payment gateway
		{
			$update_plan = $this->my_plan_model->cc_update_payment();
		}
		else
		{
			$update_plan = $this->my_plan_model->update_payment();
		}
		//print_r($update_plan);
		redirect($this->base_url.'my-plan/success');
		exit;
		
	}
	public function update_plan_app()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($user_agent =='NI-AAPP' || $user_agent =='NI-IAPP')
		{
			$data_return = $this->my_plan_model->update_payment_app();
		}
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}
	public function success()
	{
		$page_title = 'Membership Plan';
		$this->load_header($page_title);
		$this->data['base_url'] = $this->base_url;
		$this->load->view('front_end/plan_success_view',$this->data);
		$this->load_footer();
		//
	}
	public function payment_cancel($session_id ='')
	{
		if($session_id !='')
		{
			//$session_id = base64_decode($session_id);
			//session_id($session_id);
			//session_start();
		}
		$this->session->set_userdata('cancel_payment','Yes');
		redirect($this->base_url.'my-plan/index');
		exit;
	}
	public function current_plan()
	{
		$this->common_front_model->set_orgin();  
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		
		if($user_agent == 'NI-WEB')
		{
			$page_title = 'Membership Current Plan';
			$this->load_header($page_title);
			$this->data['base_url'] = $this->base_url;
			$this->load->view('front_end/current_plan_detail',$this->data);
			$this->load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			$cms_data = $this->my_plan_model->current_plan_detail();
			if($cms_data !='' &&  $cms_data > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['token'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
}
?>