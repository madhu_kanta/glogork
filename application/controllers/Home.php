<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->base_url = base_url();
        $this->ajax_search = 0;
        $this->load->model('front_end/home_model');
        $this->load->model('front_end/our_recruiters_model');
        $this->data['base_url'] = $this->base_url;
        $this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
        $this->load->model('front_end/job_application_model');
    }

    public function index($page = 1) {

        /* if($this->common_front_model->checkLoginfrontempl())
          {
          redirect($this->base_url.'employer_profile');
          } */
        if ($this->common_front_model->checkLoginfrontempl()) {
            //redirect($this->base_url.'browse-resume');
            $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
            $user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
            $this->load->library('encryption');
            if ($this->input->post('limit_per_page')) {
                $limit_per_page = $this->input->post('limit_per_page');
            }
            $limit_per_page = isset($limit_per_page) ? $limit_per_page : 5;
            $this->data['limit_per_page'] = $limit_per_page;

            $js_action_details = $this->job_application_model->manage_job_application($page, $limit_per_page);
            if ($js_action_details['status'] == 'success') {
                $this->data['status'] = 'success';
                $this->data['apply_list_count'] = $js_action_details['apply_list_count'];
                $this->data['apply_list_data'] = $js_action_details['apply_list_data'];
                if ($user_agent == 'NI-WEB') {
                    $this->data['custom_lable'] = $this->lang;
                    if ($this->ajax_search == 0) {
                        $this->common_front_model->__load_header_employer("Home");
                        $this->load->view('front_end/home_view_1', $this->data);
                    } else {
                        $this->load->view('front_end/page_part/manage_job_application_result_view', $this->data);
                    }

                    if ($this->ajax_search == 0) {
                        $this->common_front_model->__load_footer_employer();
                    }
                }
            } else {
                $this->data['status'] = 'error';
                if ($user_agent == 'NI-WEB') {
                    $this->common_front_model->__load_header_employer($page_title);
                    $this->load->view('front_end/404_view', $this->data);
                    $this->common_front_model->__load_footer_employer();
                }
            }
            if ($user_agent != 'NI-WEB') {
                $this->data['js_img'] = 'assets/js_photos';
                $this->data['emp_posted_job_list'] = $js_action_details['emp_posted_job_list'];
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode($this->data));
            }
        } else {
            $this->common_front_model->__load_header("Home");
            $this->load->view('front_end/home_view_1', $this->data);
            $this->common_front_model->__load_footer();
        }
    }

    public function getjobposting($pagenum = 1) {
        $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
        $recent_jobs_count = $this->home_model->getrecent_jobs_count();
        $recent_jobs_post = $this->home_model->getrecent_jobs($pagenum);
        $this->data['total_post_count'] = $recent_jobs_count;
        $this->data['emp_posted_job'] = $recent_jobs_post;
        if ($this->ajax_search == 0) {
            $this->load->view('front_end/page_part/job_search_result_view_index', $this->data);
        } else {
            $this->load->view('front_end/page_part/job_search_result_view_index', $this->data);
        }
    }

}
