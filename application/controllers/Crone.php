<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Crone extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->data['base_url'] = $this->base_url;
	}
	public function send_bulk_email()
	{	
		$email_data =  $this->common_model->get_count_data_manual('send_bulk_email','',2,'*','',1,50);
		if(isset($email_data) && $email_data!='' && is_array($email_data) && count($email_data)>0)
		{
			foreach($email_data as $email)
			{
				$config_arra = $this->common_model->get_site_config();
				$to_email = $config_arra['contact_email'];	
				$email_subject = $email['email_subject'];
				$email_content =$email['email_content'];
				$this->common_model->common_send_email($to_email,$email_subject,$email_content,'',$email);
				
				if(isset($email['id']) && $email['email_content']!='')
				{
					$this->db->where('id', $email['id']);
					$this->db->delete('send_bulk_email'); 
				}
			}
		}
		/* create crone in server 
		 set minute :10
		 set crone jobs command : 
		"curl -silent https://www.rojgarkendra.org/crone/send_bulk_email" */
	}
	
}