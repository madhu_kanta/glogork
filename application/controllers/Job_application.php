<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_application extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin();
		$this->data['base_url'] = $this->base_url = base_url();
		$this->load->model('front_end/job_application_model');
		$this->load->model('front_end/employer_profile_model');
		if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
		{
			redirect($this->data['base_url']);
		}
	}
	function manage_job_application($page=1)
	{
	    $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		$page_title = $this->lang->line('manage_application_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$this->load->library('encryption');
		if(is_numeric($page) && $page!='')
		{
			$js_action_details = $this->job_application_model->manage_job_application($page,$limit='');
			if($js_action_details['status'] == 'success')
			{
				$this->data['status'] = 'success';
				$this->data['apply_list_count'] = $js_action_details['apply_list_count'];
				$this->data['apply_list_data'] = $js_action_details['apply_list_data'];
				if($user_agent == 'NI-WEB')
				{
					$this->data['custom_lable'] = $this->lang;
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header_employer($page_title);
							$this->load->view('front_end/manage_job_application_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/manage_job_application_result_view',$this->data);
						}
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer_employer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				if($user_agent == 'NI-WEB')
				{
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer_employer();
				}
			}
		}
		else
		{
			$this->data['status'] = 'error';
			if($user_agent == 'NI-WEB')
			{
			$this->common_front_model->__load_header_employer($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer_employer();
			}
		}
		
		if($user_agent != 'NI-WEB')
		{
			$this->data['js_img'] = 'assets/js_photos';
			$this->data['emp_posted_job_list'] = $js_action_details['emp_posted_job_list'];
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($this->data));
		}	
	}
	function shortlisted_application($page=1)
	{
	    $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
		$page_title = $this->lang->line('shortlist_application_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$this->load->library('encryption');
		if(is_numeric($page) && $page!='')
		{
			$js_action_details = $this->job_application_model->shortlist_application($page,$limit='');
			if($js_action_details['status'] == 'success')
			{
				$this->data['apply_list_data'] = $js_action_details['apply_list_data'];
				$this->data['apply_list_count'] = $js_action_details['apply_list_count'];
				$this->data['custom_lable'] = $this->lang;
				$this->data['page_name'] = 'shortlisted_application';
				$this->data['status'] = 'success';
				if($user_agent == 'NI-WEB')
				{
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header_employer($page_title);
							$this->load->view('front_end/manage_job_application_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/manage_job_application_result_view',$this->data);
						}
					
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer_employer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer_employer();
			}
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['js_img'] = 'assets/js_photos';
			$data['status'] = $this->data['status'];
			$data['apply_list_count'] = $js_action_details['apply_list_count'];
			$data['apply_list_data'] = $js_action_details['apply_list_data'];
			$data['emp_posted_job_list'] = $js_action_details['emp_posted_job_list'];
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
	}
	function send_message_js()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='send_message')
		{
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				// $subject = $this->input->post('subject');			   
			    // $content = $this->input->post('content');
				 $this->load->library('form_validation');
				 $this->form_validation->set_rules('subject', 'Subject ', 'required');
			     $this->form_validation->set_rules('content', "Message contect ", 'required');
			}
			if($user_agent=='NI-WEB'  &&  $this->form_validation->run() == FALSE)
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->job_application_model->send_message_js();
				if($response == 'success')
				{
					$data['errmessage'] =  $this->lang->line('send_mesage_success_msg');
				    $data['status'] =  'success';
				}
				elseif($response == 'error_plan')
				{
					$data['errmessage'] =  'Sorry ! You do not have credit left to message.';
				    $data['status'] =  'error';
				}
				elseif($response == 'error_block')
				{
					$data['errmessage'] =  'Sorry ! You do not send message to this job seeker because you are block.';
				    $data['status'] =  'error';
				}
				else
				{
					$data['errmessage'] =  $this->lang->line('js_action_err_msg');
				    $data['status'] =  'error';
				}
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function add_to_shortlist()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='')
		{
				$response = $this->job_application_model->add_to_shortlist();
				if($response == 'success')
				{
					$data['errmessage'] =  $this->lang->line('add_to_shortlist_success_msg');
				    $data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] =  $this->lang->line('emp_action_err_msg');
				    $data['status'] =  'error';
				}
			
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	function remove_from_shortlist()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' )
		{
				$response = $this->job_application_model->remove_from_shortlist();
				if($response == 'success')
				{
					$data['errmessage'] =  $this->lang->line('remove_from_shortlist_success_msg');
				    $data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] =  $this->lang->line('emp_action_err_msg');
				    $data['status'] =  'error';
				}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function download_resume()
	{
		if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
		{ 
			$emp_id_stored_plan = $this->common_front_model->get_empid();
			$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','cv_access_limit');
		}
		$user_agent = $this->input->post('user_agent');
		$file_com = $this->input->post('file');
		$js_id_com = $this->input->post('jidpkd');
		$file = base64_decode($file_com);
		$js_id = base64_decode($js_id_com);
		/*$this->load->library('encryption');
		if($this->encryption->decrypt($file_com) && $this->encryption->decrypt($file_com)!='')
		{
			$file = $this->encryption->decrypt($file_com);
		}
		else
		{
			$file = 'file_name';
		}*/
		if($user_agent!='' && $file!='')
		{
			if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='' && isset($return_data_plan_emp) && $return_data_plan_emp == 'Yes' && $js_id!='')
			{
				$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1); 
				$file_path = $this->data['base_url'].'assets/resume_file/'.$file;
				$data['status'] = 'success';
				$data['file_download'] = $file_path;
				$this->common_front_model->check_for_plan_update($emp_id_stored_plan,'employer',$js_id,'resume');	
			}
			else
			{
				$data['errmessage'] = "Please upgrade your membership to access the c.v";
				$data['status'] =  'error';	
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function download_resume_for_app()
	{
		if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
		{ 
			$emp_id_stored_plan = $this->common_front_model->get_empid();
			$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','cv_access_limit');
		}
		$user_agent = $this->input->post('user_agent');
		$js_id_com = $this->input->post('jidpkd');
		$js_id = $js_id_com;
		$where = array('id'=>$js_id_com);  
		$row_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','resume_file');
		$file = $row_data['resume_file'];
		
		if($user_agent!='' && $file!='' && $js_id!='' && $return_data_plan_emp!='')
		{
			if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='' && isset($return_data_plan_emp) && $return_data_plan_emp == 'Yes' && $js_id!='')
			{
				$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1); 
				$file_path = $this->data['base_url'].'assets/resume_file/'.$file;
				$data['status'] = 'success';
				$data['file_download'] = $file_path;
				$this->common_front_model->check_for_plan_update($emp_id_stored_plan,'employer',$js_id,'resume');	
			}
			else
			{
				$data['errmessage'] = "Please upgrade your membership to access the c.v";
				$data['status'] =  'error';	
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function download_cv($page=1)
	{
	    $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		$page_title = $this->lang->line('download_cv_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$this->load->library('encryption');
		if(is_numeric($page) && $page!='')
		{
			$js_action_details = $this->job_application_model->download_cv($page,$limit='');
			if($js_action_details['status'] == 'success')
			{
				$this->data['status'] = 'success';
				$this->data['apply_list_count'] = $js_action_details['apply_list_count'];
				$this->data['apply_list_data'] = $js_action_details['apply_list_data'];
				if($user_agent == 'NI-WEB')
				{
					$this->data['custom_lable'] = $this->lang;
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header_employer($page_title);
							$this->load->view('front_end/download_cv_view',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/download_cv_result_view',$this->data);
						}
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer_employer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				if($user_agent == 'NI-WEB')
				{
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer_employer();
				}
			}
		}
		else
		{
			$this->data['status'] = 'error';
			if($user_agent == 'NI-WEB')
			{
			$this->common_front_model->__load_header_employer($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer_employer();
			}
		}
		
		if($user_agent != 'NI-WEB')
		{
			$this->data['js_img'] = 'assets/js_photos';
			$this->data['emp_posted_job_list'] = $js_action_details['emp_posted_job_list'];
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($this->data));
		}	
	}
}
?>