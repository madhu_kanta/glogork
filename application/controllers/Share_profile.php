<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Share_profile extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['base_url'] = $this->base_url = base_url();
		$this->load->model('front_end/share_profile_model');
		$this->load->model('front_end/employer_profile_model');
	}
	
	function js_profile($id='')
	{
		$id = base64_decode($id);
		if($this->input->post('js_id') && $this->input->post('js_id')!='')
		{
			$id = base64_decode($this->input->post('js_id'));
		}
		
		$page_title = $this->lang->line('js_public_profile_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$this->data['js_details'] = $this->share_profile_model->get_js_details($id);
		if($id !='' && $user_agent!='' && $this->data['js_details']!='' && is_array($this->data['js_details']) && count($this->data['js_details']) > 0)
		{
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_header_employer($page_title);
			}
			else
			{
				$this->common_front_model->__load_header($page_title);
			}
		    $this->load->view('front_end/js_public_profile_view',$this->data);
		    if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_footer_employer($page_title);
			}
			else
			{
				$this->common_front_model->__load_footer($page_title);
			}
		}
		else
		{
			 
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_header_employer($page_title);
			}
			else
			{
				$this->common_front_model->__load_header($page_title);
			}
			 $this->load->view('front_end/404_view',$this->data);
			 if($this->common_front_model->checkLoginfrontempl())
			 {
				$this->common_front_model->__load_footer_employer($page_title);
			 }
			 else
			 {
				$this->common_front_model->__load_footer($page_title);
			 }
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['js_img'] = 'assets/js_photos';
			$data['status'] = 'success';
			$data['js_details'] = $this->data['js_details'];
			if($this->data['js_details'] == '')
			{
				$data['status'] = 'error';
			}
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
		
	}
	
	function emp_profile($id='')
	{
		$id = base64_decode($id);
		if($this->input->post('emp_id') && $this->input->post('emp_id')!='')
		{
			$id = base64_decode($this->input->post('emp_id'));
		}
		$page_title = $this->lang->line('emp_public_profile_title');
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		$this->data['emp_details'] = $this->share_profile_model->get_emp_details($id);
		if($id !='' && $user_agent!='' && $this->data['emp_details']!='' && is_array($this->data['emp_details']) && count($this->data['emp_details']) > 0)
		{
			
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_header_employer($page_title);
			}
			else
			{
				$this->common_front_model->__load_header($page_title);
			}
		    $this->load->view('front_end/emp_public_profile_view',$this->data);
		    
			 if($this->common_front_model->checkLoginfrontempl())
			 {
				$this->common_front_model->__load_footer_employer($page_title);
			 }
			 else
			 {
				$this->common_front_model->__load_footer($page_title);
			 }
		}
		else
		{
			 
			 	if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
			 $this->load->view('front_end/404_view',$this->data); 
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_footer_employer($page_title);
			}
			else
			{
				$this->common_front_model->__load_footer();
			}
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['emp_img'] = 'assets/emp_photos';
			$data['comp_img'] = 'assets/company_logos';
			$data['status'] = 'success';
			$data['emp_details'] = $this->data['emp_details'];
			$data['ind_hire_name'] = '';
			$data['fn_area_hire_name'] = '';
			$data['skill_hire_name'] = '';
			$data['fn_area_name'] = '';
			$data['role_name'] = '';
				
			if($this->data['emp_details'] == '')
			{
				$data['status'] = 'error';
			}
			else
			{
				
				if($data['emp_details']['industry_hire']!='' && $data['emp_details']['industry_hire']!=0)
				{
				    $data['ind_hire_name'] = implode(',',$this->share_profile_model->get_emp_hiring_sector_name('industries_master',$data['emp_details']['industry_hire'],'industries_name'));
				}
				if($data['emp_details']['function_area_hire']!='' && $data['emp_details']['function_area_hire']!=0)
				{
					$data['fn_area_hire_name'] = implode(',',$this->share_profile_model->get_emp_hiring_sector_name('functional_area_master',$data['emp_details']['function_area_hire'],'functional_name'));
				}
				if($data['emp_details']['skill_hire']!='' && $data['emp_details']['skill_hire']!=0)
				{
					$data['skill_hire_name'] = implode(',',$this->share_profile_model->get_emp_hiring_sector_name('key_skill_master',$data['emp_details']['skill_hire'],'key_skill_name'));
				}
				if($data['emp_details']['functional_area']!='' && $data['emp_details']['functional_area']!=0)
				{
					$data['fn_area_name'] = implode(',',$this->share_profile_model->get_emp_hiring_sector_name('functional_area_master',$data['emp_details']['functional_area'],'functional_name'));
				}
				if($data['emp_details']['designation']!='' && $data['emp_details']['designation']!=0)
				{
					$data['role_name'] = implode(',',$this->share_profile_model->get_emp_hiring_sector_name('role_master',$data['emp_details']['designation'],'role_name'));
				}
			}
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
		
	}
	
	function download_resume()
	{
		if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
		{ 
			$emp_id_stored_plan = $this->common_front_model->get_empid();
			$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','cv_access_limit');
		}
		$user_agent = $this->input->post('user_agent');
		$file_com = $this->input->post('file');
		$js_id_com = $this->input->post('jidpkd');
		$file = base64_decode($file_com);
		$js_id = base64_decode($js_id_com);
		if($user_agent!='' && $file!='') 
		{
			if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='' && isset($return_data_plan_emp) && $return_data_plan_emp == 'Yes' && $js_id!='')
			{
				$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1); 
				$file_path = $this->data['base_url'].'assets/resume_file/'.$file;
				$data['status'] = 'success';
				$data['file_download'] = $file_path;
				$this->common_front_model->check_for_plan_update($emp_id_stored_plan,'employer',$js_id,'resume');	
			}
			else
			{
				$data['errmessage'] = "Please upgrade your membership to access the c.v";
				$data['status'] =  'error';	
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
		
}
?>