<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Job_listing extends CI_Controller
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin(); 
		$this->data['base_url'] = $this->base_url = base_url();
		$this->load->model('front_end/employer_post_job_model');
		$emp_id = $this->common_front_model->get_empid();
		if($emp_id!='')
		{
			$where_arra = array('id'=>$emp_id,'status'=>'APPROVED','is_deleted'=>'No');
			$this->data['emp_data_values'] = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'status,id');
		}
		else
		{
			//redirect($base_url);
		}
	}
	function check_seeker_employer_header($page_title)
	{
		if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid() == '')
		    {
				 $this->common_front_model->__load_header($page_title);
			}
			else
			{
				 $this->common_front_model->__load_header_employer($page_title);
			}
			 
	}
	function check_seeker_employer_footer()
	{
		if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid() == '')
		    {
				 $this->common_front_model->__load_footer();
				
			}
			else
			{
				 $this->common_front_model->__load_footer_employer();
			}
			 
	}
	public function index()
	{  
			if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid() == '')
		    {
				redirect($this->data['base_url']);
			}
			$page_title = 'Post new job';
			$this->common_front_model->__load_header_employer($page_title);
			$this->load->view('front_end/post_job_view',$this->data);
			$this->common_front_model->__load_footer_employer();
	}
	
	function edit_posted_job($job_id)
	{
		    $job_id = base64_decode($job_id);
		    $page_title = $this->lang->line('edit_job_title');
			$this->common_front_model->__load_header_employer($page_title);
			$edit_posted_job = $this->employer_post_job_model->edit_posted_job($job_id);
			if($edit_posted_job['status'] == 'success')
			{
				$this->data['edit_job_data'] = $edit_posted_job['edit_job_data'];
				$this->load->view('front_end/post_job_view',$this->data);
			}
			else
			{
				$this->load->view('front_end/404_view',$this->data);
			}
			$this->common_front_model->__load_footer_employer();
	}
	function view_job_details($job_id='')
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($user_agent == 'NI-WEB')
		{
			$job_id = base64_decode($job_id);
		}
		else
		{
		    $job_id = $this->input->post('job_id');
		}
		
		if($user_agent == 'NI-WEB')
		{
			$page_title = $this->lang->line('view_job_title');
			$this->check_seeker_employer_header($page_title);
		}
		$job_dtails = $this->employer_post_job_model->view_job_details($job_id);
		
		if($user_agent == 'NI-WEB')
				{
				 if($job_dtails['status'] == 'success')
					{
						$this->data['job_details'] = $job_dtails['job_details'];
						$this->data['status'] = 'success';
						$this->load->view('front_end/job_full_details_view',$this->data);
						
					}
					else
					{
						$this->data['status'] = 'error';
						$this->load->view('front_end/404_view',$this->data);
						
					}
				}
		if($user_agent == 'NI-WEB')
			{
					$this->check_seeker_employer_footer();
			}
			
			if($user_agent != 'NI-WEB')
			{
				$new_array  = array();
				$data['status']= $job_dtails['status'];
				$new_array[] =  $job_dtails['job_details'];
				//$new_array_edu = $this->employer_post_job_model->process_job_posted_data($new_array);
				$data['job_details'] = $this->employer_post_job_model->process_job_posted_data($new_array);
				//OR
				//$data['job_details'] = $this->employer_post_job_model->job_education_name($new_array);
				//$data['job_details'] = $new_array ;
				$this->output->set_content_type('application/json');
			    $this->output->set_output(json_encode($data));
			}
	}
		
	function valid_url_cus($url)
	{
	
	$pattern = "%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i";
		if (!preg_match($pattern, $url) && $url!='')
		{
		$this->form_validation->set_message('valid_url_cus', 'The URL you entered is not correct.');
		return FALSE;
		}
		else
		{
			return true;
		}
	}
	
	function save_job()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		$this->load->library('form_validation');
		$emp_id_stored_plan = $this->common_front_model->get_empid();
		$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','job_post_limit');
		if($return_data_plan_emp != 'Yes')
		{
			$this->form_validation->set_rules('plan_val', 'Plan validity', 'trim|callback_planvalidity');
		}
		
		if($user_agent!='' && $action=='save_job')
		{
			
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				   $this->form_validation->set_rules('job_title',$this->lang->line('job_title'), 'required');
				   $this->form_validation->set_rules('job_description', $this->lang->line('job_description'), 'required');
				   $this->form_validation->set_rules('skill_keyword', $this->lang->line('skill_keyword'), 'required');
				   $this->form_validation->set_rules('total_requirenment', $this->lang->line('total_requirenment'), 'required');
				   $this->form_validation->set_rules('location_hiring[]', $this->lang->line('location_hiring'), 'required');
				   $this->form_validation->set_rules('company_hiring', $this->lang->line('company_hiring'), 'required');
				   $this->form_validation->set_rules('job_industry', $this->lang->line('job_industry'), 'required');
				   $this->form_validation->set_rules('functional_area', $this->lang->line('job_functional_area'), 'required');
				   $this->form_validation->set_rules('job_post_role', $this->lang->line('job_post_role'), 'required');
				   $this->form_validation->set_rules('link_job', $this->lang->line('link_job'), 'valid_url|callback_valid_url_cus');
				   $this->form_validation->set_rules('job_education[]', $this->lang->line('job_education'), 'required');
				   $this->form_validation->set_rules('currently_hiring_status', $this->lang->line('currently_hiring'), 'required');
				   
			}
			elseif($user_agent!='' && $user_agent!='NI-WEB')
			{
				$this->form_validation->set_rules('functional_area', $this->lang->line('job_functional_area'),'trim');	
			}	
			
			if($this->form_validation->run() == FALSE )//$user_agent=='NI-WEB' &&  
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->employer_post_job_model->save_job();
				if($response == 'success')
				{
					if($this->input->post('mode') && $this->input->post('mode')=='edit')
					{
						$data['errmessage'] = $this->lang->line('edit_job_success_msg');
					}
					else
					{
						$data['errmessage'] = $this->lang->line('post_job_success_msg');
						if($return_data_plan_emp == 'Yes')
						{
							$this->common_front_model->update_plan_detail($emp_id_stored_plan,'employer','job_post_limit');
						}//if plan is valid
					}
					$data['status'] =  'success';
				}
				else
				{
					//$data['errmessage'] =  $response;
					$data['errmessage'] =  'Sorry your job could not be added';
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	function posted_job($page=1)
	{   
		$this->common_front_model->set_orgin();
		$user_agent = 'NI-WEB';
		$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
		$page_title = $this->lang->line('view_job_title');
		
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		
		$response = $this->employer_post_job_model->posted_job($page);
		//echo $response['total_post_count'];
		$this->data['total_post_count'] = $response['total_post_count'];
		$this->data['emp_posted_job'] = $response['emp_posted_job'];
		if($user_agent=='NI-WEB')
		{
			$this->data['custom_lable'] = $this->lang;
			if($this->ajax_search == 0)
			{
				$this->check_seeker_employer_header($page_title);
				$this->load->view('front_end/job_result_view',$this->data);
			}
			else
			{
				$this->load->view('front_end/page_part/job_search_result_view_new',$this->data);
			}
			if($this->ajax_search == 0)
			{
				$this->check_seeker_employer_footer();
			}
		}
		if($user_agent!='' && $user_agent!='NI-WEB')
		{	
			
			$this->output->set_content_type('application/json');			
			$data['total_post_count'] = $response['total_post_count'];
			$data['emp_posted_job'] = $this->employer_post_job_model->process_job_posted_data($response['emp_posted_job']);
			$this->output->set_output(json_encode($data));
		}
	}
	
	function add_to_highlight()
	{
		$user_agent = $this->input->post('user_agent');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='')
		{
			$response = $this->employer_post_job_model->add_to_highlight();
			if($response['status'] == 'success' )
			{
				$data['errmessage'] = $this->lang->line('add_to_highlight_success');
			    $data['status'] =  'success';
			}
			elseif($response['status'] == 'error_plan')
			{
				$data['errmessage'] =  'Sorry ! You do not have credit left to highlight this job.';
				$data['status'] =  'error';
			}
			else
			{
			  $data['errmessage'] = $this->lang->line('add_to_highlight_error');
			  $data['status'] =  'error';
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	function suggested_job($page=1)
	{       
			 $this->common_front_model->set_orgin(); 
			$get_userid = $this->common_front_model->get_userid();
			if($get_userid=='')
			{
				redirect($base_url);
			}
		    $user_agent = 'NI-WEB';
			$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
			$page_title = $this->lang->line('suggested_job_title');
			$user_agent = 'NI-WEB';
			if($this->input->post('user_agent'))
			{
				$user_agent = $this->input->post('user_agent');
			}
			
			$response = $this->employer_post_job_model->suggested_job($page);
			//echo $response['total_post_count'];
		    $this->data['total_post_count'] = $response['total_post_count'];
			$this->data['emp_posted_job'] = $response['emp_posted_job'];
			$this->data['custom_lable'] = $this->lang;
			$this->data['get_page_access_name'] = 'suggested_job';
			if($this->ajax_search == 0)
				{
					$this->common_front_model->__load_header($page_title);
					$this->load->view('front_end/job_suggested_result_view',$this->data);
				}
				else
				{
					$this->load->view('front_end/page_part/job_search_result_view',$this->data);
				}
			
			 if($this->ajax_search == 0)
				{
					$this->common_front_model->__load_footer();
				}
		if($user_agent!='' && $user_agent!='NI-WEB')
		{	
			$data['status'] = 'error';
			if($response['total_post_count']!='' && $response['total_post_count'] > 0)
			{
				$data['status'] = 'success';
			}
			$data['total_post_count'] = $response['total_post_count'];
			$data['emp_posted_job'] = $this->employer_post_job_model->process_job_posted_data($response['emp_posted_job']);
			//$data['emp_posted_job'] = $response['emp_posted_job'];
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
		}		
	}
	public function planvalidity()
	{	
		$this->form_validation->set_message('planvalidity', 'Note ! Please upgrade your membership to post a job.');
		return FALSE;
	}
	function get_search_suggestion($search = '')
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		$search = $search; 
		$str_ddr = array();
		$str_ddr = $this->employer_post_job_model->get_search_suggestion($search);
		if($user_agent !='NI-WEB')
		{
			$str_ddrdata['data']  = $str_ddr;
		}
		else
		{
			$str_ddrdata = $str_ddr; 
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddrdata));
	}
	function get_salary_to()
	{
		$salary_to = $this->input->post('salary_to');
		$data['salary_to']  = $salary_to;
		$data['status']  = 'success';
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function check_city()
	{
	    $location = $this->input->post('location_home_search');
		$location_home_search = explode(',',$location);
		$error_msg_arr = array();
		$err_status = 1;
		if(isset($location_home_search) && $location_home_search !='' && is_array($location_home_search) && count($location_home_search) > 0)
		{
			foreach($location_home_search as $search_data)
			{
				if(!is_numeric($search_data))
				{
					//echo $search_data;
					$where = "city_name  LIKE('".trim($search_data)."')";
					if($where!='')
					{
						$where = "($where) AND is_deleted='No'";
					}
					else
					{
						$where = "is_deleted='No'";
					}
							
					
					$city_data = $this->common_front_model->get_count_data_manual('city_master',$where,0);
					
					if($city_data<=0)
					{
					  $error_msg_arr[] = $search_data;
					  $err_status = 0;
					}
					
				}
			}
		}
		if($err_status==0){
			echo "These are invalid locations ".implode(",",$error_msg_arr);
		}else{
			echo 1;
		}
	}
}
?>