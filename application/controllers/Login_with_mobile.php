<?php

header('Content-type: application/json');

class Login_with_mobile extends MY_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('login_model');
    }

    function index() {
        //Inputs: mobile, (If Social Login then  facebook_account_id, google_account_id)
        $type = "";
        $mobile = $this->input->get_post("mobile");
        if (!$mobile) {
            $arr = array('err_code' => "invalid", "message" => "Mobile is required");
            echo json_encode($arr);
            die;
        }
        $pattern = "/^[7-9][0-9]{9}$/i";
        if (!preg_match($pattern, $mobile)) {
            $arr = array('err_code' => "invalid", "message" => "Mobile is Invalid");
            echo json_encode($arr);
            die;
        }

     /*   if ($this->input->get_post("facebook_account_id")) {
            $type = 'facebook';
            $account_id = $this->input->get_post("facebook_account_id");
        } else if ($this->input->get_post("google_account_id")) {
            $type = 'google';
            $account_id = $this->input->get_post("google_account_id");
        }*/

        $data = $this->db->get_where("jobseeker", ["mobile" => $this->input->get_post('mobile')])->row();
        if ($data) {

           /* if ($type == "google" && $data->google_account_id != $account_id) {
                $this->db->set("google_account_id", $account_id);
                $this->db->where("id", $data->id);
                $this->db->update("jobseeker");
                $data->google_account_id = $account_id;
            } else if ($type == "facebook" && $data->facebook_account_id != $account_id) {
                $this->db->set("facebook_account_id", $account_id);
                $this->db->where("id", $data->id);
                $this->db->update("jobseeker");
                $data->facebook_account_id = $account_id;
            }*/

            unset($data->password);
            unset($data->salt);
            unset($data->role_id);
            unset($data->email_verified);
            unset($data->email_verification_key);
            unset($data->forgot_password_verfication_code);
            unset($data->password_link_expiry_time);
            unset($data->created_at);
            unset($data->updated_at);
            unset($data->status);

            if ($this->input->get_post("auto_login") == 1) {

            } else {
                $otp = $this->customers_otp_model->send_verfication_otp($data->id);
            }

            
            unset($data->id);
            $arr = array(
                'err_code' => "valid",
                "message" => "User Details found",
                "type" => "details_found",
                //"otp" => $otp,
                "user_details" => $data
            );
        } else {
            if ($this->input->get_post("auto_login") == 1) {

            } else {
                $otp = $this->customers_otp_model->send_not_registered_customer_verification_code($mobile);
            }

            $arr = array(
                'err_code' => "valid",
                "message" => "User Details not found",
                "type" => "details_not_found",
                    "otp" => $otp,
            );
        }
        echo json_encode($arr);
    }

    function verify_otp() {
        $mobile = $this->input->get_post("mobile");
        $otp = $this->input->get_post("otp");
        if (!$mobile) {
            $arr = array('err_code' => "invalid",
                "error_type" => "invalid_mobile",
                "title" => "Mobile Number required",
                "message" => "Please enter a valid mobile Number",
                "mobile" => $this->input->get_post("mobile"),
            );
            echo json_encode($arr);
            die;
        }
        if (!$otp) {
            $arr = array('err_code' => "invalid",
                "error_type" => "invalid_otp",
                "title" => "Mobile Number required",
                "message" => "Please enter a valid mobile Number",
                "otp" => $this->input->get_post("otp"),
            );
            echo json_encode($arr);
            die;
        }



        $response = $this->customers_otp_model->verify_otp($mobile, $otp);

        $is_mobile_registered = $this->db->get_where("jobseeker", ["mobile" => $mobile]);

        if ($response) {
            if ($is_mobile_registered->num_rows() == 0) {

                $access_token = $this->customers_register_model->generate_access_token();

                if ($this->customers_register_model->check_email_existance($this->input->get_post("email")) == TRUE) {
                    $arr = array('err_code' => "invalid", "error_type" => "email_already_registered", "message" => "Email already registered");
                    echo json_encode($arr);
                    die;
                }

                $data = array(
                    "customer_name" => "",
                    "email" => "",
                    "mobile" => $this->input->get_post('mobile', true),
                    "password" => rand(156112, 999999),
                    "access_token" => $access_token
                );
                $response = $this->customers_register_model->register($data);

                $arr = array(
                    'err_code' => "valid",
                    "message" => "OTP Verified successfully",
                    "mobile" => $mobile,
                    "type" => "details_not_found",
                    "access_token" => $access_token
                );
            } else {
                $data = $this->db->get_where("jobseeker", ["mobile" => $this->input->get_post('mobile')])->row();
                $addresses = $this->customers_address_model->get($data->id);
                if (count($addresses)) {
                    $data->default_location = $addresses[0];
                } else {
                    //$data->default_location = [];
                }

                unset($data->password);
                unset($data->salt);
                unset($data->role_id);
                unset($data->email_verified);
                unset($data->email_verification_key);
                unset($data->forgot_password_verfication_code);
                unset($data->password_link_expiry_time);
                unset($data->created_at);
                unset($data->updated_at);
                unset($data->status);
                if (!isset($data->alternate_mobile)) {
                    $data->alternate_mobile = "";
                }
                if (!isset($data->referral_code)) {
                    $data->referral_code = "";
                }
                $arr = array(
                    'err_code' => "valid",
                    "message" => "OTP Verified successfully",
                    "mobile" => $mobile,
                    "type" => "details_found",
                    "user_details" => $data
                );
            }
        } else {
            $arr = array('err_code' => "invalid",
                "error_type" => "invalid_otp",
                "title" => "Invalid Verification Code",
                "message" => "Please enter a valid verification code",
                "mobile" => $this->input->get_post("mobile"),
            );
        }
        echo json_encode($arr);
    }

}
