<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sign_up_employer extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin();
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->load->model('front_end/employer_sign_up');
	}
	public function index()
	{
		if($this->common_front_model->checkLoginfrontempl())
		{
			redirect($this->common_front_model->base_url.'employer_profile');
		}
		
		$this->data['pers_title'] = $this->common_front_model->get_personal_titles() > 0 ? $this->common_front_model->get_personal_titles() : '';
		if($this->session->has_userdata('employer_reg'))
		{
			$allsessionstoredforreg = $this->session->userdata('employer_reg');
			$sessionstoredforreg = end($allsessionstoredforreg);
			if($sessionstoredforreg['emp_reg_sess']!='' && is_array($sessionstoredforreg) && count($sessionstoredforreg) > 0)
			{
				$employer_exists = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
				if(isset($employer_exists) && $employer_exists!='' && is_array($employer_exists) && count($employer_exists) > 0)
				{
					$this->data['employer_details'] = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
					$this->data['employer_details']['forstep2'] = $this->data['employer_details'];
				}
				else
				{
					$this->data['employer_details'] = "";
				}
			}
		}
		$this->common_front_model->__load_header_employer($this->data['custom_lable']->language['sign_up_emp_header_title']);
		$this->load->view('front_end/sign_up_view_emp',$this->data);
		$this->common_front_model->__load_footer_employer();
	}
	public function signupemp()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'title', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformtitle'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'fullname',
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformfullname'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'emailid_confirmation', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformemail'],
                     'rules'   => 'trim|required|valid_email|callback_email_unique'
                  ),
               array(
                     'field'   => 'emailid', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformemailcnf'], 
                     'rules'   => 'trim|required|valid_email|matches[emailid_confirmation]'
                  ),
               array(
                     'field'   => 'pass_confirmation', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformpasscnf'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'pass', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformpass'], 
                     'rules'   => 'trim|required|matches[pass_confirmation]'
                  )
            );
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				//$data['data'] = $this->output->set_output(json_encode($data_return));
				$this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_sign_up->add_employer_detail();
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					if($user_agent!='' && ($user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
					{
						$data_return['employer_id_inserted'] = isset($return['employer_id_inserted']) ? $return['employer_id_inserted'] : $this->data['custom_lable']->language['notavilablevar'];
					}
					$data_return['stepnoreturn'] = $return['stepnoreturn'];
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_reg_data_ins_succ'];
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['emp_reg_data_not_ins'];
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			//$data['data'] = $this->output->set_output(json_encode($data_return));
			$this->output->set_output(json_encode($data_return));
		}
	}
	public function signupemp2()
	{  
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'company_name', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_companydet'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'company_type', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmptypelbl'],
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'company_size', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmpsjizelbl'],
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'industry', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_industrylbl'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'mobile_c_code', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformmobcode'], 
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'mobile_num', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformmob'], 
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'country', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformcnt'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'city', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformcity'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'address', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformaddress'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'pincode', 
                     'label'   => $this->data['custom_lable']->language['pincode'],
                     'rules'   => 'trim|required'
                  )
			 
            );
			if($this->input->post('company_website') && $this->input->post('company_website')!='')
			{
				array_push($config,array(
                     'field'   => 'company_website',
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmpweblbl'],
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus'
                  ));
			}
			
			$this->form_validation->set_rules($config);
			
			/*mobile duplicate check*/
			/*if($this->session->userdata('employer_reg'))
			 {*/
			    
				if($this->session->userdata('employer_reg'))
			 	{
					$allsessionstoredforreg = $this->session->userdata('employer_reg');
				}
				if(isset($allsessionstoredforreg) && $allsessionstoredforreg!='' && is_array($allsessionstoredforreg))
				{
					$sessionstoredforreg = end($allsessionstoredforreg);
					$emp_id = $sessionstoredforreg['emp_reg_sess_id'];
					
				}
				else
				{
					$emp_id = $this->input->post('user_id');
				}
				$mobile_c_code = $this->input->post('mobile_c_code');
				$mobile_no = $this->input->post('mobile_num');
				$mobile = $mobile_c_code.'-'.$mobile_no;
				//$where = array("mobile='".$mobile."' and is_deleted='No'");
				$where = array('mobile'=>$mobile,'id!='=>$emp_id,'is_deleted'=>'No');
				$checkifmobile_exits = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','','','','','');
				
			//}			
			/*mobile duplicate check*/
			
			if($this->form_validation->run() == FALSE)
			{
				//$data_return['errormessage'] =  validation_errors();
				$data_return['errormessage'] = strip_tags(validation_errors());
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				//$data['data'] = $this->output->set_output(json_encode($data_return));
				$this->output->set_output(json_encode($data_return));
			}
			else
			{
				
				if($checkifmobile_exits==0)
				{
					$return = $this->employer_sign_up->add_employer_detail2();
					
					if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success')
					{
						$data_return['status'] = "success";
						if($user_agent!='' && ($user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && $this->input->post('user_id') && $this->input->post('user_id')!='')
						{
							$data_return['employer_id_inserted'] = isset($return['employer_id_inserted']) ? $return['employer_id_inserted'] : $this->data['custom_lable']->language['notavilablevar'];
						}
						$data_return['stepnoreturn'] = $return['stepnoreturn'];
						$data_return['successmessage'] = $this->data['custom_lable']->language['emp_reg_data_ins_succ'];		
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
						//$this->output->set_output(json_encode($data_return));
					}
					else
					{
						$data_return['status'] = "error";	
						if(isset($return) && $return!='' && isset($return['result']) && $return['result']=="file_upload_error")
						{
							$data_return['errormessage'] = 'Looks like something is wrong with your image.Please try with other images.';
						}
						else
						{
							$data_return['errormessage'] = $this->data['custom_lable']->language['emp_reg_data_not_ins'];	
						}
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
						//$this->output->set_output(json_encode($data_return));
					}
				}
				else
				{
					$data_return['errormessage'] =  "Mobile number already exists. Please use a different mobile phone number.";
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			//$data['data'] = $this->output->set_output(json_encode($data_return));
			$this->output->set_output(json_encode($data_return));
		}
	}
	public function signupemp3()
	{  
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			/*$config = array(
               array(
                     'field'   => 'currentdesignation', 
                     'label'   => 'Current designation', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'join_month', 
                     'label'   => 'Join month',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'join_year', 
                     'label'   => 'Join year',
                     'rules'   => 'trim|required'
                  )
            );*/
			if($user_agent!='NI-AAPP' && $user_agent!='NI-IAPP')
			{
				$this->form_validation->set_rules('currentdesignation[]', 'Current designation','trim');//|callback_multiple_select
				$this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim');
				$this->form_validation->set_rules('function_area_hire[]', 'Industries', 'trim');
				$this->form_validation->set_rules('skill_hire[]', 'Industries', 'trim');
				if($this->input->post('currentdesignation')!='' && is_array($this->input->post('currentdesignation')) && count($this->input->post('currentdesignation')) > 0 )
				 {
					 if($this->chechall_filds_filled($this->input->post('currentdesignation')) == false)
					 {
						$this->form_validation->set_rules('currentdesignation[]', 'Current designation', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_des_mes']));
					 }
				 }
				 elseif($this->input->post('currentdesignation')=='' || !is_array($this->input->post('currentdesignation')) || (!(count($this->input->post('currentdesignation')) > 0)))
				 {
					 $this->form_validation->set_rules('currentdesignation[]', 'Current designation', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_des_mes1']));
				 }
				$this->form_validation->set_rules('functional_area[]', 'Functional area','trim');//|callback_multiple_select
				if($this->input->post('functional_area')!='' && is_array($this->input->post('functional_area')) && count($this->input->post('functional_area')) > 0)
				 {
					 if($this->chechall_filds_filled($this->input->post('functional_area')) == false)
					 {
						$this->form_validation->set_rules('functional_area[]', 'Funcitonal area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_mes']));
					 }
				 }
				 elseif( $this->input->post('functional_area')=='' || !is_array($this->input->post('functional_area')) || (!(count($this->input->post('functional_area')) > 0)))
				 {
					 $this->form_validation->set_rules('functional_area[]', 'Funcitonal area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_mes1']));
				 }
				 if($this->input->post('industry_hire')!='' && is_array($this->input->post('industry_hire')) && count($this->input->post('industry_hire')) > 0 )
				 {
					 if($this->chechall_filds_filled($this->input->post('industry_hire')) == false)
					 {
						$this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_in_hire_mes']));
					 }
				 }
				 elseif($this->input->post('industry_hire')=='' || !is_array($this->input->post('industry_hire')) || (!(count($this->input->post('industry_hire')) > 0)))
				 {
					 $this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_in_hire_mes1']));
				 }
				 if($this->input->post('function_area_hire')!='' && is_array($this->input->post('function_area_hire')) && count($this->input->post('function_area_hire')) > 0)
				 {
					 if($this->chechall_filds_filled($this->input->post('function_area_hire')) == false)
					 {
						$this->form_validation->set_rules('function_area_hire[]', 'Functional area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_hire_mes']));
					 }
				 }
				 elseif($this->input->post('function_area_hire')=='' || !is_array($this->input->post('function_area_hire')) || (!(count($this->input->post('function_area_hire')) > 0)) )
				 {
					 $this->form_validation->set_rules('function_area_hire[]', 'Functional area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_hire_mes1']));
				 }
				 if($this->input->post('skill_hire')!='' && is_array($this->input->post('skill_hire')) && count($this->input->post('skill_hire')) > 0 )
				 {
					 if($this->chechall_filds_filled($this->input->post('skill_hire')) == false)
					 {
						$this->form_validation->set_rules('skill_hire[]', 'Skills', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_skill_hire_mes']));
					 }
				 }
				 elseif($this->input->post('skill_hire')=='' || !is_array($this->input->post('skill_hire')) || (!(count($this->input->post('skill_hire')) > 0)))
				 {
					 $this->form_validation->set_rules('skill_hire[]', 'Skills', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_skill_hire_mes1']));
				 }
			}
			else
			{
				$this->form_validation->set_rules('currentdesignation', 'Current designation','trim');//|callback_multiple_select
				$this->form_validation->set_rules('industry_hire', 'Industries', 'trim');
				$this->form_validation->set_rules('function_area_hire', 'Industries', 'trim');
				$this->form_validation->set_rules('skill_hire', 'Industries', 'trim');
			}
			//$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				//$data['data'] = $this->output->set_output(json_encode($data_return));
				$this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_sign_up->add_employer_detail3();
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					if($user_agent!='' && ($user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && $this->input->post('user_id') && $this->input->post('user_id')!='')
					{
						$data_return['employer_id_inserted'] = isset($return['employer_id_inserted']) ? $return['employer_id_inserted'] : $this->data['custom_lable']->language['notavilablevar'];
					}
					$data_return['stepnoreturn'] = $return['stepnoreturn'];
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_reg_data_ins_succ'];		
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['emp_reg_data_not_ins'];
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			//$data['data'] = $this->output->set_output(json_encode($data_return));
			$this->output->set_output(json_encode($data_return));
		}
	}
	public function signupemp4()
	{  
	
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		
		if(isset($_POST['profile_pic']) && $_POST['profile_pic']!='')
		{
			$image = $_POST['profile_pic'];
		}
		else
		{
			$image = (isset($_FILES['profile_pic']) && $_FILES['profile_pic']!='' && is_array($_FILES['profile_pic']) && count($_FILES['profile_pic']) > 0) ? $_FILES['profile_pic'] : "";
		}
		
		if($image!=''  && $user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			if (empty($_FILES['profile_pic']))
			{
				$config = array(
				   array(
						 'field'   => 'profile_pic',
						 'label'   => 'Profile picture', 
						 'rules'   => 'trim|required'
					  ),
				);
				$this->form_validation->set_rules($config);
			}
			if($this->form_validation->run() == FALSE && $image=='')
			{
				$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				//$data['data'] = $this->output->set_output(json_encode($data_return));
				$this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_sign_up->add_employer_detail4();
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					if($user_agent!='' && ($user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && $this->input->post('user_id') && $this->input->post('user_id')!='')
					{
						$data_return['employer_id_inserted'] = isset($return['employer_id_inserted']) ? $return['employer_id_inserted'] : $this->data['custom_lable']->language['notavilablevar'];
					}
					$data_return['stepnoreturn'] = $return['stepnoreturn'];
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_reg_data_ins_succ_final'];
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = (isset($return['returnmess']) && $return['returnmess']!='') ? $return['returnmess'] : $this->data['custom_lable']->language['emp_reg_data_not_ins'];
					$this->output->set_content_type('application/json');
					//$data['data'] = $this->output->set_output(json_encode($data_return));
					$this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			//$data['data'] = $this->output->set_output(json_encode($data_return));
			$this->output->set_output(json_encode($data_return));
		}
	}
	function email_unique($str)
    {
		if($this->session->has_userdata('employer_reg'))
		{
			$allsessionstoredforreg = $this->session->userdata('employer_reg');
			$sessionstoredforreg = end($allsessionstoredforreg);
			$currentregemailfromsess = "and email!='".$sessionstoredforreg['emp_reg_sess']."'";
		}
		else
		{
			$currentregemailfromsess = "";
		}
		$where = array("email='".$str."' and is_deleted='No' $currentregemailfromsess");
		$checkifemail_exits = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','','','','','');
		if ($checkifemail_exits > 0)
        {
            $this->form_validation->set_message('email_unique', $this->data['custom_lable']->language['email_of_employer_already_exits']);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
	function get_form_part($part)
    {	
		$data1['base_url'] = $this->common_front_model->base_url;
		$data1['custom_lable'] = $this->data['custom_lable'];
		
		if($this->session->has_userdata('employer_reg'))
		{
			$allsessionstoredforreg = $this->session->userdata('employer_reg');
			$sessionstoredforreg = end($allsessionstoredforreg);
			if($sessionstoredforreg['emp_reg_sess']!='' && is_array($sessionstoredforreg) && count($sessionstoredforreg) > 0)
			{
				$employer_exists = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
				if(isset($employer_exists) && $employer_exists!='' && is_array($employer_exists) && count($employer_exists) > 0)
				{
					$this->data['employer_details'] = $this->employer_sign_up->get_employerdetail_frm_email($sessionstoredforreg['emp_reg_sess']);
					$data1['forstep2'] = $this->data['employer_details'];
				}				
			}
		}
		
		//$data_return['content'] = $this->load->view('front_end/sign_up_emp_step'.$part,$data1,true);
		$data_return = $this->load->view('front_end/sign_up_emp_step'.$part,$data1,true);
		//$data_return['tocken'] = $this->security->get_csrf_hash();
		//header('Content-type: application/json');
		//$data['data'] = json_encode($data_return);
		$data['data'] = $data_return;
		$this->load->view('common_file_echo',$data);
		/*$this->output->set_content_type('application/json');*/
		//$data['data'] = $this->output->set_output(json_encode($data_return));
		/*$this->output->set_output(json_encode($data_return));*/
    }
	public function chechall_filds_filled($array_pass)
	{
		$onevaluempty = array();
		if(!empty($array_pass) && is_array($array_pass) && count($array_pass) > 0)
		 {
			foreach($array_pass as $array_pass_single)
			{
				if(trim($array_pass_single)=='')
				{
					$onevaluempty[] = 'no';
				}
				else
				{
					$onevaluempty[] = 'yes';
				}
			}
		 }
		 if(in_array('no',$onevaluempty))
		 {
			 return false;
		 }
		 else
		 {
			 return true;
		 }
	}
	public function get_suggestion($search='')
	{   
	    $search = $search; 
		$str_ddr = array();
		$str_ddr = $this->employer_sign_up->get_suggestion($search);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
	function valid_url_cus($url)
	{
		$pattern = "%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i";
		if (!preg_match($pattern, $url))
		{
			$this->form_validation->set_message('valid_url_cus', $this->data['custom_lable']->language['urlincorect']);
			return FALSE;
		}
		return TRUE;
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$this->index($method);
		}
	}
}