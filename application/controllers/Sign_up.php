<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sign_up extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('front_end/sign_up_model');
		$this->load->model('front_end/my_profile_model');
		
	}
	public function index($perameter ='signup')
	{  
		if($this->common_front_model->checkLoginfront())
		{
			redirect($this->common_front_model->base_url);
		}
	    $this->data['perameter'] = $perameter;
		$where = array("status"=>'APPROVED',"is_deleted"=>'No');
		$check_social_access = $this->common_front_model->get_count_data_manual('social_login_master',$where,'2','social_name,client_key,client_secret');
		if($check_social_access!='' && is_array($check_social_access) && count($check_social_access) > 0 )
		{ 
		   $this->data['social_access_detail'] = $check_social_access;
		   $this->data['gplus_detail'] = '';
		   if(isset($this->data['social_access_detail']['1']) && $this->data['social_access_detail']['1']!='')
		   {
		  	 $this->data['gplus_detail'] = $this->data['social_access_detail']['1'];
		   }
		}
		$where_arra = array('page_url'=>'jobseeker-registration-benefits','status'=>'APPROVED','is_deleted'=>'No');
		$this->data['jsregcms'] = $this->common_front_model->get_count_data_manual('cms_pages',$where_arra,1,'page_title,page_content');
		$this->common_front_model->__load_header("Sign Up / Login");
		$this->load->view('front_end/sign_up_view',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	/*function sign_up_success()
	{
		echo "success , remain to set success design page.design not ready";
	}*/
	
	
	function email_unique($str)
    {	
		$where = array("email='".$str."' and is_deleted='No'");
		$checkifemail_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
		if ($checkifemail_exits > 0)
				{
					$this->form_validation->set_message('email_unique', $this->lang->line('email_exits'));
					return FALSE;
				}
				else
				{
					return TRUE;
				}
    }
	
	function valid_url_cus($url)
	{
		
	$pattern = "%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i";
		if (!preg_match($pattern, $url) && $url!='')
		{
		$this->form_validation->set_message('valid_url_cus', 'The URL you entered is not correct.');
		return FALSE;
		}
		return TRUE;
	}
	
	public function add_account_detail()
	{
		$this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='sign_up')
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_unique');
			$email = $this->input->post('email');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
			   $this->form_validation->set_rules('gender', 'Gender ', 'required');
			   $fullname = $this->input->post('fullname');			   
			   $password = $this->input->post('password');
			   $c_password = $this->input->post('c_password');
			   $this->form_validation->set_rules('fullname', 'Fullname ', 'required');
			   $this->form_validation->set_rules('password', "Password ", 'required');
			   $this->form_validation->set_rules('c_password', "Repeat Password ", 'matches[password]');
			}
			else 
			{
				
			}
			//$user_agent=='NI-WEB'  &&  
			if($this->form_validation->run() == FALSE)
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->sign_up_model->add_account_detail();
				if($response == 'success')
				{
					$msg = $this->data['reg_acc_dtl_success_msg'] = $this->lang->line('reg_acc_dtl_success_msg');
					$data['errmessage'] =  $msg;
					$data['status'] =  'success';
					if($this->session->userdata('reg_index_id')!='')
					{
						$js_id = $this->session->userdata('reg_index_id');
						$data['reg_index_id'] =  $js_id;
					}
				}
				else
				{
					$data['errmessage'] =  $response;
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
		///$data1['data'] = json_encode($data);
		//$this->load->view('common_file_echo',$data1);
	}
	
	public function fb_signup()
	{
		$where = array("status"=>'APPROVED',"is_deleted"=>'No','id'=>'1');
		$get_fb_details = $this->common_front_model->get_count_data_manual('social_login_master',$where,'1','social_name,client_key,client_secret');
		if($get_fb_details!='' && is_array($get_fb_details) && count($get_fb_details) > 0 )
		{ 
		   $this->data['fb_detail'] = $get_fb_details;
		}
		$this->common_front_model->__load_header("Sign Up / Login");
		$this->load->view('front_end/fg_config',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	
	public function fb_signin()
	{
		$where = array("status"=>'APPROVED',"is_deleted"=>'No','id'=>'1');
		$get_fb_details = $this->common_front_model->get_count_data_manual('social_login_master',$where,'1','social_name,client_key,client_secret');
		if($get_fb_details!='' && is_array($get_fb_details) && count($get_fb_details) > 0 )
		{ 
		   $this->data['fb_detail'] = $get_fb_details;
		}
		$this->common_front_model->__load_header("Sign Up / Login");
		$this->load->view('front_end/fb_signin',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	public function gplus_login_signup()
	{
		$where = array("status"=>'APPROVED',"is_deleted"=>'No','id'=>'2');
		$get_gp_details = $this->common_front_model->get_count_data_manual('social_login_master',$where,'1','social_name,client_key,client_secret');
		if($get_gp_details!='' && is_array($get_gp_details) && count($get_gp_details) > 0 )
		{
		   $this->data['gplus_detail'] = $get_gp_details;
		   $this->data['base_url'] = $this->common_front_model->base_url;
		}
		$this->common_front_model->__load_header("Sign Up / Login");
		$this->load->view('front_end/gplus/gplus_login_signup',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	public function add_resume()
	{
		if($this->session->userdata('reg_index_id')=='')
		{
				redirect($this->common_front_model->base_url);
		}
		$this->common_front_model->__load_header("Add resume");
		$this->data['marital_status'] = $this->common_front_model->get_m_status();
		$this->data['country_code'] = $this->common_front_model->get_country_code();
		$this->load->view('front_end/add_resume_view',$this->data);
		$this->common_front_model->__load_footer();
	}
	
	public function add_personal_detail()
	{
		$this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='add_personal_detail')
		{
			$this->load->library('form_validation');
			
			$mobile_c_code = $this->input->post('mobile_c_code');
			$mobile_no = $this->input->post('mobile');
			$mobile = $mobile_c_code.'-'.$mobile_no;
			
			$js_id = $this->session->userdata('reg_index_id');
			$where = array('mobile'=>$mobile,'id!='=>$js_id,'is_deleted'=>'No');
			//$where = array("mobile='".$mobile."' and is_deleted='No'");
			$checkifmobile_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
			
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
			   $this->form_validation->set_rules('marital_status', "Marital status ", 'required');
			   $this->form_validation->set_rules('birthdate', "Birthdate ", 'required');
			   $this->form_validation->set_rules('mobile_c_code', "Mobile Country Code ", 'required');
			   $this->form_validation->set_rules('mobile', "Mobile ", 'required');
			   $this->form_validation->set_rules('country', "Country ", 'required');
			   $this->form_validation->set_rules('city', "City ", 'required');
			   $this->form_validation->set_rules('address', "Address ", 'required');
			}
			else 
			{
				
			}
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				if($checkifmobile_exits==0)
				{
					$response = $this->sign_up_model->add_personal_detail();
					if($response == 'success')
					{
						$msg = $this->data['reg_per_dtl_success_msg'] = $this->lang->line('reg_per_dtl_success_msg');
						$data['errmessage'] =  $msg;
						$data['status'] =  'success';
					}
					else
					{
						$data['errmessage'] =  $response;
						$data['status'] =  'error';
					}	
				}
				else
				{
					$data['errmessage'] =  "Mobile number already exists. Please use a different mobile phone number.";
				}
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function add_education_detail()
	{ 
	    $this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='add_education_detail')
		{
			$this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				$no_of_edu_row = 10; //$this->input->post('no_of_edu_row');
				for($i=1;$i <= $no_of_edu_row ; $i++)
				{
					$qualification_level = $this->input->post('qualification_level'.$i.'');
					if($qualification_level!='class-X' && $qualification_level!='class-XII' && $this->input->post('specialization'.$i.''))
				    {
						$this->form_validation->set_rules('specialization'.$i.'', "Specialization ", 'required');
					}
				
				 if(isset($_POST['institute'.$i]))
				 {
					  $this->form_validation->set_rules('institute'.$i.'', 'Institute name ', 'required');
				 }
				 
				 if( isset($_POST['qualification_level'.$i]) )
				 {
					  $this->form_validation->set_rules('qualification_level'.$i.'', "Qualification level ", 'required');
				 }
				 
				if( isset($_POST['marks'.$i]) )
				 {
					 $this->form_validation->set_rules('marks'.$i.'', "Percentage / grade ", 'required');
				 }
				 
				 if( isset($_POST['passing_year'.$i]))
				 {
					$this->form_validation->set_rules('passing_year'.$i.'', "Passing year ", 'required');
				 }
				}
				
				$no_of_certi_row = 10; //$this->input->post('no_of_certi_row');
				if($no_of_certi_row > 0 && $no_of_certi_row!='')
				{
					for($i=1;$i <= $no_of_certi_row ; $i++)
					{
						if( isset($_POST['cerificate_name'.$i]))
				 		{
							$this->form_validation->set_rules('cerificate_name'.$i.'', 'Certificate name', 'required');
						}
						if(  isset($_POST['cerificate_desc'.$i]) )
				 		{
							$this->form_validation->set_rules('cerificate_desc'.$i.'', "Certificate description", 'required');
						}
						if(  isset($_POST['passing_year_certi'.$i]) )
				 		{
							$this->form_validation->set_rules('passing_year_certi'.$i.'', "Certificate passing year ", 'required');
						}
					}
				}
			 }
			else 
			{
				
			}
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->sign_up_model->add_education_detail();
				//print_r($response);
				if($response['status'] == 'success')
				{
					$msg = $this->data['reg_edu_dtl_success_msg'] = $this->lang->line('reg_edu_dtl_success_msg');
					$data['errmessage'] =  $msg;
					$data['status'] =  'success';
					$data['form_edu_id'] = $response['form_edu_id'];   
					$data['form_certi_id'] = $response['form_certi_id'];   
				   
				}
				else
				{
					$data['errmessage'] =  $response;
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		//$data1['data'] = json_encode($data);
		//$this->load->view('common_file_echo',$data1);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function add_work_exp_detail()
	{ 
		$this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='add_work_exp_detail')
		{
			$this->load->library('form_validation');
			// && $user_agent=='NI-WEB'
			if($user_agent!='')
			{
				$no_of_work_exp_row = 3;//$this->input->post('no_of_work_exp_row');
				$l_date_count = 0;
				$l_date_warning = 0;
				for($i=1;$i <= $no_of_work_exp_row ; $i++)
				{
				 	
				   if(isset($_POST['leaving_date'.$i])  && $_POST['leaving_date'.$i]=='')
				   {
					   $l_date_count++;
					   $l_date_store = $i;
					   $data['leaving_date'.$i] = $_POST['leaving_date'.$i];
				   }
				   
				   if( isset($_POST['leaving_date'.$i])  && $_POST['leaving_date'.$i]!='')
				   {  
				   	   $l_date_store_arr[$i] = $this->input->post('leaving_date'.$i.'');
					   $l_date_store_arr_get_key[$i] = $this->input->post('leaving_date'.$i.'');
				   }
				  if( isset($_POST['leaving_date'.$i]) && isset($_POST['joining_date'.$i])  && isset($_POST['leaving_date'.$i])!='' && isset($_POST['joining_date'.$i])!='' && isset($_POST['leaving_date'.$i])  < isset($_POST['joining_date'.$i]) )
				   {
					   $l_date_warning++;
				   }
				   if( isset($_POST['company_name'.$i]) )
				   {
					   $this->form_validation->set_rules('company_name'.$i.'', 'Company name', 'required'); 
				   }
				   if( isset($_POST['industry'.$i]) )
				   {
					 $this->form_validation->set_rules('industry'.$i.'', "Type of industry", 'required');
				   }
				   if( isset($_POST['functional_area'.$i]) )
				   {
					 $this->form_validation->set_rules('functional_area'.$i.'', "Functional area ", 'required');
				   }
				   if( isset($_POST['joining_date'.$i]))
				   {
					 $this->form_validation->set_rules('joining_date'.$i.'', " Joining date ", 'required');
				   }
				   if( isset($_POST['job_role'.$i]) )
				   {
					  $this->form_validation->set_rules('job_role'.$i.'', "Job role", 'required');
				   }
				   /*if( isset($_POST['currency_type'.$i]) )
				   {
					   $this->form_validation->set_rules('currency_type'.$i.'', "Currency type", 'required');
				   }
				   if( isset($_POST['annual_salary_lacs'.$i]) )
				   {
					   $this->form_validation->set_rules('annual_salary_lacs'.$i.'', "Annual salary lacs", 'required');
				   }
				   if( isset($_POST['annual_salary_thousand'.$i]) )
				   {
					   $this->form_validation->set_rules('annual_salary_thousand'.$i.'', "Annual salary thousand", 'required');
				   }*/
				   if( isset($_POST['annual_salary'.$i]) )
				   {
					   $this->form_validation->set_rules('annual_salary'.$i.'', "Annual salary", 'required');
				   }
				   
				   //$this->form_validation->set_rules('leaving_date'.$i.'', "Leaving date", 'required');
				  
				}
				
			}
			else 
			{
				
			}
			/*$user_agent=='NI-WEB' &&  */
			if( ($this->form_validation->run() == FALSE || $l_date_count > 1 || $l_date_warning > 0)  )
			{
				
				$data['errmessage'] =  validation_errors();
				if($l_date_count > 1)
				{
					$data['errmessage'] =  $this->lang->line('specify_present_work_date');
				}
				if($l_date_warning > 0)
				{
					$data['errmessage'] =  $this->lang->line('warning_for_greater_join_date');
				}
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->sign_up_model->add_work_exp_detail();
				
				if($response['status'] == 'success')
				{
					
					if(isset($l_date_store) && $l_date_store!='')
					{
						$data['get_work_id'] = $l_date_store; 
					}
					else
					{
						
						usort($l_date_store_arr, function($a, $b)  {
							$dateTimestamp1 = strtotime($a);
							$dateTimestamp2 = strtotime($b);
						   
							return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
						});
						
						$max_date = $l_date_store_arr[count($l_date_store_arr) - 1];
						$key = array_search ($max_date, $l_date_store_arr_get_key);
						$data['get_work_id'] = $key  ; 
						
					}
					$msg = $this->data['reg_workexp_dtl_success_msg'] = $this->lang->line('reg_workexp_dtl_success_msg');
					$data['errmessage'] =  $msg; 
					$data['status'] =  'success';
					$data['form_work_id'] = $response['form_work_id'];   
					$data['exp_year'] = $response['exp_year'];   
					$data['exp_month'] = $response['exp_month'];
					$data['l_date_count'] = $l_date_count;
					
				}
				else
				{
					$data['errmessage'] =  $response;
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function js_extra_details()
	{
		$this->common_front_model->set_orgin();  
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='js_details')
		{
			$this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
			   $this->form_validation->set_rules('home_city', 'Home city', 'required');
			   $this->form_validation->set_rules('preferred_city[]', "Job preferred city", 'required');
			   //$this->form_validation->set_rules('pincode', "Pincode ", 'required');
			   $this->form_validation->set_rules('desire_job_type', "Desire job type ", 'required');
			   $this->form_validation->set_rules('employment_type', "Employment type", 'required');
			   $this->form_validation->set_rules('prefered_shift', "Job prefered shift ", 'required');
			   $this->form_validation->set_rules('key_skill', "Skill", 'required');
			   $this->form_validation->set_rules('website', "Website", 'valid_url|callback_valid_url_cus');
		    }
			else 
			{
				
			}
			if($user_agent=='NI-WEB' &&  $this->form_validation->run() == FALSE )
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->sign_up_model->js_extra_details();
				if($response == 'success')
				{
					$msg = $this->data['reg_js_dtl_success_msg'] = $this->lang->line('reg_js_dtl_success_msg');
					$data['errmessage'] =  $msg;
					$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] =  $response;
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function upload_profile_pic_resume()
	{
		$this->common_front_model->set_orgin();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		$action = $this->input->post('action');
		$data['token'] = $this->security->get_csrf_hash();
		$response_resume = 'error';
		$response_pic = 'error';
		$retrun_mes_resume['status'] = 'error';
		$retrun_mes_profile_pic['status']= 'error';
		$profile_pic = '';
		$resume_file = '';
		if($user_agent!='' && $action=='upload_img')
		{
			$set_resume_file = ($this->input->post('resume_file_upload')) ? $this->input->post('resume_file_upload') : "";
			$set_profile_file = ($this->input->post('profile_pic_upload')) ? $this->input->post('profile_pic_upload') : ""; 
			
			if($set_profile_file!='')
			{
		       $profile_pic = ($_FILES['profile_pic']!='' && is_array($_FILES['profile_pic']) &&  count($_FILES['profile_pic']) > 0) ? $_FILES['profile_pic'] : "";
			}
			if($set_resume_file!='')
			{
			  $resume_file = ($_FILES['resume_file']!='' && is_array($_FILES['resume_file']) && count($_FILES['resume_file']) > 0) ? $_FILES['resume_file'] : "";
			}
			
			 
			if($profile_pic!='')
			{
				$allowed_file_type ='jpg|jpeg|png|gif';
				$upload_path = $this->common_front_model->fileuploadpaths('js_photos',1);
				$final_param = array('file_name'=>'profile_pic','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type,'max_size'=>2048);
				
				$retrun_mes_profile_pic = $this->common_front_model->upload_file($final_param);
				
				if($retrun_mes_profile_pic['status']=='success')
				{
					$customvar = array('profile_pic'=>$retrun_mes_profile_pic['file_data']['file_name']);
					$response_pic = $this->sign_up_model->upload_profile_pic_resume($customvar,'Profile_pic');
					if($response_pic == 'success') 
					{
						$data['profile_pic_success_msg'] = $this->lang->line('profile_pic_upload_success_msg');
						$data['errmessage'] =  $data['profile_pic_success_msg'];
						$data['status'] =  'success';
					}
				}
				else
				{
					$data['errmessage_profile_pic'] = $retrun_mes_profile_pic['error_message'];
					$data['errmessage'] = $data['errmessage_profile_pic'];
					$data['status'] =  'error';
					
				}
			}
			if ($resume_file!='')
			{
				$allowed_file_type_resume ='doc|docx|pdf|rtf|txt';
				$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1);
				$final_param = array('file_name'=>'resume_file','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type_resume,'max_size'=>2048);
				$retrun_mes_resume = $this->common_front_model->upload_file($final_param);
				if($retrun_mes_resume['status']=='success')
				{
					$customvar = array('resume_file'=>$retrun_mes_resume['file_data']['file_name'],'resume_last_update'=>$this->common_front_model->getCurrentDate());
					$response_resume = $this->sign_up_model->upload_profile_pic_resume($customvar,'Resume_file');
					if($response_resume == 'success') 
					{
						$data['resume_success_msg'] = $this->lang->line('resume_upload_success_msg');
						$data['errmessage'] =  $data['resume_success_msg'];
						$data['status'] =  'success';
					}
				}
				else
				{
					    $data['errmessage_resume'] = $retrun_mes_resume['error_message'];
						$data['errmessage'] = $retrun_mes_resume['error_message'];
						$data['status'] =  'error';
				}
			
			}
		    if($retrun_mes_resume['status']=='success' && $retrun_mes_profile_pic['status']=='success' )
			{	 
				if($response_resume =='success' && $response_pic == 'success' )
				{	
						 $msg = $this->data['upload_profile_pic_resume_success_msg'] = $this->lang->line('upload_profile_pic_resume_success_msg');
						$data['errmessage'] =  $msg;
						$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] =  $response;
					$data['status'] =  'error';
				}
			}
		
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function upload_profile_pic_resume_base64()
	{	
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";		
		$js_id = '';
		if($this->session->userdata('reg_index_id') && $this->session->userdata('reg_index_id')!='')
		{
			$js_id = $this->session->userdata('reg_index_id');
			
		}
		else if($this->input->post('reg_index_id') && $this->input->post('reg_index_id')!='')
		{
			$js_id = $this->input->post('reg_index_id');
		}
		//$where = array('id'=>$js_id);  
		//$row_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','profile_pic');
		
		if(isset($js_id) && $js_id!='' && $user_agent!='')
		{
			if(isset($_POST['profile_pic']) && $_POST['profile_pic']!='')
			{
				$profile_pic_name = 'profile_pic';
				$upload_profile_pic_name = time().'-'.$js_id.'.jpg';
				$this->common_front_model->base_64_photo($profile_pic_name,'path_js_photos',$upload_profile_pic_name);
				$_REQUEST['profile_pic'] = $upload_profile_pic_name;
				$_REQUEST['profile_pic_approval'] = 'UNAPPROVED';
				
				$customvar = array('profile_pic'=>$upload_profile_pic_name,'profile_pic_approval'=>'UNAPPROVED');
				$response = $this->common_front_model->save_update_data('jobseeker',$customvar,'id','edit',$js_id);
								
					if($response == 'success') 
					{
						$data1['status'] = 'success';
						$data1['errmessage'] = $this->lang->line('profile_pic_upload_success_msg');
					}
					else
					{
						$data1['errmessage'] = 'Looks like something is wrong with your image.Please try with other images.';
					}
				
				if(isset($data1['response']))
				{
					unset($data1['response']);
				}
			}
			else
			{
				$data1['status'] = 'error';
				$data1['errmessage'] = "Please select Profile image for update.";
			}
		}
		else
		{
			$data1['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data1['status'] =  'error';
		}
		$data1['token'] = $this->security->get_csrf_hash();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data1));
	}
	public function edu_qualification_html()
	{   
		echo   $this->sign_up_model->edu_qualification_html();
	}
	
	public function work_exp_html()
	{   
		echo   $this->sign_up_model->work_exp_html();
	}
	
	public function certificate_html()
	{   
	    echo  $this->sign_up_model->certificate_html();
	}
	public function get_suggestion_city($return='city_name',$search='',$return_type='array')
	{   
	    $user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		$search = $search; 
		$str_ddr = array();
		$str_ddr = $this->sign_up_model->get_suggestion_city($search,$return,$return_type);
		if($user_agent !='NI-WEB')
		{
			$data['data']  = $str_ddr;
		}
		else
		{
			$data = $str_ddr; 
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	public function get_suggestion_list($get_list='',$search='')
	{   
		$this->common_front_model->set_orgin();  
	    $user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
	    $str_ddr = array();
		$str_ddr = $this->sign_up_model->get_suggestion_list($search,$get_list);
		$this->output->set_content_type('application/json');
		if($user_agent=='NI-AAPP' || $user_agent=='NI-IAPP')
		{
			$this->data['data'] = $str_ddr;
			$this->output->set_output(json_encode($this->data));
		}
		else
		{
			$this->output->set_output(json_encode($str_ddr));
		}
		
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$this->index($method);
		}
	}
}