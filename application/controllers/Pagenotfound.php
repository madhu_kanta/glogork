<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pagenotfound extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		
		if($user_agent == 'NI-WEB')
		{
			$page_title = "Page not found";
			$this->common_front_model->__load_header($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = $data_return;
			$this->load->view('common_file_echo',$data);
		}
	}
}
?>