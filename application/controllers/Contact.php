<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->data['config_data'] = $this->common_front_model->data['config_data'];
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function index()
	{
		$user_agent = 'NI-WEB';
		$page_title = $this->lang->line('contact_page_title');
		if($this->common_front_model->checkLoginfrontempl())
		{
			$this->common_front_model->__load_header_employer($page_title);
		}
		else
		{
			$this->common_front_model->__load_header($page_title);
		}
		if($user_agent == 'NI-WEB')
		{
			//$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
			$this->load->view('front_end/contact_view',$this->data);
			
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_footer_employer();
			}
			else
			{
				$this->common_front_model->__load_footer();	
			}
		}
	}
	public function contact_admin($type='plan-request')
	{
		$this->data['type_contact'] = 'plan-request';
		$this->index();
	}
	public function contact_mail()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('comment', 'Message', 'required');
		
		$data['token'] = $this->security->get_csrf_hash();
		if($this->form_validation->run() == FALSE)
			{
				$this->form_validation->set_error_delimiters('', '');
				//$data['message'] =  validation_errors('<li>', '</li>');//'<li>', '</li>'
				$data['message'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$name_post = $this->input->post('name');
				$email_post = $this->input->post('email');
				$mobile_post = $this->input->post('mobile');
				$comment_post = $this->input->post('comment');
				$email_temp_data = $this->common_front_model->getemailtemplate('Contact us admin');
				$trans = array("name_provided" =>$name_post,"mobile_no_provided"=>$mobile_post,"email_provided"=>$email_post,"message_provided"=>$comment_post,"webname"=>$this->data['config_data']['web_name']);
				$email_template  = $this->common_front_model->getstringreplaced($email_temp_data['email_content'],$trans);
				$response = $this->common_front_model->common_send_email($this->data['config_data']['contact_email'],$email_temp_data['email_subject'],$email_template);

				if(strtolower(trim($response)) == 'email sent.')
				{
					$msg = $this->data['contact_form_successmess'] = $this->lang->line('contact_form_successmess');
					$data['message'] =  $msg;
					$data['status'] =  'success';
				}
				else
				{
					$data['message'] = $response;
					$data['status'] =  'error';
				}		
			}
			/*$data1['data'] = json_encode($data);
			$this->load->view('common_file_echo',$data1);*/
			$this->output->set_content_type('application/json');
			$data1['data'] = $this->output->set_output(json_encode($data));
	}
}
?>