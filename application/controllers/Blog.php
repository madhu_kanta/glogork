<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog extends CI_Controller
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->ajax_search = 0;
		$this->data['singleblogflag'] = false;
		$this->data['base_url'] = $this->base_url = base_url();
		$this->load->model('front_end/blog_model');
	}
	public function index($permater='')
	{
		if($permater == 'allblogs' || $permater == '')
		{
			$this->allblogs();
		}
		else
		{
			$this->single($permater);
		}
	}
	
	public function single($blogid = '')
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		$where_arra_rec = array('status'=>'APPROVED','is_deleted'=>'No');
		$blogcount = $this->blog_model->gettotalblogcount();
		
		$this->data['blog_count'] = ($blogcount!='' && $blogcount!=0 && $blogcount > 0 ) ? $blogcount : 0;
		$where_arra = array('alias'=>$blogid,'status'=>'APPROVED','is_deleted'=>'No');
		$blogdata = $this->common_front_model->get_count_data_manual('blog_master',$where_arra,1,'','','');
		if($user_agent == 'NI-WEB')
		{
			if($blogdata !='' && is_array($blogdata) && count($blogdata) > 0)
			{
				$this->data['blog_data'] = $blogdata;
				$this->data['singleblogflag'] = true;
				$recentblogs = $this->blog_model->getblogdata($where_arra_rec,1,3);
				$this->data['recent_blog_data'] = $recentblogs;
				$page_title = $blogdata['title'];
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/blog_single',$this->data);
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				$this->load->view('front_end/404_view',$this->data);
			}
			if($this->common_front_model->checkLoginfrontempl())
			{
				$this->common_front_model->__load_footer_employer();
			}
			else
			{
				$this->common_front_model->__load_footer();
			}
				
		}
		else
		{
			$parampass = array('blog_image'=>'assets/blog_image');
			$blogdata = $this->common_front_model->dataimage_fullurl($blogdata,$parampass,'single');
			$data_return['data']   = $blogdata;
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}	
	public function allblogs($page=1)
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : $this->common_front_model->limit_per_page;
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No');
		$blogcount = $this->blog_model->gettotalblogcount();
		$this->data['blog_count'] = ($blogcount!='' && $blogcount!=0 && $blogcount > 0 ) ? $blogcount : 0;
		$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
		$blogdata = $this->blog_model->getblogdata($where_arra,$page,$limit_per_page);
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		
		if($user_agent == 'NI-WEB')
		{
			if($blogcount !='' && $blogcount > 0)
			{
				$recentblogs = $this->blog_model->getblogdata($where_arra,1,3);
				$this->data['recent_blog_data'] = $recentblogs;
				$this->data['blog_data'] = $blogdata;
				$page_title = $this->lang->line('all_blog_page_title');
				if($this->ajax_search == 0)
				{
					if($this->common_front_model->checkLoginfrontempl())
					{
						$this->common_front_model->__load_header_employer($page_title);
					}
					else
					{
						$this->common_front_model->__load_header($page_title);
					}
					$this->load->view('front_end/blog_all',$this->data);
				}
				else
				{
					$this->load->view('front_end/blog_all_result',$this->data);
				}
				
			}
			else
			{
				$page_title = 'Page Not found';
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_header_employer($page_title);
				}
				else
				{
					$this->common_front_model->__load_header($page_title);
				}
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				if($this->common_front_model->checkLoginfrontempl())
				{
					$this->common_front_model->__load_footer_employer();
				}
				else
				{
					$this->common_front_model->__load_footer();
				}
			}
		}
		else
		{
			$parampass = array('blog_image'=>'assets/blog_image');
			$blogdata = $this->common_front_model->dataimage_fullurl($blogdata,$parampass);
			$data_return['total_records'] = $blogcount;
			$data_return['data']   = $blogdata;
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}
?>