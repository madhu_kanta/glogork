<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class testandroidfile extends CI_Controller
{
	public $data = array();
	public $user_data = array();
	public function __construct()
	{
		parent::__construct();
		//$this->common_front_model->redirectLoginfrontempl();
		$this->user_data = $this->session->userdata('jobportal_employer');
		$where_arra = array('id'=>$this->user_data['employer_id'],'status'=>'APPROVED','is_deleted'=>'No');
		$this->user_data_values = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'');
		$this->load->model('front_end/employer_profile_model');
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		/* for changing password */
		/*$this->load->library('phpass');
		$password = '111111';
		$hashed_pass = $this->phpass->hash($password);
		$data_array_custom = array('password'=> $hashed_pass);
		$emalil = 'ketan@gmail.com';
		$whrerearr = array('email'=> $emalil);
		$this->common_front_model->update_insert_data_common('jobseeker',$data_array_custom,$whrerearr);*/
		/* for changing password end */
	}
	
	function uploadprofile()
	{
		$user_session = $this->user_data_values;
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		$image = ($_FILES['profile_pic']!='' && count($_FILES['profile_pic']) > 0) ? $_FILES['profile_pic'] : "";
		$upload_path = './assets/testandroid';
		$id = ($this->input->post('id')) ? $this->input->post('id') : $user_session['id'];
		if($user_agent!='' && $image!='' && $id!='')
		{
			$allowed_file_type ='jpg|jpeg|png';
			$final_param = array('file_name'=>'profile_pic','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type);
			$retrun_mes = $this->common_front_model->upload_file($final_param);
			if(isset($retrun_mes['status']) && $retrun_mes['status']=='success')
			{
				/*if(file_exists($upload_path.'/'.$this->user_data_values['profile_pic']))
				{
					$this->common_front_model->delete_file($upload_path.'/'.$this->user_data_values['profile_pic']);
				}*/
				$customvar = array('profile_pic'=>$retrun_mes['file_data']['file_name'],'profile_pic_approval'=>'UNAPPROVED'); 
				$this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$id,'',1,'is_deleted',2);
			}
			$data_return['final_result'] = $retrun_mes;	
			$data_return['tocken'] = $this->security->get_csrf_hash();
			/*$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);*/
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}
?>