<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Browse_resume extends CI_Controller
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin();
		$this->data['base_url'] = $this->base_url = base_url();
		$this->ajax_search = 0;
		$this->load->model('front_end/resume_list_model');
	}
	function check_seeker_employer_header($page_title)
	{
		if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid() == '')
		{
			 $this->common_front_model->__load_header($page_title);
		}
		else
		{
			 $this->common_front_model->__load_header_employer($page_title);
		}
	}
	function check_seeker_employer_footer()
	{
		if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid() == '')
		{
			 $this->common_front_model->__load_footer();			
		}
		else
		{
			 $this->common_front_model->__load_footer_employer();
		}
	}
	public function index($page=1)
	{
		
		$user_agent = 'NI-WEB';
		$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 4;//$this->common_front_model->limit_per_page;
		$this->data['limit_per_page'] = $limit_per_page;
		$total_resume_count = $this->resume_list_model->getjobseekers_count();
		$this->data['total_resume_count'] = ($total_resume_count!='' && $total_resume_count!=0 && $total_resume_count > 0 ) ? $total_resume_count : 0;
		if($user_agent == 'NI-WEB')
		{
			if($total_resume_count!='' && $total_resume_count > 0)
			{
				$response = $this->resume_list_model->listed_resumes($page,$limit_per_page);
				$this->data['total_resume_count'] = $response['total_resume_count'];
				$this->data['resume_data'] = $response['resume_data'];
				$this->data['custom_lable'] = $this->lang;
				$page_title = 'View Resumes';
				if($this->ajax_search == 0)
				{
					$this->check_seeker_employer_header($page_title);
					$this->load->view('front_end/resume_result_view',$this->data);
				}
				else
				{
					$this->load->view('front_end/page_part/resume_search_result_view',$this->data);
				}
			}
			else
			{
				$page_title = 'Page Not found';
				$this->common_front_model->__load_header($page_title);
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				$this->check_seeker_employer_footer();
			}
		}
		else
		{
			//for user agent androif and ohter
			//$return_var = array();
			$return_var['total_resume_count'] = $total_resume_count;
			$return_var['data'] = array();
			$return_var['tocken'] = $this->security->get_csrf_hash();
			if($total_resume_count!='' && $total_resume_count> 0)
			{
				$response = $this->resume_list_model->listed_resumes($page,$limit_per_page);
				//$return_var['data'] = $this->resume_list_model->listed_resumes($page,$limit_per_page);
				$return_var['total_resume_count'] = $response['total_resume_count'];
				$return_var['data'] = $response['resume_data'];
				$parampass = array('profile_pic'=>'assets/js_photos','resume_file'=>'assets/resume_file');
				$return_var['data'] = $this->common_front_model->dataimage_fullurl($return_var['data'],$parampass);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($return_var));
		}
	}
	function get_search_suggestion($search = '')
	{
		$search = $search; 
		$str_ddr = array();
		$str_ddr = $this->resume_list_model->get_search_suggestion($search);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
}
?>