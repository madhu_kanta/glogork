<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Common_request extends CI_Controller {
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin();
		$this->base_url = base_url();
		$this->data['base_url'] = $this->base_url;
		$this->load->model('front_end/employer_post_job_model');
	}
	public function get_list($get_list='',$return_opt='json',$currnet_val='')
	{
		$str_ddr = '<option value="">Select Option</option>';
		if($this->input->post('multivar') && $this->input->post('multivar')=='multi')
		{
			$str_ddr = $this->common_front_model->get_list_multiple($get_list);	
		}
		else
		{
			$str_ddr = $this->common_front_model->get_list($get_list);
		}
		
		if($return_opt =='json')
		{
			$data1['tocken'] = $this->security->get_csrf_hash();
			$data1['status'] = 'success';
			$data1['dataStr'] = $str_ddr;
			$data['data'] = json_encode($data1);
			$this->load->view('common_file_echo',$data);
		}
		else
		{
			echo $str_ddr;
		}
	}
	public function get_common_list_ddr()
    {
        $return_data_arr = array();
        $list_ddr_arr = array(
            'country_list','marital_list','edu_list','industries_master','functional_area_master','role_master','currency_master','key_skill_master','job_type_list','employement_list','shift_list','title_list','job_payment_list','company_type_master','company_size_master','skill_level_master','skill_language_master','salary_range_list');
        foreach($list_ddr_arr as $val)
        {
            $return_data_arr[$val] = $this->common_front_model->get_list($val,'json','','array');
        }
		$return_data_arr['country_code'] = $this->common_front_model->get_country_code('array');
        $this->output->set_content_type('application/json');
        $data['tocken'] = $this->security->get_csrf_hash();
        $data['status'] = 'success';
        $data['data'] = $return_data_arr;
        $this->output->set_output(json_encode($data));
    }
	public function get_mobile_code()
	{
		$str_ddr = array();
		$str_ddr = $this->common_front_model->get_country_code('array');
		$this->output->set_content_type('application/json');
		$data['tocken'] = $this->security->get_csrf_hash();
		$data['status'] = 'success';
		$data['data'] = $str_ddr;
		$this->output->set_output(json_encode($data));
	}
	
	function recent_view_job()
	{
		$get_list = $this->common_front_model->recent_view_job();
		$res['status'] = $get_list['status'];
		$res['tocken'] = $this->security->get_csrf_hash();
		$res['view_job_list'] = $get_list['view_job_list'];
		$data['data'] = $res ;
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	function get_counter()
	{
		$counter_id = $this->input->post('counter_id');
		if($counter_id!='' )
		{
			$str_ddr = $this->common_front_model->get_counter($counter_name='',$counter_id);
			$res['status'] = 'success';
		    $res['counter'] = $str_ddr;
		}
		else
		{
			$res['status'] = 'error';
		}
		$res['tocken'] = $this->security->get_csrf_hash();
		$data['data'] = $res ;
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	
	function check_job_seeker_action()
	{
		$this->common_front_model->set_orgin();
		$str_ddr = $this->common_front_model->check_job_seeker_action();
		$res['status'] = 'success';
		$res['check_status'] = $str_ddr;
		$res['tocken'] = $this->security->get_csrf_hash();
		$data['data'] = $res ;
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	
	public function get_list_json($get_list='',$currnet_val='')
	{
		$this->common_front_model->set_orgin();
		$str_ddr = array();
		if($this->input->post('multivar') && $this->input->post('multivar')=='multi')
		{
			$str_ddr = $this->common_front_model->get_list_multiple($get_list,'json','','array');	
		}
		else
		{
			$str_ddr = $this->common_front_model->get_list($get_list,'json','','array');
		}
		$this->output->set_content_type('application/json');
		$data['tocken'] = $this->security->get_csrf_hash();
		$data['status'] = 'success';
		$data['data'] = $str_ddr;
		$this->output->set_output(json_encode($data));
	}
	
	public function get_employer_industry_farea($get_data='',$currnet_val='')
	{
		$str_ddr = array();
		$tocken_val = 0;
		if($this->input->post('get_list') == 'skill_hire')
		{
			$tocken_val = 1 ;
		}
		$this->load->model('front_end/employer_post_job_model');
		$str_ddr = $this->employer_post_job_model->get_employer_industry_farea($get_data,'array',$currnet_val,$tocken_val);
		$this->output->set_content_type('application/json');
		$data['tocken'] = $this->security->get_csrf_hash();
		$data['status'] = 'success';
		$data['data'] = $str_ddr;
		$this->output->set_output(json_encode($data));
	}
	public function get_tocken()
	{
		$this->common_front_model->set_orgin();
		$data1['tocken'] = $this->security->get_csrf_hash();
		$data1['status'] = 'success';
		$data['data'] = json_encode($data1);
		$this->load->view('common_file_echo',$data);
	}
	public function get_site_data()
	{
		$this->common_front_model->set_orgin(); 
		$sitedata = $this->common_front_model->data['config_data'];
		$parampass = array('upload_logo'=>'assets/logo');
		$data1['tocken'] = $this->security->get_csrf_hash();
		$data1['config_data'] = $this->common_front_model->dataimage_fullurl($sitedata,$parampass,'single');
		/*$data['data'] = json_encode($data1);
		$this->load->view('common_file_echo',$data);*/
		$this->output->set_content_type('application/json');
		$data['data'] = $this->output->set_output(json_encode($data1));
	}
	public function get_label_lang()
	{
		$data1['tocken'] = $this->security->get_csrf_hash();
		$data1['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$data['data'] = json_encode($data1);
		$this->load->view('common_file_echo',$data);
	}
	function get_profile_details_emp()
	{ 
		$this->common_front_model->set_orgin(); 
		$this->load->model('front_end/employer_profile_model');
		$custom_lable_arr = $this->common_front_model->data['custom_lable']->language;
		$empdaetails = array();
		$empdaetails['tocken'] = $this->security->get_csrf_hash();
		if($this->input->post('emp_id_profile') && $this->input->post('emp_id_profile')!='')
		{
			
			$where_arra = array('id'=>$this->input->post('emp_id_profile'),'status'=>'APPROVED','is_deleted'=>'No');
			$emp_dat = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'','','','','');
			if($emp_dat!='' && is_array($emp_dat) && count($emp_dat) > 0)
			{
				/* for sending industries name */
				$industries_stored = ($emp_dat['industry_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_dat['industry_hire'])) ? $this->employer_profile_model->getdetailsfromids('industries_master','id',$emp_dat['industry_hire'],'industries_name') : "";
				if($this->common_front_model->checkfieldnotnull($industries_stored) && count($industries_stored) > 0)
				{
					$empdaetails['industry_hire_names']   = implode(',',$industries_stored);	
				}
				else
				{
					$empdaetails['industry_hire_names']   = $custom_lable_arr['notavilablevar'];
				}
				$fnarea_stored = ($emp_dat['function_area_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_dat['function_area_hire'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$emp_dat['function_area_hire'],'functional_name') : "";
				if($this->common_front_model->checkfieldnotnull($fnarea_stored) && is_array($fnarea_stored) && count($fnarea_stored) > 0)
				{
					$empdaetails['function_area_hire_names']   = implode(',',$fnarea_stored);
				}
				else
				{
					$empdaetails['function_area_hire_names']   = $custom_lable_arr['notavilablevar'];
				}
				$skillhire_stored = ($emp_dat['skill_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_dat['skill_hire'])) ? $this->employer_profile_model->getdetailsfromids('key_skill_master','id',$emp_dat['skill_hire'],'key_skill_name') : "";
				if($this->common_front_model->checkfieldnotnull($skillhire_stored) && is_array($skillhire_stored) && count($skillhire_stored) > 0)
				{
					$empdaetails['skill_hire_names']   = implode(',',$skillhire_stored);
				}
				else
				{
					$empdaetails['skill_hire_names']   = $custom_lable_arr['notavilablevar'];
				}
				$designation_stored = ($emp_dat['designation']!='0' && $this->common_front_model->checkfieldnotnull($emp_dat['designation'])) ? $this->employer_profile_model->getdetailsfromids('role_master','id',$emp_dat['designation'],'role_name') : "";
				$fnarea_stored_mine = ($emp_dat['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($emp_dat['functional_area'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$emp_dat['functional_area'],'functional_name') : "";
				if($this->common_front_model->checkfieldnotnull($fnarea_stored_mine) && count($fnarea_stored_mine) > 0)
				{
					$empdaetails['functional_area_name'] = implode(',',$fnarea_stored_mine);
				}
				else
				{
					$empdaetails['functional_area_name'] = $custom_lable_arr['notavilablevar'];
				}
				if($this->common_front_model->checkfieldnotnull($designation_stored) && is_array($designation_stored) && count($designation_stored) > 0)
				{
					$empdaetails['designation_name'] = implode(',',$designation_stored);
				}
				else
				{
					$empdaetails['designation_name'] = $custom_lable_arr['notavilablevar'];
				}
				/**/
				$parampass = array('profile_pic'=>'assets/emp_photos','company_logo'=>'assets/company_logos');
				$emp_dat_fn = $this->common_front_model->dataimage_fullurl($emp_dat,$parampass,'single');
				$empdaetails['status'] = 'success';
				$empdaetails['emp_data']   = $emp_dat_fn;	
			}
			else
			{
				$empdaetails['status'] = 'error';
				$empdaetails['errorMessage']   = 'Data Not Found';
			}
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($empdaetails));
	}
	function get_profile_details_js()
	{ 
		$this->common_front_model->set_orgin();
		$jsdaetails = array();
		$jsdaetails['tocken'] = $this->security->get_csrf_hash();
		if($this->input->post('js_id_profile') && $this->input->post('js_id_profile')!='')
		{
			$where_arra = array('id'=>$this->input->post('js_id_profile'),'status'=>'APPROVED','is_deleted'=>'No');
			$js_dat = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
			
			if($js_dat!='' && is_array($js_dat) && count($js_dat) > 0)
			{
				$js_dat['preferred_city_name'] = $this->common_model->valueFromId('city_master',$js_dat['preferred_city'],'city_name');
				$parampass = array('profile_pic'=>'assets/js_photos','resume_file'=>'assets/resume_file');
				//$parampass_resume = array('resume_file'=>'assets/resume_file');
				$js_dat_fn = $this->common_front_model->dataimage_fullurl($js_dat,$parampass,'single');
				/*if($js_dat['resume_file']!='' && !is_null($js_dat['resume_file']!=''))
				{
					$js_dat_fn = $this->common_front_model->dataimage_fullurl($js_dat,$parampass_resume,'single');	
				}*/
				$jsdaetails['status'] = 'success';
				$jsdaetails['js_data']   = $js_dat_fn;	
			}
			else
			{
				$jsdaetails['status'] = 'error';
				$jsdaetails['errorMessage']   = 'Data Not Found';
			}
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($jsdaetails));
	}
	public function get_suggestion_compnay($search='')
	{   
	    $search = $search; 
		$str_ddr = array();
		
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No',"(company_name like '%$search%' or email like '%$search%' or fullname like '%$search%' )");		
		$data_arr = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,2,'company_name,id,email','','1',50,"");
		//echo $this->common_front_model->last_query(); 
		$opt_array = array();
			
		if(isset($data_arr) && $data_arr !='' && is_array($data_arr) && count($data_arr) > 0)
		{
			foreach($data_arr as $data_arr_val)
			{
				if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
				{
					$opt_array[] = array('value'=>$data_arr_val['id'],'label'=>$data_arr_val['company_name'].'('.$this->common_model->email_disp.')');
				}
				else
				{
					$opt_array[] = array('value'=>$data_arr_val['id'],'label'=>$data_arr_val['company_name'].'('.$data_arr_val['email'].')');
				}
			}
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($opt_array));
	}
	public function get_employer_list()
	{
		$str_ddr = array();
		$str_ddr = $this->common_front_model->get_employer_listselect2();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
	public function give_industries_app_index()
	{
		$this->common_front_model->set_orgin();
		$table = ($this->input->post('table')) ? $this->input->post('table') : '';
		$id_filed = ($this->input->post('primary_key')) ? $this->input->post('primary_key') : '';
		$requiredfield = ($this->input->post('requiredfield')) ? $this->input->post('requiredfield') : '';
		$setlimit = ($this->input->post('setlimit')) ? $this->input->post('setlimit') : '';
		$return_var['data'] = array();
		$return_var['tocken'] = $this->security->get_csrf_hash();
		$return_var_temp = $this->common_front_model->get_featured_list($table,$id_filed,$requiredfield,$setlimit);
		$current_date = $this->common_model->getCurrentDate('Y-m-d');
		 
		if(isset($return_var_temp) && $return_var_temp!='' && is_array($return_var_temp) && count($return_var_temp)>0){
			foreach ($return_var_temp as $value) {
				$value['no_of_positions'] = $this->common_front_model->get_count_data_manual('job_posting',array('job_industry'=>$value['id'],'status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes'," job_expired_on >= '$current_date' "),0,'','','','','');
				
				$new_fields[] = $value;
			}
			$return_var_temp = $new_fields;
		}
		$return_var['data'] = $return_var_temp;
		header('Content-type: application/json');
	    echo json_encode($return_var);	   		
		//$this->output->set_content_type('application/json');
		//$this->output->set_output(json_encode($return_var));
	}
	public function get_jobseeker_list()
	{
		$str_ddr = array();
		$str_ddr = $this->common_front_model->get_jobseeker_listselect2();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
	public function give_recent_app_index()
	{
		$this->common_front_model->set_orgin();
		$this->load->model('front_end/home_model');
		$return_var['data'] = array();
		$return_var['tocken'] = $this->security->get_csrf_hash();
		$recent_jobs_count = $this->home_model->getrecent_jobs_count();
		$return_var['recent_job_count'] = $recent_jobs_count;
		if($recent_jobs_count > 0)
		{
			$page = ($this->input->post('page')) ? $this->input->post('page') : '1';
			$limit = ($this->input->post('limit')) ? $this->input->post('limit') : '3';
			$return_var_temp = $this->home_model->getrecent_jobs($page,$limit);
			//$return_var['data'] = $return_var_temp;
			$return_var['data'] = $this->employer_post_job_model->process_job_posted_data($return_var_temp);
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($return_var));
	}
	public function give_plan_access_auth()
	{
		$this->common_front_model->set_orgin(); 
		$return_var['data'] = array();
		$return_var['access'] = "No";
		$return_var['tocken'] = $this->security->get_csrf_hash();
		if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
		{
			$user_id = $this->common_front_model->get_userid();
			$user_type = "job_seeker";
		}
		elseif($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
		{
			$user_id = $this->common_front_model->get_empid();
			$user_type = "employer";
		}
		else
		{
			$user_id = "";
			$user_type = "";
		}
		//echo $user_id;
		$access_field = ($this->input->post('access_field')) ? $this->input->post('access_field') : 'contacts';
		$empid_jsid = ($this->input->post('empid_jsid')) ? $this->input->post('empid_jsid') : '';
		//if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
		if($user_id !='' && $user_type!='' && $access_field!='' && $empid_jsid!='')
		{
			$return_var['access'] = $this->common_front_model->get_plan_detail($user_id,$user_type,$access_field);
			if($return_var['access']=='Yes')
			{
				$this->common_front_model->check_for_plan_update($user_id,$user_type,$empid_jsid);
			}
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($return_var));
	}
	public function check_duplicate()
	{
		$return_var['data'] = array();		
		$return_var['tocken'] = $this->security->get_csrf_hash();
		$return_var['status'] = $this->common_front_model->check_duplicate();
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($return_var));
	}
	public function get_list_select2($get_list='',$search='')
	{
	    $str_ddr = array();
		$str_ddr = $this->common_front_model->get_suggestion_list($search,$get_list);
		$data['data'] = json_encode($str_ddr);
		$this->load->view('common_file_echo',$data);
	}
	
	public function update_ticket_from_developer()
	{
		if(isset($_REQUEST['web_key']) && $_REQUEST['web_key'] = $this->common_model->web_appkey && isset($_REQUEST['client_id']) && $_REQUEST['client_id'] = $this->common_model->client_id && isset($_REQUEST['ticket_number']) && $_REQUEST['ticket_number'] !='')
		{
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'ticket_reply' && isset($_REQUEST['comment']) && $_REQUEST['comment'] !='')
			{
				$ticket_number = $_REQUEST['ticket_number'];
				$user_type = $_REQUEST['user_type'];
				$user_id = $_REQUEST['user_id'];
				$comment = $_REQUEST['comment'];
				$created_on = $_REQUEST['created_on'];

				$data_array = array(
					'ticket_number'=>$ticket_number,
					'user_type'=>$user_type,
					'user_id'=>$user_id,
					'comment'=>$comment,
					'created_on'=>$created_on,
				);
				$response = $this->common_model->update_insert_data_common("ticket_history_reply",$data_array,'',0);
				$this->common_front_model->common_send_email($this->common_model->data['config_data']['contact_email'],'Ticket get new replied - '.$ticket_number,'Your ticket '.$ticket_number.' get new replied, please login and check in admin.');
			}
			else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'status_update_ticket' && isset($_REQUEST['status']) && $_REQUEST['status'] !='')
			{
				$ticket_number = $_REQUEST['ticket_number'];
			    $status = $_REQUEST['status'];
				
				$data_array = array(
					'status'=>$status,
				);
				$response = $this->common_model->update_insert_data_common("ticket_table",$data_array,array('ticket_number'=>$ticket_number),1);
				$this->common_front_model->common_send_email($this->common_model->data['config_data']['contact_email'],'Ticket Status updated - '.$ticket_number,'Your ticket '.$ticket_number.' status changed, please login and check in admin.');
			}
		}
	}
}