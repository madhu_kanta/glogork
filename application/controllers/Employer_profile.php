<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class employer_profile extends CI_Controller
{
	public $data = array();
	public $user_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->common_front_model->set_orgin();
		$this->user_data = $this->common_front_model->get_empid();
		$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
		$this->user_data_values = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'','','','','');
		$this->load->model('front_end/employer_profile_model');
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function index()
	{
		$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') : 'NI-WEB';
		if($user_agent == 'NI-WEB')
		{
			$this->common_front_model->redirectLoginfrontempl();
			
			if($this->user_data_values !='' && is_array($this->user_data_values) && count($this->user_data_values) > 0)
			{
				$this->data['country_code'] = $this->common_front_model->get_country_code();
				$this->data['user_data'] = $this->user_data_values;
				$this->data['pers_title'] = $this->common_front_model->get_personal_titles();
				$this->data['designation_stored'] = '';
				if($this->user_data_values['designation'] !='')
				{
					$this->data['designation_stored'] = $this->employer_profile_model->getdetailsfromids('role_master','id',$this->user_data_values['designation'],'role_name');
				}
				$this->data['fnarea_stored'] = '';
				if($this->user_data_values['functional_area'] !='')
				{
					$this->data['fnarea_stored'] = $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$this->user_data_values['functional_area'],'functional_name');
				}
				$page_title = 'Profile';
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/employerprofile_view',$this->data);
			}
			else
			{
				$page_title = 'Page Not found';
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/404_view',$this->data);
			}
			$this->common_front_model->__load_footer_employer();
		}
		elseif($user_agent=='NI-IAPP' || $user_agent=='IAPP')
		{
			if($this->user_data_values !='' && is_array($this->user_data_values) && count($this->user_data_values) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['emp_data']   = $this->user_data_values;
			}
			else
			{
				$data_return['status'] = 'error';
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			/*$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);*/
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function viewareaget($direct='indirect')
	{
			$this->common_front_model->redirectLoginfrontempl();
			if($direct!='indirect')
			{
				$data_return['tocken'] = $this->security->get_csrf_hash();
			}
			$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
			$this->user_data_values_v = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'','','','','');
			$this->data['base_url'] = $this->common_front_model->base_url;
			if($this->user_data_values_v !='' && is_array($this->user_data_values_v) && count($this->user_data_values_v) > 0)
			{
				$this->data['country_code'] = $this->common_front_model->get_country_code();
				$this->data['user_data'] = $this->user_data_values_v;
				$this->data['pers_title'] = $this->common_front_model->get_personal_titles();
				$this->data['designation_stored'] = '';
				if($this->user_data_values_v['designation'] !='')
				{
					$this->data['designation_stored'] = $this->employer_profile_model->getdetailsfromids('role_master','id',$this->user_data_values_v['designation'],'role_name');
				}
				$this->data['fnarea_stored'] = '';
				if($this->user_data_values_v['functional_area'] !='')
				{
					$this->data['fnarea_stored'] = $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$this->user_data_values_v['functional_area'],'functional_name');
				}
				$data_return['returndata'] = $this->load->view('front_end/empprofile_view',$this->data, true);
			}
			else
			{
				$data_return['returndata'] = $this->load->view('front_end/404_view',$this->data, true);
			}
			if($direct=='indirect')
			{
				return $data_return;
			}			
			else
			{
				/*$data['tocken'] = $this->security->get_csrf_hash();
				$data['contentreload'] = $data_return['content'];
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data));*/
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			
	}
	public function editpassword()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'old_password', 
                     'label'   => 'Old passsword', 
                     'rules'   => 'trim|required|callback_oldpassword_check'
                  ),   
               array(
                     'field'   => 'new_password', 
                     'label'   => 'New Password',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'confirm_password', 
                     'label'   => 'Confirm Password',
                     'rules'   => 'trim|required|matches[new_password]'
                  )
            );
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$this->load->library('encryption');
				$this->load->library('phpass');
				$password = $this->input->post('new_password');
				$hashed_pass = $this->phpass->hash($password);
				$data_array_custom = array('password'=> $hashed_pass);
				$this->common_front_model->save_update_data('employer_master',$data_array_custom,'id','edit',$user_data,'',1,'is_deleted',1);
				$data_return['status'] = "success";
				$data_return['successmessage'] = "Congratulations ! Your password for this account has been updated successfully.";//$this->data['custom_lable']->language['js_data_edit_succ'];		
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}

		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function oldpassword_check($pass_coming)
	{	
		$this->load->library('encryption');
		$this->load->library('phpass');
	   	$password_stored = $this->user_data_values['password'];
		if($this->phpass->check($pass_coming,$password_stored))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('oldpassword_check', 'Old password Provided is not correct');
			return FALSE;
		}
   		
	}
	public function prodeleterequest()
	{
		$this->common_front_model->set_orgin();  
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('message', 'Reason for deleting profile', 'trim|required|callback_valid_delete_req['.$user_data.']');
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				
				$return = $this->employer_profile_model->delete_pro_req($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					$data_return['successmessage'] = 'Your request for deleting your profile has been submitted successfully.';//$this->data['custom_lable']->language['js_data_edit_succ'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				elseif(isset($return) && $return!='' && isset($return['result']) && $return['result']=='already_there')
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = 'Your profile delete request is already under consideration.Please have patience while it is being processed';//$this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}

		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function uploadprofile()
	{
		if(isset($_FILES['profile_pic']) && $_FILES['profile_pic']!='')
		{
			$user_session = $this->user_data_values;
			$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
			$image = (isset($_FILES['profile_pic']) && $_FILES['profile_pic']!='' && count($_FILES['profile_pic']) > 0) ? $_FILES['profile_pic'] : "";
			$upload_path = $this->common_front_model->fileuploadpaths('emp_photos',1);
			//$id = ($this->input->post('id')) ? $this->input->post('id') : $user_session['id'];
			$id = $this->common_front_model->get_empid();
			if($user_agent!='' && $image!='' && $id!='')
			{
				$allowed_file_type = 'jpg|jpeg|png|gif';
				$final_param = array('file_name'=>'profile_pic','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type);
				$retrun_mes = $this->common_front_model->upload_file($final_param);
				if($retrun_mes['status']=='success')
				{
					if(file_exists($upload_path.'/'.$this->user_data_values['profile_pic']))
					{
						$this->common_front_model->delete_file($upload_path.'/'.$this->user_data_values['profile_pic']);
					}
					$customvar = array('profile_pic'=>$retrun_mes['file_data']['file_name'],'profile_pic_approval'=>'UNAPPROVED');
					$this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$id,'',1,'is_deleted',2);
				}
				$data_return['final_result'] = $retrun_mes;	
				$data_return['tocken'] = $this->security->get_csrf_hash();
				$data_return['file_arr'] = $_FILES;
				/*$data['data'] = json_encode($data_return);
				$this->load->view('common_file_echo',$data);*/
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$data_return['status'] = "error";
				$data_return['errormessage'] = $this->data['custom_lable']->language['emp_edit_data_not_ins'];
				$data_return['file_arr'] = $_FILES;
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
		}
		else if(isset($_POST['profile_pic']) && $_POST['profile_pic']!='')
		{
				$id = $this->common_front_model->get_empid();
				$where = array('id'=>$id);  
				$row_data = $this->common_front_model->get_count_data_manual('employer_master',$where,'1','profile_pic');
			
				$profile_pic_name = 'profile_pic';
				$upload_profile_pic_name = time().'-'.$id.'.jpg';
				$this->common_front_model->base_64_photo($profile_pic_name,'path_emp_photos',$upload_profile_pic_name);
				$_REQUEST['profile_pic'] = $upload_profile_pic_name;
				$_REQUEST['profile_pic_approval'] = 'UNAPPROVED';
				$customvar = array('profile_pic'=>$upload_profile_pic_name,'profile_pic_approval'=>'UNAPPROVED');
				$response = $this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$id);
				
				if($response && $response=='success')
				{
					if(file_exists($this->common_front_model->path_emp_photos.$row_data['profile_pic']))
					{
						$delete_profile_pic = $this->common_front_model->path_emp_photos.$row_data['profile_pic'];
						if(isset($delete_profile_pic) && $delete_profile_pic!='')
						{
							$this->common_front_model->delete_file($delete_profile_pic);
							$data_return['successmessage']='Your profile image has been submitted successfully.';
							$data_return['status'] = 'success';
						}
					}
					else
					{
						$data_return['status'] = 'error';
						$data_return['errormessage']='Upload of your profile image has failed.Please try again.';
					}
				}
				$data_return['tocken'] = $this->security->get_csrf_hash();
				/*$data['data'] = json_encode($data_return);
				$this->load->view('common_file_echo',$data);*/
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
	}
	public function deleteuploadprofile()
	{
		$user_session = $this->user_data_values;
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		$upload_path = $this->common_front_model->fileuploadpaths('emp_photos',1);
		$id = $this->common_front_model->get_empid();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $id!='' && $this->user_data_values !='' && is_array($this->user_data_values) && count($this->user_data_values) > 0)
		{
			$employer_details = $this->user_data_values;
			$old_profile_image = $employer_details['profile_pic'];
			$customvar = array('profile_pic'=>"",'profile_pic_approval'=>"UNAPPROVED");
			$query_return = $this->common_front_model->save_update_data('employer_master',$customvar,'id','edit',$id,'',1,'is_deleted',2);
			if($query_return == 'success')
			{
				if(file_exists($upload_path.'/'.$old_profile_image))
				{
					$this->common_front_model->delete_file($upload_path.'/'.$old_profile_image);
				}
				$data_return['errormessage'] = "Your Profile image has been deleted successfully.";
			}
			else
			{
				$data_return['errormessage'] = "There was a problem while deleting your profile image.";
			}
			$data_return['status'] = $query_return;
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = "There was a problem while deleting your profile image.";
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function updatepersonaldet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('emp_id')) ? $this->input->post('emp_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'title', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformtitle'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'fullname',
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformfullname'], 
                     'rules'   => 'trim|required'
                  )
            );
			if($this->input->post('email'))
			{
				array_push($config,array('field'=>'email','label'=>'Email','rules'=>'trim|required|valid_email|callback_email_unique'));
			}
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{	
				$return = $this->employer_profile_model->updatepersonaldet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					/* mail */
					$email_temp_data = $this->common_front_model->getemailtemplate('Employer profile edited');
					if(isset($email_temp_data) && is_array($email_temp_data) && count($email_temp_data) > 0)
					{
						$config_data = $this->common_front_model->data['config_data'];
						$webfriendlyname = $config_data['web_frienly_name'];
						$subject = $email_temp_data['email_subject'];
						$email_content = $email_temp_data['email_content'];
						$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
						$trans = array("websitename" =>$webfriendlyname);
						$trans_sub = array("section" =>$this->data['custom_lable']->language['section_personal_det']);
						$subject = $this->common_front_model->getstringreplaced($subject, $trans_sub);
						$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
						$this->common_front_model->common_send_email($this->user_data_values['email'],$subject,$email_template);
					}
					/* mail end */
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_edit_data_up_succ'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['emp_edit_data_not_ins'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editsociallinks()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array();
			if($this->input->post('facebookurl') && $this->input->post('facebookurl')!='')
			{
				array_push($config,array(
                     'field'   => 'facebookurl',
                     'label'   => 'Facebook social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[facebook]'
                  ));
			}
			if($this->input->post('gplusurl') && $this->input->post('gplusurl')!='')
			{
				array_push($config,array(
                     'field'   => 'gplusurl',
                     'label'   => 'Gplus social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[gplus]'
                  ));
			}
			if($this->input->post('twitterurl') && $this->input->post('twitterurl')!='')
			{
				array_push($config,array(
                     'field'   => 'twitterurl',
                     'label'   => 'Twitter social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[twitter]'
                  ));
			}
			if($this->input->post('linkdinurl') && $this->input->post('linkdinurl')!='')
			{
				array_push($config,array(
                     'field'   => 'linkdinurl',
                     'label'   => 'Linkdin social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[linkdin]'
                  ));
			}
			array_push($config,array(
                     'field'   => 'user_agent',
                     'label'   => 'User agent',
                     'rules'   => 'trim'
                  ));
			$this->form_validation->set_rules($config);	  
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_profile_model->editsocialdet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function updatecompanydet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('emp_id')) ? $this->input->post('emp_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'company_name', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_companydet'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'company_type', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmptypelbl'],
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'company_size', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmpsjizelbl'],
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'industry', 
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_industrylbl'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'mobile_c_code', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformmobcode'], 
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'mobile_num', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformmob'], 
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'country', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformcnt'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'city', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformcity'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'address', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformaddress'],
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'pincode', 
                     'label'   => $this->data['custom_lable']->language['pincode'],
                     'rules'   => 'trim|required'
                  )
            );
			if($this->input->post('company_website') && $this->input->post('company_website')!='')
			{
				array_push($config,array(
                     'field'   => 'company_website',
                     'label'   => $this->data['custom_lable']->language['myprofile_emp_cmpweblbl'],
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus'
                  ));
			}
			$this->form_validation->set_rules($config);
			
			$mobile_c_code = $this->input->post('mobile_c_code');
			$mobile_no = $this->input->post('mobile_num');
			$mobile = $mobile_c_code.'-'.$mobile_no;
			$this->user_data = $this->common_front_model->get_empid();
			$emp_id = $this->user_data;
			$where = array('mobile'=>$mobile,'id!='=>$emp_id,'is_deleted'=>'No');
			//$where = array("mobile='".$mobile."' and is_deleted='No'");
			$checkifmobile_exits = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','','','','','');
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] = strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				
				if($checkifmobile_exits==0)
				{
					$return = $this->employer_profile_model->updatecompanydet($user_data);
					if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
					{
						/* mail */
						$email_temp_data = $this->common_front_model->getemailtemplate('Employer profile edited');
						if(isset($email_temp_data) && is_array($email_temp_data) && count($email_temp_data) > 0)
						{
							$config_data = $this->common_front_model->data['config_data'];
							$webfriendlyname = $config_data['web_frienly_name'];
							$subject = $email_temp_data['email_subject'];
							$email_content = $email_temp_data['email_content'];
							$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
							$trans = array("websitename" =>$webfriendlyname);
							$trans_sub = array("section" =>$this->data['custom_lable']->language['section_compant_det']);
							$subject = $this->common_front_model->getstringreplaced($subject, $trans_sub);
							$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
							$this->common_front_model->common_send_email($this->user_data_values['email'],$subject,$email_template);
						}
						/* mail end */
						if($user_agent=='NI-WEB')
						{
							$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
						}
						$data_return['status'] = "success";
						$data_return['successmessage'] = $this->data['custom_lable']->language['emp_edit_data_up_succ'];		
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
					}
					else
					{
						$data_return['status'] = "error";
						if($return['result'] =='file_upload_error')
						{
							$data_return['errormessage'] = 'Only valid image file allow to upload, Max file size allow upto 2MB';
						}
						else
						{
							$data_return['errormessage'] = $this->data['custom_lable']->language['emp_edit_data_not_ins'];
						}
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
					}
				}
				else
				{
					$data_return['errormessage'] =  "Mobile number already exists. Please use a different mobile phone number.";
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function updatework_expdet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('emp_id')) ? $this->input->post('emp_id') : $this->user_data_values['id'];
		$user_data = $this->common_front_model->get_empid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			/*$config = array(
               array(
                     'field'   => 'join_month', 
                     'label'   => 'Join month',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'join_year', 
                     'label'   => 'Join year',
                     'rules'   => 'trim|required'
                  )
            );*/
			if($user_agent!='NI-IAPP')
			{
				$this->form_validation->set_rules('currentdesignation[]', 'Current designation','trim');//|callback_multiple_select
				$this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim');
				$this->form_validation->set_rules('function_area_hire[]', 'Industries', 'trim');
				$this->form_validation->set_rules('skill_hire[]', 'Industries', 'trim');
				if(count($this->input->post('currentdesignation')) > 0 && $this->input->post('currentdesignation')!='' && is_array($this->input->post('currentdesignation')))
				 {
					 if($this->chechall_filds_filled($this->input->post('currentdesignation')) == false)
					 {
						$this->form_validation->set_rules('currentdesignation[]', 'Current designation', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_des_mes']));
					 }
				 }
				 elseif((!(count($this->input->post('currentdesignation')) > 0)) || $this->input->post('currentdesignation')=='')
				 {
					 $this->form_validation->set_rules('currentdesignation[]', 'Current designation', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_des_mes1']));
				 }
				$this->form_validation->set_rules('functional_area[]', 'Functional area','trim');//|callback_multiple_select
				if(count($this->input->post('functional_area')) > 0 && $this->input->post('functional_area')!='' && is_array($this->input->post('functional_area')))
				 {
					 if($this->chechall_filds_filled($this->input->post('functional_area')) == false)
					 {
						$this->form_validation->set_rules('functional_area[]', 'Funcitonal area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_mes']));
					 }
				 }
				 elseif((!(count($this->input->post('functional_area')) > 0)) || $this->input->post('functional_area')=='')
				 {
					 $this->form_validation->set_rules('functional_area[]', 'Funcitonal area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_mes1']));
				 }
			}
			/*$this->form_validation->set_rules($config);*/
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_profile_model->updatework_expdet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					/* mail */
					$email_temp_data = $this->common_front_model->getemailtemplate('Employer profile edited');
					if(isset($email_temp_data) && is_array($email_temp_data) && count($email_temp_data) > 0)
					{
						$config_data = $this->common_front_model->data['config_data'];
						$webfriendlyname = $config_data['web_frienly_name'];
						$subject = $email_temp_data['email_subject'];
						$email_content = $email_temp_data['email_content'];
						$email_template = htmlspecialchars_decode($email_content,ENT_QUOTES); 
						$trans = array("websitename" =>$webfriendlyname);
						$trans_sub = array("section" =>$this->data['custom_lable']->language['section_word_det']);
						$subject = $this->common_front_model->getstringreplaced($subject, $trans_sub);
						$email_template = $this->common_front_model->getstringreplaced($email_template, $trans);
						$this->common_front_model->common_send_email($this->user_data_values['email'],$subject,$email_template);
					}
					/* mail end */
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_edit_data_up_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['emp_edit_data_not_ins'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function updateskilldet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('emp_id')) ? $this->input->post('emp_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_empid();
		if($user_data!='' && $user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim');
			$this->form_validation->set_rules('function_area_hire[]', 'Industries', 'trim');
			$this->form_validation->set_rules('skill_hire[]', 'Industries', 'trim');
			if($user_agent!='NI-IAPP')
			{
				if(count($this->input->post('industry_hire')) > 0 && $this->input->post('industry_hire')!='' && is_array($this->input->post('industry_hire')))
				 {
					 if($this->chechall_filds_filled($this->input->post('industry_hire')) == false)
					 {
						$this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_in_hire_mes']));
					 }
				 }
				 elseif((!(count($this->input->post('industry_hire')) > 0)) || $this->input->post('industry_hire')=='')
				 {
					 $this->form_validation->set_rules('industry_hire[]', 'Industries', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_in_hire_mes1']));
				 }
				 if( count($this->input->post('function_area_hire')) > 0 && $this->input->post('function_area_hire')!='' && is_array($this->input->post('function_area_hire')) )
				 {
					 if($this->chechall_filds_filled($this->input->post('function_area_hire')) == false)
					 {
						$this->form_validation->set_rules('function_area_hire[]', 'Functional area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_hire_mes']));
					 }
				 }
				 elseif((!(count($this->input->post('function_area_hire')) > 0)) || $this->input->post('function_area_hire')=='')
				 {
					 $this->form_validation->set_rules('function_area_hire[]', 'Functional area', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_fn_hire_mes1']));
				 }
				 if(count($this->input->post('skill_hire')) > 0 && $this->input->post('skill_hire')!='' && is_array($this->input->post('skill_hire')))
				 {
					 if($this->chechall_filds_filled($this->input->post('skill_hire')) == false)
					 {
						$this->form_validation->set_rules('skill_hire[]', 'Skills', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_skill_hire_mes']));
					 }
				 }
				 elseif((!(count($this->input->post('skill_hire')) > 0)) || $this->input->post('skill_hire')=='')
				 {
					 $this->form_validation->set_rules('skill_hire[]', 'Skills', 'trim|required',array('required' => $this->data['custom_lable']->language['emp_edit_skill_hire_mes1']));
				 }
			}
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->employer_profile_model->updateskilldet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['emp_edit_data_up_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}/*
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['emp_edit_data_not_ins'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}*/
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	function email_unique($str)
    {
		$currentregemailfromsess = "and email!='".$this->user_data_values['id']."'";
		$where = array("email='".$str."' and is_deleted='No' $currentregemailfromsess");
		$checkifemail_exits = $this->common_front_model->get_count_data_manual('employer_master',$where,'0','','','','','');
		if ($checkifemail_exits > 0)
        {
            $this->form_validation->set_message('email_unique', $this->data['custom_lable']->language['email_of_employer_already_exits']);
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
	public function multiple_select($arr_course)
	{
		//$arr_course = $this->input->post('currentdesignation[]');
		 if(empty($arr_course) || $arr_course == "")
		 {
		 	$this->form_validation->set_message('multiple_select', 'Select at least one Desgination');
			//$this->form_validation->set_rules('currentdesignation','Select at least one Desgination');
			return false;
		 }
		 else
		 {
			return true;
		 }
		 
	}
	public function chechall_filds_filled($array_pass)
	{
		$onevaluempty = array();
		if(!empty($array_pass) && is_array($array_pass))
		 {
			foreach($array_pass as $array_pass_single)
			{
				if(trim($array_pass_single)=='')
				{
					$onevaluempty[] = 'no';
				}
				else
				{
					$onevaluempty[] = 'yes';
				}
			}
		 }
		 if(in_array('no',$onevaluempty))
		 {
			 return false;
		 }
		 else
		 {
			 return true;
		 }
	}
	function valid_url_cus($url,$section='')
	{
		$pattern = "%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i";
		
		if (!preg_match($pattern, $url))
		{
			if($section=='facebook')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Facebook url you entered is not correct.');
			}
			elseif($section=='gplus')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Gplus url you entered is not correct.');
			}
			elseif($section=='twitter')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Twitter url you entered is not correct.');
			}
			elseif($section=='linkdin')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Linkdin url you entered is not correct.');
			}
			else
			{
				$this->form_validation->set_message('valid_url_cus', $this->data['custom_lable']->language['urlincorect']);
			}
			
			return FALSE;
		}
		return TRUE;
	}
	
	function my_listing($page=1,$get_list='')
	{
			if($this->common_front_model->get_empid()=='')
			{
				$base_url = $this->base_url;
			    redirect($base_url);
			}
			$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB';
		    $get_list = base64_decode($get_list);
			if($user_agent=='NI-WEB')
			{
				$get_list_data = array('js_activity_list_emp'=>$get_list);
				if((!$this->session->userdata('js_activity_list_emp') && $this->session->userdata('js_activity_list_emp')=='') || ($this->session->userdata('js_activity_list_emp') && $this->session->userdata('js_activity_list_emp')!=$get_list && $get_list!=''))
				{
					$this->session->set_userdata($get_list_data);
				}
				$get_list = $this->session->userdata('js_activity_list_emp');
			}
	    $this->ajax_search = $this->input->post('is_ajax') ? 1 : 0; 	 
		$page_title = $this->lang->line('js_activity_title_for_emp');
		
		if($this->ajax_search == 0)
		{
			$page = base64_decode($page);
		}
		
		if($this->input->post('get_list'))
		{
			$get_list = $this->input->post('get_list');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		//$user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
		
		if(is_numeric($page) && $page!='' && $get_list!='')
		{
			
			$js_action_details = $this->employer_profile_model->activity_list_for_emp($page,$limit=2,$get_list);
			//print_r($job_dtails); exit();
			
			if($js_action_details['status'] == 'success')
			{
				$this->data['activity_list_data'] = $js_action_details['activity_list_data'];
				$this->data['activity_list_count'] = $js_action_details['activity_list_count'];
				$this->data['get_list'] = $get_list;
				$this->data['status'] = 'success';
				$this->data['base_url'] =  base_url();
				if($user_agent == 'NI-WEB')
				{
					$this->data['custom_lable'] = $this->lang;
					if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_header_employer($page_title);
							$this->load->view('front_end/activity_list_view_for_emp',$this->data);
						}
						else
						{
							$this->load->view('front_end/page_part/activity_list_result_view_for_emp',$this->data);
						}
					
					 if($this->ajax_search == 0)
						{
							$this->common_front_model->__load_footer_employer();
						}
				}
			}
			else
			{
				$this->data['status'] = 'error';
				$this->common_front_model->__load_header_employer($page_title);
				$this->load->view('front_end/404_view',$this->data);
				$this->common_front_model->__load_footer_employer();
			}
		}
		else
		{
			$this->data['status'] = 'error';
			$this->common_front_model->__load_header_employer($page_title);
			$this->load->view('front_end/404_view',$this->data);
			$this->common_front_model->__load_footer_employer();
		}
		
		if($user_agent != 'NI-WEB')
		{
			$data['js_img'] = 'assets/js_photos';
			$data['activity_list_data'] = $js_action_details['activity_list_data'];
			$data['activity_list_count'] = $js_action_details['activity_list_count'];
			$data['status'] = $js_action_details['status'];
			$this->output->set_content_type('application/json');
		    $this->output->set_output(json_encode($data));
		}
		
	}
	
	function view_js_details()
	{
		    if($this->common_front_model->get_empid()=='')
			{
				$base_url = $this->base_url;
			    redirect($base_url);
			}
		    $user_agent = $this->input->post('user_agent') ? $this->input->post('user_agent') :'NI-WEB' ;
			$get_js_data = $this->employer_profile_model->view_js_details();
			$this->data['user_data'] =  $get_js_data;
			$this->data['status'] =  $get_js_data['status'];
			$this->data['base_url'] =  base_url();
			
			   if($user_agent == 'NI-WEB')
				{
					if($get_js_data['status'] == 'success')
					{
						$this->load->model('front_end/my_profile_model');
						$this->load->view('front_end/page_part/js_profile_for_emp_view',$this->data);
					}
					else
					{
						$this->load->view('front_end/404_view',$this->data);
					}
				}
				else
				{
					$data['user_data'] =  $get_js_data['js_data'];
					$data['js_img'] = 'assets/js_photos';
					$data['status'] =  $this->data['status'];
					$this->output->set_content_type('application/json');
		   			$this->output->set_output(json_encode($data));
				}
				
	
	}
	public function valid_delete_req($mes,$user_data)
	{	
			$check_if_req_exitst = $this->employer_profile_model->delete_pro_req_chk($user_data);
			if($check_if_req_exitst > 0)
			{
				$this->form_validation->set_message('valid_delete_req', 'Your profile delete request is already under consideration.Please have patience while it is being processed.');
				return FALSE;
			}
            else
            {
                return TRUE;
             }
		
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}	
}
?>