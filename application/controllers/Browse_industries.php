<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Browse_industries extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->base_url = base_url();
		$this->ajax_search = 0;
		$this->data['base_url'] = $this->base_url;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$this->load->model('front_end/industrywise_listing_model');
	}
	
	public function index($page=1)
	{
		$this->common_front_model->set_orgin();
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($this->input->post('page'))
		{
			$page = $this->input->post('page');
		}
		if($this->input->post('limit_per_page'))
		{
			$limit_per_page = $this->input->post('limit_per_page');
		}
		$limit_per_page = isset($limit_per_page) ? $limit_per_page : 3;//$this->common_front_model->limit_per_page;
		$this->data['limit_per_page'] = $limit_per_page;
		$jobs_by_industry_count = $this->industrywise_listing_model->gettotaljobs_groupbyind();
		$this->data['jobs_by_industry_count'] = ($jobs_by_industry_count!='' && $jobs_by_industry_count!=0 && $jobs_by_industry_count > 0 ) ? $jobs_by_industry_count : 0;
		
		$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','currently_hiring_status'=>'Yes');
		$current_data = $this->common_model->getCurrentDate('Y-m-d');
		$where_arra[] = " job_expired_on >= '$current_data' ";
		if($user_agent == 'NI-WEB')
		{
			if($jobs_by_industry_count!='' && $jobs_by_industry_count > 0)
			{
				$req_field = array('job_industry,functional_area,count(`functional_area`) as fnareacount');
				$jobs_by_industry_data = $this->industrywise_listing_model->getindustriesdata($where_arra,$page,$limit_per_page,$req_field);
				/*echo "<pre>";
				print_r($jobs_by_industry_data);echo "</pre>";exit;*/
				$this->data['jobs_by_industry_data'] = $jobs_by_industry_data;
				/*echo "<pre>";
				print_r($this->data['jobs_by_industry_data']);*/
				$page_title = "Browse Industries";//$this->lang->line('all_blog_page_title')
				$this->ajax_search = $this->input->post('is_ajax') ? 1 : 0;
				if($this->ajax_search == 0)
				{
					$this->common_front_model->__load_header($page_title);
					$this->load->view('front_end/browse_industries_view',$this->data);
				}
				else
				{
					$this->load->view('front_end/industry_all_result',$this->data);
				}
				
			}
			else
			{
				$page_title = 'Data Not found';
				$this->common_front_model->__load_header($page_title);
				//$this->load->view('front_end/404_view',$this->data);
				$this->load->view('front_end/no_data_found_image',$this->data);
			}
			if($this->ajax_search == 0)
			{
				$this->common_front_model->__load_footer();
			}
		}
		else
		{
			//for user agent androif and ohter
			$return_var['data'] = array();
			$return_var['total_jobs_by_industries_count'] = $jobs_by_industry_count;
			$return_var['tocken'] = $this->security->get_csrf_hash();
			if($jobs_by_industry_count!='' && $jobs_by_industry_count > 0)
			{
				$req_field = array('job_industry,functional_area,count(`functional_area`) as fnareacount');
				$jobs_by_industry_data = $this->industrywise_listing_model->getindustriesdata($where_arra,$page,$limit_per_page,$req_field);
				if(isset($jobs_by_industry_data) && $jobs_by_industry_data!='' && is_array($jobs_by_industry_data) && count($jobs_by_industry_data) > 0)
				{
					$unique_industry_id = 0;
					foreach($jobs_by_industry_data as $jobs_by_industry_data_single)
					{
						$where_arra_ind_nm = array('id'=>$jobs_by_industry_data_single['job_industry'],'is_deleted'=>'No','status'=>'APPROVED');
						$industries_name = $this->common_front_model->get_count_data_manual('industries_master',$where_arra_ind_nm,1,'industries_name,id','','','','');
						if(is_array($industries_name) && count($industries_name) > 0 && $industries_name!='' && $industries_name['industries_name']!='' && $jobs_by_industry_data_single['job_industry']!=$unique_industry_id)
						{
							$unique_industry_id = $jobs_by_industry_data_single['job_industry'];
							//$return_var['data'][] = array('industry_name'=>$industries_name['industries_name']);//,'functional_areas'=>array()
							$industryinsert = $industries_name['industries_name'];
							$industryinsert_id = $jobs_by_industry_data_single['job_industry'];
							$functionanlinsert = array();
							foreach($jobs_by_industry_data as $jobs_by_industry_data_single_fn)
							{
								if(is_array($jobs_by_industry_data_single_fn) && array_key_exists('job_industry',$jobs_by_industry_data_single_fn)  && array_key_exists('functional_area',$jobs_by_industry_data_single_fn)  && $jobs_by_industry_data_single_fn['functional_area']!='' && $jobs_by_industry_data_single_fn['job_industry']==$unique_industry_id)
								{
									$where_arra_fn_nm = array('id'=>$jobs_by_industry_data_single_fn['functional_area'],'is_deleted'=>'No','status'=>'APPROVED');
									$fnarea_name = $this->common_front_model->get_count_data_manual('functional_area_master',$where_arra_fn_nm,1,'functional_name,id','','','','');	
									if($fnarea_name!='' && is_array($fnarea_name) && count($fnarea_name) > 0 && $fnarea_name['functional_name']!='')
									{
										//$return_var['data'][] = array('industry_name'=>$industries_name['industries_name'],'functional_areas'=>);
										$functionanlinsert[] = array('id'=>$fnarea_name['id'],'fnareaname'=>$fnarea_name['functional_name'],'total_job_count'=>$jobs_by_industry_data_single_fn['fnareacount']);
									}
								}
							}
							$return_var['data'][] = array('industry_id'=>$industryinsert_id,'industry_name'=>$industryinsert,'functional_areas'=>$functionanlinsert);//
						}
					}
				}
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($return_var));
		}
	}
	
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
}