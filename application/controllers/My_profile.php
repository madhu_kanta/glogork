<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class My_profile extends CI_Controller 
{
	public $data = array();
	public $user_data = array();
	public function __construct()
	{
		parent::__construct();
		
		//$this->user_data = $this->session->userdata('jobportal_user');
		$this->user_data = $this->common_front_model->get_userid();
		$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
		$this->user_data_values = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
		/*if(!(count($this->user_data_values) > 0))
		{
			$this->common_front_model->redirectLoginfront();
		}*/
		$this->load->model('front_end/my_profile_model');
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function viewareaget($direct='indirect')
	{
			$this->common_front_model->redirectLoginfront();
			if($direct!='indirect')
			{
				$data_return['tocken'] = $this->security->get_csrf_hash();
			}
			$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
			$this->user_data_values_v = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
			$this->data['base_url'] = $this->common_front_model->base_url;
			if($this->user_data_values_v !='' && is_array($this->user_data_values_v) && count($this->user_data_values_v) > 0)
			{
				$this->data['user_data'] = $this->user_data_values_v;
				$this->data['resumetextread'] = $this->readresumefile();
				/**/
				$this->data['marital_status'] = $this->common_front_model->get_m_status();
				$this->data['country_code'] = $this->common_front_model->get_country_code();
				$this->data['pers_title'] = $this->common_front_model->get_personal_titles();
				$this->data['education_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'id','count');
				$this->data['work_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'id','count');
				if($this->data['education_count'] > 1)
				{
					$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','multiple');
				}
				else
				{
					$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');
				}
				if($this->data['work_count'] > 1)
				{
					$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','multiple');
				}
				else
				{
					$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');		
				}
				$this->data['language_details'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$this->user_data_values_v['id'],'',$dataorcount='data',$records='multiple');
				$this->data['skill_lvl_lang'] = $this->common_front_model->get_list('skill_level_master');
				$this->data['education_frm_tbl'] = $this->common_front_model->get_list('edu_list');
				$this->data['ind_frm_tbl'] = $this->common_front_model->get_list('industries_master');
				$this->data['salary_frm_tbl'] = $this->common_front_model->get_list('salary_range_list');
				$this->data['fnarea_frm_tbl'] = $this->common_front_model->get_list('functional_area_master');
				/**/
				$data_return['returndata'] = $this->load->view('front_end/profile_view',$this->data, true);
			}
			else
			{
				$data_return['returndata'] = $this->load->view('front_end/404_view',$this->data, true);
			}
			if($direct=='indirect')
			{
				return $data_return;
			}			
			else
			{
				/*$data['tocken'] = $this->security->get_csrf_hash();
				$data['contentreload'] = $data_return['content'];
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data));*/
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			
	}
	function get_section_part_view($part,$direct='indirect')
    {	
		$this->data['base_url'] = $this->common_front_model->base_url;
		$this->data['custom_lable'] = $this->data['custom_lable'];
		$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
		$this->user_data_values_v = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
		if($this->user_data_values_v !='' && is_array($this->user_data_values_v) && count($this->user_data_values_v) > 0 && $part!='')
		{
			$this->data['user_data_values'] = $this->user_data_values_v;
			$this->data['resumetextread'] = $this->readresumefile();
			/**/
			$this->data['marital_status'] = $this->common_front_model->get_m_status();
			$this->data['country_code'] = $this->common_front_model->get_country_code();
			$this->data['pers_title'] = $this->common_front_model->get_personal_titles();
			$this->data['education_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'id','count');
			$this->data['work_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'id','count');
			if($this->data['education_count'] > 1)
			{
				$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','multiple');
			}
			else
			{
				$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');
			}
			if($this->data['work_count'] > 1)
			{
				$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','multiple');
			}
			else
			{
				$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');		
			}
			$this->data['language_details'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$this->user_data_values_v['id'],'',$dataorcount='data',$records='multiple');
			$this->data['skill_lvl_lang'] = $this->common_front_model->get_list('skill_level_master');
			$this->data['education_frm_tbl'] = $this->common_front_model->get_list('edu_list');
			$this->data['ind_frm_tbl'] = $this->common_front_model->get_list('industries_master');
			$this->data['salary_frm_tbl'] = $this->common_front_model->get_list('salary_range_list');
			$this->data['fnarea_frm_tbl'] = $this->common_front_model->get_list('functional_area_master');
			/**/
			$data_return['content'] = $this->load->view('front_end/'.$part,$this->data,true);
		}
		else
		{
			$data_return['content'] = $this->load->view('front_end/404_view',$this->data, true);
		}
		
		if($direct=='indirect')
		{
			return $data_return;
		}
		else
		{
			$data['tocken'] = $this->security->get_csrf_hash();
			$data['contentreload'] = $data_return['content'];
			$data['contenttoreplace'] = $this->viewareaget('indirect');
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data));
		}
		/*$data_return['tocken'] = $this->security->get_csrf_hash();
		header('Content-type: application/json');
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);*/
		/*$this->output->set_content_type('application/json');
		$data['data'] = $this->output->set_output(json_encode($data_return));*/
    }
	public function index()
	{
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($user_agent == 'NI-WEB')
		{	
			$this->common_front_model->redirectLoginfront();
			if($this->user_data_values !='' && is_array($this->user_data_values) && count($this->user_data_values) > 0)
			{
				$this->data['user_data'] = $this->user_data_values;
				$this->data['resumetextread'] = $this->readresumefile();
				$page_title = 'My profile';
				$this->common_front_model->__load_header($page_title);
				$this->data['marital_status'] = $this->common_front_model->get_m_status();
				//$get_salary = $this->common_front_model->get_salary();
				$this->data['get_salary'] = $this->common_front_model->get_salary();
				//$country_code = $this->common_front_model->get_country_code();
				$this->data['country_code'] = $this->common_front_model->get_country_code();
				/*echo '<pre>';
				print_r($country_code);
				echo '</pre>';
				exit;*/
				$this->data['pers_title'] = $this->common_front_model->get_personal_titles();
				$this->data['education_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values['id'],'id','count');
				$this->data['work_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values['id'],'id','count');
				
				if($this->data['education_count'] > 1)
				{
					$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values['id'],'','data','multiple');
				}
				else
				{
					$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values['id'],'','data','single','arraytype');
				}
				if($this->data['work_count'] > 1)
				{
					$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values['id'],'','data','multiple');
				}
				else
				{
					$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values['id'],'','data','single','arraytype');		
				}
				$this->data['language_details'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$this->user_data_values['id'],'',$dataorcount='data',$records='multiple');
				$this->data['skill_lvl_lang'] = $this->common_front_model->get_list('skill_level_master');
				$this->data['education_frm_tbl'] = $this->common_front_model->get_list('edu_list');
				$this->data['ind_frm_tbl'] = $this->common_front_model->get_list('industries_master');
				$this->data['salary_frm_tbl'] = $this->common_front_model->get_list('salary_range_list');
				$this->data['fnarea_frm_tbl'] = $this->common_front_model->get_list('functional_area_master');
				$this->load->view('front_end/myprofile_view',$this->data);
			}
			else
			{
				$page_title = 'Page Not found';
				$this->common_front_model->__load_header($page_title);
				$this->load->view('front_end/404_view',$this->data);
			}
			$this->common_front_model->__load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
	function uploadresume()
	{
		$this->common_front_model->set_orgin();
		$user_session = $this->user_data;
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		$image = ($_FILES['resumeimg']!='' && is_array($_FILES['resumeimg']) && count($_FILES['resumeimg']) > 0) ? $_FILES['resumeimg'] : "";
		
		$upload_path = $this->common_front_model->fileuploadpaths('resume_file',1);
		//$id = ($this->input->post('id')) ? $this->input->post('id') : $user_session;
		$id = $this->common_front_model->get_userid();
		if($user_agent!='' && $image!='' && $id!='')
		{
			$allowed_file_type ='doc|docx|pdf|rtf|txt';
			
			$final_param = array('file_name'=>'resumeimg','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type);
			//$retrun_mes = $this->common_front_model->upload_file($final_param);
			
			/* new for fill url file name*/
			$retrun_mes1= $this->common_front_model->upload_file($final_param);
			$parampass_resume = array('file_name'=>'assets/resume_file');
			$retrun_mes['status'] = $retrun_mes1['status'];
			$retrun_mes['file_data'] = $this->common_front_model->dataimage_fullurl($retrun_mes1['file_data'],$parampass_resume,'single');
			/* new for fill url file name*/
			
			if($retrun_mes['status']=='success')
			{
				if(file_exists($upload_path.'/'.$this->user_data_values['resume_file']))
				{
					$this->common_front_model->delete_file($upload_path.'/'.$this->user_data_values['resume_file']);
				}
				$customvar = array('resume_file'=>$retrun_mes1['file_data']['file_name'],'resume_last_update'=>$this->common_front_model->getCurrentDate(),'resume_verification'=>'UNAPPROVED');
				$this->common_front_model->save_update_data('jobseeker',$customvar,'id','edit',$id,'',1,'is_deleted',2);
			}
			
			$data_return['final_result'] = $retrun_mes;	
			$data_return['tocken'] = $this->security->get_csrf_hash();
			/*if($user_agent=='NI-WEB')
			{
				$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
			}*/
			if($user_agent=='NI-WEB')
			{
				$data_return['contentreload'] = $this->get_section_part_view('resume_view_section');
			}
			/*$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);*/
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	function readresumefile()
	{
		//$user_id = ($this->input->post('id')) ? $this->input->post('id') : $this->user_data_values['id'];
		$user_id = $this->common_front_model->get_userid();
		if($user_id!='' && $user_id > 0)
		{
			$this->my_profile_model->getresumefromid($user_id);	
		}
	}
	function uploadprofile()
	{
		$this->common_front_model->set_orgin();
		//$temp = $_FILES;
		if(isset($_FILES['profile_pic']) && $_FILES['profile_pic']!='')
		{
			$user_session = $this->user_data;
			$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
			$image = ($_FILES['profile_pic']!='' && count($_FILES['profile_pic']) > 0) ? $_FILES['profile_pic'] : "";
			$upload_path = $this->common_front_model->fileuploadpaths('js_photos',1);
			//$id = ($this->input->post('id')) ? $this->input->post('id') : $user_session['user_id'];
			$id = $this->common_front_model->get_userid();
			if($user_agent!='' && $image!='' && $id!='')
			{
				$allowed_file_type ='jpg|jpeg|png|gif';
				$final_param = array('file_name'=>'profile_pic','upload_path'=>$upload_path,'allowed_types'=>$allowed_file_type,'max_size'=>1024*10);
				$retrun_mes = $this->common_front_model->upload_file($final_param);
				if($retrun_mes['status']=='success')
				{
					if(file_exists($upload_path.'/'.$this->user_data_values['profile_pic']))
					{
						$this->common_front_model->delete_file($upload_path.'/'.$this->user_data_values['profile_pic']);
					}
					$customvar = array('profile_pic'=>$retrun_mes['file_data']['file_name'],'profile_pic_approval'=>'UNAPPROVED'); 
					$this->common_front_model->save_update_data('jobseeker',$customvar,'id','edit',$id,'',1,'is_deleted',2);
					$data_return['message']='Your profile image has been submitted successfully.';
				}
				else
				{
					$data_return['message']='Upload of your profile image has failed.Please try again.'.$retrun_mes['error_message'];
					$data_return['status'] = 'error';
				}
				//$data_return['message'] = $temp;
				$data_return['final_result'] = $retrun_mes;	
				$data_return['tocken'] = $this->security->get_csrf_hash();
				/*$data['data'] = json_encode($data_return);
				$this->load->view('common_file_echo',$data);*/
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
				}
		}
		else if(isset($_POST['profile_pic']) && $_POST['profile_pic']!='')
		{
			$js_id = $this->common_front_model->get_userid();
			$where = array('id'=>$js_id);  
			$row_data = $this->common_front_model->get_count_data_manual('jobseeker',$where,'1','profile_pic');
			
			$profile_pic_name = 'profile_pic';
			$upload_profile_pic_name = time().'-'.$js_id.'.jpg';
			$this->common_front_model->base_64_photo($profile_pic_name,'path_js_photos',$upload_profile_pic_name);
			$_REQUEST['profile_pic'] = $upload_profile_pic_name;
			$_REQUEST['profile_pic_approval'] = 'UNAPPROVED';
			$customvar = array('profile_pic'=>$upload_profile_pic_name,'profile_pic_approval'=>'UNAPPROVED');
			$response = $this->common_front_model->save_update_data('jobseeker',$customvar,'id','edit',$js_id);
			
			if($response && $response=='success')
			{
				if(file_exists($this->common_front_model->path_js_photos.$row_data['profile_pic']))
				{
					 $delete_profile_pic = $this->common_front_model->path_js_photos.$row_data['profile_pic'];
					if(isset($delete_profile_pic) && $delete_profile_pic!='')
					{
						$this->common_front_model->delete_file($delete_profile_pic);
					}
					$data_return['message']='Your profile image has been submitted successfully.';
					$data_return['status'] = 'success';
				}
				else
				{
					$data_return['message']='Upload of your profile image has failed.Please try again.';
					$data_return['status'] = 'error';
				}
			}
			$data_return['tocken'] = $this->security->get_csrf_hash();
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function deleteuploadprofile()
	{
		$user_session = $this->user_data;
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "";
		$upload_path = $this->common_front_model->fileuploadpaths('js_photos',1);
		$id = $this->common_front_model->get_userid();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $id!='' && $this->user_data_values !='' && is_array($this->user_data_values) && count($this->user_data_values) > 0)
		{
			$jobseeker_details = $this->user_data_values;
			$old_profile_image = $jobseeker_details['profile_pic'];
			$customvar = array('profile_pic'=>"",'profile_pic_approval'=>"UNAPPROVED");
			$query_return = $this->common_front_model->save_update_data('jobseeker',$customvar,'id','edit',$id,'',1,'is_deleted',2);
			if($query_return == 'success')
			{
				if(file_exists($upload_path.'/'.$old_profile_image))
				{
					$this->common_front_model->delete_file($upload_path.'/'.$old_profile_image);
				}
				$data_return['errormessage'] = "Your Profile image has been deleted successfully.";
			}
			else
			{
				$data_return['errormessage'] = "There was a problem while deleting your profile image.";
			}
			$data_return['status'] = $query_return;
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = "There was a problem while deleting your profile image.";
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editpersonaldet()
	{
		$this->common_front_model->set_orgin();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'title', 
                     'label'   => 'Title', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'fullname', 
                     'label'   => 'Full name', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'resume_headline', 
                     'label'   => 'Resume headline',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'gender', 
                     'label'   => 'Gender',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'marital_status', 
                     'label'   => 'Maital status',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'mobile_c_code', 
                     'label'   => 'Mobile code',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'birthdate', 
                     'label'   => 'Birth date',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'country', 
                     'label'   => 'Country',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'city', 
                     'label'   => 'City',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'address', 
                     'label'   => 'Address',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'home_city', 
                     'label'   => 'Home city',
                     'rules'   => 'trim|required'
                  )
            );
			$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required|integer',array('integer' => 'Pincode should always be numeric value'));
			$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|integer',array('integer' => 'Mobile number should always be numeric value'));
			//$this->form_validation->set_rules('landline', 'Lincode', 'trim|integer',array('integer' => 'Landline should always be numeric value'));
			$mobile_c_code = $this->input->post('mobile_c_code');
			$mobile_no = $this->input->post('mobile');
			$mobile = $mobile_c_code.'-'.$mobile_no;
			
			$this->user_data = $this->common_front_model->get_userid();
			$js_id = $this->user_data;
			$where = array('mobile'=>$mobile,'id!='=>$js_id,'is_deleted'=>'No');
			//$where = array("(mobile='".$mobile."' and  id!='".$this->user_data."') and is_deleted='No'");
			
			$checkifmobile_exits = $this->common_front_model->get_count_data_manual('jobseeker',$where,'0','','','','','');
			$this->form_validation->set_rules('landline', 'Lincode', 'trim');
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				if($checkifmobile_exits==0)
				{
					$return = $this->my_profile_model->editpersonaldet($user_data);
					if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
					{
						if($user_agent=='NI-WEB')
						{
							$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
						}
						$data_return['status'] = "success";
						$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
					}
					else
					{
						$data_return['status'] = "error";
						$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
						$this->output->set_content_type('application/json');
						$data['data'] = $this->output->set_output(json_encode($data_return));
					}
				}
				else
				{
					$data_return['errormessage'] =  "Mobile number already exists. Please use a different mobile phone number.";
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editpassword()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'old_password', 
                     'label'   => 'Old passsword', 
                     'rules'   => 'trim|required|callback_oldpassword_check'
                  ),   
               array(
                     'field'   => 'new_password', 
                     'label'   => 'New Password',
                     'rules'   => 'trim|required'
                  ),   
               array(
                     'field'   => 'confirm_password', 
                     'label'   => 'Confirm Password',
                     'rules'   => 'trim|required|matches[new_password]'
                  )
            );
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$this->load->library('encryption');
				$this->load->library('phpass');
				$password = $this->input->post('new_password');
				$hashed_pass = $this->phpass->hash($password);
				$data_array_custom = array('password'=> $hashed_pass);
				$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$user_data,'',1,'is_deleted',1);
				$data_return['status'] = "success";
				$data_return['successmessage'] = "Congratulations ! Your password for this account has been updated successfully.";//$this->data['custom_lable']->language['js_data_edit_succ'];
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}

		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function prodeleterequest()
	{
		$this->common_front_model->set_orgin();  
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('message', 'Reason for deleting profile', 'trim|required|callback_valid_delete_req['.$user_data.']');
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				
				$return = $this->my_profile_model->delete_pro_req($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					$data_return['successmessage'] = 'Your request for deleting your profile has been submitted successfully.';//$this->data['custom_lable']->language['js_data_edit_succ'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				elseif(isset($return) && $return!='' && isset($return['result']) && $return['result']=='already_there')
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = 'Your profile delete request is already under consideration.Please have patience while it is being processed';//$this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}

		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editsociallinks()
	{
		$this->common_front_model->set_orgin(); 
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array();
			if($this->input->post('facebookurl') && $this->input->post('facebookurl')!='')
			{
				array_push($config,array(
                     'field'   => 'facebookurl',
                     'label'   => 'Facebook social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[facebook]'
                  ));
			}
			if($this->input->post('gplusurl') && $this->input->post('gplusurl')!='')
			{
				array_push($config,array(
                     'field'   => 'gplusurl',
                     'label'   => 'Gplus social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[gplus]'
                  ));
			}
			if($this->input->post('twitterurl') && $this->input->post('twitterurl')!='')
			{
				array_push($config,array(
                     'field'   => 'twitterurl',
                     'label'   => 'Twitter social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[twitter]'
                  ));
			}
			if($this->input->post('linkdinurl') && $this->input->post('linkdinurl')!='')
			{
				array_push($config,array(
                     'field'   => 'linkdinurl',
                     'label'   => 'Linkdin social profile link',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus[linkdin]'
                  ));
			}
			array_push($config,array(
                     'field'   => 'user_agent',
                     'label'   => 'User agent',
                     'rules'   => 'trim'
                  ));
			$this->form_validation->set_rules($config);	  
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editsocialdet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function profile_visibility_change()
	{
		$this->common_front_model->set_orgin();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$profile_visibility = ($this->input->post('profile_visibility')) ? $this->input->post('profile_visibility') : ""; 
		$user_data = $this->common_front_model->get_userid();
		if($user_data!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && ($profile_visibility=='public' || $profile_visibility=='private'))
		{
			$checkifmemberexists = $this->my_profile_model->get_userdetail_frm_id($user_data);
			if($checkifmemberexists!='' && is_array($checkifmemberexists) && count($checkifmemberexists) > 0)
			{
				if($profile_visibility=='private')
				{
					$profile_visibility_updae ='No';
				}
				elseif($profile_visibility=='public')
				{
					$profile_visibility_updae ='Yes';
				}
				$data_array_custom = array('profile_visibility'=> $profile_visibility_updae);
				$this->common_front_model->save_update_data('jobseeker',$data_array_custom,'id','edit',$checkifmemberexists['id'],'',1,'is_deleted',1);
				$data_return['status'] = "success";
				$data_return['successmessage'] = "Congratulations ! Your profile visibilty for this account has been updated successfully.";//$this->data['custom_lable']->language['js_data_edit_succ'];			
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$data_return['status'] = "error";
				$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editlangdet()
	{
		$this->common_front_model->set_orgin();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && count($this->input->post()) > 0)
		{
			$this->load->library('form_validation');
			$required_field = array('lang_known','proficiency');
			$cor_arry = array('lang_known'=>'Language Known','proficiency'=>'Proficiency');
			$config = array();
			$all_languages_stored = array();
			foreach($this->input->post() as $postsingle_key=>$postsingle_value)
			{
				
				if(!is_array($postsingle_key))
				{
					$postsingle_lang_chk = substr($postsingle_key, 0, 10);
					
					if($postsingle_lang_chk=='lang_known')
					{
						
						$postsingle_key_arr = explode("_",$postsingle_key);
						if(in_array("a",$postsingle_key_arr))
						{
							foreach($required_field as $singlefield)
							{
								if($this->input->post($singlefield.'_a_'.$postsingle_key_arr[3])=='')
								{
									$this->form_validation->set_rules($singlefield.'_a_'.$postsingle_key_arr[3], 'Language', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the language that you have selected.'));
								}
							}
							$all_languages_stored[] = strtolower(trim($this->input->post('lang_known_a_'.$postsingle_key_arr[3])));
						}
						else
						{
							foreach($required_field as $singlefield)
							{
								if($this->input->post($singlefield.'_'.$postsingle_key_arr[2])=='')
								{
									$this->form_validation->set_rules($singlefield.'_'.$postsingle_key_arr[2], 'Laanguage', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the language that you have selected.'));
								}
							}
							$all_languages_stored[] = strtolower(trim($this->input->post('lang_known_'.$postsingle_key_arr[2])));
						}
					}
				}
			}
			if(isset($all_languages_stored) && is_array($all_languages_stored) && count($all_languages_stored) > 0)
			{
				if(count(array_unique($all_languages_stored)) < count($all_languages_stored))
				{
					$this->form_validation->set_rules('lang_known', 'Language', 'trim|callback_duplicate_language');
				}
			}
			array_push($config,array(
                     'field'   => 'user_agent',
                     'label'   => 'User agent',
                     'rules'   => 'trim'
                  ));
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editlangdet_mod($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];
					if($user_agent=='NI-WEB')
					{
						$data_return['contentreload'] = $this->get_section_part('lang_section');
					}		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function checkwhichfieldsempty($totalfileds,$storingsecondpart)
	{
		$returningindexes = array();
		if(isset($totalfileds) && $totalfileds!='' && is_array($totalfileds) && count($totalfileds) > 0)
		{
			foreach($totalfileds as $singlefield)
			{
				$singlefield.$storingsecondpart;//.'<br>'
				if($this->input->post($singlefield.$storingsecondpart)=='')
				{
					$returningindexes[] = $singlefield.$storingsecondpart;
					echo $singlefield.$storingsecondpart;//.'<br>'
				}
			}
		}
	}
	function get_section_part($part,$direct='indirect')
    {	
		$this->data['base_url'] = $this->common_front_model->base_url;
		$this->data['custom_lable'] = $this->data['custom_lable'];
		$where_arra = array('id'=>$this->user_data,'status'=>'APPROVED','is_deleted'=>'No');
		$this->user_data_values_v = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'','','','','');
		$this->data['user_data_values'] = $this->user_data_values_v;
		$this->data['marital_status'] = $this->common_front_model->get_m_status();
		$this->data['country_code'] = $this->common_front_model->get_country_code();
		$this->data['education_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'id','count');
		$this->data['work_count'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'id','count');
		if($this->data['education_count'] > 1)
		{
			$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','multiple');
		}
		else
		{
			$this->data['educationdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');
		}
		if($this->data['work_count'] > 1)
		{
			$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','multiple');
		}
		else
		{
			$this->data['workdetails'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$this->user_data_values_v['id'],'','data','single','arraytype');		
		}
		$this->data['language_details'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$this->user_data_values_v['id'],'',$dataorcount='data',$records='multiple');
		$this->data['skill_lvl_lang'] = $this->common_front_model->get_list('skill_level_master');
		$this->data['education_frm_tbl'] = $this->common_front_model->get_list('edu_list');
		$this->data['ind_frm_tbl'] = $this->common_front_model->get_list('industries_master');
		$this->data['salary_frm_tbl'] = $this->common_front_model->get_list('salary_range_list');
		$this->data['fnarea_frm_tbl'] = $this->common_front_model->get_list('functional_area_master');
		$data_return['content'] = $this->load->view('front_end/'.$part,$this->data,true);
		if($direct=='indirect')
		{
			return $data_return;
		}
		else
		{
			$data['tocken'] = $this->security->get_csrf_hash();
			$data['contentreload'] = $data_return['content'];
			$data['contenttoreplace'] = $this->viewareaget('indirect');
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data));
		}
		/*$data_return['tocken'] = $this->security->get_csrf_hash();
		header('Content-type: application/json');
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);*/
		/*$this->output->set_content_type('application/json');
		$data['data'] = $this->output->set_output(json_encode($data_return));*/
    }
	
	/*public function editedudet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id'];
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$storingfirstpart = '' ;
			$storingsecondpart = '' ;
			$for_uniqueness = '';
			$returningindexes = array();
			$required_field = array('qual','inst','spec','perc','pass');
			$cor_arry = array('qual'=>'Qualification level','inst'=>'Institute name','spec'=>'Specialization','perc'=>'Percentage','pass'=>'Passing year');
			$this->load->library('form_validation');
			foreach($this->input->post() as $singleposttkey => $singleposttvalue )
			{
				$storingfirstpart = substr($singleposttkey,0,4);
				$storingsecondpart = substr($singleposttkey,-2);
				if(in_array($storingfirstpart, $required_field))
				{
					if($for_uniqueness!=$storingsecondpart)
					{
						foreach($required_field as $singlefield)
						{
							$singlefield.$storingsecondpart;//.'<br>'
							if($this->input->post($singlefield.$storingsecondpart)!='')
							{
								$returningindexes[] = $storingsecondpart;
							}
						}
					}
					$for_uniqueness = $storingsecondpart;
				}
			}
			
			if(count($returningindexes) > 0)
			{
				foreach(array_unique($returningindexes) as $singleretind)
				{
					foreach($required_field as $singlereq)
					{
						$this->form_validation->set_rules($singlereq.$singleretind, 'Qualification', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlereq].' of the education that you have selected.'));
					}		
				}
			}
			$this->form_validation->set_rules('user_agent', 'User agent', 'trim');
			if($this->form_validation->run() == FALSE)
			{
				$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editedudet_mod($user_data);
				if(isset($return) && $return!='' && count($return) > 0 && $return['result']=='success' )
				{
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}*/
	public function editedudet()
	{
		$this->common_front_model->set_orgin();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && count($this->input->post()) > 0)
		{
			$this->load->library('form_validation');
			$config = array();
			$required_field = array('qualification_level','institute_name','specialization','percentage','passing_year');
			$cor_arry = array('qualification_level'=>'Qualification level','institute_name'=>'Institute name','specialization'=>'Specialization','percentage'=>'Percentage','passing_year'=>'Passing year');
			$required_field_certi = array('certiu','certiy','certid');
			$cor_arry_certi = array('certiu'=>'Certificate name','certiy'=>'Certificate Passing Year','certid'=>'Certificate Description');
			foreach($this->input->post() as $postsingle_key=>$postsingle_value)
			{
				if(!is_array($postsingle_key))
				{
					$postsingle_lang_chk = substr($postsingle_key, 0, 19);
					
					if($postsingle_lang_chk=='qualification_level')
					{
						$postsingle_key_arr = explode("_",$postsingle_key);
						if(in_array("a",$postsingle_key_arr))
						{
								foreach($required_field as $singlefield)
								{
									if($this->input->post($singlefield.'_a_'.$postsingle_key_arr[3])=='')
									{
										$this->form_validation->set_rules($singlefield.'_a_'.$postsingle_key_arr[3], 'Qualification', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the education that you have selected.'));
									}
								}
							
						}
						else
						{
							foreach($required_field as $singlefield)
							{
								if($this->input->post($singlefield.'_'.$postsingle_key_arr[2])=='')
								{
									$this->form_validation->set_rules($singlefield.'_'.$postsingle_key_arr[2], 'Qualification', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the education that you have selected.'));
									
								}
							}
						}
					}
				}
			}
			
			foreach($this->input->post() as $postsingle_key=>$postsingle_value)
			{
				if(!is_array($postsingle_key))
				{
					$postsingle_lang_chk = substr($postsingle_key, 0, 6);
					
					if($postsingle_lang_chk=='certid')
					{
						$postsingle_key_arr = explode("_",$postsingle_key);
						if(in_array("a",$postsingle_key_arr))
						{
							
								foreach($required_field_certi as $singlefield_certi)
								{
									if($this->input->post($singlefield_certi.'_a_'.$postsingle_key_arr[2])=='')
									{
										$this->form_validation->set_rules($singlefield_certi.'_a_'.$postsingle_key_arr[2], 'Certificate', 'trim|required',array('required' => 'Please provide the '. $cor_arry_certi[$singlefield_certi].' of education certifiate that you have selected.'));
									}
								}
						}
						else
						{
							
								foreach($required_field_certi as $singlefield_certi)
								{
									if($this->input->post($singlefield_certi.'_'.$postsingle_key_arr[1])=='')
									{
										$this->form_validation->set_rules($singlefield_certi.'_'.$postsingle_key_arr[1], 'Certificate', 'trim|required',array('required' => 'Please provide the '. $cor_arry_certi[$singlefield_certi].' of education certifiate that you have selected.'));
									}
								}
						}
					}
				}
			}
			array_push($config,array(
                     'field'   => 'user_agent',
                     'label'   => 'User agent',
                     'rules'   => 'trim'
                  ));
			if(($this->input->post('twelveu') && $this->input->post('twelveu')!='') || ($this->input->post('twelvey') && $this->input->post('twelvey')!='') || ($this->input->post('twelvem') && $this->input->post('twelvem')!=''))
			{
				if($this->input->post('twelveu')=='')
				{
					array_push($config,array(
                     'field'   => 'twelveu',
                     'label'   => '12th passing institute name',
                     'rules'   => 'required|trim'
                  	));
				}
				if($this->input->post('twelvey')=='')
				{
					array_push($config,array(
                     'field'   => 'twelvey',
                     'label'   => '12th passing year',
                     'rules'   => 'required|trim'
                  	));
				}
				if($this->input->post('twelvem')=='')
				{
					array_push($config,array(
                     'field'   => 'twelvey',
                     'label'   => '12th passing marks',
                     'rules'   => 'required|trim'
                  	));
				}
			}
			
			if(($this->input->post('tenthu') && $this->input->post('tenthu')!='') || ($this->input->post('tenthy') && $this->input->post('tenthy')!='') || ($this->input->post('tenthm') && $this->input->post('tenthm')!=''))
			{
				
				if($this->input->post('tenthu')=='')
				{
					array_push($config,array(
                     'field'   => 'tenthu',
                     'label'   => '10th passing Institute name',
                     'rules'   => 'required|trim'
                  	));
				}
				if($this->input->post('tenthy')=='')
				{
					array_push($config,array(
                     'field'   => 'tenthy',
                     'label'   => '10th passing year',
                     'rules'   => 'required|trim'
                  	));
				}
				if($this->input->post('tenthm')=='')
				{
					array_push($config,array(
                     'field'   => 'tenthm',
                     'label'   => '10th passing marks',
                     'rules'   => 'required|trim'
                  	));
				}
			}
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editedudet_mod($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];
					if($user_agent=='NI-WEB')
					{
						$data_return['contentreload'] = $this->get_section_part('education_section');
					}
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editworkdet()
	{
		$this->common_front_model->set_orgin();
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		//$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		$user_data = $this->common_front_model->get_userid();
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP') && count($this->input->post()) > 0)
		{
			$this->load->library('form_validation');
			$config = array();
			$counterforblank_leave_date = 0;
			
			$required_field = array('companyname','industry','functional_area','job_role','joining_date');//,'leaving_date' requred field removed for leaving date 
			$not_required_field = array('leaving_date','currency_type','a_salary_lacs','a_salary_thousand');
			$cor_arry = array('companyname'=>'Company name','industry'=>'Industry','functional_area'=>'Functional area','job_role'=>'Job role','joining_date'=>'Joining date','leaving_date'=>'Leaving date');
			foreach($this->input->post() as $postsingle_key=>$postsingle_value)
			{
				if(!is_array($postsingle_key))
				{
					$postsingle_lang_chk = substr($postsingle_key, 0, 11);
					
					if($postsingle_lang_chk=='companyname')
					{
						$postsingle_key_arr = explode("_",$postsingle_key);
						if(in_array("a",$postsingle_key_arr))
						{
								foreach($required_field as $singlefield)
								{
									if($this->input->post($singlefield.'_a_'.$postsingle_key_arr[2])=='')
									{
										$this->form_validation->set_rules($singlefield.'_a_'.$postsingle_key_arr[2], 'Work history', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the work details that you have selected.'));
									}
									if($singlefield=='joining_date' && $this->input->post($singlefield.'_a_'.$postsingle_key_arr[2])!='')
									{
										if($this->input->post('leaving_date'.'_a_'.$postsingle_key_arr[2]) && $this->input->post('leaving_date'.'_a_'.$postsingle_key_arr[2])!='' && $this->input->post('leaving_date'.'_a_'.$postsingle_key_arr[2]) < $this->input->post($singlefield.'_a_'.$postsingle_key_arr[2]))
										{
											$this->form_validation->set_rules($singlefield.'_a_'.$postsingle_key_arr[2], 'Work history', 'trim|callback_valid_date');
										}
									}
								}
								foreach($not_required_field as $singlefield_not_req)
								{
									if($singlefield_not_req=='leaving_date' && $this->input->post($singlefield_not_req.'_a_'.$postsingle_key_arr[2])=='')
									{
											$counterforblank_leave_date = $counterforblank_leave_date+1;
									}
								}
						}
						else
						{
							
								foreach($required_field as $singlefield)
								{
									if($this->input->post($singlefield.'_'.$postsingle_key_arr[1])=='')
									{
										$this->form_validation->set_rules($singlefield.'_'.$postsingle_key_arr[1], 'Work history', 'trim|required',array('required' => 'Please provide your '. $cor_arry[$singlefield].' of the work details that you have selected.'));
									}
									if($singlefield=='joining_date' && $this->input->post($singlefield.'_'.$postsingle_key_arr[1])!='')
									{ 
										if($this->input->post('leaving_date'.'_'.$postsingle_key_arr[1]) && $this->input->post('leaving_date'.'_'.$postsingle_key_arr[1])!='' && $this->input->post('leaving_date'.'_'.$postsingle_key_arr[1]) < $this->input->post($singlefield.'_'.$postsingle_key_arr[1]))
										{
											$this->form_validation->set_rules($singlefield.'_'.$postsingle_key_arr[1], 'Work history', 'trim|callback_valid_date');
										}
									}
									
								}
								foreach($not_required_field as $singlefield_not_req)
								{
									if($singlefield_not_req=='leaving_date' && $this->input->post($singlefield_not_req.'_'.$postsingle_key_arr[1])=='')
									{
											$counterforblank_leave_date = $counterforblank_leave_date+1;
									}
								}
						}
					}
				}
			}
			if($counterforblank_leave_date > 1)
			{
				$this->form_validation->set_rules($singlefield.'_'.$postsingle_key_arr[1], 'Work history', 'trim|callback_twoleavedateblank');
			}
			array_push($config,array(
                     'field'   => 'user_agent',
                     'label'   => 'User agent',
                     'rules'   => 'trim'
                  ));
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] =  strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editworkdet_mod($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
					if($user_agent=='NI-WEB')
					{
						$data_return['contentreload'] = $this->get_section_part('work_section');
					}
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	public function editotherdet()
	{
		$data_return['tocken'] = $this->security->get_csrf_hash();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : ""; 
		$user_data = ($this->input->post('user_id')) ? $this->input->post('user_id') : $this->user_data_values['id']; 
		if($user_agent!='' && ($user_agent=='NI-WEB' || $user_agent=='NI-IAPP' || $user_agent=='NI-AAPP'))
		{
			$this->load->library('form_validation');
			$config = array(  
               array(
                     'field'   => 'prefered_shift', 
                     'label'   => 'Preferred shift',
                     'rules'   => 'trim|required'
                  )
            );
			if($user_agent=='NI-WEB')
			{
				array_push($config,array(
                     'field'   => 'preferred_city[]',
                     'label'   => 'Preferred city',
                     'rules'   => 'trim|required'
                  ));
			}
			else
			{
				array_push($config,array(
                     'field'   => 'preferred_city',
                     'label'   => 'Preferred city',
                     'rules'   => 'trim|required'
                  ));
			}
			if($this->input->post('website') && $this->input->post('website')!='')
			{
				array_push($config,array(
                     'field'   => 'website',
                     'label'   => 'Website',
                     'rules'   => 'trim|required|valid_url|callback_valid_url_cus'
                  ));
			}
			$this->form_validation->set_rules('exp_year', 'Experience Year', 'trim|required|integer',array('required' => 'Please provide years of your Experience. Select zero if you are a fresher with no experience.','integer' => 'Experience year should always be numeric value.'));
			$this->form_validation->set_rules('exp_month', 'Experience Month', 'trim|required|integer',array('required' => 'Please provide months of your Experience. Select zero if you are a fresher with no experience.','integer' => 'Experience year should always be numeric value.'));
			
			if(($this->input->post('exp_year')!=0 && $this->input->post('exp_year')!='') || ($this->input->post('exp_month')!=0 && $this->input->post('exp_month')!=''))
			{
				/*$this->form_validation->set_rules('currency_type', 'Currency Type', 'trim|required',array('required' => 'Please provide Currency type of your salary.'));
				$this->form_validation->set_rules('a_salary_lacs', 'Annual currency in lakhs', 'trim|required|integer',array('required' => 'Please provide lakhs in your salary.','integer' => 'Salaray should be a numeric value.'));
				$this->form_validation->set_rules('a_salary_thousand', 'Annual currency in Thousand', 'trim|required|integer',array('required' => 'Please provide Thousand unit in your salary.','integer' => 'Salaray should be a numeric value.'));*/
				$this->form_validation->set_rules('annual_salary', 'Annual salary', 'trim|required',array('required' => 'Please provide Current Annual salary.'));
				
				$this->form_validation->set_rules('industry', 'Industry', 'trim|required|integer',array('required' => 'Please provide the industry that you come from or prefer.','integer' => 'Something is not right while submitting industry.'));
			}
			   /* $this->form_validation->set_rules('exp_salary_currency_type', 'Expected Currency Type', 'trim|required',array('required' => 'Please provide Currency type of your Expected  salary.'));
				$this->form_validation->set_rules('exp_salary_lacs', 'Expected Annual currency in lakhs', 'trim|required|integer',array('required' => 'Please provide lakhs in your Expected salary.','integer' => 'Expected Salaray should be a numeric value.'));
				$this->form_validation->set_rules('exp_salary_thousand', 'Expected Annual currency in Thousand', 'trim|required|integer',array('required' => 'Please provide Thousand unit in your Expected  salary.','integer' => 'Expected Salaray should be a numeric value.'));*/
				
				$this->form_validation->set_rules('expected_annual_salary', 'Expected Annual Salary', 'trim|required',array('required' => 'Please provide Expected Annual Salary.'));
				
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE)
			{
				if($user_agent!='NI-WEB')
				{
					$data_return['errormessage'] = strip_tags(validation_errors());
				}
				else
				{
					$data_return['errormessage'] =  validation_errors();
				}
				//$data_return['errormessage'] =  validation_errors();
				$data_return['status'] = "error";
				$this->output->set_content_type('application/json');
				$data['data'] = $this->output->set_output(json_encode($data_return));
			}
			else
			{
				$return = $this->my_profile_model->editotherdet($user_data);
				if(isset($return) && $return!='' && isset($return['result']) && $return['result']=='success' )
				{
					if($user_agent=='NI-WEB')
					{
						$data_return['contenttoreplace'] = $this->viewareaget('indirect');	
					}
					$data_return['status'] = "success";
					$data_return['successmessage'] = $this->data['custom_lable']->language['js_data_edit_succ'];		
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
				else
				{
					$data_return['status'] = "error";
					$data_return['errormessage'] = $this->data['custom_lable']->language['js_data_edit_fail'];
					$this->output->set_content_type('application/json');
					$data['data'] = $this->output->set_output(json_encode($data_return));
				}
			}
		}
		else
		{
			$data_return['status'] = "error";
			$data_return['errormessage'] = $this->data['custom_lable']->language['no_user_agent_sent_in_req'];	
			$this->output->set_content_type('application/json');
			$data['data'] = $this->output->set_output(json_encode($data_return));
		}
	}
	function valid_url_cus($url,$section='')
	{
		/*$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";*/
		/*$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";*/
		$pattern = "%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i";
		if (!preg_match($pattern, $url))
		{
			if($section=='facebook')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Facebook url you entered is not correct.');
			}
			elseif($section=='gplus')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Gplus url you entered is not correct.');
			}
			elseif($section=='twitter')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Twitter url you entered is not correct.');
			}
			elseif($section=='linkdin')
			{
				$this->form_validation->set_message('valid_url_cus', 'The Linkdin url you entered is not correct.');
			}
			else
			{
				$this->form_validation->set_message('valid_url_cus', 'The URL you entered is not correct.');
			}
			
			return FALSE;
		}
	
		return TRUE;
	}
	public function get_suggestion_lang($search='',$return='skill_language')
	{   
	    $search = $search; 
		$str_ddr = array();
		$str_ddr = $this->my_profile_model->get_suggestion_lang($search,$return);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
	public function get_suggestion_lang_select2()
	{   
	   	$str_ddr = array();
		$str_ddr = $this->my_profile_model->get_suggestion_lang_select2();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($str_ddr));
	}
	public function delete_section()
	{
		$returnvar = array();
		$posted_id = $this->common_front_model->get_userid();
		$user_agent = ($this->input->post('user_agent')) ? $this->input->post('user_agent') : "NI-WEB"; 
		$section = $this->input->post('section');
		$id = $this->input->post('id');
		$count = $this->input->post('count');
	    $deleteret = $this->my_profile_model->delete_info($posted_id);
		if($deleteret == 'success')
		{
			
			if($section=='edu')
			{
				if($user_agent=='NI-AAPP')
				{
					$returnvar['status'] = 'success';
					$returnvar['message'] = 'Data deleted successfully.';
					$returnvar['tocken'] = $this->security->get_csrf_hash();
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($returnvar));
				}
				else
				{
					$returnvar['content'] = $this->get_section_part('education_section','direct');	
				}
				
			}
			if($section=='lang')
			{
				if($user_agent=='NI-AAPP')
				{
					$returnvar['status'] = 'success';
					$returnvar['message'] = 'Data deleted successfully.';
					$returnvar['tocken'] = $this->security->get_csrf_hash();
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($returnvar));
				}
				else
				{
					$returnvar['content'] = $this->get_section_part('lang_section','direct');
				}
			}
			if($section=='work')
			{
				if($user_agent=='NI-AAPP')
				{
					$returnvar['status'] = 'success';
					$returnvar['message'] = 'Data deleted successfully.';
					$returnvar['tocken'] = $this->security->get_csrf_hash();
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($returnvar));
				}
				else
				{
					$returnvar['content'] = $this->get_section_part('work_section','direct');
				}
			}
			if($section=='resume')
			{
				if($user_agent=='NI-AAPP' || $user_agent=='NI-IAPP')
				{
					$returnvar['status'] = 'success';
					$returnvar['message'] = 'Data deleted successfully.';
					$returnvar['tocken'] = $this->security->get_csrf_hash();
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($returnvar));
				}
				else
				{
					$returnvar['content'] = $this->get_section_part('resume_view_section','direct');
				}
				//$returnvar['content'] = $this->get_section_part_view('resume_view_section');
			}
		}
		else
		{
			if($user_agent=='NI-AAPP' || $user_agent=='NI-IAPP')
			{
				$returnvar['status'] = 'error';
				$returnvar['status_msg'] = 'error in deleteing file';
				$returnvar['tocken'] = $this->security->get_csrf_hash();
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($returnvar));
			}
			else
			{
				$returnvar['status'] = 'error';
				$returnvar['status_msg'] = 'error in deleteing file';
			}
			
		}
		return $returnvar;
	}
	public function get_all_detailsfromid($detail_name='education')
	{   
		$this->common_front_model->set_orgin(); 
		$education_datat =  array();
		$education_datat['tocken'] = $this->security->get_csrf_hash();
		$posted_id = $this->common_front_model->get_userid();
		if($detail_name=='education')
		{
			$education_count = $this->my_profile_model->getdetailsfromid('js_education_view','js_id',$posted_id,'id','count');	
		}
		elseif($detail_name=='work_history')
		{
			$education_count = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$posted_id,'id','count');	
		}
		elseif($detail_name=='language')
		{
			$education_count = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$posted_id,'id','count');	
		}
		
		if($education_count > 1 && $posted_id!='')
		{
			if($detail_name=='education')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('js_education_view','js_id',$posted_id,'','data','multiple');
				$education_datat['total_count'] = $education_count;
				/*$education_datat['total_10th_count'] = $this->my_profile_model->checkifadded('10','actual');
				$education_datat['total_12th_count'] = $this->my_profile_model->checkifadded('12','actual');*/
				$education_datat['total_edu_count'] = $this->my_profile_model->checkifadded('Edu','actual');
				$education_datat['total_certi_count'] = $this->my_profile_model->checkifadded('Certi','actual');
			}
			elseif($detail_name=='work_history')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$posted_id,'','data','multiple');
				$education_datat['count'] = $education_count;
			}
			elseif($detail_name=='language')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$posted_id,'','data','multiple');
				$education_datat['count'] = $education_count;
			}
		}
		elseif($education_count == 1 && $posted_id!='')
		{
			if($detail_name=='education')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('js_education_view','js_id',$posted_id,'','data','single','arraytype');
				$education_datat['total_count'] = $education_count;
				/*$education_datat['total_10th_count'] = $this->my_profile_model->checkifadded('10','actual');
				$education_datat['total_12th_count'] = $this->my_profile_model->checkifadded('12','actual');*/
				$education_datat['total_edu_count'] = $this->my_profile_model->checkifadded('Edu','actual');
				$education_datat['total_certi_count'] = $this->my_profile_model->checkifadded('Certi','actual');
			}
			elseif($detail_name=='work_history')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$posted_id,'','data','single','arraytype');
				$education_datat['count'] = $education_count;
			}
			elseif($detail_name=='language')
			{
				$education_datat['data'] = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$posted_id,'','data','single','arraytype');
				$education_datat['count'] = $education_count;
			}
		}
		else
		{
			$education_datat['data'] = array();
			$education_datat['count'] = $education_count;
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($education_datat));
	}
	public function oldpassword_check($pass_coming)
	{	
		$this->load->library('encryption');
		$this->load->library('phpass');
	   	$password_stored = $this->user_data_values['password'];
		if($this->phpass->check($pass_coming,$password_stored))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('oldpassword_check', 'Old password Provided is not correct');
			return FALSE;
		}
   		
	}
	public function valid_date()
	{	
		$this->form_validation->set_message('valid_date', 'Please note that leave date must always be greater than joining date.');
		return FALSE;
	}
	public function duplicate_language()
	{	
		$this->form_validation->set_message('duplicate_language', 'Please provide unique languages.Languages should not be repeated.');
		return FALSE;
	}
	public function twoleavedateblank()
	{	
		$this->form_validation->set_message('twoleavedateblank', 'Please note that leave date must be empty in only one job which will be considered as current job.');
		return FALSE;
	}
	public function valid_delete_req($mes,$user_data)
	{	
			$check_if_req_exitst = $this->my_profile_model->delete_pro_req_chk($user_data);
			if($check_if_req_exitst > 0)
			{
				$this->form_validation->set_message('valid_delete_req', 'Your profile delete request is already under consideration.Please have patience while it is being processed.');
				return FALSE;
			}
            else
            {
                return TRUE;
             }
		
	}
	function _remap($method, $params=array())
	{
    	$funcs = get_class_methods($this);
	    if(in_array($method, $funcs))
		{
    	    return call_user_func_array(array($this, $method), $params);
	    }
		else
		{
			$method = str_replace('_','-',$method);
			$this->index($method);
		}
	}
	function varify_mobile_send_otp()
	{
		$this->common_front_model->set_orgin(); 
		$returnvar = $this->my_profile_model->send_top_varify();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($returnvar));
		
	}
	function varify_mobile_check_otp()
	{
		$response = $this->my_profile_model->varify_mobile_otp();
		if($response == 'success')
		{
			$returnvar['status'] = 'success';
			$returnvar['error_meessage'] = 'Your mobile number verifired successfully.';
		}
		else
		{
			$returnvar['status'] = 'error';
			$returnvar['error_meessage'] = $response;
		}
		$returnvar['tocken'] = $this->security->get_csrf_hash();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($returnvar));
		
	}
}
?>