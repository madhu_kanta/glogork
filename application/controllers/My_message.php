<?php defined('BASEPATH') OR exit('No direct script access allowed');
class My_message extends CI_Controller 
{
	public $data = array();
	public $user_data = array();
	public function __construct()
	{
		parent::__construct();
		//$this->common_front_model->redirectLoginfront();
		$user_data = $this->common_front_model->get_logged_user_typeid();
		$this->user_type = $user_data['user_type'];
		$user_id = $user_data['user_id'];
		$this->base_url = $this->common_front_model->data['base_url'];
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		if($user_agent =='NI-AAPP')
		{
			$this->user_type = $this->input->post('user_type');
			$user_id = $this->input->post('user_id');
		}
		if(($this->user_type =='' || $user_id =='') && $user_agent == 'NI-WEB')
		{
			redirect($this->base_url);
		}
		$this->load->model("front_end/my_message_model");
		$this->my_message_model->user_type = $this->user_type;
		$this->my_message_model->user_id = $user_id;
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
	}
	public function load_header($page_title ='')
	{
		if($this->user_type !='' && $this->user_type =='job_seeker')
		{
			$this->common_front_model->__load_header($page_title);
		}
		else
		{
			$this->common_front_model->__load_header_employer($page_title);
		}	
	}
	public function load_footer()
	{
		if($this->user_type !='' && $this->user_type =='job_seeker')
		{
			$this->common_front_model->__load_footer();
		}
		else
		{
			$this->common_front_model->__load_footer_employer();
		}
	}
	
	public function get_message_count()
	{
		$this->common_front_model->set_orgin();
		$data_return = array();
		$data_return['status'] = 'success';
		
		$inbox_count = $this->my_message_model->getInbox(0);
		$sent_count = $this->my_message_model->getSent(0,'sent');
		$draft_count = $this->my_message_model->getSent(0,'draft');
		
		$data_return['data']   = array(
			'inbox'=>$inbox_count,
			'sent'=>$sent_count,
			'draft'=>$draft_count
		);
		$data_return['token'] = $this->security->get_csrf_hash();
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}
	public function index()
	{
		$this->common_front_model->set_orgin();
		$user_agent = 'NI-WEB';
		if($this->input->post('user_agent'))
		{
			$user_agent = $this->input->post('user_agent');
		}
		
		if($user_agent == 'NI-WEB')
		{
			$page_title = 'My Inbox';
			$this->load_header($page_title);
			$this->data['user_type'] = $this->user_type;
			$this->load->view('front_end/my_message_view',$this->data);
			$this->load_footer();
		}
		else
		{
			$data_return = array();
			$data_return['status']   = 'error';
			$cms_data = $this->my_message_model->msg_for_android();
			if($this->user_type =='employer')
			{
				$parampass = array('profile_pic'=>'assets/js_photos');
			}
			else
			{
				$parampass = array('profile_pic'=>'assets/emp_photos');
			}
			
			$cms_data = $this->common_front_model->dataimage_fullurl($cms_data,$parampass);
			
			if($cms_data !='' && is_array($cms_data) && count($cms_data) > 0)
			{
				$data_return['status'] = 'success';
				$data_return['data']   = $cms_data;
			}
			else
			{
				$data_return['errorMessage']   = 'Data Not Found';
			}
			$data_return['token'] = $this->security->get_csrf_hash();
			$data['data'] = json_encode($data_return);
			$this->load->view('common_file_echo',$data);
		}
	}
	public function send_message()
	{
		$this->common_front_model->set_orgin();
		$user_agent = $this->input->post('user_agent');
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='')
		{
			$message_action = 'sent';
			if($this->input->post('message_action'))
			{
				$message_action = $this->input->post('message_action');
			}
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				 $this->load->library('form_validation');
				 if($message_action == 'sent')
				 {
					 $this->form_validation->set_rules('subject', 'Subject ', 'required');
					 $this->form_validation->set_rules('to_message[]', 'Recruiter ', 'required');
			    	 $this->form_validation->set_rules('content', "content ", 'required');
				 }
			}
			if($user_agent=='NI-WEB' && $message_action == 'sent'  &&  $this->form_validation->run() == FALSE)
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->my_message_model->send_message();
				if($response == 'success')
				{
					$data['draft_id'] = '';
					if($message_action == 'sent')
					{
						$data['errmessage'] = $this->lang->line('send_mesage_success_msg');
					}
					else
					{
						$last_insert_id = $this->db->insert_id();
						if($last_insert_id !='' && $last_insert_id !=0)
						{
							$data['draft_id'] = $last_insert_id;
						}
						else
						{
							$draft_id = $this->input->post('draft_id');
							if($draft_id !='')
							{
								$data['draft_id'] = $draft_id;
							}
						}						
						$data['errmessage'] = 'Massage saved successfully';
					}
				    $data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] = $response;//$this->lang->line('js_action_err_msg');
				    $data['status'] =  'error';
				}
			}
		}
		else
		{
			$data['errmessage'] = $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	public function display_messsageType($page_number=1)
	{
		$this->data['page_number'] = $page_number;
		$this->data['base_url'] = $this->base_url;
		if($this->input->post('delete_action'))
		{
			$delete_action = $this->input->post('delete_action');
			if(isset($delete_action) && $delete_action !='' && $delete_action =='Yes')
			{
				$this->my_message_model->delete_message();
			}
		}
		$this->load->view('front_end/my_message_view_sub',$this->data);
	}
	public function delete_msg()
	{
		$this->common_front_model->set_orgin();
		$data_return = array();
		$message_id = '';
		if($this->input->post('message_id'))
		{
			$message_id_str = $this->input->post('message_id');
			$message_id_arr_temp = explode(',',$message_id_str);
			$message_id = array();
			if(isset($message_id_arr_temp) && $message_id_arr_temp !='' && is_array($message_id_arr_temp) && count($message_id_arr_temp) > 0)
			{
				foreach($message_id_arr_temp as $message_id_arr_temp_val)
				{
					if($message_id_arr_temp_val !='')
					{
						$message_id[] = $message_id_arr_temp_val;
					}
				}
			}
		}
		if(isset($message_id) && $message_id !='' && is_array($message_id) && count($message_id) > 0)
		{
			$this->my_message_model->delete_message($message_id);
			$data_return['status'] = 'success';
			$data_return['message']   = 'Message deleted successfully.';
		}
		else
		{
			$data_return['status'] = 'error';
			$data_return['message']   = 'Please select atleast one message to delete';
		}
		$data_return['token'] = $this->security->get_csrf_hash();
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}
	public function replay_msg_form()
	{
		$this->data['base_url'] = $this->base_url;
		$this->load->view('front_end/compose_message_view',$this->data);
	}
	public function message_readstatus()
	{
		$this->my_message_model->message_readstatus();
		$data['token'] = $this->security->get_csrf_hash();
		$data['status'] = 'success';
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	public function get_receiverList()
	{		
		$this->common_front_model->set_orgin();
		$str_ddr = array();
		$data_return['token'] = $this->security->get_csrf_hash();
		$data_return['status'] = 'success';
		if($this->user_type =='job_seeker')
		{
			$data_return['data'] = $this->my_message_model->get_employer_list();
		}
		else
		{
			$data_return['data'] = $this->my_message_model->get_jobseeker_list();
		}
		$data['data'] = json_encode($data_return);
		$this->load->view('common_file_echo',$data);
	}	
}
?>