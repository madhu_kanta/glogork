<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_employer extends CI_Controller 
{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('front_end/login_model_employer');
		$this->data['custom_lable'] = $this->common_front_model->data['custom_lable'];
		$where_arra = array('page_url'=>'employer-registration-cms','status'=>'APPROVED','is_deleted'=>'No');
		$this->data['empregcms'] = $this->common_front_model->get_count_data_manual('cms_pages',$where_arra,1,'page_title,page_content');
	}
	public function index()
	{  
		//$this->session->unset_userdata('login_attemp_stored');
		if($this->common_front_model->checkLoginfrontempl())
		{
			redirect($this->common_front_model->base_url.'employer_profile');
		}
		$this->common_front_model->__load_header_employer($this->data['custom_lable']->language['login_emp_header_title']);
		$this->load->view('front_end/login_view_emp',$this->data);
		$this->common_front_model->__load_footer_employer();
	}
	public function login()
	{
		/* FOR SETTING CAPTCHA SESSION */
		if($this->session->has_userdata('login_attemp_stored'))
		{
			$this->session->set_userdata('login_attemp_stored', $this->session->userdata('login_attemp_stored')+1) ;
		}
		else
		{
			$this->session->set_userdata('login_attemp_stored', '1') ;
		}
		
		if($this->session->userdata('login_attemp_stored') && $this->session->userdata('login_attemp_stored') > '2')
		{
			$time = $_SERVER['REQUEST_TIME'];
			$timeout_duration = 60*15;
			if ($this->session->has_userdata('LAST_ACTIVITY') && ($time - $this->session->userdata('LAST_ACTIVITY')) > $timeout_duration) 
			{
			  	$this->session->unset_userdata('login_attemp_stored');
			}			
			$this->session->set_userdata('LAST_ACTIVITY', $time) ;
			/* for unsetting the session after sometie end */
		}
		/* FOR SETTING CAPTCHA SESSION */
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		$data['login_attemp_stored'] = $this->session->userdata('login_attemp_stored');
		$data['user_agent'] = $user_agent;
		
		if($user_agent!='' && $action=='login')
		{ 
		    $this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
				$username = $this->input->post('username');
			    $password = $this->input->post('password');
				$this->form_validation->set_rules('username', $this->data['custom_lable']->language['login_lbl_username'], 'required');
			    $this->form_validation->set_rules('password', $this->data['custom_lable']->language['sign_up_emp_1stformpass'], 'required');
				/* captcha validation for desktop */
				if(($this->session->userdata('captcha_emp_login') && $this->session->userdata('captcha_emp_login')!='' ) && $this->session->userdata('login_attemp_stored') > 2)
				{
					$captcha_emp_login = $this->session->userdata('captcha_emp_login');
					if(!$this->input->post('validate_captcha') || ($this->input->post('validate_captcha') && $this->input->post('validate_captcha') != $captcha_emp_login[0] + $captcha_emp_login[1]))
					{
						$this->form_validation->set_rules('validate_captcha', 'Captcha', 'callback_captcha_check');	
					}
				}
				/* captcha validation for desktop end  */
			}
			
			if($user_agent=='NI-WEB' && $this->form_validation->run() == FALSE)
			{
				$data['errmessage'] = validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$this->common_front_model->set_orgin();
				$response = $this->login_model_employer->login();
				if($response['status'] == 'success')
				{
					$data['login_user_id'] = $response['login_user_id'];
					$data['errmessage'] = $response['errmessage'];
					$data['status'] =  'success';
					if($this->session->has_userdata('login_attemp_stored'))
					{
						$this->session->unset_userdata('login_attemp_stored');
					}
					
				}
				else
				{
					$data['errmessage'] =  $response['errmessage'];
					$data['status'] =  'error';
				}	
			}
		}
		else
		{
			$data['errmessage'] =  $this->lang->line('Unauthorized_Access');
			$data['status'] =  'error';
		}
		//header('Content-type: application/json');
		$this->output->set_content_type('application/json');
		//$data1['data'] = json_encode($data);
		$this->output->set_output(json_encode($data));
		//$this->load->view('common_file_echo',$data1);
	}
	public function verify_member($cpass='',$email='')
	{ 
		$this->login_model_employer->verify_member($cpass,$email);
	}
	public function forgot_password()
	{
		$this->common_front_model->__load_header($this->lang->line('forgot_passtit_employer'));
		$this->load->view('front_end/forgot_password_emp',$this->data);
		$this->common_front_model->__load_footer();
	}
	public function restetpasstonew($cpass='',$email='')
	{
	    $this->data['email_get'] = $email;
		$this->data['query_get'] = $cpass;
		$this->common_front_model->__load_header($this->lang->line('reset_passtit_employer'));
		$this->load->view('front_end/reset_password_emp',$this->data);
		$this->common_front_model->__load_footer();
	}
	public function reset_forgot_password()
	{
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='forgot_password')
		{
			$this->load->library('form_validation');
			if($user_agent!='' && $user_agent=='NI-WEB')
			{
					$this->form_validation->set_rules('email', $this->lang->line('sign_up_emp_1stformemail'), 'required|valid_email');
					//,array('is_unique' => $this->data['custom_lable']->language['email_exits'])
			}
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->login_model_employer->reset_forgot_password();
				if($response['status']== 'success')
				{
					$data['errmessage'] =  $response['errmessage'];
					$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] = $response['errmessage'];
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->data['custom_lable']->language['Unauthorized_Access'];
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	public function reset_new_password($cpass='',$email='')
	{
		
		$user_agent = $this->input->post('user_agent');
		$action = $this->input->post('action') ;
		$data['token'] = $this->security->get_csrf_hash();
		if($user_agent!='' && $action=='reset_password')
		{
			$this->load->library('form_validation');
			$config = array(
               array(
                     'field'   => 'pass_confirmation', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformpass'], 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'pass', 
                     'label'   => $this->data['custom_lable']->language['sign_up_emp_1stformpasscnf'],
                     'rules'   => 'trim|required|matches[pass_confirmation]'
                  )
            );
			
			$this->form_validation->set_rules($config);
			if($this->form_validation->run() == FALSE && $user_agent=='NI-WEB')
			{
				$data['errmessage'] =  validation_errors();
				$data['status'] =  'error';
			}
			else
			{
				$response = $this->login_model_employer->reset_new_password($cpass,$email);
				if($response['status']== 'success')
				{
					$data['errmessage'] =  $response['errmessage'];
					$data['status'] =  'success';
				}
				else
				{
					$data['errmessage'] = $response['errmessage'];
					$data['status'] =  'error';
				}		
			}
		}
		else
		{
			$data['errmessage'] = $this->data['custom_lable']->language['Unauthorized_Access'];
			$data['status'] =  'error';
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}
	public function captcha_check()
	{
		$this->form_validation->set_message('captcha_check', $this->data['custom_lable']->language['captcha_msg_employer']);
		return FALSE;
	}
	public function log_out()
	{
		if($this->session->userdata('jobportal_employer'))
		{
			$this->session->unset_userdata('jobportal_employer');
		}
		redirect($this->common_front_model->base_url);
	}
}
?>