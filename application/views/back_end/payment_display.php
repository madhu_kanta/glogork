<?php $dna = $this->common_model->data_not_availabel; 
$hidd_user_id = '';
$hidd_user_type ='';
if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='')
{
	$hidd_user_id = $_REQUEST['user_id'];
}
if(isset($_REQUEST['user_type']) && $_REQUEST['user_type'] !='')
{
	$hidd_user_type = $_REQUEST['user_type'];
}
$user_data = array();
$path_img = 'assets/js_photos/';
$plan_table_name = 'credit_plan_jobseeker';
if($hidd_user_id !='')
{
	if(isset($hidd_user_type) && $hidd_user_type =='job_seeker')
	{
		$user_data = $this->common_model->get_count_data_manual('jobseeker',array('id'=>$hidd_user_id),1,' fullname, email, mobile, profile_pic ','',0,'',0);
	}
	else
	{
		$path_img = 'assets/emp_photos/';
		$user_data = $this->common_model->get_count_data_manual('employer_master',array('id'=>$hidd_user_id),1,' fullname, email, mobile, profile_pic ','',0,'',0);
		$plan_table_name = 'credit_plan_employer';
	}
}
//print_r($user_data);
//	$this->load->view('back_end/job_basic_detail');
//print_r($_REQUEST);
?>
<div id="plan_detail">
	<div class="alert alert-danger" id="model_body_common_err" style="display:none"></div>
	<div class="row">
    	<div class="col-lg-5 col-xs-12 col-sm-12 col-md-5 imgPaddingRightZero" align="center">
        	<?php
				$avatar = 'assets/front_end/images/avatar-placeholder.png';
				if(isset($user_data['profile_pic']) && $user_data['profile_pic'] !='')
				{
					$temp_img = $user_data['profile_pic'];
					if(file_exists($path_img.$temp_img))
					{
						$avatar = $path_img.$temp_img;
					}
				}
			?>
            <img data-src="<?php echo $base_url.$avatar; ?>" title="<?php echo $user_data['fullname']; ?>" alt="<?php echo $user_data['fullname']; ?>" class=" img-responsive lazyload">
        </div>
        <div class="col-lg-5 col-xs-12 col-sm-12 col-md-5">
        	<p class="text-bold"><span class="fa fa-user"></span>&nbsp;&nbsp;<?php 
			if(isset($user_data['fullname']) && $user_data['fullname'] !=''){echo $user_data['fullname'];} else{ echo $dna;} ?></p>
            <p><span class="fa fa-envelope"></span>&nbsp;&nbsp;<?php if(isset($user_data['email']) && $user_data['email'] !=''){
				if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
				{
					echo $this->common_model->email_disp;
				}
				else
				{
					echo $user_data['email'];
				}
			} else{ echo $dna;}  ?></p>
            <p><span class="fa fa-phone"></span>&nbsp;&nbsp;
			<?php if(isset($user_data['mobile']) && $user_data['mobile'] !='')
			{
				if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
				{
					echo $this->common_model->mobile_disp;
				}
				else
				{
					echo $user_data['mobile'];
				}
			} else{ echo $dna;} ?>
            </p>
        </div>
    </div>
    <div class="clearfix"><br/></div>
    <div class="row form-horizontal">
    	<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
        	<div class="form-group ">
              <label class="col-sm-1 col-lg-1"></label>
              <label class="col-sm-2 col-lg-2 control-label text-bold">Plan</label>
              <div class="col-sm-6 col-lg-6">
                <select required name="plan_id" id="plan_id" class="form-control" onChange="display_plan()">
                	<option selected="" value="">Select Plan</option>
                    <?php
					$plan_data = $this->common_model->get_count_data_manual($plan_table_name,array('status'=>'APPROVED'),2,' * ','',0,'',0);
					if(isset($plan_data) && $plan_data !='' && count($plan_data) > 0)
					{
						foreach($plan_data as $plan_data_val)
						{
					?>
                    	<option value="<?php echo $plan_data_val['id']; ?>"><?php echo $plan_data_val['plan_name'].' ('.$plan_data_val['plan_currency'].' '.$plan_data_val['plan_amount'].' )'; ?></option>
                    <?php
						}
					}
					?>
                </select>
              </div>
              <label class="col-sm-1 col-lg-1"></label>
            </div>
            <div class="form-group ">
              <label class="col-sm-1 col-lg-1"></label>
              <label class="col-sm-2 col-lg-2 control-label text-bold pr0">Payment&nbsp;Mode</label>
              <div class="col-sm-6 col-lg-6">
                <select required name="payment_mode" id="payment_mode" class="form-control">
                	<option selected="" value="">Select Payment Mode</option>
                    <option value="Cash">Cash</option>
                    <option value="Credit Card">Credit Card</option>
                    <option value="Debit Card">Debit Card</option>
                    <option value="Other">Other</option>
                </select>
              </div>
              <label class="col-sm-1 col-lg-1"></label>
            </div>
        </div>
    </div>
    <?php
		//$plan_data = $this->common_model->get_count_data_manual($plan_table_name,array('status'=>'APPROVED'),2,' * ','',0,'',0);
		if(isset($plan_data) && $plan_data !='' && count($plan_data) > 0)
		{
			foreach($plan_data as $plan_data_val)
			{
	?>
    	<div class="plan_detail" style="display:none;margin: 0px;padding-top: 10px;border: 1px solid #E0E0E0;" id="plan_detail_<?php echo $plan_data_val['id']; ?>">
        <input type="hidden" id="payment_amount_<?php echo $plan_data_val['id']; ?>" value="<?php echo $plan_data_val['plan_amount']; ?>" />
	    	<div class="row">
	   		<div class="col-lg-12 col-sm-112 neAdminResultDetail">
               <div class="col-lg-2 col-sm-2 pr0">
                    Plan Name
                </div>
                <div class="col-lg-7 col-sm-7 ml20 text-bold pr0">:&nbsp;
                	<?php echo $plan_data_val['plan_name']; ?>
                </div>
            </div>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Plan Amount
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['plan_currency'].' '.$plan_data_val['plan_amount']; ?>
                </div>
            </div>
           <?php
            $service_tax = 0;
            $service_tax_amt = 0;
            $config_data = $this->common_front_model->get_site_config();
            if(isset($config_data['service_tax']) && $config_data['service_tax'] !='')
            {
                $service_tax =  $config_data['service_tax'];
            }
			$plan_amount = $plan_data_val['plan_amount'];
            if($plan_amount !='' && $plan_amount > 0 && $service_tax !='' && $service_tax > 0 )
            {
                $service_tax_amt = (($plan_amount) * $service_tax) / 100;
            }
			if($service_tax_amt > 0)
			{
				$plan_amount = $plan_amount + $service_tax_amt;
			?>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Service Tax / GST
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['plan_currency'].' '.$service_tax_amt; ?>
                </div>
            </div>
            <?php
			}
		   ?>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Total Pay 
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['plan_currency'].' '.$plan_amount; ?>
                </div>
            </div>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Plan Duration
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['plan_duration']; ?> Days
                </div>
           </div>
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Message
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['message']; ?>
                </div>
           </div>
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Contacts
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['contacts']; ?>
                </div>
           </div>
           <?php
		    if($hidd_user_type !='' && $hidd_user_type =='job_seeker')
			{
		   ?>
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Relevant Jobs
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['relevant_jobs']; ?>
                </div>
           </div>
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Highlight Application
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['highlight_application']; ?>
                </div>
           </div>
           
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Performance Report
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['performance_report']; ?>
                </div>
           </div>
           <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Job Notification
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['job_post_notification']; ?>
                </div>
           </div>
           <?php
			}
			else if($hidd_user_type !='' && $hidd_user_type =='employer')
			{
			?>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Job Post Limit
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['job_post_limit']; ?>
                </div>
           </div>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Cv Access Limit
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['cv_access_limit']; ?>
                </div>
           </div>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Highlight Job
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['highlight_job_limit']; ?>
                </div>
           </div>
            <div class="col-lg-6 col-sm-11 neAdminResultDetail">
               <div class="col-lg-5 col-sm-5 pr0">
                    Job Life
                </div>
                <div class="col-lg-7 col-sm-7 pr0">:&nbsp;
                	<?php echo $plan_data_val['job_life']; ?>
                </div>
           </div>
            <?php
			}
		   ?>
           <div class="col-lg-12 col-sm-112 neAdminResultDetail">
               <div class="col-lg-2 col-sm-2 pr0">
                    Plan Offer
                </div>
                <div class="col-lg-7 col-sm-7 ml20 pr0">:&nbsp;
                	<?php 
						if(isset($plan_data_val['plan_offers']) && $plan_data_val['plan_offers'] !='')
						{
							echo $plan_data_val['plan_offers'];
						}
						else
						{
							echo $dna;
						}
					?>
                </div>
           </div>
        </div>

        </div>
    <?php
			}
		}
	?>
    <div class="clearfix"><br/></div>
    <div class="row form-horizontal">
    	<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
        	<div class="form-group ">
              <label class="col-sm-1 col-lg-1"></label>
              <label class="col-sm-2 col-lg-2 control-label text-bold">Payment&nbsp;Note</label>
              <div class="col-sm-6 col-lg-6">
                <textarea rows="5" class="form-control" name="payment_note" id="payment_note" placeholder="Enter Payment Note"></textarea>
              </div>
              <label class="col-sm-1 col-lg-1"></label>
            </div>
        </div>
    </div>
    <div class="clearfix"><br/></div>
    <div class="row form-horizontal">
    	<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 text-center">
    		<button type="button" class="btn btn-primary mr10" onclick="update_payment_member()">Submit</button>
            <button type="button" onclick="close_payment_pop()" class="btn btn-default" data-dismiss="myModal_common">Close</button>
        </div>
    </div>
    
</div>
<input type="hidden" id="hash_tocken_id_temp" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" id="hidd_user_id" name="hidd_user_id" value="<?php echo $hidd_user_id;?>" />
<input type="hidden" id="hidd_user_type" name="hidd_user_type" value="<?php echo $hidd_user_type;?>" />

<script type="text/javascript">
	function display_plan()
	{
		var plan_id = $("#plan_id").val();
		$(".plan_detail").slideUp();
		if(plan_id !='')
		{
			$("#plan_detail_"+plan_id).slideDown();
		}
	}
</script>