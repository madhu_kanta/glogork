<?php
	$where_arra = array('is_deleted'=>'No');
	$employer = $this->common_model->get_count_data_manual('employer_master',$where_arra,0,'');
	$job_seeker = $this->common_model->get_count_data_manual('jobseeker',$where_arra,0,'');
	$job_count = $this->common_model->get_count_data_manual('job_posting',$where_arra,0,'');
	
	$empl_payment_count = $this->common_model->get_count_data_manual('plan_employer',array('plan_type'=>'Paid'),1,'sum(final_amount) as total_paid');
	$total_paid_employer = $empl_payment_count['total_paid'];
	$seeker_payment_count = $this->common_model->get_count_data_manual('plan_jobseeker',array('plan_type'=>'Paid'),1,'sum(final_amount) as total_paid');
	$total_paid_seeker = $seeker_payment_count['total_paid'];
	$total_paid_total = $total_paid_employer + $total_paid_seeker;
?>
      <div class="row">
        <div class=col-md-3>
          <div>
            <div class="widget bg-white">
              <div class="widget-icon bg-blue pull-left fa fa-user"> </div>
              <div class=overflow-hidden> <span class=widget-title><?php echo $job_seeker; ?></span> <span class=widget-subtitle>Registered Job Seekers</span> </div>
            </div>
          </div>
        </div>
        <div class=col-md-3>
          <div>
          	<div class="widget bg-white">
              <div class="widget-icon bg-danger pull-left fa fa-users"> </div>
              <div class=overflow-hidden> <span class="widget-title"><?php echo $employer; ?></span> <span class=widget-subtitle>Registered Employers</span> </div>
            </div>
          </div>
        </div>
        <div class=col-md-3>
          <div>
          	<div class="widget bg-white">
              <div class="widget-icon bg-success pull-left fa fa-bars"> </div>
              <div class=overflow-hidden> <span class=widget-title><?php echo $job_count; ?></span> <span class=widget-subtitle>Job Posted</span> </div>
            </div>
          </div>
        </div>
                        
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="widget bg-white">
            <div class="widget-details widget-list">
              <div class="mb20">
                <h4 class="no-margin text-uppercase">Total Payment</h4>
                <small class="text-uppercase"></small> 
              </div>
              <a href=javascript:; class=widget-list-item> <span class="label label-info pull-right"><?php echo number_format($total_paid_employer,2); ?></span> Employer </a> 
              <a href=javascript:; class=widget-list-item> <span class="label label-warning pull-right"><?php echo number_format($total_paid_seeker,2); ?></span> Job Seeker </a> 
              <a href=javascript:; class=widget-list-item> <span class="label label-success pull-right"><?php echo number_format($total_paid_total,2); ?></span> Total</a> 
              </div>
          </div>
        </div>
      </div>
<?php
	
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',$where_arra,2,'fullname,email,industries_name,role_name,functional_name,register_date',' register_date desc ',1,5);																	
	$na = $this->common_model->data_not_availabel;
	if(isset($job_seeker_data) && $job_seeker_data !='' && is_array($job_seeker_data) && count($job_seeker_data)> 0)
	{
?>      
      <div class="row">
        <div class="col-md-12">
          <div class="widget bg-white">
            <div class="widget-details widget-list">
              <div class="mb10">
                <h4 class="no-margin text-uppercase">Latest Job Seeker</h4>
                <small class="text-uppercase"></small> 
              </div>
              <div>
              	<table class="table">
                	<thead>
                    	<th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Industries </th>
                        <th>Functional Name</th>
                        <th>Job Role</th>
                        <th>Registered On</th>
                    </thead>
                    <tbody>
                    <?php
					$ij= 1;
					foreach($job_seeker_data as $job_seeker_data_val)
					{
					?>
                    	<tr>
                        	<td><?php echo $ij++;?></td>
                        	<td><?php if(isset($job_seeker_data_val['fullname']) && $job_seeker_data_val['fullname'] !=''){echo $job_seeker_data_val['fullname'];}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['email']) && $job_seeker_data_val['email'] !='')
							{
								if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
									echo $this->common_model->email_disp;
								}
								else
								{
									echo $job_seeker_data_val['email'];
								}
							}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['industries_name']) && $job_seeker_data_val['industries_name'] !=''){echo $job_seeker_data_val['industries_name'];}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['functional_name']) && $job_seeker_data_val['functional_name'] !=''){echo $job_seeker_data_val['functional_name'];}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['role_name']) && $job_seeker_data_val['role_name'] !=''){echo $job_seeker_data_val['role_name'];}else{ echo $na;} ?></td>                            
                            <td><?php echo $this->common_model->displayDate($job_seeker_data_val['register_date']); ?></td>
                        </tr>
                    <?php
					}
					?>
                    </tbody>
                </table>
              </div>
              </div>
          </div>
        </div>
      </div>
<?php
	}
	
	$job_seeker_data = $this->common_model->get_count_data_manual('employer_master_view',$where_arra,2,'fullname,email,industries_name,skill_hire,register_date,company_name',' register_date desc ',1,5);																	
	$na = $this->common_model->data_not_availabel;
	if(isset($job_seeker_data) && $job_seeker_data !='' && is_array($job_seeker_data) && count($job_seeker_data)> 0)
	{
?>      
      <div class="row">
        <div class="col-md-12">
          <div class="widget bg-white">
            <div class="widget-details widget-list">
              <div class="mb10">
                <h4 class="no-margin text-uppercase">Latest Employer</h4>
                <small class="text-uppercase"></small> 
              </div>
              <div>
              	<table class="table">
                	<thead>
                    	<th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Industries </th>
                        <th>Skill Hire</th>
                        <th>Company Name</th>
                        <th>Registered On</th>
                    </thead>
                    <tbody>
                    <?php
					$ij= 1;
					foreach($job_seeker_data as $job_seeker_data_val)
					{
					?>
                    	<tr>
                        	<td><?php echo $ij++;?></td>
                        	<td><?php if(isset($job_seeker_data_val['fullname']) && $job_seeker_data_val['fullname'] !=''){echo $job_seeker_data_val['fullname'];}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['email']) && $job_seeker_data_val['email'] !='')
							{
								if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
									echo $this->common_model->email_disp;
								}
								else
								{
									echo $job_seeker_data_val['email'];
								}
							}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['industries_name']) && $job_seeker_data_val['industries_name'] !=''){echo $job_seeker_data_val['industries_name'];}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['skill_hire']) && $job_seeker_data_val['skill_hire'] !='')
							{
								echo $this->common_model->valueFromId('key_skill_master',$job_seeker_data_val['skill_hire'],'key_skill_name');
								}else{ echo $na;} ?></td>
                            <td><?php if(isset($job_seeker_data_val['company_name']) && $job_seeker_data_val['company_name'] !=''){echo $job_seeker_data_val['company_name'];}else{ echo $na;} ?></td>                            
                            <td><?php echo $this->common_model->displayDate($job_seeker_data_val['register_date']); ?></td>
                        </tr>
                    <?php
					}
					?>
                    </tbody>
                </table>
              </div>
              </div>
          </div>
        </div>
      </div>
<?php
	}
?>