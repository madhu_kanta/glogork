<?php $dna = $this->common_model->data_not_availabel;
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($employer_data['company_profile']) && isset($id) && $id !='')
{
	$employer_data = $this->common_model->get_count_data_manual('employer_master_view',array('id'=>$id),1,'company_profile,id','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left text-bold">Company Profile</div>
        <div class="panel-controls">
            <?php
			if($disp_mode =='view')
			{
			?>
            	<a href="javascript:;" onClick="view_detail_form('company_profile','edit')">Edit</a>
            <?php
			}
			?>
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
         </div>
    </div>
    <div class="panel-body">
    	<?php
			if(isset($respones_ss->response) && $respones_ss->response !='')
			{
				echo $respones_ss->response;
			}
		?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
            	<?php
				if($disp_mode =='view')
				{
				?>
                <p>
                        <?php if(isset($employer_data['company_profile']) && $employer_data['company_profile'] !=''){ echo nl2br($employer_data['company_profile']);}else {echo $dna;} ?>
                </p>
                <?php
				}
				else
				{
					$ele_array = array(
						'company_profile'=>array('type'=>'textarea','is_required'=>'required'),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'employer/save-detail/'.$employer_data['id'].'/company_profile','form_id'=>'form_company_profile','onback_click'=>"view_detail_form('company_profile','view')");
					$this->common_model->set_table_name('employer_master');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
				}
				?>
             </div>
        </div>
    </div>
</div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
if($("#form_company_profile").length > 0)
{
	$("#form_company_profile").validate({
		submitHandler: function(form) 
		{
			edit_profile('company_profile','save');
			return false;
			//return true;
		}
	});
}
</script>
<?php
}
?>