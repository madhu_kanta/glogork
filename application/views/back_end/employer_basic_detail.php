<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($employer_data['fullname']) && $id !='')
{
	$employer_data = $this->common_model->get_count_data_manual('employer_master_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
/*
basic_detail
form_basic_detail
*/
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left text-bold">
                <?php 
                    if(isset($employer_data['fullname']) && $employer_data['fullname'] !='')
                    {
                        echo $employer_data['personal_titles'].' '.$employer_data['fullname'];
                    }
                    else
                    {
                        echo $dna;
                    } 
                ?>
            </div>
            <div class="panel-controls">
            	<?php
                    if($disp_mode =='view')
                    {
                    ?>
                    <a href="javascript:;" onClick="view_detail_form('basic_detail','edit')">Edit</a>
                    <?php
                    }
                ?>
                <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
             </div>
        </div>
        <div class="panel-body">
        	<?php
				if(isset($respones_ss->response) && $respones_ss->response !='')
				{
					echo $respones_ss->response;
				}
                if($disp_mode =='view')
                {
           ?>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-6 text-bold text-center">
                            <?php
                                $avatar = 'assets/front_end/images/avatar-placeholder.png';
                                if(isset($employer_data['profile_pic']) && $employer_data['profile_pic'] !='')
                                {
                                    $temp_img = $employer_data['profile_pic'];
                                    $path_img = "assets/emp_photos/";
                                    if(file_exists($path_img.$temp_img))
                                    {
                                        $avatar = $path_img.$temp_img;
                                    }
                                }
                            ?>
                            <img class="img-responsive" src="<?php echo $base_url.$avatar; ?>" />
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Full Name</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($employer_data['fullname']) && $employer_data['fullname'] !='')
                            {
                                echo $employer_data['personal_titles'].' '.$employer_data['fullname'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>                        
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Email Address</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php 
                            if(isset($employer_data['email']) && $employer_data['email'] !='')
                            { 
                                if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
                                	echo $this->common_model->email_disp;
								}
								else
								{
									echo $employer_data['email'];
								}
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <!--<div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Personal Email</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($employer_data['personal_email']) && $employer_data['personal_email'] !='')
                            { 
                                if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
    	                             echo $this->common_model->email_disp;
								}
								else
								{
								 	echo $employer_data['personal_email'];
								}
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>-->
                    
                     <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Registered On</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php if(isset($employer_data['register_date']) && $employer_data['register_date'] !='')
                            { 
                                echo $this->common_model->displayDate($employer_data['register_date']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Status</label>
                        <label class="col-sm-8 col-xs-8 control-label-val text-bold">
                        	<strong>:</strong>&nbsp;
                        <?php 
                            if(isset($employer_data['status']) && $employer_data['status'] !='' && $employer_data['status'] =='APPROVED')
                            {
                        ?>
                            <span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
                        <?php
                            }
                            else
                            {
                        ?>
                            <span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
                        <?php		
                            }
                        ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Email Verified</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($employer_data['email_verified']) && $employer_data['email_verified'] !=''){ echo $employer_data['email_verified'];}else {echo $dna;} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Profile Approval</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($employer_data['profile_pic_approval']) && $employer_data['profile_pic_approval'] !='')
							{
								if(isset($employer_data['profile_pic_approval']) && $employer_data['profile_pic_approval'] !='' && $employer_data['profile_pic_approval'] =='APPROVED')
								{
							?>
								<span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
							<?php
								}
								else
								{
							?>
								<span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
							<?php		
								}							
							}
							else 
							{
								echo $dna;
							} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Last Login</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($employer_data['last_login']) && $employer_data['last_login'] !='')
                            { 
                                echo $this->common_model->displayDate($employer_data['last_login']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                </div>                    
            </div>
            <?php
				}
				else
				{
					
					$ele_array = array(
						'title'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'personal_titles_master','key_val'=>'id','key_disp'=>'personal_titles'),'value'=>$employer_data['title']),
						'fullname'=>array('is_required'=>'required'),
						'email'=>array('is_required'=>'required','input_type'=>'email'),
						'password'=>array('type'=>'password'),
						'status'=>array('type'=>'radio'),
						'email_verified'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
						'profile_pic_approval'=>array('type'=>'radio'),
						'profile_pic'=>array('type'=>'file','path_value'=>'assets/emp_photos/'),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'enctype'=>'enctype="multipart/form-data"','action'=>'employer/save-detail/'.$employer_data['id'].'/basic_detail','form_id'=>'form_basic_detail','onback_click'=>"view_detail_form('basic_detail','view')");
					$this->common_model->set_table_name('employer_master');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
					//$this->common_model->generate_form();
				}
			?>
        </div>
    </div>
</form>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_basic_detail").length > 0)
	{
		$("#form_basic_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('basic_detail','save');
				return false;
				//return true;
			}
		});
	}
	//$('.datepicker').datepicker({format:'Y-m-d'});
</script>
<?php
}
?>