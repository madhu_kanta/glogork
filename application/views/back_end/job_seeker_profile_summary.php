<?php $dna = $this->common_model->data_not_availabel;
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker',array('id'=>$id),1,'profile_summary,id','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left text-bold">Profile Summary</div>
        <div class="panel-controls">
            <?php
			if($disp_mode =='view')
			{
			?>
            	<a href="javascript:;" onClick="view_detail_form('profile_summary','edit')">Edit</a>
            <?php
			}
			?>
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
         </div>
    </div>
    <div class="panel-body">
    	<?php
			if(isset($respones_ss->response) && $respones_ss->response !='')
			{
				echo $respones_ss->response;
			}
		?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
            	<?php
				if($disp_mode =='view')
				{
				?>
                <p>
                        <?php if(isset($job_seeker_data['profile_summary']) && $job_seeker_data['profile_summary'] !=''){ echo nl2br($job_seeker_data['profile_summary']);}else {echo $dna;} ?>
                </p>
                <?php
				}
				else
				{
					$ele_array = array(
						'profile_summary'=>array('type'=>'textarea'),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job-seeker/save-detail/'.$job_seeker_data['id'].'/profile_summary','form_id'=>'form_profile_summary','onback_click'=>"view_detail_form('profile_summary','view')");
					$this->common_model->set_table_name('jobseeker');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
				}
				?>
             </div>
        </div>
    </div>
</div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
if($("#form_profile_summary").length > 0)
{
	$("#form_profile_summary").validate({
		submitHandler: function(form) 
		{
			edit_profile('profile_summary','save');
			return false;
			//return true;
		}
	});
}
</script>
<?php
}
?>