<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($employer_data['fullname']) && $id !='')
{
	$employer_data = $this->common_model->get_count_data_manual('employer_master_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary form-horizontal">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">
                	Company Detail
                </div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('company_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Logo</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
								<?php
                                $avatar = 'assets/front_end/images/avatar-placeholder.png';
                                if(isset($employer_data['company_logo']) && $employer_data['company_logo'] !='')
                                {
                                    $temp_img = $employer_data['company_logo'];
                                    $path_img = "assets/company_logos/";
                                    if(file_exists($path_img.$temp_img))
                                    {
                                        $avatar = $path_img.$temp_img;
                                    }
                                }
                            ?>
                            <img class="img-responsive" style="height:125px;width:125px" src="<?php echo $base_url.$avatar; ?>" />
                            <br/>

                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-sm-12 col-xs-12">
	                    <div class="form-group mb0">
                        <label class="col-sm-3 col-xs-3 control-label">Company Logo <br/>Approval</label>
                        <label class="col-sm-9 col-xs-9 control-label-val text-bold">
                        	<strong>:</strong>&nbsp;
                        <?php 
                            if(isset($employer_data['company_logo_approval']) && $employer_data['company_logo_approval'] !='' && $employer_data['company_logo_approval'] =='APPROVED')
                            {
                        ?>
                            <span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
                        <?php
                            }
                            else
                            {
                        ?>
                            <span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
                        <?php		
                            }
                        ?>
                        </label>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Name</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['company_name']) && $employer_data['company_name'] !=''){ echo $employer_data['company_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>                    
                </div>
                <div class="row">
	                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    	
                    	<div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Type</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['company_type_name']) && $employer_data['company_type_name'] !=''){ echo $employer_data['company_type_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Industry</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['industries_name']) && $employer_data['industries_name'] !=''){ echo nl2br($employer_data['industries_name']);}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Country</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['country_name']) && $employer_data['country_name'] !=''){ echo $employer_data['country_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">City Name</label>
                            <label class="col-sm-9 col-xs-9 control-label-va" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['city_name']) && $employer_data['city_name'] !=''){ echo $employer_data['city_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Pincode</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['pincode']) && $employer_data['pincode'] !=''){ echo $employer_data['pincode'];}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    	
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Size</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['company_size_name']) && $employer_data['company_size_name'] !=''){ echo $employer_data['company_size_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        
                        
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Website</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['company_website']) && $employer_data['company_website'] !='')
									{ 
										echo '<a target="_blank" href="'.$employer_data['company_website'].'">'.$employer_data['company_website'].'</a>';
									}
									else
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <!--<div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Company Email</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php /*if(isset($employer_data['company_email']) && $employer_data['company_email'] !='')
									{ 
										echo $employer_data['company_email'];
									}
									else
									{
										echo $dna;
									}*/
								?>
                            </label>
                        </div>-->
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Mobile</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['mobile']) && $employer_data['mobile'] !='')
									{ 
										if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
										{
											echo $this->common_model->mobile_disp;
										}
										else
										{
											echo $employer_data['mobile'];
										}
									}
									else
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Mobile Verified</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['mobile_verified']) && $employer_data['mobile_verified'] !='')
									{ 
										echo $employer_data['mobile_verified'];
									}
									else
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>                        
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-3 col-xs-3 control-label">Address</label>
                            <label class="col-sm-9 col-xs-9 control-label-val" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['address']) && $employer_data['address'] !=''){ echo nl2br($employer_data['address']);}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>
                </div>
                <?php
                }
                else
                {	
					$mobile_num = '';
					$country_code = '';
					if(isset($employer_data['mobile']) && $employer_data['mobile'] !='')
					{
						$mobile = $employer_data['mobile'];
						$mobile_arr = explode('-',$mobile);
						if(isset($mobile_arr[0]) && $mobile_arr[0] !='')
						{
							$country_code = $mobile_arr[0];
						}
						if(isset($mobile_arr[1]) && $mobile_arr[1] !='')
						{
							$mobile_num = $mobile_arr[1];
						}
					}
					$where_country_code = '';
					if($country_code !='' && $country_code !='')
					{
						$where_country_code= " ( is_deleted ='No' or (country_code = '$country_code' ))";
					}
					else
					{
						$where_country_code= " ( is_deleted ='No' )";
					}
					$country_code_arr = $this->common_model->get_count_data_manual('country_master',$where_country_code,2,'country_code,country_name','','','',"");
					
					$mobile_ddr= '<div class="col-sm-6 col-lg-6 pl0">
						<select name="country_code" id="country_code" required class="form-control" >
						<option value="">Select Country Code</option>';
						foreach($country_code_arr as $country_code_arr)
						{
							$selected_ddr = '';
							if($country_code == $country_code_arr['country_code'])
							{
								$selected_ddr = ' selected ';
							}
							$mobile_ddr.= '<option '.$selected_ddr.' value='.$country_code_arr['country_code'].'>'.$country_code_arr['country_code'].' ('.$country_code_arr['country_name'].')'.'</option>';
						}						
					$mobile_ddr.='</select>
						</div>
						<div class="col-sm-6 col-lg-6 ">
							<input type="number" required name="mobile_num" id="mobile_num" class="form-control" placeholder="Mobile Number" value ="'.htmlentities(stripcslashes($mobile_num)).'" minlength="8" maxlength="13" />
						</div>';
					$ele_array = array(
						'company_logo'=>array('type'=>'file','path_value'=>'assets/company_logos/'),
						'company_name'=>array('is_required'=>'required'),
						'industry'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'value'=>$employer_data['industry']),
						'company_type'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'company_type_master','key_val'=>'id','key_disp'=>'company_type'),'label'=>'Company Type','value'=>$employer_data['company_type']),
						'company_size'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'company_size_master','key_val'=>'id','key_disp'=>'company_size'),'label'=>'Company Size','value'=>$employer_data['company_size']),
						'company_website'=>array('input_type'=>'url'),
						/*'company_email'=>array('is_required'=>'required','input_type'=>'email'),*/
						'mobile'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Mobile</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$mobile_ddr.'
						  <input type="hidden" name="mobile" id="mobile" value="'.$employer_data['mobile'].'" />
						  </div>
						</div>'),
						'mobile_verified'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
						'address'=>array('type'=>'textarea'),
						'country'=>array('is_required'=>'required','type'=>'dropdown','onchange'=>"dropdownChange('country','city','city_list')",'relation'=>array('rel_table'=>'country_master','key_val'=>'id','key_disp'=>'country_name'),'value'=>$employer_data['country']),
						'city'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'city_master','key_val'=>'id','key_disp'=>'city_name','rel_col_name'=>'country_id','rel_col_val'=>$employer_data['country']),'value'=>$employer_data['city']),
						'pincode'=>array('is_required'=>'required','input_type'=>'number'),
						'company_logo_approval'=>array('type'=>'radio'),
					);
					if($this->common_model->is_demo_mode == 1 && isset($mobile_num) && $mobile_num !='')
					{
						$ele_array['mobile'] = array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Mobile <span class="sub_title_mem">*</span></label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  <span><strong>Disable in demo</strong></span>
						  </div>
						</div>');
					}
					$other_config = array('mode'=>'edit','id'=>$id,'enctype'=>'enctype="multipart/form-data"','action'=>'employer/save-detail/'.$employer_data['id'].'/company_detail','form_id'=>'form_company_detail','onback_click'=>"view_detail_form('company_detail','view')");
					$this->common_model->set_table_name('employer_master');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
                }
            ?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_company_detail").length > 0)
	{
		$("#form_company_detail").validate({
			submitHandler: function(form)
			{
				<?php
				if($this->common_model->is_demo_mode == 1 && isset($mobile_num) && $mobile_num !='')
				{
				}
				else
				{
				?>
					var country_code = $("#country_code").val();
					var mobile_num = $("#mobile_num").val();
					$("#mobile").val(country_code + '-'+mobile_num);
				<?php
				}
				?>				
				
				edit_profile('company_detail','save');
				return false;
			}
		});
	}
</script>
<?php
}
?>