<?php $dna = $this->common_model->data_not_availabel; ?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
<!--<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery.tagsinput/src/jquery.tagsinput.css" />-->

<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/tokenfield-typeahead.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/bootstrap-tokenfield.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/jquery-ui.css" />
<a class="btn btn-info" href="<?php echo $this->common_model->base_url_admin.'job-seeker/seeker_list'; ?>"><i class="fa fa-arrow-left"></i> Back to list</a><br/><br/>
<div class="box-tab justified" id="rootwizard">
	<div id="basic_detail">
        <?php
			$this->load->view('back_end/job_seeker_basic_detail');
		?>
    </div>
    <div id="profile_summary">
		<?php
			$this->load->view('back_end/job_seeker_profile_summary');
		?>
    </div>
    <div id="address_detail">
    	<?php
			$this->load->view('back_end/job_seeker_address_detail');
		?>
    </div>
    <div id="keyskill_detail">
    	<?php
			$this->load->view('back_end/job_seeker_keyskill_detail');
		?>    
    </div>
        
    <div id="education_detail">
	    <?php
			$this->load->view('back_end/job_seeker_education_detail');
		?>    
    </div>
    
    <div id="work_history">
	    <?php
			$this->load->view('back_end/job_seeker_work_history');
		?>    
    </div>
    <div id="lang_detail">
	    <?php
			$this->load->view('back_end/job_seeker_lang_detail');
		?>    
    </div>
    <div id="plan_detail">
	    <?php
			$this->load->view('back_end/job_seeker_plan_detail');
		?>    
    </div>
    <div id="activity_count">
	    <?php
			$this->load->view('back_end/job_seeker_activity_count');
		?>    
    </div>
    <div id="other_detail">
    	<?php
			$this->load->view('back_end/job_seeker_other_detail');
		?>    
    </div>
</div>
<input type="hidden" id="form_url_js" name="form_url_js" value="<?php echo $this->common_model->base_url_admin.'job-seeker/save-detail/'.$job_seeker_data['id'].'/';?>" />
<input type="hidden" id="form_url_delete" name="form_url_delete" value="<?php echo $this->common_model->base_url_admin.'job-seeker/delete-js-data/'.$job_seeker_data['id'].'/';?>" />
<input type="hidden" id="url_view_edit_form" name="url_view_edit_form" value="<?php echo $this->common_model->base_url_admin.'job-seeker/view-detail/'.$job_seeker_data['id'].'/';?>" /> 
<?php
	$this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
	$this->common_model->extra_js[] = 'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js';
	/*$this->common_model->extra_js[] = 'vendor/jquery.tagsinput/src/jquery.tagsinput.js';*/
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/jquery-ui.js';
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/bootstrap-tokenfield.js';
	$this->common_model->extra_js[] = 'vendor/typeahead/typeahead.jquery.min.js';

	
	if(isset($view_edit_mode) && $view_edit_mode !='' && $view_edit_mode =='edit')
	{
		$this->common_model->js_extra_code.= " $(function(){ view_detail_form('basic_detail','edit'); }); ";
	}
?>
<script type="text/javascript">
	function edit_profile_can(form_id,mode)
	{
		var action = $('#form_'+form_id).attr('action');
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var form_data = '';
		var hash_tocken_id = $("#hash_tocken_id").val();
		form_data = form_data + 'csrf_job_portal='+hash_tocken_id;
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	
	function edit_profile(form_id,mode)
	{
		var form_url_js = $("#form_url_js").val();
		var action = form_url_js + form_id;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var form_data = new FormData();
		if($("#profile_pic").length > 0)
		{
			var profile_img = $('input[name=profile_pic]')[0].files[0];
			if(profile_img != 'undefined' && profile_img != '' && profile_img != undefined)
			{
				form_data.append("profile_pic", profile_img);
			}
		}
		if($("#resume_file").length > 0)
		{
			///alert($('input[name=resume_file]')[0].files[0]);
			var resume_file = $('input[name=resume_file]')[0].files[0];
			if(resume_file != 'undefined' && resume_file != '' && resume_file != undefined)
			{
				form_data.append("resume_file", $('input[name=resume_file]')[0].files[0]);
				//form_data.append("resume_file", $('input[name=resume_file]')[0].files[0]);
			}
		}
		var other_data = $('#form_'+form_id).serializeArray();
		$.each(other_data,function(key,input){
			form_data.append(input.name,input.value);
		});
		
		form_data.append('is_ajax',1);
		//var form_data = $('#form_'+form_id).serialize();
		//var hash_tocken_id = $("#hash_tocken_id").val();
		///form_data = 'csrf_job_portal='+hash_tocken_id+'is_ajax=1';
		//form_data = form_data+'&is_ajax=1';
		//form_data = form_data+'&profile_pic='+$('input[name=profile_pic]')[0].files[0];	
		//form_data.append('is_ajax',1);
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   contentType: false,
		   cache: false,
			processData:false,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
				settimeout_div();
				/*new added*/
				setTimeout(function() {
					$('.alert-success').fadeOut('fast');
				}, 4000);
				/*new added*/
		   }
		});
		return false;
	}
	function view_detail_form(form_id,mode)
	{
		var form_url_js = $("#url_view_edit_form").val();
		var action = form_url_js + form_id + '/'+mode;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var hash_tocken_id = $("#hash_tocken_id").val();
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: {'disp_mode':mode,'csrf_job_portal':hash_tocken_id,'is_ajax':1},
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	function lan_show_hide()
	{
		var total_open = parseInt($("#lan_total_count").val());
		if(total_open < 5)
		{
			total_open = total_open + 1;
			$("#lan_row_"+total_open).show();
			$("#lan_total_count").val(total_open);
		}
		if(total_open == 5)
		{
			$("#lan_add_more").hide();
		}
	}
	function delete_js_data(form_id,lan_id)
	{
		if(confirm("Are you sure to delete data?"))
		{
			var form_data = '';
			var hash_tocken_id = $("#hash_tocken_id").val();
			
			var form_url_js = $("#form_url_delete").val();
			var action = form_url_js + form_id+'/'+lan_id;
		
			form_data = form_data + 'csrf_job_portal='+hash_tocken_id;
			show_comm_mask();
			$.ajax({
			   url: action,
			   type: "post",
			   data: form_data,
			   success:function(data)
			   {
					$("#"+form_id).html(data);
					var tocken = $("#hash_tocken_id_temp").val();
					update_tocken(tocken);
					remove_element("#hash_tocken_id_temp",0);
					hide_comm_mask();
					scroll_to_div(form_id);
					$(".panel").collapse() 
			   }
			});
		}
	}

	function substringMatcher(strs)
	{
		return function findMatches(q, cb)
		{
			var matches, substringRegex;
			matches = [];		
			substrRegex = new RegExp(q, 'i');
			$.each(strs, function(i, str)
			{
				if (substrRegex.test(str))
				{
					matches.push(str);
				}
			});
			cb(matches);
		};
	}
</script>