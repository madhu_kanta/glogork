<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
/*
basic_detail
form_basic_detail
*/
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left text-bold">
                <?php 
                    if(isset($job_seeker_data['fullname']) && $job_seeker_data['fullname'] !='')
                    {
                        echo $job_seeker_data['personal_titles'].' '.$job_seeker_data['fullname'];
                    }
                    else
                    {
                        echo $dna;
                    } 
                ?>
            </div>
            <div class="panel-controls">
            	<?php
                    if($disp_mode =='view')
                    {
                    ?>
                    <a href="javascript:;" onClick="view_detail_form('basic_detail','edit')">Edit</a>
                    <?php
                    }
                ?>
                <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
             </div>
        </div>
        <div class="panel-body form-horizontal ">
        	<?php
				if(isset($respones_ss->response) && $respones_ss->response !='')
				{
					echo $respones_ss->response;
				}
                if($disp_mode =='view')
                {
           ?>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-6 text-bold text-center">
                            <?php
                                $avatar = 'assets/front_end/images/avatar-placeholder.png';
                                if(isset($job_seeker_data['profile_pic']) && $job_seeker_data['profile_pic'] !='')
                                {
                                    $temp_img = $job_seeker_data['profile_pic'];
                                    $path_img = "assets/js_photos/";
                                    if(file_exists($path_img.$temp_img))
                                    {
                                        $avatar = $path_img.$temp_img;
                                    }
                                }
                            ?>
                            <img style="margin:0 auto" class="img-responsive" src="<?php echo $base_url.$avatar; ?>" />
                            <br/>
                            <?php
                            if(isset($job_seeker_data['resume_file']) && $job_seeker_data['resume_file'] !='' && file_exists('assets/resume_file/'.$job_seeker_data['resume_file']))
                            {
                            ?>
                            <a target="_blank" href="<?php echo $base_url.'assets/resume_file/'.$job_seeker_data['resume_file'];?>" >View Resume</a>
                            <?php
                            }
							else
							{
								echo 'Resume Not uploaded';
							}
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Full Name</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_seeker_data['fullname']) && $job_seeker_data['fullname'] !='')
                            {
                                echo $job_seeker_data['personal_titles'].' '.$job_seeker_data['fullname'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>                        
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Email Address</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_seeker_data['email']) && $job_seeker_data['email'] !='')
                            { 
                                if($this->common_model->is_demo_mode == 1)
								{
                                	echo $this->common_model->emial_disp;
								}
								else
								{
									echo $job_seeker_data['email'];
								}
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Mobile</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_seeker_data['mobile']) && $job_seeker_data['mobile'] !='')
                            { 
                                if($this->common_model->is_demo_mode == 1)
								{
                                	echo $this->common_model->mobile_disp;
								}
								else
								{
								 	echo $job_seeker_data['mobile'];
								}
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Mobile Verified</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_seeker_data['mobile_verified']) && $job_seeker_data['mobile_verified'] !='')
                            { 
								 echo $job_seeker_data['mobile_verified'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>  
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Land Line</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_seeker_data['landline']) && $job_seeker_data['landline'] !='')
                            { 
                                if($this->common_model->is_demo_mode == 1)
								{
									echo $this->common_model->mobile_disp;
								}
								else
								{
									echo $job_seeker_data['landline'];
								}
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Registered On</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php if(isset($job_seeker_data['register_date']) && $job_seeker_data['register_date'] !='')
                            { 
                                echo $this->common_model->displayDate($job_seeker_data['register_date']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Status</label>
                        <label class="col-sm-8 col-xs-8 control-label-val text-bold">
                        	<strong>:</strong>&nbsp;
                        <?php 
                            if(isset($job_seeker_data['status']) && $job_seeker_data['status'] !='' && $job_seeker_data['status'] =='APPROVED')
                            {
                        ?>
                            <span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
                        <?php
                            }
                            else
                            {
                        ?>
                            <span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
                        <?php		
                            }
                        ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Gender</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_seeker_data['gender']) && $job_seeker_data['gender'] !=''){ echo $job_seeker_data['gender'];} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Birth Date</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_seeker_data['birthdate']) && $job_seeker_data['birthdate'] !='')
                            {
                                echo $this->common_model->displayDate($job_seeker_data['birthdate']);
                            } 
                            ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Marital Status</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_seeker_data['marital_status_val']) && $job_seeker_data['marital_status_val'] !=''){ echo $job_seeker_data['marital_status_val'];}else {echo $dna;} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Last Login</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_seeker_data['last_login']) && $job_seeker_data['last_login'] !='')
                            { 
                                echo $this->common_model->displayDate($job_seeker_data['last_login']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                </div>                    
            </div>
            <?php
				}
				else
				{
					$mobile_num = '';
					$country_code = '';
					if(isset($job_seeker_data['mobile']) && $job_seeker_data['mobile'] !='')
					{
						$mobile = $job_seeker_data['mobile'];
						$mobile_arr = explode('-',$mobile);
						if(isset($mobile_arr[0]) && $mobile_arr[0] !='')
						{
							$country_code = $mobile_arr[0];
						}
						if(isset($mobile_arr[1]) && $mobile_arr[1] !='')
						{
							$mobile_num = $mobile_arr[1];
						}
					}
					$where_country_code = '';
					if($country_code !='' && $country_code !='')
					{
						$where_country_code= " ( is_deleted ='No' or (country_code = '$country_code' ))";
					}
					else
					{
						$where_country_code= " ( is_deleted ='No' )";
					}
					$country_code_arr = $this->common_model->get_count_data_manual('country_master',$where_country_code,2,'country_code,country_name','','','',"");
					
					$mobile_ddr= '<div class="col-sm-6 col-lg-6 pl0">
						<select name="country_code" id="country_code" required class="form-control" >
						<option value="">Select Country Code</option>';
						foreach($country_code_arr as $country_code_arr)
						{
							$selected_ddr = '';
							if($country_code == $country_code_arr['country_code'])
							{
								$selected_ddr = ' selected ';
							}
							$mobile_ddr.= '<option '.$selected_ddr.' value='.$country_code_arr['country_code'].'>'.$country_code_arr['country_code'].' ('.$country_code_arr['country_name'].')'.'</option>';
						}						
					$mobile_ddr.='</select>
						</div>
						<div class="col-sm-6 col-lg-6 ">
							<input type="number" required name="mobile_num" id="mobile_num" class="form-control" placeholder="Mobile Number" value ="'.htmlentities(stripcslashes($mobile_num)).'"  minlength="8" maxlength="13" />
						</div>';
						
		$title_arr =  $this->common_front_model->get_personal_titles();
		$title_code = '<div class="col-sm-12 col-lg-12 pl0">
			<select name="title" id="title" required class="form-control" >
			<option value="">Select Title</option>';
			foreach($title_arr as $title_arr_val)
			{			
				$gn_class = '';
				if(strtolower(trim($title_arr_val['personal_titles'])) == 'mr.' || strtolower(trim($title_arr_val['personal_titles'])) == 'mr')
				{
					$gn_class = 'gn_classmale';
				}
				else if(strtolower(trim($title_arr_val['personal_titles'])) == 'ms.' || strtolower(trim($title_arr_val['personal_titles'])) == 'ms' || strtolower(trim($title_arr_val['personal_titles'])) == 'mrs' || strtolower(trim($title_arr_val['personal_titles'])) == 'mrs.' || strtolower(trim($title_arr_val['personal_titles'])) == "ma'am" || strtolower(trim($title_arr_val['personal_titles'])) == "ma'am.")
				{
					$gn_class = 'gn_classfemale';
				}	
				$selected_ddr = '';
				if($job_seeker_data['title'] == $title_arr_val['id'])
				{
					$selected_ddr = ' selected ';
				}
				$title_code.= '<option '.$selected_ddr.' class="'.$gn_class.'" value='.$title_arr_val['id'].'>'.$title_arr_val['personal_titles'].'</option>';
			}
				$title_code.='</select>
					</div>';
					$ele_array = array(
						'gender'=>array('type'=>'radio','value_arr'=>array('Male'=>'Male','Female'=>'Female'),'onclick'=>"change_gender(this.value,'add')"),
						/*'title'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'personal_titles_master','key_val'=>'id','key_disp'=>'personal_titles'),'value'=>$job_seeker_data['title']),*/
						'title'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Title</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$title_code.'
						  </div>
						</div>'),
						'fullname'=>array('is_required'=>'required'),
						'mobile'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Mobile</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$mobile_ddr.'
						  <input type="hidden" name="mobile" id="mobile" value="'.$job_seeker_data['mobile'].'" />
						  </div>
						</div>'),
						'mobile_verified'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
						'email'=>array('is_required'=>'required','input_type'=>'email','check_duplicate'=>'Yes'),
						'password'=>array('type'=>'password'),
						'landline'=>array(),
						'status'=>array('type'=>'radio'),
						
						'birthdate'=>array('is_required'=>'required','input_type'=>'date'),
						'marital_status'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'marital_status_master','key_val'=>'id','key_disp'=>'marital_status'),'value'=>$job_seeker_data['marital_status']),						
						'profile_pic'=>array('type'=>'file','path_value'=>'assets/js_photos/'),
						'resume_file'=>array('type'=>'file','path_value'=>'assets/resume_file/','extension'=>'doc|docx|pdf|rtf|txt','display_img'=>'No'),
						/*'mobile'=>array('is_required'=>'required'),*/
					);
					if($this->common_model->is_demo_mode == 1 && isset($mobile_num) && $mobile_num !='')
					{
						$ele_array['mobile'] = array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Mobile <span class="sub_title_mem">*</span></label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  <span><strong>Disable in demo</strong></span>
						  </div>
						</div>');
					}
					$other_config = array('mode'=>'edit','id'=>$id,'enctype'=>'enctype="multipart/form-data"','action'=>'job-seeker/save-detail/'.$job_seeker_data['id'].'/basic_detail','form_id'=>'form_basic_detail','onback_click'=>"view_detail_form('basic_detail','view')");
					$this->common_model->set_table_name('jobseeker');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
					//$this->common_model->generate_form();
				}
			?>
        </div>
    </div>
</form>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	$(function(){change_gender("<?php echo $job_seeker_data['gender'];?>","edit") });
	if($("#form_basic_detail").length > 0)
	{
		$("#form_basic_detail").validate({
			submitHandler: function(form)
			{
				<?php
				if($this->common_model->is_demo_mode == 1 && isset($mobile_num) && $mobile_num !='')
				{
					
				}
				else				
				{
				?>
					var country_code = $("#country_code").val();
					var mobile_num = $("#mobile_num").val();
					$("#mobile").val(country_code + '-'+mobile_num);
				<?php
				}
				?>
				
				edit_profile('basic_detail','save');
				return false;
				//return true;
			}
		});
	}	
</script>
<?php
}
?>