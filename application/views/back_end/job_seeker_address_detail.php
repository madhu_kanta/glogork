<?php $dna = $this->common_model->data_not_availabel;
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
//address_detail
///form_address_detail
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left text-bold">Address Details</div>
            <div class="panel-controls">
                <?php
                    if($disp_mode =='view')
                    {
                    ?>
                    <a href="javascript:;" onClick="view_detail_form('address_detail','edit')">Edit</a>
                    <?php
                    }
                ?>
                <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i></a>
             </div>
        </div>
        <div class="panel-body form-horizontal">
        	<?php
				if(isset($respones_ss->response) && $respones_ss->response !='')
				{
					echo $respones_ss->response;
				}
                if($disp_mode =='view')
                {
            ?>
	            <div class="row">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Address</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['address']) && $job_seeker_data['address'] !=''){ echo nl2br($job_seeker_data['address']);}else {echo $dna;} ?>
                            </label>
                        </div>
                     </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">City</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
	                            <strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['city_name']) && $job_seeker_data['city_name'] !=''){ echo $job_seeker_data['city_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Home City</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['home_city']) && $job_seeker_data['home_city'] !=''){ echo $job_seeker_data['home_city'];}else {echo $dna;} ?>
                            </label>
                        </div>
                      </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Country</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['country_name']) && $job_seeker_data['country_name'] !=''){ echo $job_seeker_data['country_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Pin Code</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['pincode']) && $job_seeker_data['pincode'] !=''){ echo $job_seeker_data['pincode'];}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>
	            </div>
            <?php
                }
                else
                {
					$ele_array = array(
						'address'=>array('type'=>'textarea'),
						'country'=>array('is_required'=>'required','type'=>'dropdown','onchange'=>"dropdownChange('country','city','city_list')",'relation'=>array('rel_table'=>'country_master','key_val'=>'id','key_disp'=>'country_name'),'value'=>$job_seeker_data['country']),	// for relation dropdown
						'city'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'city_master','key_val'=>'id','key_disp'=>'city_name','rel_col_name'=>'country_id','rel_col_val'=>$job_seeker_data['country']),'value'=>$job_seeker_data['city']),	// for relation dropdown
						'home_city'=>array(),
						'pincode'=>array('is_required'=>'required','input_type'=>'number'),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job-seeker/save-detail/'.$job_seeker_data['id'].'/address_detail','form_id'=>'form_address_detail','onback_click'=>"view_detail_form('address_detail','view')");
					$this->common_model->set_table_name('jobseeker');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
                }
            ?>
        </div>
    </div>
</form>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_address_detail").length > 0)
	{
		$("#form_address_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('address_detail','save');
				return false;
				//return true;
			}
		});
	}
	/*$('#home_city').tokenfield({
		autocomplete: {
		  source: function (request, response) {
			  $.get("<?php echo base_url(); ?>sign_up/get_suggestion_city/city_name/"+request.term, {
			  }, function (data) {
				 response(data);
			  });
		  },
		  delay: 100
		},
		showAutocompleteOnFocus: true
	});*/
	/*$('#home_city').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		 if (existingTokens != '')
		 {
			 event.preventDefault();
		 }
	}); */

</script>
<?php
}
?>