<!DOCTYPE html>
<html class=no-js>
<head>
	<meta charset="utf-8" />
	<title><?php if(isset($page_title) && $page_title !=''){ echo $page_title;} ?></title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width" />
	<?php if(isset($config_data['upload_favicon']) && $config_data['upload_favicon'] !=''){ ?>
		<link rel="shortcut icon" href="<?php echo $base_url.'assets/logo/'.$config_data['upload_favicon']; ?>" />
	<?php } ?>
	<link rel="stylesheet" href="<?php echo $base_url; ?>assets/back_end/styles/app.min.df5e9cc9.css?ver=1" />
	<?php if(isset($page_title) && $page_title =='Dashboard'){ ?>
	<link rel="stylesheet" href="<?php echo $base_url; ?>assets/back_end/styles/climacons-font.249593b4.css" />
	<link rel="stylesheet" href="<?php echo $base_url; ?>assets/back_end/vendor/rickshaw/rickshaw.min.css" />
	<?php }
	if(isset($this->common_model->extra_css) && $this->common_model->extra_css !='' && is_array($this->common_model->extra_css) && count($this->common_model->extra_css) > 0)
	{
		$extra_css = $this->common_model->extra_css;
		foreach($extra_css as $extra_css_val)
		{
	?>
	    <link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/'.$extra_css_val; ?>" />
	<?php
		}
	}
	?>
	 <style type="text/css">
    	/* .select2-container{ width:100% !important; }  */
		.select2-container{ width: 535px !important; }
    </style>
</head>
<body>
	<div class="app layout-fixed-header">
		<div class="sidebar-panel offscreen-left">
	    	<div class="brand">
	      		<div class="brand-logo">
      				<a href="<?php echo $base_url.$admin_path.'/dashboard'; ?>">
      				<?php if(isset($config_data['upload_logo']) && $config_data['upload_logo'] !=''){ ?>
          			<img alt="Admin Panel" style="width:134px" src="<?php echo $base_url.'assets/logo/'.$config_data['upload_logo']; ?>" />
			        <?php
			        	}
			        	else
			          	{
              				echo $config_data['web_frienly_name'];
          				}
				    ?>
			        </a>
			    </div>
      			<a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu"> 	
                	<span></span> 
                    <span></span> 
                    <span></span> 
                    <span></span> 
                </a> 
            </div>
    		<nav role="navigation" style="margin-top:20px !important;">
      			<ul class="nav">
        			<li id="dashboard" <?php if(isset($page_title) && $page_title =='Dashboard'){ echo 'class="active"';} ?>>
                    	<a href="<?php echo $base_url.$admin_path.'/dashboard';?>"> <i class="fa fa-tachometer"></i> <span>Dashboard</span> </a> 
                    </li>
        			<li id="site_setting">
                    	<a href=javascript:;> <i class="fa fa-cogs"></i> <span>Site Settings</span> </a>
      					<ul class=sub-menu>
            <li id="logo_favicon"> <a href="<?php echo $base_url.$admin_path.'/site-setting/logo-favicon';?>"> <span>Logo &amp; Favicon</span> </a> </li>
            <!--<li id="job_prefix"> <a href="<?php echo $base_url.$admin_path.'/site-setting/job-prefix';?>"> <span>Job Prefix</span> </a> </li>-->
           	<li id="update_email"> <a href="<?php echo $base_url.$admin_path.'/site-setting/update-email';?>"> <span>Email</span> </a> </li> 
            
            <li id="basic_setting"> <a href="<?php echo $base_url.$admin_path.'/site-setting/basic-setting';?>"> <span>Basic Site Settings</span> </a> </li>
            <li id="social_site_setting"> <a href="<?php echo $base_url.$admin_path.'/site-setting/social-site-setting';?>"> <span>Social Media Link</span> </a> </li>
            
            <li id="analytics_code_setting"> <a href="<?php echo $base_url.$admin_path.'/site-setting/analytics-code-setting';?>"> <span>Google Analytics Code</span> </a> </li>
            <li id="change_password"> <a href="<?php echo $base_url.$admin_path.'/site-setting/change-password';?>"> <span>Change Password</span> </a> </li>
           
            <li id="currency_man"> <a href="<?php echo $base_url.$admin_path.'/site-setting/currency-man';?>"> <span>Currency Management</span> </a> </li>
            
            <!--<li id="color_change"> <a href="<?php echo $base_url.$admin_path.'/site-setting/color-change';?>"> <span>Site Color</span> </a> </li>-->
          </ul>
			        </li>
                    <li id="new_listing"> 
                    	<a href=javascript:;> <i class="fa fa-plus"></i> <span>Add New Details</span> </a>
          				<ul class=sub-menu>
                        	<li class="new_listing_add" id="country_list">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/country-list';?>"> <span>Country</span> </a> 
                            </li>
                            <li class="new_listing_add" id="state_list">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/state-list';?>"> <span>State</span> </a> 
                            </li>
                            
            				<li class="new_listing_add" id="city_list">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/city-list';?>"> <span>City</span> </a> 
                            </li>
                            
                            <li class="new_listing_add" id="marital_status_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/marital-status-man';?>"> <span>Marital Status</span> </a>
                            </li>
                            <li class="new_listing_add" id="personal_titles_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/personal-titles-man';?>"> <span>Personal Titles</span> </a>
                            </li>
                            
                            <li class="new_listing_add" id="educational_qua_group">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/educational-qua-group';?>"> <span>Qualification Group</span> </a> 
                            </li>
                            <li class="new_listing_add" id="educational_qualification_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/educational-qualification-man';?>"> <span>Educational Qualification</span> </a> 
                            </li>
                            
                            <li class="new_listing_add" id="industries_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/industries-man';?>"> <span>Industries</span> </a> 
                            </li>
                            <li class="new_listing_add" id="functional_area_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/functional-area-man';?>"> <span>Functional Area</span> </a> 
                            </li>
                            <li class="new_listing_add" id="job_role_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/job-role-man';?>"> <span>Job Role</span> </a> 
                            </li>
                            <li class="new_listing_add" id="key_skill_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/key-skill-man';?>"> <span>Key Skill</span> </a> 
                            </li>
                            <li class="new_listing_add" id="skill_language_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/skill-language-man';?>"> <span>Human Language</span> </a>
                            </li>
                            <li class="new_listing_add" id="skill_level_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/skill-level-man';?>"> <span>Proficiency Level</span> </a>
                            </li>
                            <li class="new_listing_add" id="company_size_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/company-size-man';?>"> <span>Company Size</span> </a> 
                            </li>
                            <li class="new_listing_add" id="company_type_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/company-type-man';?>"> <span>Company Type</span> </a> 
                            </li>
                             <li class="new_listing_add" id="job_payment_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/job-payment-man';?>"> <span>Job Payment</span> </a>
                            </li>
                            <li class="new_listing_add" id="job_type_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/job-type-man';?>"> <span>Job Type</span> </a>
                            </li>
                            <li class="new_listing_add" id="employement_type_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/employement-type-man';?>"> <span>Employment Type</span> </a>
                            </li>
                            <li class="new_listing_add" id="shift_type_man">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/shift-type-man';?>"> <span>Job Shift Type</span> </a>
                            </li>
							 <li class="new_listing_add" id="salary_range_list">
                            	<a href="<?php echo $base_url.$admin_path.'/new-listing/salary-range-list';?>"> <span>Salary Range</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <li id="job_seeker">
                    	<a href=javascript:;> <i class="fa fa-user"></i> <span>Job Seeker</span> </a>
          				<ul class="sub-menu">
            				<li class="job_seeker_add seeker_detail" id="seeker_list">
                            	<a href="<?php echo $base_url.$admin_path.'/job-seeker/seeker-list/ALL/1/yes';?>"> <span>All Job Seeker</span> </a> 
                            </li>
                            <li class="job_seeker_add " id="active_seeker_list">
                            	<a href="<?php echo $base_url.$admin_path.'/job-seeker/active-seeker-list/ALL/1/yes';?>"> <span>Active to Paid</span> </a> 
                            </li>
                            <li class="job_seeker_add " id="paid_seeker_list">
                            	<a href="<?php echo $base_url.$admin_path.'/job-seeker/paid-seeker-list/ALL/1/yes';?>"> <span>Paid Job Seeker</span> </a> 
                            </li>
                            <li class="job_seeker_add " id="expired_seeker_list">
                            	<a href="<?php echo $base_url.$admin_path.'/job-seeker/expired-seeker-list/ALL/1/yes';?>"> <span>Expired Job Seeker</span> </a> 
                            </li>
                            <li class="" id="delete_request">
                            	<a href="<?php echo $base_url.$admin_path.'/job-seeker/delete-request/ALL/1/yes';?>"> <span>Delete Request</span> </a> 
                            </li>
			          	</ul>
			        </li>
                    <li id="employer">
                    	<a href=javascript:;> <i class="fa fa-user"></i> <span>Employer</span> </a>
          				<ul class="sub-menu">
            				<li class="employer_add employer_detail employer" id="employer_list">
                            	<a href="<?php echo $base_url.$admin_path.'/employer/employer-list/ALL/1/yes';?>"> <span>All Employer</span> </a> 
                            </li>
                            <li class="employer_add employer_detail employer" id="active_employer_list">
                            	<a href="<?php echo $base_url.$admin_path.'/employer/active-employer-list/ALL/1/yes';?>"> <span>Active to Paid</span> </a> 
                            </li>
                            <li class="employer_add employer_detail employer" id="paid_employer_list">
                            	<a href="<?php echo $base_url.$admin_path.'/employer/paid-employer-list/ALL/1/yes';?>"> <span>Paid Employer</span> </a> 
                            </li>
                            <li class="employer_add employer_detail employer" id="expired_employer_list">
                            	<a href="<?php echo $base_url.$admin_path.'/employer/expired-employer-list/ALL/1/yes';?>"> <span>Expired Employer</span> </a> 
                            </li>
                            <li class="" id="delete_request">
                            	<a href="<?php echo $base_url.$admin_path.'/employer/delete-request/ALL/1/yes';?>"> <span>Delete Request</span> </a> 
                            </li>
			          	</ul>
			        </li>
                    <li id="job">
                    	<a href=javascript:;> <i class="fa fa-tasks"></i> <span>Job Listing</span> </a>
          				<ul class="sub-menu">
            				<li class="job_list job job_add job_detail" id="job_list">
                            	<a href="<?php echo $base_url.$admin_path.'/job/job-list/ALL/1/yes';?>"> <span>Job Listing</span> </a> 
                            </li>
			          	</ul>
			        </li>
                    <li id="member_plan">
                    	<a href=javascript:;> <i class="fa fa-money"></i> <span>Member Plan</span> </a>
          				<ul class="sub-menu">
            				<li class="member_plan job_seeker_plan" id="job_seeker_plan">
                            	<a href="<?php echo $base_url.$admin_path.'/member-plan/job-seeker-plan/ALL/1/yes';?>"> <span>Job Seeker Plan</span> </a> 
                            </li>
                            <li class="member_plan employer_plan" id="employer_plan">
                            	<a href="<?php echo $base_url.$admin_path.'/member-plan/employer-plan/ALL/1/yes';?>"> <span>Employer Plan</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <li id="manage_coupan">
                    	<a href=javascript:;> <i class="fa fa-money"></i> <span>Member Coupon Code</span> </a>
          				<ul class="sub-menu">
            				<li class="manage_coupan coupan_code_add" id="coupan_code_list">
                            	<a href="<?php echo $base_url.$admin_path.'/manage-coupan/coupan-code-list/ALL/1/yes';?>"> <span>Coupon Code</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <li id="approval">
                    	<a href=javascript:;> <i class="fa fa-thumbs-up"></i> <span>Approval</span> </a>
          				<ul class="sub-menu">
            				<li class="approval" id="job_seeker_photo">
                            	<a href="<?php echo $base_url.$admin_path.'/approval/job-seeker-photo';?>"> <span>Job Seeker Photo</span> </a> 
                            </li>
                            <li class="approval" id="job_seeker_resume">
                            	<a href="<?php echo $base_url.$admin_path.'/approval/job-seeker-resume';?>"> <span>Job Seeker Resume</span> </a> 
                            </li>
                            <li class="approval" id="employer_photo">
                            	<a href="<?php echo $base_url.$admin_path.'/approval/employer-photo';?>"> <span>Employer Photo</span> </a> 
                            </li>
                            <li class="approval" id="company_logos">
                            	<a href="<?php echo $base_url.$admin_path.'/approval/company-logos';?>"> <span>Company Logo</span> </a> 
                            </li>
                            
			          	</ul>
			        </li>
                    <li id="sales_report"> 
                    	<a href=javascript:;> <i class="fa fa-bar-chart"></i> <span>Sales Report</span> </a>
          				<ul class=sub-menu>
            				<li class="sales_report job_seeker" id="job_seeker">
                            	<a href="<?php echo $base_url.$admin_path.'/sales-report/job-seeker/ALL/1';?>"> <span>Job Seeker</span> </a> 
                            </li>
                            <li class="sales_report employer" id="employer">
                            	<a href="<?php echo $base_url.$admin_path.'/sales-report/employer/ALL/1';?>"> <span>Employer</span> </a>
                            </li>
			          	</ul>
			        </li>
                    
                    <li id="content_management"> 
                    	<a href=javascript:;> <i class="fa fa-book"></i> <span>Content Management</span> </a>
          				<ul class=sub-menu>
            				<li class="content_management_add" id="cms_pages">
                            	<a href="<?php echo $base_url.$admin_path.'/content-management/cms-pages';?>"> <span>Cms Pages</span> </a> 
                            </li>
			          	</ul>
			        </li>
                     <li id="blog_management"> 
                        <a href=javascript:;> <i class="fa fa-bar-chart"></i> <span>Developer Tool</span> </a>
                        <ul class=sub-menu>
                            <li class="blog_management_add" id="blog_list">
                                <a href="<?php echo $base_url.$admin_path.'/All_tickets/ticket_list';?>"> <span>All Tickets</span> </a> 
                            </li>
                            <li class="blog_management_add" id="blog_list">
                                <a href="<?php echo $base_url.$admin_path.'/All_tickets/ticket_list/add-data';?>"> <span>Add Ticket</span> </a> 
                            </li>
                        </ul>
                    </li>


                    <li id="blog_management"> 
                    	<a href=javascript:;> <i class="fa fa-bar-chart"></i> <span>Blog Management</span> </a>
          				<ul class=sub-menu>
            				<li class="blog_management_add" id="blog_list">
                            	<a href="<?php echo $base_url.$admin_path.'/blog-management/blog-list';?>"> <span>Blog Management</span> </a> 
                            </li>
			          	</ul>
			        </li>


                    <li id="legislazione_management"> 
                        <a href=javascript:;> <i class="fa fa-bar-chart"></i> <span>Legislazione Management</span> </a>
                        <ul class=sub-menu>
                            <li class="legislazione_management_add" id="legislazione_list">
                                <a href="<?php echo $base_url.$admin_path.'/legislazione-management/legislazione-list';?>"> <span>Legislazione Management</span> </a> 
                            </li>
                        </ul>
                    </li>




                    <li id="email_templates">
                    	<a href=javascript:;> <i class="fa fa-envelope"></i> <span>Email Templates</span> </a>
          				<ul class=sub-menu>
            				<li id="email_templates">
                            	<a href="<?php echo $base_url.$admin_path.'/email-templates/email-templates';?>"> <span>Email Templates</span> </a>
                            </li>
                            <li class="email_templates_add" id="add_email_templates">
                            	<a href="<?php echo $base_url.$admin_path.'/email-templates/email-templates/add-data';?>"> <span>Add Email Templates</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <li id="sms_templates">
                    	<a href=javascript:;> <i class="fa fa-mobile"></i> <span>SMS Templates</span> </a>
          				<ul class=sub-menu>
            				<li id="sms_templates">
                            	<a href="<?php echo $base_url.$admin_path.'/sms-templates/sms-templates';?>"> <span>SMS Templates</span> </a>
                            </li>
                            <li id="sms_configuration"> <a href="<?php echo $base_url.$admin_path.'/sms-templates/sms-configuration';?>"> <span>SMS Configuration</span> </a> </li>
			          	</ul>
			        </li>
					<li id="bulk_email"> 
                    	<a href=javascript:;> <i class="fa fa-bar-chart"></i> <span>Send Bulk Email</span> </a>
          				<ul class=sub-menu>
            				<li class="blog_management_add" id="bulk_email">
                            	<a href="<?php echo $base_url.$admin_path.'/bulk-email';?>"> <span>Send Bulk Email</span> </a> 
                            </li>
			          	</ul>
			        </li>
                    <li id="advertisement_management"> 
                    	<a href=javascript:;> <i class="fa fa-tint"></i> <span>Advertisement</span> </a>
          				<ul class=sub-menu>
            				<li class="advertisement_management_add" id="adv_list">
                            	<a href="<?php echo $base_url.$admin_path.'/advertisement-management/adv-list';?>"> <span>Advertisement</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <?php
						if($base_url =='http://192.168.1.111/job_portal/')
						{
					?>
                    <li id="webservice_man"> 
                    	<a href=javascript:;> <i class="fa fa-tint"></i> <span>Web service</span> </a>
          				<ul class=sub-menu>
            				<li class="webservice_man_add" id="adv_list">
                            	<a href="<?php echo $base_url.$admin_path.'/webservice-man/service-list';?>"> <span>Web Service</span> </a>
                            </li>
			          	</ul>
			        </li>
                    <?php
						}
					?>
		      	</ul>
    		</nav>
  		</div>
  		<div class="main-panel">
    		<header class="header navbar">
      			<div class="brand visible-xs">
        			<div class=toggle-offscreen> <a href=# class="hamburger-icon visible-xs" data-toggle=offscreen data-move=ltr> <span></span> <span></span> <span></span> </a> 
                    </div>
        			<div class=brand-logo> <?php if(isset($config_data['web_logo_path']) && $config_data['web_logo_path'] !=''){ ?>
          				<img style="width:134px" src="<?php echo $base_url.'assets/logo/'.$config_data['web_logo_path']; ?>"/> 
			          <?php
					  }
					  else
					  {
						  echo $config_data['web_frienly_name'];
					  }
				  	?> 
                  	</div>
        			<div class=toggle-chat> <a href=javascript:; class="hamburger-icon v2 visible-xs" data-toggle=layout-chat-open> <span></span> <span></span> <span></span> </a> 
                    </div>
      			</div>
      			<ul class="nav navbar-nav hidden-xs">
                	<li>
                  		<p class="navbar-text bold"> <?php if(isset($page_title)){ echo $page_title;} ?> </p>
                	</li>
              	</ul>
                <?php
					$user_daat = $this->common_model->get_session_data();
					$username_dip = 'Admin';
					if(isset($user_daat['username']) && $user_daat['username'] !='')
					{
						$username_dip = $user_daat['username'];
					}
				?>
      			<ul class="nav navbar-nav navbar-right hidden-xs">
        			<!--<li>
                        <a href=javascript:; data-toggle=dropdown> <i class="fa fa-bell-o"></i>
                            <div class="status bg-danger border-danger animated bounce"></div>
                        </a>
                  		<ul class="dropdown-menu notifications">
                        	<li class=notifications-header>
                          		<p class="text-muted small">You have 3 new messages</p>
                        	</li>
                            <li>
                              <ul class=notifications-list>
                                <li> <a href=javascript:;> <span class="pull-left mt2 mr15"> <img src="<?php echo $base_url.'assets/back_end/';?>images/avatar.21d1cc35.jpg" class="avatar avatar-xs img-circle" alt=""> </span>
                                  <div class=overflow-hidden> <span>Sean launched a new application</span> <span class=time>2 seconds ago</span> </div>
                                  </a> </li>
                                <li> <a href=javascript:;>
                                  <div class="pull-left mt2 mr15">
                                    <div class="circle-icon bg-danger"> <i class="fa fa-chain-broken"></i> </div>
                                  </div>
                                  <div class=overflow-hidden> <span>Removed chrome from app list</span> <span class=time>4 Hours ago</span> </div>
                                  </a> </li>
                                <li> <a href=javascript:;> <span class="pull-left mt2 mr15"> <img src="<?php echo $base_url.'assets/back_end/';?>images/face3.0306ffff.jpg" class="avatar avatar-xs img-circle" alt=""> </span>
                                  <div class=overflow-hidden> <span class=text-muted>Jack Hunt has registered</span> <span class=time>9 hours ago</span> </div>
                                  </a> </li>
                              </ul>
                            </li>
                    		<li class=notifications-footer> <a href=javascript:;>See all messages</a> </li>
                  		</ul>
                	</li>-->
        			<li> <a href="javascript:;" data-toggle="dropdown"> <!--<img src="<?php echo $base_url; ?>assets/back_end/images/profile.png" class="header-avatar img-circle ml10" alt=user title=user>--> <i style="font-size:24px" class="fa fa-user fa-6x "></i>&nbsp;&nbsp; <span class=pull-left><?php echo $username_dip; ?>&nbsp;&nbsp;&nbsp;</span> </a>
          <ul class=dropdown-menu>
            <li> <a href="<?php echo $base_url.$admin_path.'/site-setting/basic-setting';?>">Settings</a> </li>
            <!--<li> <a href=javascript:;> <span class="label bg-danger pull-right">34</span> <span>Notifications</span> </a> </li>-->
            <li> <a href="<?php echo $base_url.$admin_path.'/site-setting/change-password';?>">Change Password</a> </li>
            <li> <a href="<?php echo $base_url;?>" target="_blank">Front End</a> </li>
            <li> <a href="<?php echo $base_url.$admin_path;?>/login/log_out">Logout</a> </li>
          </ul>
        </li>
      			</ul>
	</header>
<div class="main-content" id="main_content_ajax">