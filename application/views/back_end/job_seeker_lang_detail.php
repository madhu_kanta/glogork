<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$lang_data = $this->common_model->get_count_data_manual('jobseeker_language',array('js_id'=>$id),2,'','',0,'',0);
}
if(isset($respones) && $respones !='')
{

	$respones_ss = json_decode($respones['data']);
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
if($this->session->flashdata('success_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-success alert-dismissable"><div class="fa fa-check"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('success_message').'</div>';
	}
	$this->session->unset_userdata('success_message');
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">Languages Known</div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('lang_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="table-responsive">
                	<?php
					if(isset($lang_data) && $lang_data !='' && count($lang_data) > 0)
					{
					?>
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Language</th>
                                <th>Read</th>
                                <th>Write</th>
                                <th>Speak</th>
                                <th>Proficiency</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
								foreach($lang_data as $lang_data_val)
								{
									$data_val = $lang_data_val;
							?>
                        	<tr>
                            	<td><?php if(isset($data_val['language']) && $data_val['language'] !=''){ echo $data_val['language'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['reading']) && $data_val['reading'] !=''){ echo $data_val['reading'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['writing']) && $data_val['writing'] !=''){ echo $data_val['writing'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['speaking']) && $data_val['speaking'] !=''){ echo $data_val['speaking'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['proficiency_level']) && $data_val['proficiency_level'] !=''){ echo $data_val['proficiency_level'];}else {echo $dna;}  ?></td>
                            </tr>
                            <?php
								}
							?>
                        </tbody>
                    </table>
                    <?php
					}
					else
					{
					?>
                    	<div class="alert alert-danger">No Detail Available</div>
                    <?php
					}
					?>
                </div>
                <?php
					}
					else
					{
						$lan_arr_disp_str = '';
						$lan_arr = array('relation'=>array('rel_table'=>'skill_language_master','key_val'=>'id','key_disp'=>'skill_language'));
						$lan_arr_disp = $this->common_model->getRelationDropdown($lan_arr);
						
						$skill_lan_arr = array('relation'=>array('rel_table'=>'skill_level_master','key_val'=>'skill_level','key_disp'=>'skill_level'));
						$skill_level_arr = $this->common_model->getRelationDropdown($skill_lan_arr);
						if(isset($lan_arr_disp) && $lan_arr_disp !='' && count($lan_arr_disp) > 0)
						{
							$lan_arr_disp_str = implode("','",$lan_arr_disp);
							$lan_arr_disp_str = "'".$lan_arr_disp_str."'";
						}
				?>
                	<form id="form_lang_detail" class="form-horizontal" role="form" action="<?php if(isset($this->common_model->base_url_admin) && $this->common_model->base_url_admin !=''){ echo $this->common_model->base_url_admin;} ?>job-seeker/save-detail/<?php echo $id; ?>">
                    	<div class="row pb10">
                        	<div class="col-sm-2 col-lg-2">
                            	Language
                            </div>
                            <div class="col-sm-2 col-lg-2">
                            	Proficiency
                            </div>
                            <div class="col-sm-1 col-lg-1 text-center">
                            	Read
                            </div>
                            <div class="col-sm-1 col-lg-1 text-center">
                            	Write
                            </div>
                            <div class="col-sm-1 col-lg-1 text-center">
                            	Speak
                            </div>
                            <div class="col-sm-1 col-lg-1 text-center">
                            
                            </div>
                        </div>
                      <?php
					  $ij = 0;
					  $disp_total = 1;
					  if(isset($lang_data) && $lang_data !='')
					  {
						  $disp_total = count($lang_data);
					  }
					  for($i=1;$i<=5;$i++)
					  {
						  $displ_none = 'display:none';
						  if($i <= $disp_total)
						  {
							  $displ_none = '';
							  $ij++;
						  }
						  $langu_id = '';
						  $langu_name = '';
						  $proficiency = '';
						  $reading = '';
						  $writing = '';
						  $speaking = '';
						  $temp_i = $i - 1;
						  if(isset($lang_data[$temp_i]) && $lang_data[$temp_i] !='' && count($lang_data[$temp_i]) > 0)
						  {
							  $langu_id = $lang_data[$temp_i]['id'];
							  $langu_name = $lang_data[$temp_i]['language'];
							  $proficiency = $lang_data[$temp_i]['proficiency_level'];
							  $reading = $lang_data[$temp_i]['reading'];
							  $writing = $lang_data[$temp_i]['writing'];
							  $speaking = $lang_data[$temp_i]['speaking'];
						  }
						  //$lang_data
					  ?>  
                        <div class="row pb10" id="lan_row_<?php echo $i; ?>"  style=" <?php echo $displ_none; ?>">
                          <div class="col-sm-2 col-lg-2">
                            <input type="hidden" name="langu_id_<?php echo $i; ?>" value="<?php echo $langu_id; ?>" />
                            <input type="text" name="langu_name_<?php echo $i; ?>" id="langu_name_<?php echo $i; ?>" class="form-control language" placeholder="Enter Language " value="<?php echo $langu_name; ?>" />
                          </div>
                          <div class="col-sm-2 col-lg-2">
                            <select class="form-control" name="proficiency_<?php echo $i; ?>">
                            	<option value="">Select Proficiency</option>
                            	<?php
								if(isset($skill_level_arr) && $skill_level_arr !='' && count($skill_level_arr) > 0)
								{
									foreach($skill_level_arr as $skill_level_arr_val)
									{
								?>
                            	<option <?php if($proficiency!='' && $proficiency == $skill_level_arr_val){ echo 'selected';} ?> value="<?php echo $skill_level_arr_val; ?>"><?php echo $skill_level_arr_val; ?></option>
                                <?php
									}
								}
								?>
                            </select>
                          </div>
                          <div class="col-sm-1 col-lg-1 text-center">
                          	<input type="checkbox" name="reading_<?php echo $i; ?>" <?php if(isset($reading) && $reading !='' && $reading =='Yes'){ echo 'checked'; }?> value="Yes" />
                          </div>
                          <div class="col-sm-1 col-lg-1 text-center">
                            <input type="checkbox" name="writing_<?php echo $i; ?>" <?php if(isset($writing) && $writing !='' && $writing =='Yes'){ echo 'checked'; }?> value="Yes" />
                          </div>
                          <div class="col-sm-1 col-lg-1 text-center">
                            <input type="checkbox" name="speaking_<?php echo $i; ?>" <?php if(isset($speaking) && $speaking !='' && $speaking =='Yes'){ echo 'checked'; }?> value="Yes" />
                          </div>
                          <?php
						  	if($langu_id !='')
					  		{
						  ?>
                          <div class="col-sm-1 col-lg-1 text-center">
                            <a href="javascript:;" onClick="delete_js_data('lang_detail','<?php echo $langu_id; ?>')" >Delete</a>
                          </div>
                          <?php } ?>
                        </div>
                      <?php
					  }
					  ?>
                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"  class="hash_tocken_id" />
                      <input type="hidden" id="lan_total_count" name="lan_total_count" value="<?php echo $ij; ?>" />
                      <div class="row clear-fix clearfix" id="lan_add_more">
                        <div class="col-sm-2 col-lg-2 text-left">
                        	<a href="javascript:;" href="javascript:;" onClick="return lan_show_hide()"><i class="fa fa-plus"></i>&nbsp;Add More</a>
                      	</div>
                      </div>
                        <div class="form-group">
							<label class="col-sm-2"></label>
							<div class="col-sm-9">
								<button type="submit" name="submit" value="submit" class="btn btn-primary mr10">Submit</button>
								<button type="button" onclick="view_detail_form('lang_detail','view')" class="btn btn-default">Back</button></div>
						</div>
                    </form>
                <?php						
					}
				?>
            </div>
        </div>
   
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_lang_detail").length > 0)
	{
		$("#form_lang_detail").validate({
			submitHandler: function(form)
			{
				var lang_arr = [];
				for(var i=1;i<=5;i++)
				{
					var langu_name = $("#langu_name_"+i).val();
					if(langu_name !='')
					{
						lang_arr.push(langu_name);
					}
				}
				var recipientsArray = lang_arr.sort();
				var flag_dup = false;
				for (var i = 0; i < recipientsArray.length - 1; i++)
				{
					if (recipientsArray[i + 1] == recipientsArray[i])
					{
						flag_dup = true;
						break;
					}
				}
				if(flag_dup == true)
				{
					alert("Please select unique language name");
					return false;
				}
				//alert(flag_dup);
				edit_profile('lang_detail','save');
				return false;
				//return true;
			}
		});
	}
	
 $(function(){
	var states = [<?php echo $lan_arr_disp_str; ?>];
	$('.language').typeahead({
	  hint: true,
	  highlight: true,
	  minLength: 1
	},
	{
	  name: 'states',
	  source: substringMatcher(states)
	});
});
</script>
<?php
}
?>