<?php $dna = $this->common_model->data_not_availabel;
$upload_path_profile = $this->common_front_model->fileuploadpaths('js_photos',1);
$base_url = base_url();
if($id !='')
{
	$page = 1;
	if(isset($_REQUEST['page']) && $_REQUEST['page'] !='')
	{
		$page = $_REQUEST['page'];
	}
	$limit = 5;
	$applied_job_count = $this->common_model->get_count_data_manual('job_application_history',array('job_application_history.job_id'=>$id),0,' id ','',0,'',0);
	$this->db->join('jobseeker js', 'job_application_history.js_id = js.id', 'inner');
	$applied_job_seeker_data = $this->common_model->get_count_data_manual('job_application_history',array('job_application_history.job_id'=>$id),2,' job_application_history.*, js.fullname, js.email, js.mobile, js.profile_pic ','is_shortlisted desc, applied_on desc ',$page,$limit,0);
	
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left  text-bold">Applied Job Seeker List (<?php echo $applied_job_count; ?>)</div>
        <div class="panel-controls">
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <div>
        	<?php
			if(isset($applied_job_count) && $applied_job_count > 0 && isset($applied_job_seeker_data) && count($applied_job_seeker_data)> 0)
			{
			?>	
			<div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Sr No.</th>
                                <th>Profile</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Applied On</th>
                                <th>Short Listed</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
							$i_st = 1;
							if($page != 1)
							{
								$i_st = (($page-1) * $limit)+1;
							}
							foreach($applied_job_seeker_data as $applied_job_seeker_data_val)
							{
						?>
                            <tr>
                            	<td><?php echo $i_st++; ?></td>
                                <td><?php  if($applied_job_seeker_data_val['profile_pic'] != '' && !is_null($applied_job_seeker_data_val['profile_pic']) && file_exists($upload_path_profile.'/'.$applied_job_seeker_data_val['profile_pic']))
				{  ?>
								<img height="150px" width="150px" src="<?php echo $base_url ;?>assets/js_photos/<?php echo $applied_job_seeker_data_val['profile_pic'] ;?>" alt="<?php echo $applied_job_seeker_data_val['fullname']?>" class="img-responsive" />
                                <?php }else { ?>
                                <img style="max-width:100px" src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $applied_job_seeker_data_val['fullname']?>" class="img-responsive" />
                                <?php } ?></td>
                                <td><a target="_blank" href="<?php echo $this->common_model->base_url_admin.'/job-seeker/seeker_detail/'.$applied_job_seeker_data_val['js_id']; ?>"><?php echo $applied_job_seeker_data_val['fullname']; ?></a></td>
                                <td><?php 
								if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
									echo $this->common_model->email_disp;
								}
								else
								{
									echo $applied_job_seeker_data_val['email']; 
								}
								?></td>
                                <td><?php 
								if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
								{
									echo $this->common_model->mobile_disp;
								}
								else
								{
									echo $applied_job_seeker_data_val['mobile'];
								} ?></td>
                                <td><?php echo $this->common_model->displayDate($applied_job_seeker_data_val['applied_on']); ?></td>
                                <td><?php 
									if(isset($applied_job_seeker_data_val['is_shortlisted']) && $applied_job_seeker_data_val['is_shortlisted'] =='Yes')
									{
										echo '<span class="label label-success">Yes</span>';
									}
									else
									{
										echo '<span class="label label-danger">No</span>';
									}
									?></td>
                            </tr>
                        <?php
							}
							$this->common_model->limit_per_page = $limit;
							$this->common_model->base_url_admin_cm_status = $this->common_model->base_url_admin.'/job/applied-job-seeker';
							$this->common_model->data_tabel_filtered_count = $applied_job_count;
						?>
                        <tr>
                        	<td colspan="7">
							<?php
                            	$this->common_model->rander_pagination();
								echo $this->common_model->pagination_link;
							?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
			}
			else
			{
			?>
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 alert alert-danger">
                	No data availabel
                </div>
            </div>
            <?php
			}
			?>
        </div>
    </div>
</div>