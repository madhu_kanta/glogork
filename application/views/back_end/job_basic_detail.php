<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_data['job_title']) && $id !='')
{
	$job_data = $this->common_model->get_count_data_manual('job_posting_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
if($this->session->userdata('success_message_js'))
{
	$respones_ss = new stdClass;
	$respones_ss->response = $this->session->userdata('success_message_js');
	$this->session->unset_userdata('success_message_js');
}
if($disp_mode =='edit')
{
?>
<!--<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.min.css" />
<script type="text/javascript" src="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.jquery.min.js" ></script>
--><?php
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left text-bold">
                <?php 
                    if(isset($job_data['job_title']) && $job_data['job_title'] !='')
                    {
                        echo $job_data['job_title'];
                    }
                    else
                    {
                        echo $dna;
                    } 
                ?>
            </div>
            <div class="panel-controls">
            	<?php
                    if($disp_mode =='view')
                    {
                    ?>
                    <a href="javascript:;" onClick="view_detail_form('basic_detail','edit')">Edit</a>
                    <?php
                    }
                ?>
                <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
             </div>
        </div>
        <div class="panel-body form-horizontal ">
        	<?php
				if(isset($respones_ss->response) && $respones_ss->response !='')
				{
					echo $respones_ss->response;
				}
                if($disp_mode =='view')
                {
					
           ?>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-6 text-bold text-center">
                            <?php
                                $avatar = 'assets/front_end/images/avatar-placeholder.png';
                                if(isset($job_data['profile_pic']) && $job_data['profile_pic'] !='')
                                {
                                    $temp_img = $job_data['profile_pic'];
                                    $path_img = "assets/emp_photos/";
                                    if(file_exists($path_img.$temp_img))
                                    {
                                        $avatar = $path_img.$temp_img;
                                    }
                                }
                            ?>
                            <img class="img-responsive" src="<?php echo $base_url.$avatar; ?>" />
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Company Name</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_data['company_name']) && $job_data['company_name'] !='')
                            {
                                echo $job_data['company_name'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Recruiter Name</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_data['posted_by_employee']) && $job_data['posted_by_employee'] !='')
                            {
                                echo $job_data['posted_by_employee'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Work Experience</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php
								echo $display_experience= $this->common_model->display_job_work_exp($job_data);
                            ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Company Hiring</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_data['company_hiring']) && $job_data['company_hiring'] !='')
                            { 
                                echo $job_data['company_hiring'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Functional Area</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['functional_name']) && $job_data['functional_name'] !=''){ echo $job_data['functional_name'];}else {echo $dna;} ?>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Status</label>
                        <label class="col-sm-8 col-xs-8 control-label-val text-bold">
                        	<strong>:</strong>&nbsp;
                        <?php 
                            if(isset($job_data['status']) && $job_data['status'] !='' && $job_data['status'] =='APPROVED')
                            {
                        ?>
                            <span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
                        <?php
                            }
                            else
                            {
                        ?>
                            <span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
                        <?php		
                            }
                        ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Job Post Role</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['job_role_name']) && $job_data['job_role_name'] !=''){ echo $job_data['job_role_name'];}else {echo $dna;} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Job Salary</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
							<?php
							if(isset($job_data['job_salary_from']) && $job_data['job_salary_from']!='')
							{
								if(is_numeric($job_data['job_salary_from']))
								{
									 echo $job_salary = $this->common_model->valueFromId('salary_range',$job_data['job_salary_from'],'salary_range');
								  }
								  else if($job_data['job_salary_from']!='')
								  {
									 echo $job_salary = $job_data['job_salary_from'];
								  }
							}else{
									echo $job_salary =  "N/A";
							}
								?>
                            <?php
								//echo $this->common_model->display_job_salary($job_data);
							?>
                        </label>
                    </div>                    
                    <!--<div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">No of Views</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php /*if(isset($job_data['no_of_views']) && $job_data['no_of_views'] !='')
							{ 
								echo $job_data['no_of_views'];
							}
							else 
							{
								echo $dna;
							}*/
							?>
                        </label>
                    </div>-->
                    <!--<div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">No of Apply</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php /*if(isset($job_data['no_of_apply']) && $job_data['no_of_apply'] !='')
                            { 
                                echo $job_data['no_of_apply'];
                            }
                            else 
                            {
                                echo $dna;
                            }*/ ?>
                        </label>
                    </div>-->
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Posted On</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
	                        <strong>:</strong>&nbsp;
                            <?php if(isset($job_data['posted_on']) && $job_data['posted_on'] !='')
                            { 
                                echo $this->common_model->displayDate($job_data['posted_on']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Expired On</label>
                        <label class="col-sm-8 col-xs-8 control-label-val text-danger">
	                        <strong>:</strong>&nbsp;
                            <?php if(isset($job_data['job_expired_on']) && $job_data['job_expired_on'] !='')
                            { 
                                echo $this->common_model->displayDate($job_data['job_expired_on']);
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Industries</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['industries_name']) && $job_data['industries_name'] !='')
                            { 
                                echo $job_data['industries_name'];
                            }
                            else 
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div> 
                </div>                    
            </div>
            <?php
				}
				else
				{
				$work_experience_from = $job_data['work_experience_from'];
				$work_experience_to = $job_data['work_experience_to'];
				$total_exp= '<div class="col-sm-5 col-lg-5 pl0">
			<select name="work_experience_from" id="work_experience_from" class="form-control">
				<option selected value="" >From</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  	$val = $explode[0].'.'.$m_val;
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
					$selected_ddr = '';
					if($work_experience_from == $val)
					{
						$selected_ddr = ' selected ';
					}
					$total_exp.= '<option '.$selected_ddr.' value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='</select>
			</div>
			<div class="col-sm-1 col-lg-1">To</div>
			<div class="col-sm-5 col-lg-5 pr0">
			<select name="work_experience_to" id="work_experience_to" class="form-control">
				<option selected value="" >To</option>';
				for($i=0;$i <= 25 ; $i=$i+0.5)
				{
					$explode = explode('.',$i);
				  	$m_val = '0';
				  	if(isset($explode[1]) && $explode[1]!='')
				  	{
				  		$m_val = $explode[1] + 1;
				  	}
				  	$val = $explode[0].'.'.$m_val;
					$disp_val = '';
					if($explode[0]=='0' && $m_val=='0')
					{
						$disp_val = "Fresher";
					} 
					else
					{ 
						$disp_val = $explode[0] . ' Year '. $m_val . ' Month ';
					}
						$selected_ddr = '';
						if($work_experience_to == $val)
						{
							$selected_ddr = ' selected ';
						}
					$total_exp.= '<option '.$selected_ddr.' value="'.$val.'" >'.$disp_val.'</option>';
				}
		$total_exp.='</select></div>';
		$where_curr = " ( is_deleted ='No' )";
		$currency_type = $job_data['currency_type'];
		$job_salary_from = $job_data['job_salary_from'];
		$job_salary_to = $job_data['job_salary_to'];
		
		$currnecy_list_arr = $this->common_model->get_count_data_manual('currency_master',$where_curr,2,'id,currency_name,currency_code','','','','');
		
		$salary_list_arr = $this->common_model->get_count_data_manual('salary_range',$where_curr,2,'*','','','','');
		$exp_anual_salary_str= '<div class="col-sm-12 col-lg-12 pl0">
			<select name="job_salary" id="job_salary" class="form-control" required >
				<option value="">Select Salary range</option>';
				foreach($salary_list_arr as $salary_list_val)
				{				
					$selected_ddr = '';
					if($job_salary_from == $salary_list_val['id'])
					{
						$selected_ddr = ' selected ';
					}
					
					$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$salary_list_val['id'].'>'.$salary_list_val['salary_range'].'</option>';
				}
			$exp_anual_salary_str.='</select>
			</div>';
		/*$exp_anual_salary_str= '<div class="col-sm-4 col-lg-4 pl0">
						<select name="currency_type" id="currency_type" class="form-control" required >
							<option value="">Select Currency</option>';
							foreach($currnecy_list_arr as $currnecy_list_val)
							{
								$selected_ddr = '';
								if($currency_type == $currnecy_list_val['currency_code'])
								{
									$selected_ddr = ' selected ';
								}
									$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$currnecy_list_val['currency_code'].'>'.$currnecy_list_val['currency_code'].'</option>';
							}
						$exp_anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 ">
							<select name="job_salary_from" id="job_salary_from" class="form-control" required>
							<option selected value="" >From</option>';
							for($ij=0;$ij<=99;$ij=$ij+0.5)
							{
								$selected_ddr = '';
								if($job_salary_from == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Lacs</option>';
							}
							$exp_anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 pr0">
							<select name="job_salary_to" id="job_salary_to" class="form-control" required>
							<option selected value="" >To</option>';
							for($ij=0;$ij<=99;$ij=$ij+0.5)
							{
								$selected_ddr = '';
								if($job_salary_to == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Lacs</option>';
							}
							$exp_anual_salary_str.='</select>
						</div>';*/
						$anual_salary = 2;
						if(isset($job_salary_from) && $job_salary_from!='')
						{
							if(is_numeric($job_salary_from))
							{
								$anual_salary = 1;
							}
							else if($job_salary_from!='')
							{
								 $anual_salary = 2;
							 }
						}
						/*$anual_salary = 2;
						if($job_salary_from !='' && $job_salary_to !='')
						{
							$anual_salary = 1;
						}*/
						$temp_arr = range(1,100);
						$temp_arr_tot = array_combine($temp_arr,$temp_arr);
						$ele_array = array(
						'job_title'=>array('is_required'=>'required'),
						'work_experience'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Work Experience</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$total_exp.'
						  </div>
						</div>'),
						'anual_salary'=>array('type'=>'dropdown','value_arr'=>array('1'=>'Define Salary','2'=>'As per company rules and regulations'),'value'=>$anual_salary,'onchange'=>'display_salary_ddr()','display_placeholder'=>'no','label'=>'Annual Salary'),
						'job_salary_text'=>array('is_required'=>'required','label'=>'Description for salary','form_group_class'=>'salary_1','value'=>$job_salary_from),
						'job_salary'=>array('type'=>'manual','code'=>'
						<div class="form-group salary_2">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Job Salary</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$exp_anual_salary_str.'
						  </div>
						</div>'),
						'company_hiring'=>array('is_required'=>'required'),
						'job_industry'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'value'=>$job_data['job_industry']),
						'functional_area'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'value'=>$job_data['functional_area'],'onchange'=>"dropdownChange('functional_area','job_post_role','role_master')"),
						'job_post_role'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'role_master','key_val'=>'id','key_disp'=>'role_name','rel_col_name'=>'functional_area','rel_col_val'=>$job_data['functional_area']),'value'=>$job_data['job_post_role']),
						
						'status'=>array('type'=>'radio')
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job/save-detail/'.$job_data['id'].'/basic_detail','form_id'=>'form_basic_detail','onback_click'=>"view_detail_form('basic_detail','view')");
					$this->common_model->set_table_name('job_posting');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
					//$this->common_model->generate_form();
				}
			?>
        </div>
    </div>
</form>
<?php
if($disp_mode !='view')
{
	/*$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','',1); 
	$key_skill_data = json_encode($key_skill);*/
?>
<script type="text/javascript">
	
	/*$(function(){
		var config = {
		'.chosen-select': {},
		'.chosen-select-deselect': { allow_single_deselect: true },
		'.chosen-select-no-single': { disable_search_threshold: 10 },
		'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
		'.chosen-select-width': { width: '100%' }			
		};
		$('#location_hiring').chosen({placeholder_text_multiple:'Select Location Hiring'});
		$('#job_education').chosen({placeholder_text_multiple:'Select Education Required'});
	});*/
	function display_salary_ddr()
	{
		var anual_salary = $("#anual_salary").val();
		if(anual_salary == 1)
		{
			$(".salary_2").show();
			$(".salary_1").hide();
		}
		else
		{
			$(".salary_1").show();
			$(".salary_2").hide();
		}
	}
	if($("#form_basic_detail").length > 0)
	{
		$("#form_basic_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('basic_detail','save');
				return false;
				//return true;
			}
		});
	}
	$(function()
	{
		display_salary_ddr();
		/*$('#skill_keyword').tokenfield({
			autocomplete: {
			source: <?php echo $key_skill_data; ?> ,
			delay: 100,
		  },
		  showAutocompleteOnFocus: true,
		});
		$('#skill_keyword').on('tokenfield:createtoken', function (event) {
			var existingTokens = $(this).tokenfield('getTokens');
			$.each(existingTokens, function(index, token) {
				if (token.value === event.attrs.value)
					event.preventDefault();
			});
		});*/
	});
	//$('.datepicker').datepicker({format:'Y-m-d'});
</script>
<?php
}
?>