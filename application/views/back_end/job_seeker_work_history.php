<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$work_hist_data = $this->common_model->get_count_data_manual('jobseeker_workhistory_view',array('js_id'=>$id),2,'','id asc',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
if($this->session->flashdata('success_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-success alert-dismissable"><div class="fa fa-check"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('success_message').'</div>';
	}
	$this->session->unset_userdata('success_message');
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left text-bold">Work History</div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('work_history','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="table-responsive">
                	<?php
					if(isset($work_hist_data) && $work_hist_data !='' && count($work_hist_data) > 0)
					{
					?>
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Company Name</th>
                                <th>Industry</th>
                                <th>Functional Area</th>
                                <th>Job Role</th>
                                <th>Annual Salary</th>
                                <th>Achievements</th>
                                <th>Joining Date</th>
                                <th>Leaving Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
								foreach($work_hist_data as $work_hist_data_val)
								{
									$data_val = $work_hist_data_val;
							?>
                        	<tr>
                            	<td><?php if(isset($data_val['company_name']) && $data_val['company_name'] !=''){ echo $data_val['company_name'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['industries_name']) && $data_val['industries_name'] !=''){ echo $data_val['industries_name'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['functional_name']) && $data_val['functional_name'] !=''){ echo $data_val['functional_name'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['role_name']) && $data_val['role_name'] !=''){ echo $data_val['role_name'];}else {echo $dna;}  ?></td>
                                <?php /*?><td><?php if(isset($data_val['annual_salary']) && $data_val['annual_salary'] !=''){ echo $data_val['currency_type'].' '.$this->common_model->display_salary($data_val['annual_salary']);}else {echo $dna;}  ?></td><?php */?>
								 <td><?php if(isset($data_val['salary_range']) && $data_val['salary_range'] !=''){ echo $data_val['salary_range'];}else {echo $dna;}  ?>
								 </td>
                                <td><?php if(isset($data_val['achievements']) && $data_val['achievements'] !=''){ echo $data_val['achievements'];}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['joining_date']) && $data_val['joining_date'] !='' && $data_val['joining_date'] !='0000-00-00')
								{
									echo $this->common_model->displayDate($data_val['joining_date']);
								}else {echo $dna;}  ?></td>
                                <td><?php if(isset($data_val['leaving_date']) && $data_val['leaving_date'] !='' && $data_val['leaving_date'] !='0000-00-00')
								{
									echo $this->common_model->displayDate($data_val['leaving_date']);
								}else if($data_val['joining_date'] !='0000-00-00') {echo 'Present';}else {echo $dna;}  ?></td>
                            </tr>
                            <?php
								}
							?>
                        </tbody>
                    </table>
                    <?php
					}
					else
					{
					?>
                    	<div class="alert alert-danger">No Detail Available</div>
                    <?php
					}
					?>
                </div>
                <?php
					}
					else
					{
						$salary_master_arr = array('relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range'));
						$salary_master_arr = $this->common_model->getRelationDropdown($salary_master_arr);
						
						$industries_master_arr = array('relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'));
						$industries_master_arr = $this->common_model->getRelationDropdown($industries_master_arr);
						
						
						$functional_arr = array('relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'));
						$functional_arr = $this->common_model->getRelationDropdown($functional_arr);
						
						/*$roll_master_arr = array('relation'=>array('rel_table'=>'role_master','key_val'=>'id','key_disp'=>'role_name'));
						$roll_master_arr = $this->common_model->getRelationDropdown($roll_master_arr);*/
						
						$currency_master_arr = array('relation'=>array('rel_table'=>'currency_master','key_val'=>'id','key_disp'=>'currency_code'));
						$currency_master_arr = $this->common_model->getRelationDropdown($currency_master_arr);

						$roll_master_arr = $this->common_model->getarrayrole();
						//$work_hist_data = $this->common_model->get_count_data_manual('jobseeker_workhistory_view',array('js_id'=>$id),2,'','id asc',0,'',0);
						
						/*echo '<pre>';
						print_r($roll_master_arr);
						echo '</pre>';*/
						
						/*
						if(isset($lan_arr_disp) && $lan_arr_disp !='' && count($lan_arr_disp) > 0)
						{
							$lan_arr_disp_str = implode("','",$lan_arr_disp);
							$lan_arr_disp_str = "'".$lan_arr_disp_str."'";
						}*/
						$i = 0;
				?>
                	<form id="form_work_history" class="form-horizontal" role="form" action="<?php if(isset($this->common_model->base_url_admin) && $this->common_model->base_url_admin !=''){ echo $this->common_model->base_url_admin;} ?>job-seeker/save-detail/<?php echo $id; ?>">
                    <?php
					  $ij_o = 0;
					  $disp_total = 1;
					  if(isset($work_hist_data) && $work_hist_data !='')
					  {
						  $disp_total = count($work_hist_data);
					  }
					  for($i=1;$i<=10;$i++)
					  {
						  $displ_none = 'display:none';
						  if($i <= $disp_total)
						  {
							  $displ_none = '';
							  $ij_o++;
						  }
						  $company_name = '';
						  $joining_date = '';
						  $leaving_date = '';
						  $industry = '';
						  $functional_area = '';
						  $job_role = '';
						  $annual_salary = '';
						  $currency_type = '';
						  $achievements = '';
						  $work_id = '';
						  $lac_anal_sal ='';
						  $thou_anal_sal = '';

						  $temp_i = $i - 1;
						  if(isset($work_hist_data[$temp_i]) && $work_hist_data[$temp_i] !='' && count($work_hist_data[$temp_i]) > 0)
						  {
							  $work_id = $work_hist_data[$temp_i]['id'];
							  $company_name = $work_hist_data[$temp_i]['company_name'];
							  $joining_date = $work_hist_data[$temp_i]['joining_date'];
							  $leaving_date = $work_hist_data[$temp_i]['leaving_date'];
							  $industry = $work_hist_data[$temp_i]['industry'];
							  $functional_area = $work_hist_data[$temp_i]['functional_area'];
							  $job_role = $work_hist_data[$temp_i]['job_role'];
							  $annual_salary = $work_hist_data[$temp_i]['annual_salary'];
							  $currency_type = $work_hist_data[$temp_i]['currency_type'];
							  $achievements = $work_hist_data[$temp_i]['achievements'];
							  
							  /*if($annual_salary !='')
							  {
								  	//$annual_salary = explode('-',$annual_salary);
								  	$annual_salary = explode('.',$annual_salary);
									if(isset($annual_salary[0]) && $annual_salary[0] !='' && $annual_salary[0] !=0)
									{
										$lac_anal_sal = $annual_salary[0];
									}
									if(isset($annual_salary[1]) && $annual_salary[1] !='' && $annual_salary[1] !=0)
									{
										$thou_anal_sal = $annual_salary[1];
									}
							  }*/
						  }
						  //$work_hist_data
					  ?>	
                        <div class="pb10" id="work_row_<?php echo $i; ?>"  style=" <?php echo $displ_none; ?>">
                        	<input type="hidden" name="work_id_<?php echo $i; ?>" value="<?php echo $work_id; ?>" />
                            <div class="row pb10">
                                <div class="col-sm-4 col-lg-4">
                                    Company Name
                                    <input type="text" name="company_name_<?php echo $i; ?>" id="company_name_<?php echo $i; ?>" class="form-control language" placeholder="Enter Company Name " value="<?php if(isset($company_name) && $company_name !=''){echo $company_name; }?>" />
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    Industry
                                    <select class="form-control" name="industry_<?php echo $i; ?>">
                                        <option value="">Select Industry</option>
                                        <?php
                                        if(isset($industries_master_arr) && $industries_master_arr !='' && count($industries_master_arr) > 0)
                                        {
                                            foreach($industries_master_arr as $key=>$val)
                                            {
                                        ?>
                                        <option <?php if($industry!='' && $industry == $key){ echo 'selected';} ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    Functional Area
                                    <select class="form-control" id="functional_area_<?php echo $i; ?>" name="functional_area_<?php echo $i; ?>" onchange="dropdownChange('functional_area_<?php echo $i; ?>','job_role_<?php echo $i; ?>','role_master')">
                                        <option value="">Select Functional Area</option>
                                        <?php
                                        if(isset($functional_arr) && $functional_arr !='' && count($functional_arr) > 0)
                                        {
                                            foreach($functional_arr as $key=>$val)
                                            {
                                        ?>
                                        <option <?php if($functional_area!='' && $functional_area == $key){ echo 'selected';} ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row pb10">
                                <div class="col-sm-4 col-lg-4">
                                    Joining Date
                                    <div class="input-prepend input-group input-group-lg"> <span class="add-on input-group-addon common_height_padd"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="joining_date_<?php echo $i; ?>" id="joining_date_<?php echo $i; ?>" class="form-control datepicker common_height_padd" placeholder="Select Joining Date" value="<?php if(isset($joining_date) && $joining_date !='' && $joining_date !='0000-00-00'){echo $joining_date; }?>" data-provide="datepicker" />
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    Leaving Date (Leave Date Blank If Currently Working)
                                    <div class="input-prepend input-group input-group-lg"> <span class="add-on input-group-addon common_height_padd"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="leaving_date_<?php echo $i; ?>" id="leaving_date_<?php echo $i; ?>" class="form-control datepicker common_height_padd" placeholder="Select Leaving Date" value="<?php if(isset($leaving_date) && $leaving_date !='' && $leaving_date !='0000-00-00'){echo $leaving_date; }?>" data-provide="datepicker" />
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    Current Role
                                    <select class="form-control" name="job_role_<?php echo $i; ?>" id="job_role_<?php echo $i; ?>">
                                        <option value="">Select Role</option>
                                        <?php
                                        if(isset($roll_master_arr) && $roll_master_arr !='' && count($roll_master_arr) > 0 && isset($functional_area) && $functional_area !='')
                                        {
											$roll_master_arr_temp = $roll_master_arr[$functional_area];
                                            foreach($roll_master_arr_temp as $roll_master_arr_temp_val)
                                            {
												$key=$roll_master_arr_temp_val['id'];
												$val=$roll_master_arr_temp_val['role_name'];
                                        ?>
                                        <option <?php if($job_role!='' && $job_role == $key){ echo 'selected';} ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row pb10">
                                <div class="col-sm-6 col-lg-6">
                                    Annual Salary
                                    <div class="row">
										<select class="form-control" name="annual_salary_<?php echo $i; ?>">
                                        <option value="">Select Annual Salary</option>
                                        <?php
                                        if(isset($salary_master_arr) && $salary_master_arr !='' && count($salary_master_arr) > 0)
                                        {
                                            foreach($salary_master_arr as $key=>$val)
                                            {
                                        ?>
										<option <?php if($annual_salary!='' && $annual_salary == $key){ echo 'selected';} ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>

                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    	<?php /*?><div class="col-sm-4 col-lg-4">
                                        	<select class="form-control" name="currency_type_<?php echo $i; ?>" id="currency_type_<?php echo $i; ?>">
                                            	<?php
												if(isset($currency_master_arr) && $currency_master_arr !='' && count($currency_master_arr) > 0)
												{
													foreach($currency_master_arr as $key=>$val)
													{
												?>
												<option <?php if($currency_type!='' && $currency_type == $val){ echo 'selected';} ?> value="<?php echo $val; ?>"><?php echo $val; ?></option>
												<?php
													}
												}
												?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4 col-lg-4">
                                        <select name="annual_sal_lacs_<?php echo $i; ?>" id="annual_sal_lacs_<?php echo $i; ?>" class="form-control">
											<option selected value="">Lacs</option>
											<?php
                                                for($ij=0;$ij<=99;$ij++)
                                                {
                                                    $selected_ddr = '';
                                                    if($lac_anal_sal == $ij)
                                                    {
                                                        $selected_ddr = ' selected ';
                                                    }
                                                ?>
                                                    <option <?php echo $selected_ddr;?> value="<?php echo $ij;?>"><?php echo $ij;?> Lacs</option>
                                                <?php
                                                }
                                            ?>
										</select>
                                        </div>
                                        <div class="col-sm-4 col-lg-4">
                                        	<select name="annual_sal_tho_<?php echo $i; ?>" id="annual_sal_tho_<?php echo $i; ?>" class="form-control">
                                            <option selected value="" >Thousand</option>
                                        <?php 
                                            for($ij=0;$ij<=99;$ij=$ij+5)
                                            {
                                                $selected_ddr = '';
                                                if($thou_anal_sal == $ij)
                                                {
                                                    $selected_ddr = ' selected ';
                                                }
                                          ?>
                                          <option <?php echo $selected_ddr;?> value="<?php echo $ij;?>"><?php echo $ij;?> Thousand</option>
                                         <?php
                                            }
										?>	
	                                        </select>
                                        </div><?php */?>
                                         <?php
											if(isset($work_id) && $work_id !='')
											{
										 ?>
										 	<div class="col-sm-12 col-lg-12 text-center">
                                            	<br/>
												<a class="text-bold" href="javascript:;" onClick="delete_js_data('work_history','<?php echo $work_id; ?>')" >Delete Work History</a>
										  	</div>
										 <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    Achievements
                                    <textarea rows="3" name="achievements_<?php echo $i; ?>" id="achievements_<?php echo $i; ?>" class="form-control" placeholder="Enter Achievements"><?php if(isset($achievements) && $achievements !=''){echo $achievements; }?></textarea>
                                </div>
                            </div>
                          <div class="clearfix"></div><hr/>
                        </div>
                      <?php
					  }
					  ?>
                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"  class="hash_tocken_id" />
                      <input type="hidden" id="work_total_count" name="work_total_count" value="<?php echo $ij_o; ?>" />
                      <div class="row clear-fix clearfix" id="work_add_more">
                        <div class="col-sm-2 col-lg-2 text-left">
                        	<a href="javascript:;" href="javascript:;" onClick="return work_show_hide()"><i class="fa fa-plus"></i>&nbsp;Add More</a>
                      	</div>
                      </div>
                        <div class="form-group">
							<label class="col-sm-2"></label>
							<div class="col-sm-9">
								<button type="submit" name="submit" value="submit" class="btn btn-primary mr10">Submit</button>
								<button type="button" onclick="view_detail_form('work_history','view')" class="btn btn-default">Back</button></div>
						</div>
                    </form>
                <?php
					}
				?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_work_history").length > 0)
	{
		$("#form_work_history").validate({
			submitHandler: function(form)
			{
				edit_profile('work_history','save');
				return false;
			}
		});
	}
	function work_show_hide()
	{
		var total_open = parseInt($("#work_total_count").val());
		//alert(total_open);
		if(total_open < 10)
		{
			total_open = total_open + 1;
			$("#work_row_"+total_open).show();
			$("#work_total_count").val(total_open);
		}
		if(total_open == 10)
		{
			$("#work_add_more").hide();
		}
	}
</script>
<?php
}
?>