<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',array('id'=>$id),1,' plan_status,plan_name ','',0,'',0);
	$plan_data = $this->common_model->get_count_data_manual('plan_jobseeker',array('js_id'=>$id,'is_deleted'=>'No','current_plan'=>'Yes'),1,'','',0,'',0);
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">Plan Details</div>
               	<div class="panel-controls">
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if($disp_mode =='view')
					{
				?>
                <div>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Plan Status</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['plan_status']) && $job_seeker_data['plan_status'] !='')
								{
									echo $job_seeker_data['plan_status'];
								}
								else 
								{
									echo $dna;
								}
								?>
                            </label>
                        	</div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                                <label class="col-sm-5 col-xs-5 control-label">Plan Name</label>
                                <label class="col-sm-7 col-xs-7 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
									if(isset($job_seeker_data['plan_name']) && $job_seeker_data['plan_name'] !='')
									{
										echo $job_seeker_data['plan_name'];
									}
									else 
									{
										echo $dna;
									}
									?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php
						if(isset($plan_data) && $plan_data !='' && count($plan_data) > 0)
						{
							//print_r($plan_data);
							$check_yes = '<span class="fa fa-check text-success"></span> (Yes)';
							$check_no = '<span class="fa fa-times text-danger"></span> (No)';
					?>
                    <hr/>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Plan Amount</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['plan_amount']) && $plan_data['plan_amount'] !='')
								{
									echo $plan_data['plan_currency'].' '.$plan_data['plan_amount'];
								}
								else 
								{
									echo $dna;
								}
								?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Discount</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['discount_amount']) && $plan_data['coupan_code'] !='' && $plan_data['discount_amount'] !='')
								{
									echo $plan_data['plan_currency'].' '.$plan_data['discount_amount']; 
									if(isset($plan_data['coupan_code']) && $plan_data['coupan_code'] !='')
									{
										echo ' ('.$plan_data['coupan_code'].')';
									}
								}
								else 
								{
									echo $dna;
								}
								?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Service Tax / GST</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo $plan_data['plan_currency'].' '.$plan_data['service_tax']; ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Total Amount</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo $plan_data['plan_currency'].' '.$plan_data['final_amount']; ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Payment Mode</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo $plan_data['payment_method']; ?>
                            </label>
                        	</div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Plan Duration</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo $plan_data['plan_duration'].' Days'; ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Plan Activated On</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo $this->common_model->displayDate($plan_data['activated_on'],'F j, Y'); ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Plan Expired On</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <span class="text-danger"><?php echo $this->common_front_model->displayDate($plan_data['expired_on'],'F j, Y'); ?></span>
                            </label>
                        	</div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Message (Remaining)</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo ($plan_data['message'] - $plan_data['message_used']).' out of '.$plan_data['message']; ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Contacts (Remaining)</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php echo ($plan_data['contacts'] - $plan_data['contacts_used']).' out of '.$plan_data['contacts']; ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Relevant Jobs</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['relevant_jobs']) && $plan_data['relevant_jobs'] =='Yes'){ echo $check_yes; }else { echo $check_no; } ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Job Notification</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['job_post_notification']) && $plan_data['job_post_notification'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				} ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Performance Report</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['performance_report']) && $plan_data['performance_report'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				} ?>
                            </label>
                        	</div>
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Highlight Application</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($plan_data['highlight_application']) && $plan_data['highlight_application'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				} ?>
                            </label>
                        	</div>
                        </div>
                    </div>
                    <?php
						}
					?>
                </div>
                <?php
					}
				?>
            </div>
        </div>