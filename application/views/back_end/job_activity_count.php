<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if($id !='')
{
	$viewed_job_count = $this->common_model->get_count_data_manual('jobseeker_viewed_jobs',array('job_id'=>$id),0,' id ','',0,'',0);
	$likeed_job_count = $this->common_model->get_count_data_manual('jobseeker_viewed_jobs',array('job_id'=>$id,'is_liked'=>'Yes'),0,' id ','',0,'',0);
	$saved_job_count = $this->common_model->get_count_data_manual('jobseeker_viewed_jobs',array('job_id'=>$id,'is_saved'=>'Yes'),0,' id ','',0,'',0);
	$applied_job_count = $this->common_model->get_count_data_manual('job_application_history',array('job_id'=>$id),0,' id ','',0,'',0);
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left  text-bold">Viewed Jobs</div>
        <div class="panel-controls">
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <?php
            if($disp_mode =='view')
            {
        ?>
        <div>                    
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Applied Job Seeker</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($applied_job_count) && $applied_job_count !='')
                        {
                            echo $applied_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Viewed Job</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($viewed_job_count) && $viewed_job_count !='')
                        {
                            echo $viewed_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    	<?php if(isset($applied_job_count) && $applied_job_count !='')
                        {
						?>	
                        <input type="button" class="btn btn-success" value="Click to View Applied Job Seeker List " onClick="show_appliedjob('1')" />
                        <?php } ?>
                        <input type="hidden" name="id_job" id="id_job" value="<?php echo $id; ?>" />
                        <input type="hidden" name="job_app_url" id="job_app_url" value="<?php echo $this->common_model->base_url_admin.'job/applied_job_seeker/';?>" />
                    </div>
                </div>                
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Saved Job </label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($saved_job_count) && $saved_job_count !='')
                        {
                            echo $saved_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Liked Job</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($likeed_job_count) && $likeed_job_count !='')
                        {
                            echo $likeed_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>