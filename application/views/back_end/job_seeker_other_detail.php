<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">Other Details</div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('other_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div>
                	<h4>Social Link and Website</h4>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">    
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-facebook"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
                                        if(isset($job_seeker_data['facebook_url']) && $job_seeker_data['facebook_url'] !='')
                                        {
                                    ?>
                                        <a target="_blank" href="<?php echo $job_seeker_data['facebook_url'];?>"><?php echo $job_seeker_data['facebook_url'];?></a>
                                    <?php
                                        }
                                        else
                                        {
                                            echo $dna;
                                        } 
                                    ?>
                                </label>
                       		</div>
                     	</div>
                     	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-google-plus"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
										if(isset($job_seeker_data['gplus_url']) && $job_seeker_data['gplus_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $job_seeker_data['gplus_url'];?>"><?php echo $job_seeker_data['gplus_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-twitter"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
	                                <strong>:</strong>&nbsp;
                                    <?php 
										if(isset($job_seeker_data['twitter_url']) && $job_seeker_data['twitter_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $job_seeker_data['twitter_url'];?>"><?php echo $job_seeker_data['twitter_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-linkedin"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
										if(isset($job_seeker_data['linkedin_url']) && $job_seeker_data['linkedin_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $job_seeker_data['linkedin_url'];?>"><?php echo $job_seeker_data['linkedin_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-globe"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
	                                <strong>:</strong>&nbsp;
                                    <?php 
										if(isset($job_seeker_data['website']) && $job_seeker_data['website'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $job_seeker_data['website'];?>"><?php echo $job_seeker_data['website'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                 	</div>
                    <hr/>
                    <h4>Other Detail</h4>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-5 col-xs-5 control-label">Resume Verification</label>
                            <label class="col-sm-7 col-xs-7 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php if(isset($job_seeker_data['resume_verification']) && $job_seeker_data['resume_verification'] !='')
								{
								if($job_seeker_data['resume_verification'] =='APPROVED')
									{
								?>
									<span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
								<?php
									}
									else
									{
								?>
									<span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
								<?php
									}
								}
								else 
								{
									echo $dna;
								} ?>
                            </label>
                        </div>
                            <div class="form-group mb0">
                                <label class="col-sm-5 col-xs-5 control-label">Profile Pic Approval</label>
                                <label class="col-sm-7 col-xs-7 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
									if(isset($job_seeker_data['profile_pic_approval']) && $job_seeker_data['profile_pic_approval'] !='')
									{
										if($job_seeker_data['profile_pic_approval'] =='APPROVED')
										{
									?>
                            			<span class="text-success"><i class="fa fa-thumbs-up"></i> APPROVED</span>
		                            <?php
										}
										else
										{
									?>
                            			<span class="text-danger"><i class="fa fa-thumbs-down"></i> UNAPPROVED</span>
		                            <?php
										}
									}
									else 
									{
										echo $dna;
									}
									?>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Email Verified</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
                                <?php 
									if(isset($job_seeker_data['email_verification']) && $job_seeker_data['email_verification'] !='')
									{
										echo $job_seeker_data['email_verification'];
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                            <div class="form-group mb0">
                                <label class="col-sm-4 col-xs-4 control-label">Profile Visibility</label>
                                <label class="col-sm-8 col-xs-8 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php if(isset($job_seeker_data['profile_visibility']) && $job_seeker_data['profile_visibility'] !=''){ echo $job_seeker_data['profile_visibility'];}else {echo $dna;} ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
					}
					else
					{
						$ele_array = array(
							'facebook_url'=>array('input_type'=>'url'),
							'gplus_url'=>array('input_type'=>'url'),
							'twitter_url'=>array('input_type'=>'url'),
							'linkedin_url'=>array('input_type'=>'url'),
							'website'=>array('input_type'=>'url'),
							'profile_pic_approval'=>array('type'=>'radio'),
							'resume_verification'=>array('type'=>'radio'),
							'email_verification'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
							'profile_visibility'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
						);
						$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job-seeker/save-detail/'.$job_seeker_data['id'].'/other_detail','form_id'=>'form_other_detail','onback_click'=>"view_detail_form('other_detail','view')");
						$this->common_model->set_table_name('jobseeker');
						echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
					}
				?>
            </div>
        </div>
   
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_other_detail").length > 0)
	{
		$("#form_other_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('other_detail','save');
				return false;
				//return true;
			}
		});
	}
</script>
<?php
}
?>