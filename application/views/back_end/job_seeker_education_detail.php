<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	
	$this->db->join("educational_qualification_master as eqm","je.qualification_level = eqm.id",'LEFT');
	$edu_data = $this->common_model->get_count_data_manual('jobseeker_education as je',array('je.js_id'=>$id),2,'je.*, eqm.educational_qualification ','',0,'',0);
	//print_r($edu_data);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
if($this->session->flashdata('success_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-success alert-dismissable"><div class="fa fa-check"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('success_message').'</div>';
	}
	$this->session->unset_userdata('success_message');
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left text-bold">Education Details</div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('education_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="table-responsive">
                	<?php
					if(isset($edu_data) && $edu_data !='' && count($edu_data) > 0)
					{
						$xxxi_str = '';
						$edu_str = '';
						$cert_str = '';
						foreach($edu_data as $edu_data_val)
						{
							$qualification_level =$dna;
							$passing_year =$dna;
							$institute =$dna;
							$specialization =$dna;
							$marks =$dna;
							if(isset($edu_data_val['educational_qualification']) && $edu_data_val['educational_qualification'] !='')
							{ 
								$qualification_level = $edu_data_val['educational_qualification'];
							}
							if(isset($edu_data_val['passing_year']) && $edu_data_val['passing_year'] !='')
							{ 
								$passing_year = $edu_data_val['passing_year'];
							}
							if(isset($edu_data_val['institute']) && $edu_data_val['institute'] !='')
							{ 
								$institute = $edu_data_val['institute'];
							}
							if(isset($edu_data_val['specialization']) && $edu_data_val['specialization'] !='')
							{
								$specialization = $edu_data_val['specialization'];
							}
							if(isset($edu_data_val['marks']) && $edu_data_val['marks'] !='')
							{
								$marks = $edu_data_val['marks'];
							}
							if($edu_data_val['is_certificate_X_XII'] =='Edu')
							{
								$edu_str =$edu_str."<tr>
								<td>".$qualification_level."</td>
								<td>".$passing_year."</td>
								<td>".$institute."</td>
								<td>".$specialization."</td>
								<td>".$marks."</td></tr>";
							}
							else if($edu_data_val['is_certificate_X_XII'] =='Certi')
							{
								$cert_str =$cert_str."<tr>
								<td>".$institute."</td>
								<td>".$specialization."</td>
								<td>".$passing_year."</td>
								</tr>";
							}
							else 
							{
								$xxxi_str =$xxxi_str."<tr>
								<td>".$edu_data_val['is_certificate_X_XII']."</td>
								<td>".$institute."</td>
								<td>".$passing_year."</td>
								<td>".$marks."</td></tr>";
							}
						}
						if($xxxi_str !='')
						{
					?>
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Standard</th>
                                <th>University/ Board</th>
                                <th>Year Of Passing</th>
                                <th>Marks/ Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								echo $xxxi_str;
							?>
                        </tbody>
                    </table>
                    <?php
						}
						if($cert_str !='')
						{
					?>
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Certificate Name</th>
                                <th>Certificate Description</th>
                                <th>Year Of Passing</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								echo $cert_str;
							?>
                        </tbody>
                    </table>
                    <?php
						}
						if($edu_str !='')
						{
					?>
                	<table class="table table-bordered bordered table-striped table-condensed datatable">
                    	<thead>
                        	<tr>
                            	<th>Degree/ Certificate</th>
                                <th>Year Of Passing</th>
                                <th>University/ Board</th>
                                <th>Specialization</th>
                                <th>Marks/ Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								echo $edu_str;
							?>
                        </tbody>
                    </table>
                    <?php
						}
					}
					else
					{
					?>
                    	<div class="alert alert-danger">No Detail Available</div>
                    <?php
					}
					?>
                </div>
                <?php
					}
					else
					{
						$edu_x = '';
						$edu_xx = '';
						$edu_certi_arr = array('');
						$edu_arr = array('');
						if(isset($edu_data) && $edu_data !='' && count($edu_data) > 0)
						{
							foreach($edu_data as $edu_data_val)
							{
							if($edu_data_val['is_certificate_X_XII'] =='Edu')
							{
								$edu_arr[] = $edu_data_val;
							}
							else if($edu_data_val['is_certificate_X_XII'] =='Certi')
							{
								$edu_certi_arr[] = $edu_data_val;
							}
							else if($edu_data_val['is_certificate_X_XII'] =='XII')
							{
								$edu_xx = $edu_data_val;
							}
							else if($edu_data_val['is_certificate_X_XII'] =='X')
							{
								$edu_x = $edu_data_val;
							}
						}
						}
						$curr_year = date('Y');
						$edu_list_ddr = $this->common_front_model->get_list('edu_list','','','arr','');
						if(isset($edu_list_ddr[0]))
						{
							unset($edu_list_ddr[0]);
						}
						$final_ecu_lista_arr = array();
						foreach($edu_list_ddr as $edu_list_ddr_val)
						{
							if(isset($edu_list_ddr_val['qualification_leval']))
							{
								$group_edu_name = $edu_list_ddr_val['qualification_leval'];
								$edu_name_arr_temp = $edu_list_ddr_val['edu_name'];
								$temp_arr_edu = array();
								foreach($edu_name_arr_temp as $edu_name_arr_temp_val)
								{
									$temp_arr_edu[$edu_name_arr_temp_val['id']] = $edu_name_arr_temp_val['val'];
								}
								$final_ecu_lista_arr[$group_edu_name] = $temp_arr_edu;
							}
						}
						$i = 0;
				?>
                	<form id="form_education_detail" class="form-horizontal" role="form" action="<?php if(isset($this->common_model->base_url_admin) && $this->common_model->base_url_admin !=''){ echo $this->common_model->base_url_admin;} ?>job-seeker/save-detail/<?php echo $id; ?>">
                    	<div class="pb10" id="edu_x" style="">
                        	<input type="hidden" name="edu_id[]" value="<?php if(isset($edu_x['id']) && $edu_x['id'] !=''){echo $edu_x['id'];} ?>" />
                            <input type="hidden" name="is_certificate_X_XII[]" value="X" />
                            <input type="hidden" name="qualification_level[]" value="0" />
                            <input type="hidden" name="specialization[]" value="" />
                            
                            <div class="row pb10">
                                <div class="col-sm-2 col-lg-2">
                                	<br/>
                                	Standard - X
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    University/ Board
                                    <input type="text" name="institute[]" id="institute_x" class="form-control language" placeholder="Enter University/ Board" value="<?php if(isset($edu_x['institute']) && $edu_x['institute'] !=''){echo $edu_x['institute']; }?>" />
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    Passing Year
                                    <select class="form-control" name="passing_year[]" id="passing_year_x">
                                        <option value="">Select Year</option>
                                        <?php
											$passing_year_x = '';
											if(isset($edu_x['passing_year']) && $edu_x['passing_year'] !='')
											{
												$passing_year_x = $edu_x['passing_year'];
											}
                                            for($year=1980;$year<=$curr_year;$year++)
                                            {
                                        ?>
                                        <option <?php if(isset($passing_year_x) && $passing_year_x == $year){ echo 'selected';} ?> value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    Marks/ Grade
                                    <?php
										$marks_x = '';
										if(isset($edu_x['marks']) && $edu_x['marks'] !='')
										{
											$marks_x = $edu_x['marks'];
										}
									?>
                                    <input type="text" name="marks[]" id="marks_x" class="form-control" placeholder="Enter Marks/ Grade" value="<?php if(isset($marks_x) && $marks_x !=''){echo $marks_x; }?>" />
                                </div>
                            </div>
                          <div class="clearfix"></div>
                          <hr/>
                        </div>
                        <div class="pb10" id="edu_xx" style="">
                        	<?php
								$edu_id_xii = '';
								$institute_xii='';
								$passing_year_xii='';
								if(isset($edu_xx['id']) && $edu_xx['id'] !='')
								{
									$edu_id_xii = $edu_xx['id'];
								}
								if(isset($edu_xx['institute']) && $edu_xx['institute'] !='')
								{
									$institute_xii = $edu_xx['institute'];
								}
								if(isset($edu_xx['institute']) && $edu_xx['institute'] !='')
								{
									$institute_xii = $edu_xx['institute'];
								}
								if(isset($edu_xx['passing_year']) && $edu_xx['passing_year'] !='')
								{
									$passing_year_xii = $edu_xx['passing_year'];
								}
								$marks_xii='';
								if(isset($edu_xx['marks']) && $edu_xx['marks'] !='')
								{
									$marks_xii = $edu_xx['marks'];
								}
							?>
                        	<input type="hidden" name="edu_id[]" value="<?php if(isset($edu_id_xii)){echo $edu_id_xii;} ?>" />
                            <input type="hidden" name="is_certificate_X_XII[]" value="XII" />
                            <input type="hidden" name="qualification_level[]" value="0" />
                            <input type="hidden" name="specialization[]" value="" />
                            <div class="row pb10">
                                <div class="col-sm-2 col-lg-2">
                                	<br/>
                                	Standard - XII
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    University/ Board
                                    <input type="text" name="institute[]" id="institute_xii" class="form-control language" placeholder="Enter University/ Board" value="<?php if(isset($institute_xii) && $institute_xii !=''){echo $institute_xii; }?>" />
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    Passing Year
                                    <select class="form-control" name="passing_year[]" id="passing_year_xii">
                                        <option value="">Select Year</option>
                                        <?php
                                            for($year=1980;$year<=$curr_year;$year++)
                                            {
                                        ?>
                                        <option <?php if(isset($passing_year_xii) && $passing_year_xii == $year){ echo 'selected';} ?> value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 col-lg-3">
                                    Marks/ Grade
                                    <input type="text" name="marks[]" id="marks_xii" class="form-control" placeholder="Enter Marks/ Grade" value="<?php if(isset($marks_xii) && $marks_xii !=''){echo $marks_xii; }?>" />
                                </div>
                            </div>
                        	<div class="clearfix"></div><hr/>
                        </div>
                         <hr/>
                        <div class="row clear-fix clearfix">
                        	<div class="col-sm-12 col-lg-12 text-left">
                            	<h3>Add Education</h3>
                      		</div>
                      	</div>
                        <!-- for education group wise-->
                     <?php
					  //$ij_o = 0;
                      $ij_o = 1;
					  $disp_total = 1;
					  if(isset($edu_arr) && $edu_arr !='')
					  {
						  $disp_total = (count($edu_arr) - 1);
					  }
					  for($i=1;$i<=10;$i++)
					  {
						  $edu_id = '';
						  $institute = '';
						  $specialization = '';
						  $passing_year = '';
						  $qualification_level = '';
						  $marks = '';
						  $displ_none = 'display:none';
						  if($i <= $disp_total)
						  {
							  $displ_none = '';
							  $ij_o++;
							  
							  if(isset($edu_arr[$i]['id']) && $edu_arr[$i]['id'] !='')
							  {
								$edu_id = $edu_arr[$i]['id'];
							  }
							  if(isset($edu_arr[$i]['institute']) && $edu_arr[$i]['institute'] !='')
							  {
								$institute = $edu_arr[$i]['institute'];
							  }
							  if(isset($edu_arr[$i]['specialization']) && $edu_arr[$i]['specialization'] !='')
							  {
								$specialization = $edu_arr[$i]['specialization'];
							  }
							  if(isset($edu_arr[$i]['passing_year']) && $edu_arr[$i]['passing_year'] !='')
							  {
								$passing_year = $edu_arr[$i]['passing_year'];
							  }
							  if(isset($edu_arr[$i]['qualification_level']) && $edu_arr[$i]['qualification_level'] !='')
							  {
								$qualification_level = $edu_arr[$i]['qualification_level'];
							  }
							  if(isset($edu_arr[$i]['marks']) && $edu_arr[$i]['marks'] !='')
							  {
								$marks = $edu_arr[$i]['marks'];
							  }
							  
							  
							  //
						  }
                      ?>    
                        <div class="pb10" id="edu_group_<?php echo $i; ?>" style=" <?php echo $displ_none; ?>">
                        	<input type="hidden" name="edu_id[]" value="<?php if(isset($edu_id)){echo $edu_id;} ?>" />
                            <input type="hidden" name="is_certificate_X_XII[]" value="Edu" />
                            <div class="row pb10">
                                <div class="col-sm-4 col-lg-4 pb10">
                                    Institute Name
                                    <input type="text" name="institute[]" id="institute_<?php echo $i; ?>" class="form-control" placeholder="Enter Institute Name" value="<?php if(isset($institute) && $institute !=''){echo $institute; }?>" />
                                </div>
                                <div class="col-sm-4 col-lg-4 pb10">
                                    Qualification Level
                                    <select class="form-control" name="qualification_level[]" id="qualification_level_<?php echo $i; ?>">
                                    	<option value="">Select Qualification Level</option>
                                        <?php
											if(isset($edu_list_ddr) && $edu_list_ddr !='')
											{
												foreach($final_ecu_lista_arr as $ecu_key=>$ecu_val)
												{
													if(isset($ecu_val) && $ecu_val !='' && count($ecu_val) > 0 && $ecu_key !='')
													{
											?>
                                            	<optgroup label="<?php echo $ecu_key; ?>">
                                                	<?php 
														foreach($ecu_val as $key_final =>$val_final)
														{
													?>
                                                    <option <?php if(isset($qualification_level) && $qualification_level !='' && $qualification_level == $key_final){ echo 'selected';} ?> value="<?php echo $key_final; ?>"><?php echo $val_final; ?></option>
                                                    <?php
														}
													?>
                                                </optgroup>
                                            <?php
													}
												}
                                            	echo $edu_list_ddr;
											}
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-lg-4 pb10">
                                   	Specialization
                                    <input type="text" name="specialization[]" id="specialization_<?php echo $i; ?>" class="form-control language" placeholder="Enter Specialization" value="<?php if(isset($specialization) && $specialization !=''){echo $specialization; }?>" />
                                </div>
                                <div class="col-sm-4 col-lg-4 pb10">
                                    Percentage / Grade:
                                    <input type="text" name="marks[]" id="marks_<?php echo $i; ?>" class="form-control" placeholder="Enter Percentage / Grade" value="<?php if(isset($marks) && $marks !=''){echo $marks; }?>" />
                                </div>
                                <div class="col-sm-4 col-lg-4 pb10">
                                    Passing Year
                                    <select class="form-control" name="passing_year[]" id="passing_year_<?php echo $i; ?>">
                                        <option value="">Select Year</option>
                                        <?php
                                            for($year=1980;$year<=$curr_year;$year++)
                                            {
                                        ?>
                                        <option <?php if(isset($passing_year) && $passing_year == $year){ echo 'selected';} ?> value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
							if(isset($edu_id) && $edu_id !='')
							{
						 ?>
							<div class="col-sm-12 col-lg-12 text-center pb10">
								<a class="text-bold" href="javascript:;" onClick="delete_js_data('education_detail','<?php echo $edu_id; ?>')" >Delete Education</a>
							</div>
						 <?php } 
					 ?>
                        	<div class="clearfix"></div><hr/>
                        </div>
                      
                     
                     <?php
					  }
					  ?>
                        <!-- for education group wise-->
                      	<div class="row clear-fix clearfix" id="education_add_more">
                        	<div class="col-sm-12 col-lg-12 text-left">
                            	<input type="hidden" id="edu_total_count" name="edu_total_count" value="<?php echo $ij_o; ?>" />
                        		<a href="javascript:;" onClick="return education_add_hide()"><i class="fa fa-plus"></i>&nbsp;Add More Education</a>
                      		</div>
                      	</div>
                        <hr/>
                        <div class="row clear-fix clearfix">
                        	<div class="col-sm-12 col-lg-12 text-left">
                            	<h3>Add Certificate</h3>
                      		</div>
                      	</div>
                     <!-- for adding more certificate-->
                      <?php
					  $ij_o = 0;
					  $disp_total = 1;
					  if(isset($edu_certi_arr) && $edu_certi_arr !='')
					  {
						  $disp_total = (count($edu_certi_arr) - 1);
					  }

					  for($i=1;$i<=10;$i++)
					  {
						  $edu_id = '';
						  $institute = '';
						  $specialization = '';
						  $passing_year = '';
						  $displ_none = 'display:none';
						  if($i <= $disp_total)
						  {
							  $displ_none = '';
							  $ij_o++;

							  if(isset($edu_certi_arr[$i]['id']) && $edu_certi_arr[$i]['id'] !='')
							  {
								$edu_id = $edu_certi_arr[$i]['id'];
							  }
							  if(isset($edu_certi_arr[$i]['institute']) && $edu_certi_arr[$i]['institute'] !='')
							  {
								$institute = $edu_certi_arr[$i]['institute'];
							  }
							  if(isset($edu_certi_arr[$i]['specialization']) && $edu_certi_arr[$i]['specialization'] !='')
							  {
								$specialization = $edu_certi_arr[$i]['specialization'];
							  }
							  if(isset($edu_certi_arr[$i]['passing_year']) && $edu_certi_arr[$i]['passing_year'] !='')
							  {
								$passing_year = $edu_certi_arr[$i]['passing_year'];
							  }
						  }
						  //
                      ?>    
                        <div class="pb10" id="certi_group_<?php echo $i; ?>" style=" <?php echo $displ_none; ?>">
                        	<input type="hidden" name="edu_id[]" value="<?php if(isset($edu_id)){echo $edu_id;} ?>" />
                            <input type="hidden" name="is_certificate_X_XII[]" value="Certi" />
                            <input type="hidden" name="qualification_level[]" value="0" />
                            <input type="hidden" name="marks[]" value="" />
                            <div class="row pb10">
                                <div class="col-sm-4 col-lg-4">
                                    Certificate name
                                    <input type="text" name="institute[]" id="institute_<?php echo $i; ?>" class="form-control" placeholder="Enter Certificate name" value="<?php if(isset($institute) && $institute !=''){echo $institute; }?>" />
                                </div>                                
                                <div class="col-sm-4 col-lg-4">
                                   	Certificate Description
                                    <input type="text" name="specialization[]" id="specialization_<?php echo $i; ?>" class="form-control" placeholder="Enter Certificate Description" value="<?php if(isset($specialization) && $specialization !=''){echo $specialization; }?>" />
                                </div>
                                <div class="col-sm-4 col-lg-4">
                                    Passing Year
                                    <select class="form-control" name="passing_year[]" id="passing_year_<?php echo $i; ?>">
                                        <option value="">Select Year</option>
                                        <?php
                                            for($year=1980;$year<=$curr_year;$year++)
                                            {
                                        ?>
                                        <option <?php if(isset($passing_year) && $passing_year == $year){ echo 'selected';} ?> value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        	<div class="clearfix"></div><hr/>
                        </div>
                      <?php
							if(isset($edu_id) && $edu_id !='')
							{
						 ?>
							<div class="col-sm-12 col-lg-12 text-center pb10">
								
								<a class="text-bold" href="javascript:;" onClick="delete_js_data('education_detail','<?php echo $edu_id; ?>')" >Delete Education</a>
							</div>
						 <?php } 
					  }
					  ?>  
                        <div class="row clear-fix clearfix" id="certi_add_more">
                        	<div class="col-sm-12 col-lg-12 text-left">
                            	<input type="hidden" id="certi_total_count" name="certi_total_count" value="<?php echo $ij_o; ?>" />
                        		<a href="javascript:;" onClick="return certificate_add_hide()"><i class="fa fa-plus"></i>&nbsp;Add Certificate</a>
                      		</div>
                      	</div>
                       	<div class="form-group">
							<label class="col-sm-2"></label>
							<div class="col-sm-9">
								<button type="submit" name="submit" value="submit" class="btn btn-primary mr10">Submit</button>
								<button type="button" onclick="view_detail_form('education_detail','view')" class="btn btn-default">Back</button>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"  class="hash_tocken_id" />
                            </div>
						</div>
                    </form>
                <?php
					}
				?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_education_detail").length > 0)
	{
		$("#form_education_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('education_detail','save');
				return false;
			}
		});
	}
	function education_add_hide()
	{
		var total_open = parseInt($("#edu_total_count").val());
		//alert(total_open);
		if(total_open < 10)
		{
			total_open = total_open + 1;
			$("#edu_group_"+total_open).show();
			$("#edu_total_count").val(total_open);
		}
		if(total_open == 10)
		{
			$("#education_add_more").hide();
		}
	}
	function certificate_add_hide()
	{
		var total_open = parseInt($("#certi_total_count").val());
		//alert(total_open);
		if(total_open < 10)
		{
			total_open = total_open + 1;
			$("#certi_group_"+total_open).show();
			$("#certi_total_count").val(total_open);
		}
		if(total_open == 10)
		{
			$("#certi_add_more").hide();
		}
	}
</script>
<?php
}
?>