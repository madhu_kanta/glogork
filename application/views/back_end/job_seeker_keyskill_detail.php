<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_seeker_data['profile_summary']) && $id !='')
{
	$job_seeker_data = $this->common_model->get_count_data_manual('jobseeker_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
if($disp_mode =='edit')
{
?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.min.css" />
<script type="text/javascript" src="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.jquery.min.js" ></script>
<?php
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary form-horizontal">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">
                <?php
                	if(isset($job_seeker_data['industries_name']) && $job_seeker_data['industries_name'] !='')
					{
						echo $job_seeker_data['industries_name'].' - Skills';
					}
					else
					{
						echo 'Skill Detail';
					} 
				?></div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('keyskill_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Resume Headline</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['resume_headline']) && $job_seeker_data['resume_headline'] !=''){ echo $job_seeker_data['resume_headline'];}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Key Skill</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['key_skill']) && $job_seeker_data['key_skill'] !=''){ echo $job_seeker_data['key_skill'];}else {echo $dna;} ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Functional Area</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['functional_name']) && $job_seeker_data['functional_name'] !=''){ echo $job_seeker_data['functional_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Job Role</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['role_name']) && $job_seeker_data['role_name'] !=''){ echo $job_seeker_data['role_name'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Total Experience</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php 
								echo $this->common_model->display_experience($job_seeker_data['total_experience']);
								/*if(isset($job_seeker_data['total_experience']) && $job_seeker_data['total_experience'] !=''){ echo $job_seeker_data['total_experience'];}else {echo $dna;}*/
								?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Annual Salary</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['annual_salary_name']) && $job_seeker_data['annual_salary_name'] !=''){ echo $job_seeker_data['annual_salary_name'];}else {echo $dna;} ?>
								<?php 
								/*if(isset($job_seeker_data['currency_type']) && $job_seeker_data['currency_type'] !='')
								{
									echo $job_seeker_data['currency_type'].' ';
								}
								echo $this->common_model->display_salary($job_seeker_data['annual_salary']);*/
								/*if(isset($job_seeker_data['annual_salary']) && $job_seeker_data['annual_salary'] !=''){ echo $job_seeker_data['annual_salary'];}else {echo $dna;}*/
								?>
                            </label>
                        </div>
                     </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0 text-left">
                            <label class="col-sm-4 col-xs-4 control-label">Preferred Detail</label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Job Type</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['desire_job_type']) && $job_seeker_data['desire_job_type'] !=''){ echo $job_seeker_data['desire_job_type'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Employment Type</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['employment_type']) && $job_seeker_data['employment_type'] !=''){ echo $job_seeker_data['employment_type'];}else {echo $dna;} ?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Shift</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['prefered_shift']) && $job_seeker_data['prefered_shift'] !=''){ echo $job_seeker_data['prefered_shift'];}else {echo $dna;} ?>
                            </label>
                        </div>
             		</div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Preferred City</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php
									if(isset($job_seeker_data['preferred_city']) && $job_seeker_data['preferred_city'] !='')
									{
										echo $this->common_model->valueFromId('city_master',$job_seeker_data['preferred_city'],'city_name');
									}
									else
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                            <label class="col-sm-4 col-xs-4 control-label">Expected Salary</label>
                            <label class="col-sm-8 col-xs-8 control-label-val">
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_seeker_data['ex_annual_salary_name']) && $job_seeker_data['ex_annual_salary_name'] !=''){ echo $job_seeker_data['ex_annual_salary_name'];}else {echo $dna;} ?>
								<?php 								/*if(isset($job_seeker_data['exp_salary_currency_type']) && $job_seeker_data['exp_salary_currency_type'] !='')
								{
									echo $job_seeker_data['exp_salary_currency_type'].' ';
								}
								echo $this->common_model->display_salary($job_seeker_data['expected_annual_salary']);*/
								/*if(isset($job_seeker_data['expected_annual_salary']) && $job_seeker_data['expected_annual_salary'] !=''){ echo $job_seeker_data['expected_annual_salary'];}else {echo $dna;}*/ 
								?>
                            </label>
                        </div>
                    </div>
                </div>
                <?php
                }
                else
                {
					/*$preferred_city_str  ='';
					if(isset($job_seeker_data['preferred_city']) && $job_seeker_data['preferred_city'] !='')
					{
						$preferred_city = $job_seeker_data['preferred_city'];
						$where_city_arr = " id in ( $preferred_city ) ";
						$data_city = $this->common_model->get_count_data_manual('city_master',$where_city_arr,2,'city_name','',0,'',0);
						if(isset($data_city) && $data_city !='' && count($data_city) >0)
						{
							$preferred_city_str = implode(", ",array_map(function($a) {return implode("~",$a);},$data_city));
						}
					}*/
					$total_experience_cu = $job_seeker_data['total_experience'];
					$total_year = '';
					$total_month = '';
					if($total_experience_cu !='')
					{
						//$exp_arr = explode('-',$total_experience_cu);
						$exp_arr = explode('.',$total_experience_cu);
						if(isset($exp_arr[0]))
						{
							$total_year = $exp_arr[0];
						}
						if(isset($exp_arr[1]))
						{
							$total_month = $exp_arr[1];
						}
					}

					/*$annual_salary_cu = $job_seeker_data['annual_salary'];
					$sal_lacs = '';
					$sal_thou = '';
					if($annual_salary_cu !='')
					{
						//$exp_arr = explode('-',$annual_salary_cu);
						$exp_arr = explode('.',$annual_salary_cu);
						if(isset($exp_arr[0]))
						{
							$sal_lacs = $exp_arr[0];
						}
						if(isset($exp_arr[1]))
						{
							$sal_thou = $exp_arr[1];
						}
					}
					
					$exp_annual_salary_cu = $job_seeker_data['expected_annual_salary'];
					$exp_sal_lacs = '';
					$exp_sal_thou = '';
					if($exp_annual_salary_cu !='')
					{
						//$exp_arr = explode('-',$exp_annual_salary_cu);
						$exp_arr = explode('.',$exp_annual_salary_cu);
						if(isset($exp_arr[0]))
						{
							$exp_sal_lacs = $exp_arr[0];
						}
						if(isset($exp_arr[1]))
						{
							$exp_sal_thou = $exp_arr[1];
						}
					}*/
					
					
					$total_exp= '<div class="col-sm-6 col-lg-6 pl0">
						<select name="total_exp_year" id="total_exp_year" class="form-control">
							<option selected value="" >Select Year</option>';
					for($ij=0;$ij<=25;$ij++)
					{
						$selected_ddr = '';
						if($total_year == $ij)
						{
							$selected_ddr = ' selected ';
						}
						$total_exp.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Year</option>';
					}
					$total_exp.='</select>
						</div>
						<div class="col-sm-6 col-lg-6 pr0">
						<select name="total_exp_month" id="total_exp_month" class="form-control">
							<option selected value="" >Select Month</option>';
					for($ij=0;$ij<12;$ij++)
					{
						$selected_ddr = '';
						if($total_month == $ij)
						{
							$selected_ddr = ' selected ';
						}
						$total_exp.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Month</option>';
					}
					$total_exp.='
					</select></div>';
					/*$currency_type = $job_seeker_data['currency_type'];
					$exp_currency_type = $job_seeker_data['exp_salary_currency_type'];
					if($currency_type !='' && $exp_currency_type !='')
					{
						$where_curr = " ( is_deleted ='No' or (currency_code = '$currency_type' or currency_code = '$exp_currency_type' ))";
					}
					else if($currency_type !='')
					{
						$where_curr = " ( is_deleted ='No' or currency_code = '$currency_type' )";
					}
					else if($exp_currency_type !='')
					{
						$where_curr = " ( is_deleted ='No' or currency_code = '$exp_currency_type' )"; 
					}
					else
					{
						$where_curr = " ( is_deleted ='No' )"; 
					}
					$currnecy_list_arr = $this->common_model->get_count_data_manual('currency_master',$where_curr,2,'id,currency_name,currency_code','','','','');
					$anual_salary_str= '<div class="col-sm-4 col-lg-4 pl0">
						<select name="currency_type" class="form-control" >
						<option value="">Select Currency</option>';
						foreach($currnecy_list_arr as $currnecy_list_val)
						{
							$selected_ddr = '';
							if($currency_type == $currnecy_list_val['currency_code'])
							{
								$selected_ddr = ' selected ';
							}
								$anual_salary_str.= '<option '.$selected_ddr.' value='.$currnecy_list_val['currency_code'].'>'.$currnecy_list_val['currency_code'].'</option>';
						}
					$anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 ">
							<select name="annual_sal_lacs" id="annual_sal_lacs" class="form-control">
							<option selected value="" >Lacs</option>';
							for($ij=0;$ij<=99;$ij++)
							{
								$selected_ddr = '';
								if($sal_lacs == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Lacs</option>';
							}
							$anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 pr0">
							<select name="annual_sal_tho" id="annual_sal_tho" class="form-control">
							<option selected value="" >Thousand</option>';
							for($ij=0;$ij<=99;$ij=$ij+5)
							{
								$selected_ddr = '';
								if($sal_thou == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Thousand</option>';
							}
							$anual_salary_str.='</select>
						</div>';
					
					$exp_currency_type = $job_seeker_data['exp_salary_currency_type'];
					if($exp_currency_type !='')
					{
						$where_curr = " ( is_deleted ='No' or id = $exp_currency_type )"; 
					}
					else
					{
						$where_curr = " ( is_deleted ='No' )"; 
					}
					
					$exp_anual_salary_str= '<div class="col-sm-4 col-lg-4 pl0">
						<select name="exp_salary_currency_type" id="exp_salary_currency_type" class="form-control" >
							<option value="">Select Currency</option>';
							foreach($currnecy_list_arr as $currnecy_list_val)
							{
								$selected_ddr = '';
								if($exp_currency_type == $currnecy_list_val['currency_code'])
								{
									$selected_ddr = ' selected ';
								}
									$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$currnecy_list_val['currency_code'].'>'.$currnecy_list_val['currency_code'].'</option>';
							}
						$exp_anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 ">
							<select name="exp_annual_sal_lacs" id="exp_annual_sal_lacs" class="form-control">
							<option selected value="" >Lacs</option>';
							for($ij=0;$ij<=99;$ij++)
							{
								$selected_ddr = '';
								if($exp_sal_lacs == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Lacs</option>';
							}
							$exp_anual_salary_str.='</select>
						</div>
						<div class="col-sm-4 col-lg-4 pr0">
							<select name="exp_annual_sal_tho" id="exp_annual_sal_tho" class="form-control">
							<option selected value="" >Thousand</option>';
							for($ij=0;$ij<=99;$ij=$ij+5)
							{
								$selected_ddr = '';
								if($exp_sal_thou == $ij)
								{
									$selected_ddr = ' selected ';
								}
								$exp_anual_salary_str.= '<option '.$selected_ddr.' value='.$ij.'>'.$ij.' Thousand</option>';
							}
							$exp_anual_salary_str.='</select>
						</div>';*/
							
					$ele_array = array(
						'resume_headline'=>array('type'=>'textarea'),
						'key_skill'=>array(),
						'industry'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'value'=>$job_seeker_data['industry']),
						'functional_area'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'value'=>$job_seeker_data['functional_area'],'onchange'=>"dropdownChange('functional_area','job_role','role_master')"),
						'job_role'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'role_master','key_val'=>'id','key_disp'=>'role_name','rel_col_name'=>'functional_area','rel_col_val'=>$job_seeker_data['functional_area']),'value'=>$job_seeker_data['job_role']),
						'total_experience'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Total Experience</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$total_exp.'
						  <input type="hidden" name="total_experience" id="total_experience" value="'.$job_seeker_data['total_experience'].'" />
						  </div>
						</div>'),
						'annual_salary'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range'),'value'=>$job_seeker_data['annual_salary']),
						'expected_annual_salary'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'salary_range','key_val'=>'id','key_disp'=>'salary_range'),'value'=>$job_seeker_data['expected_annual_salary']),
						/*'annual_salary'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Current Annual Salary</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$anual_salary_str.'
						  <input type="hidden" name="annual_salary" id="annual_salary" value="'.$job_seeker_data['annual_salary'].'" />
						  </div>
						</div>'),
						'expected_annual_salary'=>array('type'=>'manual','code'=>'
						<div class="form-group">
						  <label class="col-sm-'.$this->common_model->label_col.' col-lg-'.$this->common_model->label_col.' control-label">Expected Annual Salary</label>
						  <div class="col-sm-9 col-lg-'.$this->common_model->form_control_col.'">
						  '.$exp_anual_salary_str.'
						  <input type="hidden" name="expected_annual_salary" id="expected_annual_salary" value="'.$job_seeker_data['expected_annual_salary'].'" />
						  </div>
						</div>'),*/
						'preferred_city'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'city_master','key_val'=>'id','key_disp'=>'city_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$job_seeker_data['preferred_city']),
						'desire_job_type'=>array(),
						'employment_type'=>array(),
						'prefered_shift'=>array()
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job-seeker/save-detail/'.$job_seeker_data['id'].'/keyskill_detail','form_id'=>'form_keyskill_detail','onback_click'=>"view_detail_form('keyskill_detail','view')");
					$this->common_model->set_table_name('jobseeker');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
                }
            ?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
	$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','',1); 
	$key_skill_data = json_encode($key_skill);

	$desire_job_type = $this->common_front_model->get_list('job_type_list','','','array','',1); 
	$desire_job_type_data = json_encode($desire_job_type);

	
	$employment_type = $this->common_front_model->get_list('employement_list','','','array','',1); 
	$employment_type_data = json_encode($employment_type);
	
	$shift_list = $this->common_front_model->get_list('shift_list','','','array','',1);
	$shift_list_data = json_encode($shift_list);
	
	
?>
<script type="text/javascript">
	if($("#form_keyskill_detail").length > 0)
	{
		$("#form_keyskill_detail").validate({
			submitHandler: function(form)
			{
				var total_exp_year = $("#total_exp_year").val();
				var total_exp_month = $("#total_exp_month").val();
				//$("#total_experience").val(total_exp_year + '-'+total_exp_month);
				$("#total_experience").val(total_exp_year + '.'+total_exp_month);
				
				/*var annual_sal_lacs = $("#annual_sal_lacs").val();
				var annual_sal_tho = $("#annual_sal_tho").val();
				//$("#annual_salary").val(annual_sal_lacs + '-'+annual_sal_tho);
				$("#annual_salary").val(annual_sal_lacs + '.'+annual_sal_tho);
				
				var exp_annual_sal_lacs = $("#exp_annual_sal_lacs").val();
				var exp_annual_sal_tho = $("#exp_annual_sal_tho").val();
				//$("#expected_annual_salary").val(exp_annual_sal_lacs + '-'+exp_annual_sal_tho);
				$("#expected_annual_salary").val(exp_annual_sal_lacs + '.'+exp_annual_sal_tho);*/
				
				edit_profile('keyskill_detail','save');
				return false;
				//return true;
			}
		});
	}
$(function()
{

	var config = {
	'.chosen-select': {},
	'.chosen-select-deselect': { allow_single_deselect: true },
	'.chosen-select-no-single': { disable_search_threshold: 10 },
	'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
	'.chosen-select-width': { width: '100%' }			
	};
	$('#preferred_city').chosen({placeholder_text_multiple:'Select Preferred Location'});
	/*$('#key_skill').tagsInput({width:'auto'});*/
	/*$('#preferred_city').tagsInput({width:'auto'});*/
	/*$('#preferred_city').tokenfield({
		autocomplete: {
		  source: function (request, response) {
			  $.get("<?php echo base_url(); ?>sign_up/get_suggestion_city/id/"+request.term, {
			  }, function (data) {
				 response(data);
			  });
		  },
		  delay: 100
		},
		showAutocompleteOnFocus: true
	});*/
	/*$('#preferred_city').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		$.each(existingTokens, function(index, token) {
			if (token.value === event.attrs.value)
				event.preventDefault();
		});
	});*/

	$('#key_skill').tokenfield({
		autocomplete: {
		source: <?php echo $key_skill_data; ?> ,
		delay: 100,
	  },
	  showAutocompleteOnFocus: true,
	
	})
	$('#key_skill').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		$.each(existingTokens, function(index, token) {
			if (token.value === event.attrs.value)
				event.preventDefault();
		});
	});
	
	$('#desire_job_type').tokenfield({
		autocomplete: {
		source: <?php echo $desire_job_type_data; ?> ,
		delay: 100,
	  },
	  showAutocompleteOnFocus: true,
	
	})
	$('#desire_job_type').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		$.each(existingTokens, function(index, token) {
			if (token.value === event.attrs.value)
				event.preventDefault();
		});
	});
	
	$('#employment_type').tokenfield({
		autocomplete: {
		source: <?php echo $employment_type_data; ?> ,
		delay: 100,
	  },
	  showAutocompleteOnFocus: true,
	
	})
	$('#employment_type').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		$.each(existingTokens, function(index, token) {
			if (token.value === event.attrs.value)
				event.preventDefault();
		});
	});
	
	$('#prefered_shift').tokenfield({
		autocomplete: {
		source: <?php echo $shift_list_data; ?> ,
		delay: 100,
	  },
	  showAutocompleteOnFocus: true,
	
	})
	$('#prefered_shift').on('tokenfield:createtoken', function (event) {
		var existingTokens = $(this).tokenfield('getTokens');
		$.each(existingTokens, function(index, token) {
			if (token.value === event.attrs.value)
				event.preventDefault();
		});
	});

});
</script>
<?php
}
?>