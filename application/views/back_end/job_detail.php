<?php $dna = $this->common_model->data_not_availabel; ?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/tokenfield-typeahead.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/bootstrap-tokenfield.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/jquery-ui.css" />
<a class="btn btn-info" href="<?php echo $this->common_model->base_url_admin.'job/job_list'; ?>"><i class="fa fa-arrow-left"></i> Back to list</a><br/><br/>
<div class="box-tab justified" id="rootwizard">
	<div id="basic_detail">
        <?php
			$this->load->view('back_end/job_basic_detail');
		?>
    </div>
    <div id="skill_requrired">
        <?php
			$this->load->view('back_end/job_skill_requrired');
		?>
    </div>
    <div id="description">
        <?php
			$this->load->view('back_end/job_description');
		?>
    </div>
    <div id="candidate_profile">
        <?php
			$this->load->view('back_end/job_candidate_profile');
		?>
    </div>
    <div id="activity_count">
        <?php
			$this->load->view('back_end/job_activity_count');
		?>
    </div>
    <div id="applied_jobseeker">
        <?php
			//$this->load->view('back_end/job_applied_jobseeker');
		?>
    </div>
</div>
<input type="hidden" id="form_url_js" name="form_url_js" value="<?php echo $this->common_model->base_url_admin.'job/save-detail/'.$job_data['id'].'/';?>" />
<input type="hidden" id="form_url_delete" name="form_url_delete" value="<?php echo $this->common_model->base_url_admin.'job/delete-job-data/'.$job_data['id'].'/';?>" />
<input type="hidden" id="url_view_edit_form" name="url_view_edit_form" value="<?php echo $this->common_model->base_url_admin.'job/view-detail/'.$job_data['id'].'/';?>" /> 
<?php
	$this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
	$this->common_model->extra_js[] = 'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js';
	/*$this->common_model->extra_js[] = 'vendor/jquery.tagsinput/src/jquery.tagsinput.js';*/
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/jquery-ui.js';
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/bootstrap-tokenfield.js';
	$this->common_model->extra_js[] = 'vendor/typeahead/typeahead.jquery.min.js';
	
	if(isset($view_edit_mode) && $view_edit_mode !='' && $view_edit_mode =='edit')
	{
		$this->common_model->js_extra_code.= " $(function(){ view_detail_form('basic_detail','edit'); }); ";
	}
?>
<script type="text/javascript">
	function edit_profile_can(form_id,mode)
	{
		var action = $('#form_'+form_id).attr('action');
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var form_data = '';
		var hash_tocken_id = $("#hash_tocken_id").val();
		form_data = form_data + 'csrf_job_portal='+hash_tocken_id;
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	
	function edit_profile(form_id,mode)
	{
		var form_url_js = $("#form_url_js").val();
		var action = form_url_js + form_id;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var form_data = new FormData();
		
		var other_data = $('#form_'+form_id).serializeArray();
		$.each(other_data,function(key,input){
			form_data.append(input.name,input.value);
		});
		form_data.append('is_ajax',1);
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   contentType: false,
		   cache: false,
			processData:false,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
				settimeout_div();
				/*new added*/
				setTimeout(function() {
					$('.alert-success').fadeOut('fast');
				}, 4000);
				/*new added*/
		   }
		});
		return false;
	}
	function view_detail_form(form_id,mode)
	{
		var form_url_js = $("#url_view_edit_form").val();
		var action = form_url_js + form_id + '/'+mode;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var hash_tocken_id = $("#hash_tocken_id").val();
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: {'disp_mode':mode,'csrf_job_portal':hash_tocken_id,'is_ajax':1},
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	function lan_show_hide()
	{
		var total_open = parseInt($("#lan_total_count").val());
		if(total_open < 5)
		{
			total_open = total_open + 1;
			$("#lan_row_"+total_open).show();
			$("#lan_total_count").val(total_open);
		}
		if(total_open == 5)
		{
			$("#lan_add_more").hide();
		}
	}
	function substringMatcher(strs)
	{
		return function findMatches(q, cb)
		{
			var matches, substringRegex;
			matches = [];		
			substrRegex = new RegExp(q, 'i');
			$.each(strs, function(i, str)
			{
				if (substrRegex.test(str))
				{
					matches.push(str);
				}
			});
			cb(matches);
		};
	}
	function show_appliedjob(page_number)
	{
		var id = $("#id_job").val();
		var url = $("#job_app_url").val();
		url = url +''+page_number;
		var hash_tocken_id = $("#hash_tocken_id").val();
		show_comm_mask();
		$.ajax({
		   url: url,
		   type: "post",
		   data: {'csrf_job_portal':hash_tocken_id,'is_ajax':1,'id':id,'page':page_number},
		   success:function(data)
		   {
			   	$("#applied_jobseeker").html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div('applied_jobseeker');
				if($("#ajax_pagin_ul").length > 0)
				{   
					load_pagination_code_applied();
				}
		   }
		});
		return false;
	}
	function load_pagination_code_applied()
	{	
	   $("#ajax_pagin_ul li a").click(function()
	   {
			var page_number = $(this).attr("data-ci-pagination-page");
			page_number = typeof page_number !== 'undefined' ? page_number : 0;
			if(page_number == 0)
			{
				return false;
			}
			if(page_number != undefined && page_number !='' && page_number != 0)
			{
				show_appliedjob(page_number);
			}
			return false;
	   });
	}
</script>