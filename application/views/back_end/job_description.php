<?php $dna = $this->common_model->data_not_availabel;
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_data['job_description']) && $id !='')
{
	$job_data = $this->common_model->get_count_data_manual('job_posting',array('id'=>$id),1,'job_description,id','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left text-bold">Job Description</div>
        <div class="panel-controls">
            <?php
			if($disp_mode =='view')
			{
			?>
            	<a href="javascript:;" onClick="view_detail_form('description','edit')">Edit</a>
            <?php
			}
			?>
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
         </div>
    </div>
    <div class="panel-body">
    	<?php
			if(isset($respones_ss->response) && $respones_ss->response !='')
			{
				echo $respones_ss->response;
			}
		?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
            	<?php
				if($disp_mode =='view')
				{
				?>
                <p>
                        <?php if(isset($job_data['job_description']) && $job_data['job_description'] !=''){ echo nl2br($job_data['job_description']);}else {echo $dna;} ?>
                </p>
                <?php
				}
				else
				{
					$ele_array = array(
						'job_description'=>array('type'=>'textarea','is_required'=>'required'),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job/save-detail/'.$job_data['id'].'/description','form_id'=>'form_description','onback_click'=>"view_detail_form('description','view')");
					$this->common_model->set_table_name('job_posting');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
				}
				?>
             </div>
        </div>
    </div>
</div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
if($("#form_description").length > 0)
{
	$("#form_description").validate({
		submitHandler: function(form) 
		{
			edit_profile('description','save');
			return false;
			//return true;
		}
	});
}
</script>
<?php
}
?>