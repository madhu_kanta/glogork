<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($job_data['job_title']) && $id !='')
{
	$job_data = $this->common_model->get_count_data_manual('job_posting_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
if($disp_mode =='edit')
{
?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.min.css" />
<script type="text/javascript" src="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.jquery.min.js" ></script>
<?php
}
?>

<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary form-horizontal">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">
                	Job Detail
                </div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('skill_requrired','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Skill Keyword</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php 
									if(isset($job_data['skill_keyword']) && $job_data['skill_keyword'] !='')
									{
										echo $job_data['skill_keyword'];
									}
									else
									{
										echo $dna;
									} 
								?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Location Hiring</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php 
                            if(isset($job_data['location_hiring']) && $job_data['location_hiring'] !='')
                            { 
								echo $this->common_model->valueFromId('city_master',$job_data['location_hiring'],'city_name');
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Job Education</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_data['job_education']) && $job_data['job_education'] !='')
									{ 
										echo $this->common_model->valueFromId('educational_qualification_master',$job_data['job_education'],'educational_qualification');
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                		<div class="form-group mb0 text-left">
                        	<label class="col-sm-4 col-xs-4 control-label">Job Highlighted</label>
	                        <label class="col-sm-8 col-xs-8 control-label-val">                            
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_data['job_highlighted']) && $job_data['job_highlighted'] !='')
									{ 
										echo $job_data['job_highlighted'];
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <div class="form-group mb0 text-left">
                        	<label class="col-sm-4 col-xs-4 control-label">Job Payment</label>
	                        <label class="col-sm-8 col-xs-8 control-label-val">                            
                            	<strong>:</strong>&nbsp;
								<?php 
									if(isset($job_data['job_payment']) && $job_data['job_payment'] !='' && $job_data['job_payment'] !='0')
									{ 
										echo $this->common_model->valueFromId('job_payment_master',$job_data['job_payment'],'job_payment');
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Link Job</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_data['link_job']) && $job_data['link_job'] !='')
                            { 
                                echo '<a target="_blank" href="'.$job_data['link_job'].'">'.$job_data['link_job'].'</a>';
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div> 
                	</div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                		<div class="form-group mb0 text-left">
                        	<label class="col-sm-4 col-xs-4 control-label">Job Life</label>
	                        <label class="col-sm-8 col-xs-8 control-label-val">                            
                            	<strong>:</strong>&nbsp;
								<?php if(isset($job_data['job_life']) && $job_data['job_life'] !='')
									{ 
										echo $job_data['job_life'].' Days';
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                        <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Job Shift</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['job_shift_type']) && $job_data['job_shift_type'] !=''){ echo $job_data['job_shift_type'];}else {echo $dna;} ?>
                        </label>
                    	</div>
                        <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Currently Hiring</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php 
                            if(isset($job_data['currently_hiring_status']) && $job_data['currently_hiring_status'] !='')
                            {
                                echo $job_data['currently_hiring_status'];
                            }
                            else
                            {
                                echo $dna;
                            } ?>
                        </label>
                    </div>
                    
                	</div>
	                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">                    
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">Job Type</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['job_type_name']) && $job_data['job_type_name'] !=''){ echo $job_data['job_type_name'];}else {echo $dna;} ?>
                        </label>
                    </div>
                    <div class="form-group mb0">
                        <label class="col-sm-4 col-xs-4 control-label">No. Requirement</label>
                        <label class="col-sm-8 col-xs-8 control-label-val">
                        	<strong>:</strong>&nbsp;
                            <?php if(isset($job_data['total_requirenment']) && $job_data['total_requirenment'] !='')
							{ 
								echo $job_data['total_requirenment'];
							}
							else 
							{
								echo $dna;
							}
							?>
                        </label>
                    </div>
                	</div>
                </div>
                
                <?php
                }
                else
                {	
					$temp_arr = range(1,100);
					$temp_arr_tot = array_combine($temp_arr,$temp_arr);
					$ele_array = array(
						'skill_keyword'=>array('is_required'=>'required'),
						'location_hiring'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'city_master','key_val'=>'id','key_disp'=>'city_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$job_data['location_hiring']),
						'job_education'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'educational_qualification_master','key_val'=>'id','key_disp'=>'educational_qualification'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$job_data['job_education']),
						
						'link_job'=>array('input_type'=>'url'),
						'job_shift'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'shift_type','key_val'=>'id','key_disp'=>'shift_type'),'value'=>$job_data['job_shift']),
						'job_type'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'job_type_master','key_val'=>'id','key_disp'=>'job_type'),'value'=>$job_data['job_type']),
						'total_requirenment'=>array('is_required'=>'required','type'=>'dropdown','value'=>$job_data['total_requirenment'],'value_arr'=>$temp_arr_tot),
						'currently_hiring_status'=>array('type'=>'radio','value_arr'=>array('Yes'=>'Yes','No'=>'No')),
						'job_payment'=>array('type'=>'dropdown','relation'=>array('rel_table'=>'job_payment_master','key_val'=>'id','key_disp'=>'job_payment'),'value'=>$job_data['job_payment']),
						
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'job/save-detail/'.$job_data['id'].'/skill_requrired','form_id'=>'form_skill_requrired','onback_click'=>"view_detail_form('skill_requrired','view')");
					$this->common_model->set_table_name('job_posting');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
                }
            ?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
	$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','',1); 
	$key_skill_data = json_encode($key_skill);
?>
<script type="text/javascript">
	if($("#form_skill_requrired").length > 0)
	{
		$("#form_skill_requrired").validate({
			submitHandler: function(form)
			{
				edit_profile('skill_requrired','save');
				return false;
			}
		});
		
		$(function()
		{
			var config = {
			'.chosen-select': {},
			'.chosen-select-deselect': { allow_single_deselect: true },
			'.chosen-select-no-single': { disable_search_threshold: 10 },
			'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
			'.chosen-select-width': { width: '100%' }			
			};
			$('#location_hiring').chosen({placeholder_text_multiple:'Select Location Hiring'});
			$('#job_education').chosen({placeholder_text_multiple:'Select Education Required'});
			
			$('#skill_keyword').tokenfield({
				autocomplete: {
				source: <?php echo $key_skill_data; ?> ,
				delay: 100,
			  },
			  showAutocompleteOnFocus: true,
			});
			$('#skill_keyword').on('tokenfield:createtoken', function (event) {
				var existingTokens = $(this).tokenfield('getTokens');
				$.each(existingTokens, function(index, token) {
					if (token.value === event.attrs.value)
						event.preventDefault();
				});
			});
		});
	}

</script>
<?php
}
?>