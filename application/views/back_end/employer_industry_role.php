<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($employer_data['fullname']) && $id !='')
{
	$employer_data = $this->common_model->get_count_data_manual('employer_master_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}

if(isset($disp_mode) && $disp_mode =='edit')
{
	if($this->session->userdata('success_message_js'))
	{
		$disp_mode = 'edit';
		if(!isset($respones_ss->response))
		{
			$respones_ss = new stdClass;
			$respones_ss->response = $this->session->userdata('success_message_js');
		}
		$this->session->unset_userdata('success_message_js');
	}
}
if($disp_mode =='edit')
{
?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.min.css" />
<script type="text/javascript" src="<?php echo $base_url.'assets/back_end/';?>vendor/chosen_v1.4.0/chosen.jquery.min.js" ></script>
<?php
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary form-horizontal">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">
                	Industry and Skills/Roles I hire for
                </div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('industry_role','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Industries</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php 
									if(isset($employer_data['industry_hire']) && $employer_data['industry_hire'] !='')
									{
										echo $this->common_model->valueFromId('industries_master',$employer_data['industry_hire'],'industries_name');
										//echo $employer_data['industry_hire'];
									}
									else 
									{
										echo $dna;
									}
								?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Functional Area</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['function_area_hire']) && $employer_data['function_area_hire'] !='')
								{
									echo $this->common_model->valueFromId('functional_area_master',$employer_data['function_area_hire'],'functional_name');
								}
								else 
								{
									echo $dna;
								} ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 col-xs-12">
						<div class="form-group mb0 text-left">
                            <label class="col-lg-1 col-md-1 col-sm-2 col-xs-2 control-label">Skill Hire</label>
                            <label class="col-sm-10 col-xs-10 control-label-val ml30" style="padding-left:12px" >
                            	<strong>:</strong>&nbsp;
								<?php if(isset($employer_data['skill_hire']) && $employer_data['skill_hire'] !='')
								{ 
									echo $this->common_model->valueFromId('key_skill_master',$employer_data['skill_hire'],'key_skill_name');
								}
								else 
								{
									echo $dna;
								}?>
                            </label>
                        </div>
                    </div>
                </div>
                <?php
                }
                else
                {					
					$ele_array = array(
						'industry_hire'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'industries_master','key_val'=>'id','key_disp'=>'industries_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$employer_data['industry_hire']),
						'function_area_hire'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'functional_area_master','key_val'=>'id','key_disp'=>'functional_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$employer_data['function_area_hire']),
						'skill_hire'=>array('is_required'=>'required','type'=>'dropdown','relation'=>array('rel_table'=>'key_skill_master','key_val'=>'id','key_disp'=>'key_skill_name'),'is_multiple'=>'yes','display_placeholder'=>'No','class'=>'chosen-select','value'=>$employer_data['skill_hire']),
					);
					$other_config = array('mode'=>'edit','id'=>$id,'action'=>'employer/save-detail/'.$employer_data['id'].'/industry_role','form_id'=>'form_industry_role','onback_click'=>"view_detail_form('industry_role','view')");
					$this->common_model->set_table_name('employer_master');
					echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
                }
            ?>
            </div>
        </div>
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	$(function(){
		var config = {
		'.chosen-select': {},
		'.chosen-select-deselect': { allow_single_deselect: true },
		'.chosen-select-no-single': { disable_search_threshold: 10 },
		'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
		'.chosen-select-width': { width: '100%' }			
		};
		$('#industry_hire').chosen({placeholder_text_multiple:'Select Industry'});
		$('#function_area_hire').chosen({placeholder_text_multiple:'Select Functional Area'});
		$('#skill_hire').chosen({placeholder_text_multiple:'Select Job Role'});
	});
	if($("#form_industry_role").length > 0)
	{
		$("#form_industry_role").validate({
			submitHandler: function(form)
			{
				edit_profile('industry_role','save');
				return false;
			}
		});
	}
</script>
<?php
}
?>