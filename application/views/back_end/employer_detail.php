<?php $dna = $this->common_model->data_not_availabel; ?>
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
<!--<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery.tagsinput/src/jquery.tagsinput.css" />-->

<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/tokenfield-typeahead.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/bootstrap-tokenfield.css" />
<link rel="stylesheet" href="<?php echo $base_url.'assets/back_end/';?>vendor/jquery-tag-type/jquery-ui.css" />
<a class="btn btn-info" href="<?php echo $this->common_model->base_url_admin.'employer/employer_list'; ?>"><i class="fa fa-arrow-left"></i> Back to list</a><br/><br/>
<div class="box-tab justified" id="rootwizard">
	<div id="basic_detail">
        <?php
			$this->load->view('back_end/employer_basic_detail');
		?>
    </div>
    <div id="description">
        <?php
			$this->load->view('back_end/employer_description');
		?>
    </div>
    <div id="work_exp">
		<?php
			$this->load->view('back_end/employer_work_exp');
		?>
    </div>
    <div id="company_detail">
    	<?php
			$this->load->view('back_end/employer_company_detail');
		?>
    </div>    
    <div id="company_profile">
    	<?php
			$this->load->view('back_end/employer_company_profile');
		?>
    </div>
    <div id="plan_detail">
    	<?php
			$this->load->view('back_end/employer_plan_detail');
		?>
    </div>
    <div id="activity_count">
	    <?php
			$this->load->view('back_end/employer_activity_count');
		?>    
    </div>
    <div id="industry_role">
    	<?php
			$this->load->view('back_end/employer_industry_role');
		?>    
    </div>
    <div id="other_detail">
    	<?php
			$this->load->view('back_end/employer_other_detail');
		?>    
    </div>
</div>
<input type="hidden" id="form_url_js" name="form_url_js" value="<?php echo $this->common_model->base_url_admin.'employer/save-detail/'.$employer_data['id'].'/';?>" />
<input type="hidden" id="form_url_delete" name="form_url_delete" value="<?php echo $this->common_model->base_url_admin.'employer/delete-js-data/'.$employer_data['id'].'/';?>" />
<input type="hidden" id="url_view_edit_form" name="url_view_edit_form" value="<?php echo $this->common_model->base_url_admin.'employer/view-detail/'.$employer_data['id'].'/';?>" />
<?php
	$this->common_model->extra_js[] = 'vendor/jquery-validation/dist/additional-methods.min.js';
	$this->common_model->extra_js[] = 'vendor/bootstrap-datepicker/js/bootstrap-datepicker.js';
	/*$this->common_model->extra_js[] = 'vendor/jquery.tagsinput/src/jquery.tagsinput.js';*/
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/jquery-ui.js';
	$this->common_model->extra_js[] = 'vendor/jquery-tag-type/bootstrap-tokenfield.js';
	$this->common_model->extra_js[] = 'vendor/typeahead/typeahead.jquery.min.js';
	
	if(isset($view_edit_mode) && $view_edit_mode !='' && $view_edit_mode =='edit')
	{
		$this->common_model->js_extra_code.= " $(function(){ view_detail_form('basic_detail','edit'); }); ";
	}
?>
<script type="text/javascript">
	function edit_profile_can(form_id,mode)
	{
		var action = $('#form_'+form_id).attr('action');
		
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refresh page and Try again');
			return false;
		}
		var form_data = '';
		var hash_tocken_id = $("#hash_tocken_id").val();
		form_data = form_data + 'csrf_job_portal='+hash_tocken_id;
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	
	function edit_profile(form_id,mode)
	{
		var form_url_js = $("#form_url_js").val();
		var action = form_url_js + form_id;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refress page and Try again');
			return false;
		}
		var form_data = new FormData();
		if($("#profile_pic").length > 0)
		{
			var profile_img = $('input[name=profile_pic]')[0].files[0];
			if(profile_img != 'undefined' && profile_img != '' && profile_img != undefined)
			{
				form_data.append("profile_pic", profile_img);
			}
		}
		if($("#company_logo").length > 0)
		{
			var company_logo = $('input[name=company_logo]')[0].files[0];
			if(company_logo != 'undefined' && company_logo != '' && company_logo != undefined)
			{
				form_data.append("company_logo", company_logo);
			}
		}
		if($("#resume_file").length > 0)
		{
			///alert($('input[name=resume_file]')[0].files[0]);
			var resume_file = $('input[name=resume_file]')[0].files[0];
			if(resume_file != 'undefined' && resume_file != '' && resume_file != undefined)
			{
				form_data.append("resume_file", $('input[name=resume_file]')[0].files[0]);
				//form_data.append("resume_file", $('input[name=resume_file]')[0].files[0]);
			}
		}
		var other_data = $('#form_'+form_id).serializeArray();
		$.each(other_data,function(key,input){
			form_data.append(input.name,input.value);
		});
		form_data.append('is_ajax',1);
		//var form_data = $('#form_'+form_id).serialize();
		//var hash_tocken_id = $("#hash_tocken_id").val();
		///form_data = 'csrf_job_portal='+hash_tocken_id+'is_ajax=1';
		//form_data = form_data+'&is_ajax=1';
		//form_data = form_data+'&profile_pic='+$('input[name=profile_pic]')[0].files[0];	
		//form_data.append('is_ajax',1);
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: form_data,
		   contentType: false,
		   cache: false,
			processData:false,
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
				settimeout_div();
				/*new added*/
				setTimeout(function() {
					$('.alert-success').fadeOut('fast');
				}, 4000);
				/*new added*/
		   }
		});
		return false;
	}
	function view_detail_form(form_id,mode)
	{
		var form_url_js = $("#url_view_edit_form").val();
		var action = form_url_js + form_id + '/'+mode;
		if(action == 'undefined' || action == '' || action == undefined)
		{
			alert('Please refresh page and Try again');
			return false;
		}
		var hash_tocken_id = $("#hash_tocken_id").val();
		show_comm_mask();
		$.ajax({
		   url: action,
		   type: "post",
		   data: {'disp_mode':mode,'csrf_job_portal':hash_tocken_id,'is_ajax':1},
		   success:function(data)
		   {
			   	$("#"+form_id).html(data);
				var tocken = $("#hash_tocken_id_temp").val();
				update_tocken(tocken);
				remove_element("#hash_tocken_id_temp",0);
				hide_comm_mask();
				remove_element(".response_message");
				scroll_to_div(form_id);
		   }
		});
		return false;
	}
	function lan_show_hide()
	{
		var total_open = parseInt($("#lan_total_count").val());
		if(total_open < 5)
		{
			total_open = total_open + 1;
			$("#lan_row_"+total_open).show();
			$("#lan_total_count").val(total_open);
		}
		if(total_open == 5)
		{
			$("#lan_add_more").hide();
		}
	}
	function delete_js_data(form_id,lan_id)
	{
		if(confirm("Are you sure  you want to delete data?"))
		{
			var form_data = '';
			var hash_tocken_id = $("#hash_tocken_id").val();
			
			var form_url_js = $("#form_url_delete").val();
			var action = form_url_js + form_id+'/'+lan_id;
		
			form_data = form_data + 'csrf_job_portal='+hash_tocken_id;
			show_comm_mask();
			$.ajax({
			   url: action,
			   type: "post",
			   data: form_data,
			   success:function(data)
			   {
					$("#"+form_id).html(data);
					var tocken = $("#hash_tocken_id_temp").val();
					update_tocken(tocken);
					remove_element("#hash_tocken_id_temp",0);
					hide_comm_mask();
					scroll_to_div(form_id);
					$(".panel").collapse() 
			   }
			});
		}
	}

	function substringMatcher(strs)
	{
		return function findMatches(q, cb)
		{
			var matches, substringRegex;
			matches = [];		
			substrRegex = new RegExp(q, 'i');
			$.each(strs, function(i, str)
			{
				if (substrRegex.test(str))
				{
					matches.push(str);
				}
			});
			cb(matches);
		};
	}
</script>