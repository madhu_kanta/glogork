<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if($id !='')
{
	//jobseeker_access_employer
	
	$posted_job_count = $this->common_model->get_count_data_manual('job_posting',array('posted_by'=>$id,'is_deleted'=>'No'),0,' id ','',0,'',0);
	$current_data = $this->common_model->getCurrentDate('Y-m-d');
	$job_expired_on_where = " job_expired_on >= '$current_data' ";
	$active_job_count = $this->common_model->get_count_data_manual('job_posting',array('posted_by'=>$id,'is_deleted'=>'No','currently_hiring_status'=>'Yes',$job_expired_on_where),0,' id ','',0,'',0);
	$highlighted_job_count = $this->common_model->get_count_data_manual('job_posting',array('posted_by'=>$id,'is_deleted'=>'No','job_highlighted'=>'Yes'),0,' id ','',0,'',0);

	$this->db->join('job_posting jp', 'job_application_history.job_id = jp.id', 'inner');
	$applied_job_count = $this->common_model->get_count_data_manual('job_application_history',array('jp.posted_by'=>$id),0,' id ','',0,'',0);
	
	
	$this->db->join('job_posting jp', 'job_application_history.job_id = jp.id', 'inner');
	$shortlisted_job_count = $this->common_model->get_count_data_manual('job_application_history',array('jp.posted_by'=>$id,'job_application_history.is_shortlisted'=>'Yes'),0,' id ','',0,'',0);
	
	
	
	$follow_emp_count = $this->common_model->get_count_data_manual('jobseeker_access_employer',array('emp_id'=>$id,'is_follow'=>'Yes'),0,' id ','',0,'',0);
	$liked_emp_count = $this->common_model->get_count_data_manual('jobseeker_access_employer',array('emp_id'=>$id,'is_liked'=>'Yes'),0,' id ','',0,'',0);
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-left  text-bold">Jobs Count</div>
        <div class="panel-controls">
            <a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <?php
            if($disp_mode =='view')
            {
        ?>
        <div>                    
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Posted Job</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($posted_job_count) && $posted_job_count !='')
                        {
                            echo $posted_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Active Job</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($active_job_count) && $active_job_count !='')
                        {
                            echo $active_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Highlighted Job</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($highlighted_job_count) && $highlighted_job_count !='')
                        {
                            echo $highlighted_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Applied Job Seeker</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($applied_job_count) && $applied_job_count !='')
                        {
                            echo $applied_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Shortlisted Application</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($shortlisted_job_count) && $shortlisted_job_count !='')
                        {
                            echo $shortlisted_job_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Followers</label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($follow_emp_count) && $follow_emp_count !='')
                        {
                            echo $follow_emp_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>                    
                    <div class="form-group mb0">
                    <label class="col-sm-5 col-xs-5 control-label">Profile Liked </label>
                    <label class="col-sm-7 col-xs-7 control-label-val">
                        <strong>:</strong>&nbsp;
                        <?php if(isset($liked_emp_count) && $liked_emp_count !='')
                        {
                            echo $liked_emp_count;
                        }
                        else 
                        {
                            echo $dna;
                        }
                        ?>
                    </label>
                    </div>
                </div>
                
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>