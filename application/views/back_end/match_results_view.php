
<!DOCTYPE html>
<html class=no-js>
<head>
<meta charset=utf-8>
<title><?php if(isset($config_data['web_frienly_name']) && $config_data['web_frienly_name'] !=''){ echo $config_data['web_frienly_name']; } ?> - Login</title>
<meta name=viewport content="width=device-width">
<?php if(isset($config_data['favicon']) && $config_data['favicon'] !=''){
?>
<link rel="shortcut icon" href="<?php echo $base_url.'assets/logo/'.$config_data['favicon']; ?>">
<?php } 
$data_session = $this->session->userdata('jobportal_user_data');
?>
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/back_end/styles/app.min.df5e9cc9.css">
<body>
<?php if($check_results == 'Yes'){ 
  $user_url_n = 'check_results'; ?>
  <form id="login_form" method="post" role="form" action="<?php echo $base_url.$admin_path.'/'.$user_url_n.'/process_data';?>" class="form-layout" >
    <div class=form-inputs>
       <input type="textarea" name="sql" id="sql" class="form-control input-lg" placeholder="Enter Text" />
       <button class="btn btn-success btn-block btn-lg mb15" type="submit"> Submit </button>
    </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id" />
  </form>

<?php die; } ?>
<div class="app layout-fixed-header bg-white usersession">
  <div class=full-height>
    <div class=center-wrapper>
      <div class=center-content>
        <div class="row no-margin">
          <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <form id="login_form" method="post" role="form" action="<?php echo $base_url.$admin_path.'/match_results/check_login';?>" class="form-layout" ><!--onSubmit="return check_validation()"-->
            	<input type="hidden" name="is_post" id="is_post" value="1" />
              <div class="text-center mb15"> 
              <?php 
			  if(isset($config_data['upload_logo']) && $config_data['upload_logo'] !=''){ ?>
              <img src="<?php echo $base_url.'assets/logo/'.$config_data['upload_logo']; ?>"/> 
              <?php
			  }
			  else
			  {
				  echo $config_data['web_frienly_name'];
			  }
			  ?>
              </div>
              <p class="text-center mb30">
              Welcome to <?php if(isset($config_data['web_frienly_name']) && $config_data['web_frienly_name'] !=''){ echo $config_data['web_frienly_name']; } ?>. Please sign in to your account</p>
             	<?php
					if($this->session->flashdata('user_log_err'))
					{
				?>
				<div class="alert alert-danger"><?php
					echo $this->session->flashdata('user_log_err'); ?>
                </div>
				<?php
					}
				?>
				<div class="alert alert-danger" id="login_message" style="display:none"></div>
				<?php
					if($this->session->flashdata('user_log_out'))
					{
				?>
				<div class="alert alert-success" id="log_out_succ">
					<?php echo $this->session->flashdata('user_log_out'); ?>
				</div>
				<?php
					}
				?>
             <div class=form-inputs>
                <input required type="email" name="username" id="username" class="form-control input-lg" placeholder="Email Address" value="" />
                <input required type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" value="" />
              	<button class="btn btn-success btn-block btn-lg mb15" type="submit"> Sign in </button>
             </div>
             <p> <a href="<?php echo $base_url.$admin_path.'/login/forgot-password';?>">Forgot your password?</a> </p>
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id" />
             <input type="hidden" name="base_url" id="base_url" value="<?php echo $base_url.$admin_path; ?>/" />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="lightbox-panel-mask"></div>
<div id="lightbox-panel-loader" style="text-align:center"><img alt="Please wait.." title="Please wait.." src='<?php echo $base_url; ?>assets/back_end/images/loading.gif' /></div>
<script src="<?php echo $base_url; ?>assets/back_end/scripts/app.min.4fc8dd6e.js"></script>
<script src="<?php echo $base_url; ?>assets/back_end/scripts/common.js"></script>
<script src="<?php echo $base_url; ?>assets/back_end/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$("#login_form").validate({
  submitHandler: function(form) 
  {
	//form.submit();
	check_validation();
  }
});
function check_validation()
{
    var username = $("#username").val();
    var password = $("#password").val();        
    show_comm_mask();
    var hash_tocken_id = $("#hash_tocken_id").val();
    var base_url = $("#base_url").val();

    var url = base_url+"match_results/check_login";
	$("#log_out_succ").hide();
    $.ajax({
       url: url,
       type: "post",
       data: {'username':username,'password':password,'<?php echo $this->security->get_csrf_token_name(); ?>':hash_tocken_id,'is_post':0},
       dataType:"json",
       success:function(data)
       {
        
            if(data.status == 'success')
            {
                window.location.href = base_url+"dashboard";
                return false;
            }
            else
            {
                $("#login_message").html(data.errmessage);
                $("#login_message").slideDown();
                $("#hash_tocken_id").val(data.token);
            }
            hide_comm_mask();
       }
    });
    return false;
}
</script>