<?php $dna = $this->common_model->data_not_availabel;
$base_url = base_url();
if(!isset($disp_mode) || $disp_mode =='')
{
	$disp_mode = 'view';
}
if(!isset($employer_data['fullname']) && $id !='')
{
	$employer_data = $this->common_model->get_count_data_manual('employer_master_view',array('id'=>$id),1,' * ','',0,'',0);
}
if(isset($respones) && $respones !='')
{
	$respones_ss = json_decode($respones['data']);	
}
if($this->session->flashdata('error_message'))
{
	$disp_mode = 'edit';
	if(!isset($respones_ss->response))
	{
		$respones_ss = new stdClass;
		$respones_ss->response = '<div class="alert alert-danger alert-dismissable"><div class="fa fa-warning"></div><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$this->session->flashdata('error_message').'</div>';
	}
	$this->session->unset_userdata('error_message');
}
?>
	<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="panel panel-primary">
        	<div class="panel-heading">
            	<div class="pull-left  text-bold">Other Details</div>
               	<div class="panel-controls">
                	<?php
						if($disp_mode =='view')
						{
						?>
						<a href="javascript:;" onClick="view_detail_form('other_detail','edit')">Edit</a>
						<?php
						}
					?>
                   	<a href="#" class="panel-collapse" data-toggle="panel-collapse"> <i class="panel-icon-chevron"></i> </a>
                </div>
            </div>
            <div class="panel-body form-horizontal">
            	<?php
					if(isset($respones_ss->response) && $respones_ss->response !='')
					{
						echo $respones_ss->response;
					}
					if($disp_mode =='view')
					{
				?>
                <div>
                	<h4>Social Link</h4>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">    
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-facebook"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
                                        if(isset($employer_data['facebook_url']) && $employer_data['facebook_url'] !='')
                                        {
                                    ?>
                                        <a target="_blank" href="<?php echo $employer_data['facebook_url'];?>"><?php echo $employer_data['facebook_url'];?></a>
                                    <?php
                                        }
                                        else
                                        {
                                            echo $dna;
                                        } 
                                    ?>
                                </label>
                       		</div>
                     	</div>
                     	<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-google-plus"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
										if(isset($employer_data['gplus_url']) && $employer_data['gplus_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $employer_data['gplus_url'];?>"><?php echo $employer_data['gplus_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-twitter"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
	                                <strong>:</strong>&nbsp;
                                    <?php 
										if(isset($employer_data['twitter_url']) && $employer_data['twitter_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $employer_data['twitter_url'];?>"><?php echo $employer_data['twitter_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-linkedin"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
                                	<strong>:</strong>&nbsp;
                                    <?php 
										if(isset($employer_data['linkedin_url']) && $employer_data['linkedin_url'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $employer_data['linkedin_url'];?>"><?php echo $employer_data['linkedin_url'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>
                        <!--<div class="col-lg-4 col-md-6 col-sm-6 col-sm-12 col-xs-12">
                        	<div class="form-group mb0">
                                <label class="col-sm-2 col-xs-2 control-label">
                                    <i class="fa fa-globe"></i>
                                </label>
                                <label class="col-sm-10 col-xs-10 control-label-val">
	                                <strong>:</strong>&nbsp;
                                    <?php 
										if(isset($employer_data['website']) && $employer_data['website'] !='')
										{
									?>
										<a target="_blank" href="<?php echo $employer_data['website'];?>"><?php echo $employer_data['website'];?></a>
									<?php
										}
										else
										{
											echo $dna;
										} 
									?>
                                </label>
                       		</div>
                    	</div>-->
                 	</div>
                </div>
                <?php
					}
					else
					{
						$ele_array = array(
							'facebook_url'=>array('input_type'=>'url'),
							'gplus_url'=>array('input_type'=>'url'),
							'twitter_url'=>array('input_type'=>'url'),
							'linkedin_url'=>array('input_type'=>'url')
						);
						$other_config = array('mode'=>'edit','id'=>$id,'action'=>'employer/save-detail/'.$employer_data['id'].'/other_detail','form_id'=>'form_other_detail','onback_click'=>"view_detail_form('other_detail','view')");
						$this->common_model->set_table_name('employer_master');
						echo $data = $this->common_model->generate_form_main($ele_array,$other_config);
					}
				?>
            </div>
        </div>
   
<?php
if($disp_mode !='view')
{
?>
<script type="text/javascript">
	if($("#form_other_detail").length > 0)
	{
		$("#form_other_detail").validate({
			submitHandler: function(form)
			{
				edit_profile('other_detail','save');
				return false;
				//return true;
			}
		});
	}
</script>
<?php
}
?>