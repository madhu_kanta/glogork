<?php //$custom_lable_arr = $custom_lable->language;
 ?>
<?php $upload_path = $this->common_front_model->fileuploadpaths('resume_file',1); ?>

                        		<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid;">
								<div class="panel-heading panel-bg " style="color:#ffffff;border-bottom:4px solid;"><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-file"></span> Your latest resume</span> <!--<a href="#" class="btn btn-md pull-right" style="margin:-3px;"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> Edit</a>--></div><!--background:none;-->
								<div class="panel-body" style="padding:10px;">
									<div class="row"><!--pull-right-->
                                    <div id="success_message" style="position:fixed;  left:40%; top:18%; z-index:9999; opacity:0">
                                    
                                    </div>
                                    <?php if(!($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file'])))
			{ ?>			
                                        <div class="col-lg-7 col-md-8 col-sm-7 col-xs-10 col-xs-offset-1 alert alert-danger " id="noreusme" >
                                            <strong> Sorry ! </strong> Your resume is not avilable.Please upload to increase the value of your profile.
                                        </div>
                                    <?php } ?>
                                    
                                    	<div class=" <?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?> col-lg-6  col-md-6  col-sm-7  col-xs-offset-0 col-xs-12<?php }else{ ?> col-lg-4  col-md-3 col-sm-4  col-xs-12 <?php } ?>"><!-- col-lg-offset-6 col-md-offset-6 col-sm-offset-5 --><!--col-lg-offset-9 col-md-offset-6 col-sm-offset-5 col-xs-offset-0 -->
                                    		<div class="row">
                                            <?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ $getfilename = explode('.',$user_data_values['resume_file']); ?>
                                        	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 marginclass" id="downloadresumediv" >
                                            	<a href="<?php echo $upload_path.'/'.$user_data_values['resume_file']; ?>" class="btn btn-sm btn-info col-xs-12" download="<?php echo $user_data_values['fullname'].'.'.$getfilename['1']; ?>"><i class="fa fa-fw fa-download" aria-hidden="true"></i> Download Resume</a>
                                            </div>
                                            <?php } ?>
                                            
                                            <div class="<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass <?php }else{ ?>col-lg-offset-5 col-lg-6 col-md-8 col-sm-7 col-xs-12 marginclass<?php } ?>" id="imagediv">
                                            
                                            	<div class="btn btn-sm btn-warning col-xs-12" style="cursor:pointer;">
                                            	<input type="file" id="uploadimage" name="uploadimage" class="fileinp col-xs-12"><i class="fa fa-fw fa-upload" aria-hidden="true"></i> Upload Resume</div>
                                                </div>
                                            <?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>    
                                            <div class="<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>col-lg-3 col-md-3 col-sm-3  col-xs-12 marginclass<?php }else{ ?>col-lg-6 col-md-3 col-sm-3  col-xs-12 marginclass<?php } ?>" id="deletediv" data-deletepart="resume" data-deleteid="<?php echo $user_data_values['resume_file'];?>">
                                            	<a href="javascript:void(0)" id="" data-file = "<?php echo $upload_path.'/'.$user_data_values['resume_file']; ?>" class="btn btn-sm btn-danger col-xs-12" ><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a>
                                            </div>
                                        <?php } ?>
                                        </div>
                                        <?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>
                                        <h6 class="text-gray m-top-10 nospace1"><i class="fa fa-clock-o"></i> Last Updated Date : <?php echo $this->common_front_model->displayDate($user_data_values['resume_last_update'],'F j, Y h:i A'); ?></h6>
                                        <?php } ?>
                                    	</div>
									</div>
                                    
									<!--<div class="row">
										<div class="margin-top-20 col-md-12 col-sm-12 col-xs-12">
                                        	<?php //echo $user_data_values['resume_last_update']; ?>
										</div>
									</div>-->
								</div>
							</div>