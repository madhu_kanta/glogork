<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="panel-heading panel-bg bg-new-p" id="education_vieweditdiv"><span class="th_bgcolor" style="padding:5px;"><span class="fa fa-graduation-cap" style="margin-right:5px;"></span> Education Details</span> </div>
<form method="post" id="educationform" name="educationform" action="<?php echo $base_url.'my_profile/editedudet'; ?>" >
<?php if($this->my_profile_model->checkifadded('10'))
{
 ?>
<!--<div>
	<a  class="button" href="javascript:void(0);" style="color:#fff" id="educ10" onClick="showpart('educ10')">add 10 th</a>
</div>-->
<div class="col-md-2 col-xs-12">
	<div class="margin-top-20"></div>
	<h5> Standard - X :<!--<span class="red-only"> *</span>--></h5>                                                        
</div>
<div class="col-md-4 col-xs-12">
	<div class="margin-top-20"></div>
    	<h5 >University/ Board</h5>
		<input type="text" name="tenthu" id="tenthu" class="form-control language" placeholder="Enter University/ Board" value="">
</div>

<div class="col-md-4 col-xs-12">
<input type="hidden" name="tenthhid" id="tenthhid" class="form-control language" value="">
<div class="margin-top-20"></div>
		<h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <!--<span class="red-only">*</span>--></h5>
                                               <select name="tenthy" id="tenthy" data-pasyrten="" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <?php 
                                                        $current_year = date('Y');
                                                        echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
                                                        for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?>
                                                             <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                                                        <?php 	
                                                        } 
                                                        ?>
                                                    </select>
</div>
<div class="col-md-2 col-xs-12">
<div class="margin-top-20"></div>
		<h5 >Marks</h5>	
		<input type="text" name="tenthm" id="tenthm" class="form-control language" placeholder="Marks" value="">
</div>                                         
<?php } ?>
<?php if($this->my_profile_model->checkifadded('12'))
{
 ?>
 <div class="clearfix"></div>
<div class="col-md-2 col-xs-12">
	<div class="margin-top-20"></div>
	<h5 >Standard - XII :<!--<span class="red-only"> *</span>--></h5>                                                        
</div>
<div class="col-md-4 col-xs-12">
	<div class="margin-top-20"></div>
    	<h5 >University/ Board</h5>
		<input type="text" name="twelveu" id="twelveu" class="form-control language" placeholder="Enter University/ Board" value="">
</div>
<div class="col-md-4 col-xs-12">
<div class="margin-top-20"></div>
		<h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <!--<span class="red-only">*</span>--></h5>
                                               <select name="twelvey" id="twelvey" data-pasyrten="" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <?php 
                                                        $current_year = date('Y');
                                                        echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
                                                        for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?>
                                                             <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                                                        <?php 	
                                                        } 
                                                        ?>
                                                    </select>
</div>
<div class="col-md-2 col-xs-12">
<div class="margin-top-20"></div>
		<h5 >Marks</h5>	
		<input type="text" name="twelvem" id="twelvem" class="form-control language" placeholder="Marks" value="">
        <input type="hidden" name="twelvehid" id="twelvehid" class="form-control language"  value="">
</div>

<?php } ?>
<!--<div>
<a class="button" href="javascript:void(0);" style="color:#fff">add certificate</a>
</div>-->

                                        <?php //$education_frm_tbl = $this->common_front_model->get_list('edu_list'); ?>
                                        <?php 
                                        if($education_count > 0 )
                                        {
                                            //$educationdetails = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$user_data['id'],'','data','multiple');
                                            $customcoount = 0;
											$customcoount_certi = 0;
											$customcoount_edu = 0;
											$customcoount_x = 0;
											$customcoount_xii = 0;
											//$educationdetails = sort($educationdetails);
										if(isset($educationdetails) && $educationdetails !='' && is_array($educationdetails) && count($educationdetails)> 0)
										{	
                                            foreach($educationdetails as $educationdetails_single)
                                            { 
											if($educationdetails_single['is_certificate_X_XII'] == 'X') 
											{ //for ten 
											
											$customcoount_x = $customcoount_x +1;
											if($customcoount_x == 1)
											{
												echo "<div class='col-xs-12 text-center' ><div class='margin-top-20'></div><h1 >Standard X Details</h1></div>";
											}
											?>
                                            <?php if($customcoount > 1) 
                                            { ?>
                                                <hr class="th_bordercolor" style="border-width: 4px;" >
                                            <?php 	}
											?>
												<div class="col-md-2 col-xs-12 clearfix">
                                                    <div class="margin-top-20"></div>
                                                    <h5> Standard - X :<!--<span class="red-only"> *</span>--></h5>
                                                </div>
<div class="col-md-4 col-xs-12">
	<div class="margin-top-20"></div>
    	<h5 >University/ Board</h5>
		<input type="text" name="tenthu" id="tenthu" class="form-control language" placeholder="Enter University/ Board" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['institute'])) ? $educationdetails_single['institute'] : ""; ?>">
</div>

<div class="col-md-4 col-xs-12">
<input type="hidden" name="tenthhid" id="tenthhid" class="form-control language" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['id'])) ? $educationdetails_single['id'] : ""; ?>">
<div class="margin-top-20"></div>
		<h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <!--<span class="red-only">*</span>--></h5>
                                               <select name="tenthy" id="tenthy" data-pasyrten="" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <?php 
                                                        $current_year = date('Y');
                                                        echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
                                                        for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?>
                                                             <option value="<?php echo $i; ?>"  <?php if($this->common_front_model->checkfieldnotnull($educationdetails_single['passing_year']) && $educationdetails_single['passing_year']==$i){ echo "selected"; } ?>><?php echo $i; ?></option>
                                                        <?php 	
                                                        } 
                                                        ?>
                                                    </select>
</div>
<div class="col-md-2 col-xs-12">
<div class="margin-top-20"></div>
		<h5 >Marks</h5>	
		<input type="text" name="tenthm" id="tenthm" class="form-control language" placeholder="Marks" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['marks'])) ? $educationdetails_single['marks'] : ""; ?>">
</div>
											<!--delete button starts-->
                                               <div class="col-xs-12">
                                               <div class="margin-top-20"></div>
                                                  <div class="input-group-btn text-center clearfix">
                                                  <button class="btn btn-danger deletepart" data-deletepart="edu" data-deleteid="<?php echo $educationdetails_single['id'];?>" data-deleteid_count="<?php echo $customcoount;?>" onClick="deletepart('edu','<?php echo $educationdetails_single['id'];?>','<?php echo $customcoount;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete Education</button>
                                                    <div class="margin-bottom-20"></div>
                                                  </div>
                                              </div>
                                              <!--delete button ends-->
										<?php 	}
										elseif($educationdetails_single['is_certificate_X_XII'] == 'XII') 
										{ //for twelve
										
										$customcoount_xii = $customcoount_xii + 1;
										if($customcoount_xii == 1)
										{
										echo "<div class='col-xs-12 text-center' ><div class='margin-top-20'></div><h1 >Standard XII Details</h1></div>";
										}
										 ?>
											<div class="clearfix"></div>
<div class="col-md-2 col-xs-12">
	<div class="margin-top-20"></div>
	<h5 >Standard - XII :<!--<span class="red-only"> *</span>--></h5>                                                        
</div>
<div class="col-md-4 col-xs-12">
	<div class="margin-top-20"></div>
    	<h5 >University/ Board</h5>
		<input type="text" name="twelveu" id="twelveu" class="form-control language" placeholder="Enter University/ Board" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['institute'])) ? $educationdetails_single['institute'] : ""; ?>">
</div>
<div class="col-md-4 col-xs-12">
<div class="margin-top-20"></div>
		<h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <!--<span class="red-only">*</span>--></h5>
                                               <select name="twelvey" id="twelvey" data-pasyrten="" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <?php 
                                                        $current_year = date('Y');
                                                        echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
                                                        for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?>
                                                             <option value="<?php echo $i; ?>" <?php if($this->common_front_model->checkfieldnotnull($educationdetails_single['passing_year']) && $educationdetails_single['passing_year']==$i){ echo "selected"; } ?>><?php echo $i; ?></option>
                                                        <?php 	
                                                        } 
                                                        ?>
                                                    </select>
</div>
<div class="col-md-2 col-xs-12">
<div class="margin-top-20"></div>
		<h5 >Marks</h5>	
		<input type="text" name="twelvem" id="twelvem" class="form-control language" placeholder="Marks" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['marks'])) ? $educationdetails_single['marks'] : ""; ?>">
        <input type="hidden" name="twelvehid" id="twelvehid" class="form-control language"  value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['id'])) ? $educationdetails_single['id'] : ""; ?>">
</div>
											<!--delete button starts-->
                                               <div class="col-xs-12">
                                               <div class="margin-top-20"></div>
                                                  <div class="input-group-btn text-center clearfix">
                                                  <button class="btn btn-danger deletepart" data-deletepart="edu" data-deleteid="<?php echo $educationdetails_single['id'];?>" data-deleteid_count="<?php echo $customcoount;?>" onClick="deletepart('edu','<?php echo $educationdetails_single['id'];?>','<?php echo $customcoount;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete Education</button>
                                                    <div class="margin-bottom-20"></div>
                                                  </div>
                                              </div>
                                              <!--delete button ends-->
										<?php }
										elseif($educationdetails_single['is_certificate_X_XII'] == 'Certi') 
										{$customcoount_certi = $customcoount_certi + 1;
										
											if($customcoount_certi == 1)
											{
												echo "<div class='col-xs-12 text-center' ><div class='margin-top-20'></div><h1 >Certificate details</h1></div>";	
											}
											
											 ?>
                                            <div class="certi_known_whole clearfix" data-certi_num="<?php echo $customcoount_certi; ?>">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="margin-top-20"></div>
                                                    <h5 >Certificate name</h5>
                                                    <input type="text" name="certiu_<?php echo $educationdetails_single['id']; ?>" id="certiu_<?php echo $educationdetails_single['id']; ?>" class="form-control language" placeholder="Enter Certificate name" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['institute'])) ? $educationdetails_single['institute'] : ""; ?>" data-namecerti="<?php echo $customcoount_certi; ?>">
                                            </div>
                                            <div class="col-md-4 col-xs-12">
<div class="margin-top-20"></div>
		<h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <span class="red-only">*</span></h5>
                                               <select name="certiy_<?php echo $educationdetails_single['id']; ?>" id="certiy_<?php echo $educationdetails_single['id']; ?>" data-pasyrcerti="<?php echo $customcoount_certi; ?>" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <?php 
                                                        $current_year = date('Y');
                                                        echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
                                                        for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?>
                                                             <option value="<?php echo $i; ?>" <?php if($this->common_front_model->checkfieldnotnull($educationdetails_single['passing_year']) && $educationdetails_single['passing_year']==$i){ echo "selected"; } ?>><?php echo $i; ?></option>
                                                        <?php 	
                                                        } 
                                                        ?>
                                                    </select>
</div>
											<div class="col-md-4 col-xs-12">
                                                <div class="margin-top-20"></div>
                                                    <h5 >Certificate Description</h5>
                                                    <input type="text" name="certid_<?php echo $educationdetails_single['id']; ?>" id="certid_<?php echo $educationdetails_single['id']; ?>" class="form-control language" placeholder="Enter Certificate description" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['specialization'])) ? $educationdetails_single['specialization'] : ""; ?>" data-certides="<?php echo $customcoount_certi; ?>">
                                            </div>

											<!--delete button certi starts-->
                                               <div class="col-xs-12">
                                               <div class="margin-top-20"></div>
                                                  <div class="input-group-btn text-center clearfix">
                                                  <button class="btn btn-danger deletepart" data-deletepart="edu" data-deleteid="<?php echo $educationdetails_single['id'];?>" data-deleteid_count="<?php echo $customcoount_certi;?>" onClick="deletepart('edu','<?php echo $educationdetails_single['id'];?>','<?php echo $customcoount;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete Certificate</button>
                                                    <div class="margin-bottom-20"></div>
                                                  </div>
                                              </div>
                                              <!--delete button certi ends-->
											</div>
										<?php }
						elseif($educationdetails_single['is_certificate_X_XII'] == 'Edu') 
						{//if
						
							$customcoount_edu = $customcoount_edu +1;
							$customcoount = $customcoount + 1;
							if($customcoount > 1) 
							{ ?>
								<hr class="th_bordercolor" style="border-width: 4px;" >
							<?php 	}//if for hr ends
							if($customcoount_edu == 1) 
							{
								echo "<div class='col-xs-12 text-center' ><div class='margin-top-20'></div><h1 >Educational Details</h1></div>";
							}
							?>
						<div class="panel-body edu_known_whole" style="padding:10px;" data-edu_num="<?php echo $customcoount;?>">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['qualification_lvl']; ?></label>
									<select name="qualification_level_<?php echo $educationdetails_single['id'] ; ?>" id="qualification_level_<?php echo $educationdetails_single['id'];?>" data-qual = "<?php echo $customcoount;?>" class="input-select-b select-drop-3 city" onChange="donotrepeatedu(this);">
										<?php if(isset($education_frm_tbl) && $education_frm_tbl!='' && ($educationdetails_single['qualification_level']=='' || is_null($educationdetails_single['qualification_level'])) )
										{ 
											print_r($education_frm_tbl); 
										}
										elseif(isset($education_frm_tbl) && $education_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($educationdetails_single['qualification_level']))
										{
											print_r($this->common_front_model->get_list('edu_list','str','','str',$educationdetails_single['qualification_level']));
										}
										else
										{ ?>
											<option value="">Please select your education</option>
										<?php } ?>
									</select>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['institute_name']; ?></label>
									<input type="text" name="institute_name_<?php echo $educationdetails_single['id'] ; ?>" id="institute_name_<?php echo $educationdetails_single['id'] ; ?>" data-ins = "<?php echo $customcoount;?>" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['institute_name']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['institute'])) ? $educationdetails_single['institute'] : ""; ?>"/>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['specialization']; ?></label>
									<input type="text" name="specialization_<?php echo $educationdetails_single['id'] ; ?>" id="specialization_<?php echo $educationdetails_single['id'] ; ?>" data-spez = "<?php echo $customcoount;?>" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['specialization']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['specialization'])) ? $educationdetails_single['specialization'] : ""; ?>"/>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['passing_year']; ?></label>
									
									<select name="passing_year_<?php echo $educationdetails_single['id'];?>" id="passing_year_<?php echo $educationdetails_single['id'];?>" data-pasyr="<?php echo $customcoount;?>" class="input-select-b select-drop-3">
										<?php 
										$current_year = date('Y');
										echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
										for($i = 1980; $i < $current_year ; $i++)
										{ 
										?>
												<option value="<?php echo $i; ?>" <?php if($this->common_front_model->checkfieldnotnull($educationdetails_single['passing_year']) && $educationdetails_single['passing_year']==$i){ echo "selected"; } ?>><?php echo $i; ?></option>
										<?php 	
										} 
										?>
									</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['percentage']; ?></label>
									
									<input type="text" name="percentage_<?php echo $educationdetails_single['id'] ; ?>" id="percentage_<?php echo $educationdetails_single['id'] ; ?>" data-perc = "<?php echo $customcoount;?>" class="edit-input""  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['percentage']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($educationdetails_single['marks'])) ? $educationdetails_single['marks'] : ""; ?>"/>
								</div>
								
							</div>
							<!--delete button starts-->
							<div class="col-xs-12">
								<div class="margin-top-20"></div>
								<div class="input-group-btn text-center clearfix">
									<button class="btn btn-danger deletepart" data-deletepart="edu" data-deleteid="<?php echo $educationdetails_single['id'];?>" data-deleteid_count="<?php echo $customcoount;?>" onClick="deletepart('edu','<?php echo $educationdetails_single['id'];?>','<?php echo $customcoount;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete Education</button>
									<div class="margin-bottom-20"></div>
								</div>
							</div>
							<!--delete button ends-->
						</div><!--whole div ends-->  
						<?php 
											}//if it is education
                                        }
											
										}//foreach ens
                                         ?>
                                        <div class="clear"></div>
                                                <div class="margin-top-10"></div>
                                                <?php 
                                                if(isset($education_count) && $education_count < 11){
                                                ?>
                                                <div class="input-group-btn text-center">
                                                        <button class="btn btn-success" type="button"  id="addmorebutton_edu"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add More Education</button>
                                                    <div class="margin-bottom-20"></div>
                                                </div>        
                                                <?php }
												if($this->my_profile_model->checkif_certi_added() < 11)
												{
												 ?>
                                                <div class="col-xs-12 ">
                                                    <div class="input-group-btn text-center">
                                                            <button class="btn btn-success" type="button"  id="addmorebutton_certi"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add Certificates</button>
                                                            <div class="margin-bottom-20"></div>
                                                    </div>
                                                </div>
                                                 <?php } ?>
                                                 <div class="row margin-top-20" style="margin-bottom:0px;">
													<div class="col-md-12 col-xs-12 col-sm-12">
														<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
														<!-- <button type="button" onClick="viewsection('education_view');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php //echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
													</div>
												</div>
                                                
                                        <?php 
                                        }
                                        elseif((!($education_count > 0)) || 
										(
											($education_count > 0) &&
											($this->my_profile_model->checkifadded('12')===false || $this->my_profile_model->checkifadded('10')===false || $this->my_profile_model->checkifadded('Certi')===false)
										)
										)
                                        {
                                            //$educationdetails = $this->my_profile_model->getdetailsfromid('jobseeker_education','js_id',$user_data['id'],'','data','single');
											echo "<div class='col-xs-12 text-center' ><div class='margin-top-20'></div><h1 >Educational Details</h1></div>";
                                         ?>
										 <div class="panel-body edu_known_whole" style="padding:10px;" data-edu_num="1">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['qualification_lvl']; ?></label>
									<select name="qualification_level_a_1" id="qualification_level_a_1" data-qual = "1" class="input-select-b select-drop-3 city" onChange="donotrepeatedu(this);">
										<?php if(isset($education_frm_tbl) && $education_frm_tbl!='')
										{ 
											print_r($education_frm_tbl); 
										}
										else
										{ ?>
											<option value="">Please select your education</option>
										<?php } ?>
									</select>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['institute_name']; ?></label>
									<input type="text" name="institute_name_a_1" id="institute_name_a_1" data-ins = "1" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['institute_name']; ?>" value=""/>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['specialization']; ?></label>
									<input type="text" name="specialization_a_1" id="specialization_a_1" data-spez = "1" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['specialization']; ?>" value=""/>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['passing_year']; ?></label>
									
									<select name="passing_year_a_1" id="passing_year_a_1" data-pasyr="<?php echo $customcoount;?>" class="input-select-b select-drop-3">
										<?php 
										$current_year = date('Y');
										echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
										for($i = 1980; $i < $current_year ; $i++)
										{ 
										?>
												<option value="<?php echo $i; ?>" <?php /*if($this->common_front_model->checkfieldnotnull($educationdetails_single['passing_year']) && $educationdetails_single['passing_year']==$i){ echo "selected"; } */?>><?php echo $i; ?></option>
										<?php 	
										} 
										?>
									</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['percentage']; ?></label>
									
									<input type="text" name="percentage_a_1" id="percentage_a_1" data-perc = "1" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['percentage']; ?>" value=""/>
								</div>
								
							</div>
							
						</div><!--whole div ends-->  
						<div class="clear"></div>
						<div class="margin-top-10"></div>
						<?php 
						if($education_count < 11){
						?>
						<div class="input-group-btn text-center">
							<button class="btn btn-success" type="button"  id="addmorebutton_edu"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add More Education</button>
							<div class="margin-bottom-20"></div>
						</div>        
						<?php }
						if($this->my_profile_model->checkif_certi_added() < 11)
						{
							?>
							<div class="col-xs-12 " ><!--data-certi_num="1"--><!--certi_known_whole-->
							<div class="input-group-btn text-center">
									<button class="btn btn-success" type="button"  id="addmorebutton_certi"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add Certificates</button>
								<div class="margin-bottom-20"></div>
							</div>
						</div>
							<?php } ?>
						<div class="row margin-top-20" style="margin-bottom:0px;">
							<div class="col-md-12 col-xs-12 col-sm-12">
								<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
								<!-- <button type="button" onClick="viewsection('education_view');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php //echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
							</div>
						</div>
						<?php 
                                        }
										
                                        ?>
						<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
						</form>