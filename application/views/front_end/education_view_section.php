<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid ;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
								<div class="panel-heading panel-bg" style="color:#ffffff;border-bottom:4px solid"><!--#D9534F--><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><!--background-color:#D9534F;background:none;--><span class="glyphicon glyphicon-education"></span> <?php echo $custom_lable_arr['edu_qualification_lbl']?> </span> <a href="javascript:void(0)" class="btn btn-md pull-right foreditfontcolor" style="margin:-3px;" onClick="editsection('education_view');"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_Edit']?></a></div>
								<div class="panel-body" style="padding:10px;" id="defineheight">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
                                        <?php 
										//$education_details = $this->my_profile_model->geteducationfromid($user_data['id']);
										
										$education_details = $this->my_profile_model->getdetailsfromid('js_education_view','js_id',$user_data['id'],'',$dataorcount='data',$records='multiple');
										if(isset($education_details) && $education_details!='' && is_array($education_details) && count($education_details) > 0)
										{
										$countloop = 0;	 
										$customcoount_certi = 0;
										$customcoount_edu = 0;
										$customcoount_x = 0;
										$customcoount_xii = 0;
										//$education_details = sort($education_details);
										foreach($education_details as $education_details_single)
										{	$countloop = $countloop+1;
										 ?>
                                         
                                            
											<table class="table" id="edutbid<?php echo $countloop; ?>" <?php if($countloop > 1){/*echo "style='display:none'";*/} ?> >
												<tbody>
                                                	<?php 
													if($education_details_single['is_certificate_X_XII'] == 'X') 
													{$customcoount_x = $customcoount_x + 1;
													
													?>
                                                    <?php if($countloop > 1){  ?>
                                            			<tr><hr class="th_bordercolor" style="border-width:4px" id="eduhrid<?php echo $countloop; ?>"></tr>
													<?php } ?><!--;display:none-->
                                                    <?php if($customcoount_x == 1){  ?>
                                                    <tr>
														<!--<td class="col-md-12 col-xs-12" style="border-top:none;">--><h4 class="text-center"> <u>Standard - X</u> </h4><!--</td>-->
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;">Qualification Level:</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;">
														<?php 
														if($education_details_single['qualification_level']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['educational_qualification']))
														{
															echo $education_details_single['educational_qualification'];
														}
														elseif($education_details_single['qualification_level']=='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['is_certificate_X_XII']))
														{
															if($education_details_single['is_certificate_X_XII']=='XII')
															{
																echo "12 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='X')
															{
																echo "10 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='Certi')
															{
																echo "Certificate course";
															}
															else
															{
																echo $custom_lable_arr['notavilablevar']; 
															}
														}
														else
														{
															echo $custom_lable_arr['notavilablevar']; 
														}
														?>
                                                        </td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">University / Institute:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['institute']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['institute'])) ? $education_details_single['institute'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php if($education_details_single['is_certificate_X_XII']!='X' && $education_details_single['is_certificate_X_XII']!='XII'){?>
													<tr>
														<td class="col-md-6 col-xs-6">Specialization:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['specialization']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['specialization'])) ? $education_details_single['specialization'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6">Passing Year:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['passing_year']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['passing_year'])) ? $education_details_single['passing_year'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Grade / percentage:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['marks']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['marks'])) ? $education_details_single['marks'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php /* tenth section ends*/}
													if($education_details_single['is_certificate_X_XII'] == 'XII') 
													{
														$customcoount_xii = $customcoount_xii +1;
													 ?>
                                                    <?php if($countloop > 1){  ?>
                                            			<tr><hr class="th_bordercolor" style="border-width:4px" id="eduhrid<?php echo $countloop; ?>"></tr>
													<?php } ?><!--;display:none-->
                                                    <?php if($customcoount_xii == 1){  ?>
                                                    <tr>
														<!--<td class="col-md-12 col-xs-12" style="border-top:none;">--><h4 class="text-center"> <u>Standard XII Details</u> </h4><!--</td>-->
													</tr>
                                                    <?php }  ?>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;">Qualification Level:</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;">
														<?php 
														if($education_details_single['qualification_level']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['educational_qualification']))
														{
															echo $education_details_single['educational_qualification'];
														}
														elseif($education_details_single['qualification_level']=='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['is_certificate_X_XII']))
														{
															if($education_details_single['is_certificate_X_XII']=='XII')
															{
																echo "12 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='X')
															{
																echo "10 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='Certi')
															{
																echo "Certificate course";
															}
															else
															{
																echo $custom_lable_arr['notavilablevar']; 
															}
														}
														else
														{
															echo $custom_lable_arr['notavilablevar']; 
														}
														?>
                                                        </td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">University / Institute:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['institute']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['institute'])) ? $education_details_single['institute'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
													
													<tr>
														<td class="col-md-6 col-xs-6">Passing Year:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['passing_year']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['passing_year'])) ? $education_details_single['passing_year'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Grade / percentage:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['marks']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['marks'])) ? $education_details_single['marks'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php /* twelve section ends*/}
													if($education_details_single['is_certificate_X_XII'] == 'Certi'){ 
													
													$customcoount_certi = $customcoount_certi + 1; ?>
                                                    <?php if($countloop > 1){  ?>
                                            			<tr><hr class="th_bordercolor" style="border-width:4px" id="eduhrid<?php echo $countloop; ?>"></tr>
													<?php } ?><!--;display:none-->
                                                    <?php if($customcoount_certi == 1)
													{?>
                                                    <tr>
														<!--<td class="col-md-12 col-xs-12" style="border-top:none;">--><h4 class="text-center"> <u>Certificate details</u> </h4><!--</td>-->
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6">University / Institute:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['institute']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['institute'])) ? $education_details_single['institute'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php if($education_details_single['is_certificate_X_XII']!='X' || $education_details_single['is_certificate_X_XII']!='XII'){?>
													<tr>
														<td class="col-md-6 col-xs-6">Specialization:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['specialization']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['specialization'])) ? $education_details_single['specialization'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6">Passing Year:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['passing_year']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['passing_year'])) ? $education_details_single['passing_year'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <!--<tr>
														<td class="col-md-6 col-xs-6">Grade / percentage:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['marks']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['marks'])) ? $education_details_single['marks'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
                                                    <?php /* certi section ends*/}
													if($education_details_single['is_certificate_X_XII'] == 'Edu') {
														
														$customcoount_edu =$customcoount_edu+1;
														 ?>
                                                    <?php if($countloop > 1){  ?>
                                            			<tr><hr class="th_bordercolor" style="border-width:4px" id="eduhrid<?php echo $countloop; ?>"></tr>
													<?php } ?><!--;display:none-->
                                                    <?php if($customcoount_edu == 1)
													{ ?>
                                                    <tr>
														<!--<td class="col-md-12 col-xs-12" style="border-top:none;">--><h4 class="text-center"> <u>Educational Details</u> </h4><!--</td>-->
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;">Qualification Level:</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;">
														<?php 
														if($education_details_single['qualification_level']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['educational_qualification']))
														{
															echo $education_details_single['educational_qualification'];
														}
														elseif($education_details_single['qualification_level']=='0' && $this->common_front_model->checkfieldnotnull($education_details_single['qualification_level']) && $this->common_front_model->checkfieldnotnull($education_details_single['is_certificate_X_XII']))
														{
															if($education_details_single['is_certificate_X_XII']=='XII')
															{
																echo "12 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='X')
															{
																echo "10 th";
															}
															elseif($education_details_single['is_certificate_X_XII']=='Certi')
															{
																echo "Certificate course";
															}
															else
															{
																echo $custom_lable_arr['notavilablevar']; 
															}
														}
														else
														{
															echo $custom_lable_arr['notavilablevar']; 
														}
														?>
                                                        </td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">University / Institute:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['institute']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['institute'])) ? $education_details_single['institute'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php if($education_details_single['is_certificate_X_XII']!='X' && $education_details_single['is_certificate_X_XII']!='XII'){?>
													<tr>
														<td class="col-md-6 col-xs-6">Specialization:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['specialization']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['specialization'])) ? $education_details_single['specialization'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php } ?>
													<tr>
														<td class="col-md-6 col-xs-6">Passing Year:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['passing_year']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['passing_year'])) ? $education_details_single['passing_year'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Grade / percentage:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($education_details_single['marks']!='0' && $this->common_front_model->checkfieldnotnull($education_details_single['marks'])) ? $education_details_single['marks'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
                                                    <?php } ?>
                                                    
												</tbody>
											</table>
                                            
                                         <?php 
										}//foreach end 
											if(count($education_details) > 1)
											{
										?>
                                        <!--<div class="input-group-btn text-center">
                                        	<a class="button" id="viewmoreedu" onClick="viewmoreedu()"> View More</a>
                                        </div>-->
										<?php 
											}
										}else{ ?>
                                         <div class="alert alert-info">
                                         	<?php echo $custom_lable_arr['myprofile_no_edu_msg']; ?>
                                         </div>
                                         <?php } ?>
										</div>
									</div>
								</div>
							</div>