<?php // Merchant key here as provided by Payu
	//print_r($plan_data);
	$table_name = 'jobseeker';
	if($plan_data['user_type'] =='employer')
	{
		$table_name = 'employer_master';
	}
	$user_data = $this->common_front_model->get_logged_user_typeid();
	$user_type = $user_data['user_type'];
	$user_id = $user_data['user_id'];
	$user_data = $this->common_front_model->get_count_data_manual($table_name,array('status'=>'APPROVED','id'=>$user_id),1,' email,fullname, mobile ','',0,'',0);
	$plan_data_array = $plan_data['plan_data_array'];
	
	$total = $plan_data['total_pay'];
	
	$cust_name = $user_data['fullname'];
	$cust_email = $user_data['email'];
	$cust_number = '';
	if($user_data['mobile'] !='')
	{
		$mo_arr = explode('-',$user_data['mobile']);
		$cust_number = $mo_arr[1];
	}	
	
	$order_id = 'Membership Plan - '.$plan_data_array['plan_name'];
	
	$_POST['amount'] = $total;
	$_POST['firstname'] = $cust_name;
	$_POST['email'] = $cust_email;
	$_POST['phone'] = $cust_number;
	$_POST['productinfo'] = $order_id;
	$session_id = base64_encode(session_id());
	
	$_POST['surl'] = $base_url.'my-plan/payment-success/'.$session_id;
	$_POST['furl'] = $base_url.'my-plan/payment-cancel/'.$session_id;


$MERCHANT_KEY = $this->my_plan_model->marchent_key;	// update key and salt here for live
$SALT = $this->my_plan_model->salt;
$PAYU_BASE_URL = $this->my_plan_model->pay_url;
$action = '';
$posted = array();

$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
$_POST['txnid'] = $txnid;
$_POST['key'] = $MERCHANT_KEY;
$_POST['service_provider'] = 'payu_paisa';


if(!empty($_POST))
{
	//print_r($_POST);
  	foreach($_POST as $key => $value)
	{
    	$posted[$key] = $value;
  	}
}
$formError = 0;

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0)
{
	if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<form action="<?php echo $action;?>" method="post" name="frmpayumoney" id="frmpayumoney">
    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
    <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
    
    <input type="hidden" name="productinfo" value="<?php echo $order_id; ?>" />
    <input type="hidden" name="amount" value="<?php echo $_POST['amount'];?>">
    <input type="hidden" name="firstname" value="<?php echo $_POST['firstname'];?>">
    <input type="hidden" name="phone" value="<?php echo $_POST['phone']; ?>" />
    <input type="hidden" name="email" value="<?php echo $_POST['email']; ?>" />
    <input type="hidden" name="service_provider" value="payu_paisa" size="64"/>
    
    <input type="hidden" name="surl" value="<?php echo $_POST['surl'];?>"/>
    
    <input type="hidden" name="furl" value="<?php echo $_POST['furl'];?>"/>
    
</form>
<script language='javascript'>document.frmpayumoney.submit();</script>
