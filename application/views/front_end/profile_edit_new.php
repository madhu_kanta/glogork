<style>
	.tab-pane {
	background: transparent;
    box-shadow:none;
	}
	.panel-heading.panel-bg, ui-menu .ui-menu-item a:hover, table.manage-table.resumes th:first-child {
    background-color: #ff3434;
    color: white;
	}
	
</style>
<?php $custom_lable_arr = $custom_lable->language; ?>
<?php  $upload_path_profile ='./assets/js_photos';
$mobile = ($this->common_front_model->checkfieldnotnull($user_data['mobile'])) ? explode("-",$user_data['mobile']) : "";
$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode(".",$user_data['total_experience']) : "";
$annual_salary = ($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? explode(".",$user_data['annual_salary']) : "";
$annual_salary_exp = ($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) ? explode(".",$user_data['expected_annual_salary']) : "";
/*$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode("-",$user_data['total_experience']) : "";
$annual_salary = ($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? explode("-",$user_data['annual_salary']) : "";
$annual_salary_exp = ($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) ? explode("-",$user_data['expected_annual_salary']) : "";*/
$pass_var_progress['custom_lable_arr'] = $custom_lable_arr;
$pass_var_progress['user_data_values'] = $user_data;
$pass_var_personal_view['user_data_values'] = $user_data;
$pass_var_personal_view['custom_lable_arr'] = $custom_lable_arr;
$pass_var_personal_view['blah2'] = 'blah2';
 ?>
			<!-- <div id="progressbarview_edit">
                    <?php //$this->load->view('front_end/progressbar_view',$pass_var_progress); ?>
				</div>	 -->
				<div class="container-fluid" style="padding:0px;">
					<!-- <div class="row">
						<div class="col-sm-12">
							<h2 class="register"><u><?php echo $custom_lable_arr['js_edit_title']; ?></u></h2>
						</div>
					</div> -->
					
					
                    <div class="clearfix"></div>
                    <div class="tabs-left"></div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
							<?php $this->load->view('front_end/job_seeker_left_menu',$pass_var_personal_view); ?>
							<?php // include_once("job_seeker_left_menu.php",$pass_var_personal_view); ?>
						</div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 tab-content clearfix">
                        <div id="accordion-container">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile"> 
						<div class="panel-heading panel-bg bg-new-p"><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-user"></span> Personal Details </span> </div>
						
						<div class="panel-body" style="padding:10px;">
                        <form method="post" name="editpersonaldet" id="editpersonaldet" action="<?php echo $base_url.'my_profile/editpersonaldet'; ?>" >
						<div class="alert alert-danger" id="message2" style="display:none" ></div>
						<div class="alert alert-success" id="success_msg2" style="display:none" ></div>
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['sign_up_emp_1stformtitle']; ?>* </label>
									<select  class="input-select-b select-drop-3" name="title" id="title" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformtitlevalmes']; ?>" style="background-position-x:97% !important;">
															<?php
                                                            if($pers_title != '' && is_array($pers_title) && count($pers_title) > 0)
                                                            {
                                                                foreach ($pers_title as $pers_title_single)
                                                                {
																	$gn_class = '';
																	if(strtolower(trim($pers_title_single['personal_titles'])) == 'mr.' || strtolower(trim($pers_title_single['personal_titles'])) == 'mr')
																	{
																		$gn_class = 'gn_classmale';
																	}
																	else if(strtolower(trim($pers_title_single['personal_titles'])) == 'ms.' || strtolower(trim($pers_title_single['personal_titles'])) == 'ms' || strtolower(trim($pers_title_single['personal_titles'])) == 'mrs' || strtolower(trim($pers_title_single['personal_titles'])) == 'mrs.' || strtolower(trim($pers_title_single['personal_titles'])) == "ma'am" || strtolower(trim($pers_title_single['personal_titles'])) == "ma'am.")
																	{
																		$gn_class = 'gn_classfemale';
																	}
																	 ?>
                                                                    <option value="<?php echo $pers_title_single['id'];?>" <?php if($user_data!='' && $pers_title_single['id']==$user_data['title'] ){ echo "selected" ; } ?> class="<?php echo $gn_class; ?>"  ><?php echo $pers_title_single['personal_titles'];?> </option>	
                                                                <?php }
                                                            }
                                                            ?>
                                                            </select>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['reg_lbl_fullname']; ?>* </label>
									<input type="text" class="edit-input" placeholder="Enter Full Name" id="fullname" name="fullname" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ? $user_data['fullname'] : "" ; ?>">
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Date of Birth:</label>
                                    <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <input type="text" class="edit-input" id="birthdate" name="birthdate" placeholder="Date of Birth" onkeydown="return false" style="padding:9px 0 9px 10px;border-radius:0;" value="<?php echo ($user_data['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($user_data['birthdate'])) ? $user_data['birthdate'] : ""; ?>"  ><!-- onkeydown="return false" readonly='true'-->
                                </div>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['gender_lbl']; ?></label>
									<select class="input-select-b select-drop-3" name="gender" style="background-position-x: 97% !important;">
										<option value="Male" <?php if($user_data['gender']=='Male') {echo "selected";} ?>>Male</option>
										<option value="Female" <?php if($user_data['gender']=='Female') {echo "checked";} ?>>Female</option>
										
									</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['m_status_lbl']; ?></label>
									<select class="input-select-b select-drop-3" name="marital_status" style="background-position-x:97% !important;">
                                    <option value="">Select <?php echo $custom_lable_arr['m_status_lbl']; ?></option>
                                    <?php 
                                        if(isset($marital_status) && is_array($marital_status) && count($marital_status) > 0)
                                        {
                                        foreach ($marital_status as $m_status)
                                        {   ?>
                                            <option value="<?php echo $m_status['id'];?>" <?php if($m_status['id']==$user_data['marital_status']) {echo "selected";} ?>><?php echo $m_status['marital_status'];?></option>
                                            
                                            <?php }
                                        }
                                        ?>
                                       
										
									</select>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['mobile_lbl']; ?></label>
                                    <div class="row" style="margin-bottom:0px;">
										<div class="col-md-4 col-xs-4 col-sm-4">
                                        <select  class="input-select-b select-drop-3" name="mobile_c_code" id="mobile_c_code" style="height:42px;" data-validation="required">
                                        <?php
                                        if(isset($country_code) && $country_code !='' && is_array($country_code) && count($country_code) > 0)
                                        {
                                        foreach ($country_code as $code)
                                        { ?>
                                            <option value="<?php echo $code['country_code'];?>" <?php if($mobile!='' && isset($mobile[0]) && $mobile[0]==$code['country_code'] ){ echo "selected" ; } ?>><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
                                        <?php }
                                        }
                                        ?>
                                    </select>
										</div>
										<div class="col-md-8 col-xs-8 col-sm-8">
                                            <input type="text" class="edit-input" id="mobile" name="mobile"  placeholder="Ex.0987654321" data-validation="required,number,length"  data-validation-length="10-13" value="<?php if($mobile!='' && isset($mobile[1])){ echo $mobile[1] ; } ?>">
											<input type="hidden" name="mobile_num_old" value="<?php if(isset($user_data['mobile']) && $user_data['mobile']!=''){ echo $user_data['mobile']; } ?>" />
										</div>
                                    </div>
                                    
									
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['country_lbl']; ?></label>
                                    <select name="country" id="country" class="input-select-b select-drop-3" style="background-position-x:97% !important;" data-validation-skipped='0' onChange="dropdownChange('country','city','city_list');" data-validation="required">
                                                        <?php if($this->common_front_model->checkfieldnotnull($user_data['country'])) { echo  $this->common_front_model->get_list('country_list','str','','str',$user_data['country']);}else{ echo $this->common_front_model->get_list('country_list','str'); } ?>
														</select>
												
									<!-- <select class="input-select-b select-drop-3" name="ne" style="background-position-x:97% !important;">
										<option value="">India</option>
										<option value="">Canada</option>
										
									</select> -->
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['city_lbl']; ?></label>
                                    <select name="city" id="city" class="input-select-b select-drop-3" data-validation="required" style="background-position-x:97% !important;" placeholder="Select City Name">
                                    <option value="">First select country</option>
                                                <option value=""><?php echo $custom_lable_arr['sign_up_emp_1stformcitydfltopt']; ?></option>
                                               
												</select>
                                                
									
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['address_lbl']; ?></label>
									<!-- <input type="text" class="edit-input" placeholder="A/502, Siddhivinayak Tower, Near Kataria Automobile, Behind BMW Show Room, Sarkhej"> -->
                                    <textarea class="edit-input" name="address" id="address" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['address_lbl'];  ?>" style="padding:9px 0 9px 10px;border-radius:0;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['address'])) ? $user_data['address'] : ""; ?></textarea>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['landline']; ?></label>
									<input type="text" class="edit-input" id="landline" name="landline" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['landline'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['landline'])) ? $user_data['landline'] : ""; ?>" placeholder="Not Available">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['pincode']; ?></label>
                                    <input type="text" class="edit-input" name="pincode" id="pincode" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['pincode'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['pincode'])) ? $user_data['pincode'] : ""; ?>" >
									<!-- <input type="text" class="edit-input" placeholder="383110"> -->
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['home_city']; ?>*</label>
									<input type="text" class="edit-input" name="home_city" id="home_city" style="border-radius:0px;" data-validation="required" placeholder="Add Home City By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['home_city'])) ? $user_data['home_city'] : ""; ?>" style="padding:9px 0 9px 10px;border-radius:0;">
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['resume_headline']; ?>*</label>
									<input type="text" class="edit-input" id="resume_headline" name="resume_headline" placeholder="xyz Developer" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['resume_headline'])) ? $user_data['resume_headline'] : ""; ?>" style="padding:9px 0 9px 10px;border-radius:0;">
								</div>
								
							</div>
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
								
                                <input type="hidden" name="csrf_job_portal" id="csrf_job_portal" value="<?php echo $this->security->get_csrf_hash();?>"/>
								<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                <button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
								<!-- <button type="button" onClick="viewsection('profile_snapshot');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php //echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
									<!-- <a href="#" class="btn-job-new block new-save"> Save Settings</a> -->
								</div>
							</div>
                            </form>
							<!--<div class="row margin-top-0" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12 margin-top-5">
								<div class="checkbox-new">
								<label class="checkbox-new">
								<input type="checkbox">
								<span class="indicator"></span>
								You accepts our <a href="" class="a-color">Terms and Conditions</a> and <a href="" class="a-color">Privacy Policy</a>
								</label>
								</div>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>

			<!-- Other-Details -->
			<div id="other_detailseditdivscroll">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile"> 
						<div class="panel-heading panel-bg bg-new-p"><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-user"></span> Other Details </span> </div>
						<form method="post" name="editotherdet" id="editotherdet" action="<?php echo $base_url.'my_profile/editpersonaldet'; ?>" >
						<div class="alert alert-danger" id="messageother" style="display:none" ></div>
						<div class="alert alert-success" id="success_msgother" style="display:none" ></div>
						<div class="panel-body" style="padding:10px;">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new">Profile Summary</label>
									<textarea class="edit-input" name="profile_summary" id="profile_summary" style="padding:9px 0 9px 10px;border-radius:0;" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['profile_summary'];  ?>"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['profile_summary'])) ? $user_data['profile_summary'] : ""; ?></textarea>
									
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Preferred City</label>
									<select name="preferred_city[]" id="preferred_city" class="input-select-b select-drop-3" style="width:100%;"  multiple data-placeholder="<?php echo $custom_lable_arr['select'] .' '. $custom_lable_arr['preferred_city'];  ?>" data-validation="required">
                                        <?php
										if($user_data['preferred_city']!='')
										{ 
											$city = $user_data['preferred_city'];
											$where_arra_city = "id IN (".$city.")";
											$city_list = $this->common_front_model->get_count_data_manual('city_master',$where_arra_city,2,'city_name,id');
											if(isset($city_list) && $city_list !='' && is_array($city_list) && count($city_list) > 0)
											{
											//echo $this->db->last_query();
											foreach($city_list as $city)
											{ ?>
                                            	<option value="<?php echo $city['id']; ?>" selected><?php echo $city['city_name']; ?></option>
											<?php }
											}
											?>
                                        	
										<?php }
										?>
                                                     
                                                      </select>
									<!-- <input type="text" class="edit-input" placeholder="Ahmedabad, Surat, Bhavnagar, Rajkot, Vadodara"> -->
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Preferred Shift</label>
									<input type="text" name="prefered_shift" class="form-control" id="prefered_shift" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['prefered_shift'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['prefered_shift'])) ? $user_data['prefered_shift'] : ""; ?>" >
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['website']; ?></label>
									<input type="text" class="edit-input" name="website" id="website" style="border-radius:0px;"  placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['website'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['website'])) ? $user_data['website'] : ""; ?>" >
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Total Experience</label>
									<div class="row" style="margin-bottom:0px;">
										<div class="col-md-6 col-xs-6 col-sm-6">
											<select name="exp_year" id="exp_year" data-validation="required" class="input-select-b select-drop-3" onChange="expchange();">
                                                        <option value=""><?php echo $custom_lable_arr['year']; ?></option>
                                                        <?php
                                                        for($i=0;$i <= 25 ; $i++)
                                                        { ?>
                                                            <option value="<?php echo  $i; ?>" <?php if($totalexp!='' && isset($totalexp[0]) && $totalexp[0]==$i ){ echo "selected" ; } ?>><?php echo  $i; ?> </option>
                                                    <?php	}
                                                        ?>
                                            </select>
										</div>
										<div class="col-md-6 col-xs-6 col-sm-6">
											<select name="exp_month" id="exp_month" data-validation="required" class="input-select-b select-drop-3" onChange="expchange();">
                                                    <option value=""><?php echo $custom_lable_arr['month']; ?></option>
                                                    <?php
                                                    for($i=0;$i <= 11 ; $i++)
                                                    { ?>
                                                    <option value="<?php echo  $i; ?>" <?php if($totalexp!='' && isset($totalexp[1]) && $totalexp[1]==$i ){ echo "selected" ; } ?>><?php echo  $i; ?> </option>
                                                <?php	}
                                                    ?>
                                            </select>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['current_annual_salary']; ?></label>
										<select name="annual_salary" id="annual_salary" data-validation="required" class="input-select-b select-drop-3">
														<?php if($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) { echo  $this->common_front_model->get_list('salary_range_list','str','','str',$user_data['annual_salary']);}else{ echo $this->common_front_model->get_list('salary_range_list','str'); } ?>
										</select>
									<!-- <input type="text" class="edit-input" placeholder="INR 17 Lacs 95 Thousands"> -->
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['type_industry']; ?>*</label>
									<select name="industry" id="industry" data-validation="required" class="input-select-b select-drop-3">
                                            <?php if($this->common_front_model->checkfieldnotnull($user_data['industry'])) { echo  $this->common_front_model->get_list('industries_master','str','','str',$user_data['industry']);}else{ echo $this->common_front_model->get_list('industries_master','str'); } ?>
                                        </select>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['functional_area']; ?></label>
									<select name="functional_area" id="functional_area" data-validation="required" class="input-select-b select-drop-3" onChange="dropdownChange('functional_area','job_role','role_master');">
                                         <?php /*if($this->common_front_model->checkfieldnotnull($user_data['functional_area'])) { echo  $this->common_front_model->get_list('functional_area_master','str','','str',$user_data['functional_area']);}else{ echo $this->common_front_model->get_list('functional_area_master','str'); }*/ ?>
                                         <?php if(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && ($user_data['functional_area']=='' || is_null($user_data['functional_area'])) )
                                                    { 
                                                        print_r($fnarea_frm_tbl); 
                                                    }
                                                    elseif(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($user_data['functional_area']))
                                                    {
                                                        print_r($this->common_front_model->get_list('functional_area_master','str','','str',$user_data['functional_area']));
                                                    }
                                                    else
                                                    { ?>
                                                        <option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['functional_area']; ?></option>
                                                    <?php } ?>
                                    </select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['job_role']; ?></label>
									<select name="job_role" id="job_role" data-validation="required" class="input-select-b select-drop-3">
                                            <?php /*if($this->common_front_model->checkfieldnotnull($user_data['job_role'])) { echo  $this->common_front_model->get_list('role_master','str','','str',$user_data['job_role']);}else{ echo $this->common_front_model->get_list('role_master','str'); }*/ ?>
                                            <?php if(($user_data['job_role']=='' || is_null($user_data['job_role'])) && $this->common_front_model->checkfieldnotnull($user_data['functional_area']))
                                                    { 
                                                        print_r($this->common_front_model->get_list('role_master','str',$user_data['functional_area'],'str',''));
                                                    }
                                                    elseif($this->common_front_model->checkfieldnotnull($user_data['functional_area']) && $this->common_front_model->checkfieldnotnull($user_data['job_role']))
                                                    {
                                                        print_r($this->common_front_model->get_list('role_master','str',$user_data['functional_area'],'str',$user_data['job_role']));
                                                    }
                                                    else
                                                    { ?>
                                                        <option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['job_role']; ?></option>
                                                    <?php } ?>
                                        </select>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['skill']; ?></label>
									<input type="text" name="key_skill" class="edit-input" id="key_skill" style="border-radius:0px;" data-validation="required"  placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['skill'];  ?>Find Specify Skill By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['key_skill'])) ? $user_data['key_skill'] : ""; ?>">
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['desire_job_type']; ?></label>
									<input type="text" name="desire_job_type" class="form-control" id="desire_job_type" style="border-radius:0px;" data-validation="required"   placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['desire_job_type'];  ?>Find Specify Desire Job Type By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['desire_job_type'])) ? $user_data['desire_job_type'] : ""; ?>">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['employment_type']; ?></label>
									<input type="text" name="employment_type" class="form-control" id="employment_type" style="border-radius:0px;" data-validation="required" placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['employment_type'];  ?>Find Specify Employment Type By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['employment_type'])) ? $user_data['employment_type'] : ""; ?>">
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['expected_annual_salary'] ?></label>
									<select name="expected_annual_salary" id="expected_annual_salary" data-validation="required" class="input-select-b select-drop-3">
										<?php if($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) { echo  $this->common_front_model->get_list('salary_range_list','str','','str',$user_data['expected_annual_salary']);}else{ echo $this->common_front_model->get_list('salary_range_list','str'); } ?>
									</select>
								</div>
								
							</div>
							<!-- <div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new">Description</label>
									<span style="color:red;">Ck editor Add In This Section</span>
								</div>
								
							</div>
							 -->
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<!-- <a href="#" class="btn-job-new block new-save"> Save Settings</a> -->
									<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/> 
									<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
													<!-- <button type="button" onClick="viewsection('other_details');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
								</div>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
			<!-- Other-Details -->

			<!--  Social Media Links start -->
			<div id="social_linkseditdivscroll">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile"> 
						<div class="panel-heading panel-bg bg-new-p" id="social_linkseditdiv"><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['myprofile_social_med_tit']; ?> </span> </div>
						<?php $this->load->view('front_end/soaial_link_edit_section'); ?>
					</div>
				</div>
			</div>
			<!--  Social Media Links end -->

			<!--  Work history section starts -->
			<div id="workdetails_vieweditdivscroll">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile" id="workwholeid"> 
						
							<?php $this->load->view('front_end/work_section'); ?>
					</div>
				</div>
			</div>
			<!-- Work history section ends -->

			<!-- education-details start-->
			<div id="education_vieweditdivscroll">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile" id="educationwholeid"> 
						<?php $this->load->view('front_end/education_section'); ?>
					</div>
				</div>
			</div>
			<!-- education-details end-->

			<!-- Language-Section Start -->
			<div id="language_vieweditdivscroll">
				<div class="clearfix"></div>
				<div class="width-new">
					<div class="panel panel-primary box-shadow1 th_bordercolor new-profile" id="langwholeid"> 
						<?php $this->load->view('front_end/lang_section'); ?>
					</div>
				</div>
			</div>
			<!-- Language-Section End -->
			<!-- Change Password start-->
		<div id="changepassword_vieweditdivscroll">
			<div class="clearfix"></div>
			<div class="width-new">
				<div class="panel panel-primary box-shadow1 th_bordercolor new-profile"> 
					<div class="panel-heading panel-bg bg-new-p"><span class="th_bgcolor" style="padding:5px;"><span class="fa fa-key" style="margin-right:5px;"></span> Change Password</span> </div>
					
					<div class="panel-body" style="padding:10px;">
						<form method="post" name="editpassword" id="editpassword" action="<?php echo $base_url.'my_profile/editpassword'; ?>">
							<div class="alert alert-danger" id="messagepassword" style="display:none" ></div>
							<div class="alert alert-success" id="success_msgpassword" style="display:none" ></div>
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">New Password*</label>
									<input type="password" class="edit-input" id="new_password" name="new_password" placeholder="Enter your new password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" />
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Confirm Password*</label>
									<input type="password" class="edit-input" id="confirm_password" name="confirm_password" placeholder="Confirm your password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" />
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Old Password*</label>
									<input type="password" class="edit-input" id="old_password" name="old_password" placeholder="Enter your old password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required"/ >
									<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
									<input type="hidden" name="csrf_job_portal" id="csrf_job_portal" value="<?php echo $this->security->get_csrf_hash();?>"/>
								
								</div>
							
							</div>
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
									<!-- <a href="#" class="btn-job-new block new-save"> Save Settings</a> -->
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- Change Password end-->
	<!-- Resume Download start -->
<div id="resumedwn_vieweditdivscroll">
	<div class="clearfix"></div>
	<div class="width-new">
		<div class="panel panel-primary box-shadow1 th_bordercolor new-profile"> 
			<div class="panel-heading panel-bg bg-new-p"><span class="th_bgcolor" style="padding:5px;"><span class="fa fa-file" style="margin-right:5px;"></span> Resume Download</span> </div>
			<?php $upload_path = $this->common_front_model->fileuploadpaths('resume_file',1); ?>
			<div class="panel-body" style="padding:10px;" id="resume_dwn_viewdiv">
				<div class="row" style="margin-bottom:0px;">
					<div class="<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>col-md-6 col-xs-12 col-sm-12<?php }else{ ?>col-md-12 col-xs-12 col-sm-12<?php } ?>">
						<label class="label-new"> Your latest resume</label>
						<div class="row">
							<div id="success_message" style="position:fixed;  left:40%; top:18%; z-index:9999; opacity:0">
                                    
                                    </div>
                                    <?php if(!($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file'])))
			{ ?>			
			<div class="col-md-7 col-xs-7 col-sm-7 margin-top-5 alert alert-danger " id="noreusme" >
				<strong> Sorry ! </strong> Your resume is not avilable.Please upload to increase the value of your profile.
			</div>
		<?php } ?>
							<div class="col-md-5 col-xs-5 col-sm-5 margin-top-5" id="imagediv">
								<div class="file-upload-2 marginclass">
									<label for="uploadimage" class="file-upload__label-2"> <img src="<?php echo $base_url; ?>assets/front_end/images/upload-to-cloud.png" alt="" style="display: inline-block;"> Upload Resume</label>
									<input type="file" id="uploadimage" name="uploadimage"class="file-upload__input-2">
									<!-- <input id="upload-2" class="file-upload__input-2" type="file" name="file-upload"> -->
								</div>
							</div>
							<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ $getfilename = explode('.',$user_data_values['resume_file']); ?>
							<div class="col-md-5 col-xs-5 col-sm-5 margin-top-5 job-padding-zero" id="downloadresumediv">
								<div class="file-upload-2 marginclass">
								<a href="<?php echo $upload_path.'/'.$user_data_values['resume_file']; ?>" class="btn-job-new-down" download="<?php echo $user_data_values['fullname'].'.'.$getfilename['1']; ?>"><img src="<?php echo $base_url; ?>assets/front_end/images/down-to-cloud.png" alt="" style="display:inline-block;"> Download Resume</a>
                                            </div>
									
								</div>
							
							<?php } ?>
							<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>    
							<div class="col-md-2 col-xs-2 col-sm-2 margin-top-5 job-padding-zero">
								<div class="file-upload-2 marginclass" id="deletediv" data-deletepart="resume" data-deleteid="<?php echo $user_data_values['resume_file'];?>">
								<a href="javascript:void(0)" id="" data-file = "<?php echo $upload_path.'/'.$user_data_values['resume_file']; ?>" class="btn-job-new-down job-delete" ><img src="<?php echo $base_url; ?>assets/front_end/images/delete-i.png" alt="" style="display:inline-block;"> Delete</a>
									<!-- <a href="" class="btn-job-new-down job-delete"> <img src="<?php echo $base_url; ?>assets/front_end/images/delete-i.png" alt="" style="display:inline-block;"> Delete</a>
									 -->
								</div>
							</div>
							<?php } ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<?php if($user_data_values['resume_file'] != '' && !is_null($user_data_values['resume_file']) && file_exists($upload_path.'/'.$user_data_values['resume_file']))
			{ ?>
                                        <h6 class="text-gray m-top-10 nospace1"><i class="fa fa-clock-o"></i> Last Updated Date : <?php echo $this->common_front_model->displayDate($user_data_values['resume_last_update'],'F j, Y h:i A'); ?></h6>
                                        <?php } ?>
					
				</div>
				
			</div>
			
			<div class="row margin-top-20" style="margin-bottom:0px;">
				<div class="col-md-12 col-xs-12 col-sm-12">
					<a href="#" class="btn-job-new block new-save"> Save Settings</a>
				</div>
			</div>
			<!--<div class="row margin-top-0" style="margin-bottom:0px;">
				<div class="col-md-12 col-xs-12 col-sm-12 margin-top-5">
				<div class="checkbox-new">
				<label class="checkbox-new">
				<input type="checkbox">
				<span class="indicator"></span>
				You accepts our <a href="" class="a-color">Terms and Conditions</a> and <a href="" class="a-color">Privacy Policy</a>
				</label>
				</div>
				</div>
			</div>-->
		</div>
	</div>
</div>
</div>
<!-- Resume Download end-->
                        </div>	

				</div>
			
<script>
$(document).ready(function(e) {
   <?php if($user_data['city']!=""){ ?> 
dropdownChange('country','city','city_list',function() {setcityval();});
//$.when(dropdownChange('country','city','city_list')).then(setcityval());
function setcityval()
{
	$("#city").val('<?php echo $user_data['city']; ?>');
	$('#city').trigger('chosen:updated');
}
<?php }  ?> 
expchange();

change_gender('<?php echo $user_data['gender']; ?>','onload');
});

function expchange()
{
	var expyear = $('#exp_year').val();
	var expmonth = $('#exp_month').val();
	var hideids = ["annsaldivid", "jbroledivid", "fnareadivid", "industrydivid"];
	/*alert(expmonth);alert(expyear);*/
	if(($.trim(expmonth)!=0 && checkvarok(expmonth)) || ($.trim(expyear)!=0 && checkvarok(expyear)))
	{
		showidsfn(hideids);
	}
	else
	{
		hideidsfn(hideids);
		$('#currency_type').val('').trigger('chosen:updated');
		$('#a_salary_lacs').val('').trigger('chosen:updated');
		$('#a_salary_thousand').val('').trigger('chosen:updated');
	}
}
function hideidsfn(idarray)
{
	/*var arrayLength = idarray.length;*/
	$.each(idarray,function(index,value){
		$('#'+value).slideUp();
	}) 
	/*for (var i = 0; i < arrayLength; i++) 
	{
			//alert(idarray[i]);
	}*/
}
function showidsfn(idarray)
{
	$.each(idarray,function(index,value){
		$('#'+value).slideDown();
	}) 
}
function checkvarok(parvar)
{
	if($.trim(parvar)!='' && $.trim(parvar)!='undefined' && $.trim(parvar)!='null' && $.trim(parvar)!=null && $.trim(parvar)!=undefined && $.trim(parvar)!=undefined)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function donotrepeatedu(elm)
{
	var selectedoption = $(elm).find('option:selected');
	var selectedselectname = $(elm).closest('select').attr('name');
	var selected_group = selectedoption.closest('optgroup').attr('data-optgrpid');
	
	$("select[name^='qualification_level']").each(function( index ) {
		$this = $( this );
		var allselectname = $this.attr('name');
		if(allselectname != selectedselectname)
		{
			//$( "optgroup[data-optgrpid='"+selected_group+"']" ).attr('disabled','disabled').trigger("chosen:updated");
			//$this.find("optgroup[data-optgrpid="+selected_group+"]").attr('disabled','disabled').trigger("chosen:updated");
		}
		else
		{
			//$this.find("optgroup").removeAttr('disabled').trigger("chosen:updated");
			//$( "optgroup[data-optgrpid='"+selected_group+"']" ).removeAttr('disabled').trigger("chosen:updated");
		}
	});
}

function change_gender(gender,action)
{
	if(action=='change')
	{
		$('#title').val('');
	}
	
	if(gender=='Male')
	{
		$('.gn_classfemale').css('display','none');
		$('.gn_classmale').css('display','block');
	}
	else if(gender=='Female')
	{
		$('.gn_classfemale').css('display','block');
		$('.gn_classmale').css('display','none');
	}
}  

</script>  