<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="step44">
    <div class="panel panel-success">
      <div class="panel-heading"> <?php echo $custom_lable_arr['sign_up_emp_complete']; ?>! </div>
      <div class="panel-body"><?php echo $custom_lable_arr['sign_up_emp_complete_mes']; ?></div>
    </div>
</div>
