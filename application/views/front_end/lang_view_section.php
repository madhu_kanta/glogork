<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid ;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
                                    <div class="panel-heading panel-bg" style="color:#ffffff;border-bottom:4px solid"><!--#D9534F--><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><!--background-color:#D9534F;background:none;--><span class="glyphicon glyphicon-plus"></span> Work history Details</span> <a href="javascript:void(0)" class="btn btn-md pull-right foreditfontcolor" style="margin:-3px;" onClick="editsection('workdetails_view');"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_Edit']?></a></div>
                                    <div class="panel-body" style="padding:10px;" id="defineheightlng">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                            <?php 
                                            $work_details = $this->my_profile_model->getdetailsfromid('jobseeker_workhistory_view','js_id',$user_data['id'],'',$dataorcount='data',$records='multiple');
                                            if(isset($work_details) && $work_details!='' && is_array($work_details) && count($work_details) > 0){
                                            $countloopwork = 0;	 
                                            foreach($work_details as $work_details_single)
                                            {
												$countloopwork = $countloopwork+1;
												$annual_salary_stored = '';
                                             ?>
                                             <?php if($countloopwork > 1){ ?>
                                                <hr class="th_bordercolor" style="border-width:4px" id="workhrid<?php echo $countloopwork; ?>">                                         <?php } ?><!--;display:none-->
                                                <table class="table" id="worktbid<?php echo $countloopwork; ?>" <?php if($countloopwork > 1){/*echo "style='display:none'";*/} ?>>
                                                    <tbody>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6" style="border-top:none;">Company Name :</td>
                                                            <td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($work_details_single['company_name']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['company_name'])) ? $work_details_single['company_name'] : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Industry :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['industries_name']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['industries_name'])) ? $work_details_single['industries_name'] : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Functional Area :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['functional_name']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['functional_name'])) ? $work_details_single['functional_name'] : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Job Role :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['role_name']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['role_name'])) ? $work_details_single['role_name'] : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Annual Salary :</td>
															<td class="col-md-6 col-xs-6">
															<?php 
															$salary_range = $this->my_profile_model->getdetailsfromid('salary_range','id',$work_details_single['annual_salary'],'salary_range');
															echo ($work_details_single['annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['annual_salary']) && count($salary_range) > 0 && $salary_range['salary_range']!='' ) ? $salary_range['salary_range'] :  $custom_lable_arr['notavilablevar']; 
															//echo ($work_details_single['annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['annual_salary'])) ? $work_details_single['annual_salary'] : $custom_lable_arr['notavilablevar'];?></td>
                                                            <?php /*?><td class="col-md-6 col-xs-6">
															<?php 
                                                            if($this->common_front_model->checkfieldnotnull($work_details_single['annual_salary']))
                                                            {
                                                                $annual_salary_stored = $work_details_single['annual_salary'];
                                                             
															    $annual_salary_work = ($this->common_front_model->checkfieldnotnull($annual_salary_stored)) ? explode(".",$annual_salary_stored) : "";
																if(($this->common_front_model->checkfieldnotnull($annual_salary_work) && isset($annual_salary_work[0]) && $annual_salary_work[0]!='' && $annual_salary_work[0]!='0') || ($this->common_front_model->checkfieldnotnull($annual_salary_work) && isset($annual_salary_work[1]) && $annual_salary_work[1]!='' && $annual_salary_work[1]!='0'))
																{
																	if($this->common_front_model->checkfieldnotnull($work_details_single['currency_type']))
																	{
																		echo $work_details_single['currency_type'];
																	}
																	if($this->common_front_model->checkfieldnotnull($annual_salary_work) && isset($annual_salary_work[0]) && $annual_salary_work[0]!='' && $annual_salary_work[0]!='0' )
																	{ 
																		echo " ".$annual_salary_work[0].' '.$custom_lable_arr['lacs'] ; 																	
																	}
																	if($this->common_front_model->checkfieldnotnull($annual_salary_work) && isset($annual_salary_work[1]) && $annual_salary_work[1]!='' && $annual_salary_work[1]!='0' )
																	{ 
																	   echo " ".$annual_salary_work[1].' '.$custom_lable_arr['thousand'] ; 														
																	}
																}
																else
																{
																	echo $custom_lable_arr['myprofile_no_salary_msg'];
																}
                                                            }
                                                            else
                                                            {
                                                                echo   $custom_lable_arr['notavilablevar'];
                                                            }
                                                                ?><?php */?></td>
                                                                
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Acheivements :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['achievements']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['achievements'])) ? $work_details_single['achievements'] : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Joining Date :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['joining_date']!='0000-00-00' && $work_details_single['joining_date']!='0' && $this->common_front_model->checkfieldnotnull($work_details_single['joining_date'])) ? $this->common_front_model->displayDate($work_details_single['joining_date'],'j S \of F Y') : $custom_lable_arr['notavilablevar'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-md-6 col-xs-6">Leaving Date :</td>
                                                            <td class="col-md-6 col-xs-6"><?php echo ($work_details_single['leaving_date']!='0' && $work_details_single['leaving_date']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($work_details_single['leaving_date'])) ? $this->common_front_model->displayDate($work_details_single['leaving_date'],'j S \of F Y') : 'Present'/*$custom_lable_arr['notavilablevar']*/;?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <?php 
                                            }//foreach end 
                                                if(count($work_details) > 1)
                                                {
                                            ?>
                                            <!--<div class="input-group-btn text-center">
                                                <a class="button"  id="viewmorelang" onClick="viewmorelang()"> View More</a>
                                            </div>-->
                                            <?php 
                                                }
                                            }else{ ?>
                                             <div class="alert alert-info">
                                                <?php echo $custom_lable_arr['myprofile_no_work_msg'];?>
                                             </div>
                                             <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>