<?php $custom_lable_arr = $custom_lable->language; ?>
<form method="post" name="editsociallinks" id="editsociallinks" action="<?php echo $base_url.'my_profile/editsociallinks'; ?>" >
<div class="alert alert-danger" id="message_social" style="display:none" ></div>
<div class="alert alert-success" id="success_msg_social" style="display:none" ></div>
						<div class="panel-body" style="padding:10px;" id="socialwholeid">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['facebook']; ?></label>
									<input type="text" class="edit-input"id="facebookurl" name="facebookurl" placeholder="<?php echo $custom_lable_arr['justurl'] .' '. $custom_lable_arr['facebook'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['facebook_url'])) ? $user_data['facebook_url'] : ""; ?>"  data-validation="url" data-validation-optional="true">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['gplus']; ?></label>
									<input type="text" class="edit-input" id="gplusurl" name="gplusurl" placeholder="<?php echo $custom_lable_arr['justurl'] .' '. $custom_lable_arr['gplus'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['gplus_url'])) ? $user_data['gplus_url'] : ""; ?>" data-validation="url" data-validation-optional="true">
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['twitter']; ?></label>
									<input type="text" class="edit-input" id="twitterurl" name="twitterurl" placeholder="<?php echo $custom_lable_arr['justurl'] .' '. $custom_lable_arr['twitter'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['twitter_url'])) ? $user_data['twitter_url'] : ""; ?>" data-validation="url" data-validation-optional="true">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['linkedin']; ?></label>
									<input type="text" class="edit-input" id="linkdinurl" name="linkdinurl" placeholder="<?php echo $custom_lable_arr['justurl'] .' '. $custom_lable_arr['linkedin'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['linkedin_url'])) ? $user_data['linkedin_url'] : ""; ?>" data-validation="url" data-validation-optional="true">
								</div>
							</div>
							
							
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<!-- <a href="#" class="btn-job-new block new-save"> Save Settings</a> -->
									<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
									<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true" ></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
											<!-- <button type="button" class="btn btn-default" onClick="viewsection('socaila');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php //echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
								</div>
							</div>
						</div>
						</form>
                                        
                                    