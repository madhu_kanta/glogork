<div class="clearfix"></div>
<div id="titlebar" class="single photo-bg" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner/pricing-banner.jpg);background-repeat: no-repeat;background-size: cover;min-width: 100%;background-position: center center;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-money" aria-hidden="true"></i> Membership Plan</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>Upgrade your membership plan and enjoy over service</li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="container">    
	<?php
		if($this->session->userdata('cancel_payment'))
		{
			$cancel_payment = $this->session->userdata('cancel_payment');
			if(isset($cancel_payment) && $cancel_payment !='' && $cancel_payment =='Yes')
			{
	?>
    	<div class="sixteen columns">
            <div class="notification error margin-bottom-30 text-bold">
                <p>Your Payment has been cancelled by you or you have enter wrong payment detail, please try again with proper detail, Thank you</p>
            </div>
        </div>
	<?php
				$this->session->unset_userdata('cancel_payment');
			}
		}
	$user_type = '';
	$user_id = '';
	$user_data = $this->common_front_model->get_logged_user_typeid();
	$user_type = $user_data['user_type'];
	$user_id = $user_data['user_id'];
	if($user_type !='' && $user_id !='')
	{
		$plan_data = $this->my_plan_model->plan_list();
		//print_r($plan_data);
?>
	<!--<div class="sixteen columns">
		<h3 class="margin-bottom-20"><u>Price Category - Silver</u>:</h3>
	</div>-->
	<?php	
	if(isset($plan_data) && $plan_data !='' && is_array($plan_data) && count($plan_data) > 0)
	{
		$plan_data_count = count($plan_data);
		$plan_bg_arr = array(
			'color-basic','color-saver','color-premium','color-ultimate','color-basic','color-basic','color-basic'
		);
		$class_plan_disp = 'five';
		if($plan_data_count == 4 )
		{
			$class_plan_disp = ' four ';
		}
		$plan_sr = 0;
		$check_yes = '<span class="fa fa-check text-success"></span>';
		$check_no = '<span class="fa fa-times text-danger"></span>';
		foreach($plan_data as $plan_data_val)
		{
			$displ_class = '';
			if(isset($plan_bg_arr[$plan_sr]) && $plan_bg_arr[$plan_sr] !='')
			{
				$displ_class = $plan_bg_arr[$plan_sr];
			}
			$plan_sr++;
	?>
	<div class="plan <?php if($displ_class !=''){ echo $displ_class;} ?> <?php echo $class_plan_disp; ?>  columns box-jobs1">
		<div class="plan-price">
			<h3 style="vertical-align:middle;"><i class="fa fa-line-chart" aria-hidden="true"></i> <?php echo $plan_data_val['plan_name']; ?></h3>
			<span class="plan-currency"><?php echo $plan_data_val['plan_currency']; ?></span>
			<span class="value"><?php echo $plan_data_val['plan_amount']; ?></span>
		</div>
		<div class="plan-features" align="left">
			<ul>
				<li>Allowed Message - <?php echo $plan_data_val['message']; ?></li>
                <li>Allowed Contacts - <?php echo $plan_data_val['contacts']; ?></li>
           <?php
		    if($user_type == 'job_seeker')
			{
		   ?>
                 <li>Relevant Jobs - <?php if(isset($plan_data_val['relevant_jobs']) && $plan_data_val['relevant_jobs'] =='Yes'){ echo $check_yes;}else{echo $check_no; }?></li>
                <li>Job Notification - <?php if(isset($plan_data_val['job_post_notification']) && $plan_data_val['job_post_notification'] =='Yes'){ echo $check_yes;}else{echo $check_no; }?></li>
                <li>Performance Report - <?php if(isset($plan_data_val['performance_report']) && $plan_data_val['performance_report'] =='Yes'){ echo $check_yes;}else{echo $check_no; }?></li>
                <li>Highlight Application - <?php if(isset($plan_data_val['highlight_application']) && $plan_data_val['highlight_application'] =='Yes'){ echo $check_yes;}else{echo $check_no; }?></li>
                
           <?php
			}
			else
			{
			?>
            	<li>Job Life - <?php echo $plan_data_val['job_life']; ?> Days</li>
                <li>Highlight Job - <?php echo $plan_data_val['highlight_job_limit']; ?></li>
                <li>Job Post Limit - <?php echo $plan_data_val['job_post_limit']; ?></li>
                <li>Cv Access Limit - <?php echo $plan_data_val['cv_access_limit']; ?></li>
            <?php
			}
		   ?>     
				<li>Plan Duration - <?php echo $plan_data_val['plan_duration']; ?> Days</li>
			</ul>
			<a class="button" href="javascript:;" onClick="update_plan_id('<?php echo $plan_data_val['id']; ?> ')"><i class="fa fa-shopping-cart" style="font-size:20px;"></i> Buy Now</a>
		</div>
	</div>
	<?php
		}
	}
	else 
	{
?>	
  	<div class="alert alert-danger">Sorry, Currently not any plan active.</div> 
<?php
	}
}
else
{
?>	
   	<div class="alert alert-danger">Please login first..</div> 
<?php
}
?>
</div>
<form action="<?php echo $base_url.'my-plan/buy-now'; ?>" method="post" id="plan_submit" name="plan_submit">
	<input type="hidden" name="plan_id" id="plan_id" value="" />
    <input type="hidden" id="hash_tocken_id_temp" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</form>
<div class="margin-top-40"></div>
<script type="text/javascript">
	function update_plan_id(plan_id)
	{
		if(plan_id !='')
		{
			$("#plan_id").val(plan_id);
			$("#plan_submit").submit();
		}
		return false;
	}
</script>