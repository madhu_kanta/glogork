<?php $custom_lable_arr = $custom_lable->language; ?>
<?php  if(get_cookie('employer_login_detail') && get_cookie('employer_login_detail')!='')
	 {
		$employer_login_detail = json_decode(get_cookie('employer_login_detail'));
	 } 
?>
<div id="titlebar1" class="photo-bg margin-bottom-0" style="background: url(<?php echo $base_url.'assets/front_end/' ?>images/banner-home-02.jpg);no-repeat;background-size:cover;">
	<div class="container">
		<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
        <div class="panel panel-default" style="padding:0px;background-color:rgba(255,255,255,.7);">
            <div class="panel-heading box-shadow-radius">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-user"></span> Login Employer</h4>
            </div>
            <div class="panel-body">
                <div class="row" style="margin:0px;">
                    <div class="col-md-7 col-sm-7 col-xs-12" style="border-right: 1px dotted #ffffff;padding-right: 30px;">
                        <!-- Nav tabs -->
							<a href="javascript:void(0);" class="button" onclick="return false;"><span class="glyphicon glyphicon-user"></span> Login</a>
						<div class="">
							<div class="" style="display: block;">
								<form method="post" class="login" name="loginemployer" id="loginemployer" action="<?php echo $base_url.'login_employer/login'; ?>">
                                <div class="alert alert-danger" id="message2" <?php if(!$this->session->flashdata('emp_ver_incorect')){ ?> style="display:none" <?php } ?> ><?php if($this->session->flashdata('emp_ver_incorect')){ echo $this->session->flashdata('emp_ver_incorect'); } ?></div>
								<div class="alert alert-success" id="success_msg2" <?php if(!$this->session->flashdata('emp_ver_corect')){ ?> style="display:none" <?php } ?> ><?php if($this->session->flashdata('emp_ver_corect')){ echo $this->session->flashdata('emp_ver_corect'); } ?></div>
									<div class="group">
										<input type="text" class="text-filed" placeholder="Email ID" style="padding:9px 0 9px 10px;" data-validation="required,email"  data-validation-error-msg="Enter your login email" name="username" id="username" value="<?php if(isset($employer_login_detail) &&  $employer_login_detail->username!='') {echo $employer_login_detail->username;} ?>"/><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="group">
										<input type="password" class="text-filed" placeholder="Password" style="padding:9px 0 9px 10px;" data-validation="required"  data-validation-error-msg="Enter your password"  name="password" id="password" value="<?php if(isset($employer_login_detail) && $employer_login_detail->password!='') {echo $employer_login_detail->password;} ?>" autocomplete="new-password" /><span class="highlight"></span><span class="bar"></span><!-- autocomplete="off" -->
									</div>
									<div class="row margin-bottom-0">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="form-row pull-left">
												<label for="rememberme" class="rememberme">
												<input name="rememberme" type="checkbox" id="rememberme" value="Yes" <?php if(get_cookie('employer_login_detail') && get_cookie('employer_login_detail')!='') {echo "checked";} ?>/> Remember Me</label>				
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p class="lost_password pull-right">
												<a href="<?php echo $base_url; ?>login-employer/forgot_password" style="color:grey;">Forgot Password ?</a>
											</p>
										</div>
									</div>
                                    <div class="row margin-top-5" id="google_recaptcha" style="display:none;">
									<div class="col-md-8" style="padding-top:10px;">
                                    <?php
									$rand_captcha = array( mt_rand(0,9), mt_rand(1, 9) );
									$captcha = array('captcha_emp_login' => $rand_captcha);
									$this->session->set_userdata($captcha); 	
									$captcha_list = $this->session->userdata('captcha_emp_login');
									echo $custom_lable_arr['security_text'].' '. $captcha_list[0]?> + <?php echo $captcha_list[1]?> ? (<?php echo $custom_lable_arr['security_question']; ?>) 
										</div>
										<div class="col-md-4 group">
											
                                            <input type="text"  data-validation="spamcheck"
           class="text-filed text-center"  name="validate_captcha"  id="captcha" data-validation-captcha="<?php echo ( $captcha_list[0] + $captcha_list[1] )?>"><span class="highlight"></span><span class="bar"></span>
			<!---->
            							</div>
									</div>
                                    
									<p class="form-row text-center margin-top-0">
										<input type="submit" class="button border fw margin-top-10 box-shadow-radius" name="login" value="Login" />
									</p>
                                    <input type="hidden" name="action" id="action" value="login"/>
                                    <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB" />
                                    <input type="hidden" name="login_attepmt_value" id="login_attepmt_value" value=""/>
								</form>
							</div>
                            <a  href="<?php echo $base_url; ?>sign-up/login" style="text-decoration:none;float:right;"><strong>Job Seeker ? </strong> Click here</a>
						</div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 hidden-xs">
                    <?php if(isset($empregcms) && count($empregcms) > 0){ ?>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3 class="margin-top-5"><?php echo $custom_lable_arr['read_more']; ?></h3>
                               
                            </div>
                        </div>
                        <hr class="colorgraph">
                         <?php echo $this->common_front_model->checkfieldnotnull($empregcms['page_content']) ? $empregcms['page_content'] : ''; ?>
                         <?php } ?>
						
                      <p><a href="<?php echo $base_url; ?>sign-up-employer" class="btn btn-info btn-block box-shadow-radius margin-top-10">Get Started!</a></p>
                    </div>
                </div>
            </div>
        </div>
		</div>
	</div>
</div>
<link href="<?php echo $base_url; ?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
/*$(function () {
    $('.button-checkbox').each(function () {
        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");
            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);
            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        // Initialization
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});*/
</script>
<script>
<!-------Tooltip------->
$('input')
    .on('beforeValidation', function(value, lang, config) {
		var login_attepmt = $('#login_attepmt_value').val();
		if(login_attepmt > 2)
		{
			$('#captcha').removeAttr('data-validation-skipped');
		}
		else
		{
			$('#captcha').attr('data-validation-skipped','1');
		}
	 });
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
<!------Pop Up------>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
	validateformlogin();
	$('#google_recaptcha').slideUp();
	<?php
	if($this->session->userdata('login_attemp_stored')!='' && $this->session->userdata('login_attemp_stored') > 2)
	{ ?>  
	 $('#google_recaptcha').slideDown();
	<?php } ?>
});
function validateformlogin()
{
	$.validate({
    form : '#loginemployer',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg2").hide();
      $("#message2").slideDown();
	  //$("#message2").focus();
	  $("#message2").html("<?php echo $custom_lable_arr['login_emp_form_val_mes']; ?>");
	  scroll_to_div('message2',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message2").hide();
	  var datastring = $("#loginemployer").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'login_employer/login'; ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#message2").slideUp();
				$("#success_msg2").slideDown();
				$("#success_msg2").html(data.errmessage);
				scroll_to_div('success_msg2',-100);
				document.getElementById('loginemployer').reset();
				setTimeout(function(){ window.location='<?php echo $base_url.'employer_profile' ?>';  }, 500);
			}
			else
			{
				if(data.login_attemp_stored > 2)
				{
					$('#login_attepmt_value').val(data.login_attemp_stored)
					$('#google_recaptcha').slideDown();
				}
				$("#success_msg2").slideUp();
				$("#message2").html(data.errmessage);
				$("#message2").slideDown();
				scroll_to_div('message2',-100);
			}
		}
	});
	  	hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
}
</script>