<?php $message_type = 'inbox';
	$total_for_pag = 0;
	$limit_page = 10;
	if(!isset($page_number) || $page_number =='')
	{
		$page_number = 1;
	}
	if($this->input->post('message_type'))
	{
		$message_type_p = $this->input->post('message_type');
		if($message_type_p !='')
		{
			$message_type = $message_type_p;
		}
	}
	$search_message = '';
	if($this->input->post('search_message'))
	{
		$search_message = $this->input->post('search_message');
	}
		$inbox_count = $this->my_message_model->getInbox(0);
		$sent_count = $this->my_message_model->getSent(0,'sent');
		$draft_count = $this->my_message_model->getSent(0,'draft');
		
		$glphi_mess = 'inbox';
		if($message_type == 'inbox')
		{
			$message_data = $this->my_message_model->getInbox(2,1,$page_number,$limit_page);
			$total_for_pag = $this->my_message_model->getInbox(0,1);
		}
		else if($message_type == 'sent')
		{
			$message_data = $this->my_message_model->getSent(2,'sent','1',$page_number,$limit_page);
			$glphi_mess = 'envelope';
			$total_for_pag = $this->my_message_model->getSent(0,'sent','1');
		}
		else if($message_type == 'draft')
		{
			$message_data = $this->my_message_model->getSent(2,'draft','1',$page_number,$limit_page);
			$glphi_mess = 'new-window';
			$total_for_pag = $this->my_message_model->getSent(0,'draft','1');
		}
		$delete_message ='';
		if($this->session->userdata('delete_message') && $this->session->userdata('delete_message') !="")
		{
			$delete_message = $this->session->userdata('delete_message');
			$this->session->unset_userdata('delete_message');
		}
?>
<style>
.unread_message {background-color: #efefef;}
</style>
<div class="row">
	<div class="panel panel-default panel-static-top" role="navigation">
		<div class="panel-heading th_bgcolor"><span class="glyphicon glyphicon-envelope"></span> My Message</div>
		<div class="panel-body th_bordercolor">
			<div class="tab-content">
				<div class="tab-pane active" id="inbox">
					<div class="clearfix">
						<div class="col-md-12">
							<h2 class="content-title"><span class="glyphicon glyphicon-<?php echo $glphi_mess; ?>"></span> 
							<?php echo ucfirst($message_type); 
							?></h2>
							<hr>
                            <form action="<?php echo $base_url; ?>my-message/send_message" method="post" id="search_from_message" name="search_from_message" onSubmit="return get_ajax_search('my-message/display_messsageType',1,'search_from_message')">
                            <input type="hidden" id="search_from_id" name="search_from_id" value="search_from_message" />
                            <input type="hidden" id="delete_action" name="delete_action" value="" />
							<?php
								if(isset($delete_message) && $delete_message !='')
								{
								?>
								<div class="alert alert-success" id="success_message_delete">
									<?php echo $delete_message; ?>
								</div>
                                <script type="text/javascript">
								$(document).ready(function(e) {
                                	settimeout_div('success_message_delete');
								});
								</script>
								<?php
								}
							?>
                            <div class="row">
								<div class="col-sm-2 col-md-2 border-right">
									<div class="btn-compose pull-left">
									</div>
                                    <div class="row">
										<a onClick="show_modelpp()" class="popup-with-zoom-anim btn btn-danger navbar-btn" href="#small-dialog"><span class="glyphicon glyphicon-pencil"></span> Compose</a>
										<hr>
										<ul class="nav nav-pills nav-stacked col-sm-12">
											<li <?php if(isset($message_type) && $message_type =='inbox'){?>class="active" <?php } ?>><a href="javascript" onClick="return display_messageType('inbox',1)" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span><span class="badge pull-right"><?php echo $inbox_count; ?></span> Inbox </a></li>
											<li class="hr"></li>
											<li <?php if(isset($message_type) && $message_type =='sent'){?>class="active" <?php } ?>><a href="javascript" onClick="return display_messageType('sent',1)" data-toggle="tab"><span class="glyphicon glyphicon-envelope"></span> <span class="badge pull-right"><?php echo $sent_count; ?></span> Sent</a></li>
											<li class="hr"></li>
											<li <?php if(isset($message_type) && $message_type =='draft'){?>class="active" <?php } ?>><a href="javascript" onClick="return display_messageType('draft',1)" data-toggle="tab"><span class="glyphicon glyphicon-new-window"></span><span class="badge pull-right"><?php echo $draft_count; ?></span> Draft</a></li>
											<li class="hr"></li>
											<!--<li><a href="#trash"  data-toggle="tab"><span class="glyphicon glyphicon-trash"></span><span class="badge pull-right"><?php //echo $trash_count; ?></span> Trash</a></li>-->
											<!--<li class="hr"></li>-->
										</ul>
									</div>
								</div>
								<div class="col-sm-12 col-md-10 col-xs-12 margin-top-10">
									<div class="btn-group">
										<a class="btn btn-default" style="border-color:#ccc !important">
											<div class="checkbox" style="margin: 0;">
												<label>
													<input type="checkbox" id="all" onClick="check_all()">
												</label>
											</div>
										</a>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li><a href="javascript:;" onClick="return delete_message()">Delete</a></li>
											<!--<li><a href="#">None</a></li>
											<li><a href="#">Read</a></li>
											<li><a href="#">Unread</a></li>
											<li><a href="#">Starred</a></li>
											<li><a href="#">Unstarred</a></li>-->
										</ul>
									</div>
									<button onClick="return get_ajax_search('my-message/display_messsageType',1,'search_from_message')" type="button" class="btn btn-default" data-toggle="tooltip" title="Refresh">
										   <span class="glyphicon glyphicon-refresh"></span>
									</button>
                                    <button  onclick="return delete_message()" type="button" class="btn btn-default" data-toggle="tooltip" title="Delete">
										   <span class="glyphicon glyphicon-trash"></span>
									</button>

									<div class="pull-right">
										<div class="text-muted" style="margin-bottom: -12px;">
                                        	<?php echo $this->common_front_model->rander_pagination('my-message/display_messsageType',$total_for_pag); ?>
                                        </div>
									</div>
									<hr>
									<div class="inner-addon right-addon">
										<i class="glyphicon glyphicon-search"></i>
										<input type="search" id="search_message" name="search_message" placeholder="Search Mail" class="form-control mail-search" value="<?php echo $search_message; ?>" />
										<input type="hidden" name="message_type" id="message_type" value="<?php echo $message_type; ?>" />
									</div>
									<ul class="mail-list">
										<?php
										if(isset($message_data) && $message_data !='' && is_array($message_data) && count($message_data) > 0)
										{
											foreach($message_data as $message_data_val)
											{
												$id = $message_data_val['id'];
												$class_read ='';
												$call_update_status ='';
												if(isset($message_data_val['read_status']) && $message_data_val['read_status'] =='Unread' && $message_type =='inbox')
												{
													$class_read =' unread_message ';
													$call_update_status = 'onClick="update_msg_status('.$id.')"';
												}
										?>
										<li >
											<div id="message_div_id_<?php echo $id; ?>" class="list-group-item row hover margin-bottom-0 <?php echo $class_read; ?>" <?php echo $call_update_status; ?>>
												<div class="col-md-1 col-sm-1 col-xs-1 text-center checkbox">
													<input onClick="check_uncheck_all()" type="checkbox" class="checkbox_val" name="message_id[]" value="<?php echo $id;?>">
                                                    <input type="hidden" id="message_status_<?php echo $id;?>" value="<?php echo $message_data_val['read_status']; ?>" />
												</div>
												<div class="col-md-9 col-sm-9 col-xs-9 white" data-toggle="collapse" data-target="#demo_<?php echo $id;?>">
													<span class="mail-sender"><?php if(isset($message_data_val['fullname']) && $message_data_val['fullname'] !=''){ echo $message_data_val['fullname'];} else { echo 'N/A';} ?></span>
													<span class="mail-message-preview"><?php if(isset($message_data_val['subject']) && $message_data_val['subject'] !=''){ echo $message_data_val['subject'];}else { echo 'N/A';} ?></span>
												</div>
												<span>
												<?php if($message_type == 'inbox')
												{ 
													  if(isset($message_data_val['is_deleted']) && $message_data_val['is_deleted']=='No' && $message_data_val['status']!='UNAPPROVED')
													  {?>
													<button type="button" onClick="replay_message('<?php echo $id; ?>','replay')" class="btn btn-info btn-sm "><i class="fa fa-reply"></i> Reply</button>
													<?php }
													else
													{?>
                                                    <button type="button" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="Does Not Exist"> Not Exist</button>
													<?php }?>
												<?php 
												} else if($message_type == 'draft')
												{
													 if(isset($message_data_val['is_deleted']) && $message_data_val['is_deleted']=='No' && $message_data_val['status']!='UNAPPROVED')
													  {?> 
                                                <button type="button" onClick="replay_message('<?php echo $id; ?>','draft')" class="btn btn-info btn-sm "><i class="fa fa-edit"></i> Edit and Send</button>
												<?php }
													else
													{?>
                                                    <button type="button" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="Does Not Exist"> Not Exist</button>
													<?php }?>
												<?php 
												} ?>
                                                <span class="badge small pull-right"><?php if(isset($message_data_val['sent_on']) && $message_data_val['sent_on'] !=''){ echo $this->common_front_model->displayDate($message_data_val['sent_on'],'F j');} ?></span></span>
											</div>
											<div id="demo_<?php echo $id;?>" class="collapse" style="border:1px solid #ccc;padding:10px;background-color:#FDFFF1;">
											<p class="text-grey margin-bottom-0">Subject :  <?php if(isset($message_data_val['subject']) && $message_data_val['subject'] !=''){ echo $message_data_val['subject'];}else { echo 'N/A';} ?></p>
												<p class="small margin-top-0"><i class="fa fa-clock-o"></i> &nbsp;&nbsp;<?php echo $this->common_front_model->displayDate($message_data_val['sent_on']); ?></p>
												<hr>
												<p><?php if(isset($message_data_val['content']) && $message_data_val['content'] !=''){ echo nl2br($message_data_val['content']);}else { echo 'N/A';} ?></p>
												</div>
										</li>
										<?php
											}
										}
										else
										{
										?>
                                        <li> <div class="alert alert-danger">No data available</div></li>
                                        <?php
										}
										?>
									</ul>
								</div>
							</div>
                            </form>
						</div>
					</div>
			   </div>
			</div>
			</div>
		</div>
	</div>
    
<input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id_temp" />

<script type="text/javascript">
	$(document).ready(function(e) {
		if($("#ajax_pagin_ul").length > 0)
		{   
			load_pagination_code();
		}		
	});
</script>