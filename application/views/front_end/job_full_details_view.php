<style>
#scroll {
    height:400px;
    overflow:scroll;
}
</style>
<?php
$custom_lable_array = $custom_lable->language;
$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
$where_arra = array('id'=>$job_details['posted_by'],'is_deleted'=>'No');
$get_emp_details = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1);
if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
{
	if(isset($get_emp_details['email']) && $get_emp_details['email'] !='')
	{
		$get_emp_details['email'] = $this->common_model->emial_disp;
	}
	if(isset($get_emp_details['mobile']) && $get_emp_details['mobile'] !='')
	{
		$get_emp_details['mobile'] = $this->common_model->mobile_disp;
	}
}
/*echo "<pre>";
print_r($get_emp_details);
echo "</pre>";*/
if(isset($get_emp_details) && is_array($get_emp_details) && count($get_emp_details)>1)
{
	if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
	{
			$js_id = $this->common_front_model->get_userid();
			$where_arra = array('js_id'=>$js_id,'job_id'=>$job_details['id']);
			$jobseeker_viewed_jobs = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,1);
			$check_already_apply = $this->common_front_model->get_count_data_manual('job_application_history',$where_arra,1,'','','',1);
			$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$get_emp_details['id']);
			//print_r($where_arra_emp);
			$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);

			  $get_count = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,0);
			  $update_on = $this->common_front_model->getCurrentDate();
			   if($get_count=='' || $get_count=='0')
				{
				   $data_insert = array('update_on'=>$update_on,'js_id'=>$js_id,'job_id'=>$job_details['id']);
				   $this->common_front_model->update_insert_data_common('jobseeker_viewed_jobs',$data_insert,'',0);
				}
			   else
			   {
				   $data_update = array('update_on'=>$update_on);
				   $this->common_front_model->update_insert_data_common('jobseeker_viewed_jobs',$data_update,$where_arra);
			   }
		}

	$company_logo = '';
	if(isset($get_emp_details['company_logo']) && $get_emp_details['company_logo']!='')
	{
		$company_logo = $base_url.'assets/company_logos/'.$get_emp_details['company_logo'];
	}

	if(get_cookie('viewed_job_cookie') && get_cookie('viewed_job_cookie')!='')
	{

		$set_view_job_cookie = get_cookie('viewed_job_cookie');
		$set_view_job_cookie_val = $set_view_job_cookie.','.$job_details['id'];
		$exp_view_cookie = explode(',',$set_view_job_cookie_val);
		$array_unic = array_unique($exp_view_cookie);
		if(count($array_unic) > 10)
		{
			$array_unic = array_slice($array_unic,10);
		}
		$set_view_job_cookie_val = implode(',',$array_unic);
	 }
	else
	{
		$set_view_job_cookie_val = $job_details['id'];
	}
	$viewed_job_cookie = array('name'  => 'viewed_job_cookie','value'  => $set_view_job_cookie_val,'expire' => 604800); // For 7 days
	$this->input->set_cookie($viewed_job_cookie);

	?>
	<div class="clearfix"></div>
	<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/job-page.jpg); background-size:cover; height: 331px;">
		<div class="container">
			<div class="ten columns">
				<span>
				<form method="post" id="submit_ind_search_form"  target="_blank"  action="<?php echo $base_url; ?>job-listing/posted-job">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				 <input type="hidden" name="search_industry[]" value="<?php echo $job_details['job_industry']; ?>">
				 <input type="hidden" name="user_agent" value="NI-WEB">
				  <a  style="cursor:pointer;" onClick="return submit_ind_search_form();"><?php echo $job_details['industries_name']; ?></a>
				</form>
				</span>
				<h2><?php echo $job_details['job_title']; ?> <span class="full-time"><?php echo $job_details['job_shift_type']; ?></span></h2>
				<div>
					<a class="btn btn-primary btn-twitter btn-circle" target="_blank" data-toggle="tooltip" href="http://twitter.com/home?status=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($job_details['id'])); ?>" title="<?php echo $custom_lable_array['share_twitter']; ?>"><i class="fa fa-twitter"></i>
					</a>
					<a class="btn btn-danger" data-toggle="tooltip" target="_blank" title="<?php echo $custom_lable_array['share_gplus']; ?>" href="https://plus.google.com/share?url=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($job_details['id'])); ?>"><i class="fa fa-google-plus"></i>
					</a>

					<a class="btn btn-primary" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_facebook']; ?>" onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($job_details['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($company_logo); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="fa fa-facebook" ></i>
					</a>

					<a class="btn btn-info" target="_blank" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_linkedin']; ?>" href=" https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($job_details['id'])); ?>&title=<?php echo urlencode($job_details['job_title']); ?>
	&summary=<?php echo urlencode($job_details['job_title']); ?>&source=<?php echo urlencode($job_details['job_title']); ?>"><i class="fa fa-linkedin"></i>
					 </a>

					 <a class="btn btn-danger" target="_blank" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_pinterest']; ?>" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($job_details['id'])); ?>	&media=<?php echo urlencode($company_logo); ?>&description=<?php echo urlencode($job_details['job_title']); ?>"><i class="fa fa-pinterest-square"></i>
					 </a>
				</div>
			</div>
			<div class="six columns">

				<div class="two-buttons">

					<?php
					if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
					{ ?>
					<?php
						if(isset($js_id) && $js_id!='')
						{
							$return_data_plan_msg = $this->common_front_model->get_plan_detail($js_id,'job_seeker','message');
							if($return_data_plan_msg=='Yes')
							{ ?>
					<a href="#small-dialog" class="popup-with-zoom-anim button"><i class="fa fa-lg fa-envelope"></i><?php echo  $custom_lable_array['send_msg']; ?></a>
					<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">

						<?php
						if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
						{ ?>

						<div class="small-dialog-headline">
						  <i class="fa fa-envelope"></i> <?php echo  $custom_lable_array['send_msg']; ?>
						</div>
						<div class="clearfix margin-bottom-10"></div>
						<div class="alert alert-danger" id="error_msg" style="display:none" > </div>
						<div class="alert alert-success" id="success_msg" style="display:none" ></div>

						<div class="small-dialog-content">
							<form  method="post"  id="send_msg_form" >
								<div>
								<input type="text" name="subject" data-validation="required" placeholder="Subject" value="" />
								</div>
								<div>
								<textarea placeholder="Message" data-validation="required" name="content" ></textarea>
								</div>
								<button class="send" ><i class="fa fa-check"></i> <?php echo $custom_lable_array['send_msg']; ?></button>
								<input type="hidden" value="NI-WEB" name="user_agent">
								<input type="hidden" value="send_message" name="action">
								<input type="hidden" value="<?php echo $job_details['posted_by']; ?>" name="emp_id">
							</form>
						</div>

						<?php }
						else
						{ $this->session->set_userdata($return_after_login_url); ?>
						<div class="">
							<div class="alert alert-danger">
								 <?php echo  $custom_lable_array['please_login']; ?>
							</div>
						</div>
						<?php }
						 ?>
					</div>
					<?php
							}//if plan is valid
						}//if js is login ?>
					<?php }
					?>
				</div>
			</div>
			<div class="clearfix"></div>

			<?php
			$f_icon = "<i  class='fa fa-thumbs-up'></i>";
			$like_text = $custom_lable_array['like'];
			$save_text =  $custom_lable_array['save_job'];
			$like_action = 'like_job';
			$save_action = 'save_job';
			$save_icon = '<i class="fa fa-save"></i>';
			if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
			{
				if(isset($jobseeker_viewed_jobs) && $jobseeker_viewed_jobs!='' && is_array($jobseeker_viewed_jobs) && count($jobseeker_viewed_jobs) > 0)
				{

					if($jobseeker_viewed_jobs['is_liked']=='Yes')
					{
						$f_icon = "<i class='fa fa-check'></i>";
						$like_text = $custom_lable_array['liked'];
						$like_action = 'unlike_job';
					}
					if($jobseeker_viewed_jobs['is_saved']=='Yes')
					{
						$save_text = $custom_lable_array['saved'];
						$save_icon = "<i class='fa fa-check'></i>";
						$save_action = 'remove_save_job';
					}
			}

			 ?>
			 <div class="clearfix"></div>
			<div class="six columns pull-right">
				<!--<button type="button" class="btn btn-sm btn-warning pull-right"><i class="fa fa-save"></i> Action</button>-->
				<button type="button" class="btn btn-sm btn-success pull-right margin-right-10 save_job_button" onClick="return jobseeker_action('<?php echo $save_action; ?>','job','<?php echo $job_details['id']; ?>');"><?php echo $save_icon; ?><?php echo $save_text; ?></button>
				<button type="button" onClick="return jobseeker_action('<?php echo $like_action; ?>','job','<?php echo $job_details['id']; ?>');" class="btn btn-sm btn-primary pull-right margin-right-10 like_job_button"><?php echo $f_icon ; ?> <?php echo $like_text; ?></button>
				</br>
				<!--<div id="js_action_msg_div1"></div>-->
			</div>
			<div class="clearfix"></div>
			<div class="six columns pull-right">
				<div id="js_action_msg_div">
				</div>
		    </div>
		   <?php } ?>
		</div>
	</div>
	<div class="container">
		<div class="eleven columns">
			<div>
				<!-- Company Info -->
				<div class="company-info well" id="scrolldiv">
					<div class="col-md-9 col-sm-9 col-xs-12">
					<?php if(isset($get_emp_details['company_logo']) && $get_emp_details['company_logo']!='' && $get_emp_details['company_logo_approval'] == 'APPROVED')
					{ ?>
					<img src="<?php echo $base_url; ?>assets/company_logos/<?php echo $get_emp_details['company_logo']; ?>" alt="company logo" />
					<?php }else
					{ ?>
					 <img src="<?php echo $base_url; ?>assets/front_end/images/company-logo.png" alt="company logo" />
					<?php }
					?>

						<div class="content">
							<h4><?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['company_name']) ?  $get_emp_details['company_name'] : 'N/A'; ?></h4>
				<?php
				if(isset($js_id) && $js_id!='')
				{
					$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
					if($return_data_plan == 'Yes')
					{
						 ?>
							<span class="small"><a target="_blank" href="<?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['company_website']) ?  $get_emp_details['company_website'] : ''; ?>"><i class="fa fa-link"></i> <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['company_website']) ?  $get_emp_details['company_website'] : 'N/A'; ?></a></span><br />
							<span class="small"><a href="javascript:;"><i class="fa fa-envelope"></i> <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['email']) ?  $get_emp_details['email'] : 'N/A'; ?></a></span>
				<?php
					}//if plan is valid
				} ?>
								<!--<div class="rating-block">
									<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
								</div>-->
						</div>
					</div>
					<?php
					$block_action = 'block_emp';
					$block_icon = '<span class="glyphicon glyphicon-ban-circle"></span>';
					$block_text = $custom_lable_array['block'];
					$follow_action = 'follow_emp';
					$follow_icon = '<span class="glyphicon glyphicon-user"></span>';
					$follow_text = $custom_lable_array['follow'];
					$like_emp_action = 'like_emp';
					$like_emp_icon = '<span class="glyphicon glyphicon-thumbs-up"></span>';
					$like_emp_text = 'Like';
					if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
					{

						if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) && count($jobseeker_access_employer) > 0)
						{
							if($jobseeker_access_employer['is_liked']=='Yes')
							{
								$like_emp_action = 'unlike_emp';
								$like_emp_icon = '<span class="glyphicon glyphicon-check"></span>';
								$like_emp_text = $custom_lable_array['liked'];
							}
							if($jobseeker_access_employer['is_follow']=='Yes')
							{
								$follow_action = 'unfollow_emp';
								$follow_icon = '<span class="glyphicon glyphicon-check"></span>';
								$follow_text = $custom_lable_array['following'];
							}
							if($jobseeker_access_employer['is_block']=='Yes')
							{
								$block_action = 'unblock_emp';
								$block_icon = '<span class="glyphicon glyphicon-check"></span>';
								$block_text = $custom_lable_array['blocked'];
							}
						}



						?>

					<div class="col-md-3 col-sm-3 col-xs-12 text-center">
					  <div class="btn-group">
							<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" style="padding:4px;text-transform:capitalize;"><span class="glyphicon glyphicon-cog"></span> <?php echo $custom_lable_array['action'] ?> <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a  class="emp_block_action " style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $block_action; ?>','Emp','<?php echo $get_emp_details['id']; ?>');"><?php echo $block_icon; ?> <?php echo $block_text; ?></a></li>
									<li class="divider"></li>
									<li><a class="emp_follow_action" style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $follow_action; ?>','Emp','<?php echo $get_emp_details['id']; ?>');"><?php echo $follow_icon; ?> <?php echo $follow_text; ?></a></li>
									<li class="divider"></li>
									<li><a  class="emp_like_action" style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $like_emp_action; ?>','Emp','<?php echo $get_emp_details['id']; ?>');"><?php echo $like_emp_icon; ?> <?php echo $like_emp_text; ?></a></li>
								</ul>
						</div>
					</div>
					<div class="clearfix"></div>
					<div id="js_action_emp_div"></div>

				   <?php  } ?>
					<div class="clearfix"></div>
				</div>
				<p class="margin-reset"><?php echo htmlspecialchars_decode($job_details['job_description'],ENT_QUOTES); ?> </p>
				<div class="margin-top-20"></div>
				<!--<hr>
				<p>The <strong>Food Service Specialist</strong> will have responsibilities that include:</p>
				<ul class="list-1">
					<li>Executing the Food Service program, including preparing and presenting our wonderful food offerings
					to hungry customers on the go!
					</li>
					<li>Greeting customers in a friendly manner and suggestive selling and sampling items to help increase sales.</li>
					<li>Keeping our Store food area looking terrific and ready for our valued customers by managing product
					inventory, stocking, cleaning, etc.</li>
					<li>We’re looking for associates who enjoy interacting with people and working in a fast-paced environment!</li>
				</ul>
				<div class="margin-top-20"></div>
				<hr>-->
				<!--<h4 class="margin-bottom-10">Job Requirment</h4>-->

				<?php
				if($job_details['work_experience_from']!='' && $job_details['work_experience_to']!='')
				{
					if($job_details['work_experience_from']=='0' && $job_details['work_experience_to']=='0')
					{
						$work_exp =  $custom_lable_array['fresher'];
					}
					else
					{
						$work_exp =$job_details['work_experience_from'] .' '.$custom_lable_array['to'] .' '.$job_details['work_experience_to'].' '.  $custom_lable_array['year'];
					}
				}
				else
				{
					$work_exp =  "N/A";
				}

				/*if($job_details['job_salary_from']!='' && $job_details['job_salary_to']!='')
				{

						$job_salary =  $job_details['currency_type'] .' '. $job_details['job_salary_from'] .' to '.$job_details['job_salary_to']. $custom_lable_array['lacs'];
				}
				else if($job_details['job_salary_from']!='')
				{
						$job_salary = $job_details['currency_type'] .' '. $job_details['job_salary_from'];
				}
				else
				{
						$job_salary =  "N/A";
				}*/
				if(isset($job_details['job_salary_from']) && $job_details['job_salary_from']!='')
				{
					if(is_numeric($job_details['job_salary_from']))
					{
						 $job_salary = $this->common_front_model->valueFromId('salary_range',$job_details['job_salary_from'],'salary_range');
					  }
					  else if($job_details['job_salary_from']!='')
					  {
						 $job_salary = $job_details['job_salary_from'];
					  }
				}else{
						$job_salary =  "N/A";
				}
				?>
				<ul class="list-1">
					<li><?php echo $custom_lable_array['salary']; ?> : <?php echo $job_salary; ?> </li>
					<li><?php echo $custom_lable_array['exprience']; ?> : <?php echo $work_exp; ?></li>
					<li><?php echo $custom_lable_array['functional_area']; ?> :  <?php echo $job_details['functional_name']; ?></li>
					<li><?php echo $custom_lable_array['job_role']; ?>  :  <?php  echo $this->common_front_model->checkfieldnotnull($job_details['job_role_name']) ?  $job_details['job_role_name'] : 'N/A';  ?></li>
					<li><?php echo $custom_lable_array['job_shift']; ?> :  <?php echo $this->common_front_model->checkfieldnotnull($job_details['job_shift_type']) ?  $job_details['job_shift_type'] : 'N/A';  ?></li>
					<li><?php echo $custom_lable_array['job_type']; ?> :  <?php echo $this->common_front_model->checkfieldnotnull($job_details['job_type_name']) ?  $job_details['job_type_name'] : 'N/A';   ?></li>
					<li><?php echo $custom_lable_array['total_requirenment'] ?>:  <?php  echo $this->common_front_model->checkfieldnotnull($job_details['total_requirenment']) ?  $job_details['total_requirenment'] : 'N/A';  ?></li>

					<li>
						<h4 class="margin-bottom-10"><?php echo $custom_lable_array['key_skills']; ?></h4>
							<div class="row">
								<div class="margin-bottom-10">
								<?php
								if(isset($job_details['skill_keyword']) && $job_details['skill_keyword'] !='')
								{
									$skill_keyword = explode(',',$job_details['skill_keyword']);
									if($skill_keyword !='' && is_array($skill_keyword) && count($skill_keyword) > 0)
									{
										foreach($skill_keyword as $skill)
										{
											echo '<a href="javascript:;"><span class="label label-default margin-right-10">'.$skill.'</span></a>';
										}
									}
								}
								 ?>
								</div>
								<div class="clearfix"></div>
							</div>
					</li>
				</ul>
				<hr>


				<h4 class="text-center p-bottom-20"><button class="button" data-toggle="collapse" data-target="#demo1"><span class="glyphicon glyphicon-plus"></span> <?php echo $custom_lable_array['read_more']; ?></button></h4>
				<div id="demo1" class="margin-reset collapse margin-top-20">
				<h4 class="margin-bottom-10"><?php echo $custom_lable_array['desire_c_profile']; ?></h4>
					<p class="margin-reset"><?php echo htmlspecialchars_decode($job_details['desire_candidate_profile'],ENT_QUOTES); ?></p>
					<div class="margin-bottom-10"></div>
					<p class="margin-reset"><?php echo $custom_lable_array['job_education']; ?> :<?php
					if($job_details['job_education']!='')
					{
					echo $this->common_model->valueFromId('educational_qualification_master',$job_details['job_education'],'educational_qualification');
					}
					else
					{
						echo "N/A";
					}
					 ?></p>

					<div class="margin-top-20"></div>
					<hr>
					<p><?php echo $custom_lable_array['myprofile_emp_cmpprolbl'] ?></p>
					<p><strong><?php echo $get_emp_details['company_name']; ?></strong></p>
					<ul class="list-1">
						<li><?php echo $get_emp_details['company_profile']; ?></li>
					</ul>
					<div class="margin-top-20"></div>
					<hr>
					<!--<h4 class="margin-bottom-10">Job Requirment</h4>
					<ul class="list-1">
						<li>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.</li>
						<li>Must be available to work required shifts including weekends, evenings and holidays.</li>
						<li>Must be able to perform repeated bending, standing and reaching.</li>
						<li> Must be able to occasionally lift up to 50 pounds</li>
					</ul>-->
				</div>
				<div class="margin-top-20"></div>
				<?php
				if(isset($js_id) && $js_id!='')
				{
					$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
					if($return_data_plan=='Yes')
					{ ?>
						<h4 class="text-center p-bottom-20"><button class="button" data-toggle="collapse" data-target="#demo2"><span class="glyphicon glyphicon-plus"></span><?php echo $custom_lable_array['view_contact_details']; ?></button></h4>
						<div id="demo2" class="margin-reset collapse margin-top-20">
				<h4 class="margin-bottom-10"> <?php echo $custom_lable_array['recruiter_details']; ?>:</h4>
					<ul class="list-1">
						<li> <?php echo $custom_lable_array['recruiter_name']; ?>: <?php echo  $this->common_model->valueFromId('personal_titles_master',$get_emp_details['title'],'personal_titles') .' '.$get_emp_details['fullname']; ?></li>
						<li><?php echo $custom_lable_array['email']; ?> : <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['email']) ?  $get_emp_details['email'] : 'N/A'; ?></li>
						<li><?php echo $custom_lable_array['web_site']; ?> : <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['company_website']) ?  $get_emp_details['company_website'] : 'N/A'; ?></li>
						  <li><?php echo $custom_lable_array['mobile']; ?> : <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['mobile']) ?  $get_emp_details['mobile'] : 'N/A'; ?></li>
						  <li><?php echo $custom_lable_array['company_name'] ?>: <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['company_name']) ?  $get_emp_details['company_name'] : 'N/A'; ?></li>
						  <li><?php echo $custom_lable_array['address_lbl']; ?> : <?php echo $this->common_front_model->checkfieldnotnull($get_emp_details['address']) ?  $get_emp_details['address'] : 'N/A'; ?></li>
					</ul>
					<div class="margin-top-20"></div>
					<hr>
					<!--<h4 class="margin-bottom-10">Job Requirment</h4>
					<ul class="list-1">
						<li>Excellent customer service skills, communication skills, and a happy, smiling attitude are essential.</li>
						<li>Must be available to work required shifts including weekends, evenings and holidays.</li>
						<li>Must be able to perform repeated bending, standing and reaching.</li>
						<li> Must be able to occasionally lift up to 50 pounds</li>
					</ul>-->
				</div>
				<?php
					}//if plan is valid
				} ?>
			</div>
		</div>
		<div class="five columns">
			<!-- Sort by -->
			<div class="widget">
				<h4> <?php echo $custom_lable_array['overview']; ?> :</h4>
				<div class="job-overview">
					<ul>
						<li>
							<i class="fa fa-map-marker"></i>
							<div style="margin-top:-10px;">
								<strong> <?php echo $custom_lable_array['location']; ?> :</strong>
								<span><?php echo $this->common_front_model->checkfieldnotnull($job_details['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($job_details['location_hiring']) : 'N/A'; ?></span>
							</div>
						</li>
						<li class="hr"></li>
						<li class="margin-top-20">
							<i class="fa fa-user"></i>
							<div style="margin-top:-10px;">
								<strong><?php echo $custom_lable_array['job_title']; ?>:</strong>
								<span><?php echo $job_details['job_title']; ?></span>
							</div>
						</li>
						<li class="hr"></li>
						<li class="margin-top-20">

							<i class="fa fa-clock-o"></i>
							<div style="margin-top:-10px;">
								<strong> <?php echo $custom_lable_array['job_details']; ?></strong>
								<span> <?php echo $custom_lable_array['posetd_on']; ?> : <?php echo $this->common_front_model->displayDate($job_details['posted_on']); ?></span>

							   <?php
					if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
					{
							  ?>   <span> <?php echo $custom_lable_array['expired_on']; ?> : <?php echo $this->common_front_model->displayDate($job_details['job_expired_on'],$dformat='F j, Y'); ?></span>
						<?php
					}
					?>

								<span> <?php echo $custom_lable_array['company_hiring']; ?> : <?php echo $this->common_front_model->checkfieldnotnull($job_details['company_hiring']) ?  $job_details['company_hiring'] : 'N/A'; ?></span>
								<?php
				if(isset($js_id) && $js_id!='')
				{
					$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
					if($return_data_plan == 'Yes')
					{
						 ?>
								<span> <?php echo $custom_lable_array['job_link']; ?>  : <?php  echo $this->common_front_model->checkfieldnotnull($job_details['link_job']) ?  $job_details['link_job'] : 'N/A'; ?></span>
			  <?php
					}//if plan is valid
				} ?>
							</div>
						</li>
						<li class="hr"></li>
						<!--<li class="margin-top-20">
							<i class="fa fa-money"></i>
							<div style="margin-top:-10px;">
								<strong>Rate:</strong>
								<span>$9.50 - $12.50 / hour</span>
							</div>
						</li>-->
					</ul>
					<?php
					if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
					{ ?> <!--removed classs == popup-with-zoom-anim-->
						 <!--removed classs == small-dialog1-->
						<?php
							if(isset($check_already_apply) && $check_already_apply!='' && is_array($check_already_apply) && count($check_already_apply) > 0)
							{ ?>
							   <a class="button" style="cursor:default;"><?php echo $custom_lable_array['already_apply']; ?></a>
							<?php }
							else
							{ ?>
							<a   onClick="apply_for_job(<?php echo $job_details['id']; ?>);" class="button apply_for_job"><?php echo $custom_lable_array['apply_job']; ?></a>
							<?php } ?>
					<!--<div id="small-dialog1" class="zoom-anim-dialog mfp-hide">
						<div class="small-dialog-headline">
							<h2><?php echo $custom_lable_array['apply_job']; ?></h2>
						</div>
						<div class="small-dialog-content">
							<form action="#" method="get" >
								<input type="text" placeholder="Full Name" value=""/>
								<input type="text" placeholder="Email Address" value=""/>
								<textarea placeholder="Your message / cover letter sent to the employer"></textarea>
								<div class="upload-info"><strong><?php echo $custom_lable_array['upload_cv']; ?></strong> <span><?php echo $custom_lable_array['upload_cv_size_lbl']; ?></span></div>
								<div class="clearfix"></div>
								<label class="upload-btn">
									<input type="file" multiple />
									<i class="fa fa-upload"></i> <?php echo $custom_lable_array['browse']; ?>
								</label>
								<span class="fake-input"> <?php echo $custom_lable_array['no_file_selected']; ?></span>
								<div class="divider"></div>
								<button class="send"> <?php echo $custom_lable_array['send_application']; ?></button>
							</form>
						</div>
					</div>-->
						<div class="clearfix margin-bottom-10"></div>
						<div class="alert alert-danger" id="error_msg_apply_job" style="display:none" > </div>
						<div class="alert alert-success" id="success_msg_apply_job" style="display:none" ></div>

					<?php }
					else
					{
							if($job_details['job_highlighted']=='Yes')
							   {

								   $lable_highlight = $custom_lable_array['job_highlighted'];
								   $icon_highlight = '<i class="fa fa-check" aria-hidden="true"></i>
	';
									$onclick_fn = '';
									$style = 'style="cursor:default;"';
							   }
							   else
							   {

								   $lable_highlight = $custom_lable_array['add_to_highlighted'];
								   $icon_highlight = '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
	';
								   $onclick_fn = 'onClick="add_to_highlight('.$job_details['id'].')";';
								   $style = 'style="cursor:pointer;"';
								}
						?>
						<a href="javascript:;" <?php echo $style; ?> class="button add_to_highlight_class" <?php echo $onclick_fn; ?> > <?php echo $icon_highlight; ?><?php echo  $lable_highlight; ?></a>

						<div class="clearfix margin-bottom-10"></div>
						<div  id="emp_action_msg_div"  ></div>

					<?php }
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="margin-top-50"></div>
	<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

	<script>

	function set_time_out_msg(div_id)
	{
		setTimeout(function(){ $('#'+div_id).html(''); $('#'+div_id).removeAttr('class');  }, 4000);
	}

	function apply_for_job(job_id)
	{

		<?php
		if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
		{
			//$this->session->set_userdata($return_after_login_url);
			$return_after_login_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$this->session->set_userdata($return_after_login_url);
			?>
			if (typeof(Storage) !== "undefined") {
				localStorage.setItem("return_after_login_url",'<?php echo $return_after_login_url; ?>');
				}

			 var message = '<?php echo $this->lang->line('please_login_apply'); ?>';
			 $("#error_msg_apply_job").html(message);
			 $("#error_msg_apply_job").slideDown();
			 //scroll_to_div('error_msg_apply_job',-100);
			 scroll_to_div_new('error_msg_apply_job');
			  return false;
		<?php } ?>
		var c = confirm("<?php echo $custom_lable_array['confirm_apply_job']; ?>");
		if(c==true)
		{
			var hash_tocken_id = $("#hash_tocken_id").val();
			var datastring = 'csrf_job_portal='+hash_tocken_id+'&job_id='+job_id+'&user_agent=NI-WEB';
			show_comm_mask();

			$.ajax({
			url : "<?php echo $base_url.'job_seeker_action/apply_for_job' ?>",
			type: 'post',
			data: datastring,
			dataType:"json",
			success: function(data)
			{

				$("#hash_tocken_id").val(data.token);
				if($.trim(data.status) == 'success')
				{
					$("#error_msg_apply_job").slideUp();
					$("#success_msg_apply_job").slideDown();
					$("#success_msg_apply_job").html(data.errmessage);
					set_time_out_msg('success_msg_apply_job');
					scroll_to_div('success_msg_apply_job',-100);
					$('.apply_for_job').removeAttr('onClick');
					$('.apply_for_job').css('cursor','default');
					$('.apply_for_job').html('<i class="fa fa-check"></i>  <?php echo $this->lang->line('already_apply'); ?>');
				}
				else if($.trim(data.status) == 'error close')
				{
					$("#success_msg_apply_job").slideUp();
					$("#error_msg_apply_job").html(data.errmessage);
					$("#error_msg_apply_job").slideDown();
					set_time_out_msg('error_msg_apply_job');
					scroll_to_div('error_msg_apply_job',-100);
				}
				else
				{
					$("#success_msg_apply_job").slideUp();
					$("#error_msg_apply_job").html(data.errmessage);
					$("#error_msg_apply_job").slideDown();
					set_time_out_msg('error_msg_apply_job');
					scroll_to_div('error_msg_apply_job',-100);
				}
				hide_comm_mask();
			}

			});

			 return false;

		}
	}
	function jobseeker_action(action,action_for,action_id)
	{
		<?php
		if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
		{
			//$this->session->set_userdata($return_after_login_url);
			$return_after_login_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$this->session->set_userdata($return_after_login_url);
			?>
			if (typeof(Storage) !== "undefined") {
				localStorage.setItem("return_after_login_url",'<?php echo $return_after_login_url; ?>');
				}

			var message = '<?php echo $this->lang->line('please_login'); ?>';

				if(action_for=='Emp')
				{
						$('#js_action_emp_div').html('');
						$('#js_action_emp_div').slideUp('Slow');
						$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_emp_div').slideDown('Slow');
						set_time_out_msg('js_action_emp_div');
						//scroll_to_div('js_action_emp_div',-100);
						scroll_to_div_new('scrolldiv');

				}
				else
				{
					$('#js_action_msg_div').html('');
					$('#js_action_msg_div').slideUp('Slow');
					$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_msg_div').slideDown('Slow');
					set_time_out_msg('js_action_msg_div');
					//scroll_to_div('js_action_emp_div',-100);
					//scroll_to_div_new('scrolldiv');

					if(action=="save_job" || action=="like_job")
					{
					    scroll_to_div_new('titlebar');
					}else
					{
						scroll_to_div_new('scrolldiv');
					}
				}
				return false;
		<?php }
		?>
		show_comm_mask();
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&action='+action+'&action_for='+action_for+'&action_id='+action_id+'&user_agent=NI-WEB';
		$.ajax({
			url : "<?php echo $base_url.'job-seeker-action/seeker-action' ?>",
			type: 'post',
			data: datastring,
			success: function(data)
			{
				$("#hash_tocken_id").val(data.token);
				if(action_for=='Emp')
				{
					$('#js_action_emp_div').html('');
					$('#js_action_emp_div').slideUp('Slow');
				}
				else
				{
					$('#js_action_msg_div').html('');
					$('#js_action_msg_div').slideUp('Slow');
				}

				if(data.status=='success')
				{
					if(action=='like_job')
					{
						$('.like_job_button').html('<i class="fa fa-check"></i> <?php echo $this->lang->line('liked'); ?>');
						$('.like_job_button').removeAttr('onClick');
						$('.like_job_button').attr('onClick','jobseeker_action("unlike_job","job",'+action_id+')');
					}
					if(action=='unlike_job')
					{
						$('.like_job_button').html('<i class="fa fa-thumbs-up"></i> <?php echo $this->lang->line('like'); ?>');
						$('.like_job_button').removeAttr('onClick');
						$('.like_job_button').attr('onClick','jobseeker_action("like_job","job",'+action_id+')');
					}
					if(action=='save_job')
					{
						$('.save_job_button').html('<i class="fa fa-check"></i> <?php echo $this->lang->line('saved'); ?>');
						$('.save_job_button').removeAttr('onClick');
						$('.save_job_button').attr('onClick','jobseeker_action("remove_save_job","job",'+action_id+')')
					}
					if(action=='remove_save_job')
					{
						$('.save_job_button').html('<i class="fa fa-save"></i> <?php echo $this->lang->line('save'); ?>');
						$('.save_job_button').removeAttr('onClick');
						$('.save_job_button').attr('onClick','jobseeker_action("save_job","job",'+action_id+')')
					}
					if(action=='block_emp')
					{
						$('.emp_block_action').html('<span class="glyphicon glyphicon-check"></span> <?php echo $this->lang->line('blocked'); ?>');
						$('.emp_block_action').removeAttr('onClick');
						$('.emp_block_action').attr('onClick','jobseeker_action("unblock_emp","Emp",'+action_id+')')

					}
					if(action=='unblock_emp')
					{
						$('.emp_block_action').html('<span class="glyphicon glyphicon-ban-circle"></span> <?php echo $this->lang->line('block'); ?>');
						$('.emp_block_action').removeAttr('onClick');
						$('.emp_block_action').attr('onClick','jobseeker_action("block_emp","Emp",'+action_id+')')
					}
					if(action=='follow_emp')
					{
						$('.emp_follow_action').html('<span class="glyphicon glyphicon-check"></span> <?php echo $this->lang->line('following'); ?>');
						$('.emp_follow_action').removeAttr('onClick');
						$('.emp_follow_action').attr('onClick','jobseeker_action("unfollow_emp","Emp",'+action_id+')')
					}
					if(action=='unfollow_emp')
					{
						$('.emp_follow_action').html('<span class="glyphicon glyphicon-user"></span> <?php echo $this->lang->line('follow'); ?>');
						$('.emp_follow_action').removeAttr('onClick');
						$('.emp_follow_action').attr('onClick','jobseeker_action("follow_emp","Emp",'+action_id+')')
					}
					if(action=='like_emp')
					{
						$('.emp_like_action').html('<span class="glyphicon glyphicon-check"></span> <?php echo $this->lang->line('liked'); ?>');
						$('.emp_like_action').removeAttr('onClick');
						$('.emp_like_action').attr('onClick','jobseeker_action("unlike_emp","Emp",'+action_id+')')
					}
					if(action=='unlike_emp')
					{
						$('.emp_like_action').html('<span class="glyphicon glyphicon-thumbs-up"></span> <?php echo $this->lang->line('like'); ?>');
						$('.emp_like_action').removeAttr('onClick');
						$('.emp_like_action').attr('onClick','jobseeker_action("like_emp","Emp",'+action_id+')')
					}


					if(action_for=='Emp')
					{
						$('#js_action_emp_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					   $('#js_action_emp_div').slideDown('Slow');
					   set_time_out_msg('js_action_emp_div');
					  // scroll_to_div('js_action_emp_div',-100);
					  scroll_to_div_new('scrolldiv');
					}
					else
					{

						$('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					   $('#js_action_msg_div').slideDown('Slow');
					   set_time_out_msg('js_action_msg_div');
					  // scroll_to_div('js_action_msg_div',-100);
					  //scroll_to_div_new('scrolldiv');
					}

				}
				else
				{
					if(action_for=='Emp')
					{
						$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_emp_div').slideDown('Slow');
						set_time_out_msg('js_action_emp_div');
						//scroll_to_div('js_action_emp_div',-100);
						 scroll_to_div_new('scrolldiv');
					}
					else
					{

						$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_msg_div').slideDown('Slow');
						set_time_out_msg('js_action_msg_div');
						//scroll_to_div('js_action_msg_div',-100);
						 scroll_to_div_new('scrolldiv');
					}
				}

				hide_comm_mask();
			}
		});

		  return false;
	}


	function add_to_highlight(job_id)
	{
		var c = confirm("<?php echo $custom_lable_array['confirm_hightlight_job']; ?>");
		if(c==true)
		{
			var hash_tocken_id = $("#hash_tocken_id").val();
			var datastring = 'csrf_job_portal='+hash_tocken_id+'&id='+job_id+'&user_agent=NI-WEB&mode=edit';
			show_comm_mask();

			$.ajax({
			url : "<?php echo $base_url.'job-listing/add-to-highlight' ?>",
			type: 'post',
			data: datastring,
			dataType:"json",
			success: function(data)
			{
				$("#hash_tocken_id").val(data.token);
				if($.trim(data.status) == 'success')
				{
					$('#emp_action_msg_div').html('');
					$('#emp_action_msg_div').slideUp('Slow');
					$('#emp_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#emp_action_msg_div').slideDown('Slow');
					$('.add_to_highlight_class').removeAttr('onClick');
					//$('.add_to_highlight_class'+job_id).removeAttr('class');
					$('.add_to_highlight_class').html('<i class="fa fa-check" aria-hidden="true"></i> <?php echo $this->lang->line('job_highlighted'); ?>');
					$('.add_to_highlight_class').css('cursor','default');
					set_time_out_msg('emp_action_msg_div');
					//scroll_to_div('emp_action_msg_div',-100);
					var objDiv = document.getElementById("emp_action_msg_div");
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				else
				{
					$('#emp_action_msg_div').html('');
					$('#emp_action_msg_div').slideUp('Slow');
					$('#emp_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#emp_action_msg_div').slideDown('Slow');
					set_time_out_msg('emp_action_msg_div');
					//scroll_to_div('emp_action_msg_div',-100);
					var objDiv = document.getElementById("emp_action_msg_div");
					objDiv.scrollTop = objDiv.scrollHeight;
				}
			hide_comm_mask();
			}

			});

			 return false;

		}

	}


	$.validate({
		form : '#send_msg_form',
		modules : 'security',
		onError : function($form) {
		  $("#error_msg").slideDown();
		  $("#error_msg").focus();
		  scroll_to_div('wrapper',-100);
		  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
		  set_time_out_msg('error_msg');
		  scroll_to_div('error_msg',-100);
		},
		onSuccess : function($form) {
		  show_comm_mask();
		  $("#error_msg").hide();
		  var datastring = $("#send_msg_form").serialize();
		  var hash_tocken_id = $("#hash_tocken_id").val();
		  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;

			$.ajax({
			url : "<?php echo $base_url.'job_seeker_action/send_message' ?>",
			type: 'post',
			data: datastring,
			dataType:"json",
			success: function(data)
			{
				$("#hash_tocken_id").val(data.token);
				if($.trim(data.status) == 'success')
				{
					$("#error_msg").slideUp();
					$("#success_msg").slideDown();
					$("#success_msg").html(data.errmessage);
					document.getElementById('send_msg_form').reset();
					scroll_to_div('success_msg',-100);
					set_time_out_msg('success_msg');
				}
				else
				{

					$("#success_msg").slideUp();
					$("#error_msg").html(data.errmessage);
					$("#error_msg").slideDown();
					scroll_to_div('error_msg',-100);
					set_time_out_msg('error_msg');
				}
				hide_comm_mask();
			}
		});

		  return false; // Will stop the submission of the form
		}
	  });

	function submit_ind_search_form()
	{
		$('#submit_ind_search_form').submit();
		return false;
	}

	</script>
	<?php
	if(isset($js_id) && $js_id!='')
	{
		$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
		if($return_data_plan == 'Yes')
		{
			$this->common_front_model->check_for_plan_update($js_id,'job_seeker',$job_details['posted_by']);
		}//if plan is valid
	}
}
else
{
	//Redirect($base_url.'job-listing/posted-job');
	Redirect($base_url);
}
?>