<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid ;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);" ><!--#D9534F-->
							<div class="panel-heading panel-bg" style="color:#ffffff;border-bottom:4px solid"><!--#D9534F--><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><!--background-color:#D9534F;--><span class="	glyphicon glyphicon-file"></span> <?php echo $custom_lable_arr['myprofile_social_med_tit']?></span> <a href="javascript:void(0);" class="btn btn-md pull-right foreditfontcolor" style="margin:-3px;" onClick="editsection('social_links');"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_Edit']?></a></div><!--background:none;-->
								
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<table class="table">
												<tbody>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['facebook']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;word-wrap:break-word"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['facebook_url'])) ? $user_data['facebook_url'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['gplus']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="word-wrap:break-word"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['gplus_url'])) ? $user_data['gplus_url'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['twitter']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="word-wrap:break-word"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['twitter_url'])) ? $user_data['twitter_url'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['linkedin']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="word-wrap:break-word"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['linkedin_url'])) ? $user_data['linkedin_url'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>