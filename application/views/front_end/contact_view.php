<?php $custom_lable_arr = $custom_lable->language;
$message_contact = '';
if(isset($type_contact) && $type_contact =='plan-request')
{
	$message_contact = "
Hi Admin,
I request for Free plan to my account.
Thanks in advance.";
}
?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">
		<div class="sixteen columns">
			<h2><?php echo $custom_lable_arr['contact_page_header_title']; ?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li><?php echo $custom_lable_arr['contact_page_bcrum_title']; ?>:</li>
					<li><a href="<?php echo $base_url;?>"><?php echo $custom_lable_arr['contact_page_bcrum_link']; ?></a></li>
					<li><?php echo $custom_lable_arr['contact_page_bcrum_link1']; ?></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<!-- Content
================================================== -->
<!-- Container -->
<div class="container">
	<div class="sixteen columns">
		<h3 class="margin-bottom-20"><?php echo $custom_lable_arr['contact_page_above_map_tit']; ?></h3>
		<!-- Google Maps -->
		<section class="google-map-container">
			<div id="googlemaps" class="google-map google-map-full"></div>
		</section>
		<!-- Google Maps / End -->
	</div>
</div>
<!-- Container / End -->
<!-- Container -->
<div class="container">
<div class="eleven columns">
	<h3 class="margin-bottom-15"><?php echo $custom_lable_arr['contact_page_frm_tit']; ?></h3>
		<!-- Contact Form -->
		<section id="contact" class="padding-right	" >
			<!-- Success Message -->
			<!--<mark id="message"></mark>-->
            <div class="alert alert-danger" id="message" style="display:none" ></div>
            <div class="alert alert-success" id="success_msg" style="display:none" ></div>
			<!-- Form -->
			<form method="post" action="<?php echo $base_url.'contact/contact_mail' ?>" name="contactform" id="contactform" ><!--onSubmit="submitcontact();"-->
				<fieldset>
					<div>
						<label><?php echo $custom_lable_arr['contact_form_field_title']; ?>: <span>*</span></label>
						<input name="name" type="text" id="name" data-validation="required" />
					</div>
					<div>
						<label><?php echo $custom_lable_arr['contact_form_field_title3']; ?>: <span>*</span></label>
						<input name="mobile" type="text" id="mobile" data-validation="required,number,length" data-validation-length="min10"/>
					</div>
					<div>
						<label><?php echo $custom_lable_arr['contact_form_field_title1']; ?>: <span>*</span></label>
						<input name="email" type="email" id="email"  data-validation="required,email" />
                        <!-- pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" -->
					</div>
					<div>
						<label><?php echo $custom_lable_arr['contact_form_field_title2']; ?>: <span>*</span></label>
						<textarea name="comment" cols="40" rows="3" id="comment" spellcheck="true" data-validation="required"><?php if(isset($message_contact) && $message_contact !=''){ echo $message_contact; } ?></textarea>
					</div>
				</fieldset>
				<div id="result"></div>
				<input type="submit" class="submit" id="submit" value="Send Message"  /><!--onClick="submitcontact();"-->
				<div class="clearfix"></div>
				<div class="margin-bottom-40"></div>
			</form>
		</section>
		<!-- Contact Form / End -->
</div>
<!-- Container / End -->
<!-- Sidebar
================================================== -->
<div class="five columns">
	<!-- Information -->
	<h3 class="margin-bottom-10"><?php echo $custom_lable_arr['contact_info_box_info_title']; ?></h3>
	<div class="widget-box well">
		<p style="font-weight:bold"><?php echo $config_data['web_frienly_name']; //echo $custom_lable_arr['contact_info_box_info']; ?></p>
		<ul class="contact-informations">
			<li><?php echo $config_data['full_address']; ?></li>
		</ul>
		<ul class="contact-informations second">
			<li><i class="fa fa-phone"></i> <p><?php echo '+'.$config_data['contact_no']; ?></p></li>
			<li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $config_data['contact_email']; ?>?Subject=Contact" target="_top"><?php echo $config_data['contact_email']; ?></a></li>
			<li><i class="fa fa-globe"></i> <p><?php echo $config_data['web_name']; ?></p></li>
		</ul>
	</div>
	<!-- Social -->
	<div class="widget margin-top-30">
		<h3 class="margin-bottom-5"><?php echo $custom_lable_arr['contact_social_title']; ?></h3>
		<ul class="social-icons">
			<li><a class="facebook" target="_blank" href="<?php echo $config_data['facebook_link'];?>"><i class="icon-facebook"></i></a></li>
			<li><a class="twitter" target="_blank" href="<?php echo $config_data['twitter_link'];?>"><i class="icon-twitter"></i></a></li>
			<li><a class="gplus" target="_blank" href="<?php echo $config_data['google_link'];?>"><i class="icon-gplus"></i></a></li>
			<li><a class="linkedin" target="_blank" href="<?php echo $config_data['linkedin_link'];?>"><i class="icon-linkedin"></i></a></li>
		</ul>
		<div class="clearfix"></div>
		<div class="margin-bottom-50"></div>
	</div>
</div>
</div>
<!-- Container / End -->
<!--<link href="<?php //echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="<?php //echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>-->
<!--<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.form-validator.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyC3UO8qO4SX-IcnV1F0aZsGze45kn1yW4Q&ver=3.7&sensor=true"></script>

<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.gmaps.min.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    submitcontact();
});
	(function($){
		$(document).ready(function(){
			$('#googlemaps').gMap({
				maptype: 'ROADMAP',
				scrollwheel: false,
				zoom: 13,
				markers: [
					{
						address: '<?php echo trim($config_data['map_address']); ?>',
						html: '<?php echo $config_data['map_tooltip']; ?>',
						popup: true,
					}
				],
			});
		   });
	})(this.jQuery);
</script>
<script>
function submitcontact()
{
	$.validate({
    form : '#contactform',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg").hide();
      $("#message").slideDown();
	  $("#message").focus();
	  $("#message").html("<?php echo $custom_lable_arr['error_msg_on_frm_submit']; ?>");
	  scroll_to_div('message',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message").hide();
	  var datastring = $("#contactform").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'contact/contact_mail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#message").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.message);
				scroll_to_div('success_msg',-100);
				$('#contactform')[0].reset();
			}
			else
			{
				$("#success_msg").slideUp();
				$("#message").html(data.message);
				$("#message").slideDown();
				scroll_to_div('message',-100);
			}		
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
	/*return false;*/
}

</script>