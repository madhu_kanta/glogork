<?php
$custom_lable_arr = $custom_lable->language;
if($custom_lable_arr == '')
{
	$custom_lable_arr = $custom_lable;
}
?>
<section id="Content" role="main">

        <div class="full-width infobox four-o-four">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 sixteen columns ">
                        <h1 class=""><!--strong-header-->
                            <?php echo $custom_lable_arr['404page_error'];?>:<br>
                            <?php echo $custom_lable_arr['404page_error_txt'];?>
                        </h1>
                        <p>
                            <?php echo $custom_lable_arr['404page_error_msg'];?>
                        </p>
                        <a href="<?php echo $base_url; ?>" class=""><?php echo $custom_lable_arr['404page_error_btntxt'];?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


<style>
/* ====== 404 PAGE ====== */
/*.four-o-four {
  background: #ea693f;
}*/
/*.four-o-four .strong-header {
  margin-top: 56px;
  margin-bottom: 24px;
  margin-left: -4px;
  font-size: 85px;
  line-height: 85px;
  color: #fff;
}
.four-o-four p {
  font-size: 16px;
}
.four-o-four .btn {
  margin-top: 35px;
  margin-bottom: 60px;
}*/
/* ======================= */
</style>
<!--<script src="<?php echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>-->