<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css?ver=1.11" type="text/css" rel="stylesheet">
<div class="clearfix"></div>
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/message.png); background-size:cover;">
    <div class="container">
        <div class="sixteen columns">
            <h2 style="vertical-align:middle;"><i class="fa fa-envelope"></i> My Message</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="<?php echo $base_url; ?>">Home</a></li>
                    <li>My Account</li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<style type="text/css">
.select2-container{z-index:99999999999 !important}
</style>
<div class="container" id="main_content_ajax">
	<?php include_once('my_message_view_sub.php'); 
	$place_holder_s2 = 'Find recruiters by typing here';
	$get_empl_jse = 'get_employer_list';
	if(isset($user_type) && $user_type !='' && $user_type =='employer')
	{
		$place_holder_s2 = 'Find employer by typing here';
		$get_empl_jse = 'get_jobseeker_list';
	}
	?>
</div>
<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
    <div class="small-dialog-headline">
        <i class="fa fa-envelope"></i> Write Message
    </div>
    <div class="small-dialog-content" id="compose_message_div">
    	<?php include_once('compose_message_view.php'); ?>
    </div>
</div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id" />
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
	function common_send_message()
	{
		show_comm_mask();
		  $("#success_message").slideUp();
		  $("#error_messaeg").slideUp();
		  var datastring = $("#message_form").serialize();
		  var hash_tocken_id = $("#hash_tocken_id").val();
		  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
			$.ajax({
				url : "<?php echo $base_url; ?>my-message/send_message",
				type: 'post',
				data: datastring,
				dataType:"json",
				success: function(data)
				{	
					$("#hash_tocken_id").val(data.token);
					if($.trim(data.status) == 'success')
					{
						$("#error_messaeg").slideUp();
						$("#success_message").slideDown();
						$("#success_message").html(data.errmessage);						
						scroll_to_div('success_message');
						if($("#message_action").val() =='draft')
						{
							if(data.draft_id !='' && data.draft_id != 0)
							{
								$("#draft_id").val(data.draft_id);
							}
						}
						else
						{
							document.getElementById('message_form').reset();
							$("#to_message").val('').change();
							$("#subject").val('');
							$("#content").val('');
							$("#draft_id").val('');
						}
						$("#search_from_message").submit();
					}
					else
					{
						$("#success_message").slideUp();
						$("#error_messaeg").html(data.errmessage);
						$("#error_messaeg").slideDown();
						scroll_to_div('error_messaeg');
					}
					hide_comm_mask();
					settimeout_div('success_message');
				}
			});
			return false;
			
	}
	function draft_message()
	{
		$("#message_action").val('draft');
		common_send_message();
	}
	function send_message()
	{
		$.validate({
			form : '#message_form',
			modules : 'security',
			onError : function($form)
			{
			  
			},
			onSuccess : function($form)
			{
				$("#message_action").val('sent');
				common_send_message();
				return false;
			}
		  });
	}
	function select2_int()
	{	
		var base_url = '<?php echo $base_url ; ?>';
		$('#to_message').select2({
		 placeholder: "<?php echo $place_holder_s2; ?>",
		  ajax: {
			url: base_url+'common_request/<?php echo $get_empl_jse; ?>',
			type: "POST",
			dataType:'json',
			data: function (params) {
			  return {
				q: params.term,
				page: params.page,
			  };
			},
		  }
		});
	}
	function display_messageType(type,page_number=1)
	{
		show_comm_mask();
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
			url : "<?php echo $base_url; ?>my-message/display_messsageType",
			type: 'post',
			data: {'csrf_job_portal':hash_tocken_id,'message_type':type,'page_number':page_number},
			success: function(data)
			{
				$("#main_content_ajax").html(data);
				$("#hash_tocken_id").val($("#hash_tocken_id_temp").val());
				$("#hash_tocken_id_temp").remove();
				//scroll_to_div('success_message');
				hide_comm_mask();
			}
		});
	}
	function show_modelpp()
	{
		document.getElementById('message_form').reset();
		$("#subject").val('');
		$("#content").val('');
		$("#to_message").val('').change();
		$("#draft_id").val('');
		$("#success_message").slideUp();
		  $("#error_messaeg").slideUp();
		$.magnificPopup.open({
			items: {
				src: '#small-dialog',
			},
			type: 'inline'
		});
	}
	$(document).ready(function(e) {
		select2_int();
		send_message();
	});
	function check_uncheck_all()
	{
		var total_checked = $('input[name="message_id[]"]:checked').length;
		var total = $('input[name="message_id[]"]').length;
		if(total ==total_checked)
		{
			$("#all").prop("checked",true);
		}
		else
		{
			$("#all").prop("checked",false);	
		}
	}
	function check_all()
	{
		$(".checkbox_val").prop("checked",$("#all").prop("checked"))
	}
	function delete_message()
	{
		var total_checked = $('input[name="message_id[]"]:checked').length;
		if(total_checked == 0 || total_checked =='')
		{
			alert("Please select atleast one record to proccess");
			return false;
		}
		$("#delete_action").val('Yes');
		$("#search_from_message").submit();

	}
	function replay_message(msg_id,msg_type)
	{
		//alert(msg_id);
		show_comm_mask();
		var hash_tocken_id = $("#hash_tocken_id").val();
		$.ajax({
			url : "<?php echo $base_url; ?>my-message/replay_msg_form",
			type: 'post',
			data: {'csrf_job_portal':hash_tocken_id,'msg_id':msg_id,'msg_type':msg_type},
			success: function(data)
			{
				$("#compose_message_div").html(data);
				$("#hash_tocken_id").val($("#hash_tocken_id_temp").val());
				$("#hash_tocken_id_temp").remove();
				//scroll_to_div('success_message');
				hide_comm_mask();
				$.magnificPopup.open({
					items: {
						src: '#small-dialog',
					},
					type: 'inline'
				});
			}
		});
	}
	function update_msg_status(msg_id)
	{
		var message_status = $("#message_status_"+msg_id).val();
		if(message_status =='Unread')
		{
			var hash_tocken_id = $("#hash_tocken_id").val();
			$.ajax({
				url : "<?php echo $base_url; ?>my-message/message_readstatus",
				type: 'post',
				data: {'csrf_job_portal':hash_tocken_id,'message_id':msg_id},
				dataType:"json",
				success: function(data)
				{
					$("#hash_tocken_id").val(data.token);
					$("#message_status").val('Read');
					$("#message_div_id_"+msg_id).removeClass('unread_message');
				}
			});
		}
	}
</script>