<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid ;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
								<div class="panel-heading panel-bg" style="color:#ffffff;border-bottom:4px solid"><!--#D9534F--><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><!--background-color:#D9534F;background:none;--><span class="glyphicon glyphicon-plus"></span> <?php echo $custom_lable_arr['myprofile_lang_known_tit']?></span> <a href="javascript:void(0)" class="btn btn-md pull-right foreditfontcolor" style="margin:-3px;" onClick="editsection('language_view');"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_Edit']?></a></div>
								<div class="panel-body" style="padding:10px;" id="defineheightlng">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
                                        <?php 
										//$language_details = $this->my_profile_model->getlangknownfromid($user_data['id']);
										//$language_details = $this->my_profile_model->getdetailsfromid('jobseeker_language','js_id',$user_data['id'],'',$dataorcount='data',$records='multiple');
										if(isset($language_details) && $language_details!='' && is_array($language_details) && count($language_details) > 0){
										$countlooplan = 0;	 
										foreach($language_details as $language_details_single)
										{$countlooplan = $countlooplan+1;
										 ?>
                                         <?php if($countlooplan > 1){ ?>
                                            <hr class="th_bordercolor" style="border-width:4px" id="langhrid<?php echo $countlooplan; ?>">                                         <?php } ?><!--;display:none-->
                                        	<table class="table" id="langtbid<?php echo $countlooplan; ?>" <?php if($countlooplan > 1){/*echo "style='display:none'";*/} ?>>
												<tbody>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;">Language Known :</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($language_details_single['language']!='0' && $this->common_front_model->checkfieldnotnull($language_details_single['language'])) ? $language_details_single['language'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Proficiency Level :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($language_details_single['proficiency_level']!='0' && $this->common_front_model->checkfieldnotnull($language_details_single['proficiency_level'])) ? $language_details_single['proficiency_level'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Read :</td>
														<td class="col-md-6 col-xs-6"><span class="glyphicon <?php if($language_details_single['reading']=='Yes'){ echo "glyphicon-ok"; }else{ echo "glyphicon-remove"; } ?>"></span></td>
													</tr>												
													<tr>
														<td class="col-md-6 col-xs-6">Write :</td>
														<td class="col-md-6 col-xs-6"><span class="glyphicon <?php if($language_details_single['writing']=='Yes'){ echo "glyphicon-ok"; }else{ echo "glyphicon-remove"; } ?>"></span></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Speak :</td>
														<td class="col-md-6 col-xs-6"><span class="glyphicon <?php if($language_details_single['speaking']=='Yes'){ echo "glyphicon-ok"; }else{ echo "glyphicon-remove"; } ?>"></span></td>
													</tr>
												</tbody>
											</table>
                                            <?php 
										}//foreach end 
											if(count($language_details) > 1)
											{
										?>
                                        <!--<div class="input-group-btn text-center">
                                        	<a class="button"  id="viewmorelang" onClick="viewmorelang()"> View More</a>
                                        </div>-->
										<?php 
											}
										}else{ ?>
                                         <div class="alert alert-info">
                                         	<?php echo $custom_lable_arr['myprofile_no_lang_msg']; ?>
                                         </div>
                                         <?php } ?>
										</div>
									</div>
								</div>
							</div>