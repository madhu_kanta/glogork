<style>
ul li input[type="search"] 
{
	width : 300% !important;
}
</style>
<?php 
//$place_holder_s2 = 'Select Recruiter to send message';
$subject_msg = '';
$draft_id = '';
$reply_msg_id = '';
$msg_id ='';
$msg_type = 'replay';
$message_cont = '';
if($this->input->post('msg_id'))
{
	$msg_id = $this->input->post('msg_id');	
}
if($this->input->post('msg_type'))
{
	$msg_type = $this->input->post('msg_type');
}
if($msg_type =='draft')
{
	$draft_id = $msg_id;
}
	if($msg_id !='')
	{
		$data_arr_msg = $this->common_front_model->get_count_data_manual('message',array('id'=>$msg_id),1,'*','','1',1,"");
		if(isset($data_arr_msg) && $data_arr_msg !='' && is_array($data_arr_msg) && count($data_arr_msg) > 0)
		{
			$subject_msg = $data_arr_msg['subject'];
			
			$sender = $data_arr_msg['sender'];
			$message_cont = $data_arr_msg['content'];
			
			if($msg_type =='replay')
			{
				$subject_msg = 'Re: '.$subject_msg;
				$message_cont = '';
			}
			
			$receiver= $data_arr_msg['receiver'];
			$sender_type = $data_arr_msg['sender_type'];
			$id = $data_arr_msg['id'];
			if($msg_type =='replay')
			{
				$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','id'=>$sender);
				if($sender_type =='employer')
				{
					$data_user_arr = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,'fullname,id,company_name as email','','1',1,"");
				}
				else
				{
					$data_user_arr = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,1,'fullname,id,email','','1',1,"");
				}
			}
			else
			{
				
				$where_arra = array('status'=>'APPROVED','is_deleted'=>'No','id'=>$receiver);
				
				if($sender_type =='employer')
				{
					$data_user_arr = $this->common_front_model->get_count_data_manual('jobseeker',$where_arra,1,'fullname,id,email','','1',1,"");
				}
				else
				{
					$data_user_arr = $this->common_front_model->get_count_data_manual('employer_master',$where_arra,1,'fullname,id,company_name as email','','1',1,"");
				}
				
			}
		}
	}
?>
<form action="<?php echo $base_url; ?>my-message/send_message" method="post" id="message_form" name="message_form">
	
    <div style="display:none" class="alert alert-danger" id="error_messaeg" ></div>
    <div style="display:none" class="alert alert-success" id="success_message"></div>
    <div>
    <select data-validation="required" data-validation-error-msg="<?php if(isset($place_holder_s2)){ echo $place_holder_s2;} ?>" class="margin-top-15" style="width:100%" id="to_message" name="to_message[]" placeholder="Subject" multiple>
    <?php

	if(isset($data_user_arr) && $data_user_arr !='' && is_array($data_user_arr) && count($data_user_arr))
	{
	?>
    	<option selected value="<?php echo $data_user_arr['id']; ?>"><?php echo $data_user_arr['fullname'].' ('.$data_user_arr['email'].')'; ?></option>
    <?php
	}
	?>
    </select> 
                                 
    </div>
    <div>
   <input data-validation="required" data-validation-error-msg="Please Write Subject" class="margin-top-15" type="text" placeholder="Subject" id="subject" name="subject" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Please Write Subject" value="<?php if(isset($subject_msg) && $subject_msg !=''){echo $subject_msg;} ?>" />
   </div>
   <div>
    <textarea  data-validation="required" data-validation-error-msg="Please Write Message" placeholder="Message" id="content" name="content" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Please Write Message"><?php if(isset($message_cont) && $message_cont !=''){ echo $message_cont;} ?></textarea>
    </div>
    <button onClick="return send_message()" class="send"><span class="glyphicon glyphicon-send"></span> Send</button>
    	<a style="float:right" href="javascript:;" onClick="return draft_message()">Save to Draft</a>
    <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB" >
    <input type="hidden" name="message_action" id="message_action" value="" >
    <input type="hidden" name="draft_id" id="draft_id" value="<?php if(isset($draft_id) && $draft_id !=''){echo $draft_id;} ?>" >
    <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id_temp" />
</form>
<script type="text/javascript">
$(document).ready(function(e) {
    select2_int();
    send_message();
});
</script>