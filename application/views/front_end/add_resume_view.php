<?php
if($this->session->userdata('js_fb_data'))
{
	$js_fb_data = $this->session->userdata('js_fb_data');
	if($js_fb_data != '')
	{
		$social_image = $js_fb_data['fb_image'];
	}
				
	$js_gplus_data = $this->session->userdata('js_gplus_data');
	if($js_gplus_data != '')
	{
		$social_image = $js_gplus_data['gp_image'];
	}
}

$custom_lable_array = $custom_lable->language;

$employement_type = $this->common_front_model->get_list('employement_list','','','array','','1');
$employement_type_data = json_encode($employement_type);

$job_type_master = $this->common_front_model->get_list('job_type_list','','','array','','1'); 
$job_type_master_data = json_encode($job_type_master);  

$shift_list = $this->common_front_model->get_list('shift_list','','','array','','1'); 
$shift_list_data = json_encode($shift_list);
?>
<style>
.select2-container--default .select2-selection--single
{
	   border: 1px solid #aaa !important;
}
.select2-search__field{
	width:100% !important;
}
</style>
<?php 

/*echo "<pre>";
print_r($employement_type);
echo "</pre>";
echo "<pre>";
print_r($employement_type_data);
echo "</pre>";
echo "<pre>";
print_r($job_type_master);
echo "</pre>";

echo "<pre>";
print_r($job_type_master_data);
echo "</pre>";

echo "<pre>";
print_r($shift_list );
echo "</pre>";

echo "<pre>";
print_r($shift_list_data);
echo "</pre>";*/
?>
<div id="titlebar" class="photo-bg single submit-page" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/add-resume.png);background-size:cover;">

<div class="container">
		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-file-text"></i>  <?php echo $custom_lable_array['add_resume_page_title']; ?></h2>
			<span style="margin-left:9%;"> <?php echo $custom_lable_array['add_resume_page_title_text']; ?></span>
		</div>

	</div>
</div>
 
<!--<div class="container">
	<div class="sixteen columns">
		<div class="submit-page">
			<div class="row notification notice margin-bottom-30 text-center" style="border:1px solid;">
				<div class="col-md-9 col-sm-12 col-xs-12">
					<p><i class="fa fa-2x fa-hand-o-right" aria-hidden="true"></i><span style="marfin-top:-5px;"> <a href="<?php //echo $base_url; ?>sign-up/login"> <u><?php //echo $custom_lable_array['account_ask_msg']; ?></u> </a> </span> <?php //echo $custom_lable_array['account_msg']; ?></p>
				</div>
				
				
			</div>
		</div>
	</div>
	<hr>
</div>-->

<div class="container" id="for_scrool">
    <div class="row">
		<section>
			<div class="wizard">
				<div class="wizard-inner">
					<div class="connecting-line"></div>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo $custom_lable_array['personal_info_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo $custom_lable_array['edu_qualification_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-education"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?php  echo $custom_lable_array['work_experiance']; ?>">
								<span class="round-tab">
									<i class="fa fa-building" aria-hidden="true"></i>
								</span>
							</a>
						</li>
                       
						<li role="presentation" class="disabled">
							<a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="<?php  echo $custom_lable_array['js_details']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>	
						<li role="presentation" class="disabled" style="font-size:5px!important;">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="<?php echo $custom_lable_array['upload_resume_pro_pic']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-open-file"></i>
								</span>
							</a>
						</li>
                        <li role="presentation" class="disabled hidden">
							<a href="#complete_reg" data-toggle="tab" aria-controls="complete" role="tab">
								<span class="round-tab">
									<i class="glyphicon glyphicon-open-file"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>
				
				       
					<div class="tab-content" >
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                
                      
                        <div class="tab-pane active" role="tabpanel" id="step1">
                          <form class="well" method="post" id="personal_info_form" >
							<div class="step2">
								<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
									<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php echo $custom_lable_array['personal_info_lbl']; ?></strong></div>
									<div class="panel-body" style="background-color:">
										<!--<div class="row">
											<div class="margin-top-20"></div>
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>First Name<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="fname" type="text" class="form-control" name="first name" placeholder="First Name" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Last Name</h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="lname" type="text" class="form-control" name="last name" placeholder="Last Name" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>-->

										<div class="margin-top-20"></div>
										<!--<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php //echo $custom_lable_array['gender_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
														<div id="radioBtn" class="btn-group">
															<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y" data-value='Male' style="font-size: 14px;padding: 5px 26px;"><?php //echo $custom_lable_array['gender_lbl_male']; ?></a>
															<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N" data-value='Female' style="font-size: 14px;padding: 5px 24px;"><?php //echo $custom_lable_array['gender_lbl_female']; ?></a>
														</div>
														<input type="hidden" name="gender" id="gender" value="Male">
													</div>
												</div>
											</div>
										</div>-->

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['m_status_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="form-group">
														<div id="radioBtn1" class="btn-group">
                                                        <?php 
														if(isset($marital_status) && is_array($marital_status) && count($marital_status) > 0)
														{
															foreach ($marital_status as $m_status)
															{ 
														?>
                                                        <a class="btn btn-primary btn-sm <?php if($m_status['marital_status']=='Unmarried') {echo "active";} else {echo "notActive";}?>" data-toggle="happy1" data-title="<?php echo $m_status['id']; ?>" data-value="<?php echo  $m_status['id']; ?>" style="font-size: 14px;padding: 5px 15px;"><?php echo $m_status['marital_status']; ?></a>
														<?php 
															}
														}
														?>
														</div>
														<input type="hidden" name="marital_status" id="m_status" value="3">
													</div>
												</div>
											</div>
										</div>

										<!--<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Age<span class="red-only"> *</span></h5>
														<select name="Age" id="Age" class="city" style="padding:10px;border-radius:0;">
															<option value="Age">Select Age</option>
															<option value="15 - 20">15 - 20</option>
															<option value="21 - 25">21 - 25</option>
															<option value="26 - 30">26 - 30</option>
															<option value="31 - 35">31 - 35</option>
															<option value="Above 35...">Above 35...</option>
														</select>
												</div>
											</div>
										</div>-->

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['dob_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                      <input type="text" name="birthdate" autocomplete="off" class="form-control datepicker" id="exampleInputDOB1" style="border-radius:0px;"data-date-format="yyyy-mm-dd" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['dob_lbl'];  ?>" onkeydown="return false" data-validation="required">
													</div>
                                                    </div>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-7">
													<h5><?php echo $custom_lable_array['mobile_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="col-md-4 col-xs-12" style="margin-left:-15px;">
														<div class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                            <select  class="form-control" name="mobile_c_code" style="height:42px;" data-validation="required">
                                                            <?php
															if(isset($country_code) && is_array($country_code) && count($country_code) > 0)
															{
																foreach ($country_code as $code)
																{ ?>
																	<option value="<?php echo $code['country_code'];?>"><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
															<?php 
																}
															}
															?>
                                                            </select>
															<!--<input id="mno" type="text" class="form-control" name="last name" placeholder="+91" style="padding:9px 0 9px 10px;">-->
														</div>
													</div>
													<div class="col-md-7" style="margin-left:-15px;">
														<input id="mobile" type="text" class="form-control" name="mobile" placeholder="<?php echo $custom_lable_array['mobile_palce_h_lbl']; ?>" style="border-radius:0px;"data-validation="required,number,length"  data-validation-length="10-13" data-suggestions="<?php $custom_lable_array['mobile_charecter_limit_info'];  ?>" >
													</div>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['country_lbl']; ?><span class="red-only"> *</span></h5>
														<select name="country" id="country" class="form-control city ketan" style="padding:10px;border-radius:0;" data-validation-skipped='0' onChange="dropdownChange('country','city','city_list');" data-validation="required">
                                                        <?php echo $this->common_front_model->get_list('country_list','str'); ?>
														</select>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['city_lbl']; ?><span class="red-only"> *</span></h5>
														<select name="city" id="city" class="city form-control" style="padding:10px;border-radius:0;" data-validation="required" >
                                                        <option value="">Select City</option>
															
														</select>
												</div>
											</div>
										</div>
                                     <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['pincode']; ?></h5>										<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="pincode" class="form-control" id="pincode" style="border-radius:0px;" data-validation="number,length" data-validation-optional="true" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['pincode'];  ?>"  data-validation-length="4-10" >
													</div>
                                                    </div>
												</div>
											</div>
										</div>   
									<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												
                                                <div class="col-md-6">
                                                <div class="form-group">
													<h5><?php echo $custom_lable_array['address_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                        <textarea style="border-radius:0px;"rows="2" cols="2" name="address" class="form-control" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['address_lbl'];  ?>"></textarea>
													</div>
												</div>
                                                </div>
											</div>
										</div>
										<!--<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Nationality<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="nationality" type="text" class="form-control" name="last name" placeholder="Nationality" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Email address<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
														<input id="emailid" type="text" class="form-control" name="first name" placeholder="abc@gmail.com" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Conform Email address<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
														<input id="cemailid" type="text" class="form-control" name="first name" placeholder="abc@gmail.com" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>-->
									</div>
								</div>
							</div>

							<div class="margin-top-20"></div>
							<ul class="list-inline pull-right">
								<li><input type="submit" class="btn btn-primary next-step" value="<?php  echo $custom_lable_array['save_continue_lbl']; ?>"></li>
							</ul>
                            
                            <input name="action" type="hidden"  value="add_personal_detail" />
                            <input name="user_agent" type="hidden"  value="NI-WEB" /> 
                           <div class="clearfix"></div>
					        </form> 
						</div>
                        <div class="tab-pane " role="tabpanel" id="step2">
                          <form class="well" method="post" id="education_form">
							<div class="step2">
								<!--style="background-color:#337AB7;"-->
								<div class="panel panel-default">
									<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php echo $custom_lable_array['edu_qualification_lbl']; ?></strong></div>
									<div class="panel-body">
										
                                        <?php // echo  $this->sign_up_model->edu_qualification_html(); ?>
										
                                        <div id="education_fields">

										</div>
                                        
										<div class="margin-top-10"></div>
										<div class="clear"></div>
										<div class="margin-top-10"></div>
										<div class="input-group-btn text-center " id="add_more_edu_detail">
											<button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_array['more_edu_btn_lbl']; ?></button>
											<div class="margin-bottom-20"></div>
										</div>
                                        
                                        <div id="certificate_fields">

										</div>
                                        
                                        <div class="input-group-btn text-center " id="add_more_certi_detail">
											<button class="btn btn-success" type="button"  onclick="certificate_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_array['add_cerificate']; ?></button>										<div class="margin-bottom-20"></div>
										</div>
                                        
										
										<div class="form with-line">
											<h5><?php echo $custom_lable_array['url(s)']; ?><span>(<?php echo $custom_lable_array['optional_lbl']; ?>)</span></h5>
											<div class="form-inside">
												<div id="add_url_err_msg" class="alert alert-danger" style="display:none;"></div>
												<!-- Adding URL(s) url-box remove-box-->
												<div id="add_url_div">
                                                <div class="form boxed box-to-clone " id="social_option1">
													<a href="javascript:;" onClick="remove_box('1');" class="close-form  button"><i class="fa fa-close"></i></a>
                                                <?php /*?><div class="row">
											       <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><!--<span class="red-only"> *</span>--></h5>
                                                    <!--data-validation="required"-->
														<select  id="social_val_option1" class="city chosen-select"  style="padding:10px;border-radius:0;" name="facebook_url" onChange="create_social_name(this.value,'social_val_option1');" > 
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													  <option value="facebook_url" selected ><?php echo $custom_lable_array['facebook']; ?></option>
                                                      
														</select>
												</div>
											</div>
										         </div><?php */?>
									            <div class="margin-top-20"></div>
                                                     
									             <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url_facebook']; ?><!--<span class="red-only"> *</span>--></h5>
													
                                                    <input class=" form-control" type="text"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url_facebook'] .' '. $custom_lable_array['url_note'];  ?>" name="name_facebook_url" data-validation-optional="true"  data-validation="url" value="" id="social_val_option1_name"/ style="">	
												</div>
											</div>
										</div>
                                                <div class="margin-top-20"></div>
                                        
                                                  <?php /*?><div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><!--<span class="red-only"> *</span>--></h5> <!--data-validation="required" -->
													<select  id="social_val_option2" class="city chosen-select" style="padding:10px;border-radius:0;" name="gplus_url" onChange="create_social_name(this.value,'social_val_option2');">
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													  
                                                      <option value="gplus_url" selected ><?php echo $custom_lable_array['gplus']; ?></option>
                                                      
														</select>	
												</div>
											</div>
										</div><?php */?>
									              <div class="margin-top-20"></div>
                                                     <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url_gplus']; ?><!--<span class="red-only"> *</span>--></h5>
													
                                                    <input class=" form-control" type="text"   data-validation="url" value="" id="social_val_option2_name" style=""  data-validation-optional="true" name="name_gplus_url"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url_gplus'] .' '. $custom_lable_array['url_note'];  ?>"/>	
												</div>
											</div>
										</div>
									              <div class="margin-top-20"></div>
                                          
                                                   <?php /*?><div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><!--<span class="red-only"> *</span>--></h5> <!--data-validation="required"-->
														<select  onChange="create_social_name(this.value,'social_val_option3');" name="twitter_url" id="social_val_option3" class="city chosen-select" style="padding:10px;border-radius:0;"  >
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													 
                                                      <option value="twitter_url" selected><?php echo $custom_lable_array['twitter']; ?></option>
                                                     
														</select>
												</div>
											</div>
										</div><?php */?>
									               <div class="margin-top-20"></div>
                                                     <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url_twitter']; ?><!--<span class="red-only"> *</span>--></h5>
													
                                                    <input class=" form-control" style="" type="text" id="social_val_option3_name" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url_twitter'] .' '. $custom_lable_array['url_note'];  ?>"  data-validation="url" data-validation-optional="true" name="name_twitter_url" value=""/>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                         
                                                   <?php /*?><div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><!--<span class="red-only"> *</span>--></h5> <!--data-validation="required"-->
														<select onChange="create_social_name(this.value,'social_val_option4');"  name="linkedin_url" id="social_val_option4" class="city chosen-select" style="padding:10px;border-radius:0;"  >
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
                                                      <option value="linkedin_url" selected><?php echo $custom_lable_array['linkedin']; ?></option>
														</select>
												</div>
											</div>
										</div><?php */?>
									               <div class="margin-top-20"></div>
                                                     <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url_linkedin']; ?><!--<span class="red-only"> *</span>--></h5>
													
                                                    <input class=" form-control" type="text" id="social_val_option4_name" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url_linkedin'] .' '. $custom_lable_array['url_note'];  ?>" style=""  data-validation="url" data-validation-optional="true" name="name_linkedin_url" value=""/>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                         
												</div>
                                                
                                                 
                                                
                                                     
                                                
                                                
                                                </div>
                                                
                                              <div class="margin-top-10"></div>  
												
											<a href="javascript:;" class="button gray add-url " onClick="add_social_url_option();" style="border:2px solid;"><i class="fa fa-link" aria-hidden="true" style="font-size:20px;"></i><?php echo $custom_lable_array['add_url']; ?> </a>	<p class="note bg-info"><?php echo $custom_lable_array['add_url_msg']; ?> </p>
											
										 </div>
											</div>
										</div>
									</div>
								<!--<div class="panel-footer"><small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php echo $custom_lable_array['another_field']; ?>  :)</small>, <small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-minus gs"></span> <?php echo $custom_lable_array['remove_field']; ?>  :)</small>
									</div>-->
							
								<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-default prev-step"> <?php  echo $custom_lable_array['previous']; ?></button></li>
								<li><input type="submit" class="btn btn-primary next-step" value="<?php  echo $custom_lable_array['save_continue_lbl']; ?>"/></li>
							</ul>
                            <div class="clearfix"></div>
                          <input name="no_of_edu_row" id="no_of_edu_row" type="hidden"  value="0" />
                          <input name="no_of_certi_row" id="no_of_certi_row" type="hidden"  value="0" />
                          <input name="add_url_list_row" id="add_url_list_row" type="hidden"  value="0" />
                          <input name="action" type="hidden"  value="add_education_detail" />
                           <input name="user_agent" type="hidden"  value="NI-WEB" />
                           </div>  
                        </form>   
						</div>
                       <div class="tab-pane " role="tabpanel" id="step3">
                          <form class="well" method="post" id="work_exp_form">
							<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
								<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php  echo $custom_lable_array['work_experiance']; ?></strong></div>
                            
								<div class="panel-body work-exp-html">
									
								<?php //echo  $this->sign_up_model->work_exp_html(); ?>
                                
                                <div id="work_fields">
								  </div>	
                                    
                                <div class="margin-top-10"></div>
								<div class="input-group-btn text-center" id="add_more_work_exp">
										<button class="btn btn-success" type="button"  onclick="work_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php echo $custom_lable_array['more_exp'] ?></button>
                                        <div class="margin-bottom-20"></div>
								</div>
								</div>
								
                                <!--<div class="margin-top-30"></div>
									<div class="col-sm-4 nopadding">
										<h6><?php  //echo $custom_lable_array['notice_period']; ?><span class="red-only"> *</span></h6>
									    <div class="form-group">
											<input type="text" class="form-control" id="NoticePeriod"  name="NoticePeriod[]" value="" placeholder="Days and months"  style="border-radius:0px;"/>
									    </div>
									</div>-->
                                    
                                  
									<div class="margin-bottom-20"></div>
									<!--<div class="panel-footer"><small><?php //echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php //echo $custom_lable_array['another_field']; ?>:)</small>, <small><?php //echo $custom_lable_array['press']; ?> <span class="glyphicon glyphicon-minus gs"></span> <?php //echo $custom_lable_array['remove_field']; ?>  :)</small></div>-->
									
									<ul class="list-inline pull-right margin-top-20">
									<li><button type="button" class="btn btn-default prev-step"><?php echo $custom_lable_array['previous']; ?></button></li>	
										<li><button type="button" id="skip_work_exp" class="btn btn-default next-step"><?php echo $custom_lable_array['skip']; ?></button></li>
										<li><input type="submit" id="save_work_exp_data" class="btn btn-primary btn-info-full next-step" value="<?php echo $custom_lable_array['save_continue_lbl']; ?>" > </li>
									</ul>
								
                       <input name="no_of_work_exp_row" id="no_of_work_exp_row" type="hidden"  value="0" />
                       <input name="action" type="hidden"  value="add_work_exp_detail" />
                       <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </div>
                               <div class="clear"></div>
                        </form>
                        </div>
					    <div class="tab-pane " role="tabpanel" id="step4">
						  <form class="well" method="post" id="js_details">
							<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
								<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php  echo $custom_lable_array['js_details']; ?></strong></div>
                            
								<div class="panel-body work-exp-html">
								
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['home_city']; ?><span class="red-only"> *</span></h5>
                                                   
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="home_city" class="form-control tokenfieldwidth" id="home_city" style="border-radius:0px;" data-validation="required" placeholder="Add Home City By Typing Here">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                
                                       
                               <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['landline']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="landline" class="form-control" id="landline" style="border-radius:0px;" data-validation="number,length" data-validation-optional="true" data-validation-length="6-13" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['landline'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['website']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="website" class="form-control" id="website" style="border-radius:0px;" data-validation="url" data-validation-optional="true"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['website'];  ?>"><!--data-validation="url"-->
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['profile_summary']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <textarea style="border-radius:0px;"rows="2" cols="2" name="profile_summary" class="form-control" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['profile_summary'];  ?>"></textarea>
													</div>
                                                    </div>
												</div>
											</div>
										</div>                            
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['resume_headline']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <textarea style="border-radius:0px;"rows="2" cols="2" name="resume_headline" class="form-control" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['resume_headline'];  ?>" ></textarea>
													</div>
                                                    </div>
												</div>
											</div>
										</div>  
                                
                                
                                <div class="row">
                                     <div class="col-md-12">
										<div class="col-md-3"></div>
                                       <div class="col-sm-6 nopadding">
									  <h6><?php echo $custom_lable_array['total_experience'] ?></h6>
									  <div class="form-group">
									  
									  <div class="col-md-6 col-sm-6 nopadding tufelpaleft15" style="padding-left:0px;">
                                      <!--onChange="check_user_exp_or_fresher();"-->
                                        <select name="exp_year" id="exp_year"  class="form-control" style="border-radius:0px;" >
											<option value=""><?php echo $custom_lable_array['year'] ?></option>
                                           <?php
											for($i=0;$i <= 25 ; $i++)
											{ ?>
												<option value="<?php echo  $i; ?>" ><?php echo  $i; ?> </option>	
										<?php	}
											?>
										</select>
                                        </div>
									 <div class="col-md-6 col-sm-6 nopadding tufelparight15" style="padding-right:0px;">
                                        <select  name="exp_month"  id="exp_month"  class="form-control" style="border-radius:0px;"  > <!--onChange="check_user_exp_or_fresher();"-->
										    <option value=""><?php echo $custom_lable_array['month'] ?></option>
											 <?php
											for($i=0;$i <= 11 ; $i++)
											{ ?>
												<option value="<?php echo  $i; ?>" ><?php echo  $i; ?> </option>	
										<?php	}
											?>
										</select>
                                        </div>
                                        
									  </div>
									</div>
                                     </div>
                                   </div>
         				         	
                                <div class="row" id="annual_salary_div">
                                     <div class="col-md-12 ">
										<div class="col-md-3"></div>
                                       <div class="col-sm-6 nopadding">
									  <h6><?php if(isset($custom_lable_array['current_annual_salary']))
									           {
												   echo $custom_lable_array['current_annual_salary'];
											   }?></h6>
									  <div class="form-group">
										<div class="input-group">
										  <span class="input-group-addon"></span>
										   <select name="annual_salary" id="annual_salary" data-validation="required" class="form-control" >
										 <?php echo  $this->common_front_model->get_list('salary_range_list','str'); ?>
										 </select>
										</div>
									  <?php /*?><div class="col-sm-4 nopadding tufelpaleft15" style="padding-left:0px;">
                                        <select name="currency_type"  class="form-control" >
											<?php echo  $this->common_front_model->get_list('currency_master','str'); ?>
										</select>
                                        </div>
									  
										<div class="col-sm-4 nopadding">
                                        <select name="a_salary_lacs"  class="form-control" >
										    <option value=""><?php echo $custom_lable_array['lacs'] ?></option>
											 <?php 
                                             for($i=0;$i <= 99 ; $i++)
											 { ?>
												 <option value="<?php echo $i ?>"><?php echo $i ?> </option>
											 <?php } ?>
										</select>
                                        </div>
                                        <div class="col-sm-4 nopadding tufelparight15" style="padding-right:0px;">
                                        <select name="a_salary_thousand" class="form-control" >
										    <option value=""><?php echo $custom_lable_array['thousand'] ?></option>
											 <?php
											 for($i=0;$i <= 90;)
											 {  ?>
												  <option value="<?php echo $i ?>"><?php echo $i ?> </option>
											<?php $i = $i+5; }
											 ?>
										</select>
                                        </div><?php */?>
                                        
									  </div>
									</div>
                                     </div>
                                   </div> 
                                      	
                                <div class="row" id="industry_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['type_industry']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="industry" id="industry" data-validation="required" class="form-control" >
                                                     <?php echo  $this->common_front_model->get_list('industries_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row" id="functional_area_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['functional_area']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select onChange="dropdownChange('functional_area','job_role','role_master');" name="functional_area" id="functional_area" data-validation="required" class="form-control" >
                                                     <?php echo  $this->common_front_model->get_list('functional_area_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row" id="job_role_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_role']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="job_role" id="job_role"  class="form-control" >
                                                     <?php // echo  $this->common_front_model->get_list('role_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div> 
                              <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['preferred_city']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      
                                                      <select name="preferred_city[]" id="preferred_city" class="select2" style="width:100%;" data-validation="required" multiple data-placeholder="<?php echo $custom_lable_array['select'] .' '. $custom_lable_array['preferred_city'];  ?>">
                                                     
                                                      </select>
                                                     <!-- <input type="text" name="preferred_city" class="form-control tokenfieldwidth" id="preferred_city" style="border-radius:0px;" data-validation="required" placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['preferred_city'];  ?>">-->
													</div>
                                                    </div>
												</div>
											</div>
										</div>             
                            		 <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['skill']; ?><span class="red-only"> *</span></h5>
													<div class="input-group bs-example">
													  <span class="input-group-addon"></span>
                                                       <input type="text" name="key_skill" class="form-control tokenfieldwidth" id="key_skill" style="border-radius:0px;" data-validation="required"  placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['skill'];  ?>Find Specify Skill By Typing Here">
                                                     
													</div>
                                                    </div>
												</div>
											</div>
										</div>   
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['desire_job_type']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="desire_job_type" class="form-control tokenfieldwidth" id="desire_job_type" style="border-radius:0px;" data-validation="required"   placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['desire_job_type'];  ?>Find Specify Desire Job Type By Typing Here">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['employment_type']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="employment_type" class="form-control tokenfieldwidth" id="employment_type" style="border-radius:0px;" data-validation="required" placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['employment_type'];  ?>Find Specify Employment Type By Typing Here">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                              			<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['prefered_shift']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                     <input type="text" name="prefered_shift" class="form-control" id="prefered_shift" style="border-radius:0px;" data-validation="required" placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['prefered_shift'];  ?>Find Specify Preferred Shift By Typing Here">
                                                </div>
                                                </div>
												</div>
											</div>
										</div>                                          
                                   
                                   
                                   <div class="row" >
                                     <div class="col-md-12">
										<div class="col-md-3"></div>
										<div class="col-sm-6">
										  <h6><?php echo $custom_lable_array['expected_annual_salary'] ?><span class="red-only"> *</span></h6>
											
											<div class="input-group">
											  <span class="input-group-addon"></span>
											   <select name="expected_annual_salary" id="expected_annual_salary" data-validation="required" class="form-control" >
											 <?php echo  $this->common_front_model->get_list('salary_range_list','str'); ?>
											 </select>
											</div>
											<?php /*?><div class="col-sm-4 nopadding tufelpaleft15" style="padding-left:0px;">
											<select name="exp_salary_currency_type" data-validation="required" class="form-control" >
												<?php echo  $this->common_front_model->get_list('currency_master','str'); ?>
											</select>
											</div>
										  
											<div class="col-sm-4 nopadding">
											<select name="exp_salary_lacs" data-validation="required" class="form-control" >
												<option value=""><?php echo $custom_lable_array['lacs'] ?></option>
												 <?php 
												 for($i=0;$i <= 99 ; $i++)
												 { ?>
													 <option value="<?php echo $i ?>"><?php echo $i ?> </option>
												 <?php } ?>
											</select>
											</div>
											
											<div class="col-sm-4 nopadding tufelparight15" style="padding-right:0px;">
											<select name="exp_salary_thousand" data-validation="required" class="form-control" >
												<option value=""><?php echo $custom_lable_array['thousand'] ?></option>
												 <?php
												 for($i=0;$i <= 90;)
												 {?>
													  <option value="<?php echo $i ?>"><?php echo $i ?> </option>
												<?php  $i = $i+5; }
												 ?>
											</select>
											</div><?php */?>
                                        
										</div>
                                     </div>
                                   </div>  
                                         
                                <div class="margin-top-10"></div>
									<!--<div class="input-group-btn text-center">
										<button class="btn btn-success" type="button"  onclick="work_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php //echo $custom_lable_array['more_exp'] ?></button>
									</div>	-->
								</div>
								
                                
									<div class="margin-bottom-20"></div>
									<!--<div class="panel-footer"><small><?php //echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php //echo $custom_lable_array['another_field']; ?>:)</small>, <small><?php //echo $custom_lable_array['press']; ?> <span class="glyphicon glyphicon-minus gs"></span> <?php //echo $custom_lable_array['remove_field']; ?>  :)</small></div>-->
									
									<ul class="list-inline pull-right margin-top-20">
									<li><button type="button" class="btn btn-default prev-step"><?php echo $custom_lable_array['previous']; ?></button></li>	
										<!--<li><button type="button" id="skip_work_exp" class="btn btn-default next-step"><?php //echo $custom_lable_array['skip']; ?></button></li>-->
										<li><input type="submit" class="btn btn-primary btn-info-full next-step" value="<?php echo $custom_lable_array['save_continue_lbl']; ?>"> </li>
									</ul>
								
                       <input name="action" type="hidden"  value="js_details" />
                       <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </div>
                               <div class="clear"></div>
                        </form>
						</div>
                        <div class="margin-top-20"></div>
						<div class="tab-pane" role="tabpanel" id="complete">
                           
                        	<div class="step44">
								<h5><?php echo $custom_lable_array['upload_resume_pro_pic']; ?></h5>
								<hr>
								<div id="accordion-container">
								<form id="js_resume_upload" method="post" enctype="multipart/form-data" >	
                                    <h2 class="accordion-header th_bgcolor"><i class="fa fa-2x fa-cloud-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload_resume']; ?></h2>
									<div class="accordion-content">
                                      <div class="col-md-12">
                               			<div class="alert alert-success" id="success_msg_resume" style="display:none;"></div>
                                  		<div class="alert alert-danger" id="error_msg_resume" style="display:none;"></div>
                                    	</div>
										<div class="row">
											<div class="col-md-12" id="resume_form">
												<div class="form-group">
													<label for="exampleInputFile"><?php echo $custom_lable_array['upload_resume']; ?></label>
													<input type="file" class="form-control-file"  aria-describedby="fileHelp" name="resume_file" id="resume_file" data-validation="required,extension,size"  data-validation-max-size="2M" data-validation-allowing="pdf, docx, doc, rtf, txt" > <!---->
													<small id="fileHelp" class="form-text text-muted red-only"><i> <?php echo $custom_lable_array['upload_resume_info']; ?>  </i></small>
												</div>

												<div class="margin-top-10"></div>
												<button  class="btn btn-md  th_bgcolor"><i class="fa fa-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload']; ?></button>
												<div class="margin-top-20"></div>

												<div class="panel panel-info">
													<div class="panel-heading">
														<h4 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_array['imp_notes']; ?></h4>
													</div>
													<div class="panel-body">
														<ul>
															<li><?php echo $custom_lable_array['upload_size_info']; ?></li>
															<li><?php echo $custom_lable_array['upload_img_frmt_info']; ?> </li>
															
														</ul>
													</div>
												</div>
											</div>
                                            <div class="col-md-12" id="resume_message" style="display:none;">
                                       		  <h4 class="panel-title" style="margin:-10px 0px;">Resume Uploaded</h4>
                                     		</div>
										</div>
									</div>
                                    <input type="hidden" name="action" value="upload_img" >
                                    <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </form>
								<form id="js_img_upload" method="post" enctype="multipart/form-data" >	
                                
                                	<h2 class="accordion-header th_bgcolor"><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></i> <?php echo $custom_lable_array['upload_pro_pic']; ?></h2>
									<div class="accordion-content">
										<div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success" id="success_msg_img_upload" style="display:none"></div>
                                            <div class="alert alert-danger" id="error_msg_img_upload" style="display:none"></div>
                                    	</div>
											<div class="col-md-12" id="img_upload_form">
												<div class="form-group">
													<label for="exampleInputFile"><?php echo $custom_lable_array['upload_pro_pic']; ?></label>
                                                    
                                                    <?php
													if(isset($social_image) && $social_image!='')
													{
														$def_img = $social_image;
														?>
                                                        
                                <h2 style="color:white;font-size:14px;margin-bottom:10px;" class="th_bgcolor"><?php echo $custom_lable_array['upload_pro_pic_social_info']; ?></h2>
                                                        <?php
													}
													else
													{
														$def_img = $base_url.'assets/front_end/images/demoprofileimge.png';	
													}
													?>
													<img src="<?php echo $def_img; ?>" id="blah" class="blah view_preview_img" alt="your image" />
													<input type="file" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" name="profile_pic" id="profile_pic" data-validation="required,extension,size"  data-validation-max-size="2M" data-validation-allowing="jpg, png, gif, jpeg " > <!---->
													<small id="file" class="form-text text-muted red-only"><i> <?php echo $custom_lable_array['upload_proc_pic_info']; ?></i></small>
												</div>

												<div class="margin-top-10"></div>
												<button type="submit"  class="btn btn-md th_bgcolor"><i class="fa fa-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload']; ?></button>

												<div class="margin-top-10"></div>
												<div class="panel panel-info">
														<div class="panel-heading">
															<h3 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_array['imp_notes']; ?></h3>
														</div>
														<div class="panel-body">
															<ul>
																<li><?php echo $custom_lable_array['upload_size_info']; ?></li>  															<li><?php echo $custom_lable_array['upload_pro_img_frmt_info']; ?></li>
																
															</ul>
														</div>
												</div>
                                                 </div> 
                                                <div class="col-md-12" id="img_upload_message" style="display:none;">
                                               <h4 class="panel-title" style="margin:-10px 0px;">Profile Pic Uploaded</h4>
											</div>
										</div>
									</div>
                                 <input type="hidden" name="action" value="upload_img" >
                                    <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </form>    
								</div>
							</div>
							<div class="margin-top-20"></div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-default prev-step previous_photo_upload"><?php echo $custom_lable_array['previous']; ?></button></li>
								<li><button type="button" class="btn btn-default next-step skip_photo_upload"><?php echo $custom_lable_array['skip']; ?></button></li>
								<li><button  class="btn th_bgcolor btn-info-full next-step finish_photo_upload" ><?php echo $custom_lable_array['finish']; ?></button></li>
							</ul>
                        	 <!--<div class="step44">
								<div class="panel panel-success">
								  <div class="panel-heading">Upload Completed</div>
								  <div class="panel-body">Resume Upload Successfully...</div>
								</div>
							</div>-->
							<div class="divider margin-top-0 padding-reset"></div>
							<!--<a href="#" class="button big margin-top-5"><?php //echo $custom_lable_array['preview']; ?> <i class="fa fa-arrow-circle-right"></i></a>-->
                            
						</div>
                      
                      <div class="tab-pane active" role="tabpanel" id="complete_reg">
                        <div class="step44">
                            <div class="panel panel-success">
                              <div class="panel-heading"> <?php echo $custom_lable_array['sign_up_js_complete']; ?>! </div>
                              <div class="panel-body"><?php echo $custom_lable_array['sign_up_js_complete_mes']; ?></div>
                            </div>
					   </div>
                      </div>

						<div class="clearfix"></div>
					</div>
			</div>
            <input type="hidden" name="remove_previous_disbled" id="remove_previous_disbled"> 
            <input type="hidden" name="store_selected_optgroup" id="store_selected_optgroup"> 
		</section>
   </div>
</div>
<link href="<?php echo $base_url;?>assets/front_end/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css" type="text/css" rel="stylesheet">


<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>-->
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8">
</script>
<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-datepicker_for_conf.min.js"></script>
<script>


function time_out_message()
{
	settimeout_div('success_msg',8000);
	settimeout_div('error_msg',8000);
}

/*function check_user_exp_or_fresher()
{ 
	var exp_year = $('#exp_year').val();
    var exp_month = $('#exp_month').val();
	if( exp_year > 0 || exp_month > 0 )
	{
		$('#industry_div').slideDown();
		$('#functional_area_div').slideDown();
		$('#job_role_div').slideDown();
		$('#annual_salary_div').slideDown();
	}
	else
	{
		$('#industry_div').slideUp();
		$('#functional_area_div').slideUp();
		$('#job_role_div').slideUp();
		$('#annual_salary_div').slideUp();
		$('#functional_area').val('');
		$('#job_role').val('');
		$('#a_salary_lacs').val('');
		$('#a_salary_thousand').val('');
	}
	
}*/

$('#social_option1,#social_option2,#social_option3,#social_option4,#step4,#step2,#step3').on('change', function () {
	$('.chosen-select').chosen();
	//$('.chosen-select', this).chosen('destroy').chosen();
	//$('.chosen-select', this).chosen().trigger('chosen:updated');
	//$('.chosen-select').chosen();
	//$('.chosen-select', this).trigger('chosen:updated');
});

$('#social_option1,#social_option2,#social_option3,#social_option4,#step4,#step2,#step3').on('change', function () {
	//$('.chosen-select', this).chosen('destroy').chosen();
	//$('.chosen-select', this).chosen();
	//$('.chosen-select', this).trigger('chosen:updated');
});


$('#key_skill').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_list/skill_list/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#key_skill').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});


function get_suggestion_list(list_id,return_val,get_list)
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'sign_up/get_suggestion_list/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
			return_val: return_val,
			get_list: get_list,
		  };
		},
	  }
	});
}	


/*$('#key_skill').tokenfield({
    autocomplete: {
    source: <?php //echo $key_skill_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#key_skill').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});*/

$('#employment_type').tokenfield({
    autocomplete: {
    source: <?php echo $employement_type_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#employment_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

$('#desire_job_type').tokenfield({
    autocomplete: {
    source: <?php echo $job_type_master_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#desire_job_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

$('#prefered_shift').tokenfield({
    autocomplete: {
    source: <?php echo $shift_list_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#prefered_shift').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

/*$('#home_city').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php //echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#home_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
});  */

/*$('#preferred_city').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php //echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#preferred_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
*/


$('#skip_work_exp').click(function()
{
	for_active_tab();
	scroll_to_div('for_scrool',-100);
});

$('.skip_photo_upload,.finish_photo_upload').click(function()
{
	scroll_to_div('for_scrool',-100);
	$('#complete').slideUp();
	$("#complete_reg").slideDown();
	$('#step1,#step2,#step3,#step4,#complete').hide();
});



function create_social_name(val,social_id)
{   
	$('#'+social_id).attr('name',val);
	$('#'+social_id+'_name').attr('name',"name_"+val);
}
var icon = 0
function add_social_url_option()
{ icon++;
show_comm_mask();
 $('#add_url_div').slideDown();
 $('#social_option1,#social_option2,#social_option3,#social_option4').slideDown();
hide_comm_mask();
return false;
  show_comm_mask();
  $('.chosen-select').chosen();
   var new_add_url = parseInt($('#add_url_list_row').val());
	$('#add_url_err_msg').slideUp();
	if(new_add_url >= 4)
	{
		$('#add_url_err_msg').html('<?php echo $custom_lable_array['add_url_err_msg']; ?>');
		$('#add_url_err_msg').slideDown();
		scroll_to_div('add_url_err_msg',-100);
		hide_comm_mask();
		return false;
	}
	if(new_add_url == 0)
	{
		var new_add_url = 1 ;
		var field_row = new_add_url ;
	}
	else
	{
		var field_row = new_add_url + parseInt(1) ;
	}
	
		$('#social_option'+field_row).slideDown();
		scroll_to_div('social_option'+new_add_url,-100);
		$('#social_option1,#social_option2,#social_option3,#social_option4').trigger('change');
	
	
    $('#add_url_list_row').val(field_row);
	hide_comm_mask();
}

function remove_box(box_id)
{
	show_comm_mask();
	$('#add_url_div').slideUp();
	$('#social_option1,#social_option2,#social_option3,#social_option4').slideUp();
	hide_comm_mask();
	return false;
	var url_val = parseInt($('#add_url_list_row').val());
	if(url_val > box_id)
	{
		scroll_to_div('add_url_err_msg',-100);
		$('#add_url_err_msg').slideDown();
		$('#add_url_err_msg').html('<?php echo $custom_lable_array['remove_url_warning']; ?>');
		hide_comm_mask();
		return false;
	}
	$('#social_val_option'+box_id).val('');
	$('#social_val_option'+box_id).val('');
	$('#social_val_option'+box_id).trigger('chosen:updated');
	$('#social_val_option'+box_id+'_name').val('');
	$('#social_option'+box_id).hide();
	var new_val_url = url_val - parseInt(1) ;
	$('#add_url_list_row').val(parseInt(new_val_url));
	hide_comm_mask();
}

	
var field_row = $('#no_of_edu_row').val();
function education_fields() 
{ 
	show_comm_mask();
	var edu_val = parseInt($('#no_of_edu_row').val());
	
	if(edu_val == 0)
	{
		var edu_val = 1 ;
		var field_row = edu_val ;
	}
	else
	{
		var field_row = edu_val + parseInt(1) ;
	}
    
    $('#no_of_edu_row').val(parseInt(field_row));
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "");
	//var rdiv = 'removeclass'+field_row;
	var edu_form_data = $('#education_form [name^="institute"]').serialize();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+field_row+"&"+edu_form_data;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/edu_qualification_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#no_of_edu_row').val(field_row);
			divtest.innerHTML = data ;
			objTo.appendChild(divtest)
			if(field_row == 10)
			{
				$('#add_more_edu_detail').slideUp();
			}
			//$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 
			 });*/
			
			
			
				 for(i=1;i <= field_row ; i++)
				 {   
					
					
					$('.qualification_lvl'+i).change(function(){
						
						for(k=1;k <= field_row ; k++)
						{
							var new_k = k;
							var check_val = $('#qualification_level'+new_k).val();
							//alert(check_val);
							if(check_val == 'class-X')
							{   
								$('.specialization'+new_k).hide();
							}
							else
							{  
								$('.specialization'+new_k).show();
							}
							if(check_val == 'class-XII')
							{   
								$('#specialization'+new_k).attr('data-validation-skipped','1');
								$('#specialization'+new_k).removeAttr('data-validation','required');
								$('.red-only-sp'+new_k).hide();
							}
							else
							{  
								$('#specialization'+new_k).removeAttr('data-validation-skipped','1');
								$('#specialization'+new_k).attr('data-validation','required');
								$('.red-only-sp'+new_k).show();
							}
						}
						
						});
					
					 /*var selected = $(':selected', $('.qualification_lvl'+i));
					 var selectedtst = $(':selected', $('.qualification_lvl'+i)).val();
					 var selected_group =selected.closest('optgroup').attr('class');
					 var selected_option = $('option:selected', $('.qualification_lvl'+i));
					 var selected_option_val = selected_option.closest('select').attr('id');
					   for(s=1;s<= field_row ; s++)
						 { 
							 rr = s + parseInt(1);
							 
							 if(selected_option_val!='')
							 {
								$('.qualification_lvl'+rr+' option[value='+selected_option_val+']').attr('disabled','disabled').trigger("chosen:updated");
							 }
							 if(selected_group != 'class-X/XII')
							 {
							  
							   $('.qualification_lvl'+s+' optgroup[class='+selected_group+']').attr('disabled','disabled').trigger("chosen:updated");
							 }
							 
						 }
						 
						 if($('#'+selected_option_val).val()!='')
						 {
							 var selected_new_opt = $(':selected', $('#'+selected_option_val));
							 var selected_group_opt =selected_new_opt.closest('optgroup').attr('class');
							 if(selected_group != 'class-X/XII')
							 {
							 $('#'+selected_option_val+' optgroup[class='+selected_group+']').removeAttr('disabled').trigger("chosen:updated");
							 }
						 }*/
						 
				}
				
				hide_comm_mask();
			}
			
		
		});
		
		hide_comm_mask();
}
function remove_education_fields(rid) {
	//$('.'+rid).remove();
	show_comm_mask();
   var edu_val = parseInt($('#no_of_edu_row').val());
   var new_val = edu_val - parseInt(1) ;
   $('#no_of_edu_row').val(parseInt(new_val));
  $('.removeclass'+rid).closest('div').remove();
  
   if(new_val < 10)
	{
		$('#add_more_edu_detail').slideDown();
	}
	hide_comm_mask();
}

var certi_row = $('#no_of_certi_row').val();
function certificate_fields() 
{ 
	show_comm_mask();
	var certi_val = parseInt($('#no_of_certi_row').val());
	if(certi_val == 0)
	{
		var certi_val = 1 ;
		var field_row = certi_val ;
	}
	else
	{
		var field_row = certi_val + parseInt(1) ;
	}
   // alert(field_row);
    $('#no_of_certi_row').val(parseInt(field_row));
    var objTo = document.getElementById('certificate_fields')
    var divtest = document.createElement("div");
	//divtest.setAttribute("class", "");
	//var rdiv = 'removeclass'+field_row;
	
	var hash_tocken_id = $("#hash_tocken_id").val();
	var certi_data = $('#education_form [name^="cerificate_name"]').serialize();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+field_row+"&"+certi_data;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/certificate_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#no_of_certi_row').val(field_row);
			divtest.setAttribute("class", "removeclasscerti");
			divtest.innerHTML = data;
			objTo.appendChild(divtest)
			if(field_row == 10)
			{
				$('#add_more_certi_detail').slideUp();
			}
			
			$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 
			 });*/
			 
			 hide_comm_mask();	
		}
		});
		
		
}
function remove_certi_fields(rid) {
   show_comm_mask();
   //alert(rid);
   var certi_val = parseInt($('#no_of_certi_row').val());
   var new_val = certi_val - parseInt(1) ;
   $('#no_of_certi_row').val(parseInt(new_val));
   $('.removeclasscerti'+rid).remove();
   if(new_val < 10)
	{
		$('#add_more_certi_detail').slideDown();
	}
	hide_comm_mask();
}


function check_work_exp_date()
{
	
	$("#error_msg").slideUp();
	$('#save_work_exp_data').removeAttr("disabled");
	var exp_val = parseInt($('#no_of_work_exp_row').val());
	var l_date = 0;
	var warning = 0;
	
	for (var w=1;w <= exp_val ;w++)
	{   //alert(w);
		var l_date_val = $('#ldate'+w).val();
		var joining_date = $('#joining_date'+w).val();
	    //alert(joining_date);
		/*|| l_date_val==undefined   || l_date_val==null || typeof l_date_val == 'undefined'*/
			if(l_date_val=='')
			{
				 l_date++;
			}
			if(joining_date!='' && l_date_val!='' && joining_date!=undefined && l_date_val!=undefined && joining_date > l_date_val)
			{  
			   warning++;
			}
		
	}
	if(warning > 0)
	{
		$("#error_msg").slideDown();
		scroll_to_div("error_msg",-100);
	    $("#error_msg").html('<?php echo $custom_lable_array['warning_for_greater_join_date']; ?>');
		$('#save_work_exp_data').attr("disabled", 'disabled');
		return false;
	}
	if(l_date > 1)
	{
		$("#error_msg").slideDown();
	    $("#error_msg").html('<?php echo $custom_lable_array['specify_present_work_date']; ?>');
		scroll_to_div("error_msg",-100);
		$('#save_work_exp_data').attr("disabled", 'disabled');
	    return false;
	}
	return true;
}				




var exp_row = $('#no_of_work_exp_row').val();
function work_fields() {
	
	check_work_exp_date();
	if(check_work_exp_date()==false)
	{
		return false;
	}
	var exp_val = parseInt($('#no_of_work_exp_row').val());
    if(exp_val == 0)
	{
		var exp_val = 1 ;
		var exp_row = exp_val ;
	}
	else
	{
		var exp_row = exp_val + parseInt(1) ;
	}
    show_comm_mask();
    $('#no_of_edu_row').val(parseInt(exp_row));
    var objTo = document.getElementById('work_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "");
	//var rdiv = 'removeclass'+exp_row;
	var work_data = $('#work_exp_form [name^="company_name"]').serialize();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+exp_row+"&"+work_data;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/work_exp_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('.datepicker').trigger('click');
			
			$('#no_of_work_exp_row').val(exp_row);
			divtest.innerHTML = data ;
			objTo.appendChild(divtest)
			if(exp_row == 10)
			{
				$('#add_more_work_exp').slideUp();
			}
			//$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			$('.datepicker-join,.datepicker-leave').datepicker({
							  format: 'yyyy-mm-dd',
							  endDate: '+0d',
							   autoclose: true
							  
							});
			$('.datepicker-join,.datepicker-leave').datepicker().on('changeDate', function (ev) {
    				check_work_exp_date();
			});				
		    // $('.datepicker-join,.datepicker-leave').trigger('click');
			/*$('.datepicker-join,.datepicker-leave').click(function(){
				$('.datepicker-join,.datepicker-leave').datepicker({
							  format: 'yyyy-mm-dd',
							  endDate: '+0d',
								  autoclose: true
							  
							});
						
				});*/
			
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 // $('.chosen-select').trigger('click');
			 });*/
			
			hide_comm_mask();
			
		}
		});
	
}

function remove_work_fields(rid) {
	   show_comm_mask();
	   var exp_val = parseInt($('#no_of_work_exp_row').val());
   	   var new_val = exp_val - parseInt(1) ;
   	  $('#no_of_work_exp_row').val(parseInt(new_val));
	  $('.removeclasswork'+rid).remove();
	  if(new_val < 10)
		{
			$('#add_more_work_exp').slideDown();
		}
		check_work_exp_date();
		hide_comm_mask();
  }


function for_active_tab()
{
	$('.chosen-select').css('display','none');
	var $active = $('.wizard .nav-tabs li.active');
	$active.next().removeClass('disabled');
	nextTab($active);
	$('#step4').trigger('change');
	var tokenfieldwidth = $('.tokenfield').outerWidth();
    $('.tokenfieldwidth-tokenfield').width(tokenfieldwidth);
    $('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
}


/*$('select')
    .on('beforeValidation', function(value, lang, config) {
      $('.chosen-select').css('display','block');
	 });
$('select')
    .on('validation', function(value, lang, config) {
      $('.chosen-select').css('display','none');
	  //$('.chosen-select').trigger('click');
 });	*/



	
$.validate({
    form : '#personal_info_form',
    modules : 'security',
    onError : function($form) {
	  scroll_to_div('for_scrool',-100);
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#personal_info_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_personal_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			
			$("#hash_tocken_id").val(data.token);
			  hide_comm_mask();
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				
				var edu_val = parseInt($('#no_of_edu_row').val());
				if(edu_val == 0)
				{
					
					education_fields();
				}
				
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				//document.getElementById('personal_info_form').reset();
				scroll_to_div('for_scrool',-100);
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	});	
	
      return false; // Will stop the submission of the form
    }
  });            
  
$.validate({
    form : '#education_form',
    modules : 'security',
	onError : function($form) {
	  scroll_to_div('for_scrool',-100);	
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#education_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_education_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			  hide_comm_mask();
			if($.trim(data.status) == 'success')
			{  
				
				if(data.form_edu_id!='' && data.form_edu_id!=undefined)
				{
					jQuery.each( data.form_edu_id, function( edu_id, edu_val ) {
					  $('#form_edu_id'+edu_id).val(edu_val);
					});
				}
				
				if(data.form_certi_id!='' && data.form_certi_id!=undefined)
				{
					jQuery.each( data.form_certi_id, function( certi_id, certi_val ) {
					  $('#form_certi_id'+certi_id).val(certi_val);
					});
				}
				
				
				for_active_tab();
				var exp_val = parseInt($('#no_of_work_exp_row').val());
				if(exp_val == 0)
				{
					work_fields();
				}
				 
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				//document.getElementById('education_form').reset();
				scroll_to_div('for_scrool',-100);
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	});	
	
      return false; // Will stop the submission of the form
    }
  });
$.validate({
   form : '#work_exp_form',
    modules : 'security',
	onValidate : function($form) {
       check_work_exp_date();
	   if(check_work_exp_date()==false)
		{
			return false;
		}
     },
	onError : function($form) {
	  scroll_to_div('for_scrool',-100);
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  //check_work_exp_date();
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#work_exp_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_work_exp_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			 hide_comm_mask();
			if($.trim(data.status) == 'success')
			{
				if(data.form_work_id!='' && data.form_work_id!=undefined)
				{
					jQuery.each(data.form_work_id,function(work_id, work_val) {
					  $('#form_work_id'+work_id).val(work_val);
					});
				}
				
				if(data.get_work_id!='' && data.get_work_id!=null && data.get_work_id!=undefined)
				{
					var ind_val = $('#industry'+data.get_work_id).val();
					var fn_area_val = $('#functional_area'+data.get_work_id).val();
					var job_role = $('#job_role'+data.get_work_id).val();
					$('#industry').val(ind_val);
					$('#functional_area').val(fn_area_val);
					if(data.exp_year !='' && data.exp_year !=null && data.exp_year !=undefined)
					{
						$('#exp_year').val(data.exp_year);
					}
					if(data.exp_month !='' && data.exp_month !=null && data.exp_month !=undefined)
					{
						$('#exp_month').val(data.exp_month);
					}
					$('.chosen-select').trigger('chosen:updated');
					function setroleval()
						{
							$("#job_role").val(job_role);
							$('#job_role').trigger('chosen:updated');
						}
					dropdownChange('functional_area','job_role','role_master',function() {setroleval();});
					
				}
				for_active_tab();
				$('#step4').trigger('change');
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				
				/* new code for annual salary selected when leaving date blank*/
				var l_row = $("input[name=no_of_work_exp_row]").val();
				for(l_row = 0; l_row < 10; l_row++) 
				{
					if($('#ldate'+l_row).length > 0 && $('#ldate'+l_row).val()=='')
					{
						var currency_type = 'currency_type'+l_row;
						var annual_salary_lacs = 'annual_salary_lacs'+l_row;
						var annual_salary_thousand  = 'annual_salary_thousand'+l_row;
						
						var currency_type_get = $('select[name="'+currency_type+'"] option:selected').val();
						var annual_salary_lacs_get= $('select[name="'+annual_salary_lacs+'"] option:selected').val();
						var annual_salary_thousand_get = $('select[name="'+annual_salary_thousand+'"] option:selected').val();
						document.querySelector('select[name=currency_type] option[value="'+currency_type_get+'"]').selected = true;
						document.querySelector('select[name=a_salary_lacs] option[value="'+annual_salary_lacs_get+'"]').selected = true;
						document.querySelector('select[name=a_salary_thousand] option[value="'+annual_salary_thousand_get+'"]').selected = true;
			/* new code for annual salary selected when leaving date blank*/
					}
				}
				time_out_message();
				//document.getElementById('work_exp_form').reset();
				scroll_to_div('for_scrool',-100);
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	});	
	 
      return false; // Will stop the submission of the form
    }
 });
$.validate({
  form : '#js_details',
    modules : 'security',
    onError : function($form) {
	  scroll_to_div('for_scrool',-100);
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#js_details").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/js_extra_details' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			 hide_comm_mask();
			if($.trim(data.status) == 'success')
			{
				
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				for_active_tab();
				//time_out_message();
				//document.getElementById('js_details').reset();
				scroll_to_div('for_scrool',-100);
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	});	
	 
      return false; // Will stop the submission of the form
    }
 });
$.validate({
    form : '#js_img_upload',
    modules : 'file',
    onError : function($form) {
	 /* scroll_to_div('for_scrool');
	  $("#error_msg").html('');
	  $("#success_msg").html('');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").hide();
	  $("#error_msg").html("<?php //echo $custom_lable_array['please_select_file']; ?>");*/
	  //time_out_message();
	 $('.view_preview_img').hide();
    },
    onSuccess : function($form) {
	 /*event.preventDefault();
	 return false;*/
	  var profile_pic= $("#profile_pic").val();
	  var resume_file= '';
	  if(profile_pic=='')
	  {  
		  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");
		  $("#error_msg").slideDown();
		  scroll_to_div('for_scrool',-100);
		  return false;
	  }
	  show_comm_mask();
	  
	  /*$("#error_msg").html('');
	  $("#success_msg").html('');*/
	  $("#error_msg").hide();
	  var form_data = new FormData();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  form_data.append('profile_pic', $('input[name=profile_pic]')[0].files[0]);
	  form_data.append('user_agent', 'NI-WEB');
	  form_data.append('action', 'upload_img');
	  form_data.append('profile_pic_upload', profile_pic);
	  form_data.append('resume_file_upload', resume_file);
	  form_data.append('csrf_job_portal', hash_tocken_id);
	  $.ajax({
		url : "<?php echo $base_url.'sign-up/upload_profile_pic_resume' ?>",
		type: 'post',
		data: form_data,        // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		dataType:"json",
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,
		success: function(data)
		{
			hide_comm_mask();
			$("#hash_tocken_id").val(data.token);
			//alert(data.status);
			if($.trim(data.status) == 'success')
			{
				//for_active_tab();
				/*$('#complete_reg').slideDown();
				$('#complete').slideUp();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();*/
				$("#profile_pic").val('');
				/*$('#js_img_upload').hide();
				$("#success_msg").html(data.errmessage);*/
				$("#success_msg_img_upload").slideDown();
				$("#success_msg_img_upload").html(data.errmessage);
				$("#img_upload_form").hide('');
				$("#img_upload_message").show('');
				time_out_message();
				document.getElementById('js_img_upload').reset();
				//scroll_to_div('for_scrool',-100);
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#for_scrool").offset().top
				}, 200);
				$('.skip_photo_upload,.previous_photo_upload').hide();
				//setTimeout(function(){ window.location='<?php //echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
			}
			else
			{
				if(data.resume_success_msg!='' || data.errmessage_resume!=''  || data.errmessage_profile_pic!='' || data.profile_pic_success_msg!='')
				{
					if(typeof(data.resume_success_msg) != "undefined" && data.resume_success_msg!='')
					{
						$("#success_msg").append('<strong>Resume :</strong>');
						$("#success_msg").slideDown();
						$('.skip_photo_upload,.previous_photo_upload').hide();
						//for_active_tab();
						//$('#complete_reg').slideDown();
						//$('#complete').slideUp();
						$("#success_msg").append(data.resume_success_msg);
						$("#resume_file").val('');
					}
					else if(typeof(data.errmessage_resume) != "undefined" && data.errmessage_resume!='')
					{
						$("#error_msg").append('<strong>Resume :</strong>');
						$("#error_msg").append(data.errmessage_resume);
						$("#error_msg").slideDown();
					}
					if(typeof(data.profile_pic_success_msg) != "undefined" && data.profile_pic_success_msg!='')
					{
						$("#success_msg").append('<strong>Profile picture  : </strong>');
						//for_active_tab();
						$('#js_img_upload').hide();
						$('.skip_photo_upload,.previous_photo_upload').hide();
						//$('#complete').slideUp();
						//$('#complete_reg').slideDown();
						$("#success_msg").slideDown();
						$("#success_msg").append(data.profile_pic_success_msg);
						$("#profile_pic").val('');
					}
					else if(typeof(data.errmessage_profile_pic) != "undefined" && data.errmessage_profile_pic!='')
					{
						$("#error_msg").append('<strong>Profile picture :</strong>');
						$("#error_msg").append(data.errmessage_profile_pic);
						$("#error_msg").slideDown();
					}
					//setTimeout(function(){ window.location='<?php //echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
				}
				else
				{
					$("#error_msg").html(data.errmessage);
					$("#success_msg").slideUp();
				    $("#error_msg").slideDown();
				}
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	  });	
	  
      return false; // Will stop the submission of the form
    }
 });
$.validate({
    form : '#js_resume_upload',
    modules : 'file',
    onError : function($form) {
	 /* scroll_to_div('for_scrool');
	  $("#error_msg").html('');
	  $("#success_msg").html('');
	  $("#success_msg").hide();
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#error_msg").html("<?php //echo $custom_lable_array['please_select_file']; ?>");*/
	  //time_out_message();
	  
    },
    onSuccess : function($form) {
	  
	 /*event.preventDefault();
	 return false;*/
	  var profile_pic= '';
	  var resume_file= $("#resume_file").val();
	  if(resume_file=='')
	  {  
		  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");
		   $("#error_msg").slideDown();
		  scroll_to_div('for_scrool',-100);
		  return false;
	  }
	  show_comm_mask();
	 
	  //$("#error_msg").html('');
	  //$("#success_msg").html('');
	  $("#error_msg").hide();
	  var form_data = new FormData();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  form_data.append('resume_file', $('input[name=resume_file]')[0].files[0]);
	  form_data.append('user_agent', 'NI-WEB');
	  form_data.append('action', 'upload_img');
	  form_data.append('profile_pic_upload', profile_pic);
	  form_data.append('resume_file_upload', resume_file);
	  form_data.append('csrf_job_portal', hash_tocken_id);
	  $.ajax({
		url : "<?php echo $base_url.'sign-up/upload_profile_pic_resume' ?>",
		type: 'post',
		data: form_data,        // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		dataType:"json",
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			 hide_comm_mask();
			//alert(data.status);
			if($.trim(data.status) == 'success')
			{
				//for_active_tab();
				$('.skip_photo_upload,.previous_photo_upload').hide();
				/*$('#complete_reg').slideDown();
				$('#complete').slideUp();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);*/
				$("#resume_file").val('');
				$("#success_msg_resume").slideDown();
				$("#success_msg_resume").html(data.errmessage);
				$("#resume_form").hide('');
				$("#resume_message").show('');
				//$('#js_resume_upload').hide();
				time_out_message();
				document.getElementById('js_img_upload').reset();
				//scroll_to_div('for_scrool',-100);
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#for_scrool").offset().top
				}, 200);
				//setTimeout(function(){ window.location='<?php //echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
			}
			else
			{
				if(data.resume_success_msg!='' || data.errmessage_resume!=''  || data.errmessage_profile_pic!='' || data.profile_pic_success_msg!='')
				{
					if(typeof(data.resume_success_msg) != "undefined" && data.resume_success_msg!='')
					{
						$("#success_msg").append('<strong>Resume :</strong>');
						$("#success_msg").slideDown();
						//for_active_tab();
						$('#js_resume_upload').hide();
						$('.skip_photo_upload,.previous_photo_upload').hide();
						//$('#complete_reg').slideDown();
						//$('#complete').slideUp();
						$("#success_msg").append(data.resume_success_msg);
						$("#resume_file").val('');
					}
					else if(typeof(data.errmessage_resume) != "undefined" && data.errmessage_resume!='')
					{
						$("#error_msg").append('<strong>Resume :</strong>');
						$("#error_msg").append(data.errmessage_resume);
						$("#error_msg").slideDown();
					}
					if(typeof(data.profile_pic_success_msg) != "undefined" && data.profile_pic_success_msg!='')
					{
						$("#success_msg").append('<strong>Profile picture  : </strong>');
						$('.skip_photo_upload,.previous_photo_upload').hide();
						//for_active_tab();
						
						//$('#complete').slideUp();
						//$('#complete_reg').slideDown();
						$("#success_msg").slideDown();
						$("#success_msg").append(data.profile_pic_success_msg);
						$("#profile_pic").val('');
					}
					else if(typeof(data.errmessage_profile_pic) != "undefined" && data.errmessage_profile_pic!='')
					{
						$("#error_msg").append('<strong>Profile picture :</strong>');
						$("#error_msg").append(data.errmessage_profile_pic);
						$("#error_msg").slideDown();
					}
					//setTimeout(function(){ window.location='<?php //echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
				}
				else
				{
					$("#error_msg").html(data.errmessage);
					$("#success_msg").slideUp();
				    $("#error_msg").slideDown();
				}
				scroll_to_div('for_scrool',-100);
				time_out_message();
			}
			
		}
	  });	
	 
      return false; // Will stop the submission of the form
    }
 }); 
 
 
 
$(document).ready(function(){
	
	get_suggestion_list('preferred_city','id','city_list');
	$('#complete_reg').slideUp();
	$('.datepicker').datepicker({
	  dateFormat: 'yy-mm-dd',
	  endDate: '+0d',
      autoclose: true
	});  
//var tokenfieldwidth = $('.tokenfield').outerWidth();
//$('.tokenfieldwidth-tokenfield').width(tokenfieldwidth);
//check_user_exp_or_fresher();
/*if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
   var datepicker = $.fn.datepicker.noConflict();
   $.fn.bootstrapDP = datepicker;
   $('.datepicker').bootstrapDP({
   format: 'yyyy-mm-dd',
   endDate: '+0d',
   autoclose: true
});
}*/
 	
   /*  var config = {
		  '.chosen-select'           : {},
		  '.chosen-select-deselect'  : {allow_single_deselect:true},
		  '.chosen-select-no-single' : {disable_search_threshold:10},
		  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		  '.chosen-select-width'     : {width:"100%"}
		}
		for (var selector in config) {
		  $(selector).chosen(config[selector]);
		}    */
	
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });


   $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
	
	
	
	$('.accordion-header').toggleClass('inactive-header');

	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});

	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');

	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}

		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	
	$(".prev-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);
		$('#step4').trigger('change');
		scroll_to_div('for_scrool',-100);
    });
});
</script>
<script>

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
   
$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    var gender =  $(this).data('value');
	$('#gender').val(gender);
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    var m_status =  $(this).data('value');        
	$('#m_status').val(m_status);
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
}) 

</script>
