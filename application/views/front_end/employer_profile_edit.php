<?php  $upload_path_profile ='./assets/emp_photos';$custom_lable_arr = $custom_lable->language;
	$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('company_logos',1);
$mobile = ($this->common_front_model->checkfieldnotnull($user_data['mobile'])) ? explode("-",$user_data['mobile']) : "";
//$join_in = ($this->common_front_model->checkfieldnotnull($user_data['join_in'])) ? explode(" ,",$user_data['join_in']) : "";
 ?>
<div class="tab-pane fade " id="change" style="padding:0px;">
			<!--<div class="progress">
					<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
					  Your Profile Complete - 40%
					</div>
				</div>-->
				<div class="container fom-main" >
					<div class="row">
						<div class="col-sm-12">
							<h2 class="register"><u><?php echo $custom_lable_arr['sign_up_emp_edit_title']; ?></u></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-10 col-sm-11 col-xs-11">
								<div  class="panel-group"  id="accordion-container" role="tablist" aria-multiselectable="true">
                                	<div class="clearfix" id="profile_snapshoteditdivscroll"></div>
									<h2 class="accordion-header" id="profile_snapshoteditdiv"><?php echo $custom_lable_arr['myprofileedit_emp_personaltitle']; ?></h2>
								<form class=" text-left" method="post"  id="personal_det_form" name="personal_det_form" action="<?php echo $base_url.'employer_profile/updatepersonaldet'; ?>" ><!--form-horizontal-->
                                	<div class="alert alert-danger" id="message2" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg2" style="display:none" ></div>
                                    
									<div class="accordion-content" >
										<div class="row" >
											<div class="col-md-12" >
												<!--<div class="row">-->
                                                	<div class="col-md-3" >
                                                    	<div class="form-group">
                                                      		<h5><?php echo $custom_lable_arr['sign_up_emp_1stformtitle']; ?> :<span class="red-only"> *</span></h5>
                                                      		<div class="input-group">
                                                      	<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                            <select  class="form-control" name="title" id="title" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformtitlevalmes']; ?>" >
															<?php
                                                            if($pers_title != '' && is_array($pers_title) && count($pers_title) > 0)
                                                            {
                                                                foreach ($pers_title as $pers_title_single)
                                                                { ?>
                                                                    <option value="<?php echo $pers_title_single['id'];?>" <?php if($user_data!='' && $pers_title_single['id']==$user_data['title'] ){ echo "selected" ; } ?>><?php echo $pers_title_single['personal_titles'];?> </option>	
                                                                <?php }
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                       </div> 
                                                    </div>
                                                    <div class="col-md-9" >
                                                            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformfullname']; ?> :<span class="red-only"> *</span></h5>
                                                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformfullnameplacehold']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ?  $user_data['fullname'] : "" ; ?>" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformfullnamevalmes']; ?>" >
                                                    </div>
                                                    <?php /* ?>
                                                    <div class="col-md-6" >
                                                            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformlastname']; ?><span class="red-only"> *</span></h5>
                                                            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformlastnameplacehold']; ?>" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformlastnamevalmes']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['lastname'])) ? $user_data['lastname'] : "" ; ?>" style="padding:9px 0 9px 10px;border-radius:0;"><!--data-validation="required"-->
                                                    </div>
                                                    <?php */ ?>
												<!--</div>	-->
                                                <div class="margin-top-20"></div>      
                                                <div class="col-md-12" >
                                                	<div class="form-group">
                                                <h5><?php echo $custom_lable_arr['sign_up_emp_1stformabout']; ?> :</h5>
												<div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <textarea class="form-control" id="job_profile" name="job_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $custom_lable_arr['sign_up_emp_1stformaboutphold']; ?>" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformaboutphold']; ?>"><?php echo ($user_data!='') ? $user_data['job_profile'] : ""?></textarea>
												</div>
                                                </div>
                                                </div>
                                                <div class="margin-top-20"></div>
                                                <div class="col-md-12" >
                                                <div class="form-group">
                                                <h5><?php echo $custom_lable_arr['sign_up_emp_1stformemail']; ?> :</h5>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
                                                    <input type="email" class="form-control" name="emailid" id="emailid" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformemailplacehold']; ?>" style="padding:9px 0 9px 10px;text-transform: lowercase;" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformemailvalmes']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['email'])) ? $user_data['email'] : "" ; ?>" disabled/> <!--data-validation="required,email"-->
                                                </div>
                                                </div>
                                                </div>
												
												
                                                    <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB">
												
                                                <div class="col-md-12">
                                                <div class="margin-top-20"></div>
                                                <div class="text-center">
													<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
													<button type="button" class="btn btn-default" onClick="viewsection('personal_det');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></button>
												</div>
                                                </div>
											</div>
										</div>
									</div>
									</form>
                                    <div class="clearfix" id="company_detailseditdivscroll"></div>
									<h2 class="accordion-header" id="company_detailseditdiv"> <?php echo $custom_lable_arr['sign_up_emp_2ndtabedittitle']; ?></h2>
									<div class="accordion-content">
                                    <form class=" text-left" method="post"  id="company_det_form" name="company_det_form" action="<?php echo $base_url.'employer_profile/updatecompanydet'; ?>"><!--form-horizontal-->
                                    <div class="alert alert-danger" id="message_comp" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_comp" style="display:none" ></div>
                                    <div class="margin-top-20 clearfix"></div>
        								<!--<div class="row">-->
											<div class="col-md-12">
                                            
												<!--<div class="form-group">-->
													<h5><?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogo']; ?> :</h5>
													<img src="<?php if($user_data!='' && $user_data['company_logo'] != '' && !is_null($user_data['company_logo']) && file_exists($upload_path_profile_cmp.'/'.$user_data['company_logo']))
			{ echo $upload_path_profile_cmp.'/'.$user_data['company_logo']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/company-logo.png<?php } ?>" id="blah1" class="blah1" alt="" />
                                                   <input type="file"  name="company_logo" id="company_logo" data-validation=" mime size" data-validation-max-size="2M" data-validation-error-msg-size="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval']; ?>" data-validation-allowing="jpg, jpeg, png, gif" data-validation-error-msg-mime="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval1']; ?>"/>
													<small id="file" class="form-text text-muted red-only"><i><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></i></small>
												<!--</div>-->
												<div class="progress" id="imageprogressbardiv" style="display:none;">
													<div class="progress-bar progress-bar-success" id="imageprogressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0%<?php echo $custom_lable_arr['emp_edit_data_image_succ_comp']; ?></div>
												</div>
												<div class="margin-top-10"></div>
											</div>
										<!--</div>-->
										<div class="col-md-12">
                                        	<div class="margin-top-20"></div>
											<h5><?php echo $custom_lable_arr['sign_up_emp_companydet']; ?> :<span class="red-only"> *</span></h5>
                                            <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo ($user_data['company_name']!='') ? $user_data['company_name'] : ""?>" placeholder="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnyplace']; ?>"  style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnyval']; ?>">
                                        </div>
										
                                        <div class="col-md-12">
                                        <div class="margin-top-20"></div>
										<h5><?php echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?> :<span class="red-only"> *</span></h5>
                                        <select name="company_type" id="company_type" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnytypeval']; ?>">
            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['company_type'])) { echo  $this->common_front_model->get_list_multiple('company_type_master','str','','str',$user_data['company_type']);}else{ echo $this->common_front_model->get_list_multiple('company_type_master','str','','str','') ; } ?>
        </select>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['myprofile_emp_cmpsjizelbl']; ?> :<span class="red-only"> *</span></h5>
                          <select name="company_size" id="company_size" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnysizeval']; ?>">
                        <!--<option value=""><?php //echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?></option>-->
							<?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['company_size'])) { echo  $this->common_front_model->get_list_multiple('company_size_master','str','','str',$user_data['company_size']);}else{ echo $this->common_front_model->get_list_multiple('company_size_master','str','','str','') ; } ?>
						</select>
                                        </div>
                                        <!--  -->
                                        
        								<div class="col-md-12">
		<div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_cmpweblbl']; ?> :</h6>
        <input type="text" name="company_website" class="form-control" id="company_website" style="border-radius:0px;text-transform:lowercase;"  placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['website'].' '. $custom_lable_arr['example_website'];  ?>" value="<?php echo ($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['company_website'])) ? $user_data['company_website'] : ""; ?>"  data-validation="url" data-validation-optional="true">
        </div>
                                        <div class="col-md-12">
                                            <div class="margin-top-20"></div>
                                        <h6><?php echo $custom_lable_arr['myprofile_emp_cmpprolbl']; ?> :</h6>
                                            <textarea class="form-control" id="company_profile" name="company_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $custom_lable_arr['myprofile_emp_cmpprofiltexz']; ?>"><?php echo ($user_data!='') ? $user_data['company_profile'] : ""?></textarea>
                                        </div>
        								<div class="col-md-12">
		<div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_industrylbl']; ?> :<span class="red-only"> *</span></h6>
            <select name="industry" id="industry" style="padding:9px 0 9px 10px;" class=" city" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofile_emp_industryvalmes']; ?>">
		 		<?php if($user_data!='') { echo $this->common_front_model->get_list('industries_master','str','','str',$user_data['industry']); }else{ echo $this->common_front_model->get_list('industries_master','str','','str','') ; } ?>
         	</select>
        </div>
        
        <div class="col-md-12">
        <div class="margin-top-20"></div>
           <!-- <div class="row">-->
            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformmob']; ?> :<span class="red-only"> *</span></h5>
            <div class="col-md-4 col-xs-12" style="margin-left:-15px;">
            	<div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                    <select  class="form-control" name="mobile_c_code" id="mobile_c_code" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformmobcodevalmes']; ?>">
                        <?php
						if(isset($country_code) && is_array($country_code) && count($country_code) > 0)
						{
                        foreach ($country_code as $code)
                        { ?>
                            <option value="<?php echo $code['country_code'];?>" <?php if($mobile!='' && isset($mobile[0]) && $mobile[0]==$code['country_code'] ){ echo "selected" ; } ?>><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
                        <?php }
						}
                        ?>
                        </select>
                </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12" style="margin-left:-15px;">
                <input id="mobile_num" type="text" class="form-control" name="mobile_num" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformmobnumplacehold']; ?>" style="padding:9px 0 9px 10px;" value="<?php if($mobile!='' && isset($mobile[1])){ echo $mobile[1] ; } ?>" data-validation="number,length"  data-validation-length="min8,max13" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformmobnumvalmes']; ?>">
                <input type="hidden" name="mobile_num_old" value="<?php if(isset($user_data['mobile']) && $user_data['mobile']!=''){ echo $user_data['mobile']; } ?>" />
            </div>
        <!--</div>-->
		</div>
        <div class="margin-top-20"></div>
                                                <!--<div class="row">-->
                                                    	<div class="col-md-6" >
															<h5><?php echo $custom_lable_arr['sign_up_emp_1stformcnt']; ?> :<span class="red-only"> *</span></h5>
                                                            <select name="country" id="country" class="country" style="padding:10px;border-radius:0;" onChange="dropdownChange('country','city','city_list');" data-validation="required"  data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformcntvalmes']; ?>" >
                                                            <?php if($this->common_front_model->checkfieldnotnull($user_data['country'])) { echo  $this->common_front_model->get_list('country_list','str','','str',$user_data['country']);}else{ echo $this->common_front_model->get_list('country_list','str','','str','') ; } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6" >
														<h5><?php echo $custom_lable_arr['sign_up_emp_1stformcity']; ?> :<span class="red-only"> *</span></h5>
                                                        <select name="city" id="city" class="city" style="padding:10px;border-radius:0;"  data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformvalmescity']; ?>" data-validation="required">
                                                        <option value=""><?php echo $custom_lable_arr['sign_up_emp_1stformcitydfltopt']; ?></option>
                                                        </select>
                                                 	</div>
                                                 <!--</div>-->       
                                                
        
        	<div class="form-group col-xs-12">
            <div class="margin-top-20 clearfix"></div>
        		<h5><?php echo $custom_lable_arr['sign_up_emp_1stformaddress']; ?> :<span class="red-only"> *</span></h5>
            	<div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                    <textarea id="address" class="form-control" name="address" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformaddress']; ?>" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformaddressvalmessup']; ?>" data-validation="required"><?php if($user_data!=''){ echo $user_data['address'] ; } ?></textarea>
                    </div>
        	</div>
        <div class="form-group col-xs-12">
    	<div class="margin-top-20 clearfix"></div>
        <h5><?php echo $custom_lable_arr['sign_up_emp_1stformpincode']; ?> :<span class="red-only"> *</span></h5>
        	<input type="text" class="form-control" name="pincode" id="pincode" placeholder="Pincode" style="padding:9px 0 9px 10px;"  data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformpincodevalmes']; ?>" value="<?php if($user_data!='' && $user_data['pincode']!='0'){ echo $user_data['pincode'] ; }else { echo "";} ?>" data-validation="required,number">
        </div>
                                        <!--  -->
										
                                        <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
										<div class="margin-top-20"></div>
											<div class="text-center">
												<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
												<button type="button" class="btn btn-default" onClick="viewsection('company_det');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></button>
											</div>		
                                    </form>        
									</div>
                                    
                                    <div class="clearfix" id="work_expeditdivscroll"></div>
									<h2 class="accordion-header" id="work_expeditdiv"> <?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']; ?></h2>
									<div class="accordion-content">
                                    <form class=" text-left" method="post"  id="work_exp_form" name="work_exp_form" action="<?php echo $base_url.'employer_profile/updatecompanydet'; ?>"><!--form-horizontal-->
                                    <div class="alert alert-danger" id="message_work" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_work" style="display:none" ></div>
                                    		
                                            <div class="form-group col-xs-12">
                                    		<div class="margin-top-20"></div>
                                            <h6><?php echo $custom_lable_arr['functional_area_empl_lbl']; ?> :<span class="red-only"> *</span></h6>
											<!--<input type="text" class="form-control" name="currentdesignation" id="currentdesignation" placeholder="Hr / Manager"  style="padding:9px 0 9px 10px;border-radius:0;" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Enter Your Current Designation" data-validation="required" data-validation-error-msg="Please provide your current designation" value="<?php //echo ($user_data!='') ? $user_data['designation'] : ""?>">-->
                                            <select name="functional_area[]" id="functional_area" class=" chosen-select" onChange="dropdownChange('functional_area','currentdesignation','role_master','','multi');" multiple data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformaddressvalmessup']; ?>" data-validation="required">
                                            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['functional_area'])) { echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str',$user_data['functional_area'],'1'); }else{ echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str','','1') ; } ?>
                                            </select>
										</div>
                                        <div class="form-group col-xs-12">
											<div class="margin-top-20"></div>
											<h6><?php echo $custom_lable_arr['current_role_empl_lbl']; ?> :<span class="red-only"> *</span></h6>
											<!--<input type="text" class="form-control" name="currentdesignation" id="currentdesignation" placeholder="Hr / Manager"  style="padding:9px 0 9px 10px;border-radius:0;" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Enter Your Current Designation" data-validation="required" data-validation-error-msg="Please provide your current designation" value="<?php //echo ($user_data!='') ? $user_data['designation'] : ""?>">-->
                                            <select name="currentdesignation[]" id="currentdesignation" class=" chosen-select" multiple max-selected-options="3" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['emp_edit_des_mes1']; ?>">
                                            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['designation']) && $this->common_front_model->checkfieldnotnull($user_data['functional_area'])) { 
											$fn_area_arr = explode(",",$user_data['functional_area']);
											echo $this->common_front_model->get_list_multiple('role_master','str',$fn_area_arr,'str',$user_data['designation'],1,'multi'); }else{ /*echo "<option value=''>".$custom_lable_arr['myprofile_emp_desemptopt']."</option>" ;*/ } ?>
                                            </select>
										</div>
                                        <?php /* ?>
                                        <div class="margin-top-20"></div>
										<h6>Joined In<span class="red-only"> *</span></h6>
										<div class="row">
											<div class="col-sm-6">
												<select name="join_month" id="join_month" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="Please provide your join in month">
													<option value="" >Select Month</option>
													<?php 
													for ($m=1; $m<=12; $m++) {
													 $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
													 ?>
													 <option value = '<?php echo $month; ?>' <?php if($join_in!='' && $join_in[0]==$month){ echo "selected"; } ?>><?php echo $month; ?></option>
													 <?php } ?>
												</select>
											</div>
											<div class="col-sm-6">
                                            	<select name="join_year" id="join_year" class="city" style="padding:9px 0 9px 10px;"  data-validation="required" data-validation-error-msg="Please provide your join in year">
													<option value=""><?php echo $custom_lable_arr['year'] ?></option>
													<?php
													$current_yr = date('Y');
													$start_yr = $current_yr - 25;
                                                    for($i = $current_yr ; $i >= $start_yr ; $i--)
												 	{  ?>
                                                        <option value="<?php echo  $i; ?>" <?php if($join_in!='' && $join_in[1]==$i){ echo "selected"; } ?>><?php echo  $i; ?> </option>	
                                                <?php	}
                                                    ?>	
												</select>
											</div>
										</div>
                                        
										<div class="margin-top-20"></div>
										<h6>Job Profile</h6>
										<div class="">
											<textarea class="form-control" id="job_profile" name="job_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Enter Your Job description"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['job_profile'])) ? $user_data['job_profile'] : "" ; ?></textarea>
										</div>
                                        <?php */ ?>
                                        <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                        <div class="margin-top-20"></div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
                                            <button type="button" class="btn btn-default" onClick="viewsection('work_exp_det');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></button>
                                        </div>		
                                    </form>    
									</div>
                                    
                                    <div class="clearfix" id="skills_i_hireeditdivscroll"></div>
									<h2 class="accordion-header" id="skills_i_hireeditdiv"> <?php echo $custom_lable_arr['sign_up_emp_Edit']; ?> <?php echo $custom_lable_arr['skill_hire_for_tit']; ?></h2>
									<div class="accordion-content">
                                    <form class=" text-left" method="post"  id="hire_form" name="hire_form" action="<?php echo $base_url.'employer_profile/updatehiredet'; ?>"><!--form-horizontal-->
                                    <div class="alert alert-danger" id="message_skill" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_skill" style="display:none" ></div>
                                    	<div class="form-group col-xs-12">
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['indu_hire_for_lbl']; ?> :<span class="red-only"> *</span></h6>
                                        <select name="industry_hire[]" id="industry_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>">
                                            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['industry_hire'])) { echo $this->common_front_model->get_list_multiple('industries_master','str','','str',$user_data['industry_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('industries_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                        <div class="form-group col-xs-12">
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['fn_hire_for_lbl']; ?> :<span class="red-only"> *</span></h6>
                                       <select name="function_area_hire[]" id="function_area_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" >
                                            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['function_area_hire'])) { echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str',$user_data['function_area_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                        <div class="form-group col-xs-12">
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['skill_hire_for_lbl']; ?> :<span class="red-only"> *</span></h6>
                                        <select name="skill_hire[]" id="skill_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>">
                                            <?php if($user_data!='' && $this->common_front_model->checkfieldnotnull($user_data['skill_hire'])) { echo $this->common_front_model->get_list_multiple('key_skill_master','str','','str',$user_data['skill_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('key_skill_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                        <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                        <div class="margin-top-20"></div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
                                            <button type="button" class="btn btn-default" onClick="viewsection('sectors_det');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></button>
                                        </div>		
                                    </form>    
									</div>
                                    
                                    <!-- social links section starts-->
                                    <div class="clearfix" id="social_linkseditdivscroll"></div>
									<h3 class="accordion-header" id="social_linkseditdiv"> <?php echo $custom_lable_arr['myprofile_social_med_tit']; ?></h3>
									<div class="accordion-content">
                                    <div class="alert alert-danger" id="message_social" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_social" style="display:none" ></div>
                                    	<div id="socialwholeid">
                                        	<?php $this->load->view('front_end/soaial_link_edit_section_emp'); ?>
                                    	</div>
									</div>
                                    <!-- social links section ends-->
                                    
									<div class="clearfix" id="profile_photoeditdivscroll"></div>
                                    <form method="post" enctype="multipart/form-data" id="profileresumeform" name="profileresumeform" action="<?php echo $base_url.'my_profile/uploadprofile'; ?>"  onSubmit="return uploadprofilepic();" >
                                        <h3 class="accordion-header" id="profile_photoeditdiv"><?php echo $custom_lable_arr['sign_up_emp_1stformuploadpictit']; ?> </h3>
                                        <div class="accordion-content"  >
                                            <div class="ten columns">
                                                <div id="image_val" style="position:fixed;  left:40%; top:18%; z-index:9999; opacity:0"></div>
                                                <img id="blah" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic']))
			{ echo $upload_path_profile.'/'.$user_data['profile_pic']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" class="blah" alt="your image" />
                                                <div class="margin-top-10"></div>
                                                <input type="file" onchange="displayprofile();" name="profile_img" id="profile_img"  data-validation="mime size" data-validation-max-size="2M" data-validation-error-msg-size="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval']; ?>" data-validation-allowing="jpg, jpeg, png, gif" data-validation-error-msg-mime="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval1']; ?>"/>
                                                <small id="fileHelp" class="text-left text-muted red-only"><i><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></i></small>
                                                <div class="margin-top-20"></div>
                                                <!--<a class="btn btn-primary" onClick="uploadprofilepic();" style="text-transform:capitalize;color:#fff;"><i class="fa fa-upload" aria-hidden="true"></i> Upload a new  Photo!</a>-->
                                                <div class="margin-bottom-10"></div>
                                            </div>
                                            <?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic']))
			{  ?>
                                            <div class="five columns" style="text-align:right;" >
                                                <button type="button" class="btn btn-danger" id="deleteprofile_btn" onClick="deleteprofilepic();">Delete Profile</button>
                                            </div>
                                            <?php } ?>
                                            <!--<hr>-->
                                            <!--<h4><u>Upload Resume</u>:</h4>-->
                                            <div class="entry input-group col-xs-3">
                                               <?php /*?> <input class="btn" name="fields[]" type="file"><?php */?>
                                            </div>
                                            <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary" name="submitphotoresume" value="submitphotoresume"> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
                                                <a class="btn btn-default" onClick="viewsection('personal_det');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></a>
                                            </div>
                                        </div>
                                    </form>
								</div>
						</div>
					</div>
				</div>
			</div>

<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>

<script>

$(document).ready(function(e) {
	
	/*$('#accordion-container').on('shown.bs.collapse', function() {
 alert('ok'); console.log('jay');
}).on('show.bs.collapse', function() {
 alert('ok'); console.log('jay');
});
	$('#profile_snapshoteditdiv').on('show.bs.collapse', function () {
		alert('test');
		//call a service here 
	});
	$("#accordion").on('hidden.bs.collapse', function(){
        alert("Collapsible element has been completely closed.");
    });*/
	/*$("#currentdesignation").chosen({
		max_selected_options:3, //Max select limit 
	});*/
});
/*$('#currentdesignation').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up_employer/get_suggestion/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: false
});
$('#currentdesignation').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$('#industry_hire').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php //echo $base_url; ?>sign_up_employer/get_suggestion/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: false,
	limit: 3
});
$('#industry_hire').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$('#function_area_hire').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up_employer/get_suggestion/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: false,
	limit: 3
});
$('#function_area_hire').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$('#skill_hire').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php //echo $base_url; ?>sign_up_employer/get_suggestion/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: false,
	limit: 3
});
$('#skill_hire').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});*/
</script>