<?php  $upload_path_profile ='./assets/emp_photos'; ?>
<?php 
$custom_lable_arr = $custom_lable->language;
if(isset($forstep2) && $forstep2!="" && is_array($forstep2) && count($forstep2) > 0)
{
	$forstep2svar = $forstep2;
}
else
{
	$forstep2svar = "";
}
//print_r($forstep2svar);
?>
<form method="post" name="emplregform4" enctype="multipart/form-data" id="emplregform4" action="<?php echo $base_url.'sign_up_employer/signupemp4'; ?>" >
<div class="" id="image_val"  style="position:fixed;  left:40%; top:18%; z-index:9999; opacity:0"></div>
<div class="alert alert-danger" id="message4" style="display:none" ></div>
<div class="alert alert-success" id="success_msg4" style="display:none" ></div>
<div class="step44">
								<div class="panel panel-primary">
                                <div class="panel-heading th_bgcolor"><strong style="color:white;"><i class="fa fa-2x fa-cloud-upload" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformuploadpictit']; ?></strong></div>
									
									<div class=" collapse in"><!--accordion-content-->
										<div class="row">
											<div class="col-md-12">
												<div class="form-group" style="margin-left:10px;">
													<label for="exampleInputFile"><?php echo $custom_lable_arr['sign_up_emp_1stformuploadpictit']; ?>
                                                    </label>
													<img src="<?php if($forstep2svar!='' && $forstep2svar['profile_pic'] != '' && !is_null($forstep2svar['profile_pic']) && file_exists($upload_path_profile.'/'.$forstep2svar['profile_pic']))
			{ echo $upload_path_profile.'/'.$forstep2svar['profile_pic']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" id="blah" class="blah" alt="your image" />
                                                   <input type="file"  name="profile_img" id="profile_img" data-validation="required mime size" data-validation-max-size="2M" data-validation-error-msg-size="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval']; ?>" data-validation-allowing="jpg, jpeg, png, gif" data-validation-error-msg-mime="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval1']; ?>"/>
                                                   <!-- onchange="displayprofile();" -->
													<small id="file" class="form-text text-muted red-only"><i><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></i></small>
												</div>
												<div class="progress" id="imageprogressbardiv" style="display:none;">
													<div class="progress-bar progress-bar-success" id="imageprogressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0% <?php echo $custom_lable_arr['emp_edit_data_image_succ_comp']; ?> </div>
												</div>
												<!--<div class="margin-top-10"></div>
												<div class="panel panel-info">
													<div class="panel-heading th_bgcolor">
														<h3 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_arr['emp_edit_data_image_impnot']; ?></h3>
													</div>
													<div class="panel-body">
														<ul>
															<li><?php echo $custom_lable_arr['emp_edit_data_image_impnot1']; ?></li>
															<li><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></li>
															<li><?php echo $custom_lable_arr['emp_edit_data_image_impnot3']; ?></li>
														</ul>
													</div>
												</div>-->
											</div>
										</div>
									</div>
								</div>
                                <div class="panel panel-info">
													<div class="panel-heading th_bgcolor">
														<h3 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_arr['emp_edit_data_image_impnot']; ?></h3>
													</div>
													<div class="panel-body">
														<ul>
															<li><?php echo $custom_lable_arr['emp_edit_data_image_impnot1']; ?></li>
															<li><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></li>
															<li><?php echo $custom_lable_arr['emp_edit_data_image_impnot3']; ?></li>
														</ul>
													</div>
												</div>
							</div>
<div class="margin-top-20"></div>
<ul class="list-inline pull-right">
    <li><button type="button" class="btn btn-sm btn-default " onClick="prev_step_mm('3')"><?php echo $custom_lable_arr['previous_emp']; ?></button></li>
    <li><button type="submit" class="btn btn-sm btn-primary btn-info-full next-step th_bgcolor"><?php echo $custom_lable_arr['sign_up_emp_1stformsave']; ?></button></li>
</ul>
<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
<input type="hidden" name="hash_tocken_id_return4" id="hash_tocken_id_return4" value="<?php echo $this->security->get_csrf_hash(); ?>"/>
</form>
<script>
$(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab('#step4');
		 prevTab($active);

    });
</script>