<?php
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('emp_photos',1);
$custom_lable_arr = $custom_lable->language;
?>
<?php if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
{ 
	$js_id = $this->common_front_model->get_userid();
	$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'employer','contacts');
} 
?>
<div class="clearfix"></div>
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/public-profile-img.jpg); background-size:cover;">
	<div class="container">
		<div class="ten columns">
			<h2><i class="fa fa-file-text"></i> <?php echo $custom_lable_arr['emp_profile']; ?> </h2>
			<!--<span>Resume Viewed: 92 Employers</span>-->
		</div>
	</div>
</div>
<div class="margin-top-40"></div>
<div id="container-fluid">
    <div class="col-md-3 col-sm-3 col-xs-12">
		<div class="row panel panel-default" style="padding:0px;border-radius:0px;">
			<div class="panel-body" style="padding:0px;">
				
                <?php if($emp_details!='' && $emp_details['profile_pic'] != ''  && $emp_details['profile_pic_approval'] == 'APPROVED' && !is_null($emp_details['profile_pic']) && file_exists($upload_path_profile_cmp.'/'.$emp_details['profile_pic']))
				{ echo '<img src="'.$base_url.'assets/emp_photos/'.$emp_details['profile_pic'].'"   style="width:100%" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"   style="width:100%" alt="" />'; } ?>
                
				<div class="text-center">
					<div class="col-md-8 col-sm-12 col-xs-8">
						<h3><?php echo $emp_details['personal_titles'] .' '. $emp_details['fullname']; ?></h3>
					</div>
					
				</div>
			</div>
			<hr>
			<div class="panel-body" style="margin-top:-10px;">
            <?php 
			if(isset($js_id) && $js_id!='')
			{
				if($return_data_plan == 'Yes')
				{
					 ?>
				<div class="row col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
				  <span><i class="glyphicon glyphicon-envelope"></i><?php echo ( $this->common_front_model->checkfieldnotnull($emp_details['email'])) ? $emp_details['email'] : "Not Available";?></span>
					
				</div>
                <hr>
                <?php 
					}//if plan is valid
				} ?>
				<div class="row col-md-12 col-sm-12 col-xs-12 text-center margin-bottom-20">
					<div class="row margin-bottom-10 text-center">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-primary btn-twitter box-shadow-radius" title="<?php echo $custom_lable_arr['share_twitter']; ?>" onclick="window.open('http://twitter.com/home?status=<?php echo urlencode($base_url.'share-profile/emp-profile/'.base64_encode($emp_details['id'])); ?>')" data-toggle="tooltip" ><i class="fa fa-twitter"></i></button>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-danger box-shadow-radius"   rel="publisher" data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_gplus']; ?>" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode($base_url.'share-profile/emp-profile/'.base64_encode($emp_details['id']))?>')"><i class="fa fa-google-plus"></i></button>
						</div>
                        
                        <?php
						$js_photo  = '';
						if(isset($emp_details['profile_pic']) && $emp_details['profile_pic_approval']=='APPROVED')
							{
								$js_photo = $base_url.'assets/emp_photos/'.$emp_details['profile_pic'];
							}
						?>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-primary box-shadow-radius" rel="publisher" title="<?php echo $custom_lable_arr['share_facebook']; ?>" data-toggle="tooltip" onClick=         "window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo urlencode($emp_details['fullname']); ?>&amp;p[url]=<?php echo urlencode($base_url.'share-profile/emp-profile/'.base64_encode($emp_details['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($js_photo); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"><i class="fa fa-facebook"></i></button>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-info box-shadow-radius" rel="linkedin"  data-toggle="tooltip"  title="<?php echo $custom_lable_arr['share_linkedin']; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  echo $base_url.'share-profile/emp-profile/'.base64_encode($emp_details['id']); ?>&title=<?php echo urlencode($emp_details['fullname']); ?>&summary=<?php echo urlencode($emp_details['fullname']); ?>&source=<?php echo urlencode($emp_details['fullname']); ?>')"><i class="fa fa-linkedin"></i></button>
						</div>
                        
                        <div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-danger box-shadow-radius" title="<?php echo $custom_lable_arr['share_pinterest']; ?>" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php echo urlencode($base_url.'share-profile/emp-profile/'.base64_encode($emp_details['id'])); ?>	&media=<?php echo urlencode($js_photo); ?>&description=<?php echo urlencode($emp_details['fullname']); ?>')" data-toggle="tooltip" ><i class="fa fa-pinterest-square"></i></button>
						</div>
					</div>
				</div>
				<!--<div class="col-md-12 col-sm-12 col-xs-12 text-center margin-bottom-20">
					<div class="row rating-block text-center">
					<hr>
						<h4>Average user rating</h4>
						<h2 class="bold padding-bottom-7">4.3 <small>/ 5</small></h2>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
					</div>
				</div>-->
			</div>
		</div><br />
    </div>
	<!--<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="panel panel-default text-center" style="padding:10px;border-radius:0px;">
			<div class="user-rating col-md-4 col-sm-6 col-xs-12 text-center">
				<h5>Rating breakdown</h5>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 100%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">1</div>
				</div>
				<div class="pull-left">
					<div class="pull-left">
						<div>4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">1</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center" style="border-right: 1px dotted grey;border-left: 1px dotted grey;">
				<h5><i class="fa fa-asterisk"></i> Skills</h5>
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:70%"> 70% <span class="small-13">HTML</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> 40% <span class="small-13">CSS</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:60%"> 60% <span class="small-13">Javascript</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:80%"> 80% <span class="small-13">Jquery</span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center">
				<h4><i class="fa fa-globe"></i> Languages</h4>
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:80%"> 80% <span class="small-13">English</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> 40% <span class="small-13">Hindi</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:88%"> 88% <span class="small-13">Gujarati</span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>-->
    <div class="col-md-9 col-sm-9 col-xs-12">
		<div data-spy="scroll" class="panel panel-default" style="padding:10px;border-radius:0px;">
			<div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_default_1" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['emp_share_profile_c_details']; ?></a>
					</li>
					<li>
						<a href="#tab_default_2" data-toggle="tab"><i class="fa fa-lg fa-graduation-cap"></i> <?php echo $custom_lable_arr['emp_share_profile_sector_for_hire']; ?></a>
					</li>
                    
                    <li>
						<a href="#tab_default_3" data-toggle="tab"><i class="fa fa-lg fa-graduation-cap"></i> <?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']; ?></a>
					</li>
					
                    
				</ul>
				<div class="clearfix"></div>
				<div class="tab-content">
					
                    <div class="tab-pane fade in active" id="tab_default_1" style="padding:10px;">
						<div class="row margin-top-10">
							<div class="" style="padding:15px;border-radius:0px;">
								<h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['emp_share_profile_c_details']; ?> :</h3>
			                     
										<hr class="hr-dotted th_bgcolor">
										<div>
										<div class="clearfix"></div>
										
										
					<div class="margin-top-10">
                    
                          <table class="table table-user-information margin-top-20">
													<tbody>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoview']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php if($emp_details!='' && $emp_details['company_logo'] != '' && !is_null($emp_details['company_logo']) && file_exists($upload_path_profile_cmp.'/'.$emp_details['company_logo']))
				{ echo '<img src="'.$upload_path_profile_cmp.'/'.$emp_details['company_logo'].'"  class="blah1" alt="" />'; ?><?php }else{ echo $custom_lable_arr['notavilablevar']; } ?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6" style=""><?php echo $custom_lable_arr['sign_up_emp_companydet']; ?> :</td>
															<td class="col-md-6 col-xs-6" style=""><?php echo ($emp_details['company_name']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['company_name'])) ? $emp_details['company_name'] : "Not Available";?></td>
														</tr>
														
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php if($emp_details['company_type_name']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['company_type_name']) && $this->common_front_model->checkfieldnotnull($emp_details['company_type_name'])) { 
															
															echo $emp_details['company_type_name'];
																
															 }
															 else 
															 { 
																echo $custom_lable_arr['notavilablevar'];
															 } ?></td>
														</tr>
                                                        
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpsjizelbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php if($emp_details['company_size']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['company_size']) && $this->common_front_model->checkfieldnotnull($emp_details['company_size_name'])) { 
															
															echo $emp_details['company_size_name'];
																
															 }
															 else 
															 { 
																echo $custom_lable_arr['notavilablevar'];
															 } ?></td>
														</tr>
                                                        <?php 
														if(isset($js_id) && $js_id!='')
														{
															if($return_data_plan == 'Yes')
															{
																 ?>
																	<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpweblbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['company_website']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['company_website'])) ? $emp_details['company_website'] : "Not Available";?></td>
														</tr>
														<?php 
															}//if plan is valid
														} ?>
														
                                                        
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpprolbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['company_profile']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['company_profile'])) ? $emp_details['company_profile'] : $custom_lable_arr['notavilablevar'];?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_industrylbl']; ?>:</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['industries_name']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['industries_name'])) ? $emp_details['industries_name'] : "Not Available";?></td>
														</tr>
                                                        <?php 
														if(isset($js_id) && $js_id!='')
														{
															if($return_data_plan == 'Yes')
															{
																 ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformmob']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['mobile']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['mobile'])) ? $emp_details['mobile'] : $custom_lable_arr['notavilablevar'];?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformcnt']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['city']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['country_name']) && $this->common_front_model->checkfieldnotnull($emp_details['country_name'])) ? $emp_details['country_name'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
                                                        <?php 
														if(isset($js_id) && $js_id!='')
														{
															if($return_data_plan == 'Yes')
															{
																 ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformcity']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['city']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['city']) && $this->common_front_model->checkfieldnotnull($emp_details['city_name'])) ? $emp_details['city_name'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformaddress']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($emp_details['address'])) ? $emp_details['address'] : "Not Available"; ?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformpincode']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($emp_details['pincode']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['pincode'])) ? $emp_details['pincode'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
													</tbody>
												</table>
				</div>
                
			</div>
               
              
			
             <div class="clearfix"></div>
		</div>
						</div>
					</div>
					<div class="tab-pane" id="tab_default_2" style="padding:10px;">
						<div class="row margin-top-10">
							<div class="" style="padding:15px;border-radius:0px;">
								<h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['emp_share_profile_sector_for_hire']; ?> :</h3>
			                     
										<hr class="hr-dotted th_bgcolor">
										<div>
										<div class="clearfix"></div>
										
										
					<div class="margin-top-10">
                    
                    <table class="table table-user-information margin-top-20">
												<tbody>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['indu_hire_for_share_emp_lbl']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php $industries_stored = ($emp_details['industry_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['industry_hire'])) ? $this->employer_profile_model->getdetailsfromids('industries_master','id',$emp_details['industry_hire'],'industries_name') : "";
														if($this->common_front_model->checkfieldnotnull($industries_stored) && count($industries_stored) > 0)
														{
															echo implode(',',$industries_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['fn_area_for_share_emp_lbl']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php $fnarea_stored = ($emp_details['function_area_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['function_area_hire'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$emp_details['function_area_hire'],'functional_name') : "";
														if($this->common_front_model->checkfieldnotnull($fnarea_stored) && is_array($fnarea_stored) && count($fnarea_stored) > 0)
														{
															echo implode(',',$fnarea_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?>
                                                        </td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['skill_for_share_emp_lbl']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php $skillhire_stored = ($emp_details['skill_hire']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['skill_hire'])) ? $this->employer_profile_model->getdetailsfromids('key_skill_master','id',$emp_details['skill_hire'],'key_skill_name') : "";
														if($this->common_front_model->checkfieldnotnull($skillhire_stored) && is_array($skillhire_stored) && count($skillhire_stored) > 0)
														{
															echo implode(',',$skillhire_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?>
                                                        </td>
													</tr>
												</tbody>
											</table>
						</div>
			</div>
             <div class="clearfix"></div>
		</div>
						</div>
					</div>
                    
                    <div class="tab-pane" id="tab_default_3" style="padding:10px;">
						<div class="row margin-top-10">
							<div class="" style="padding:15px;border-radius:0px;">
								<h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']; ?> :</h3>
			                     
										<hr class="hr-dotted th_bgcolor">
										<div>
										<div class="clearfix"></div>
											<div class="margin-top-10">
                    <?php $fnarea_stored = ($emp_details['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['functional_area'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$emp_details['functional_area'],'functional_name') : ""; 
					
					$designation_stored = ($emp_details['designation']!='0' && $this->common_front_model->checkfieldnotnull($emp_details['designation'])) ? $this->employer_profile_model->getdetailsfromids('role_master','id',$emp_details['designation'],'role_name') : "";
					?>
                   							 <table class="table">
													<tbody>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['functional_area_empl_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php 
															if($this->common_front_model->checkfieldnotnull($fnarea_stored) && is_array($fnarea_stored) && count($fnarea_stored) > 0)
															{
																echo implode(',',$fnarea_stored);
															}
															else
															{
																echo $custom_lable_arr['notavilablevar'];
															}
															?>
															</td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['current_role_empl_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php 
															if($this->common_front_model->checkfieldnotnull($designation_stored) && is_array($designation_stored) && count($designation_stored) > 0)
															{
																echo implode(',',$designation_stored);
															}
															else
															{
																echo $custom_lable_arr['notavilablevar'];
															}
															?>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
                						</div>
             <div class="clearfix"></div>
		</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
	
	<div class="col-md-3 col-sm-3 col-xs-12"></div>
	<!--<div class="col-md-9 col-sm-9 col-xs-12 margin-bottom-20">
		<div class="panel panel-default" style="padding:15px;border-radius:0px;">
			<h3 class="text-bg"><i class="fa fa-certificate"></i> Company Projects:</h3>
			<hr>
			<div>
				<h5 class="text-bg"><b>W3Schools.com</b></h5>
				<h6><i class="fa fa-calendar"></i> Forever</h6>
				<p>Web Development! All I need to know in one place</p>
				<hr>
			</div>
			<div>
				<h5 class="text-bg"><b>London Business School</b></h5>
				<h6><i class="fa fa-calendar"></i> 2013 - 2015</h6>
				<p>Master Degree</p>
				<hr>
			</div>
			<div>
				<h5 class="text-bg"><b>School of Coding</b></h5>
				<h6><i class="fa fa-calendar"></i> 2010 - 2013</h6>
				<p>Bachelor Degree</p><br />
			</div>
		</div>
    </div>-->
</div>
<div class="clearfix"></div>

<script>
function download_resume(file)
{
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&file='+file+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'share_profile/download_resume' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
			if(data.status=='success')
			{
				window.open(data.file_download);
			}
			else
			{
				alert(data.errmessage);
			}
			hide_comm_mask(); 
		}
	});	
	
}

</script>
<?php 
			if(isset($js_id) && $js_id!='')
			{
				if($return_data_plan == 'Yes')
				{
					$this->common_front_model->check_for_plan_update($js_id,'job_seeker',$emp_details['id']);
				}//if plan is valid
			} ?>