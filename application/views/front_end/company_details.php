<?php
$custom_lable_arr = $custom_lable->language;
$upload_path_cmp = $this->common_front_model->fileuploadpaths('company_logos',1);
$upload_path_profile = $this->common_front_model->fileuploadpaths('emp_photos',1);
$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
 $tab_active1= 'active';
  $tab_active_chan = '';
  if(isset($tab_active) && $tab_active =='tab_default_2')
  {
	$tab_active_chan = 'active';
	$tab_active1= '';
  }
?>
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/slideanim.css">
<style>
.th_bgcolor {
    background-color: transparent !important;
}
.social-icons-2 div {
    vertical-align: top;
    display: inline;
    height: 100px;
    border: 0px !important;
    background-color: transparent!important;
    position: relative;
    top: -5px;
    left: 16px;
}
.list-search input
{
	margin-top: -56px;
}
/* for this page only */
.tabbable-panel,.tabbable-line > .tab-content
{
	color:inherit !important;/* for this page only */
}
.tab-content > .tab-pane.active,.tab-content > .tab-pane.active > a
{
	color:inherit !important;/* for this page only */
}
.new-full-time{
 color: white;
    font-weight: bold;
    padding: 5px;
}
/* for this page only */
</style>
<?php
$return_data_plan = 'No';
if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
{
	$js_id = $this->common_front_model->get_userid();
	$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
	$update_on = $this->common_front_model->getCurrentDate();
}
?>
<?php
if(isset($companydata) && $companydata!='' && is_array($companydata) && count($companydata) > 0)
{
$company_logo = '';
if(isset($companydata['company_logo']) && $companydata['company_logo']!='')
{
	$company_logo = $base_url.'assets/company_logos/'.$companydata['company_logo'];
}
if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
{
	if(isset($companydata['email']) && $companydata['email'] !='')
	{
		$companydata['email'] = $this->common_model->emial_disp;
	}
	if(isset($companydata['mobile']) && $companydata['mobile'] !='')
	{
		$companydata['mobile'] = $this->common_model->mobile_disp;
	}
}
?>
<div class="clearfix"></div>
<div class="container-fluid">
	<div class="row">

		<div class="fb-profile">
			<?php  if($companydata['company_logo_approval'] == 'APPROVED' && $companydata['company_logo'] != '' && !is_null($companydata['company_logo']) && file_exists($upload_path_cmp.'/'.$companydata['company_logo']))
				{  ?>
                                <!--<img class="fb-image-lg" src="<?php echo $base_url ;?>assets/company_logos/<?php echo $companydata['company_logo'] ;?>" alt="<?php echo $companydata['company_name']?>" style="max-height:400px;"/>--><!--class="img-responsive"-->
                                <?php }else { ?>
                                <!--<img src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $companydata['company_name']?>" class="fb-image-lg" style="max-height:400px;"/>-->
                                <?php } ?>
                                <img class="fb-image-lg" src="<?php echo $base_url ;?>assets/front_end/images/banner/staffing.png" alt="<?php echo $companydata['company_name']?>" style="max-height:400px;"/>
			<div class="col-md-3 col-xs-3">
            	<?php  if($companydata['profile_pic_approval'] == 'APPROVED' && $companydata['profile_pic'] != '' && !is_null($companydata['profile_pic']) && file_exists($upload_path_profile.'/'.$companydata['profile_pic']))
				{  ?>
                                <img class="fb-image-profile thumbnail" src="<?php echo $base_url ;?>assets/emp_photos/<?php echo $companydata['profile_pic'] ;?>" alt="<?php echo $companydata['fullname']?>"/><!--class="img-responsive"-->
                                <?php }else { ?>
                                <img src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $companydata['fullname']?>" class="fb-image-profile thumbnail" />
                                <?php } ?>
			</div>

			<div class="col-md-9 col-sm-9  col-xs-12 cinfo">
				<h1 class="fb-profile-text margin-bottom-0"><?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_name'])) ? $companydata['company_name'] : $custom_lable_arr['notavilablevar'] ; ?></h1>
                <?php
			if(isset($js_id) && $js_id!='')
			{
				if($return_data_plan == 'Yes')
				{
					 ?>
				<span class="small"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:<?php echo ($this->common_front_model->checkfieldnotnull($companydata['email'])) ? $companydata['email'] : "" ; ?>"><?php echo ($this->common_front_model->checkfieldnotnull($companydata['email'])) ? $companydata['email'] : $custom_lable_arr['notavilablevar'] ; ?></a></span><br />
				<span class="small"><i class="glyphicon glyphicon-globe"></i> <a target="_blank" href="<?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_website'])) ? $companydata['company_website'] : "javascript:void(0);" ; ?>"><?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_website'])) ? $companydata['company_website'] : $custom_lable_arr['notavilablevar'] ; ?></a></span>
                <?php
				}//if plan is valid
			} ?>
			</div>

		</div>
		<div class="Container">
			<div class="">
				<div class="col-md-9 col-sm-9 col-md-offset-3 col-sm-offset-3 col-xs-12 xs">

					<?php
                    if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
                    { ?>
                    <span class="small"><a href="#small-dialog" class="popup-with-zoom-anim btn btn-default"><i class="glyphicon glyphicon-comment"></i><?php echo  $custom_lable_arr['send_msg']; ?></a></span>
                    <!--<span class="small"><a href="#small-dialog" class="popup-with-zoom-anim btn btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a></span>-->
                    <div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">

                        <?php
                        if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
                        { ?>
                        <div class="small-dialog-headline">
                          <i class="fa fa-envelope"></i> <?php echo  $custom_lable_arr['send_msg']; ?>
                        </div>
                        <div class="clearfix margin-bottom-10"></div>
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>

                        <div class="small-dialog-content">
                            <form  method="post"  id="send_msg_form" >
                                <div>
                                <input type="text" name="subject" data-validation="required" placeholder="Subject" value="" />
                                </div>
                                <div>
                                <textarea placeholder="Message" data-validation="required" name="content" ></textarea>
                                </div>
                                <button class="send" ><i class="fa fa-check"></i> <?php echo $custom_lable_arr['send_msg']; ?></button>
                                <input type="hidden" value="NI-WEB" name="user_agent">
                                <input type="hidden" value="send_message" name="action">
                                <input type="hidden" value="<?php echo $companydata['id']; ?>" name="emp_id">
                            </form>
                        </div>
                        <?php }
                        else
                        { $this->session->set_userdata($return_after_login_url); ?>
                        <div class="">
                            <div class="alert alert-danger">
                                 <?php echo  $custom_lable_arr['please_login']; ?>
                            </div>
                        </div>
                        <?php }
                         ?>
                    </div>
                    <!--<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
						<div class="small-dialog-headline">
							<h2><span class="glyphicon glyphicon-envelope"></span> Send Message to John Doe</h2>
						</div>
						<div class="small-dialog-content">
							<form action="#" method="get" >
								<input type="text" placeholder="Full Name" value="" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Your Full Name (\, @, '', -, Special Character not Allowed)" />
								<input type="text" placeholder="Email Address" value="" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Your Email Id" />
								<textarea placeholder="Message" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Message"></textarea>
								<button class="send">Send Application</button>
							</form>
						</div>
					</div>-->

                    <?php }
                    ?>
					<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
						<div class="small-dialog-headline">
							<h2><span class="glyphicon glyphicon-envelope"></span> Send Message to John Doe</h2>
						</div>
						<div class="small-dialog-content">
							<form action="#" method="get" >
								<input type="text" placeholder="Full Name" value="" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Your Full Name (\, @, '', -, Special Character not Allowed)" />
								<input type="text" placeholder="Email Address" value="" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Your Email Id" />
								<textarea placeholder="Message" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Please Write Message"></textarea>
								<button class="send">Send Application</button>
							</form>
						</div>
					</div>
                     <?php
						$block_action = 'block_emp';
						$block_icon = '<span class="glyphicon glyphicon-ban-circle"></span>';
						$block_text = $custom_lable_arr['block'];
						$follow_action = 'follow_emp';
						$follow_icon = '<span class="glyphicon glyphicon-user"></span>';
						$follow_icon_class = 'btn btn-success btn-block';
						$follow_icon_class_ico = 'glyphicon glyphicon-thumbs-up';
						$follow_text = $custom_lable_arr['follow'];
						$like_emp_action = 'like_emp';
						$like_emp_icon = '<span class="glyphicon glyphicon-thumbs-up"></span>';
						$like_emp_text = 'Like';
						if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
						{
							if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
							{
								//$js_id = $this->common_front_model->get_userid();
								$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$companydata['id']);
								$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
							}
							if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) && count($jobseeker_access_employer) > 0)
							{
								if($jobseeker_access_employer['is_liked']=='Yes')
								{
									$like_emp_action = 'unlike_emp';
									$like_emp_icon = '<span class="glyphicon glyphicon-check"></span>';
									$like_emp_text = $custom_lable_arr['liked'];
								}
								if($jobseeker_access_employer['is_follow']=='Yes')
								{
									$follow_action = 'unfollow_emp';
									$follow_icon = '<span class="glyphicon glyphicon-check"></span>';
									$follow_text = $custom_lable_arr['following'];
									$follow_icon_class = 'btn btn-danger btn-block';
									$follow_icon_class_ico = 'glyphicon glyphicon-thumbs-down';
								}
								else
								{
									$follow_icon_class = 'btn btn-success btn-block';
									$follow_icon_class_ico = 'glyphicon glyphicon-thumbs-up';
								}
								if($jobseeker_access_employer['is_block']=='Yes')
								{
									$block_action = 'unblock_emp';
									$block_icon = '<span class="glyphicon glyphicon-check"></span>';
									$block_text = $custom_lable_arr['blocked'];
								}
							}
							?>
                            <?php  } ?>
					<div class="btn-group">
						<a class="small btn dropdown-toggle btn-default" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-cog"></span> <?php echo $custom_lable_arr['action'] ?><span class="caret"></span>
						</a>
						<ul class="small dropdown-menu" style="padding:5px;">
                        		<li><a  class="emp_block_action " style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $block_action; ?>','Emp','<?php echo $companydata['id']; ?>');"><?php echo $block_icon; ?> <?php echo $block_text; ?></a></li>
								<li class="divider"></li>
								<li><a class="emp_follow_action" style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $follow_action; ?>','Emp','<?php echo $companydata['id']; ?>');"><?php echo $follow_icon; ?> <?php echo $follow_text; ?></a></li>
                                <li class="divider"></li>
								<li><a  class="emp_like_action" style="cursor:pointer;" OnClick="return jobseeker_action('<?php echo $like_emp_action; ?>','Emp','<?php echo $companydata['id']; ?>');"><?php echo $like_emp_icon; ?> <?php echo $like_emp_text; ?></a></li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container margin-bottom-50" id="about_recruiters">
<hr>
<div class="clearfix"></div>
<div id="js_action_msg_div"></div>
<div id="js_action_emp_div"></div>
	<div class="">

		<div class="col-md-9 col-sm-9 col-xs-12">
			<div data-spy="scroll" class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs">
						<li class="<?php echo $tab_active1;?>">
							<a href="#tab_default_1" class="btn bt-default" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['company_profil_sin']; ?></a>
						</li>
						<li class="<?php echo $tab_active_chan;?>">
							<a href="#tab_default_2"  class="btn bt-primary" data-toggle="tab"><i class="fa fa-lg fa-star"></i> <?php echo $custom_lable_arr['rec_profil_sin']; ?></a>
						</li>
						<li>
							<a href="#tab_default_3" data-toggle="tab"><span class="glyphicon glyphicon-briefcase"></span> <?php echo $custom_lable_arr['our_work_exp_sin']; ?></a>
						</li>
						<li>
							<a href="#tab_default_4" data-toggle="tab"><span class="glyphicon glyphicon-send"></span> <?php echo $custom_lable_arr['skils_hire_sin']; ?></a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane <?php echo $tab_active1; ?>" id="tab_default_1">
							<?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_profile'])) ? $companydata['company_profile'] : $custom_lable_arr['notavilablevar'] ; ?>
						</div>
						<div class="tab-pane <?php echo $tab_active_chan;?>" id="tab_default_2">
							<?php echo ($this->common_front_model->checkfieldnotnull($companydata['job_profile'])) ? $companydata['job_profile'] : $custom_lable_arr['notavilablevar'] ; ?>
						</div>
						<div class="tab-pane" id="tab_default_3">
							<ul class="margin-top-20">
								<li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['myprofile_emp_industrylbl'] ; ?> : <?php echo ($companydata['industries_name']!='0' && $this->common_front_model->checkfieldnotnull($companydata['industries_name'])) ? $companydata['industries_name'] :  $custom_lable_arr['notavilablevar']; ?></span><!--</a>--></li>
                                <li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['functional_area_empl_lbl'] ; ?> : <?php $fnarea_stored = ($companydata['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($companydata['functional_area'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$companydata['functional_area'],'functional_name') : "";
														if($this->common_front_model->checkfieldnotnull($fnarea_stored) && count($fnarea_stored) > 0)
														{
															echo implode(', ',$fnarea_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?></span><!--</a>--></li>
								<li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['current_role_empl_lbl'] ; ?> : <?php
								$designation_stored = ($companydata['designation']!='0' && $this->common_front_model->checkfieldnotnull($companydata['designation'])) ? $this->employer_profile_model->getdetailsfromids('role_master','id',$companydata['designation'],'role_name') : "";
															if($this->common_front_model->checkfieldnotnull($designation_stored) && is_array($designation_stored) && count($designation_stored) > 0)
															{
																echo implode(', ',$designation_stored);
															}
															else
															{
																echo $custom_lable_arr['notavilablevar'];
															}
															?></span><!--</a>--></li>
							</ul>
						</div>
						<div class="tab-pane" id="tab_default_4">
                        	<ul>
							<li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['indu_hire_for_lbl'] ; ?> : <?php $industries_stored = ($companydata['industry_hire']!='0' && $this->common_front_model->checkfieldnotnull($companydata['industry_hire'])) ? $this->employer_profile_model->getdetailsfromids('industries_master','id',$companydata['industry_hire'],'industries_name') : "";
														if($this->common_front_model->checkfieldnotnull($industries_stored) && is_array($industries_stored) && count($industries_stored) > 0)
														{
															echo implode(', ',$industries_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?></span><!--</a>-->
							</li>
                            <li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['fn_hire_for_lbl'] ; ?> : <?php $fnarea_stored_he = ($companydata['function_area_hire']!='0' && $this->common_front_model->checkfieldnotnull($companydata['function_area_hire'])) ? $this->employer_profile_model->getdetailsfromids('functional_area_master','id',$companydata['function_area_hire'],'functional_name') : "";
														if($this->common_front_model->checkfieldnotnull($fnarea_stored_he) && is_array($fnarea_stored_he) && count($fnarea_stored_he) > 0)
														{
															echo implode(', ',$fnarea_stored_he);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?></span><!--</a>-->
							</li>
                            <li><span class="glyphicon glyphicon-triangle-right"></span> <span><!--<a href="javascript:void(0);">--><?php echo $custom_lable_arr['skill_hire_for_lbl'] ; ?> : <?php $skillhire_stored = ($companydata['skill_hire']!='0' && $this->common_front_model->checkfieldnotnull($companydata['skill_hire'])) ? $this->employer_profile_model->getdetailsfromids('key_skill_master','id',$companydata['skill_hire'],'key_skill_name') : "";
														if($this->common_front_model->checkfieldnotnull($skillhire_stored) && is_array($skillhire_stored) && count($skillhire_stored) > 0)
														{
															echo implode(', ',$skillhire_stored);
														}
														else
														{
															echo $custom_lable_arr['notavilablevar'];
														}
														?></span><!--</a>-->
							</li>
                            </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div data-spy="scroll" class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs">
						<li class="active" <?php if(!isset($js_id) || (isset($js_id) && $js_id=='') || !isset($return_data_plan)  || (isset($return_data_plan) && $return_data_plan!='Yes')){ ?> style="width:100%;text-align:center;" <?php } ?>>
							<a href="#tab_default_5" data-toggle="tab"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Following</a>
						</li>
                         <?php
			if(isset($js_id) && $js_id!='')
			{
				if($return_data_plan == 'Yes')
				{
					 ?>
						<li>
							<a href="#tab_default_6" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span><strong> Address</strong></a>
						</li>
                        <?php
				}//if plan is valid
			} ?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_5">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 divider text-center">
									<div class="col-xs-12 col-sm-12 emphasis margin-top-20">
										<h4 class="line-height follower_count_<?php echo $companydata['id']; ?>"><?php echo $this->our_recruiters_model->getrecruiterstotal_follower_count($companydata['id']); ?></h4>
										<small><?php echo $custom_lable_arr['followers']; ?></small>
										<button type="button"  OnClick="return jobseeker_action('<?php echo $follow_action; ?>','Emp','<?php echo $companydata['id']; ?>');"class="<?php echo $follow_icon_class; ?> emp_follow_action_custom"><span class="<?php echo $follow_icon_class_ico; ?>"></span><?php echo $follow_text; ?></button>
									</div>
								</div>

								<div class="col-md-12 col-xs-12 divider text-center">
								<hr class="margin-top-20">
									<div class="margin-top-20">
										<!--<a class="btn btn-primary btn-twitter btn-circle" data-toggle="tooltip" title="twitter"  href="https://twitter.com/webmaniac"><i class="fa fa-twitter"></i>
										</a>-->
										<!--<a class="btn btn-danger" rel="publisher" data-toggle="tooltip" title="Google+" href="https://plus.google.com/+ahmshahnuralam"><i class="fa fa-google-plus"></i>
										</a>-->
										<!--<a class="btn btn-primary" rel="publisher"  data-toggle="tooltip" title="facebook" href="https://plus.google.com/shahnuralam"><i class="fa fa-facebook"></i>
										</a>-->
										<!--<a class="btn btn-warning" rel="publisher"  data-toggle="tooltip" title="publisher" href="https://plus.google.com/shahnuralam"><i class="fa fa-behance"></i>
										</a>-->
                                        <a class="btn btn-primary btn-twitter btn-circle" target="_blank" data-toggle="tooltip" href="http://twitter.com/home?status=<?php echo urlencode($base_url.'share_profile/emp_profile/'.base64_encode($companydata['id'])); ?>" title="<?php echo $custom_lable_arr['share_twitter']; ?>"><i class="fa fa-twitter"></i>
				</a>
				<a class="btn btn-danger" data-toggle="tooltip" target="_blank" title="<?php echo $custom_lable_arr['share_gplus']; ?>" href="https://plus.google.com/share?url=<?php echo urlencode($base_url.'share_profile/emp_profile/'.base64_encode($companydata['id'])); ?>"><i class="fa fa-google-plus"></i>
				</a>

				<a class="btn btn-primary" data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_facebook']; ?>" onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($base_url.'share_profile/emp_profile/'.base64_encode($companydata['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($company_logo); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="fa fa-facebook" ></i>
				</a>

				<a class="btn btn-info" target="_blank" data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_linkedin']; ?>" href=" https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($base_url.'share_profile/emp_profile/'.base64_encode($companydata['id'])); ?>&title=<?php echo urlencode($companydata['company_name']); ?>
&summary=<?php echo urlencode($companydata['company_name']); ?>&source=<?php echo urlencode($companydata['company_name']); ?>"><i class="fa fa-linkedin"></i>
				 </a>

                 <a class="btn btn-danger" target="_blank" data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_pinterest']; ?>" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode($base_url.'share_profile/emp_profile/'.base64_encode($companydata['id'])); ?>	&media=<?php echo urlencode($company_logo); ?>&description=<?php echo urlencode($companydata['company_name']); ?>"><i class="fa fa-pinterest-square"></i>
				 </a>
									</div>
								</div>

							</div>
						</div>
                        <?php
			if(isset($js_id) && $js_id!='')
			{
				if($return_data_plan == 'Yes')
				{
					 ?>
						<div class="tab-pane" id="tab_default_6">
							<div class="row">
								<div class="col-lg-12 padding-10">
									<div class="form-group">
										<label><strong>Our Contatct:</strong></label>
										<span class="small"><span class="glyphicon glyphicon-map-marker"></span> <?php echo ($this->common_front_model->checkfieldnotnull($companydata['address'])) ? $companydata['address'] : $custom_lable_arr['notavilablevar'] ; ?></span><br />
										<span class="small"><span class="glyphicon glyphicon-earphone"></span> <?php echo ($this->common_front_model->checkfieldnotnull($companydata['mobile'])) ? $companydata['mobile'] : $custom_lable_arr['notavilablevar'] ; ?></span><br />
										<span class="small"><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:<?php echo ($this->common_front_model->checkfieldnotnull($companydata['email'])) ? $companydata['email'] : "" ; ?>"><?php echo ($this->common_front_model->checkfieldnotnull($companydata['email'])) ? $companydata['email'] : $custom_lable_arr['notavilablevar'] ; ?></a></span><br />
										<span class="small"><i class="glyphicon glyphicon-globe"></i> <a target="_blank" href="<?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_website'])) ? $companydata['company_website'] : "javascript:void(0)" ; ?>"><?php echo ($this->common_front_model->checkfieldnotnull($companydata['company_website'])) ? $companydata['company_website'] : $custom_lable_arr['notavilablevar'] ; ?></a></span>
									</div>
								</div>
							</div>
						</div>
                        <?php
				}//if plan is valid
			} ?>
					</div>
				</div>
			</div>
            <?php
			$advert_data = $this->common_front_model->getadvertisement('Level 1');
			if(isset($advert_data) && $advert_data!='' && is_array($advert_data) && count($advert_data) > 0)
			{ ?>
        <div class="widget">
			<h4 class="margin-bottom-25"><?php echo  $custom_lable_arr['blog_right_ad_title']; ?></h4>
            <?php

					if(isset($advert_data['type']) && $advert_data['type']!='' && $advert_data['type']=='Image')
					{
			?>
            	<div class="margin-top-10">
            	<a href="<?php echo $advert_data['link']; ?>" target="_blank"><img src="<?php echo $base_url; ?>assets/advertisement/<?php echo $advert_data['image']; ?>" alt="Ad" /></a>
            <?php 	}elseif(isset($advert_data['type']) && $advert_data['type']!='' && $advert_data['type']=='Google')
					{ echo $advert_data['google_adsense']; ?>

            <?php 	}
			 ?>
             </div>
		</div>
        <?php } ?>
		</div>
	</div>
</div>
<hr>

	<!-- Posting Jobs -->
<div class="container margin-bottom-50" id="activejobs">
	<div class="row tabbable-panel">
		<div class="sixteen columns">
			<div class="panel-heading title"><span class="glyphicon glyphicon-briefcase"></span> Job Posting by Company</div>
				<div class="panel-body">
<?php

$varforpass = array();
$all_company_job_posted_count = $this->our_recruiters_model->getrecruitersalljob_count($companydata['id']);
if($all_company_job_posted_count > 0)
{
$all_company_job_posted = $this->our_recruiters_model->getrecruitersalljob($companydata['id']);
$varforpass['total_post_count'] = $all_company_job_posted_count;
$varforpass['emp_posted_job'] = $all_company_job_posted;
$varforpass['custom_lable'] = $custom_lable;
$varforpass['company_id_pass'] = $companydata['id'];
$varforpass['limit_per_page'] = 3;
	 ?>
				<!--<form action="#" method="get" class="list-search margin-top-30">
					<button><i class="fa fa-search"></i></button>
					<input type="text" placeholder="Search job posted by company" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="You can search like: Core Php Dev./ Resturant Team Manager" />
					<div class="clearfix"></div>
				</form>-->
                <div class="alert alert-danger" id="error_msg_apply_job" style="display:none" > </div>
                <div class="alert alert-success" id="success_msg_apply_job" style="display:none" ></div>
                <div id="main_content_ajax">
				<div class="job-list">
                	<?php //$this->load->view('front_end/page_part/job_search_result_view_new',$varforpass); ?>
                    <?php $this->load->view('front_end/page_part/job_search_result_view_company',$varforpass); ?>
                </div>
				</div>
<?php }
else
{ ?>
		<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
<?php
}
?>

					</div>
				</div>
		</div>
</div>

<!-- Companies logo section start -->
<?php
	$this->load->view('front_end/company_logo_list');
?>
<!-- Companies logo section end -->

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

$(document).ready(function(e) {
	onpopupopenappend();
	validatemessageform();
if($("#ajax_pagin_ul").length > 0)
{
	load_pagination_code();
}
$('[data-toggle="tooltip"]').tooltip();
$('[data-toggle="popover"]').popover();
});

function set_time_out_msg(div_id)
{
	/*setTimeout(function(){ $('#'+div_id).html(''); $('#'+div_id).removeAttr('class');  }, 4000);*/
	setTimeout(function(){ $('#'+div_id).html(''); $('#'+div_id).hide();  }, 4000);
}
function onpopupopenappend()
{
	$( ".myclassclick" ).click(function() {
	  var emp_id = $(this).data("emp_id");
	  $( "#emp_id_msg" ).val(emp_id);
	  validatemessageform();
	});
}
function validatemessageform()
{
$.validate({
    form : '#send_msg_form',
    modules : 'security',
    onError : function($form) {
		$("#success_msg").hide();
	  $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('error_msg',-100);
	  $("#error_msg").html("<?php echo $custom_lable_arr['mandatory_field_msg']; ?>");
	  set_time_out_msg('error_msg');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#send_msg_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;

		$.ajax({
		url : "<?php echo $base_url.'job_seeker_action/send_message' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('send_msg_form').reset();
				scroll_to_div('success_msg',-100);
				set_time_out_msg('success_msg');
			}
			else
			{
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('error_msg',-100);
				set_time_out_msg('error_msg');
			}
		}
	});
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
}
/*function apply_for_job(job_id)
{
	<?php
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{
		//$this->session->set_userdata($return_after_login_url);

	?>    var message = '<?php //echo $this->lang->line('please_login_apply'); ?>';
	     $("#error_msg_apply_job").html(message);
		 $("#error_msg_apply_job").slideDown();
		 scroll_to_div('error_msg_apply_job',-100);
		  return false;
	<?php } ?>
	var c = confirm("<?php //echo $custom_lable_arr['confirm_apply_job']; ?>");
	if(c==true)
	{
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&job_id='+job_id+'&user_agent=NI-WEB';
		show_comm_mask();

		$.ajax({
		url : "<?php //echo $base_url.'job_seeker_action/apply_for_job' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg_apply_job").slideUp();
				$("#success_msg_apply_job").slideDown();
				$("#success_msg_apply_job").html(data.errmessage);
				set_time_out_msg('success_msg_apply_job');
				scroll_to_div('success_msg_apply_job',-100);
			}
			else
			{
				$("#success_msg_apply_job").slideUp();
				$("#error_msg_apply_job").html(data.errmessage);
				$("#error_msg_apply_job").slideDown();
				set_time_out_msg('error_msg_apply_job');
				scroll_to_div('error_msg_apply_job',-100);
			}

		}

		});
		hide_comm_mask();
	     return false;

	}
}*/
function apply_for_job(job_id)
{

	<?php
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{
	    //$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
		//$this->session->set_userdata($return_after_login_url);
		$return_after_login_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$this->session->set_userdata($return_after_login_url);
		?>
		if (typeof(Storage) !== "undefined") {
   	 		localStorage.setItem("return_after_login_url",'<?php echo $return_after_login_url; ?>');
   			}

				var message = '<?php echo $this->lang->line('please_login_apply'); ?>';
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
		   	    return false;
	<?php }
	?>

	var c = confirm("<?php echo $custom_lable_arr['confirm_apply_job']; ?>");
	if(c==true)
	{
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&job_id='+job_id+'&user_agent=NI-WEB';
		show_comm_mask();

		$.ajax({
		url : "<?php echo $base_url.'job_seeker_action/apply_for_job' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				$('.apply_for_job'+job_id).removeAttr('onClick');
				$('.apply_for_job'+job_id).css('cursor','default');
				$('.apply_for_job'+job_id).html('<i class="fa fa-check"></i>  <?php echo $this->lang->line('already_apply'); ?>');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			 hide_comm_mask();

		}

		});
		//hide_comm_mask();
	     return false;

	}

}
function jobseeker_action(action,action_for,action_id)
{
	<?php
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{
		//$this->session->set_userdata($return_after_login_url);

		$return_after_login_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$this->session->set_userdata($return_after_login_url);
		?>
		if (typeof(Storage) !== "undefined") {
   	 		localStorage.setItem("return_after_login_url",'<?php echo $return_after_login_url; ?>');
   			}

	   var message = '<?php echo $custom_lable_arr['please_login']; ?>';
			if(action_for=='Emp')
			{
					$('#js_action_emp_div').html('');
					$('#js_action_emp_div').slideUp('Slow');
					$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_emp_div').slideDown('Slow');
					set_time_out_msg('js_action_emp_div');
					scroll_to_div('js_action_emp_div',-100);

			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);

			}
			return false;
	<?php }
	?>
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&action='+action+'&action_for='+action_for+'&action_id='+action_id+'&user_agent=NI-WEB';
	$.ajax({
		url : "<?php echo $base_url.'job-seeker-action/seeker-action' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if(action_for=='Emp')
			{
				$('#js_action_emp_div').html('');
			    $('#js_action_emp_div').slideUp('Slow');
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
			}

			if(data.status=='success')
			{
				var followercount = parseInt($('.follower_count_'+action_id).html());
				if(action=='like_job')
				{
					$('.like_job_button').html('<i class="fa fa-check"></i> <?php echo $custom_lable_arr['liked']; ?>');
					$('.like_job_button').removeAttr('onClick');
					$('.like_job_button').attr('onClick','jobseeker_action("unlike_job","job",'+action_id+')');
				}
				if(action=='unlike_job')
				{
					$('.like_job_button').html('<i class="fa fa-thumbs-up"></i> <?php echo $custom_lable_arr['like']; ?>');
					$('.like_job_button').removeAttr('onClick');
					$('.like_job_button').attr('onClick','jobseeker_action("like_job","job",'+action_id+')');
				}
				if(action=='save_job')
				{
					$('.save_job_button'+action_id).html('<i class="fa fa-check"></i> <?php echo $custom_lable_arr['saved']; ?>');
					$('.save_job_button'+action_id).removeAttr('onClick');
					$('.save_job_button'+action_id).attr('onClick','jobseeker_action("remove_save_job","job",'+action_id+')')
				}
				if(action=='remove_save_job')
				{
					$('.save_job_button'+action_id).html('<i class="fa fa-save"></i> <?php echo $custom_lable_arr['save']; ?>');
					$('.save_job_button'+action_id).removeAttr('onClick');
					$('.save_job_button'+action_id).attr('onClick','jobseeker_action("save_job","job",'+action_id+')')
				}
				if(action=='block_emp')
				{
					$('.emp_block_action'+action_id).html('<span class="glyphicon glyphicon-check"></span> <?php echo $custom_lable_arr['blocked']; ?>');
					$('.emp_block_action'+action_id).removeAttr('onClick');
					$('.emp_block_action'+action_id).attr('onClick','jobseeker_action("unblock_emp","Emp",'+action_id+')')
				}
				if(action=='unblock_emp')
				{
					$('.emp_block_action'+action_id).html('<span class="glyphicon glyphicon-ban-circle"></span> <?php echo $custom_lable_arr['block']; ?>');
					$('.emp_block_action'+action_id).removeAttr('onClick');
					$('.emp_block_action'+action_id).attr('onClick','jobseeker_action("block_emp","Emp",'+action_id+')')
				}
				if(action=='follow_emp')
				{
					$('.emp_follow_action').html('<span class="glyphicon glyphicon-check"></span> <?php echo $custom_lable_arr['following']; ?>');
					$('.emp_follow_action').removeAttr('onClick');
					$('.emp_follow_action').attr('onClick','jobseeker_action("unfollow_emp","Emp",'+action_id+')');
					$('.emp_follow_action_custom').html('<span class="glyphicon glyphicon-thumbs-down"></span> <?php echo $custom_lable_arr['following']; ?>');
					$('.emp_follow_action_custom').removeAttr('onClick');
					$('.emp_follow_action_custom').attr('onClick','jobseeker_action("unfollow_emp","Emp",'+action_id+')');
					$('.emp_follow_action_custom').removeClass('btn-success').removeClass('btn-danger').addClass('btn-danger');

					if(followercount!=='')
					{
						var followercount_incre = followercount + 1;
						$('.follower_count_'+action_id).html(followercount_incre);
					}
				}
				if(action=='unfollow_emp')
				{
					$('.emp_follow_action').html('<span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['follow']; ?>');
					$('.emp_follow_action').removeAttr('onClick');
					$('.emp_follow_action').attr('onClick','jobseeker_action("follow_emp","Emp",'+action_id+')');
					$('.emp_follow_action_custom').html('<span class="glyphicon glyphicon-thumbs-up"></span> <?php echo $custom_lable_arr['follow']; ?>');
					$('.emp_follow_action_custom').removeAttr('onClick');
					$('.emp_follow_action_custom').attr('onClick','jobseeker_action("follow_emp","Emp",'+action_id+')');
					$('.emp_follow_action_custom').removeClass('btn-success').removeClass('btn-danger').addClass('btn-success');
					if(followercount!=='')
					{
							var followercount_decre = followercount - 1;
							$('.follower_count_'+action_id).html(followercount_decre);
					}
				}
				if(action=='like_emp')
				{
					$('.emp_like_action').html('<span class="glyphicon glyphicon-check"></span> <?php echo $custom_lable_arr['liked']; ?>');
					$('.emp_like_action').removeAttr('onClick');
					$('.emp_like_action').attr('onClick','jobseeker_action("unlike_emp","Emp",'+action_id+')')
				}
				if(action=='unlike_emp')
				{
					$('.emp_like_action').html('<span class="glyphicon glyphicon-thumbs-up"></span> <?php echo $custom_lable_arr['like']; ?>');
					$('.emp_like_action').removeAttr('onClick');
					$('.emp_like_action').attr('onClick','jobseeker_action("like_emp","Emp",'+action_id+')')
				}


				if(action_for=='Emp')
				{
					$('#js_action_emp_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_emp_div').slideDown('Slow');
				   set_time_out_msg('js_action_emp_div');
				   scroll_to_div('js_action_emp_div',-100);
				}
				else
				{
					$('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_msg_div').slideDown('Slow');
				   set_time_out_msg('js_action_msg_div');
				   scroll_to_div('js_action_msg_div',-100);
				}

			}
			else
			{
				if(action_for=='Emp')
				{
					$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_emp_div').slideDown('Slow');
					set_time_out_msg('js_action_emp_div');
					scroll_to_div('js_action_emp_div',-100);
				}
				else
				{
					$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_msg_div').slideDown('Slow');
					set_time_out_msg('js_action_msg_div');
					scroll_to_div('js_action_msg_div',-100);
				}
			}
		}
	});
	  hide_comm_mask();
	  return false;
}
</script>

<?php
}
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>
</div>
<?php
}
?>
<?php
if(isset($js_id) && $js_id!='')
{
	if($return_data_plan == 'Yes')
	{
		$where_arra_contact = array('js_id'=>$js_id,'employer_id'=>$companydata['id']);
		$get_count_contact = $this->common_front_model->get_count_data_manual('jobseeker_viewed_employer_contact',$where_arra_contact,0,'id','','','','');
		if($get_count_contact=='' || $get_count_contact=='0')
		{
			/* for updating plan details */
			$this->common_front_model->update_plan_detail($js_id,'job_seeker','contacts');
			/* for updating plan details end */
			/* for inserting that this company details is viewed */
			$data_insert_contact = array('last_viewed_on'=>$update_on,'js_id'=>$js_id,'employer_id'=>$companydata['id']);
			$this->common_front_model->update_insert_data_common('jobseeker_viewed_employer_contact',$data_insert_contact,'',0);
			/* for inserting that this job is viewed end */
		}
		else
		{
			/* for updating that this company details is already viewed */
			$data_update_contact = array('last_viewed_on'=>$update_on);
			$this->common_front_model->update_insert_data_common('jobseeker_viewed_employer_contact',$data_update_contact,$where_arra_contact);
			/* for updating that this company details is already viewed end */
		}
	}//if plan is valid
} ?>