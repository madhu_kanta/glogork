<?php $upload_path_profile ='./assets/emp_photos';$custom_lable_arr = $custom_lable->language; ?>
<div class="profile-head" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner/pricing-hero.png);background-repeat: no-repeat;background-size: cover;min-width: 100%;background-position: center center;">
			<div class="margin-top-10"></div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center col-md-offset-2">
				<img id="headmenuprofilediv" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $upload_path_profile.'/'.$user_data['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/demoprofileimge2.png<?php } ?>" class="img-responsive" />
			<h6><?php echo $user_data['fullname']; ?></h6>
				<div class="margin-top-10"></div>
                <?php /* ?>
				<div class="dropdown" style="color:black;">
					<button class="btn btn-warning text-center dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Visibility <span class="caret"></span></button>
					<ul class="dropdown-menu" style="margin:auto;">
						<li ><a href="#" class="text-left"><span class="glyphicon glyphicon-globe"></span> Public</a></li>
						<li class="text-left"><a href="#"><span class="glyphicon glyphicon-user"></span> Private</a></li>
						<li class="text-left"><a href="#"><span class="glyphicon glyphicon-lock"></span> Protected</a></li>
					</ul>
				</div>
                <?php */ ?>
				<div class="margin-top-20"></div>
                <div class="row col-md-offset-2">
					<div class="col-xs-12 social-btns text-center">
						<ul class="social-icons" >
                        	<?php  if($this->common_front_model->checkfieldnotnull($user_data['facebook_url'])){ ?>
							<li data-toggle="tooltip" title="Your facebook profile link"><a class="facebook" target="_blank" href="<?php echo $user_data['facebook_url']; ?>"><i class="icon-facebook" style="width:10%;"></i></a></li>
                             <?php } ?>
                             <?php  if($this->common_front_model->checkfieldnotnull($user_data['twitter_url'])){ ?>
							<li data-toggle="tooltip" title="Your twitter profile link"><a class="twitter" target="_blank" href="<?php echo $user_data['twitter_url']; ?>"><i class="icon-twitter"></i></a></li>
                            <?php } ?>
                            <?php  if($this->common_front_model->checkfieldnotnull($user_data['gplus_url'])){ ?>
							<li data-toggle="tooltip" title="Your gplus profile link"><a class="gplus" target="_blank" href="<?php echo $user_data['gplus_url']; ?>"><i class="icon-gplus"></i></a></li>
                            <?php } ?>
                            <?php  if($this->common_front_model->checkfieldnotnull($user_data['linkedin_url'])){ ?>
							<li data-toggle="tooltip" title="Your linkdin profile link"><a class="linkedin" target="_blank" href="<?php echo $user_data['linkedin_url']; ?>"><i class="icon-linkedin"></i></a></li>
                            <?php } ?>
						</ul>	
					</div>
				</div>
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12">
				<ul class="list-group">
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your full name"><span class="glyphicon glyphicon-user"></span> <?php echo  $user_data['personal_titles'].' '.ucwords($user_data['fullname']); ?></li>
                    <?php  if($this->common_front_model->checkfieldnotnull($user_data['designation']) && $this->common_front_model->checkfieldnotnull($user_data['designation'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your job role"><span class="glyphicon glyphicon-briefcase"></span> <?php 
					if($this->common_front_model->checkfieldnotnull($designation_stored) && count($designation_stored) > 0)
					{
						echo ucwords(implode(',',$designation_stored));
					}
					else
					{
						echo $custom_lable_arr['notavilablevar'];
					} ?>
                    </li>
                    <?php } ?>
                    <?php  if($user_data['country']!='0' && $this->common_front_model->checkfieldnotnull($user_data['country']) && $this->common_front_model->checkfieldnotnull($user_data['country_name'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"  data-toggle="tooltip" title="Your country"><span class="glyphicon glyphicon-map-marker"></span> <?php echo  $user_data['country_name']; ?></li>
                    <?php } ?>
                    <?php  if($this->common_front_model->checkfieldnotnull($user_data['address'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your address"><span class="glyphicon glyphicon-home"></span> <?php echo  $user_data['address']; ?></li>
                    <?php } ?>
                    <?php  if($this->common_front_model->checkfieldnotnull($user_data['mobile'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your mobile" ><span class="glyphicon glyphicon-phone"></span><?php echo  $user_data['mobile']; ?></li>
                    <?php } ?>
                    <?php  if($this->common_front_model->checkfieldnotnull($user_data['email'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your email"><span class="glyphicon glyphicon-envelope"></span><!--<a href="#" title="mail" style="color: black;">--><?php echo  $user_data['email']; ?><!--</a>--></li>
                    <?php } ?>
				</ul>
			</div>
		</div>