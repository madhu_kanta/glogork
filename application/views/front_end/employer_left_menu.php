<?php $custom_lable_array = $this->lang->language;
$custom_lable_arr = $custom_lable->language;
?>
<div class="">
    <div class="panel-group" id="accordion">
       <!--<div class="panel panel-default" style="border-radius:0px;">
          <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo $custom_lable_array['emp_left_acc_set']; ?> <span class="glyphicon glyphicon-lock"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;"></i></a></h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" >
                        <li class="">
                            <a class="list-group-item" href="#change_password" data-toggle="tab"><i class="fa fa-key"></i> <?php echo $custom_lable_array['change_pass_emp']; ?></a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#view_profile_picture" data-toggle="tab"><i class="fa fa-key"></i> Profile Picture</a>
                         </li>
                    </ul>
                </div>
            </div>
          </div>
        </div>-->
       <?php
		if(isset($this->common_front_model->page_profile) && $this->common_front_model->page_profile ==1)
		{
		?>
       		<div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowemp('collapseOne')"><?php echo $custom_lable_arr['emp_edit_title']?> <span class="glyphicon glyphicon-edit"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowemp('collapseOne')"><i id='icon_collapseOne' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;" ></i></a></h4>
          </div>
          	<div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" role="tablist"><!--nav nav-pills--> <!--or  nav-tabs nav-menu tabs-left-->
                        <li class="active ">
                            <a class="list-group-item" href="#personal_deteditdivscroll_sec" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-user"></i> <?php echo $custom_lable_arr['myprofile_emp_personaltitle']?></a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#other_details_viewdiv_sec" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['sign_up_emp_2ndtabtitle']?></a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#education_view_viewdiv_sec" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-education"></i> <?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']?></a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#socaila_viewdiv_sec" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_social_med_tit']?></a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#workdetails_viewdiv_sec" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-plus"></i> <?php echo $custom_lable_arr['skill_hire_for_tit']?></a>
                        </li>
                        <li class="">
                            <a class="list-group-item" href="#change_password" role="tab" data-toggle="tab"><i class="fa fa-key"></i> &nbsp;&nbsp;<?php echo $custom_lable_arr['change_pass_emp']?></a>
                         </li>
                     	<li class="">
                        	<a class="list-group-item" href="#delete_profile" data-toggle="tab"><i class="fa fa-trash-o"></i> &nbsp;&nbsp;&nbsp;Delete Profile</a>
                     	</li>
                    </ul>
                </div>
            </div>
          </div>
       <?php
		}
		else
		{
		?>	
        	<div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowemp('collapseOne')"><?php echo $custom_lable_arr['emp_edit_title']?> <span class="glyphicon glyphicon-edit"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowemp('collapseOne')"><i id='icon_collapseOne' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;" ></i></a></h4>
          </div>
          	<div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" role="tablist"><!--nav nav-pills--> <!--or  nav-tabs nav-menu tabs-left-->
                       <li class="">
            		<a class="list-group-item" href="<?php echo $base_url; ?>employer-profile" ><i class="fa fa-check"></i> &nbsp;My profile </a>
                </li>
                    </ul>
                </div>
            </div>
          </div>
        <?php
		}
	   ?>
       <div class="panel panel-default" style="border-radius:0px;">
          <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowemp('collapseThree')"><?php echo $custom_lable_array['activity_list_lbl_for_emp']; ?>  <span class="glyphicon glyphicon-list"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowemp('collapseThree')"><i id='icon_collapseThree' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;"></i></a></h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" >
                       
                        <li class="<?php if(isset($page) && $page=='manage_job_application') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-application/manage-job-application"><i class="fa fa-check"></i> &nbsp;&nbsp;<?php echo $custom_lable_array['job_application_left_p_lbl']; ?> </a>
                        </li>
                        
                        <li class="<?php if(isset($page) && $page=='shortlisted_application') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-application/shortlisted-application"><i class="fa fa-check"></i> &nbsp;&nbsp;<?php echo $custom_lable_array['shortlist_application_left_p_lbl']; ?> </a>
                        </li>
                        
                        <li class="<?php if(isset($get_list) && $get_list=='follow_emp') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>employer-profile/my-listing/<?php echo base64_encode('1');?>/<?php echo base64_encode('follow_emp');?>"><i class="fa fa-check"></i> &nbsp;&nbsp;<?php echo $custom_lable_array['follow_emp_left_p_lbl']; ?> </a>
                        </li>
                        
                        <li class="<?php if(isset($get_list) && $get_list=='like_emp') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>employer-profile/my-listing/<?php echo base64_encode('1');?>/<?php echo base64_encode('like_emp');?>"><i class="fa fa-heart"></i> &nbsp;&nbsp;<?php echo $custom_lable_array['like_emp_left_p_lbl']; ?> </a>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
<script>
var change_arrow_process = 0 ;
var y = ['collapseOne','collapseTwo','collapseThree','collapsefour'];
function change_arrowemp(id)
{	
	if(change_arrow_process == 0)
	{
		setTimeout(function(){
		change_arrow_process = 1;		
		$.each( y, function( key, value )
		{
			var className = $('#icon_'+value).attr('class');
			var idName = $('#icon_'+value).attr('id');
			if ( $('#'+value).hasClass("collapse in"))
			{				
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-down pull-right');
			}
			else
			{
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-up pull-right');				
			}
		});
		change_arrow_process = 0;
		},400);
	}	
}
</script>