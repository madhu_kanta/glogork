<style>
/*.carousel-indicators .active {
border:1px solid grey;}*/
.carousel-indicators li {
border:1px solid grey !important;}
</style>
<?php 

$custom_lable_arr = $custom_lable->language;
$js_id = $this->common_front_model->get_userid();

if( (get_cookie('viewed_job_cookie') && get_cookie('viewed_job_cookie')!='') || $js_id!='')
{ 
	$view_job_cookie = get_cookie('viewed_job_cookie');
	$check_view_count = explode(',',$view_job_cookie);
	//print_r($view_job_cookie);exit;
	if($js_id!='' && count($check_view_count) < 11)
	{
	    $get_view_list_limit = 10 - count($check_view_count);
		$where_array = "js_id = '".$js_id."'"; 
	    $old_view_job_list = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_array,2,'','update_on desc','',$get_view_list_limit);
		
		if($old_view_job_list!='' && is_array($old_view_job_list) && count($old_view_job_list) > 0)
		{
			foreach($old_view_job_list as $old_view_job)
			{
				$old_view_job_list_arr[] = $old_view_job['job_id'];
			}
			$old_view_job_list = implode(',',$old_view_job_list_arr);
			if($view_job_cookie!='')
			{
			  $view_job_cookie .= ','.$old_view_job_list;
			}
			else
			{
				$view_job_cookie = $old_view_job_list ;
			}
		}
	}
	
	if(isset($view_job_cookie) && $view_job_cookie!='')
	{
	$where_arra = "id IN (".$view_job_cookie.") AND is_deleted='No' AND employer_delete='No' AND employer_status='APPROVED'";
		$recent_view_job_list = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arra,2,'','',1,16);

		if($recent_view_job_list!='' && is_array($recent_view_job_list) && count($recent_view_job_list) > 0)
	{?>
  <div class="container margin" >
	<div class="sixteen columns">
		<h3 class="margin-bottom-25"> <?php echo $custom_lable_arr['recent_job_view_lbl']; ?> </h3>
	</div>
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    
    <ol class="carousel-indicators" style="bottom:10px !important;">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" style="padding-bottom:20px;">
    	
        <?php 
		$view_count = 0;
		$recent_view_job_list_count = count($recent_view_job_list);
		foreach($recent_view_job_list as $recent_view)
		{    
		$view_count++;
		if($view_count == 1	)
		     {?>
               <div class="item active">
             <?php } ?>
                <div class="four columns" >
                    <div class="panel panel-default box-jobs" style="min-height:300px !important; max-height:300px !important;">
                        <div class="panel-heading text-center box-jobs"><a target="_blank" href="<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($recent_view['id']);?>"><span class="glyphicon glyphicon-list-alt"></span><?php echo $recent_view['job_title']; ?></a></div>
                            <div class="panel-body padding-top-0" style="min-height:100px;">
                                <h5> <i class="fa fa-info"></i> <?php echo substr(htmlspecialchars_decode($recent_view['job_description'],ENT_QUOTES),0,40) ?>...</h5>
                                <h6> <i class="fa fa-globe"></i> <a target="_blank" href="<?php echo $base_url; ?>recruiters/company/<?php echo base64_encode($recent_view['posted_by']);?>"><?php echo $this->common_front_model->checkfieldnotnull($recent_view['company_name']) ? $recent_view['company_name'] : 'N/A';  ?></a></h6>
                                <fieldset class="small">  <i class="fa fa-map-marker"></i>  <?php echo $this->common_front_model->checkfieldnotnull($recent_view['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($recent_view['location_hiring']) : 'N/A';  ?> <br />  <i class="fa fa-briefcase"></i>   <?php 
									if($recent_view['work_experience_from']!='' && $recent_view['work_experience_to']!='')
									{
										if($recent_view['work_experience_from']=='0' && $recent_view['work_experience_to']=='0')
										{
											echo "Fresher";
										}
										else
										{
											echo $recent_view['work_experience_from'] .' to '.$recent_view['work_experience_to']. '  Year';
										}
									}
									else
									{
										echo $custom_lable_arr['notavilablevar'];
									}
									?></fieldset>
                                <button type="button" onClick="window.open('<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($recent_view['id']);?>','_blank');"  class="btn btn-sm bnt-top-10 bg-btn-shadow-hover" value="View"><?php echo $custom_lable_arr['view_detail_lbl']; ?> </button>
                                <!--<h6 class="pull-right small-13" style="margin-top:15px;"><a href="#"><b> <?php echo $custom_lable_arr['more_job_view_lbl']; ?>  </b></a></h6>-->
                            </div>
                    </div>
                </div>
            
			<?php
			$recent_view_job_list_new = $recent_view_job_list_count - $view_count;
			
			if( ($view_count==4 || $view_count==8 ||  $view_count==12 ||  $view_count==16 ) || ($recent_view_job_list_new==0))
			{?>
            </div>
            <?php if($recent_view_job_list_new!='' && $recent_view_job_list_new!=0)
			{ ?>
            <div class="item ">
			<?php } }
			
		}
		?>
      
   
    <!--</div>-->

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only"><?php echo $custom_lable_arr['previous']; ?></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only"><?php echo $custom_lable_arr['next']; ?></span>
    </a>
  </div>
    
	
    	 
</div>
</div>
<?php } }
}


?>

