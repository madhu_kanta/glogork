<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="fa fa-trash-o"></span> Request For Deleting Your Profile</span></div>
								<div class="panel-body" style="padding:10px;">
                                <!-- info div-->
                                <div class="bs-example bs-example-standalone" data-example-id="dismissible-alert-js">
    							<div class="alert alert-info alert-dismissible fade in margin-top-20px" role="alert">
      								<button type="button" class="close margin-top-10px" data-dismiss="alert" aria-label="Close">
                                    	<span aria-hidden="true">×</span>
                                    </button>
      								<p class="">
                                    	Submit the below form with reason for deleting your profile.
                                    </p>
                                    
                   				 </div>
    							</div>
                                <!-- info div end-->
                                <form method="post" name="prodeleterequest" id="prodeleterequest" action="<?php echo $base_url.'my_profile/prodeleterequest'; ?>">
                                <div class="alert alert-danger" id="messagedelpro" style="display:none" ></div>
								<div class="alert alert-success" id="success_msgdelpro" style="display:none" ></div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<!--<!--<!---->
                                            <div class="margin-top-20"></div>
                                                <h5>Enter Your Reason For Deleting Your Profile : <span class="red-only"> *</span></h5>
                                                <div>
                                                	<textarea class="form-control border-radius-5px" placeholder="Enter Your Reason Here" rows="6" id="message" name="message" style=" height:300px;" data-validation="required"></textarea>
                                                </div>
                                                <div class="margin-top-20"></div>
                                                <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                                <div class="text-center">
													<button type="submit" class="btn-primary col-xs-12 col-md-4"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['delete_req_btn_txt']?></button>
												</div>
										</div>
									</div>
                                    
                                  </form>  
								</div>
							</div>