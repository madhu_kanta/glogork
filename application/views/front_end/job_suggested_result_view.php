<?php
$custom_lable_array = $custom_lable->language;
$text_home_search = '';
$text_search_lbl_val ='';
if($this->input->post('text_home_search') && $this->input->post('text_home_search')!='')
{
	$text_home_search = $this->input->post('text_home_search');
	$text_search_data = explode(',',$text_home_search);
	if(isset($text_search_data) && is_array($text_search_data) && count($text_search_data) > 0)
	{
		foreach($text_search_data as $search_data)
		{
			$get_string = explode('-',$search_data);
			$lable = $get_string[0];
			if(isset($get_string[1]) && $get_string[1]!='')
			{
				$lable = $get_string[1];
			}
			if(isset($get_string[0]) &&  trim($get_string[0]) == 'R' && is_numeric($lable))
			{   
				$lable = $this->common_front_model->valueFromId('role_master',$lable,'role_name');
			}
			$text_search_lbl[] = $lable;
			$text_search_value[] = $search_data;
			//$text_search_array_data[] = array('value'=>$search_data,'label'=>$lable);
		}
	}
	$text_search_lbl_val = implode(',',$text_search_lbl);
}	
$this->data['page'] = 'suggested_job';
?>

<!-- Titlebar ================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/staffing.png); background-size:cover;">
	<div class="container">
		<div class="ten columns">
			<h2><i class="fa fa-briefcase"></i>   <?php echo $custom_lable_array['suggested_job_title']; ?> </h2>
			<span><?php echo $custom_lable_array['total_job_count_msg']; ?> <span id="total_job_count"></span></span>
		</div>

<?php if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
				{ ?>
		<div class="six columns">
			<a href="<?php echo $base_url; ?>job-listing"  target="_blank" class="button"><?php echo $custom_lable_array['post_new_job_text']; ?></a>
		</div>
        <?php } ?>

	</div>
</div>

<!-- Titlebar end================================================== -->

<!-- Content ================================================== -->
<div class="container">
	<div class="sixteen columns">
		<div class="info-banner text-center" style="border:1px solid;padding:20px;background-color:#E9F7FE;">
			<div class="col-md-8 col-xs-12">
				<div class="info-content">
					<h3><?php echo $custom_lable_array['contact_top_recruiters_lbl']; ?></h3>
					<p><?php echo $custom_lable_array['contact_top_recruiters_di_lbl']; ?></p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 text-center" style="margin:0 auto;text-align:center;">
				<a href="<?php echo $base_url; ?>recruiters" target="_blank" class="button" ><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_array['top_recruiters_lbl']; ?></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>


<!--<div class="container" >-->
<div class="margin-top-20"></div>

<div class="clearfix">
</div>

	
<div id="job_search_part">
    <div class="col-md-3 col-sm-12 col-xs-12">
    <?php   $this->load->view('front_end/job_seeker_left_menu',$this->data); ?>
    </div>
</div>
	 
	
	<div class="col-md-9 col-sm-12 col-xs-12 margin-top-10" id="for_scrool_div"> 
	<div class="">
   
		<div id="js_action_msg_div"></div>
		<ul class="job-list">
            <div id="main_content_ajax">
               <?php  
				   $this->load->view('front_end/page_part/job_search_result_view',$this->data);
				 ?>
            </div>
        </ul>
       
      <div class="clearfix"></div>
       <!--<div class="pagination-container">
			<nav class="pagination">
				<ul>
					<li><a href="#" class="current-page">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li class="blank">...</li>
					<li><a href="#">22</a></li>
				</ul>
			</nav>

			<nav class="pagination-next-prev">
				<ul>
					<li><a href="#" class="prev">Previous</a></li>
					<li><a href="#" class="next">Next</a></li>
				</ul>
			</nav>
		</div>-->

	</div>
	</div>

<div class="clearfix">
</div>
	
<!--</div>-->

<!--<div class="container">

</div>
-->
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8">
</script>


<script>

function get_suggestion_list(list_id,return_val,get_list)
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'sign_up/get_suggestion_list/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
			return_val: return_val,
			get_list: get_list,
		  };
		},
	  }
	});
}	

<?php
if(($this->input->post('text_home_search') && $this->input->post('text_home_search')!='') || $this->input->post('location_home_search') && $this->input->post('location_home_search')!='')
{ ?>
  job_option_search();
<?php }
?>
/*function search_job_by_text()
{
	$('#search_from_id').val('text_search_form');
	$('.chosen-select').val('');
	$('.chosen-select').removeAttr('selected');
	$('.chosen-select').trigger('chosen:updated');			
	document.getElementById('job_search_option').reset();
	show_comm_mask();
	var data_string = $('#text_search_form').serialize();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = data_string+'&is_ajax=1'+'&csrf_job_portal='+hash_tocken_id;
	
	$.ajax({
		url : "<?php echo $base_url.'job-listing/employer-posted-job' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{  
			$('#main_content_ajax').html(data);
			$('#search_from_id').val('text_search_form');
			scroll_to_div('main_content_ajax');
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			load_pagination_code();
		}
	});	
	  hide_comm_mask();
	  return false;
}*/




$('#text_search').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>job_listing/get_search_suggestion/"+request.term, {
          }, function (data) {
            response(data);
		});
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#text_search').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});  

$(document).ready(function(e) {
	
	if($("#ajax_pagin_ul").length > 0)
	{   
		load_pagination_code();
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();  

	get_suggestion_list('search_skill','key_skill_name','skill_list');
	get_suggestion_list('search_location','id','city_list');
	get_suggestion_list('search_company','company_name','company_list');
	
	
});

function job_option_search()
{ 
	show_comm_mask();
	var data_string = $('#job_search_option').serialize();
	var text_search = $('#text_search').val();
	var hash_tocken_id = $("#hash_tocken_id").val();
	if(hash_tocken_id=='' || hash_tocken_id=='null' || hash_tocken_id=='undefined' || hash_tocken_id==undefined || hash_tocken_id==null)
	{
		var hash_tocken_id = '<?php echo $this->security->get_csrf_hash(); ?>';
	}
	var datastring = data_string+'&is_ajax=1'+'&csrf_job_portal='+hash_tocken_id+'&text_search='+text_search;
		
	$.ajax({	
		url : "<?php echo $base_url.'job-listing/posted-job' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#main_content_ajax').html(data);
			scroll_to_div('main_content_ajax');
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			load_pagination_code();
			scroll_to_div('main_content_ajax',-100);
			 hide_comm_mask();
		}
	});	
	 
	  return false;
}

/*$('#job_search_option').change(function(){
	$('#search_from_id').val('job_search_option');
	document.getElementById('text_search_form').reset();
	show_comm_mask();
	var data_string = $('#job_search_option').serialize();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = data_string+'&is_ajax=1'+'&csrf_job_portal='+hash_tocken_id;
	
	$.ajax({
		url : "<?php echo $base_url.'job-listing/employer-posted-job' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#main_content_ajax').html(data);
			$('#search_from_id').val('job_search_option');
			scroll_to_div('main_content_ajax');
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			load_pagination_code();
		}
	});	
	  hide_comm_mask();
	});*/

function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html('');  }, 8000);
}

function apply_for_job(job_id)
{
	 
	<?php 
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{ 
	    //$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
		//$this->session->set_userdata($return_after_login_url);
		
		$return_after_login_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$this->session->set_userdata($return_after_login_url);
		?>
		if (typeof(Storage) !== "undefined") {
   	 		localStorage.setItem("return_after_login_url",'<?php echo $return_after_login_url; ?>');
   			} 
		
				var message = '<?php echo $this->lang->line('please_login_apply'); ?>';
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
		   	    return false;
	<?php }
	?>
	
	var c = confirm("<?php echo $custom_lable_array['confirm_apply_job']; ?>");
	if(c==true)
	{
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&job_id='+job_id+'&user_agent=NI-WEB';
		show_comm_mask();
		
		$.ajax({
		url : "<?php echo $base_url.'job_seeker_action/apply_for_job' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				$('.apply_for_job'+job_id).removeAttr('onClick');
				$('.apply_for_job'+job_id).css('cursor','default');
				$('.apply_for_job'+job_id).html('<i class="fa fa-check"></i>  <?php echo $this->lang->line('already_apply'); ?>');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			 hide_comm_mask();
			
		}
	     
		});
		//hide_comm_mask();
	     return false;
	
	}
	 
}

function jobseeker_action(action,action_for,action_id)
{
	<?php 
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{ 
	    $return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
		$this->session->set_userdata($return_after_login_url);
		?>
		
				var message = '<?php echo $this->lang->line('please_login'); ?>';
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
		   	    return false;	
	<?php }
	?>
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&action='+action+'&action_for='+action_for+'&action_id='+action_id+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'job-seeker-action/seeker-action' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
			
			
			if(data.status=='success')
			{   
				
				
				if(action=='save_job')
				{
					$('.save_job_button'+action_id).html('<i class="fa fa-check"></i>  <?php echo $this->lang->line('saved'); ?>');
					$('.save_job_button'+action_id).removeAttr('onClick');
					$('.save_job_button'+action_id).attr('onClick','jobseeker_action("remove_save_job","job",'+action_id+')')
				}
				if(action=='remove_save_job')
				{
					$('.save_job_button'+action_id).html('<i class="fa fa-save"></i>  <?php echo $this->lang->line('save'); ?>');
					$('.save_job_button'+action_id).removeAttr('onClick');
					$('.save_job_button'+action_id).attr('onClick','jobseeker_action("save_job","job",'+action_id+')')
				}
				if(action=='block_emp')
				{
					$('.emp_block_action'+action_id).html('<span class="fa fa-check"></span>  <?php echo $this->lang->line('blocked'); ?>');
					$('.emp_block_action'+action_id).removeAttr('onClick');
					$('.emp_block_action'+action_id).attr('onClick','jobseeker_action("unblock_emp","Emp",'+action_id+')')
				}
				if(action=='unblock_emp')
				{
					$('.emp_block_action'+action_id).html('<i class="fa fa-lg fa-ban" aria-hidden="true"></i> <?php echo $this->lang->line('block'); ?>');
					$('.emp_block_action'+action_id).removeAttr('onClick');
					$('.emp_block_action'+action_id).attr('onClick','jobseeker_action("block_emp","Emp",'+action_id+')')
				}
				
				
					$('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_msg_div').slideDown('Slow');
				   set_time_out_msg('js_action_msg_div');
				   scroll_to_div('js_action_msg_div',-100);
				}
			else
			{
				
					$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_msg_div').slideDown('Slow');
					set_time_out_msg('js_action_msg_div');
					scroll_to_div('js_action_msg_div',-100);
				
			}
			hide_comm_mask();
		}
	});	
	  
	  return false;
}
	
function add_to_highlight(job_id)
{
	
	var c = confirm("<?php echo $custom_lable_array['confirm_hightlight_job']; ?>");
	if(c==true)
	{
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&id='+job_id+'&user_agent=NI-WEB&mode=edit';
		show_comm_mask();
		
		$.ajax({
		url : "<?php echo $base_url.'job-listing/add-to-highlight' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				$('.add_to_highlight_class'+job_id).removeAttr('onClick');
				//$('.add_to_highlight_class'+job_id).removeAttr('class');
				$('.add_to_highlight_class'+job_id).html('<i class="fa fa-check" aria-hidden="true"></i> <?php echo $this->lang->line('job_highlighted'); ?>'); 
				$('.add_to_highlight_class'+job_id).css('height','30px');
				$('.add_to_highlight_class'+job_id).attr('class','label label-success');
				     
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-100);
			}
			hide_comm_mask();
		}
	     
		});
		
	     return false;
	
	}
	 
}
</script>
