<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/skill.css">
<style>
.th_bgcolor {
background-color: transparent !important;
}
	#defineheight{ max-height:600px;overflow-y:auto;overflow-x:hidden; }
	#defineheightlng{ max-height:600px;overflow-y:auto;overflow-x:hidden; }
	#defineheightwork{ max-height:600px;overflow-y:auto;overflow-x:hidden; }
	.fileinp
	{
		/*position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		cursor: pointer;*/
		position: absolute;
		cursor: pointer;
		opacity: 0;
	}
	input.forminputpadding
	{
		padding:13px 0 12px 10px;
	}
	@media only screen and (max-width: 768px) {
		.marginclass
		{
			margin-bottom:5px;
		}
	}
	.foreditfontcolor
	{
		color:#ffffff;
	}
	.carousel-control
	{
		color:#000000 !important;	
		width:5% !important;
	}
	.select2-search__field
	{
	   width:100% !important;
	}
	.panel.panel-default {
    box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.12);
}
.th_bgcolor {
    background-color: transparent;
}
</style>
<?php $custom_lable_arr = $custom_lable->language;
$pass['custom_lable'] = $custom_lable_arr;
$pass['education_count'] = $education_count;

$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','','1'); 
$key_skill_data = json_encode($key_skill); 
$shift_list = $this->common_front_model->get_list('shift_list','','','array','','1'); 
$shift_list_data = json_encode($shift_list);

$employement_type = $this->common_front_model->get_list('employement_list','','','array','','1'); 
$employement_type_data = json_encode($employement_type);

$job_type_master = $this->common_front_model->get_list('job_type_list','','','array','','1'); 
$job_type_master_data = json_encode($job_type_master);  

$languages_frm = $this->common_front_model->get_list('skill_language_master','','','array','','1'); 
$languages_frm_table = json_encode($languages_frm);

?>
<?php  $upload_path_profile ='./assets/js_photos';
$upload_path_demomenu ='./assets/front_end/images/demoprofileimge2.png';
$upload_path_demoheader ='./assets/front_end/images/demoprofileimge.png'; ?>
<!-- Titlebar
================================================== -->
<input type="hidden" name="profileeditflag" id="profileeditflag">
<section>
<div id="container-fluid">
		<?php $this->load->view('front_end/profile_head',$pass['custom_lable']); ?>
	</div>
   
	<div id="sticky" class="container-fluid">
		<ul class="nav nav-tabs nav-menu tabing-menu-new" role="tablist">
			<li class="active">
			  <a href="#profile" role="tab" data-toggle="tab" class="text-center">
				  <span class="glyphicon glyphicon-user"></span> Profile
			  </a>
			</li>
			<li><a href="#change" role="tab" data-toggle="tab" class="text-center">
			  <span class="glyphicon glyphicon-edit"></span> Edit Profile
			  </a>
			</li>
			<!--<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
			<div class="margin-top-20"></div>	
				<div class="panel panel-primary box-shadow1"  style="border:none;border-radius:0px;border:0px solid #D9534F;padding:5px;">
					<div class="panel-heading th_bordercolor" style="background:none;color:#000000;padding:0px;border:0px;border-bottom:1px solid;"><span style="padding:8px;"> Complete Your Profile - Add Details</span>
						<button type="button" class="close" data-target="#copyright-wrap" data-dismiss="alert" style="font-size:30px;"> <span aria-hidden="true">&times;</span><span class="sr-only">X</span></button>
					</div>
								
					<div class="panel-body" style="padding:10px;">
						<div class="form-group">
							<label class="col-md-2 text-center" for="pname">Project Name:</label>
							<div class="col-md-6">
								<input type="text" placeholder="Abc@xyz.com" value="" style="padding:0px 0 9px 10px;"/>
							</div>
							<div class="margin-top-10"></div>
								<div class="text-center">
									<button id="button1idFFF" name="button1idFFF" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
									<button id="button2id" name="button2id" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
								</div>
						</div>
					</div>
				</div>
			</div>-->
		</ul>
		<div class="tab-content">
        <!--profile view tab-->
        <div class="tab-pane fade active in " id="profile" style="padding:0px;">
			<?php $this->load->view('front_end/profile_view',$pass['custom_lable']); ?>
        </div>    
		<!--profile view tab end -->
        <!--profile edit tab-->
        <div class="tab-pane fade " id="change" style="padding:0px;">
            <?php $this->load->view('front_end/profile_edit_new',$pass['custom_lable']); ?>
        </div>
		<!--profile edit tab end-->
		</div>
	</div>
</section>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<link href="<?php echo $base_url;?>assets/front_end/css/bootstrap-datepicker_for_conf.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/new-home.css"  rel="stylesheet">
<!--<script src="<?php echo $base_url; ?>assets/front_end/js/common.js"></script>-->
<!-- WYSIWYG Editor -->
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery.sceditor.bbcode.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery.sceditor.js"></script>-->
<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-datepicker_for_conf.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
$(function(){
	
/*var dateset = '<?php echo ($user_data['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($user_data['birthdate'])) ? $user_data['birthdate'] : ""; ?>';
if(dateset!=''){ var setdate = dateset; }else{ var setdate = '1990-01-01'; }*/
/*if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
   var datepicker = $.fn.datepicker.noConflict();
   $.fn.bootstrapDP = datepicker;
   $('#birthdate').bootstrapDP({
	  format: 'yyyy-mm-dd',
	  endDate: '+0d',
      autoclose: true
	  
	});
	$('.datepicker-join').bootstrapDP({
	  format: 'yyyy-mm-dd',
	  endDate: '+0d',
      autoclose: true
	});
	$('.datepicker-leave').bootstrapDP({
	  format: 'yyyy-mm-dd',
	  endDate: '+0d',
      autoclose: true
	  
	});
}*/
$('.datepicker-join,.datepicker-leave,#birthdate').datepicker({
  format: 'yyyy-mm-dd',
  endDate: '+0d',
   autoclose: true,
  });
/*$(".datepicker-join").on("dp.change", function(e) {
    alert('hey');
});*/
$('.datepicker-join,.datepicker-leave').datepicker().on('changeDate', function (ev) {
   //alert('hey');
});
  
});

  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
  
	jQuery(document).ready(function(){
		jQuery('.skillbar').each(function(){
			jQuery(this).find('.skillbar-bar').animate({
				width:jQuery(this).attr('data-percent')
			},6000);
		});
	});
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.1.3/circle-progress.min.js'></script>
<script>
	


/****Accordian Menu***/
$(document).ready(function()
{
	var count = $(".per").first().text().replace('%', '');
	
	count = count/100;
	var progressBarOptions = {
	startAngle: -1.55,
	size: 200,
    value: count,
    fill: {
		color: '#FF0000'
	}
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
	
});

$('#circle-b').circleProgress({
	value : count,
	fill: {
		color: '#FF0000'
	}
});
	get_suggestion_list('preferred_city','id','city_list');
	
	selectinglangajax();

	validateformpersonal();
	validateformpassword();
	validateformdeleteprofile();
	validateformsociallink();
	validateformother();
	validateformedu();
	validateformwork();
	validateformlang();
	foraddingmorebuttonfnlang();
	foraddingmorebuttonfnedu();
	foraddingmorebuttonfncerti();
	foraddingmorebuttonfnwork();
	removepart();
	//deletepart();
	deleteresume();
	resumeuploadfn();
	changelanguage();
    //Add Inactive Class To All Accordion Headers
	$('.accordion-header').toggleClass('inactive-header');
	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});
	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');
	// The Accordion Effect
	
	
	
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content').promise().done(function(){
				$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');		    	
			});
		}
		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content').promise().done(function(){
    			$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');
			});
		}
	});
	return false;
});
function editsection(section)
{
	$(".chosen-select").chosen();
	var scrolltodivid = "#"+section+'editdivscroll';
	var openacc = "#"+section+'editdiv';
	$this = $(openacc);
	$this1 = $(scrolltodivid);
	$( "a[href='#profile']" ).attr( "aria-expanded", false ).closest("li").removeClass("active");
	$( "a[href='#change']" ).attr( "aria-expanded", true ).closest("li").addClass("active");
	$( "#change" ).addClass( "active in" );
	$( "#profile" ).removeClass( "active in" );
	if($.trim(section)=='')
	{
		var section = 'profile_snapshot';
	}
	else
	{
		var section = section;
	}
	setsuggwidth();
	$('html,body').animate({
				scrollTop: $this1.offset().top - 100
			}
		,'slow');
	setTimeout(function() { 
	if($this.is('.inactive-header')) 
	{
		$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
		$this.toggleClass('active-header').toggleClass('inactive-header');
		//$this.next().slideToggle().toggleClass('open-content');
		$this.next().slideToggle().toggleClass('open-content').promise().done(function(){
    			$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');
			});
	} }, 200);
	 
	
	return false;
}
function viewsection(section)
{
	var scrolltodivid = "#"+section+'_viewdiv';
	/*alert(section);
	alert(scrolltodivid);*/
	var openacc = "#"+section+'editdiv';
	var tabactdivid = "#"+section+'_viewdiv';
	$this = $(openacc);
	$this1 = $(scrolltodivid);
	$( "a[href='#change']" ).attr( "aria-expanded", false ).closest("li").removeClass("active");
	$( "a[href='#profile']" ).attr( "aria-expanded", true ).closest("li").addClass("active");
	$( "#profile" ).addClass( "active in" );
	$( "#change" ).removeClass( "active in" );
	if($.trim(section)=='' || $.trim(section)=='undefined' || $.trim(section)==undefined || $.trim(section)=='null' || $.trim(section)==null)
	{
		var section = 'profile_snapshot';
	}
	else
	{
		var section = section;
	}
	$('html,body').animate({
				scrollTop: $this1.offset().top - 100
			}
		,'slow');
	return false;
}
function deleteresume()
{
	$('#deletediv').click(function () {
		var r = confirm("Are you sure.You want to delete your resume from your profile!");
			if (r == true) 
			{	
				var deletepart = $(this).attr("data-deletepart");
				var deleteid = $(this).attr("data-deleteid");
				var deleteid_count = '1';
				forgetting_part(deletepart,deleteid,deleteid_count);
				resumeuploadfn();
			} 
		});	
}
function resumeuploadfn()
{
	$("#uploadimage").on('change',function(){
			show_comm_mask();
			var htmlobject=$( "#uploadimage" ).get( 0 );
			var filename = $("#uploadimage").val();
			var form_data = new FormData();
			var hash_tocken_id = $("#hash_tocken_id").val();
			form_data.append('resumeimg', $('input[name=uploadimage]')[0].files[0]);
			form_data.append('user_agent', 'NI-WEB');
			form_data.append('csrf_job_portal', hash_tocken_id);
			var hiddendwn = $('#downloadresumediv').length;
			var hiddendel = $('#deletediv').length;
				if(filename!='')
				{
					$.ajax({
						url: "<?php echo $base_url.'my_profile/uploadresume' ?>", // Url to which the request is send
						type: "POST",             // Type of request to be send, called as method
						data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						dataType:"json",
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,        // To send DOMDocument or non processed data file it is set to false
						success: function(data)   // A function to be called if request succeeds
						{
							if(data.final_result.status=='success')	
							{	
							
								$("#uploadimage").val('');
								if(hiddendwn)
								{
									//$("#downloadresumediv").slideDown();
								}
								if(hiddendel)
								{
									//$("#deletediv").slideDown();
								}
								$("#resume_dwn_viewdiv").html(data.contentreload.content);
								$("#deletediv").attr("hello","hello");
								$("#downloadresumediv").attr("hello","hello");
								$("#success_message").addClass("alert alert-success alert-dismissable fade in");
								$("#success_message").html('<div class="col-xs-11">'+'You resume file has been submitted successfully'+'</div><div class="col-xs-1"><a href="javascript:void(0)" class="close" onclick="hideid()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
								$("#success_message").show().css("opacity","1");
								setTimeout(function() { $("#success_message").hide(); }, 10000);
								$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
								$("#noreusme").remove();
								
							}					
							if(data.final_result.status=='error')
							{
								
								hide_comm_mask();
								$("#uploadimage").val('');
								$("#success_message").addClass("alert alert-danger alert-dismissable fade in");
								$("#success_message").html('<div class="col-xs-11">'+data.final_result.error_message+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
								$("#success_message").show().css("opacity","1");
								setTimeout(function() { $("#success_message").hide(); }, 10000);
							}
							$("#hash_tocken_id").val(data.tocken);
							resumeuploadfn();
							deleteresume();
							hide_comm_mask();
						}
						});
				}
			});	
}
function hideid()
{
	$("#success_message").hide().removeClass("alert alert-danger alert-dismissable fade in");
}
function hidetheclass(class_name)
{
	$("."+class_name).parent().hide();
}

function hideid1()
{
	$("#image_val").hide().removeClass("alert alert-danger alert-dismissable fade in");
}
function displayprofile()
{
	var myimage = $("#profile_img")[0];
	if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
	{
		document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
		$("#blah").slideDown();
		document.getElementById('blah2').src = window.URL.createObjectURL(myimage.files[0]);
		$("#blah2").slideDown();
	}
	else
	{
		//$("#blah").hide();
		//document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
		$("#profile_img").val('');
		showmessage('<?php echo $custom_lable_arr['invalid_file_format']; ?>','10000');
	}
}

function uploadprofilepic()
{
	if($("#profile_img").val()=='')
	{
		showmessage('Please select an image to uplaod as profile picture','10000');
	}
	else
	{
		senduploadedprofile();
	}
	return false;
}
function deleteprofilepic()
{
	var confirm_resp = confirm("Are you sure you want delete your profile picture.");
	if(confirm_resp)
	{
		deleteuploadedprofile();
	}
	return false;
}
function showmessage(message,fortime)
{
		$("#image_val").show().css("opacity","1");
		$("#image_val").addClass("alert alert-danger alert-dismissable fade in");
		$("#image_val").html('<div class="col-xs-9">'+message+'</div><div class="col-xs-3"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');
		setTimeout(function() { $("#image_val").hide(); }, fortime);
}
function senduploadedprofile()
{
			//event.preventDefault();
			show_comm_mask();
			var htmlobject=$( "#profile_img" ).get( 0 );
			var filename = $("#profile_img").val();
			var form_data = new FormData();
			var hash_tocken_id = $("#hash_tocken_id").val();
			form_data.append('profile_pic', $('input[name=profile_img]')[0].files[0]);
			form_data.append('user_agent', 'NI-WEB');
			form_data.append('csrf_job_portal', hash_tocken_id);
			if(filename!='')
			{
					$.ajax({
						url: "<?php echo $base_url.'my_profile/uploadprofile' ?>", // Url to which the request is send
						type: "POST",             // Type of request to be send, called as method
						data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						dataType:"json",
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,        // To send DOMDocument or non processed data file it is set to false
						success: function(data)   // A function to be called if request succeeds
						{
							if(data.final_result.status=='success')	
							{	
								$("#profile_img").val('');
								//$("#blah").hide();
								$("#image_val").removeAttr("class").addClass("alert alert-success alert-dismissable fade in");
								$("#image_val").html('<div class="col-xs-10">'+'You profile image has been submitted successfully'+'</div><div class="col-xs-2"><a href="javascript:void(0)" class="close" onclick="hideid1()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
								$("#image_val").show().css("opacity","1");
								setTimeout(function() { $("#image_val").hide(); }, 10000);
								$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
								$(".headermenuprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								$(".headerprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								$("#headmenuprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								
								
							}					
							if(data.final_result.status=='error')
							{
								hide_comm_mask();
								$("#profile_img").val('');
								$("#image_val").addClass("alert alert-danger alert-dismissable fade in");
								$("#imagediv").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
								$("#image_val").html('<div class="col-xs-10">'+data.final_result.error_message+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
								$("#image_val").show().css("opacity","1");
								setTimeout(function() { $("#image_val").hide(); }, 10000);
							}
							$("#hash_tocken_id").val(data.tocken);
							hide_comm_mask();
						}
						});
				}
}
function deleteuploadedprofile()
{
			show_comm_mask();
			var hash_tocken_id = $("#hash_tocken_id").val();
			var datastring = 'user_agent=NI-WEB&csrf_job_portal='+hash_tocken_id;
			$.ajax({
				url: "<?php echo $base_url.'my_profile/deleteuploadprofile'; ?>", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: datastring, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				dataType:"json",
				//contentType: false,       // The content type used when sending data to the server.
				//cache: false,             // To unable request pages to be cached
				//processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.status=='success')	
					{
						//$("#blah").hide();
						$("#image_val").removeAttr("class").addClass("alert alert-success alert-dismissable fade in");
						$("#image_val").html('<div class="col-xs-10">'+data.errormessage+'</div><div class="col-xs-2"><a href="javascript:void(0)" class="close" onclick="hideid1()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
						$("#image_val").show().css("opacity","1");
						setTimeout(function() { $("#image_val").hide(); }, 10000);
						$("#deleteprofile_btn").remove();
						$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
						$(".headermenuprofilediv").attr('src','<?php echo $upload_path_demomenu; ?>');
						$(".headerprofilediv").attr('src','<?php echo $upload_path_demoheader; ?>');
						$("#headmenuprofilediv").attr('src','<?php echo $upload_path_demoheader; ?>');
						
					}					
					if(data.status=='error')
					{
						hide_comm_mask();
						$("#imagediv").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
						$("#image_val").removeAttr("class").attr('class', 'alert alert-danger alert-dismissable fade in');
						$("#image_val").html('<div class="col-xs-11">'+data.errormessage+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
						$("#image_val").show().css("opacity","1");
						setTimeout(function() { $("#image_val").hide(); }, 10000);
					}
					$("#hash_tocken_id").val(data.tocken);
					hide_comm_mask();
				}
				});
return false;
}
//$('#home_city').tokenfield();


/*$('#home_city').tokenfield({
			autocomplete: {
			  source: function (request, response) {
				  jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
				  }, function (data) {
					 response(data);
				  });
			  }, 
			},
			 showAutocompleteOnFocus: false,
			 limit:1,
		});
$('#home_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
});*/
/*$('#home_city').on('tokenfield:edittoken', function (event) {
	var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
		 alert(existingTokens);
	 }
});*/
/*$('#home_city').on('tokenfield:initialize', function (event) {
	var existingTokens = $(this).tokenfield('getTokens');
	$.each(existingTokens, function(index, token) {
		if (token.value === event.attrs.value)
			event.preventDefault();
			alert(existingTokens);
	});
});*/
$('#prefered_shift').tokenfield({
    autocomplete: {
    source: <?php echo $shift_list_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

 })
$('#prefered_shift').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

/*$('#preferred_city').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#preferred_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

*/

function get_suggestion_list_2(list_id)
{
	$('#'+list_id).select2();
}
function get_suggestion_list(list_id,return_val,get_list)
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'sign_up/get_suggestion_list/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
			return_val: return_val,
			get_list: get_list,
		  };
		},
	  }
	});
}	
$('#key_skill').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_list/skill_list/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#key_skill').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

/*$('#key_skill').tokenfield({
    autocomplete: {
    source: <?php //echo $key_skill_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#key_skill').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});*/
/*language known tokenfield*/
function selectinglangajax()
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('.lang_known').select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'my_profile/get_suggestion_lang_select2/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
		  };
		},
	  }
	});
}	
function changelanguage()
{
	$(".lang_known").on("change", function (e) {
		$( "#forshowingnomore" ).remove();
		 $this = $(this);
	var languages_stored = new Array();	
	var forstoringallthelangnos_mes = new Array();
	$(".lang_known").each(function() {
	if($this.data("langknwon")!=$(this).data("langknwon"))
	{
		languages_stored.push($.trim($(this).val()).toLowerCase());
	}	
		forstoringallthelangnos_mes.push($(this).data("langknwon"));
	});
	
	if(languages_stored.length > 0)
	{
		if($.inArray($.trim($this.val()).toLowerCase(), languages_stored) > -1)
		{
			var maximumnooflang_mes = Math.max.apply(Math,forstoringallthelangnos_mes);
			maximumnooflang_mes_inc =maximumnooflang_mes +1; 
			$this.val('').trigger('change');
			$( ".lang_known_whole[data-lang_num='"+maximumnooflang_mes+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_already_added']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();
			return false;
		}
	}
	});//on change end
}
/*$('.lang_known').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>my_profile/get_suggestion_lang/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
    },
    showAutocompleteOnFocus: true,
	limit: 1,
});
$('.lang_known').on('tokenfield:createtoken', function (event) {
	
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
}).on('tokenfield:createdtoken', function (e) {
	$("#forshowingnomore").remove();
	$this = $(this);
    var valueselected = e.attrs.value;
	$(".lang_known").each(function() {
		
		var existingTokenschk = $(this).tokenfield('getTokens');
		if(existingTokenschk.length > 0)
		{
			var existingTokenschkvalue = existingTokenschk[0].value;
			if($.trim(existingTokenschkvalue).toLowerCase()==$.trim(valueselected).toLowerCase() && $this.data("langknwon")!=$(this).data("langknwon"))
			{
				$this.tokenfield('setTokens', []);
				var lastidstored = $this.data("langknwon");
				$( ".lang_known_whole[data-lang_num='"+lastidstored+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_already_added']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
			}
		}
		
	});
  });*/
  /*language known tokenfield end*/
/*$('.lang_known').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$('.lang_known').tokenfield({
    autocomplete: {
    source: <?php //echo $languages_frm_table; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

});*/
/*$('.lang_known').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
});*/

$('#desire_job_type').tokenfield({
    autocomplete: {
    source: <?php echo $job_type_master_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#desire_job_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$('#employment_type').tokenfield({
    autocomplete: {
    source: <?php echo $employement_type_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#employment_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
var timer;
function change_visibility(profile_visibility)
{
	$("#messageprivacy").hide();
	$("#success_msgprivacy").hide();
	clearTimeout(timer);
	if(profile_visibility=='private' || profile_visibility=='public')
	{	
		
		show_comm_mask();
	  	var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = "profile_visibility="+profile_visibility + "&csrf_job_portal="+hash_tocken_id + "&user_agent=NI-WEB";
		$.ajax({
		url : "<?php echo $base_url.'my_profile/profile_visibility_change' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messageprivacy").hide();
				if(profile_visibility=='private')
				{
					$("#messageprivacy").slideDown("slow", function() { scroll_to_div('messageprivacy',-100); });
					timer = setTimeout(function(){
					   $('#messageprivacy').hide(100);
					   //$('#messageprivacy').delay(5000).fadeOut('slow'); 
					},4000);
					$("#messageprivacy").html(data.successmessage+'<a href="javaScript:void(0);" class="close closemyalert " data-hide="alert" onclick="hidetheclass(&apos;closemyalert&apos;);" aria-label="close">&times;</a>');
				}
				else if(profile_visibility=='public')
				{
					$("#success_msgprivacy").slideDown("slow", function() { scroll_to_div('success_msgprivacy',-100); });
					clearTimeout(timer);
					timer = setTimeout(function(){
				   //$mytext.hide(500)
				   $('#success_msgprivacy').hide(100);
					   //$('#success_msgprivacy').delay(5000).fadeOut('slow');
					},4000);	
					$("#success_msgprivacy").html(data.successmessage+'<a href="javaScript:void(0);" class="close closemyalert " data-hide="alert" onclick="hidetheclass(&apos;closemyalert&apos;);" style="color:grey;" aria-label="close">&times;</a>');
				}
				
				if(profile_visibility=='private')
				{
					$("#private_sing").removeClass('glyphicon-ok').removeClass('glyphicon-remove').addClass('glyphicon-ok');
					$("#public_sing	").removeClass('glyphicon-ok').removeClass('glyphicon-remove').addClass('glyphicon-ok').addClass('glyphicon-remove');
				}
				if(profile_visibility=='public')
				{
					$("#public_sing	").removeClass('glyphicon-ok').removeClass('glyphicon-remove').addClass('glyphicon-ok');
					$("#private_sing").removeClass('glyphicon-ok').removeClass('glyphicon-remove').addClass('glyphicon-remove');
				}
				
			}
			else
			{
				$("#success_msgprivacy").hide();
				$("#messageprivacy").html(data.errormessage+'<a href="javaScript:void(0);" class="close closemyalert" data-hide="alert" onclick="hidetheclass(&apos;closemyalert&apos;);" aria-label="close">&times;</a>');
				$("#messageprivacy").slideDown("slow", function() { scroll_to_div('messageprivacy',-100);});
				clearTimeout(timer);
				timer = setTimeout(function(){
				   $('#messageprivacy').hide(100);
				   //$('#messageprivacy').delay(5000).fadeOut('slow'); 
				},5000);
			}
			hide_comm_mask();
		}
	});
	}
	else
	{
		hide_comm_mask();
	}
	
}
function validateformpassword()
{
	$.validate({
    form : '#editpassword',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgpassword").hide();
      $("#messagepassword").slideDown();
	  //$("#messagepassword").focus();
	  $("#messagepassword").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('messagepassword',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messagepassword").hide();
	  var datastring = $("#editpassword").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editpassword' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messagepassword").slideUp();
				$("#success_msgpassword").slideDown("slow", function() { scroll_to_div('success_msgpassword',-100); });
				$("#success_msgpassword").html(data.successmessage);
			}
			else
			{
				$("#success_msgpassword").slideUp();
				$("#messagepassword").html(data.errormessage);
				$("#messagepassword").slideDown("slow", function() { scroll_to_div('messagepassword',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformdeleteprofile()
{
	$.validate({
    form : '#prodeleterequest',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgdelpro").hide();
      $("#messagedelpro").slideDown();
	  $("#messagedelpro").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('messagedelpro',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messagedelpro").hide();
	  var datastring = $("#prodeleterequest").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/prodeleterequest' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messagedelpro").slideUp();
				$("#success_msgdelpro").slideDown("slow", function() { scroll_to_div('success_msgdelpro',-100); });
				$("#success_msgdelpro").html(data.successmessage);
				$('#prodeleterequest')[0].reset();
			}
			else
			{
				$("#success_msgdelpro").slideUp();
				$("#messagedelpro").html(data.errormessage);
				$("#messagedelpro").slideDown("slow", function() { scroll_to_div('messagedelpro',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformsociallink()
{
	$.validate({
    form : '#editsociallinks',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_social").hide();
      $("#message_social").slideDown();
	  $("#message_social").focus();
	  $("#message_social").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  //scroll_to_div('message_social',-100);
	  scroll_to_div_new('social_linkseditdivscroll');
	  
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_social").hide();
	  var datastring = $("#editsociallinks").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editsociallinks'; ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_social").slideUp();
				$("#success_msg_social").slideDown("slow", function()
				{ 
				  //scroll_to_div('success_msg_social',-100); 
				   scroll_to_div_new('social_linkseditdivscroll');
				});
				$("#success_msg_social").html(data.successmessage);
				   timer = setTimeout(function(){
				   $('#success_msg_social').hide(100);
					   $('#success_msg_social').delay(5000).fadeOut('slow');
					},4000);
						
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				
			}
			else
			{
				$("#success_msg_social").slideUp();
				$("#message_social").html(data.errormessage);
				$("#message_social").slideDown("slow", function() 
				{ 
					//scroll_to_div('message_social',-100); 
					scroll_to_div_new('social_linkseditdivscroll'); 
				});
				//clearTimeout(timer);
			    timer = setTimeout(function(){
			    $('#message_social').hide(100);
				   $('#message_social').delay(5000).fadeOut('slow');
				},4000);
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformpersonal()
{
	$.validate({
    form : '#editpersonaldet',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg2").hide();
      $("#message2").slideDown();
	  /*$("#message2").focus();*/
	  $("#message2").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	 // scroll_to_div('message2',-100);
		scroll_to_div_new('profile_snapshoteditdivscroll');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message2").hide();
	  var datastring = $("#editpersonaldet").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editpersonaldet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
		    //alert(data.status);
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message2").slideUp();
				$("#success_msg2").slideDown("slow", function() 
				{ 
					//scroll_to_div('success_msg2',-100); 
					scroll_to_div_new('profile_snapshoteditdiv');
				});
					
				$("#success_msg2").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				resumeuploadfn();
			}
			else
			{
				$("#success_msg2").slideUp();
				$("#message2").html(data.errormessage);
				$("#message2").slideDown("slow", function() {
					//scroll_to_div('message2',-100); 
					scroll_to_div_new('profile_snapshoteditdivscroll');
				});
				
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformother()
{
	$.validate({
    form : '#editotherdet',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgother").hide();
      $("#messageother").slideDown();
	  $("#messageother").focus();
	  $("#messageother").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	   scroll_to_div_new('other_detailseditdivscroll');
	 
	  
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messageother").hide();
	  var datastring = $("#editotherdet").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editotherdet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messageother").slideUp();
				$("#success_msgother").slideDown("slow", function() 
				{ 
					//scroll_to_div('success_msgother',-100); 
					 scroll_to_div_new('other_detailseditdivscroll');
				});
				$("#success_msgother").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				resumeuploadfn();
			}
			else
			{
				$("#success_msgother").slideUp();
				$("#messageother").html(data.errormessage);
				$("#messageother").slideDown("slow", function()
				{ 
					//scroll_to_div('messageother',-100); 
					 scroll_to_div_new('other_detailseditdivscroll');
				});
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformlang()
{
	$.validate({
    form : '#editlangdet',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_lang").hide();
      $("#message_lang").slideDown();
	  $("#message_lang").focus();
	  $("#message_lang").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	   scroll_to_div_new('language_vieweditdivscroll');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_lang").hide();
	  var datastring = $("#editlangdet").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editlangdet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#langwholeid").html(data.contentreload.content);
				$("#message_lang").slideUp();
				$("#success_msg_lang").slideDown("slow", function() 
				{ 
					//scroll_to_div('success_msg_lang',-100);
					scroll_to_div_new('language_vieweditdivscroll');
				 });
				$("#success_msg_lang").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				resumeuploadfn();
				foraddingmorebuttonfnlang();
				removepart();
				//deletepart();
				validateformlang();
				//$(data.contentreload.content).filter( '.chosen-select' ).chosen('destroy').chosen();
				
				/*language known tokenfield*/
				 selectinglangajax();
				 changelanguage();
/*$('.lang_known').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>my_profile/get_suggestion_lang/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
    },
    showAutocompleteOnFocus: true
});
$('.lang_known').on('tokenfield:createtoken', function (event) {
	
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
}).on('tokenfield:createdtoken', function (e) {
	$("#forshowingnomore").remove();
	$this = $(this);
    var valueselected = e.attrs.value;
	$(".lang_known").each(function() {
		
		var existingTokenschk = $(this).tokenfield('getTokens');
		if(existingTokenschk.length > 0)
		{
			var existingTokenschkvalue = existingTokenschk[0].value;
			if($.trim(existingTokenschkvalue).toLowerCase()==$.trim(valueselected).toLowerCase() && $this.data("langknwon")!=$(this).data("langknwon"))
			{
				$this.tokenfield('setTokens', []);
				var lastidstored = $this.data("langknwon");
				$( ".lang_known_whole[data-lang_num='"+lastidstored+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_already_added']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
			}
		}
		
	});
  });*/
  /*language known tokenfield end*/
				var config = {
					  '.chosen-select'           : {},
					  '.chosen-select-deselect'  : {allow_single_deselect:true},
					  '.chosen-select-no-single' : {disable_search_threshold:10},
					  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
					  $(selector).chosen(config[selector]);
				}
			}
			else
			{
				$("#success_msg_lang").slideUp();
				$("#message_lang").html(data.errormessage);
				$("#message_lang").slideDown("slow", function()
				 { 
					//scroll_to_div('message_lang',-100);
					scroll_to_div_new('language_vieweditdivscroll'); 
				});
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformedu()
{
	$.validate({
    form : '#educationform',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgedu").hide();
      $("#messageedu").slideDown();
	  $("#messageedu").focus();
	  $("#messageedu").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div_new('education_vieweditdivscroll');
	  /*$('.chosen-select').css('display','none');*/
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messageedu").hide();
	  var datastring = $("#educationform").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editedudet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				
				$("#educationwholeid").html(data.contentreload.content);
				$("#messageedu").slideUp();
				$("#success_msgedu").slideDown("slow", function() 
				{ 
					//scroll_to_div('success_msgedu',-100); 
					 scroll_to_div_new('education_vieweditdivscroll');
				});
				$("#success_msgedu").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				resumeuploadfn();
				foraddingmorebuttonfnedu();
				foraddingmorebuttonfncerti();
				removepart();
				//deletepart();
				validateformedu();
				//$(data.contentreload.content).filter( '.chosen-select' ).chosen('destroy').chosen();
				var config = {
					  '.chosen-select'           : {},
					  '.chosen-select-deselect'  : {allow_single_deselect:true},
					  '.chosen-select-no-single' : {disable_search_threshold:10},
					  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
					  $(selector).chosen(config[selector]);
				}
			}
			else
			{
				$("#success_msgedu").slideUp();
				$("#messageedu").html(data.errormessage);
				$("#messageedu").slideDown("slow", function() 
				{ 
					//scroll_to_div('messageedu',-100); 
					 scroll_to_div_new('education_vieweditdivscroll');
				});
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformwork()
{
	$.validate({
    form : '#workdetform',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgwork").hide();
      $("#messagework").slideDown();
	  $("#messagework").focus();
	  $("#messagework").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  //scroll_to_div('messagework',-100);
	  scroll_to_div_new('workdetails_vieweditdivscroll');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messagework").hide();
	  var datastring = $("#workdetform").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'my_profile/editworkdet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#workwholeid").html(data.contentreload.content);
				$("#messagework").slideUp();
				$("#success_msgwork").slideDown("slow", function()
				 { 
					//scroll_to_div('success_msgwork',-100);
					 scroll_to_div_new('workdetails_vieweditdivscroll');
				 });
				$("#success_msgwork").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				deleteresume();
				resumeuploadfn();
				foraddingmorebuttonfnwork();
				removepart();
				//deletepart();
				validateformwork();
				$('.datepicker-join,.datepicker-leave').datepicker({
					  format: 'yyyy-mm-dd',
					  endDate: '+0d',
					   autoclose: true
					  
					});
					/*$('select')
					 .on('beforeValidation', function(value, lang, config) {
					  $('.chosen-select').css('display','block');
					 });
					$('select')
						.on('validation', function(value, lang, config) {
						  $('.chosen-select').css('display','none');
						 // $('.chosen-select').trigger('click');
					 });*/
				//$(data.contentreload.content).filter( '.chosen-select' ).chosen('destroy').chosen();
				var config = {
					  '.chosen-select'           : {},
					  '.chosen-select-deselect'  : {allow_single_deselect:true},
					  '.chosen-select-no-single' : {disable_search_threshold:10},
					  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
					  $(selector).chosen(config[selector]);
				}
			}
			else
			{
				$("#success_msgwork").slideUp();
				$("#messagework").html(data.errormessage);
				$("#messagework").slideDown("slow", function() 
				{
					// scroll_to_div('messagework',-100); 
					scroll_to_div_new('workdetails_vieweditdivscroll');
				});
			}
			
		}
	});
	 hide_comm_mask(); 	
      return false; // Will stop the submission of the form
    }
  });
}
function setsuggwidth()
{
	var home_citywidth = $('.tokenfield').outerWidth();
	$('#home_city-tokenfield').width(home_citywidth);
	$('#prefered_shift-tokenfield').width(home_citywidth);
	$('#preferred_city-tokenfield').width(home_citywidth);
	$('#key_skill-tokenfield').width(home_citywidth);
	$('#desire_job_type-tokenfield').width(home_citywidth);
	$('#employment_type-tokenfield').width(home_citywidth);
	$('[id^=lang_known]').width(home_citywidth);
}
function foraddingmorebuttonfnlang()
{
	$("#addmorebutton_lang").click(function(){
		 									
	$("#forshowingnomore").remove();
	var forstoringallthelangnos = new Array();
		var increment_counter = 0;
		$(".lang_known_whole").each(function() {
			increment_counter = increment_counter+1;
    		//var imagenumber = $(this).data('imagenumber');
			//forstoringallthelangnos.push($(this).data('lang_num'));
			forstoringallthelangnos.push(increment_counter);
		});
		var maximumnooflang = Math.max.apply(Math,forstoringallthelangnos);
		if(maximumnooflang > 10)
		{
			//$( "#addmorebutton_lang" )
			$( ".lang_known_whole[data-lang_num='"+maximumnooflang+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_max_exceed']; ?></div></div>" );
			$( "#forshowingnomore" ).delay(10000).fadeOut();//.closest( ".form-group" )
			//$( "#addmorebutton_lang" ).attr("disabled","disabled");
			//$( "#addmorebutton_lang" ).remove();
			$( "#addmorebutton_lang" ).parent().remove();
		}
		else
		{
			var forincresaingonecount = 0;
			var forstoring_messages = new Array();
			var increment_counter_final = 0;
			var forstoringallthelangnos_final = new Array();
			$(".lang_known_whole").each(function() {
				var language_num = $(this).data('lang_num');
				var valueofthistext = $('[data-langknwon="'+language_num+'"]').val();
				if($.trim(valueofthistext)=='' || $.trim(valueofthistext)=='undefined' || $.trim(valueofthistext)==undefined || $.trim(valueofthistext)==null || $.trim(valueofthistext)=='null')
				{
					forstoring_messages.push("<li>Please provide language number "+language_num+"</li>");
				}
				forstoringallthelangnos_final.push(language_num);
			});
			var maximumnooflang_final = Math.max.apply(Math,forstoringallthelangnos_final);
			if(forstoring_messages.length > 0)
			{
				//$( "#addmorebutton" ).closest( ".form-group" ).before( "<div class='form-group' id='forshowingnomoreblank'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'>Alert! Please select an image in the already available inputs then only you can add more.</div></div>" );
				$( ".lang_known_whole[data-lang_num='"+maximumnooflang_final+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_one_empty']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
			}
			else
			{
				//forincresaingonecount = maximumnooflang+1;
				forincresaingonecount = maximumnooflang_final + 1;
				var htmlstored = getlanguagehtml(forincresaingonecount);
				if(forincresaingonecount > 9 )
				{
					$( "#addmorebutton_lang" ).parent().remove();
				}
				$( ".lang_known_whole[data-lang_num='"+maximumnooflang_final +"']" ).after( htmlstored );
				$( ".lang_known" ).css("width", "100%");
				changelanguage();
				/* for chosen and tokenfield */
				removepart();
				//deletepart();
				//$('#proficiency_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
				selectinglangajax();
				/*$('.lang_known').tokenfield({
					autocomplete: {
					  source: function (request, response) {
						  jQuery.get("<?php echo $base_url; ?>my_profile/get_suggestion_lang/"+request.term, {
						  }, function (data) {
							 response(data);
						  });
					  },
					},
					showAutocompleteOnFocus: true
				});
				$('.lang_known').on('tokenfield:createtoken', function (event) {
					
					var existingTokens = $(this).tokenfield('getTokens');
					 if (existingTokens != '')
					 {
						 event.preventDefault();
					 }
				}).on('tokenfield:createdtoken', function (e) {
					$("#forshowingnomore").remove();
					$this = $(this);
					var valueselected = e.attrs.value;
					
					$(".lang_known").each(function() {
						var existingTokenschk = $(this).tokenfield('getTokens');
						if(existingTokenschk.length > 0)
						{
							var existingTokenschkvalue = existingTokenschk[0].value;
							
							if($.trim(existingTokenschkvalue).toLowerCase()==$.trim(valueselected).toLowerCase() && $this.data("langknwon")!=$(this).data("langknwon"))
							{
								$this.tokenfield('setTokens', []);
								var lastidstored = $this.data("langknwon");
								$( ".lang_known_whole[data-lang_num='"+lastidstored+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_already_added']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
							}
						}
					});
				  });*/
				/* for chosen and tokenfield end */
			}
		}
	}) 
}

function foraddingmorebuttonfncerti()
{
	$("#addmorebutton_certi").click(function(){
	$("#forshowingnomore_certi").remove();
	if($(".certi_known_whole").length >= 1)
	{
			var forstoringallthecertinos = new Array();
			var increment_counter = 0;
			$(".certi_known_whole").each(function() {
				increment_counter = increment_counter + 1;
				//forstoringallthelangnos.push($(this).data('lang_num'));
				forstoringallthecertinos.push(increment_counter);
			});
			var maximumnoofcerti = Math.max.apply(Math,forstoringallthecertinos);
			if(maximumnoofcerti > 10)
			{
				$( ".certi_known_whole[data-certi_num='"+maximumnoofcerti+"']" ).after( "<div class='clearfix' id='forshowingnomore_certi'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_certi_max_exceed']; ?></div></div>" );
				$( "#forshowingnomore_certi" ).delay(10000).fadeOut();
				$( "#addmorebutton_certi" ).fadeIn().remove();
				return false;
			}
			else
			{
				var forincresaingonecount = 0;
				var forstoring_certi = new Array();
				var increment_counter_final = 0;
				var forstoringallthecertinos_final = new Array();
				$(".certi_known_whole").each(function() {
					
					var certi_num = $(this).data('certi_num');
					
					var valueofnamecertitext = $('[data-namecerti="'+certi_num+'"]').val();
					var valueofpasyrcerti = $('[data-pasyrcerti="'+certi_num+'"]').val();
					var valueofcertides = $('[data-certides="'+certi_num+'"]').val();
					if($.trim(valueofcertides)=='' || $.trim(valueofcertides)=='undefined' || $.trim(valueofcertides)==undefined || $.trim(valueofcertides)==null || $.trim(valueofcertides)=='null' || $.trim(valueofpasyrcerti)=='' || $.trim(valueofpasyrcerti)=='undefined' || $.trim(valueofpasyrcerti)==undefined || $.trim(valueofpasyrcerti)==null || $.trim(valueofpasyrcerti)=='null' || $.trim(valueofnamecertitext)=='' || $.trim(valueofnamecertitext)=='undefined' || $.trim(valueofnamecertitext)==undefined || $.trim(valueofnamecertitext)==null || $.trim(valueofnamecertitext)=='null')
					{
						forstoring_certi.push("<li>Please provide "+$(this).attr('name')+" of certi number "+certi_num+"</li>");
					}
					forstoringallthecertinos_final.push(certi_num);
				});
				var maximumnoofcerti_final = Math.max.apply(Math,forstoringallthecertinos_final);
				if(forstoring_certi.length > 0)
				{
					$( ".certi_known_whole[data-certi_num='"+maximumnoofcerti_final+"']" ).after( "<div class='clearfix' id='forshowingnomore_certi'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_certi_one_empty']; ?></div></div>" );
					$("#forshowingnomore_certi").fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
				}
				else
				{
					//forincresaingonecount = maximumnoofcerti+1;
					forincresaingonecount = maximumnoofcerti_final+1;
					var htmlstored = getcertihtml(forincresaingonecount);
					$( ".certi_known_whole[data-certi_num='"+maximumnoofcerti_final+"']" ).after( htmlstored );
					/* for chosen */
						//$('#certiy_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
						
						if(forincresaingonecount > 9 )
						{
							$( "#addmorebutton_certi" ).parent().remove();
						}
						removepart();
						//deletepart();
						/*$('select')
						 .on('beforeValidation', function(value, lang, config) {
						  $('.chosen-select').css('display','block');
						 });
						$('select')
							.on('validation', function(value, lang, config) {
							  $('.chosen-select').css('display','none');
							 // $('.chosen-select').trigger('click');
						 });*/
					/* for chosen end */
				}
			}
	}
	else
	{
		var htmlstored = getcertihtml(1);
		$( "#addmorebutton_certi" ).parent().before( htmlstored );
		/* for chosen */
		//$('#certiy_a_1').chosen().trigger("chosen:updated");
		//foraddingmorebuttonfncerti();
		removepart();
		//deletepart();
		/* for chosen end */
	}
	}) 
}
function foraddingmorebuttonfnedu()
{
	$("#addmorebutton_edu").click(function(){					
	$("#forshowingnomore_edu").remove();
	/*$("#forshowingnomore_edu").hide();*/
	if($(".edu_known_whole").length >= 1)
	{
		var forstoringalltheedunos = new Array();
		var increment_counter = 0;
		$(".edu_known_whole").each(function() {
			increment_counter = increment_counter+1;
			//forstoringallthelangnos.push($(this).data('lang_num'));
			forstoringalltheedunos.push(increment_counter);
		});
		var maximumnoofedu = Math.max.apply(Math,forstoringalltheedunos);
		if(maximumnoofedu > 10)
		{
			$( ".edu_known_whole[data-edu_num='"+maximumnoofedu+"']" ).after( "<div class='' id='forshowingnomore_edu'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_edu_max_exceed']; ?></div></div>" );
			$( "#forshowingnomore_edu" ).fadeIn().delay(10000).fadeOut();
			$( "#addmorebutton_edu" ).parent().remove();
			/* for scrolling */
				 $('html,body').animate({
					scrollTop: $("#forshowingnomore_edu").offset().top +100 }
				,'slow');
				/* for scrolling end */
		}
		else
		{
			var forincresaingonecount = 0;
			var forstoring_education = new Array();
			var increment_counter_final = 0;
			var forstoringalltheedunos_final = new Array();
			$(".edu_known_whole").each(function() {
				var edu_num = $(this).data('edu_num');
				var valueofqllvltext = $('[data-qual="'+edu_num+'"]').val();
				var valueofinstext = $('[data-ins="'+edu_num+'"]').val();
				var valueofspeztext = $('[data-spez="'+edu_num+'"]').val();
				var valueofperctext = $('[data-perc="'+edu_num+'"]').val();
				var valueofpasyrtext = $('[data-pasyr="'+edu_num+'"]').val();
				if($.trim(valueofqllvltext)=='' || $.trim(valueofqllvltext)=='undefined' || $.trim(valueofqllvltext)==undefined || $.trim(valueofqllvltext)==null || $.trim(valueofqllvltext)=='null' || $.trim(valueofinstext)=='' || $.trim(valueofinstext)=='undefined' || $.trim(valueofinstext)==undefined || $.trim(valueofinstext)==null || $.trim(valueofinstext)=='null' || $.trim(valueofspeztext)=='' || $.trim(valueofspeztext)=='undefined' || $.trim(valueofspeztext)==undefined || $.trim(valueofspeztext)==null || $.trim(valueofspeztext)=='null' || $.trim(valueofperctext)=='' || $.trim(valueofperctext)=='undefined' || $.trim(valueofperctext)==undefined || $.trim(valueofperctext)==null || $.trim(valueofperctext)=='null' || $.trim(valueofpasyrtext)=='' || $.trim(valueofpasyrtext)=='undefined' || $.trim(valueofpasyrtext)==undefined || $.trim(valueofpasyrtext)==null || $.trim(valueofpasyrtext)=='null')
				{
					forstoring_education.push("<li>Please provide "+$(this).attr('name')+" of education number "+edu_num+"</li>");
				}
				//var res = language_num.split(" "); 
				forstoringalltheedunos_final.push(edu_num);
				/*alert(valueofqllvltext);alert(valueofinstext);alert(valueofspeztext);alert(valueofperctext);alert(valueofpasyrtext);*/
			});
			var maximumnoofedu_final = Math.max.apply(Math,forstoringalltheedunos_final);
			if(forstoring_education.length > 0)
			{
				//$( "#addmorebutton" ).closest( ".form-group" ).before( "<div class='form-group' id='forshowingnomoreblank'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'>Alert! Please select an image in the already available inputs then only you can add more.</div></div>" );
				$( ".edu_known_whole[data-edu_num='"+maximumnoofedu+"']" ).after( "<div class='' id='forshowingnomore_edu'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_edu_one_empty']; ?></div></div>" );
			$( "#forshowingnomore_edu" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
			}
			else
			{
				//forincresaingonecount = maximumnoofedu+1;
				forincresaingonecount = maximumnoofedu_final+1;
				var htmlstored = geteducationhtml(forincresaingonecount);
				//$( ".edu_known_whole[data-edu_num='"+maximumnoofedu+"']" ).after( htmlstored );
				$( ".edu_known_whole[data-edu_num='"+maximumnoofedu_final+"']" ).after( htmlstored );
				/* for scrolling */
				 $('html,body').animate({
					scrollTop: $(".edu_known_whole[data-edu_num='"+maximumnoofedu_final+"']").offset().top +100 }
				,'slow');
				/* for scrolling end */
				/* for chosen */
					//$('#qualification_level_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#passing_year_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					removepart();
					//deletepart();
				/* for chosen end */
				if(forincresaingonecount > 9 )
				{
					$( "#addmorebutton_edu" ).parent().remove();
				}
			}
		}
	}// if there is one edu end 
	else ///if there is no  edu
	{
		var htmlstored = geteducationhtml(1);
		$( "#addmorebutton_edu" ).parent().before( htmlstored );
		/* for scrolling */
		 $('html,body').animate({
			scrollTop: $(".edu_known_whole[data-edu_num='1']").offset().top +100 }
		,'slow');
				/* for scrolling end */
				/* for chosen */
		//$('#qualification_level_a_1').chosen().trigger("chosen:updated");
		//$('#passing_year_a_1').chosen().trigger("chosen:updated");
					
		//foraddingmorebuttonfncerti();
		removepart();
	}
	}) //on click end
}
function foraddingmorebuttonfnwork()
{
	$("#addmorebutton_work").click(function(){					
	$("#forshowingnomore_work").remove();
	var forstoringalltheworknos = new Array();
		var increment_counter = 0;
		$(".work_whole").each(function() {
			increment_counter = increment_counter+1;
			//forstoringallthelangnos.push($(this).data('lang_num'));
			forstoringalltheworknos.push(increment_counter);
		});
		var maximumnoofwork = Math.max.apply(Math,forstoringalltheworknos);
		/*if ($("#forshowingnomore_work").length > 0)
		{
			$( "#forshowingnomore_work" ).remove();
		}*/
		if(maximumnoofwork > 10)
		{
			$( ".work_whole[data-work_num='"+maximumnoofwork+"']" ).after( "<div class='' id='forshowingnomore_work'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_work_max_exceed']; ?></div></div>" );
			$( "#forshowingnomore_work" ).fadeIn().delay(10000).fadeOut();
			$( "#addmorebutton_work" ).parent().remove();
			return false;
		}
		else
		{
			var forincresaingonecount = 0;
			var forstoring_work = new Array();
			var increment_counter_final = 0;
			var forstoringalltheworknos_final = new Array();
			$(".work_whole").each(function() {
				var work_num = $(this).data('work_num');
				
				var valueofcmpnametext = $('[data-cmpname="'+work_num+'"]').val();
				var valueofindusdatext = $('[data-indusdat="'+work_num+'"]').val();
				var valueoffunareatext = $('[data-funarea="'+work_num+'"]').val();
				var valueofjbroltext = $('[data-jbrol="'+work_num+'"]').val();
				//var valueofldatetext = $('[data-ldate="'+work_num+'"]').val();
				var valueofjdatetext = $('[data-jdate="'+work_num+'"]').val();
				//var valueofcurrtyptext = $('[data-currtyp="'+work_num+'"]').val();
				//var valueofsallacsptext = $('[data-sallacs="'+work_num+'"]').val();
				//var valueofsalthotext = $('[data-saltho="'+work_num+'"]').val();
				if($.trim(valueofcmpnametext)=='' || $.trim(valueofcmpnametext)=='undefined' || $.trim(valueofcmpnametext)==undefined || $.trim(valueofcmpnametext)==null || $.trim(valueofcmpnametext)=='null' || $.trim(valueofindusdatext)=='' || $.trim(valueofindusdatext)=='undefined' || $.trim(valueofindusdatext)==undefined || $.trim(valueofindusdatext)==null || $.trim(valueofindusdatext)=='null' || $.trim(valueoffunareatext)=='' || $.trim(valueoffunareatext)=='undefined' || $.trim(valueoffunareatext)==undefined || $.trim(valueoffunareatext)==null || $.trim(valueoffunareatext)=='null' || $.trim(valueofjbroltext)=='' || $.trim(valueofjbroltext)=='undefined' || $.trim(valueofjbroltext)==undefined || $.trim(valueofjbroltext)==null || $.trim(valueofjbroltext)=='null'  || $.trim(valueofjdatetext)=='' || $.trim(valueofjdatetext)=='undefined' || $.trim(valueofjdatetext)==undefined || $.trim(valueofjdatetext)==null || $.trim(valueofjdatetext)=='null' /*|| $.trim(valueofcurrtyptext)=='' || $.trim(valueofcurrtyptext)=='undefined' || $.trim(valueofcurrtyptext)==undefined || $.trim(valueofcurrtyptext)==null || $.trim(valueofcurrtyptext)=='null' || $.trim(valueofsallacsptext)=='' || $.trim(valueofsallacsptext)=='undefined' || $.trim(valueofsallacsptext)==undefined || $.trim(valueofsallacsptext)==null || $.trim(valueofsallacsptext)=='null' || $.trim(valueofsalthotext)=='' || $.trim(valueofsalthotext)=='undefined' || $.trim(valueofsalthotext)==undefined || $.trim(valueofsalthotext)==null || $.trim(valueofsalthotext)=='null'*/)
				{
					forstoring_work.push("<li>Please provide "+$(this).attr('name')+" of work number "+work_num+"</li>");
				}
				/*alert(valueofcmpnametext);alert(valueofindusdatext);alert(valueoffunareatext);alert(valueofjbroltext);alert(valueofjdatetext);alert(valueofcurrtyptext);alert(valueofsallacsptext);alert(valueofsalthotext);*/
				//|| $.trim(valueofldatetext)=='' || $.trim(valueofldatetext)=='undefined' || $.trim(valueofldatetext)==undefined || $.trim(valueofldatetext)==null || $.trim(valueofldatetext)=='null'
				forstoringalltheworknos_final.push(work_num);
			});
			var maximumnoofwork_final = Math.max.apply(Math,forstoringalltheworknos_final);
			
			if(forstoring_work.length > 0)
			{
				if ($("#forshowingnomore_work").length > 0)
				{
					$("#forshowingnomore_work").html('<?php echo $custom_lable_arr['myprofile_work_one_empty']; ?>');
				}
				else
				{
					$( ".work_whole[data-work_num='"+maximumnoofwork_final+"']" ).after( "<div class='' id='forshowingnomore_work'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_work_one_empty']; ?></div></div>" );
					$( "#forshowingnomore_work" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" ).fadeOut();
				}
				
			}
			else
			{
				//forincresaingonecount = maximumnoofwork+1;
				forincresaingonecount = maximumnoofwork_final+1;
				
				var htmlstored = getworkhtml(forincresaingonecount);
				//$( ".work_whole[data-work_num='"+maximumnoofwork+"']" ).after( htmlstored );
				$( ".work_whole[data-work_num='"+maximumnoofwork_final+"']" ).after( htmlstored );
				/* for chosen */
					//$('#industry_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#functional_area_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#job_role_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#currency_type_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#currency_type_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#a_salary_lacs_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					//$('#a_salary_thousand_a_'+forincresaingonecount).chosen().trigger("chosen:updated");
					$('.datepicker-join,.datepicker-leave').datepicker({
					  format: 'yyyy-mm-dd',
					  endDate: '+0d',
					   autoclose: true
					  
					});
					if(forincresaingonecount > 9 )
					{
						$( "#addmorebutton_work" ).parent().remove();
					}
					removepart();
					//deletepart();
				/* for chosen end */
			}
		}
	}) 
}
function getlanguagehtml(number)
{
		var htmlret = '<div class="panel-body lang_known_whole" style="padding:10px;"  data-lang_num="'+number+'"><hr class="th_bordercolor" style="border-width: 4px;"><div class="row" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new">Language Known </label><select name="lang_known_a_'+number+'" id="lang_known_a_'+number+'" class="input-select-b select-drop-3 lang_known"  placeholder="Language Known" data-langknwon='+number+' value="" style="padding:9px 0 9px 10px;border-radius:0;width:100%;" data-validation="required"></select></div><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new">Proficiency Level</label><select name="proficiency_a_'+number+'" id="proficiency_a_'+number+'" class="input-select-b select-drop-3 city">';
			<?php if(isset($skill_lvl_lang) && $skill_lvl_lang!='' )
												{ ?>
													htmlret= htmlret+'<?php print_r($skill_lvl_lang); ?>';
												<?php }
												else
												{ ?>
													htmlret= htmlret+'<option value=""><?php echo $custom_lable_arr['myprofile_pro_lvl_blak_opt']?></option>';
												<?php }
												 ?>
												 htmlret= htmlret+'</select></div></div><div class="row" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><div class="row" style="margin-bottom:0px;"><div class="col-md-4 col-xs-4 col-sm-4 margin-top-5"><div class="checkbox-new"><label class="checkbox-new" style="font-size:15px;"><input type="checkbox" name="checkbox_a_'+number+'[]" value="reading"><span class="indicator"></span>Read</label></div></div><div class="col-md-4 col-xs-4 col-sm-4 margin-top-5"><div class="checkbox-new"><label class="checkbox-new" style="font-size:15px;"><input type="checkbox" name="checkbox_a_'+number+'[]" value="writing"><span class="indicator"></span>Write</label></div></div><div class="col-md-4 col-xs-4 col-sm-4 margin-top-5"><div class="checkbox-new"><label class="checkbox-new" style="font-size:15px;"><input type="checkbox" name="checkbox_a_'+number+'[]" value="speaking"><span class="indicator"></span>Speak</label></div></div></div></div></div><div class="input-group-btn text-center"><button class="btn btn-success removepart" data-removepart="lang" data-removeid="'+number+'"  type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Remove</button><div class="margin-bottom-20"></div></div></div>';
										return htmlret;
}
function geteducationhtml(number)
{
	var htmlret = '<div class="panel-body edu_known_whole" style="padding:10px;" data-edu_num="'+number+'"><hr class="th_bordercolor" style="border-width: 4px;"><div class="row" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['qualification_lvl']; ?> </label><select name="qualification_level_a_'+number+'" id="qualification_level_a_'+number+'" data-qual = "'+number+'" class="input-select-b select-drop-3 city" onChange="donotrepeatedu(this);"><?php if(isset($education_frm_tbl) && $education_frm_tbl !='')
                                                    { 
                                                        print_r($education_frm_tbl); 
                                                    }
                                                    else
                                                    { ?><option value="">Please select your education</option><?php } ?></select></div><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['institute_name']; ?> </label><input type="text" name="institute_name_a_'+number+'" id="institute_name_a_'+number+'" data-ins = "'+number+'" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['institute_name']; ?>" value=""  /></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['specialization']; ?> </label><input type="text" name="specialization_a_'+number+'" id="specialization_a_'+number+'" data-spez = "'+number+'" class="edit-input"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['specialization']; ?>" value=""  /></div><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['passing_year']; ?></label><select name="passing_year_a_'+number+'" id="passing_year_a_'+number+'" data-pasyr="'+number+'" class="input-select-b select-drop-3 city"><?php 
													$current_year = date('Y');
													echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
													for($i = 1980; $i < $current_year ; $i++)
													{ 
													?><option value="<?php echo $i; ?>" ><?php echo $i; ?></option><?php 	
													} 
													?></select></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['percentage']; ?> </label><input type="text" name="percentage_a_'+number+'" id="percentage_a_'+number+'" data-perc = "'+number+'" class="edit-input"   placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['percentage']; ?>" value=""  /></div></div><div class="margin-bottom-20 clearfix"></div><div class="row"><div class="input-group-btn text-center"><button class="btn btn-success removepart" data-removepart="edu" data-removeid="'+number+'"  type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Remove</button><div class="margin-bottom-20"></div></div></div></div>';
										return htmlret;
										//<div class="input-group-btn text-center"><button class="btn btn-success removepart" data-removepart="edu" data-removeid="'+number+'"  type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Remove</button><div class="margin-bottom-20"></div></div>
}
function getworkhtml(number)
{
	
	var htmlret = '<div class="panel-body work_whole" style="padding:10px;" data-work_num="'+number+'"><hr class="th_bordercolor" style="border-width: 4px;"><div class="row" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['company_name']; ?></label><input type="text" class="edit-input" name="companyname_a_'+number+'" id="companyname_a_'+number+'" data-cmpname = "'+number+'" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['company_name']; ?>" value="" data-validation="required"/></div><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['type_industry']; ?> </label><select name="industry_a_'+number+'" id="industry_a_'+number+'" data-indusdat = "'+number+'" class="input-select-b select-drop-3 city" data-validation="required"><?php if(isset($ind_frm_tbl) && $ind_frm_tbl !=''){ 
                                                        print_r($ind_frm_tbl); 
                                                    }
                                                    else
                                                    { ?><option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['type_industry']; ?></option><?php } ?></select></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['functional_area']; ?> </label><select onChange="dropdownChange(&apos;functional_area_a_'+number+'&apos;,&apos;job_role_a_'+number+'&apos;,&apos;role_master&apos;);" name="functional_area_a_'+number+'" id="functional_area_a_'+number+'" data-funarea="'+number+'" data-validation="required" class="input-select-b select-drop-3" ><?php if(isset($fnarea_frm_tbl) && $fnarea_frm_tbl !='')
                                                    { 
                                                        print_r($fnarea_frm_tbl); 
                                                    }
                                                    else
                                                    { ?><option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['functional_area']; ?></option><?php } ?></select></div><div class="col-md-6 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['job_role']; ?> </label><select name="job_role_a_'+number+'" id="job_role_a_'+number+'" data-validation="required" class="input-select-b select-drop-3" data-jbrol="'+number+'"><option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['job_role']; ?></option></select></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-12 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['joining_date']; ?>  </label><input type="text" class="edit-input datepicker datepicker-join" data-validation="required" id="joining_date_a_'+number+'" name="joining_date_a_'+number+'" data-jdate="'+number+'" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['joining_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value=""></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-12 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['leaving_date']; ?> </label><input type="text" class="edit-input datepicker datepicker-leave" id="leaving_date_a_'+number+'" name="leaving_date_a_'+number+'" data-ldate="'+number+'" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['leaving_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value=""></div></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-12 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['annual_salary']; ?> </label><select name="annual_salary_a_'+number+'" id="annual_salary_a_'+number+'" data-indusdat = "'+number+'" class="input-select-b select-drop-3 city" data-validation="required"><?php if(isset($salary_frm_tbl) && $salary_frm_tbl!='')
                                                    { 
                                                        print_r($salary_frm_tbl); 
                                                    }
                                                    else
                                                    { ?><option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['annual_salary']; ?></option><?php } ?></select></div><?php /*?><div class="col-sm-4"><select name="currency_type_a_'+number+'" id="currency_type_a_'+number+'" data-currtyp="'+number+'" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;"><?php echo $this->common_front_model->get_list('currency_master','str'); ?></select></div><div class="col-sm-4"><select name="a_salary_lacs_a_'+number+'" id="a_salary_lacs_a_'+number+'" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;"  data-sallacs="'+number+'"><option value=""><?php echo $custom_lable_arr['lacs']; ?></option><?php 
                                                                 for($i=0;$i <= 99 ; $i++)
                                                                 { ?><option value="<?php echo $i ?>" ><?php echo $i ?> </option><?php } ?></select></div><div class="col-sm-4"><select name="a_salary_thousand_a_'+number+'" id="a_salary_thousand_a_'+number+'" data-saltho="'+number+'" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;"><option value=""><?php echo $custom_lable_arr['thousand']; ?></option><?php
                                                             for($i=0;$i <= 90;)
                                                             {  ?><option value="<?php echo $i ?>"><?php echo $i ?> </option><?php $i = $i+5; }
                                                             ?></select></div><?php */?></div><div class="row margin-top-10" style="margin-bottom:0px;"><div class="col-md-12 col-xs-12 col-sm-12"><label class="label-new"><?php echo $custom_lable_arr['achievements']; ?></label><textarea class="edit-input" id="achievements_a_'+number+'" name="achievements_a_'+number+'" data-toggle="popover" data-placement="top" data-trigger="hover" data-achv = "'+number+'" data-content="<?php echo $custom_lable_arr['achievements_popoer']; ?>" data-original-title="" title=""></textarea></div></div><div class="margin-bottom-20 clearfix"></div><div class="row"><div class="input-group-btn text-center"><button class="btn btn-success removepart" data-removepart="work" data-removeid="'+number+'"  type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Remove</button><div class="margin-bottom-20"></div></div></div></div>';
										return htmlret;
}

function getcertihtml(number)
{
	var htmlret = '<div class="row certi_known_whole clearfix" data-certi_num="'+number+'"><div class="col-md-4 col-xs-12"><div class="margin-top-20"></div><h5 >Certificate name</h5><input type="text" name="certiu_a_'+number+'" id="certiu_'+number+'" class="form-control language" placeholder="Enter Certificate name" value="" data-namecerti="'+number+'"></div><div class="col-md-4 col-xs-12"><div class="margin-top-20"></div><h5 ><?php echo $custom_lable_arr['passing_year']; ?>: <span class="red-only">*</span></h5><select name="certiy_a_'+number+'" id="certiy_a_'+number+'" data-pasyrcerti="'+number+'" class="form-control city" style="padding:10px;border-radius:0;"><?php $current_year = date('Y');echo '<option value="">'.$custom_lable_arr['passing_year'].'</option>';
														for($i = 1980; $i < $current_year ; $i++)
                                                        { 
                                                        ?><option value="<?php echo $i; ?>" ><?php echo $i; ?></option><?php 	
                                                        } 
                                                        ?></select></div><div class="col-md-4 col-xs-12"><div class="margin-top-20"></div><h5 >Certificate Description</h5><input type="text" name="certid_a_'+number+'" id="certid_a_'+number+'" class="form-control language" placeholder="Enter Certificate description" value="" data-certides="'+number+'"></div><div class="margin-bottom-20 clearfix"></div><div class="row"><div class="input-group-btn text-center"><button class="btn btn-success removepart" data-removepart="cert" data-removeid="'+number+'"  type="button" id=""><span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Remove</button></div></div></div>';
										return htmlret;
}
function removepart()
{
	$( ".removepart" ).click(function() {
  		var removepart = $(this).attr("data-removepart");
		var removeid = $(this).attr("data-removeid");
		if(removepart=='work')
		{
			$('[data-work_num="'+removeid+'"]').remove();
			$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
			$("#forshowingnomore_work").remove();
			/* for showing add more button if not shown */
			if($(".work_whole").length > 0)
			{
				var forstoringalltheworknos_final = new Array();
				$(".work_whole").each(function() {
				var work_num = $(this).data('work_num');
				forstoringalltheworknos_final.push(work_num);
				});	
				var maximumnoofwork_final = Math.max.apply(Math,forstoringalltheworknos_final);
				
				if (!($("#addmorebutton_work").length > 0))
				{
					$( ".work_whole[data-work_num='"+maximumnoofwork_final+"']" ).after( "<div class='input-group-btn text-center clearfix'><button class='btn btn-success' type='button' id='addmorebutton_work'> <span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Add more Work details</button></div>" );
				}
			}
			/* for showing add more button if not shown end*/
			$("#forshowingnomore_work").remove();
			//foraddingmorebuttonfnwork();
		}
		if(removepart=='lang')
		{
			$('[data-lang_num="'+removeid+'"]').remove();
			$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
			$("#forshowingnomore").remove();
			/* for showing add more button if not shown */
			if($(".lang_known_whole").length > 0)
			{
				var forstoringallthelangnos_final = new Array();
				$(".lang_known_whole").each(function() {
				var lang_num = $(this).data('lang_num');
				forstoringallthelangnos_final.push(lang_num);
				});	
				var maximumnooflang_final = Math.max.apply(Math,forstoringallthelangnos_final);
				
				if (!($("#addmorebutton_lang").length > 0))
				{
					$( ".lang_known_whole[data-lang_num='"+maximumnooflang_final+"']" ).after( "<div class='input-group-btn text-center clearfix'><button class='btn btn-success' type='button' id='addmorebutton_lang'> <span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Add more Languages</button></div>" );
				}
			}
			/* for showing add more button if not shown end*/
			$("#forshowingnomore").remove();
			//foraddingmorebuttonfnlang();
		}
		if(removepart=='edu')
		{
			/*$('[data-qual="'+removeid+'"]').remove().trigger("chosen:updated");
			$('[data-ins="'+removeid+'"]').remove().trigger("chosen:updated");
			$('[data-spez="'+removeid+'"]').remove().trigger("chosen:updated");
			$('[data-perc="'+removeid+'"]').remove().trigger("chosen:updated");
			$('[data-pasyr="'+removeid+'"]').remove().trigger("chosen:updated");*/
			
			
			$('[data-edu_num="'+removeid+'"]').remove();
			$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
			/* for showing add more button if not shown */
			if($(".edu_known_whole").length > 0)
			{
				var forstoringalltheedunos_final = new Array();
				$(".edu_known_whole").each(function() {
				var edu_num = $(this).data('edu_num');
				forstoringalltheedunos_final.push(edu_num);
				});	
				var maximumnoofedu_final = Math.max.apply(Math,forstoringalltheedunos_final);
				
				if (!($("#addmorebutton_edu").length > 0))
				{
					$( ".edu_known_whole[data-edu_num='"+maximumnoofedu_final+"']" ).after( "<div class='input-group-btn text-center clearfix'><button class='btn btn-success' type='button' id='addmorebutton_edu'> <span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Add more Education</button></div>" );
				}
			}
			/* for showing add more button if not shown end*/
			$("#forshowingnomore_edu").remove();
			//foraddingmorebuttonfnedu();
			
		}
		if(removepart=='cert')
		{
			$('[data-certi_num="'+removeid+'"]').remove();
			$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
			/* for showing add more button if not shown */
			if($(".certi_known_whole").length > 0)
			{
				var forstoringallthecertinos_final = new Array();
				$(".certi_known_whole").each(function() {
				var certi_num = $(this).data('certi_num');
				forstoringallthecertinos_final.push(certi_num);
				});	
				var maximumnoofcerti_final = Math.max.apply(Math,forstoringallthecertinos_final);	
				if (!($("#addmorebutton_certi").length > 0))
				{
					$( ".certi_known_whole[data-certi_num='"+maximumnoofcerti_final+"']" ).after( "<div class='input-group-btn text-center clearfix'><button class='btn btn-success' type='button' id='addmorebutton_certi'> <span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Add more Certificate</button></div>" );
				}
			}
			/* for showing add more button if not shown end*/
			//foraddingmorebuttonfncerti();
			$("#forshowingnomore_certi").remove();
		}
	});
}
function deletepart(deletepart,deleteid,deleteid_count)
{
	/*$( ".input-group-btn button.deletepart" ).click(function() {*/
			/*var deletepart = $(this).attr("data-deletepart");
			var deleteid = $(this).attr("data-deleteid");
			var deleteid_count = $(this).attr("data-deleteid_count");*/
			if(deletepart=='work')
			{
				var r = confirm("Are you sure.You want to delete information from your profile!");
				if (r == true) {	
				//$('[data-work_num="'+deleteid+'"]').remove();
				forgetting_part(deletepart,deleteid,deleteid_count);
				//$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
				} 
				else
				{
					return false;
				}
			}
			if(deletepart=='lang')
			{
				var r = confirm("Are you sure.You want to delete information from your profile!");
				if (r == true) 
				{		
					//$('[data-lang_num="'+deleteid+'"]').remove();
					forgetting_part(deletepart,deleteid,deleteid_count);
				//$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
				}
				else
				{
					return false;
				}
			}
			if(deletepart=='edu')
			{
				var forstoringlastid = deleteid;
				var r = confirm("Are you sure.You want to delete information from your profile!");
				if (r == true) {	
				//$('[data-edu_num="'+deleteid+'"]').remove();
				//$('[data-deleteid_count="'+deleteid_count+'"]').remove();
				forgetting_part(deletepart,deleteid,deleteid_count);
				//$('.removepart[data-removeid="'+removeid+'"][data-removepart="'+removepart+'"]').parent().remove();
				}
				else
				{
					return false;
				}
			}
		
	/*});*/
}
function forgetting_part(part,id,count)
{
	var hash_tocken_id = $("#hash_tocken_id").val();
	var url_req = "<?php echo $base_url.'my_profile/delete_section'; ?>";
	//in case of resumedelete id will filename
	show_comm_mask();
	var datastring = 'section='+part+'&id='+id+'&count='+count+ "&csrf_job_portal="+hash_tocken_id;
	$.ajax({
		url : url_req,
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if(part=='edu')
			{
				$("#educationwholeid").html(data.contentreload);	
				foraddingmorebuttonfnedu();
				foraddingmorebuttonfncerti();
				validateformedu();
				removepart();
				//deletepart();
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			if(part=='lang')
			{
				$("#langwholeid").html(data.contentreload);	
				foraddingmorebuttonfnlang();
				validateformlang();
				removepart();
				//deletepart();
				selectinglangajax();
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				/*$('.lang_known').tokenfield({
					autocomplete: {
					  source: function (request, response) {
						  jQuery.get("<?php echo $base_url; ?>my_profile/get_suggestion_lang/"+request.term, {
						  }, function (data) {
							 response(data);
						  });
					  },
					},
					showAutocompleteOnFocus: true
				});
				$('.lang_known').on('tokenfield:createtoken', function (event) {
					
					var existingTokens = $(this).tokenfield('getTokens');
					 if (existingTokens != '')
					 {
						 event.preventDefault();
					 }
				}).on('tokenfield:createdtoken', function (e) {
					$("#forshowingnomore").remove();
					$this = $(this);
					var valueselected = e.attrs.value;
					
					$(".lang_known").each(function() {
						var existingTokenschk = $(this).tokenfield('getTokens');
						if(existingTokenschk.length > 0)
						{
							var existingTokenschkvalue = existingTokenschk[0].value;
							
							if($.trim(existingTokenschkvalue).toLowerCase()==$.trim(valueselected).toLowerCase() && $this.data("langknwon")!=$(this).data("langknwon"))
							{
								$this.tokenfield('setTokens', []);
								var lastidstored = $this.data("langknwon");
								$( ".lang_known_whole[data-lang_num='"+lastidstored+"']" ).after( "<div class='' id='forshowingnomore'><div class='alert alert-danger text-center col-md-8 col-md-offset-2'><?php echo $custom_lable_arr['myprofile_lang_already_added']; ?></div></div>" );
			$( "#forshowingnomore" ).fadeIn().delay(10000).fadeOut();//.closest( ".form-group" )
							}
						}
					});
				  });*/
			}
			if(part=='work')
			{
				$("#workwholeid").html(data.contentreload);	
				foraddingmorebuttonfnwork();
				validateformwork();
				removepart();
				$('.datepicker-join,.datepicker-leave,#birthdate').datepicker({
				  format: 'yyyy-mm-dd',
				  endDate: '+0d',
				   autoclose: true,
				  });
				//deletepart();
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			if(part=='resume')
			{
				$("#resume_dwn_viewdiv").html(data.contentreload);
				//$("#resume_dwn_viewdiv").html(data.contentreload.content);
				deleteresume();
				resumeuploadfn();
			}
			
				deleteresume();
				
				//$(data.contentreload.content).filter( '.chosen-select' ).chosen('destroy').chosen();
				var config = {
					  '.chosen-select'           : {},
					  '.chosen-select-deselect'  : {allow_single_deselect:true},
					  '.chosen-select-no-single' : {disable_search_threshold:10},
					  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
					  $(selector).chosen(config[selector]);
				}
				hide_comm_mask();
		}
	});
}
</script>