<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/skill.css">
<input type="hidden" name="checkupdated" id="checkupdated" value="0">
<style>
	/*#defineheight{ max-height:300px;overflow-y:auto;overflow-x:hidden; }
	#defineheightlng{ max-height:300px;overflow-y:auto;overflow-x:hidden; }*/
	.fileinp
	{
		/*position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		cursor: pointer;*/
		position: absolute;
		cursor: pointer;
		opacity: 0;
	}
	@media only screen and (max-width: 768px) {
		.marginclass
		{
			margin-bottom:5px;
		}
	}
	.foreditfontcolor
	{
		color:#ffffff;
	}
</style>
<?php  $custom_lable_arr = $custom_lable->language;
$langinc['custom_lable'] = $custom_lable_arr;

	 ?>
<?php  $upload_path_profile ='./assets/emp_photos';
$upload_path_demomenu ='./assets/front_end/images/demoprofileimge2.png';
$upload_path_demoheader ='./assets/front_end/images/demoprofileimge.png'; ?>
<!-- Titlebar
================================================== -->
<input type="hidden" name="profileeditflag" id="profileeditflag">
<section>
	<div id="container-fluid">
		<?php $this->load->view('front_end/employer_profile_head',$langinc['custom_lable']); ?>
	</div>
	<div id="sticky" class="container-fluid">
		<ul class="nav nav-tabs nav-menu" role="tablist">
			<li class="active">
			  <a href="#profile" role="tab" data-toggle="tab" class="text-center">
				  <span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['emp_pro_tab_tit']; ?>
			  </a>
			</li>
			<li><a href="#change" role="tab" data-toggle="tab" class="text-center">
			  <span class="glyphicon glyphicon-edit"></span> <?php echo $custom_lable_arr['emp_edit_pro_tab_tit']; ?>
			  </a>
			</li>
			<!--<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
			<div class="margin-top-20"></div>	
				<div class="panel panel-primary box-shadow1"  style="border:none;border-radius:0px;border:0px solid #D9534F;padding:5px;">
					<div class="panel-heading th_bordercolor" style="background:none;color:#000000;padding:0px;border:0px;border-bottom:1px solid;"><span style="padding:8px;"> Complete Your Profile - Add Details</span>
						<button type="button" class="close" data-target="#copyright-wrap" data-dismiss="alert" style="font-size:30px;"> <span aria-hidden="true">&times;</span><span class="sr-only">X</span></button>
					</div>
								
					<div class="panel-body" style="padding:10px;">
						<div class="form-group">
							<label class="col-md-2 text-center" for="pname">Project Name:</label>
							<div class="col-md-6">
								<input type="text" placeholder="Abc@xyz.com" value="" style="padding:0px 0 9px 10px;"/>
							</div>
							<div class="margin-top-10"></div>
								<div class="text-center">
									<button id="button1idFFF" name="button1idFFF" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
									<button id="button2id" name="button2id" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
								</div>
						</div>
					</div>
				</div>
			</div>-->
		</ul>
		<div class="tab-content">
        <!--profile view tab-->
        <div class="tab-pane fade active in" id="profile" style="padding:0px;">
			<?php $this->load->view('front_end/empprofile_view',$langinc['custom_lable']); ?>
        </div>
		<!--profile view tab end -->
        <!--profile edit tab-->
        <!--<div id="editdicv">-->
            <?php $this->load->view('front_end/employer_profile_edit',$langinc['custom_lable']); ?>
        <!--</div>    -->
		<!--profile edit tab end-->
		</div>
	</div>
</section>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script src="<?php echo $base_url; ?>assets/front_end/js/common.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<!-- WYSIWYG Editor -->
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery.sceditor.bbcode.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery.sceditor.js"></script>
<script>
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });

jQuery(document).ready(function(){
	jQuery('.skillbar').each(function(){
		jQuery(this).find('.skillbar-bar').animate({
			width:jQuery(this).attr('data-percent')
		},6000);
	});
});
</script>
<script>
/****Accordian Menu***/
$(document).ready(function()
{
	/* */
	validateformpersonal();	
	validateformcompany();
	validateformwork();
	validateformskillhire();
	validateformsociallink();
	validateformpassword();
	validateformdeleteprofile();
	/* */
    //Add Inactive Class To All Accordion Headers
	 
	$('.accordion-header').toggleClass('inactive-header');
	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});
	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');
	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content').promise().done(function(){
				$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');		    	
			});
		}
		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content').promise().done(function(){
    			$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');
			});
		}
	});

<?php if($user_data!=""){?> 
dropdownChange('country','city','city_list',function() {setcityval();});
function setcityval()
{
	$("#city").val('<?php echo $user_data['city']; ?>');
}
<?php }  ?>		
	return false;
});

function editsection(section)
{
	/*$( "a[href='#change']" ).trigger( "click" );*/
	var scrolltodivid = "#"+section+'editdivscroll';
	var openacc = "#"+section+'editdiv';
	$this = $(openacc);
	$this1 = $(scrolltodivid);
	
	$( "a[href='#profile']" ).attr( "aria-expanded", false ).closest("li").removeClass("active");
	$( "a[href='#change']" ).attr( "aria-expanded", true ).closest("li").addClass("active");
	$( "#change" ).addClass( "active in" );
	$( "#profile" ).removeClass( "active in" );
	if($.trim(section)=='')
	{
		var section = 'profile_snapshot';
	}
	else
	{
		var section = section;
	}
	
	$('html,body').animate({
				scrollTop: $this1.offset().top - 100
			}
		,'slow');
	
	setTimeout(function() { 
	if($this.is('.inactive-header')) 
	{
		$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
		$this.toggleClass('active-header').toggleClass('inactive-header');
		$this.next().slideToggle().toggleClass('open-content').promise().done(function(){
    			$('html,body').animate({
					scrollTop: $(this).offset().top -100 }
				,'slow');
			});
	} }, 200);	
	return false;
}
function viewsection(section)
{
	/*if(checkifupdated())
	{
		var hash_tocken_id = $("#hash_tocken_id").val();
		show_comm_mask();
		$.ajax({
						url: "<?php echo $base_url.'employer_profile/viewareaget' ?>", // Url to which the request is send
						type: "POST",             // Type of request to be send, called as method
						data: {"csrf_job_portal":hash_tocken_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						dataType:"json",
						success: function(data)   // A function to be called if request succeeds
						{
							$("#hash_tocken_id").val(data.tocken);
							$("#profile").val(data.data);
							editsection(section);
							hide_comm_mask();
						}
						});
	hide_comm_mask();					
	}
	else
	{*/
		var scrolltodivid = "#"+section+'editdivscroll';
		var openacc = "#"+section+'editdiv';
		var tabactdivid = "#"+section+'editdivscroll_sec';
		$this = $(openacc);
		$this1 = $(scrolltodivid);
		$this2 = $(tabactdivid);
		$( "a[href='#change']" ).attr( "aria-expanded", false ).closest("li").removeClass("active");
		$( "a[href='#profile']" ).attr( "aria-expanded", true ).closest("li").addClass("active");
		$( "#profile" ).addClass( "active in" );
		$( "#change" ).removeClass( "active in" );
		if($.trim(section)=='' || $.trim(section)=='undefined' || $.trim(section)==undefined || $.trim(section)=='null' || $.trim(section)==null)
		{
			var section = 'personal_det';
		}
		else
		{
			var section = section;
		}
		$('html,body').animate({
					scrollTop: $this1.offset().top - 100
				}
			,'slow');	
			
		//$('.tab-pane').removeClass( "active" );
		//$this2.addClass( "active" );
	/*}*/
	
	return false;
}
$("#uploadimage").on('change',function(){
			show_comm_mask();
			var htmlobject=$( "#uploadimage" ).get( 0 );
			var filename = $("#uploadimage").val();
			var form_data = new FormData();
			var hash_tocken_id = $("#hash_tocken_id").val();
			form_data.append('resumeimg', $('input[name=uploadimage]')[0].files[0]);
			form_data.append('user_agent', 'NI-WEB');
			form_data.append('csrf_job_portal', hash_tocken_id);
			var hiddendwn = $('#downloadresumediv').length;
			var hiddendel = $('#deletediv').length;
				if(filename!='')
				{
					$.ajax({
						url: "<?php echo $base_url.'my_profile/uploadresume' ?>", // Url to which the request is send
						type: "POST",             // Type of request to be send, called as method
						data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						dataType:"json",
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,        // To send DOMDocument or non processed data file it is set to false
						success: function(data)   // A function to be called if request succeeds
						{
							if(data.final_result.status=='success')	
							{	
								$("#uploadimage").val('');
								if(hiddendwn)
								{
									//$("#downloadresumediv").slideDown();
								}
								if(hiddendel)
								{
									//$("#deletediv").slideDown();
								}
								$("#deletediv").attr("hello","hello");
								$("#downloadresumediv").attr("hello","hello");
								$("#success_message").addClass("alert alert-success alert-dismissable fade in");
								$("#success_message").html('<div class="col-xs-11">'+'<?php echo $custom_lable_arr['sign_up_emp_reume_upload_succ']; ?>'+'</div><div class="col-xs-1"><a href="javascript:void(0)" class="close" onclick="hideid()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
								$("#success_message").show().css("opacity","1");
								setTimeout(function() { $("#success_message").hide(); }, 10000);
								$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
								$("#noreusme").remove();
								
							}					
							if(data.final_result.status=='error')
							{
								hide_comm_mask();
								$("#uploadimage").val('');
								$("#success_message").addClass("alert alert-danger alert-dismissable fade in");
								$("#success_message").html('<div class="col-xs-11">'+data.final_result.error_message+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
								$("#success_message").show().css("opacity","1");
								setTimeout(function() { $("#success_message").hide(); }, 10000);
							}
							$("#hash_tocken_id").val(data.tocken);
							hide_comm_mask();
						}
						});
				}
			});	
function hideid()
{
	$("#success_message").hide().removeClass("alert alert-danger alert-dismissable fade in");
}
function hideid1()
{
	$("#image_val").hide().removeClass("alert alert-danger alert-dismissable fade in");
}
function displayprofile()
{
	var myimage = $("#profile_img")[0];
	if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
	{
		document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
		$("#blah").slideDown();
	}
	else
	{
		$("#blah").hide();
		document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
		$("#profile_img").val('');
		showmessage('<?php echo $custom_lable_arr['invalid_file_format']; ?>','10000');
	}
}
function uploadprofilepic()
{
	if($("#profile_img").val()=='')
	{
		showmessage('Please select an image to uplaod as profile picture','10000');
	}
	else if($("#profile_img").val()!='')
	{
	 	var profile = document.getElementById("profile_img").files[0]; 
		if (profile.size > 2097152) // 2 mb for bytes.
		{
			alert("The file you are attempting to upload is larger than the permitted size, Max file size allow upto 2MB");
			return false;
		}
		else
		{
			senduploadedprofile();
		}
	}
	
	return false;
}
function showmessage(message,fortime)
{
		$("#image_val").show().css("opacity","1");
		$("#image_val").addClass("alert alert-danger alert-dismissable fade in");
		$("#image_val").html('<div class="col-xs-9">'+message+'</div><div class="col-xs-3"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');
		setTimeout(function() { $("#image_val").hide(); }, fortime);
}
function senduploadedprofile()
{
			//event.preventDefault();
			show_comm_mask();
			var htmlobject=$( "#profile_img" ).get( 0 );
			var filename = $("#profile_img").val();
			var form_data = new FormData();
			var hash_tocken_id = $("#hash_tocken_id").val();
			if(filename!='')
			{
				form_data.append('profile_pic', $('input[name=profile_img]')[0].files[0]);	
			}
			
			form_data.append('user_agent', 'NI-WEB');
			form_data.append('csrf_job_portal', hash_tocken_id);
			if(filename!='')
			{
					$.ajax({
						url: "<?php echo $base_url.'employer_profile/uploadprofile' ?>", // Url to which the request is send
						type: "POST",             // Type of request to be send, called as method
						data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						dataType:"json",
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,        // To send DOMDocument or non processed data file it is set to false
						success: function(data)   // A function to be called if request succeeds
						{
							if(data.final_result.status=='success')	
							{
								$("#profile_img").val('');
								$("#blah").hide();
								$("#image_val").removeAttr("class").addClass("alert alert-success alert-dismissable fade in");
								$("#image_val").html('<div class="col-xs-10">'+'<?php echo $custom_lable_arr['sign_up_emp_profile_upload_succ']; ?>'+'</div><div class="col-xs-2"><a href="javascript:void(0)" class="close" onclick="hideid1()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
								$("#image_val").show().css("opacity","1");
								setTimeout(function() { $("#image_val").hide(); }, 10000);
								$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
								$(".headermenuprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								$(".headerprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								$("#headmenuprofilediv").attr('src','<?php echo $upload_path_profile; ?>'+'/'+data.final_result.file_data.file_name);
								
							}					
							if(data.final_result.status=='error')
							{
								hide_comm_mask();
								$("#profile_img").val('');
								//$("#imagediv").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
								$("#image_val").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
								$("#image_val").html('<div class="col-xs-11">'+data.final_result.error_message+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
								$("#image_val").show().css("opacity","1");
								setTimeout(function() { $("#image_val").hide(); }, 10000);
							}
							$("#hash_tocken_id").val(data.tocken);
							hide_comm_mask();
						}
						});
				}
return false;
}
function deleteuploadedprofile()
{
			show_comm_mask();
			var hash_tocken_id = $("#hash_tocken_id").val();
			var datastring = 'user_agent=NI-WEB&csrf_job_portal='+hash_tocken_id;
			$.ajax({
				url: "<?php echo $base_url.'employer_profile/deleteuploadprofile'; ?>", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: datastring, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				dataType:"json",
				//contentType: false,       // The content type used when sending data to the server.
				//cache: false,             // To unable request pages to be cached
				//processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.status=='success')	
					{
						$("#blah").hide();
						$("#image_val").removeAttr("class").addClass("alert alert-success alert-dismissable fade in");
						$("#image_val").html('<div class="col-xs-10">'+data.errormessage+'</div><div class="col-xs-2"><a href="javascript:void(0)" class="close" onclick="hideid1()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
						$("#image_val").show().css("opacity","1");
						setTimeout(function() { $("#image_val").hide(); }, 10000);
						$("#deleteprofile_btn").remove();
						$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
						$(".headermenuprofilediv").attr('src','<?php echo $upload_path_demomenu; ?>');
						$(".headerprofilediv").attr('src','<?php echo $upload_path_demoheader; ?>');
						$("#headmenuprofilediv").attr('src','<?php echo $upload_path_demoheader; ?>');
						
					}					
					if(data.status=='error')
					{
						hide_comm_mask();
						//$("#imagediv").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
						$("#image_val").removeAttr("class").attr('class', 'alert alert-danger alert-dismissable fade in');
						$("#image_val").html('<div class="col-xs-11">'+data.errormessage+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
						$("#image_val").show().css("opacity","1");
						setTimeout(function() { $("#image_val").hide(); }, 10000);
					}
					$("#hash_tocken_id").val(data.tocken);
					hide_comm_mask();
				}
				});
return false;
}
function deleteprofilepic()
{
	var confirm_resp = confirm("Are you sure you want delete your profile picture.");
	if(confirm_resp)
	{
		deleteuploadedprofile();
	}
	return false;
}
function validateformpersonal()
{
	$.validate({
    form : '#personal_det_form',
    modules : 'security',
	 onModulesLoaded : function() {
		//alert('All modules loaded!');
	 },
    onError : function($form) {
	  $("#success_msg2").hide();
	  $("#message2").focus();
	  $("#message2").slideDown();
	  $("#message2").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message2',-100);
	  
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message2").hide();
	  var datastring = $("#personal_det_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/updatepersonaldet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message2").slideUp();
				$("#success_msg2").slideDown();
				$("#success_msg2").html(data.successmessage);
				$("#checkupdated").val('one');
				scroll_to_div('success_msg2',-100);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			else
			{
				$("#success_msg2").slideUp();
				$("#message2").html(data.errormessage);
				$("#message2").slideDown();
				scroll_to_div('message2',-100);
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformcompany()
{
	$.validate({
    form : '#company_det_form',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_comp").hide();
      $("#message_comp").slideDown();
	  $("#message_comp").focus();
	  $("#message_comp").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message_comp',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_comp").hide();
	  //var datastring = $("#company_det_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  //var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	  var filename = $("#company_logo").val();
	  var form_data = new FormData();
	  var formDataArray = $("#company_det_form").serializeArray();
	  for(var i = 0; i < formDataArray.length; i++)
	  {
		var formDataItem = formDataArray[i];
		form_data.append(formDataItem.name, formDataItem.value);
	  }
	 	var fd = new FormData();
		var file_data = $('#company_logo')[0].files; // for multiple files
		//fd.append('company_logo', file_data.files[0]); 
		if(filename!='')
		{
			fd.append('company_logo', $('#company_logo')[0].files[0]); 	
		}
		else
		{
			//fd.append('company_logo', ''); 	
		}
		var other_data = $("#company_det_form").serializeArray();
		$.each(other_data,function(key,input){
			fd.append(input.name,input.value);
		});
	  fd.append('csrf_job_portal', hash_tocken_id);
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/updatecompanydet' ?>",
		type: 'post',
		data: fd,
		dataType:"json",
		contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
	    processData: false, // NEEDED, DON'T OMIT THIS
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_comp").slideUp();
				$("#success_msg_comp").slideDown();
				$("#success_msg_comp").html(data.successmessage);
				scroll_to_div('success_msg_comp',-100);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			else
			{
				$("#success_msg_comp").slideUp();
				$("#message_comp").html(data.errormessage);
				$("#message_comp").slideDown();
				scroll_to_div('message_comp',-100);
			}
		hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformwork()
{
	$.validate({
    form : '#work_exp_form',
    modules : 'security, file',
    onError : function($form) {
	  $("#success_msg_work").hide();
      $("#message_work").slideDown();
	  $("#message_work").focus();
	  $("#message_work").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message_work',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_work").hide();
	  var datastring = $("#work_exp_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/updatework_expdet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_work").slideUp();
				$("#success_msg_work").slideDown();
				$("#success_msg_work").html(data.successmessage);
				scroll_to_div('success_msg_work',-100);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			else
			{
				$("#success_msg_work").slideUp();
				$("#message_work").html(data.errormessage);
				$("#message_work").slideDown();
				scroll_to_div('message_work',-100);
			}
			hide_comm_mask();
		}
	});
      return false; // Will stop the submission of the form
    }
  });
}
function validateformsociallink()
{
	$.validate({
    form : '#editsociallinks',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_social").hide();
      $("#message_social").slideDown();
	  $("#message_social").focus();
	  $("#message_social").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message_social',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_social").hide();
	  var datastring = $("#editsociallinks").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/editsociallinks'; ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_social").slideUp();
				$("#success_msg_social").slideDown("slow", function() { scroll_to_div('success_msg_social',-100); });
				$("#success_msg_social").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				
			}
			else
			{
				$("#success_msg_social").slideUp();
				$("#message_social").html(data.errormessage);
				$("#message_social").slideDown("slow", function() { scroll_to_div('message_social',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformskillhire()
{
	$.validate({
    form : '#hire_form',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_skill").hide();
      $("#message_skill").slideDown();
	  $("#message_skill").focus();
	  $("#message_skill").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message_skill',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_skill").hide();
	  var datastring = $("#hire_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/updateskilldet' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_skill").slideUp();
				$("#success_msg_skill").slideDown();
				$("#success_msg_skill").html(data.successmessage);
				scroll_to_div('success_msg_skill',-100);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
			}
			else
			{
				$("#success_msg_skill").slideUp();
				$("#message_skill").html(data.errormessage);
				$("#message_skill").slideDown();
				scroll_to_div('message_skill',-100);
			}
			hide_comm_mask();	
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformpassword()
{
	$.validate({
    form : '#editpassword',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgpassword").hide();
      $("#messagepassword").slideDown();
	  $("#messagepassword").focus();
	  $("#messagepassword").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messagepassword").hide();
	  var datastring = $("#editpassword").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/editpassword' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messagepassword").slideUp();
				$("#success_msgpassword").slideDown("slow", function() { scroll_to_div('success_msgpassword',-100); });
				$("#success_msgpassword").html(data.successmessage);
				
			}
			else
			{
				$("#success_msgpassword").slideUp();
				$("#messagepassword").html(data.errormessage);
				$("#messagepassword").slideDown("slow", function() { scroll_to_div('messagepassword',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformdeleteprofile()
{
	$.validate({
    form : '#prodeleterequest',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgdelpro").hide();
      $("#messagedelpro").slideDown();
	  $("#messagedelpro").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('messagedelpro',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#messagedelpro").hide();
	  var datastring = $("#prodeleterequest").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/prodeleterequest' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#messagedelpro").slideUp();
				$("#success_msgdelpro").slideDown("slow", function() { scroll_to_div('success_msgdelpro',-100); });
				$("#success_msgdelpro").html(data.successmessage);
				$('#prodeleterequest')[0].reset();
			}
			else
			{
				$("#success_msgdelpro").slideUp();
				$("#messagedelpro").html(data.errormessage);
				$("#messagedelpro").slideDown("slow", function() { scroll_to_div('messagedelpro',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateformsociallink()
{
	$.validate({
    form : '#editsociallinks',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg_social").hide();
      $("#message_social").slideDown();
	  $("#message_social").focus();
	  $("#message_social").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message_social',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message_social").hide();
	  var datastring = $("#editsociallinks").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'employer_profile/editsociallinks'; ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message_social").slideUp();
				$("#success_msg_social").slideDown("slow", function() { scroll_to_div('success_msg_social',-100); });
				$("#success_msg_social").html(data.successmessage);
				/*for replacing view section*/
				if($.trim(data.contenttoreplace.returndata)!='' && $.trim(data.contenttoreplace.returndata)!='undefined' && $.trim(data.contenttoreplace.returndata)!=undefined && $.trim(data.contenttoreplace.returndata)!='null' && $.trim(data.contenttoreplace.returndata)!=undefined)
				{
					$("#profile").html(data.contenttoreplace.returndata);
				}
				/*for replacing view section*/
				//deleteresume();
				
			}
			else
			{
				$("#success_msg_social").slideUp();
				$("#message_social").html(data.errormessage);
				$("#message_social").slideDown("slow", function() { scroll_to_div('message_social',-100); });
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function checkifupdated()
{
	var updatevar = $("#checkupdated").val();
	if(updatevar == 'one')
	{
		return true;
	}
	else
	{
		return false;
	}
}
$("#company_logo").change(function() {
	if($("#company_logo").length > 0)
	{
		var myimage = $("#company_logo")[0];
		if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
		{
			document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
			$("#blah1").slideDown();
		}
		else
		{
			$("#blah1").hide();
			document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
			showmessage('<?php echo $custom_lable_arr['invalid_file_format']; ?>','10000');
		}
	}
});
$("#profile_img").change(function() {
	if($("#profile_img").length > 0)
	{
		var myimage = $("#profile_img")[0];
		if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
		{
			document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
			$("#blah").slideDown();
		}
		else
		{
			$("#blah").hide();
			document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
			/*$("#profile_img").val('');*/
			showmessage('Invalid image','10000');
		}
	}
});

</script>