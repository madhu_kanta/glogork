<?php $custom_lable_arr = $custom_lable->language;
$percentage_stored = $this->my_profile_model->getprofile_completeness($user_data_values['id']);
 ?>
<div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $percentage_stored; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_stored; ?>%">
                          Your Profile Complete - <?php echo $percentage_stored; ?> %
                        </div>
                    </div>