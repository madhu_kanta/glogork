<?php 	
if(isset($jobs_by_industry_data) && $jobs_by_industry_data!='' && is_array($jobs_by_industry_data) && count($jobs_by_industry_data) > 0)
{
	$unique_industry_id = 0;
	foreach($jobs_by_industry_data as $jobs_by_industry_data_single)
	{ 
		$custom_counter_fordiv = 0 ;
		$where_arra_ind_nm = array('id'=>$jobs_by_industry_data_single['job_industry'],'is_deleted'=>'No','status'=>'APPROVED');
		$industries_name = $this->common_front_model->get_count_data_manual('industries_master',$where_arra_ind_nm,1,'industries_name,id','','','','');
		if($industries_name!='' && is_array($industries_name) && count($industries_name) > 0 && $industries_name['industries_name']!='' && $jobs_by_industry_data_single['job_industry']!=$unique_industry_id)
		{
			$unique_industry_id = $jobs_by_industry_data_single['job_industry'];
		 ?>
	<div class="categories-group">
		<div class="container">
			<h4><?php echo $industries_name['industries_name']; ?></h4>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="list-group">
     				<?php 
					foreach($jobs_by_industry_data as $jobs_by_industry_data_single_fn)
					{
						if(is_array($jobs_by_industry_data_single_fn) && array_key_exists('job_industry',$jobs_by_industry_data_single_fn)  && array_key_exists('functional_area',$jobs_by_industry_data_single_fn)  && $jobs_by_industry_data_single_fn['functional_area']!='' && $jobs_by_industry_data_single_fn['job_industry']==$unique_industry_id)
						{
		$where_arra_fn_nm = array('id'=>$jobs_by_industry_data_single_fn['functional_area'],'is_deleted'=>'No','status'=>'APPROVED');
		$fnarea_name = $this->common_front_model->get_count_data_manual('functional_area_master',$where_arra_fn_nm,1,'functional_name,id','','','','');	
		if($fnarea_name!='' && is_array($fnarea_name) && count($fnarea_name) > 0 && $fnarea_name['functional_name']!='')
		{
			$custom_counter_fordiv = $custom_counter_fordiv+1;
					?>  
                    	<form method="post" id="text_search_form" action="<?php echo $base_url; ?>job-listing/posted-job">
                        <button type="submit" class="list-group-item" style="margin-bottom:-1px;"><?php echo $fnarea_name['functional_name']; ?><span class="badge"><?php echo $jobs_by_industry_data_single_fn['fnareacount']; ?></span></button>
						<!--<a href="javascript:void(0);" class="list-group-item" style="margin-bottom:-1px;"><?php //echo $fnarea_name['functional_name']; ?> <span class="badge">12</span></a>-->
                        <input type="hidden" name="search_fn_area[]" value="<?php echo $jobs_by_industry_data_single_fn['functional_area']; ?>">
                        <input type="hidden" name="search_industry[]" value="<?php echo $jobs_by_industry_data_single['job_industry']; ?>">
            			<input type="hidden" name="csrf_job_portal" value="<?php echo $this->security->get_csrf_hash(); ?>">
            			<input type="hidden" name="user_agent" value="NI-WEB">
                        </form>
                    <?php 
		if($custom_counter_fordiv > 2)
		{$custom_counter_fordiv=0; ?>
			</div>
			</div>
            <div class="col-md-4 col-sm-4 col-xs-12">
				<div class="list-group">
		<?php }//if for putting in different row
		}//if we get fn area in fn master table
						}
					}//innner foreach end ?>
				</div>
			</div>
		</div>
	</div>
<?php 
}//if only unique industry id
}//foreach end
 }
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>    
</div>    
<?php 
}
?>
<?php //echo $this->common_front_model->rander_pagination('browse_industries',$jobs_by_industry_count,$limit_per_page); ?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />