<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span> Personal Details </span> <a href="javascript:void(0);" class="btn btn-md pull-right foreditfontcolor " style="margin:-3px;" onClick="editsection('profile_snapshot');"><i class="fa fa-fw fa-edit" aria-hidden="true" ></i> Edit</a></div>
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<table class="table">
												<tbody>
													<tr>
                                                        <td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['reg_lbl_fullname']; ?> :</td>
                                                        <td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ? $user_data['personal_titles'].' '.$user_data['fullname'] : $custom_lable_arr['notavilablevar'] ; ?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Email Address :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['email']!='0' && $this->common_front_model->checkfieldnotnull($user_data['email'])) ? $user_data['email'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Resume Headline :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['resume_headline'])) ? $user_data['resume_headline'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Gender :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['gender'])) ? $user_data['gender'] : "Not Availalbe" ; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Marital Status :</td>
                                                        <?php $maritalstatusget = $this->my_profile_model->getdetailsfromid('marital_status_master','id',$user_data['marital_status'],'marital_status'); ?>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['marital_status']!='0' && $this->common_front_model->checkfieldnotnull($user_data['marital_status']) && count($maritalstatusget) > 0 && $maritalstatusget['marital_status']!='' ) ? $maritalstatusget['marital_status'] : "Not Availalbe"; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Mobile Number&nbsp;:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['mobile'])) ? $user_data['mobile'] : "Not Availalbe"; ?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Mobile Verified Status :</td>
														<td class="col-md-6 col-xs-6" id="mobile_verifired">
                                                        	
														<?php 
														if($user_data['mobile_verified'] =='Yes')
														{
															echo '<label class="alert alert-success" style="width:50px; font-weight:bold; margin-bottom: 0px; padding: 10px;float:left;text-align:center">Yes</label>';
														}
														else
														{
															echo '<label class="alert alert-danger" style="width:50px; font-weight:bold; margin-bottom: 0px; padding: 10px;;float:left;text-align:center">No</label>';
														?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <div class="visible-xs"><br></div>
                                                        <a href="#varify_mobile_pop_up" class="popup-with-zoom-anim " onClick="varify_mobile()">Verify Mobile Number</a>
                                                        <div id="varify_mobile_pop_up" class="zoom-anim-dialog mfp-hide apply-popup" style="width:450px;margin:0 auto;max-width:100%">
                                                            <div class="small-dialog-headline">
                                                                <i class="fa fa-mobile"></i> Verify Mobile 
                                                            </div>
                                                            <div class="small-dialog-content">
                                                            		<div class="alert alert-danger" style="display:none" id="error_message_mv"></div>
                                                                    <div class="alert alert-success" style="display:none" id="success_message_mv"></div>
                                                                    <div id="verify_mobile_cont">
                                                                    Enter Your OTP: <input type="text" placeholder="Enter OTP Sent On Your Mobile" value="" id="otp_mobile" name="otp_mobile" />
                                                                    <span id="resend_link" style="display:none"><a href="javascript:;" onClick="return varify_mobile()">Resend OTP</a></span>
                                                                    <button onClick="varify_mobile_check()" class="send"><span class="glyphicon glyphicon-phone"></span> Verify Mobile</button>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <?php
														}
														?>
                                                        </td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Landline :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['landline'])) ? $user_data['landline'] : "Not Availalbe"; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Age :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($user_data['birthdate'])) ? $this->my_profile_model->getagefrmbdate($user_data['birthdate']) : "Not Availalbe"; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Date of Birth :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($user_data['birthdate'])) ? $this->common_front_model->displayDate($user_data['birthdate'],'F j, Y') : "Not Availalbe"; ?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6">Country :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['country']!='0' && $this->common_front_model->checkfieldnotnull($user_data['country']) && $this->common_front_model->checkfieldnotnull($user_data['country_name'])) ? $user_data['country_name'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">City : </td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['city']!='0' && $this->common_front_model->checkfieldnotnull($user_data['city']) && $this->common_front_model->checkfieldnotnull($user_data['city_name'])) ? $user_data['city_name'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Permanent Address :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['address'])) ? $user_data['address'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['home_city']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['home_city']!='0' && $this->common_front_model->checkfieldnotnull($user_data['home_city'])) ? $user_data['home_city'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6">Pincode :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['pincode']!='0' && $this->common_front_model->checkfieldnotnull($user_data['pincode'])) ? $user_data['pincode'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<!--<tr>
														<td class="col-md-6 col-xs-6">Industry:</td>
														<td class="col-md-6 col-xs-6"><?php //echo ($user_data['industry']!='0' && $this->common_front_model->checkfieldnotnull($user_data['industry'])) ? $user_data['industry'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
													<!--<tr>
														<td class="col-md-6 col-xs-6">Functional Area:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($user_data['functional_area'])) ? $user_data['functional_area'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
													<!--<tr>
														<td class="col-md-6 col-xs-6">Role:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['job_role']!='0' && $this->common_front_model->checkfieldnotnull($user_data['job_role'])) ? $user_data['job_role'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
													<!--<tr>
														<td class="col-md-6 col-xs-6">Annual Salary:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? $user_data['annual_salary'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
													<!--<tr>
														<td class="col-md-6 col-xs-6">Total Experiance:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? $user_data['total_experience'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
													<!--<tr>
														<td class="col-md-6 col-xs-6">Skill:</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['key_skill']!='0' && $this->common_front_model->checkfieldnotnull($user_data['key_skill'])) ? $user_data['key_skill'] : $custom_lable_arr['notavilablevar'];?></td>
													</tr>-->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>