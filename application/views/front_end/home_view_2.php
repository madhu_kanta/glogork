<!-- Banner
================================================== -->
<div id="banner" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner-home-01.jpg)" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">
	<div class="container">
		<div class="sixteen columns">

			<div class="search-container">

				<!-- Form -->
				<h2>Find job</h2>
				<?php $this->load->view('front_end/search_box');?>

				<!-- Browse Jobs -->
				<div class="browse-jobs">
					Browse job offers by <a href="browse-categories.html"> category</a> or <a href="#">location</a>
				</div>

				<!-- Announce -->
				<div class="announce">
					We’ve over <strong>15 000</strong> job offers for you!
				</div>

			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Icon Boxes -->
<div class="section-background top-0">
	<div class="container">

		<div class="one-third column well box">
			<div class="icon-box rounded alt">
				<a href="add-resume.html"><i class="ln ln-icon-Folder-Add"></i>
				<h4>Add Resume</h4></a>
				<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
			</div>
		</div>

		<div class="one-third column well box">
			<div class="icon-box rounded alt">
				<a href="browse-jobs.html"><i class="ln ln-icon-Search-onCloud"></i>
				<h4>Search For Jobs</h4></a>
				<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
			</div>
		</div>

		<div class="one-third column well box">
			<div class="icon-box rounded alt">
				<a href="#"><i class="ln ln-icon-Business-ManWoman"></i>
				<h4>Find Crew</h4></a>
				<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
			</div>
		</div>

	</div>
</div>
<!-- Icon Boxes / End -->


<div class="container">

	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
		<h3 class="margin-bottom-25">Recent Jobs</h3>
		<ul class="job-list">

			<li class="highlighted"><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-01.png" alt="">
				<div class="job-list-content">
					<h4>Marketing Coordinator - SEO / SEM Experience <span class="full-time">Full-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> King</span>
						<span><i class="fa fa-map-marker"></i> Sydney</span>
						<span><i class="fa fa-money"></i> $100 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-02.png" alt="">
				<div class="job-list-content">
					<h4>Core PHP Developer for Site Maintenance <span class="part-time">Part-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Cubico</span>
						<span><i class="fa fa-map-marker"></i> London</span>
						<span><i class="fa fa-money"></i> $50 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-03.png" alt="">
				<div class="job-list-content">
					<h4>Restaurant Team Member - Crew <span class="full-time">Full-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> King</span>
						<span><i class="fa fa-map-marker"></i> Sydney</span>
						<span><i class="fa fa-money"></i> $15 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-04.png" alt="">
				<div class="job-list-content">
					<h4>Power Systems User Experience Designer  <span class="internship">Internship</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Hexagon</span>
						<span><i class="fa fa-map-marker"></i> London</span>
						<span><i class="fa fa-money"></i> $75 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-05.png" alt="">
				<div class="job-list-content">
					<h4>iPhone / Android Music App Development <span class="temporary">Temporary</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Mates</span>
						<span><i class="fa fa-map-marker"></i> New York</span>
						<span><i class="fa fa-money"></i> $115 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>
		</ul>

		<a href="browse-jobs.html" class="button centered"><i class="fa fa-plus-circle"></i> Show More Jobs</a>
		<div class="margin-bottom-55"></div>
	</div>
	</div>

	<!-- Job Spotlight -->
	<div class="five columns">
		<h3 class="margin-bottom-5">Job Spotlight</h3>

		<!-- Navigation -->
		<div class="showbiz-navigation">
			<div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
			<div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
		</div>
		<div class="clearfix"></div>

		<!-- Showbiz Container -->
		<div id="job-spotlight" class="showbiz-container">
			<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
				<div class="overflowholder">

					<ul>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Social Media: Advertising Coordinator <span class="part-time">Part-Time</span></h4></a>
								<span><i class="fa fa-briefcase"></i> Mates</span>
								<span><i class="fa fa-map-marker"></i> New York</span>
								<span><i class="fa fa-money"></i> $20 / hour</span>
								<p>As advertising & content coordinator, you will support our social media team in producing high quality social content for a range of media channels.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Prestashop / WooCommerce Product Listing <span class="freelance">Freelance</span></h4></a>
								<span><i class="fa fa-briefcase"></i> King</span>
								<span><i class="fa fa-map-marker"></i> London</span>
								<span><i class="fa fa-money"></i> $25 / hour</span>
								<p>Etiam suscipit tellus ante, sit amet hendrerit magna varius in. Pellentesque lorem quis enim venenatis pellentesque.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Logo Design <span class="freelance">Freelance</span></h4></a>
								<span><i class="fa fa-briefcase"></i> Hexagon</span>
								<span><i class="fa fa-map-marker"></i> Sydney</span>
								<span><i class="fa fa-money"></i> $10 / hour</span>
								<p>Proin ligula neque, pretium et ipsum eget, mattis commodo dolor. Etiam tincidunt libero quis commodo.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>


					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>
</div>


<!-- Testimonials -->
<div id="testimonials">
	<!-- Slider -->
	<div class="container">
		<div class="col-md-12 col-sm-12 col-xs-12">
                <div id="Carousel" class="carousel slide">
                <br><br>
                <!-- Carousel items -->
                <div class="carousel-inner">
					<div class="item active">
					 <ul class="row">
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/1.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/2.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/3.png" alt="Image" style="height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/4.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/5.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/6.jpg" alt="Image" style="height:80px;"></a></li>
					 </ul><!--.row-->
                </div><!--.item-->

                <div class="item">
					 <ul class="row">
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/7.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/17.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/9.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/10.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/11.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/12.jpg" alt="Image" style="max-height:80px;"></a></li>
					 </ul><!--.row-->
                </div><!--.item-->

                <div class="item">
					 <ul class="row">
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/13.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/14.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/15.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/16.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/1.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-lg-2 col-md-4 col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/2.jpg" alt="Image" style="height:80px;"></a></li>
                       </ul>
					 </div>
                </div>
			</div>
		</div>
		<br><br>
	</div>
</div>


<!-- Infobox -->
<div class="infobox margin-bottom-0">
	<div class="container">
		<div class="sixteen columns">Start Building Your Own Job Board Now <a href="my-account.html">Get Started</a></div>
	</div>
</div>



<script src="<?php echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>