<?php
$custom_lable_array = $custom_lable->language;
$key_skill = $this->common_front_model->get_list('key_skill_master','','','array','','1'); 
$key_skill_data = json_encode($key_skill); 

$employement_type = $this->common_front_model->get_list('employement_list','','','array','','1'); 
$employement_type_data = json_encode($employement_type);

$job_type_master = $this->common_front_model->get_list('job_type_list','','','array','','1'); 
$job_type_master_data = json_encode($job_type_master);  

$shift_list = $this->common_front_model->get_list('shift_list','','','array','','1'); 
$shift_list_data = json_encode($shift_list);
?>
<div id="titlebar" class="photo-bg single submit-page" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/add-resume.png);background-size:cover;">

<div class="container">
		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-file-text"></i><?php echo $custom_lable_array['add_resume_page_title']; ?></h2>
			<span>Add Resume and Find The Jobs</span>
		</div>

	</div>
</div>

<div class="container">
	
    
    <div class="sixteen columns">
		<div class="submit-page">
			<!-- Notice -->
			<div class="row notification notice margin-bottom-30 text-center" style="border:1px solid;">
				<div class="col-md-9 col-sm-12 col-xs-12">
					<p><i class="fa fa-2x fa-hand-o-right" aria-hidden="true"></i><span style="marfin-top:-5px;"> <a href="<?php echo $base_url; ?>sign-up/login"> <u><?php echo $custom_lable_array['account_ask_msg']; ?></u> </a> </span> <?php echo $custom_lable_array['account_msg']; ?></p>
				</div>
				
				<!--<div class="col-md-3 col-sm-12 col-xs-12">
					<a href="#" class="button linkedin-btn text-center">Import from LinkedIn</a>
				</div>-->
			</div>
		</div>
	</div>
	<hr>
</div>

<div class="container" id="for_scrool">
    <div class="row">
		<section>
			<div class="wizard">
				<div class="wizard-inner">
					<div class="connecting-line"></div>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo $custom_lable_array['personal_info_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo $custom_lable_array['edu_qualification_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-education"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?php  echo $custom_lable_array['work_experiance']; ?>">
								<span class="round-tab">
									<i class="fa fa-building" aria-hidden="true"></i>
								</span>
							</a>
						</li>
                       
						<li role="presentation" class="disabled">
							<a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="<?php  echo $custom_lable_array['js_details']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>	
						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="<?php echo $custom_lable_array['upload_resume_pro_pic']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-open-file"></i>
								</span>
							</a>
						</li>
                        <li role="presentation" class="disabled hidden">
							<a href="#complete_reg" data-toggle="tab" aria-controls="complete" role="tab">
								<span class="round-tab">
									<i class="glyphicon glyphicon-open-file"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>
				
				       
					<div class="tab-content" >
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                
                      
                        <div class="tab-pane active" role="tabpanel" id="step1">
                          <form class="well" method="post" id="personal_info_form" >
							<div class="step2">
								<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
									<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php echo $custom_lable_array['personal_info_lbl']; ?></strong></div>
									<div class="panel-body" style="background-color:">
										<!--<div class="row">
											<div class="margin-top-20"></div>
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>First Name<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="fname" type="text" class="form-control" name="first name" placeholder="First Name" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Last Name</h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="lname" type="text" class="form-control" name="last name" placeholder="Last Name" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>-->

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['gender_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
														<div id="radioBtn" class="btn-group">
															<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y" data-value='Male' style="font-size: 14px;padding: 5px 26px;"><?php echo $custom_lable_array['gender_lbl_male']; ?></a>
															<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N" data-value='Female' style="font-size: 14px;padding: 5px 24px;"><?php echo $custom_lable_array['gender_lbl_female']; ?></a>
														</div>
														<input type="hidden" name="gender" id="gender" value="Male">
													</div>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['m_status_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="form-group">
														<div id="radioBtn1" class="btn-group">
                                                        <?php 
														if(isset($marital_status) && $marital_status !='' && is_array($marital_status) && count($marital_status) > 0)
														{
														foreach ($marital_status as $m_status)
														{ ?>
                                                        <a class="btn btn-primary btn-sm <?php if($m_status['marital_status']=='Unmarried') {echo "active";} else {echo "notActive";}?>" data-toggle="happy1" data-title="<?php echo $m_status['id']; ?>" data-value="<?php echo  $m_status['id']; ?>" style="font-size: 14px;padding: 5px 15px;"><?php echo $m_status['marital_status']; ?></a>
														<?php }
														}
														?>
														</div>
														<input type="hidden" name="marital_status" id="m_status" value="3">
													</div>
												</div>
											</div>
										</div>

										<!--<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Age<span class="red-only"> *</span></h5>
														<select name="Age" id="Age" class="city" style="padding:10px;border-radius:0;">
															<option value="Age">Select Age</option>
															<option value="15 - 20">15 - 20</option>
															<option value="21 - 25">21 - 25</option>
															<option value="26 - 30">26 - 30</option>
															<option value="31 - 35">31 - 35</option>
															<option value="Above 35...">Above 35...</option>
														</select>
												</div>
											</div>
										</div>-->

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['dob_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                      <input type="text" name="birthdate" class="form-control datepicker" id="exampleInputDOB1" style="border-radius:0px;"data-date-format="yyyy-mm-dd" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['dob_lbl'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-7">
													<h5><?php echo $custom_lable_array['mobile_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="col-md-4 col-xs-3" style="margin-left:-15px;">
														<div class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                            <select  class="form-control " name="mobile_c_code" style="height:42px;" data-validation="required">
                                                            <?php
															if(isset($country_code) && $country_code !='' && is_array($country_code) && count($country_code) > 0)
															{
															foreach ($country_code as $code)
															{ ?>
																<option value="<?php echo $code['country_code'];?>"><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
															<?php }
															}
															?>
                                                            </select>
															<!--<input id="mno" type="text" class="form-control" name="last name" placeholder="+91" style="padding:9px 0 9px 10px;">-->
														</div>
													</div>
													<div class="col-md-7" style="margin-left:-15px;">
														<input id="mobile" type="text" class="form-control" name="mobile" placeholder="<?php echo $custom_lable_array['mobile_palce_h_lbl']; ?>" style="border-radius:0px;"data-validation="required,number" data-validation-optional="true">
													</div>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['country_lbl']; ?><span class="red-only"> *</span></h5>
														<select name="country" id="country" class="city form-control  ketan" style="padding:10px;border-radius:0;" data-validation-skipped='0' onChange="dropdownChange('country','city','city_list');" data-validation="required">
                                                        <?php echo $this->common_front_model->get_list('country_list','str'); ?>
														</select>
												</div>
											</div>
										</div>

										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['city_lbl']; ?><span class="red-only"> *</span></h5>
														<select name="city" id="city" class="city form-control" style="padding:10px;border-radius:0;" data-validation="required" >
															
														</select>
												</div>
											</div>
										</div>
                                     <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['pincode']; ?><span class="red-only"> *</span></h5>										<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="pincode" class="form-control" id="pincode" style="border-radius:0px;" data-validation="required,number" data-validation-optional="true" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['pincode'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>   
									<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												
                                                <div class="col-md-6">
                                                <div class="form-group">
													<h5><?php echo $custom_lable_array['address_lbl']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                        <textarea style="border-radius:0px;"rows="2" cols="2" name="address" class="form-control" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['address_lbl'];  ?>"></textarea>
													</div>
												</div>
                                                </div>
											</div>
										</div>
										<!--<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Nationality<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
														<input id="nationality" type="text" class="form-control" name="last name" placeholder="Nationality" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Email address<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
														<input id="emailid" type="text" class="form-control" name="first name" placeholder="abc@gmail.com" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>
										<div class="margin-top-20"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5>Conform Email address<span class="red-only"> *</span></h5>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
														<input id="cemailid" type="text" class="form-control" name="first name" placeholder="abc@gmail.com" style="padding:9px 0 9px 10px;">
													</div>
												</div>
											</div>
										</div>-->
									</div>
								</div>
							</div>

							<div class="margin-top-20"></div>
							<ul class="list-inline pull-right">
								<li><input type="submit" class="btn btn-primary next-step" value="<?php  echo $custom_lable_array['save_continue_lbl']; ?>"></li>
							</ul>
                            
                            <input name="action" type="hidden"  value="add_personal_detail" />
                            <input name="user_agent" type="hidden"  value="NI-WEB" /> 
                           <div class="clearfix"></div>
					        </form> 
						</div>
                        <div class="tab-pane active" role="tabpanel" id="step2">
                          <form class="well" method="post" id="education_form">
							<div class="step2">
								<!--style="background-color:#337AB7;"-->
								<div class="panel panel-default">
									<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php echo $custom_lable_array['edu_qualification_lbl']; ?></strong></div>
									<div class="panel-body">
										
                                        <?php // echo  $this->sign_up_model->edu_qualification_html(); ?>
										
                                        <div id="education_fields">

										</div>
                                        
										<div class="margin-top-10"></div>
										<div class="clear"></div>
										<div class="margin-top-10"></div>
										<div class="input-group-btn text-center " id="add_more_edu_detail">
											<button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_array['more_edu_btn_lbl']; ?></button>
											<div class="margin-bottom-20"></div>
										</div>
                                        
                                        <div id="certificate_fields">

										</div>
                                        
                                        <div class="input-group-btn text-center " id="add_more_certi_detail">
											<button class="btn btn-success" type="button"  onclick="certificate_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_array['add_cerificate']; ?></button>										<div class="margin-bottom-20"></div>
										</div>
                                        
										
										<div class="form with-line">
											<h5><?php echo $custom_lable_array['url(s)']; ?><span>(<?php echo $custom_lable_array['optional_lbl']; ?>)</span></h5>
											<div class="form-inside">

												<!-- Adding URL(s) -->
												<div class="form boxed box-to-clone url-box" id="social_option1">
													<a href="#" onClick="remove_box();" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                                    <div class="row">
											      <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><span class="red-only"> *</span></h5>
														<select  id="social_val_option1" class="city form-control"  style="padding:10px;border-radius:0;" data-validation="required" onChange="create_social_name(this.value,'social_val_option1');" > 
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													  <option value="facebook_url" ><?php echo $custom_lable_array['facebook']; ?></option>
                                                      <option value="gplus_url" ><?php echo $custom_lable_array['gplus']; ?></option>
                                                      <option value="twitter_url" ><?php echo $custom_lable_array['twitter']; ?></option>
                                                      <option value="linkedin_url" ><?php echo $custom_lable_array['linkedin']; ?></option>
														</select>
												</div>
											</div>
										         </div>
									               <div class="margin-top-20"></div>
                                                     
									              <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url']; ?><span class="red-only"> *</span></h5>
													
                                                    <input class=" form-control" type="text"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url'];  ?>" data-validation-optional="true"  data-validation="url" value="" id="social_val_option1_name"/ style="">	
												</div>
											</div>
										</div>
                                        <div class="margin-top-20"></div>
                                        
                                                    
                                                    
												</div>
                                                
                                               <div class="form boxed box-to-clone url-box" id="social_option2">
													<a href="#" onClick="remove_box();" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                                    <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><span class="red-only"> *</span></h5>
													<select  id="social_val_option2" class="city form-control" style="padding:10px;border-radius:0;"  data-validation-optional="true" data-validation="required" onChange="create_social_name(this.value,'social_val_option2');">
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													  <option value="facebook_url" ><?php echo $custom_lable_array['facebook']; ?></option>
                                                      <option value="gplus_url" ><?php echo $custom_lable_array['gplus']; ?></option>
                                                      <option value="twitter_url" ><?php echo $custom_lable_array['twitter']; ?></option>
                                                      <option value="linkedin_url" ><?php echo $custom_lable_array['linkedin']; ?></option>
														</select>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                                <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url']; ?><span class="red-only"> *</span></h5>
													
                                                    <input class=" form-control" type="text"   data-validation="url" value="" id="social_val_option2_name" style=""  data-validation-optional="true" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url'];  ?>"/>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                        
                                                    
                                                    
												</div> 
                                                
                                               <div class="form boxed box-to-clone url-box" id="social_option3">
													<a href="#" onClick="remove_box();" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                                    <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><span class="red-only"> *</span></h5>
														<select  onChange="create_social_name(this.value,'social_val_option3');" id="social_val_option3" class="city form-control" style="padding:10px;border-radius:0;" data-validation="required" >
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													 <option value="facebook_url" ><?php echo $custom_lable_array['facebook']; ?></option>
                                                      <option value="gplus_url" ><?php echo $custom_lable_array['gplus']; ?></option>
                                                      <option value="twitter_url" ><?php echo $custom_lable_array['twitter']; ?></option>
                                                      <option value="linkedin_url" ><?php echo $custom_lable_array['linkedin']; ?></option>
														</select>
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                                     <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url']; ?><span class="red-only"> *</span></h5>
													
                                                    <input class=" form-control" style="" type="text" id="social_val_option3_name" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url'];  ?>"  data-validation="url" data-validation-optional="true" value=""/>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                        
                                                    
                                                    
												</div>     
                                                
                                                <div class="form boxed box-to-clone url-box" id="social_option4">
													<a href="#" onClick="remove_box();" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                                    <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_name']; ?><span class="red-only"> *</span></h5>
														<select onChange="create_social_name(this.value,'social_val_option4');" id="social_val_option4" class="city form-control" style="padding:10px;border-radius:0;" data-validation="required" >
                                                        <option value=""><?php echo $custom_lable_array['select_option']; ?></option>
													 <option value="facebook_url" ><?php echo $custom_lable_array['facebook']; ?></option>
                                                      <option value="gplus_url" ><?php echo $custom_lable_array['gplus']; ?></option>
                                                      <option value="twitter_url" ><?php echo $custom_lable_array['twitter']; ?></option>
                                                      <option value="linkedin_url" ><?php echo $custom_lable_array['linkedin']; ?></option>
														</select>
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                                     <div class="row">
											    <div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['social_url']; ?><span class="red-only"> *</span></h5>
													
                                                    <input class=" form-control" type="text" id="social_val_option4_name" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['social_url'];  ?>" style=""  data-validation="url" data-validation-optional="true" value=""/>	
												</div>
											</div>
										</div>
									               <div class="margin-top-20"></div>
                                        
                                                    
                                                    
												</div>
                                                
                                              <div class="margin-top-10"></div>  
												
											<a href="#" class="button gray add-url " onClick="add_social_url_option();" style="border:2px solid;"><i class="fa fa-link" aria-hidden="true" style="font-size:20px;"></i><?php echo $custom_lable_array['add_url']; ?> </a>	<p class="note bg-info"><?php echo $custom_lable_array['add_url_msg']; ?> </p>
											
										 </div>
											</div>
										</div>
									</div>
								<div class="panel-footer"><small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php echo $custom_lable_array['another_field']; ?>  :)</small>, <small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-minus gs"></span> <?php echo $custom_lable_array['remove_field']; ?>  :)</small>
									</div>
							
								<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-default prev-step"> <?php  echo $custom_lable_array['previous']; ?></button></li>
								<li><input type="submit" class="btn btn-primary next-step" value="<?php  echo $custom_lable_array['save_continue_lbl']; ?>"/></li>
							</ul>
                            <div class="clearfix"></div>
                          <input name="no_of_edu_row" id="no_of_edu_row" type="hidden"  value="0" />
                          <input name="no_of_certi_row" id="no_of_certi_row" type="hidden"  value="0" />
                          <input name="add_url_list_row" id="add_url_list_row" type="hidden"  value="0" />
                          <input name="action" type="hidden"  value="add_education_detail" />
                           <input name="user_agent" type="hidden"  value="NI-WEB" />
                           </div>  
                        </form>   
						</div>
                       <div class="tab-pane active" role="tabpanel" id="step3">
                          <form class="well" method="post" id="work_exp_form">
							<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
								<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php  echo $custom_lable_array['work_experiance']; ?></strong></div>
                            
								<div class="panel-body work-exp-html">
									
								<?php //echo  $this->sign_up_model->work_exp_html(); ?>
                                
                                <div id="work_fields">
								  </div>	
                                    
                                <div class="margin-top-10"></div>
									<div class="input-group-btn text-center" id="add_more_work_exp">
										<button class="btn btn-success" type="button"  onclick="work_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php echo $custom_lable_array['more_exp'] ?></button>
									</div>
                                    	
								</div>
								
                                <!--<div class="margin-top-30"></div>
									<div class="col-sm-4 nopadding">
										<h6><?php  echo $custom_lable_array['notice_period']; ?><span class="red-only"> *</span></h6>
									    <div class="form-group">
											<input type="text" class="form-control" id="NoticePeriod"  name="NoticePeriod[]" value="" placeholder="Days and months"  style="border-radius:0px;"/>
									    </div>
									</div>-->
                                    
                                  
									<div class="margin-bottom-20"></div>
									<div class="panel-footer"><small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php echo $custom_lable_array['another_field']; ?>:)</small>, <small><?php echo $custom_lable_array['press']; ?> <span class="glyphicon glyphicon-minus gs"></span> <?php echo $custom_lable_array['remove_field']; ?>  :)</small></div>
									
									<ul class="list-inline pull-right margin-top-20">
									<li><button type="button" class="btn btn-default prev-step"><?php echo $custom_lable_array['previous']; ?></button></li>	
										<li><button type="button" id="skip_work_exp" class="btn btn-default next-step"><?php echo $custom_lable_array['skip']; ?></button></li>
										<li><input type="submit" class="btn btn-primary btn-info-full next-step" value="<?php echo $custom_lable_array['save_continue_lbl']; ?>"> </li>
									</ul>
								
                       <input name="no_of_work_exp_row" id="no_of_work_exp_row" type="hidden"  value="0" />
                       <input name="action" type="hidden"  value="add_work_exp_detail" />
                       <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </div>
                               <div class="clear"></div>
                        </form>
                        </div>
					    <div class="tab-pane active" role="tabpanel" id="step4">
						  <form class="well" method="post" id="js_details">
							<div class="panel panel-default"> <!--style="background-color:#337AB7;"-->
								<div class="panel-heading th_bgcolor" ><strong style="color:white;"><?php  echo $custom_lable_array['js_details']; ?></strong></div>
                            
								<div class="panel-body work-exp-html">
								
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['home_city']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="home_city" class="form-control tokenfieldwidth" id="home_city" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['home_city'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                
                                       
                               <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['landline']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="landline" class="form-control" id="landline" style="border-radius:0px;" data-validation="number" data-validation-optional="true" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['landline'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['website']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="website" class="form-control" id="website" style="border-radius:0px;"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['website'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['profile_summary']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <textarea style="border-radius:0px;"rows="2" cols="2" name="profile_summary" class="form-control" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['profile_summary'];  ?>"></textarea>
													</div>
                                                    </div>
												</div>
											</div>
										</div>                            
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['resume_headline']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <textarea style="border-radius:0px;"rows="2" cols="2" name="resume_headline" class="form-control" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['resume_headline'];  ?>" ></textarea>
													</div>
                                                    </div>
												</div>
											</div>
										</div>  
                                
                                
                                <div class="row">
                                     <div class="col-md-12">
										<div class="col-md-3"></div>
                                       <div class="col-sm-6 nopadding">
									  <h6><?php echo $custom_lable_array['total_experience'] ?></h6>
									  <div class="form-group">
									  
									  <div class="col-md-6 col-sm-6 nopadding" style="padding-left:0px;">
                                      <!--onChange="check_user_exp_or_fresher();"-->
                                        <select name="exp_year" id="exp_year"  class="form-control" style="border-radius:0px;" >
											<option value=""><?php echo $custom_lable_array['year'] ?></option>
                                           <?php
											for($i=0;$i <= 25 ; $i++)
											{ ?>
												<option value="<?php echo  $i; ?>" ><?php echo  $i; ?> </option>	
										<?php	}
											?>
										</select>
                                        </div>
									 <div class="col-md-6 col-sm-6 nopadding" style="padding-right:0px;">
                                        <select  name="exp_month"  id="exp_month"  class="form-control" style="border-radius:0px;" onChange="check_user_exp_or_fresher();" >
										    <option value=""><?php echo $custom_lable_array['month'] ?></option>
											 <?php
											for($i=0;$i <= 11 ; $i++)
											{ ?>
												<option value="<?php echo  $i; ?>" ><?php echo  $i; ?> </option>	
										<?php	}
											?>
										</select>
                                        </div>
                                        
									  </div>
									</div>
                                     </div>
                                   </div>
         				         	
                                <div class="row" id="annual_salary_div">
                                     <div class="col-md-12 ">
										<div class="col-md-3"></div>
                                       <div class="col-sm-6 nopadding">
									  <h6><?php echo $custom_lable_array['annual_salary'] ?></h6>
									  <div class="form-group">
									  
									  <div class="col-sm-4 nopadding" style="padding-left:0px;">
                                        <select name="currency_type"  class="form-control" >
											<?php echo  $this->common_front_model->get_list('currency_master','str'); ?>
										</select>
                                        </div>
									  
										<div class="col-sm-4 nopadding">
                                        <select name="a_salary_lacs"  class="form-control" >
										    <option value=""><?php echo $custom_lable_array['lacs'] ?></option>
											 <?php 
                                             for($i=0;$i <= 99 ; $i++)
											 { ?>
												 <option value="<?php echo $i ?>"><?php echo $i ?> </option>
											 <?php } ?>
										</select>
                                        </div>
                                        
                                        <div class="col-sm-4 nopadding" style="padding-right:0px;">
                                        <select name="a_salary_thousand" class="form-control" >
										    <option value=""><?php echo $custom_lable_array['thousand'] ?></option>
											 <?php
											 for($i=0;$i <= 90;)
											 {  ?>
												  <option value="<?php echo $i ?>"><?php echo $i ?> </option>
											<?php $i = $i+5; }
											 ?>
										</select>
                                        </div>
                                        
									  </div>
									</div>
                                     </div>
                                   </div> 
                                      	
                                <div class="row" id="industry_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['type_industry']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="industry" id="industry" data-validation="required" class="form-control" >
                                                     <?php echo  $this->common_front_model->get_list('industries_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row" id="functional_area_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['functional_area']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select onChange="dropdownChange('functional_area','job_role','role_master');" name="functional_area" id="functional_area" data-validation="required" class="form-control" >
                                                     <?php echo  $this->common_front_model->get_list('functional_area_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row" id="job_role_div">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_role']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="job_role" id="job_role"  class="form-control" >
                                                     <?php // echo  $this->common_front_model->get_list('role_master','str'); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div> 
                              <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['preferred_city']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="preferred_city" class="form-control tokenfieldwidth" id="preferred_city" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['preferred_city'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>             
                             <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['skill']; ?><span class="red-only"> *</span></h5>
													<div class="input-group bs-example">
													  <span class="input-group-addon"></span>
                                                       <input type="text" name="key_skill" class="form-control tokenfieldwidth" id="key_skill" style="border-radius:0px;" data-validation="required"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['skill'];  ?>">
                                                     
													</div>
                                                    </div>
												</div>
											</div>
										</div>   
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['desire_job_type']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="desire_job_type" class="form-control tokenfieldwidth" id="desire_job_type" style="border-radius:0px;" data-validation="required"   placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['desire_job_type'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['employment_type']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="employment_type" class="form-control tokenfieldwidth" id="employment_type" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['employment_type'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                              <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['prefered_shift']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                     <input type="text" name="prefered_shift" class="form-control" id="prefered_shift" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['prefered_shift'];  ?>">
                                                </div>
                                                </div>
												</div>
											</div>
										</div>                                          
                                   
                                   
                                   <div class="row" >
                                     <div class="col-md-12">
										<div class="col-md-3"></div>
										<div class="col-sm-6">
										  <h6><?php echo $custom_lable_array['expected_annual_salary'] ?><span class="red-only"> *</span></h6>
										 
											<div class="col-sm-4 nopadding" style="padding-left:0px;">
											<select name="exp_salary_currency_type" data-validation="required" class="form-control" >
												<?php echo  $this->common_front_model->get_list('currency_master','str'); ?>
											</select>
											</div>
										  
											<div class="col-sm-4 nopadding">
											<select name="exp_salary_lacs" data-validation="required" class="form-control" >
												<option value=""><?php echo $custom_lable_array['lacs'] ?></option>
												 <?php 
												 for($i=0;$i <= 99 ; $i++)
												 { ?>
													 <option value="<?php echo $i ?>"><?php echo $i ?> </option>
												 <?php } ?>
											</select>
											</div>
											
											<div class="col-sm-4 nopadding" style="padding-right:0px;">
											<select name="exp_salary_thousand" data-validation="required" class="form-control" >
												<option value=""><?php echo $custom_lable_array['thousand'] ?></option>
												 <?php
												 for($i=0;$i <= 90;)
												 {?>
													  <option value="<?php echo $i ?>"><?php echo $i ?> </option>
												<?php  $i = $i+5; }
												 ?>
											</select>
											</div>
                                        
										</div>
                                     </div>
                                   </div>  
                                         
                                <div class="margin-top-10"></div>
									<!--<div class="input-group-btn text-center">
										<button class="btn btn-success" type="button"  onclick="work_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php echo $custom_lable_array['more_exp'] ?></button>
									</div>-->	
								</div>
								
                                
									<div class="margin-bottom-20"></div>
									<!--<div class="panel-footer"><small><?php echo $custom_lable_array['press']; ?>  <span class="glyphicon glyphicon-plus gs"></span> <?php echo $custom_lable_array['another_field']; ?>:)</small>, <small><?php echo $custom_lable_array['press']; ?> <span class="glyphicon glyphicon-minus gs"></span> <?php echo $custom_lable_array['remove_field']; ?>  :)</small></div>-->
									
									<ul class="list-inline pull-right margin-top-20">
									<li><button type="button" class="btn btn-default prev-step"><?php echo $custom_lable_array['previous']; ?></button></li>	
										<!--<li><button type="button" id="skip_work_exp" class="btn btn-default next-step"><?php echo $custom_lable_array['skip']; ?></button></li>-->
										<li><input type="submit" class="btn btn-primary btn-info-full next-step" value="<?php echo $custom_lable_array['save_continue_lbl']; ?>"> </li>
									</ul>
								
                       <input name="action" type="hidden"  value="js_details" />
                       <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </div>
                               <div class="clear"></div>
                        </form>
						</div>
                        <div class="margin-top-20"></div>
						<div class="tab-pane active" role="tabpanel" id="complete">
                           
                        	<div class="step44">
								<h5><?php echo $custom_lable_array['upload_resume_pro_pic']; ?></h5>
								<hr>
								<div id="accordion-container">
								<form id="js_resume_upload" method="post" enctype="multipart/form-data" >	
                                    <h2 class="accordion-header th_bgcolor"><i class="fa fa-2x fa-cloud-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload_resume']; ?></h2>
									<div class="accordion-content">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="exampleInputFile"><?php echo $custom_lable_array['upload_resume']; ?></label>
													<input type="file" class="form-control-file"  aria-describedby="fileHelp" name="resume_file" id="resume_file" data-validation="required,extension,size"  data-validation-max-size="2M" data-validation-allowing="pdf, docx, doc, rtf, txt" > <!---->
													<small id="fileHelp" class="form-text text-muted red-only"><i> <?php echo $custom_lable_array['upload_resume_info']; ?>  </i></small>
												</div>

												<div class="margin-top-10"></div>
												<button  class="btn btn-md  th_bgcolor"><i class="fa fa-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload']; ?></button>
												<div class="margin-top-20"></div>

												<div class="panel panel-info">
													<div class="panel-heading">
														<h4 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_array['imp_notes']; ?></h4>
													</div>
													<div class="panel-body">
														<ul>
															<li><?php echo $custom_lable_array['upload_size_info']; ?></li>
															<li><?php echo $custom_lable_array['upload_img_frmt_info']; ?> </li>
															
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
                                    <input type="hidden" name="action" value="upload_img" >
                                    <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </form>
								<form id="js_img_upload" method="post" enctype="multipart/form-data" >	
                                	<h2 class="accordion-header th_bgcolor"><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></i> <?php echo $custom_lable_array['upload_pro_pic']; ?></h2>
									<div class="accordion-content">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="exampleInputFile"><?php echo $custom_lable_array['upload_pro_pic']; ?></label>
													<img src="<?php echo $base_url; ?>assets/front_end/images/demoprofileimge.png" id="blah" class="blah view_preview_img" alt="your image" />
													<input type="file" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" name="profile_pic" id="profile_pic" data-validation="required,extension,size"  data-validation-max-size="2M" data-validation-allowing="jpg, png, jpeg, gif " > <!---->
													<small id="file" class="form-text text-muted red-only"><i> <?php echo $custom_lable_array['upload_proc_pic_info']; ?></i></small>
												</div>

												<div class="margin-top-10"></div>
												<button type="submit"  class="btn btn-md th_bgcolor"><i class="fa fa-upload" aria-hidden="true"></i> <?php echo $custom_lable_array['upload']; ?></button>

												<div class="margin-top-10"></div>
												<div class="panel panel-info">
														<div class="panel-heading">
															<h3 class="panel-title" style="margin:-10px 0px;"><?php echo $custom_lable_array['imp_notes']; ?></h3>
														</div>
														<div class="panel-body">
															<ul>
																<li><?php echo $custom_lable_array['upload_size_info']; ?></li>  															<li><?php echo $custom_lable_array['upload_pro_img_frmt_info']; ?></li>
																
															</ul>
														</div>
												</div>
											</div>
										</div>
									</div>
                                 <input type="hidden" name="action" value="upload_img" >
                                    <input name="user_agent" type="hidden"  value="NI-WEB" />
                                </form>    
								</div>
							</div>
							<div class="margin-top-20"></div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-default prev-step"><?php echo $custom_lable_array['previous']; ?></button></li>
								<li><button type="button" class="btn btn-default next-step skip_photo_upload"><?php echo $custom_lable_array['skip']; ?></button></li>
								<li><button  class="btn th_bgcolor btn-info-full next-step skip_photo_upload" ><?php echo $custom_lable_array['finish']; ?></button></li>
							</ul>
                        	 <!--<div class="step44">
								<div class="panel panel-success">
								  <div class="panel-heading">Upload Completed</div>
								  <div class="panel-body">Resume Upload Successfully...</div>
								</div>
							</div>-->
							<div class="divider margin-top-0 padding-reset"></div>
							<!--<a href="#" class="button big margin-top-5"><?php echo $custom_lable_array['preview']; ?> <i class="fa fa-arrow-circle-right"></i></a>-->
                            
						</div>
                      
                      <div class="tab-pane active" role="tabpanel" id="complete_reg">
                        <div class="step44">
                            <div class="panel panel-success">
                              <div class="panel-heading"> <?php echo $custom_lable_array['sign_up_js_complete']; ?>! </div>
                              <div class="panel-body"><?php echo $custom_lable_array['sign_up_js_complete_mes']; ?></div>
                            </div>
					   </div>
                      </div>

						<div class="clearfix"></div>
					</div>
			</div>
            <input type="hidden" name="remove_previous_disbled" id="remove_previous_disbled"> 
            <input type="hidden" name="store_selected_optgroup" id="store_selected_optgroup"> 
		</section>
   </div>
</div>
<link href="<?php echo $base_url;?>assets/front_end/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">



<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>-->
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8">
</script>
<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-datepicker_for_conf.min.js"></script>
<script>


function time_out_message()
{
	settimeout_div('success_msg',8000);
	settimeout_div('error_msg',8000);
}

/*function check_user_exp_or_fresher()
{ 
	var exp_year = $('#exp_year').val();
    var exp_month = $('#exp_month').val();
	if( exp_year > 0 || exp_month > 0 )
	{
		$('#industry_div').slideDown();
		$('#functional_area_div').slideDown();
		$('#job_role_div').slideDown();
		$('#annual_salary_div').slideDown();
	}
	else
	{
		$('#industry_div').slideUp();
		$('#functional_area_div').slideUp();
		$('#job_role_div').slideUp();
		$('#annual_salary_div').slideUp();
		$('#functional_area').val('');
		$('#job_role').val('');
		$('#a_salary_lacs').val('');
		$('#a_salary_thousand').val('');
	}
	
}*/

$('#social_option1,#social_option2,#social_option3,#social_option4,#step4,#step2,#step3').on('change', function () {
	$('.chosen-select', this).chosen();
});

$('#social_option1,#social_option2,#social_option3,#social_option4,#step4,#step2,#step3').on('change', function () {
	//$('.chosen-select', this).chosen('destroy').chosen();
});




$('#key_skill').tokenfield({
    autocomplete: {
    source: <?php echo $key_skill_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#key_skill').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

$('#employment_type').tokenfield({
    autocomplete: {
    source: <?php echo $employement_type_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#employment_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

$('#desire_job_type').tokenfield({
    autocomplete: {
    source: <?php echo $job_type_master_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#desire_job_type').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

$('#prefered_shift').tokenfield({
    autocomplete: {
    source: <?php echo $shift_list_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#prefered_shift').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

/*$('#home_city').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#home_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
	 if (existingTokens != '')
	 {
		 event.preventDefault();
	 }
});  */

$('#preferred_city').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/city_name/"+request.term, {
          }, function (data) {
             response(data);
          });
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#preferred_city').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});



$('#skip_work_exp').click(function()
{
	for_active_tab();
	scroll_to_div('for_scrool');
});

$('.skip_photo_upload').click(function()
{
	scroll_to_div('for_scrool');
	$("#success_msg").slideDown();
	$('#complete').slideUp();
	$("#complete_reg").slideDown();
	$("#success_msg").html('<?php echo $custom_lable_array['register_success']; ?>');
});



function create_social_name(val,social_id)
{   
	$('#'+social_id).attr('name',val);
	$('#'+social_id+'_name').attr('name',"name_"+val);
}
var icon = 0
function add_social_url_option()
{ icon++;
   var new_add_url = parseInt($('#add_url_list_row').val());
	
	if(new_add_url == 0)
	{
		var new_add_url = 1 ;
		var field_row = new_add_url ;
	}
	else
	{
		var field_row = new_add_url + parseInt(1) ;
	}
	if(new_add_url < 5)
	{  
		$('#social_option'+field_row).slideDown();
		scroll_to_div('social_option'+icon);
		$('#social_option1,#social_option2,#social_option3,#social_option4').trigger('change');
	}
	
    
    $('#add_url_list_row').val(parseInt(field_row));
}

function remove_box()
{
	var url_val = parseInt($('#add_url_list_row').val());
    var new_val_url = url_val - parseInt(1) ;
	$('#add_url_list_row').val(parseInt(new_val_url));
	//alert(new_val_url);
}

	
var field_row = $('#no_of_edu_row').val();
function education_fields() 
{ 

	var edu_val = parseInt($('#no_of_edu_row').val());
	
	if(edu_val == 0)
	{
		var edu_val = 1 ;
		var field_row = edu_val ;
	}
	else
	{
		var field_row = edu_val + parseInt(1) ;
	}
    
    $('#no_of_edu_row').val(parseInt(field_row));
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+field_row);
	var rdiv = 'removeclass'+field_row;
	
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+field_row;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/edu_qualification_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#no_of_edu_row').val(field_row);
			divtest.innerHTML = data ;
			objTo.appendChild(divtest)
			if(field_row == 10)
			{
				$('#add_more_edu_detail').slideUp();
			}
			//$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 
			 });*/
			
			
			
				 for(i=1;i <= field_row ; i++)
				 {   
					
					
					$('.qualification_lvl'+i).change(function(){
						
						for(k=1;k <= field_row ; k++)
						{
							var new_k = k;
							var check_val = $('#qualification_level'+new_k).val();
							//alert(check_val);
							if(check_val == 'class-X')
							{   
								$('.specialization'+new_k).hide();
							}
							else
							{  
								$('.specialization'+new_k).show();
							}
							if(check_val == 'class-XII')
							{   
								$('#specialization'+new_k).attr('data-validation-skipped','1');
								$('#specialization'+new_k).removeAttr('data-validation','required');
								$('.red-only-sp'+new_k).hide();
							}
							else
							{  
								$('#specialization'+new_k).removeAttr('data-validation-skipped','1');
								$('#specialization'+new_k).attr('data-validation','required');
								$('.red-only-sp'+new_k).show();
							}
						}
						
						});
					
					 var selected = $(':selected', $('.qualification_lvl'+i));
					 var selectedtst = $(':selected', $('.qualification_lvl'+i)).val();
					 var selected_group =selected.closest('optgroup').attr('class');
					 var selected_option = $('option:selected', $('.qualification_lvl'+i));
					 var selected_option_val = selected_option.closest('select').attr('id');
					   for(s=1;s<= field_row ; s++)
						 { 
							 rr = s + parseInt(1);
							 
							 if(selected_option_val!='')
							 {
								$('.qualification_lvl'+rr+' option[value='+selected_option_val+']').attr('disabled','disabled').trigger("chosen:updated");
							 }
							 if(selected_group != 'class-X/XII')
							 {
							  
							   $('.qualification_lvl'+s+' optgroup[class='+selected_group+']').attr('disabled','disabled').trigger("chosen:updated");
							 }
							 
						 }
						 
						 if($('#'+selected_option_val).val()!='')
						 {
							 var selected_new_opt = $(':selected', $('#'+selected_option_val));
							 var selected_group_opt =selected_new_opt.closest('optgroup').attr('class');
							 if(selected_group != 'class-X/XII')
							 {
							 $('#'+selected_option_val+' optgroup[class='+selected_group+']').removeAttr('disabled').trigger("chosen:updated");
							 }
						 }
				}
			}
			
		
		});
}
function remove_education_fields(rid) {
	//$('.'+rid).remove();
   var edu_val = parseInt($('#no_of_edu_row').val());
   var new_val = edu_val - parseInt(1) ;
   $('#no_of_edu_row').val(parseInt(new_val));
   $('.removeclass'+edu_val).remove();
   if(new_val < 10)
	{
		$('#add_more_edu_detail').slideDown();
	}
}

var certi_row = $('#no_of_certi_row').val();
function certificate_fields() 
{ 

	var certi_val = parseInt($('#no_of_certi_row').val());
	if(certi_val == 0)
	{
		var certi_val = 1 ;
		var field_row = certi_val ;
	}
	else
	{
		var field_row = certi_val + parseInt(1) ;
	}
   // alert(field_row);
    $('#no_of_certi_row').val(parseInt(field_row));
    var objTo = document.getElementById('certificate_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclasscerti"+field_row);
	var rdiv = 'removeclass'+field_row;
	
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+field_row;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/certificate_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#no_of_certi_row').val(field_row);
			divtest.innerHTML = data ;
			objTo.appendChild(divtest)
			if(field_row == 10)
			{
				$('#add_more_certi_detail').slideUp();
			}
			
			$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 
			 });	*/
		}
		});
}
function remove_certi_fields(rid) {

   var certi_val = parseInt($('#no_of_certi_row').val());
   var new_val = certi_val - parseInt(1) ;
   $('#no_of_certi_row').val(parseInt(new_val));
   $('.removeclasscerti'+certi_val).remove();
   if(new_val < 10)
	{
		$('#add_more_certi_detail').slideDown();
	}
}

				
	

var exp_row = $('#no_of_work_exp_row').val();
function work_fields() {
    var exp_val = parseInt($('#no_of_work_exp_row').val());
	if(exp_val == 0)
	{
		var exp_val = 1 ;
		var exp_row = exp_val ;
	}
	else
	{
		var exp_row = exp_val + parseInt(1) ;
	}
    
    $('#no_of_edu_row').val(parseInt(exp_row));
    var objTo = document.getElementById('work_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclasswork"+exp_row);
	var rdiv = 'removeclass'+exp_row;
	
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&field_row="+exp_row;
	
	$.ajax({
		url : "<?php echo $base_url.'sign-up/work_exp_html' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('.datepicker').trigger('click');
			$('#no_of_work_exp_row').val(exp_row);
			divtest.innerHTML = data ;
			objTo.appendChild(divtest)
			if(exp_row == 10)
			{
				$('#add_more_work_exp').slideUp();
			}
			//$('.chosen-select').chosen();
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
   			}
			$('.datepicker-join,.datepicker-leave').datepicker({
							  format: 'yyyy-mm-dd',
							  endDate: '+0d',
							   autoclose: true
							  
							});
		    // $('.datepicker-join,.datepicker-leave').trigger('click');
			/*$('.datepicker-join,.datepicker-leave').click(function(){
				$('.datepicker-join,.datepicker-leave').datepicker({
							  format: 'yyyy-mm-dd',
							  endDate: '+0d',
								  autoclose: true
							  
							});
						
				});*/
			
			
			/*$('select')
			 .on('beforeValidation', function(value, lang, config) {
			  $('.chosen-select').css('display','block');
			 });
			$('select')
				.on('validation', function(value, lang, config) {
				  $('.chosen-select').css('display','none');
				 // $('.chosen-select').trigger('click');
			 });*/
			
			
		}
		});
}

function remove_work_fields(rid) {
	   
	   var exp_val = parseInt($('#no_of_work_exp_row').val());
   	   var new_val = exp_val - parseInt(1) ;
   	  $('#no_of_work_exp_row').val(parseInt(new_val));
	  $('.removeclasswork'+exp_val).remove();
	  if(new_val < 10)
		{
			$('#add_more_work_exp').slideDown();
		}
  }


function for_active_tab()
{
	$('.chosen-select').css('display','none');
	var $active = $('.wizard .nav-tabs li.active');
	$active.next().removeClass('disabled');
	nextTab($active);
	$('#step4').trigger('change');
	var tokenfieldwidth = $('.tokenfield').outerWidth();
    $('.tokenfieldwidth-tokenfield').width(tokenfieldwidth);
}


/*$('select')
    .on('beforeValidation', function(value, lang, config) {
      $('.chosen-select').css('display','block');
	 });
$('select')
    .on('validation', function(value, lang, config) {
      $('.chosen-select').css('display','none');
	  //$('.chosen-select').trigger('click');
 });	*/



	
$.validate({
    form : '#personal_info_form',
    modules : 'security',
    onError : function($form) {
	  scroll_to_div('for_scrool');
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#personal_info_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_personal_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				education_fields();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				//document.getElementById('personal_info_form').reset();
				scroll_to_div('for_scrool');
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });            
  
$.validate({
    form : '#education_form',
    modules : 'security',
	onError : function($form) {
	  scroll_to_div('for_scrool');	
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#education_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_education_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				 work_fields();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				//document.getElementById('education_form').reset();
				scroll_to_div('for_scrool');
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
$.validate({
   form : '#work_exp_form',
    modules : 'security',
	onError : function($form) {
	  scroll_to_div('for_scrool');
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#work_exp_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/add_work_exp_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				//document.getElementById('work_exp_form').reset();
				scroll_to_div('for_scrool');
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
 });
$.validate({
  form : '#js_details',
    modules : 'security',
    onError : function($form) {
	  scroll_to_div('for_scrool');
	  $('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").slideUp();
	  time_out_message();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#js_details").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign-up/js_extra_details' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				for_active_tab();
				//time_out_message();
				//document.getElementById('js_details').reset();
				scroll_to_div('for_scrool');
			}
			else
			{
				$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
 });
$.validate({
    form : '#js_img_upload',
    modules : 'file',
    onError : function($form) {
	 /* scroll_to_div('for_scrool');
	  $("#error_msg").html('');
	  $("#success_msg").html('');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#success_msg").hide();
	  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");*/
	  //time_out_message();
	 $('.view_preview_img').hide();
    },
    onSuccess : function($form) {
	 /*event.preventDefault();
	 return false;*/
	  var profile_pic= $("#profile_pic").val();
	  var resume_file= '';
	  if(profile_pic=='')
	  {  
		  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");
		  $("#error_msg").slideDown();
		  scroll_to_div('for_scrool');
		  return false;
	  }
	  show_comm_mask();
	  $("#error_msg").html('');
	  $("#success_msg").html('');
	  $("#error_msg").hide();
	  var form_data = new FormData();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  form_data.append('profile_pic', $('input[name=profile_pic]')[0].files[0]);
	  form_data.append('user_agent', 'NI-WEB');
	  form_data.append('action', 'upload_img');
	  form_data.append('profile_pic_upload', profile_pic);
	  form_data.append('resume_file_upload', resume_file);
	  form_data.append('csrf_job_portal', hash_tocken_id);
	  $.ajax({
		url : "<?php echo $base_url.'sign-up/upload_profile_pic_resume' ?>",
		type: 'post',
		data: form_data,        // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		dataType:"json",
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			//alert(data.status);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				$('#complete_reg').slideDown();
				$('#complete').slideUp();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#profile_pic").val('');
				$('#js_img_upload').hide();
				$("#success_msg").html(data.errmessage);
				time_out_message();
				document.getElementById('js_img_upload').reset();
				scroll_to_div('for_scrool');
				//setTimeout(function(){ window.location='<?php echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
			}
			else
			{
				if(data.resume_success_msg!='' || data.errmessage_resume!=''  || data.errmessage_profile_pic!='' || data.profile_pic_success_msg!='')
				{
					if(typeof(data.resume_success_msg) != "undefined" && data.resume_success_msg!='')
					{
						$("#success_msg").append('<strong>Resume :</strong>');
						$("#success_msg").slideDown();
						for_active_tab();
						//$('#complete_reg').slideDown();
						//$('#complete').slideUp();
						$("#success_msg").append(data.resume_success_msg);
						$("#resume_file").val('');
					}
					else if(typeof(data.errmessage_resume) != "undefined" && data.errmessage_resume!='')
					{
						$("#error_msg").append('<strong>Resume :</strong>');
						$("#error_msg").append(data.errmessage_resume);
						$("#error_msg").slideDown();
					}
					if(typeof(data.profile_pic_success_msg) != "undefined" && data.profile_pic_success_msg!='')
					{
						$("#success_msg").append('<strong>Profile picture  : </strong>');
						for_active_tab();
						$('#js_img_upload').hide();
						//$('#complete').slideUp();
						//$('#complete_reg').slideDown();
						$("#success_msg").slideDown();
						$("#success_msg").append(data.profile_pic_success_msg);
						$("#profile_pic").val('');
					}
					else if(typeof(data.errmessage_profile_pic) != "undefined" && data.errmessage_profile_pic!='')
					{
						$("#error_msg").append('<strong>Profile picture :</strong>');
						$("#error_msg").append(data.errmessage_profile_pic);
						$("#error_msg").slideDown();
					}
					//setTimeout(function(){ window.location='<?php echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
				}
				else
				{
					$("#error_msg").html(data.errmessage);
					$("#success_msg").slideUp();
				    $("#error_msg").slideDown();
				}
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	  });	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
 });
$.validate({
    form : '#js_resume_upload',
    modules : 'file',
    onError : function($form) {
	 /* scroll_to_div('for_scrool');
	  $("#error_msg").html('');
	  $("#success_msg").html('');
	  $("#success_msg").hide();
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");*/
	  //time_out_message();
	  
    },
    onSuccess : function($form) {
	  
	 /*event.preventDefault();
	 return false;*/
	  var profile_pic= '';
	  var resume_file= $("#resume_file").val();
	  if(resume_file=='')
	  {  
		  $("#error_msg").html("<?php echo $custom_lable_array['please_select_file']; ?>");
		   $("#error_msg").slideDown();
		  scroll_to_div('for_scrool');
		  return false;
	  }
	  show_comm_mask();
	  $("#error_msg").html('');
	  $("#success_msg").html('');
	  $("#error_msg").hide();
	  var form_data = new FormData();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  form_data.append('resume_file', $('input[name=resume_file]')[0].files[0]);
	  form_data.append('user_agent', 'NI-WEB');
	  form_data.append('action', 'upload_img');
	  form_data.append('profile_pic_upload', profile_pic);
	  form_data.append('resume_file_upload', resume_file);
	  form_data.append('csrf_job_portal', hash_tocken_id);
	  $.ajax({
		url : "<?php echo $base_url.'sign-up/upload_profile_pic_resume' ?>",
		type: 'post',
		data: form_data,        // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		dataType:"json",
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			//alert(data.status);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				$('#complete_reg').slideDown();
				$('#complete').slideUp();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#resume_file").val('');
				$("#success_msg").html(data.errmessage);
				$('#js_resume_upload').hide();
				time_out_message();
				document.getElementById('js_img_upload').reset();
				scroll_to_div('for_scrool');
				//setTimeout(function(){ window.location='<?php echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
			}
			else
			{
				if(data.resume_success_msg!='' || data.errmessage_resume!=''  || data.errmessage_profile_pic!='' || data.profile_pic_success_msg!='')
				{
					if(typeof(data.resume_success_msg) != "undefined" && data.resume_success_msg!='')
					{
						$("#success_msg").append('<strong>Resume :</strong>');
						$("#success_msg").slideDown();
						for_active_tab();
						$('#js_resume_upload').hide();
						//$('#complete_reg').slideDown();
						//$('#complete').slideUp();
						$("#success_msg").append(data.resume_success_msg);
						$("#resume_file").val('');
					}
					else if(typeof(data.errmessage_resume) != "undefined" && data.errmessage_resume!='')
					{
						$("#error_msg").append('<strong>Resume :</strong>');
						$("#error_msg").append(data.errmessage_resume);
						$("#error_msg").slideDown();
					}
					if(typeof(data.profile_pic_success_msg) != "undefined" && data.profile_pic_success_msg!='')
					{
						$("#success_msg").append('<strong>Profile picture  : </strong>');
						for_active_tab();
						
						//$('#complete').slideUp();
						//$('#complete_reg').slideDown();
						$("#success_msg").slideDown();
						$("#success_msg").append(data.profile_pic_success_msg);
						$("#profile_pic").val('');
					}
					else if(typeof(data.errmessage_profile_pic) != "undefined" && data.errmessage_profile_pic!='')
					{
						$("#error_msg").append('<strong>Profile picture :</strong>');
						$("#error_msg").append(data.errmessage_profile_pic);
						$("#error_msg").slideDown();
					}
					//setTimeout(function(){ window.location='<?php echo $base_url.'sign_up/sign-up-success' ?>';  }, 3000);
				}
				else
				{
					$("#error_msg").html(data.errmessage);
					$("#success_msg").slideUp();
				    $("#error_msg").slideDown();
				}
				scroll_to_div('for_scrool');
				time_out_message();
			}
			
		}
	  });	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
 }); 
 
 
 
$(document).ready(function(){
	
	
	$('#complete_reg').slideUp();
	$('.datepicker').datepicker({
	  dateFormat: 'yy-mm-dd',
	  endDate: '+0d',
      autoclose: true
	});  
var tokenfieldwidth = $('.tokenfield').outerWidth();
$('.tokenfieldwidth-tokenfield').width(tokenfieldwidth);
//check_user_exp_or_fresher();
/*if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
   var datepicker = $.fn.datepicker.noConflict();
   $.fn.bootstrapDP = datepicker;
   $('.datepicker').bootstrapDP({
   format: 'yyyy-mm-dd',
   endDate: '+0d',
   autoclose: true
});
}*/
 	
     var config = {
		  '.chosen-select'           : {},
		  '.chosen-select-deselect'  : {allow_single_deselect:true},
		  '.chosen-select-no-single' : {disable_search_threshold:10},
		  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		  '.chosen-select-width'     : {width:"100%"}
		}
		for (var selector in config) {
		  $(selector).chosen(config[selector]);
		}    
	
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });


   $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
	
	
	
	$('.accordion-header').toggleClass('inactive-header');

	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});

	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');

	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}

		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	
	$(".prev-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);
		$('#step4').trigger('change');
		scroll_to_div('for_scrool');
    });
});
</script>
<script>

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
   
$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    var gender =  $(this).data('value');
	$('#gender').val(gender);
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$('#radioBtn1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    var m_status =  $(this).data('value');        
	$('#m_status').val(m_status);
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
}) 
 

</script>
