<style>
	.tab-pane {
	background: transparent;
    box-shadow:none;
	}
	.panel-heading.panel-bg, ui-menu .ui-menu-item a:hover, table.manage-table.resumes th:first-child {
    background-color: #ff3434;
    color: white;
	}
	
</style>
<?php $custom_lable_arr = $custom_lable->language; ?>
<?php  $upload_path_profile ='./assets/js_photos';
$mobile = ($this->common_front_model->checkfieldnotnull($user_data['mobile'])) ? explode("-",$user_data['mobile']) : "";
$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode(".",$user_data['total_experience']) : "";
$annual_salary = ($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? explode(".",$user_data['annual_salary']) : "";
$annual_salary_exp = ($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) ? explode(".",$user_data['expected_annual_salary']) : "";
/*$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode("-",$user_data['total_experience']) : "";
$annual_salary = ($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? explode("-",$user_data['annual_salary']) : "";
$annual_salary_exp = ($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) ? explode("-",$user_data['expected_annual_salary']) : "";*/
$pass_var_progress['custom_lable_arr'] = $custom_lable_arr;
$pass_var_progress['user_data_values'] = $user_data;
$pass_var_personal_view['user_data_values'] = $user_data;
$pass_var_personal_view['custom_lable_arr'] = $custom_lable_arr;
 ?>
			<!-- <div id="progressbarview_edit">
                    <?php //$this->load->view('front_end/progressbar_view',$pass_var_progress); ?>
				</div>	 -->
				<div class="container-fluid" style="padding:0px;">
					<!-- <div class="row">
						<div class="col-sm-12">
							<h2 class="register"><u><?php echo $custom_lable_arr['js_edit_title']; ?></u></h2>
						</div>
					</div> -->
					<br clear="all" />
					
                    <div class="clearfix"></div>
                    <div class="tabs-left"></div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
							<?php $this->load->view('front_end/job_seeker_left_menu',$pass_var_personal_view); ?>
							<?php // include_once("job_seeker_left_menu.php",$pass_var_personal_view); ?>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 tab-content clearfix">
								<div id="accordion-container" >
                                	<div class="clearfix" id="profile_snapshoteditdivscroll"></div>
									<h2 class="accordion-header" id="profile_snapshoteditdiv">Personal Details :</h2>
									<div class="accordion-content">
                                    	<form method="post" name="editpersonaldet" id="editpersonaldet" action="<?php echo $base_url.'my_profile/editpersonaldet'; ?>" >
                                    <div class="alert alert-danger" id="message2" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg2" style="display:none" ></div>
										<div class="row" >
											<div class="col-md-12">
                                            	<div class="row">
                                            	<div class="col-md-3" >
                                                      <h5><?php echo $custom_lable_arr['sign_up_emp_1stformtitle']; ?> :<span class="red-only"> *</span></h5>
                                                      <div class="input-group">
                                                      	<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                            <select  class="form-control" name="title" id="title" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformtitlevalmes']; ?>" style="padding:9px 0px;">
															<?php
                                                            if($pers_title != '' && is_array($pers_title) && count($pers_title) > 0)
                                                            {
                                                                foreach ($pers_title as $pers_title_single)
                                                                {
																	$gn_class = '';
																	if(strtolower(trim($pers_title_single['personal_titles'])) == 'mr.' || strtolower(trim($pers_title_single['personal_titles'])) == 'mr')
																	{
																		$gn_class = 'gn_classmale';
																	}
																	else if(strtolower(trim($pers_title_single['personal_titles'])) == 'ms.' || strtolower(trim($pers_title_single['personal_titles'])) == 'ms' || strtolower(trim($pers_title_single['personal_titles'])) == 'mrs' || strtolower(trim($pers_title_single['personal_titles'])) == 'mrs.' || strtolower(trim($pers_title_single['personal_titles'])) == "ma'am" || strtolower(trim($pers_title_single['personal_titles'])) == "ma'am.")
																	{
																		$gn_class = 'gn_classfemale';
																	}
																	 ?>
                                                                    <option value="<?php echo $pers_title_single['id'];?>" <?php if($user_data!='' && $pers_title_single['id']==$user_data['title'] ){ echo "selected" ; } ?> class="<?php echo $gn_class; ?>"  ><?php echo $pers_title_single['personal_titles'];?> </option>	
                                                                <?php }
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>    
                                               </div>
                                                    
                                                <div class="col-md-9">
                                                            <h5><?php echo $custom_lable_arr['reg_lbl_fullname']; ?> :<span class="red-only"> *</span></h5>
                                                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="John" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ? $user_data['fullname'] : "" ; ?>" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformfullnamevalmes']; ?>" >
                                               </div>
                                               	</div>
                                                
                                                
                                                
                                                <div class="margin-top-20"></div>
                                                <h5><?php echo $custom_lable_arr['resume_headline']; ?> :<span class="red-only"> *</span></h5>
                                                <input type="text" class="form-control" id="resume_headline" name="resume_headline" placeholder="xyz Developer" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['resume_headline'])) ? $user_data['resume_headline'] : ""; ?>" style="padding:9px 0 9px 10px;border-radius:0;">
												
												<div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['gender_lbl']; ?> :<span class="red-only"> *</span></h5>
												<div style="padding-left:10px;">
														<label class="radio-inline">
														  <input type="radio" onClick="change_gender('Male','change');" name="gender" value="Male" <?php if($user_data['gender']=='Male') {echo "checked";} ?>><?php echo $custom_lable_arr['gender_lbl_male']; ?>
														</label>
														<label class="radio-inline">
														  <input type="radio" name="gender" value="Female" <?php if($user_data['gender']=='Female') {echo "checked";} ?> onClick="change_gender('Female','change');"><?php echo $custom_lable_arr['gender_lbl_female']; ?>
														</label>
												</div>
												<div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['m_status_lbl']; ?> :<span class="red-only"> *</span></h5>
												<div style="padding-left:10px;">
                                               <?php 
											   			if(isset($marital_status) && is_array($marital_status) && count($marital_status) > 0)
														{
														foreach ($marital_status as $m_status)
														{   ?>
                                                        <label class="radio-inline">
														  <input type="radio" name="marital_status" <?php if($m_status['id']==$user_data['marital_status']) {echo "checked";} ?> value="<?php echo  $m_status['id']; ?>" ><?php echo $m_status['marital_status']; ?>
														</label>
														<?php }
														}
														?>
												</div>
												<div class="margin-top-20"></div>
												<div class="row">
												<h5 style="margin-left:15px;"><?php echo $custom_lable_arr['mobile_lbl']; ?> :<span class="red-only"> *</span></h5>
												<div class="col-md-2 col-xs-12">
													<div class="input-group">
														<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                        <select  class="form-control " name="mobile_c_code" id="mobile_c_code" style="height:42px;" data-validation="required">
                                                            <?php
															if(isset($country_code) && $country_code !='' && is_array($country_code) && count($country_code) > 0)
															{
															foreach ($country_code as $code)
															{ ?>
																<option value="<?php echo $code['country_code'];?>" <?php if($mobile!='' && isset($mobile[0]) && $mobile[0]==$code['country_code'] ){ echo "selected" ; } ?>><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
															<?php }
															}
															?>
                                                            </select>
													</div>
												</div>
												<div class="col-md-6">
														<input type="text" class="form-control" id="mobile" name="mobile"  placeholder="<?php echo $custom_lable_arr['mobile_palce_h_lbl']; ?>" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required,number,length"  data-validation-length="10-13" value="<?php if($mobile!='' && isset($mobile[1])){ echo $mobile[1] ; } ?>">
                                                        <input type="hidden" name="mobile_num_old" value="<?php if(isset($user_data['mobile']) && $user_data['mobile']!=''){ echo $user_data['mobile']; } ?>" />
												</div>
												</div>
                                                
                                                
                                                <div class="margin-top-20"></div>
                                                <h5><?php echo $custom_lable_arr['landline']; ?> :</h5>
                                                <input type="text" class="form-control" id="landline" name="landline" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['landline'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['landline'])) ? $user_data['landline'] : ""; ?>" style="padding:9px 0 9px 10px;border-radius:0;">
                                                
												<div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['dob_lbl']; ?> :<span class="red-only"> *</span></h5>
												<div class="input-group">
												  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												  <input type="text" class="form-control" id="birthdate" name="birthdate" placeholder="Date of Birth" onkeydown="return false" style="padding:9px 0 9px 10px;border-radius:0;" value="<?php echo ($user_data['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($user_data['birthdate'])) ? $user_data['birthdate'] : ""; ?>"  ><!-- onkeydown="return false" readonly='true'-->
												</div>
                                                
												<div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['country_lbl']; ?> :<span class="red-only"> *</span></h5>
													<select name="country" id="country" class="city form-control ketan" style="padding:10px;border-radius:0;" data-validation-skipped='0' onChange="dropdownChange('country','city','city_list');" data-validation="required">
                                                        <?php if($this->common_front_model->checkfieldnotnull($user_data['country'])) { echo  $this->common_front_model->get_list('country_list','str','','str',$user_data['country']);}else{ echo $this->common_front_model->get_list('country_list','str'); } ?>
														</select>
												<div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['city_lbl']; ?> :<span class="red-only"> *</span></h5>
                                                <select name="city" id="city" class="city form-control" style="padding:10px;border-radius:0;" data-validation="required" >
                                                <option value=""><?php echo $custom_lable_arr['sign_up_emp_1stformcitydfltopt']; ?></option>
												</select>
                                                
                                                <div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['address_lbl']; ?> :<span class="red-only"> *</span></h5>
												<textarea class="form-control" name="address" id="address" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['address_lbl'];  ?>" style="padding:9px 0 9px 10px;border-radius:0;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['address'])) ? $user_data['address'] : ""; ?></textarea>
                                                
												<div class="margin-top-20"></div>
													<h5 ><?php echo $custom_lable_arr['pincode']; ?> :<span class="red-only"> *</span></h5>
													<input type="text" class="form-control" name="pincode" id="pincode" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['pincode'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['pincode'])) ? $user_data['pincode'] : ""; ?>" style="padding:9px 0 9px 10px;border-radius:0;">
                                                 
                                                
                                                 <div class="margin-top-20"></div>
												<h5 ><?php echo $custom_lable_arr['home_city']; ?> :<span class="red-only"> *</span></h5>
                                                <div class="">
                                                	<input type="text" name="home_city" class="form-control" id="home_city" style="border-radius:0px;" data-validation="required" placeholder="Add Home City By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['home_city'])) ? $user_data['home_city'] : ""; ?>">
                                                </div>
                                                <div class="margin-top-20"></div>
                                               <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
												<div class="text-center">
													<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
													<button type="button" onClick="viewsection('profile_snapshot');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button>
												</div>
											</div>
										</div> 
                                    </form>
									</div>
                                    
                                    <!-- other details section starts -->
                                    <div class="clearfix" id="other_detailseditdivscroll"></div>
                                    <h2 class="accordion-header" id="other_detailseditdiv">Other Details :</h2>
									<div class="accordion-content">
                                    <form method="post" name="editotherdet" id="editotherdet" action="<?php echo $base_url.'my_profile/editpersonaldet'; ?>" >
                                    <div class="alert alert-danger" id="messageother" style="display:none" ></div>
									<div class="alert alert-success" id="success_msgother" style="display:none" ></div>
                                    
                                    <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['profile_summary']; ?> :</h5>
                                        <textarea style="border-radius:0px;"rows="2" cols="2" name="profile_summary" class="form-control" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['profile_summary'];  ?>"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['profile_summary'])) ? $user_data['profile_summary'] : ""; ?></textarea>
                                    
                                    <div class="margin-top-20"></div>
                                    	<div>
                                        <h5><?php echo $custom_lable_arr['preferred_city']; ?> :<span class="red-only"> *</span></h5>
                                         <select name="preferred_city[]" id="preferred_city" class="select2" style="width:100%;"  multiple data-placeholder="<?php echo $custom_lable_arr['select'] .' '. $custom_lable_arr['preferred_city'];  ?>" data-validation="required">
                                        <?php
										if($user_data['preferred_city']!='')
										{ 
											$city = $user_data['preferred_city'];
											$where_arra_city = "id IN (".$city.")";
											$city_list = $this->common_front_model->get_count_data_manual('city_master',$where_arra_city,2,'city_name,id');
											if(isset($city_list) && $city_list !='' && is_array($city_list) && count($city_list) > 0)
											{
											//echo $this->db->last_query();
											foreach($city_list as $city)
											{ ?>
                                            	<option value="<?php echo $city['id']; ?>" selected><?php echo $city['city_name']; ?></option>
											<?php }
											}
											?>
                                        	
										<?php }
										?>
                                                     
                                                      </select>
										</div>	                                                      

                                        <!--<input type="text" name="preferred_city" class="form-control" id="preferred_city" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['preferred_city'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['preferred_city'])) ? $user_data['preferred_city'] : ""; ?>">-->
                                    
										<div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['prefered_shift']; ?> :<span class="red-only"> *</span></h5>
                                        <div>
                                        <input type="text" name="prefered_shift" class="form-control" id="prefered_shift" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['prefered_shift'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['prefered_shift'])) ? $user_data['prefered_shift'] : ""; ?>" >
                                        </div>
                                        
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['website']; ?> :</h5>
                                        <input type="text" name="website" class="form-control" id="website" style="border-radius:0px;"  placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['website'];  ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['website'])) ? $user_data['website'] : ""; ?>" > <!--data-validation="url"-->
                                        
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['total_experience'] ?> :<span class="red-only"> *</span></h5>
                                        <div class="row">
											<div class="col-sm-6">
                                                    	<select name="exp_year" id="exp_year" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;" onChange="expchange();">
                                                        <option value=""><?php echo $custom_lable_arr['year']; ?></option>
                                                        <?php
                                                        for($i=0;$i <= 25 ; $i++)
                                                        { ?>
                                                            <option value="<?php echo  $i; ?>" <?php if($totalexp!='' && isset($totalexp[0]) && $totalexp[0]==$i ){ echo "selected" ; } ?>><?php echo  $i; ?> </option>
                                                    <?php	}
                                                        ?>
                                                    </select>
													</div>
											<div class="col-sm-6">
												<select name="exp_month" id="exp_month" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;" onChange="expchange();">
                                                    <option value=""><?php echo $custom_lable_arr['month']; ?></option>
                                                    <?php
                                                    for($i=0;$i <= 11 ; $i++)
                                                    { ?>
                                                    <option value="<?php echo  $i; ?>" <?php if($totalexp!='' && isset($totalexp[1]) && $totalexp[1]==$i ){ echo "selected" ; } ?>><?php echo  $i; ?> </option>
                                                <?php	}
                                                    ?>
                                                </select>
											</div>
										</div>
                                        
                                        <div class="margin-top-20"></div>
                                        <div id="annsaldivid">
                                            <h5><?php echo $custom_lable_arr['current_annual_salary']; ?> :<span class="red-only"> *</span></h5>
                                            <div class="row">
												<div class="col-sm-12">
													<select name="annual_salary" id="annual_salary" data-validation="required" class="form-control" style="padding:10px;border-radius:0;" >
														<?php if($this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) { echo  $this->common_front_model->get_list('salary_range_list','str','','str',$user_data['annual_salary']);}else{ echo $this->common_front_model->get_list('salary_range_list','str'); } ?>
													</select>
												</div>
                                               <?php /*?><div class="col-sm-4">
                                                    <select name="currency_type" id="currency_type" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;">
                                                         <?php if($this->common_front_model->checkfieldnotnull($user_data['currency_type'])) 
														 {
															echo $this->common_front_model->get_list('currency_master','str','','str',$user_data['currency_type']);
														 }
														 else
														 { 
														 	  echo $this->common_front_model->get_list('currency_master','str'); 
														 } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="a_salary_lacs" id="a_salary_lacs" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <option value=""><?php echo $custom_lable_arr['lacs']; ?></option>
                                                         <?php 
                                                         for($i=0;$i <= 99 ; $i++)
                                                         { ?>
                                                             <option value="<?php echo $i ?>" <?php if($annual_salary!='' && isset($annual_salary[0]) && $annual_salary[0]==$i ){ echo "selected" ; } ?> ><?php echo $i ?> </option>
                                                         <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="a_salary_thousand" id="a_salary_thousand" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;">
                                                        <option value=""><?php echo $custom_lable_arr['thousand']; ?></option>
                                                         <?php
                                                         for($i=0;$i <= 90;)
                                                         { $i = $i+5; ?>
                                                              <option value="<?php echo $i ?>" <?php if($annual_salary!='' && isset($annual_salary[1]) && $annual_salary[1]==$i ){ echo "selected" ; } ?> ><?php echo $i ?> </option>
                                                        <?php  }
                                                         ?>
                                                    </select>
                                                </div><?php */?>
                                            </div>
                                        </div>
                                        <div class="margin-top-20"></div>
                                        <div id="industrydivid">
                                        <h5><?php echo $custom_lable_arr['type_industry']; ?> :<span class="red-only"> *</span></h5>
                                        <select name="industry" id="industry" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;" >
                                            <?php if($this->common_front_model->checkfieldnotnull($user_data['industry'])) { echo  $this->common_front_model->get_list('industries_master','str','','str',$user_data['industry']);}else{ echo $this->common_front_model->get_list('industries_master','str'); } ?>
                                        </select>
                                        </div>
                                        
                                        <div class="margin-top-20"></div>
                                        <div id="fnareadivid">
                                        <h5><?php echo $custom_lable_arr['functional_area']; ?> :</h5>
                                        <select name="functional_area" id="functional_area" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;" onChange="dropdownChange('functional_area','job_role','role_master');">
                                         <?php /*if($this->common_front_model->checkfieldnotnull($user_data['functional_area'])) { echo  $this->common_front_model->get_list('functional_area_master','str','','str',$user_data['functional_area']);}else{ echo $this->common_front_model->get_list('functional_area_master','str'); }*/ ?>
                                         <?php if(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && ($user_data['functional_area']=='' || is_null($user_data['functional_area'])) )
                                                    { 
                                                        print_r($fnarea_frm_tbl); 
                                                    }
                                                    elseif(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($user_data['functional_area']))
                                                    {
                                                        print_r($this->common_front_model->get_list('functional_area_master','str','','str',$user_data['functional_area']));
                                                    }
                                                    else
                                                    { ?>
                                                        <option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['functional_area']; ?></option>
                                                    <?php } ?>
                                        </select>
                                        </div>
                                        
                                        <div class="margin-top-20"></div>
                                        <div id="jbroledivid">
                                        <h5><?php echo $custom_lable_arr['job_role']; ?> :</h5>
                                        <select name="job_role" id="job_role" data-validation="required" class="form-control city" style="padding:10px;border-radius:0;">
                                            <?php /*if($this->common_front_model->checkfieldnotnull($user_data['job_role'])) { echo  $this->common_front_model->get_list('role_master','str','','str',$user_data['job_role']);}else{ echo $this->common_front_model->get_list('role_master','str'); }*/ ?>
                                            <?php if(($user_data['job_role']=='' || is_null($user_data['job_role'])) && $this->common_front_model->checkfieldnotnull($user_data['functional_area']))
                                                    { 
                                                        print_r($this->common_front_model->get_list('role_master','str',$user_data['functional_area'],'str',''));
                                                    }
                                                    elseif($this->common_front_model->checkfieldnotnull($user_data['functional_area']) && $this->common_front_model->checkfieldnotnull($user_data['job_role']))
                                                    {
                                                        print_r($this->common_front_model->get_list('role_master','str',$user_data['functional_area'],'str',$user_data['job_role']));
                                                    }
                                                    else
                                                    { ?>
                                                        <option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['job_role']; ?></option>
                                                    <?php } ?>
                                        </select>
                                        </div>
                                        
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['skill']; ?> :<span class="red-only"> *</span></h5>
                                        <input type="text" name="key_skill" class="form-control" id="key_skill" style="border-radius:0px;" data-validation="required"  placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['skill'];  ?>Find Specify Skill By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['key_skill'])) ? $user_data['key_skill'] : ""; ?>">
                                        
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['desire_job_type']; ?> :<span class="red-only"> *</span></h5>
                                        <input type="text" name="desire_job_type" class="form-control" id="desire_job_type" style="border-radius:0px;" data-validation="required"   placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['desire_job_type'];  ?>Find Specify Desire Job Type By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['desire_job_type'])) ? $user_data['desire_job_type'] : ""; ?>">
                                        
                                        <div class="margin-top-20"></div>
                                        <h5><?php echo $custom_lable_arr['employment_type']; ?> :<span class="red-only"> *</span></h5>
                                        <input type="text" name="employment_type" class="form-control" id="employment_type" style="border-radius:0px;" data-validation="required" placeholder="<?php //echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['employment_type'];  ?>Find Specify Employment Type By Typing Here" value="<?php echo ($this->common_front_model->checkfieldnotnull($user_data['employment_type'])) ? $user_data['employment_type'] : ""; ?>">
                                   
                                   <div class="margin-top-20"></div>
                                    <div >
                                            <h5><?php echo $custom_lable_arr['expected_annual_salary'] ?> :<span class="red-only"> *</span></h5>
                                            <div class="row">
												<div class="col-sm-12">
													<select name="expected_annual_salary" id="expected_annual_salary" data-validation="required" class="form-control" style="padding:10px;border-radius:0;" >
														<?php if($this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) { echo  $this->common_front_model->get_list('salary_range_list','str','','str',$user_data['expected_annual_salary']);}else{ echo $this->common_front_model->get_list('salary_range_list','str'); } ?>
													</select>
												</div>
                                                <?php /*?><div class="col-sm-4">
                                                    <select name="exp_salary_currency_type" data-validation="required" class="form-control" >
														<?php if($this->common_front_model->checkfieldnotnull($user_data['exp_salary_currency_type'])) 
														 {
															echo $this->common_front_model->get_list('currency_master','str','','str',$user_data['exp_salary_currency_type']);
														 }
														 else
														 { 
														 	  echo $this->common_front_model->get_list('currency_master','str'); 
														 } ?>
													</select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="exp_salary_lacs" data-validation="required" class="form-control" >
												<option value=""><?php echo $custom_lable_arr['lacs'] ?></option>
												 <?php 
												 for($i=0;$i <= 99 ; $i++)
												 { ?>
													 <option value="<?php echo $i ?>" <?php if($annual_salary_exp!='' && isset($annual_salary_exp[0]) && $annual_salary_exp[0]==$i ){ echo "selected" ; } ?>><?php echo $i ?> </option>
												 <?php } ?>
											</select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="exp_salary_thousand" data-validation="required" class="form-control" >
														<option value=""><?php echo $custom_lable_arr['thousand'] ?></option>
												 <?php
												 for($i=0;$i <= 90;)
												 {?>
													  <option value="<?php echo $i ?>" <?php if($annual_salary_exp!='' && isset($annual_salary_exp[1]) && $annual_salary_exp[1]==$i ){ echo "selected" ; } ?>><?php echo $i ?> </option>
												<?php  $i = $i+5; }
												 ?>
											</select>
                                                </div><?php */?>
                                            </div>
                                        </div>
                                        
										<div class="margin-top-20"></div>
										<div class="clear"></div>
										<div class="margin-top-10"></div>
                                        <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>     
										<div class="text-center">
													<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
													<button type="button" onClick="viewsection('other_details');" class="btn btn-default"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button>
												</div>
                                        </form>
									</div>
                                    <!-- other details section ends -->
                                    <!-- social links section starts-->
                                    <div class="clearfix" id="social_linkseditdivscroll"></div>
									<h3 class="accordion-header" id="social_linkseditdiv"> <?php echo $custom_lable_arr['myprofile_social_med_tit']; ?> :</h3>
									<div class="accordion-content">
                                    <div class="alert alert-danger" id="message_social" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_social" style="display:none" ></div>
                                    	<div id="socialwholeid">
                                        	<?php $this->load->view('front_end/soaial_link_edit_section'); ?>
                                    	</div>
									</div>
                                    <!-- social links section ends-->
                                    <!-- Work history section starts -->
                                    <div class="clearfix" id="workdetails_vieweditdivscroll"></div>
                                    <h2 class="accordion-header" id="workdetails_vieweditdiv">Work History Details :</h2>
                                    <div class="accordion-content">
                                    <div class="alert alert-danger" id="messagework" style="display:none" ></div>
									<div class="alert alert-success" id="success_msgwork" style="display:none" ></div>
                                    <div id="workwholeid">
                                    	<?php $this->load->view('front_end/work_section'); ?>
                                    </div>                                    
                                    </div>
                                    <!-- Work history section ends -->
                                    <!-- education section starts -->
                                    <div class="clearfix" id="education_vieweditdivscroll"></div>
<h2 class="accordion-header" id="education_vieweditdiv"><?php echo $custom_lable_arr['edu_qualification_lbl']; ?> :</h2>

	<div class="accordion-content">
                                        <div class="alert alert-danger" id="messageedu" style="display:none" ></div>
                                        <div class="alert alert-success" id="success_msgedu" style="display:none" ></div>	
                                    <div id="educationwholeid">
                                    	<?php $this->load->view('front_end/education_section'); ?>
                                    </div><!-- education whole dive ends here-->
                                    </div>
                                    <!-- education section ends-->
                                    <!-- language section starts-->
                                    <div class="clearfix" id="language_vieweditdivscroll"></div>
									<h3 class="accordion-header" id="language_vieweditdiv"> <?php echo $custom_lable_arr['myprofile_lang_known_tit']; ?> :</h3>
									<div class="accordion-content">
                                    <div class="alert alert-danger" id="message_lang" style="display:none" ></div>
									<div class="alert alert-success" id="success_msg_lang" style="display:none" ></div>
                                    	<div id="langwholeid">
                                        	<?php $this->load->view('front_end/lang_section'); ?>
                                    	</div>
									</div>
                                    <!-- language section ends-->
                                    
									<div class="clearfix" id="profile_photoeditdivscroll"></div>
                                        <form method="post" enctype="multipart/form-data" id="profileresumeform" name="profileresumeform" action="<?php echo $base_url.'my_profile/uploadprofile'; ?>"  onSubmit="return uploadprofilepic();">
                                            <h3 class="accordion-header" id="profile_photoeditdiv">Your Photo :</h3>
                                            <div class="accordion-content" >
                                                <div class="ten columns">
                                                    <div id="image_val" style="position:fixed;  left:40%; top:18%; z-index:9999; opacity:0"></div>
                                                    <img id="blah" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic']))
                { echo $upload_path_profile.'/'.$user_data['profile_pic']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" class="blah" alt="your image" />
                                                    <div class="margin-top-10"></div>
                                                    <input type="file" onchange="displayprofile();" name="profile_img" id="profile_img">
                                                    <small id="fileHelp" class="text-left text-muted red-only"><i>Please Select JPG, PNG, JPEG <br/>Max File Size 2MB.</i></small>
                                                    <div class="margin-top-20"></div>
                                                    <!--<a class="btn btn-primary" onClick="uploadprofilepic();" style="text-transform:capitalize;color:#fff;"><i class="fa fa-upload" aria-hidden="true"></i> Upload a new  Photo!</a>-->
                                                    <div class="margin-bottom-10"></div>
                                                <?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic']))
                {  ?>    
                                                    <button type="button" class="btn btn-danger" id="deleteprofile_btn" onClick="deleteprofilepic();">Delete Profile</button>
          <?php } ?>
                                                </div>
                                                
                                                <div class="five columns" >
                                                <!--<button type="button" class="btn btn-danger" id="deleteprofile_btn" onClick="deleteprofilepic();">Delete Profile</button>-->
                                            </div>
                                            	
                                                <!--<hr>-->
                                                <!--<h4><u>Upload Resume</u>:</h4>-->
                                                <div class="entry input-group col-xs-3">
                                                   <?php /*?> <input class="btn" name="fields[]" type="file"><?php */?>
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-primary" name="submitphotoresume" value="submitphotoresume"> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
                                                    <a class="btn btn-default" onClick="viewsection('profile_snapshot');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></a>
                                                </div>
                                            </div>
                                        </form>
								</div>
						</div>
					
				</div>
			
<script>
$(document).ready(function(e) {
   <?php if($user_data['city']!=""){ ?> 
dropdownChange('country','city','city_list',function() {setcityval();});
//$.when(dropdownChange('country','city','city_list')).then(setcityval());
function setcityval()
{
	$("#city").val('<?php echo $user_data['city']; ?>');
	$('#city').trigger('chosen:updated');
}
<?php }  ?> 
expchange();

change_gender('<?php echo $user_data['gender']; ?>','onload');
});

function expchange()
{
	var expyear = $('#exp_year').val();
	var expmonth = $('#exp_month').val();
	var hideids = ["annsaldivid", "jbroledivid", "fnareadivid", "industrydivid"];
	/*alert(expmonth);alert(expyear);*/
	if(($.trim(expmonth)!=0 && checkvarok(expmonth)) || ($.trim(expyear)!=0 && checkvarok(expyear)))
	{
		showidsfn(hideids);
	}
	else
	{
		hideidsfn(hideids);
		$('#currency_type').val('').trigger('chosen:updated');
		$('#a_salary_lacs').val('').trigger('chosen:updated');
		$('#a_salary_thousand').val('').trigger('chosen:updated');
	}
}
function hideidsfn(idarray)
{
	/*var arrayLength = idarray.length;*/
	$.each(idarray,function(index,value){
		$('#'+value).slideUp();
	}) 
	/*for (var i = 0; i < arrayLength; i++) 
	{
			//alert(idarray[i]);
	}*/
}
function showidsfn(idarray)
{
	$.each(idarray,function(index,value){
		$('#'+value).slideDown();
	}) 
}
function checkvarok(parvar)
{
	if($.trim(parvar)!='' && $.trim(parvar)!='undefined' && $.trim(parvar)!='null' && $.trim(parvar)!=null && $.trim(parvar)!=undefined && $.trim(parvar)!=undefined)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function donotrepeatedu(elm)
{
	var selectedoption = $(elm).find('option:selected');
	var selectedselectname = $(elm).closest('select').attr('name');
	var selected_group = selectedoption.closest('optgroup').attr('data-optgrpid');
	
	$("select[name^='qualification_level']").each(function( index ) {
		$this = $( this );
		var allselectname = $this.attr('name');
		if(allselectname != selectedselectname)
		{
			//$( "optgroup[data-optgrpid='"+selected_group+"']" ).attr('disabled','disabled').trigger("chosen:updated");
			//$this.find("optgroup[data-optgrpid="+selected_group+"]").attr('disabled','disabled').trigger("chosen:updated");
		}
		else
		{
			//$this.find("optgroup").removeAttr('disabled').trigger("chosen:updated");
			//$( "optgroup[data-optgrpid='"+selected_group+"']" ).removeAttr('disabled').trigger("chosen:updated");
		}
	});
}

function change_gender(gender,action)
{
	if(action=='change')
	{
		$('#title').val('');
	}
	
	if(gender=='Male')
	{
		$('.gn_classfemale').css('display','none');
		$('.gn_classmale').css('display','block');
	}
	else if(gender=='Female')
	{
		$('.gn_classfemale').css('display','block');
		$('.gn_classmale').css('display','none');
	}
}  

</script>  