<?php $custom_lable_arr = $custom_lable->language; ?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/about-us-banner.jpg); background-size:cover;"><!--class="single"-->
	<div class="container">
		<div class="sixteen columns">
			<h2><?php echo  $custom_lable_arr['all_blog_page_title']; ?></h2>
			<span><?php echo $custom_lable_arr['all_blog_page_subtitle']; ?></span>
		</div>
	</div>
</div>
<!-- Content
================================================== -->
<div class="container">
	<!-- Blog Posts -->
	<div class="eleven columns" id="main_content_ajax">
		<?php $this->load->view('front_end/blog_all_result'); ?>
	</div>
	<!-- Blog Posts / End -->

	<!-- Widgets -->
	<?php $this->load->view('front_end/blog_right'); ?>
	<!-- Widgets / End -->


</div>
<input type="hidden" name="base_url_ajax" id="base_url_ajax" value="<?php echo $base_url; ?>" />

<script src="<?php echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>
<script>
$(document).ready(function(e) {
if($("#ajax_pagin_ul").length > 0)
{
	load_pagination_code();
}
});
</script>