<?php
$custom_lable_array = $custom_lable->language;
?>

<div id="titlebar" class="single submit-page" style="background-color:#94AFCC;">
	<div class="container">
		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-lock" aria-hidden="true"></i> <?php echo $custom_lable_array['reset_pass_page_title']; ?></h2>
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
		<div class="sixteen columns">
			<div class="well">
				<div class="panel panel-primary">
					<div class="panel-heading"><?php echo $custom_lable_array['reset_pass_page_title']; ?></div>
						<div class="panel-body">
							<div class="margin-top-10"></div>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="text-center">
											  <img src="<?php echo $base_url; ?>assets/front_end/images/shield.png" class="img-responsive text-center" alt="setting" style="width:30%;margin:0 auto;"/>
											  <h2 class="text-center"><?php echo $custom_lable_array['reset_pass_page_title']; ?> ?</h2>
											  <p><?php echo $custom_lable_array['forgot_pass_page_text']; ?>.</p>
                                        <div class="alert alert-danger" id="error_msgf" style="display:none" > </div>
                                        <div class="alert alert-success" id="success_msgf" style="display:none" ></div>
               							   <form class="form-group" id="reset_pass_form" role="form">
												<div class="panel-body">
                                                <div class="row">
													<div class="form-group has-feedback has-feedback-left">
														  <!--<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>-->
                                                          <i class="form-control-feedback glyphicon glyphicon-envelope color-blue"></i>
                                                          <input id="pass_confirmation" type="password" class="form-control" name="pass_confirmation" style="padding:9px 0 9px 10px;" data-validation="length" data-validation-length="min6" data-validation-error-msg="<?php echo $custom_lable_array['sign_up_emp_1stformpassvalmes']; ?>" placeholder="<?php echo $custom_lable_array['sign_up_emp_1stformpassvalmes']; ?>" />
														</div>
                                                   </div>  
                                                   <div class="row">   
                                                        <div class="form-group has-feedback has-feedback-left">
														  <!--<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>-->
                                                          <i class="form-control-feedback glyphicon glyphicon-envelope color-blue"></i>
                                                          <input id="pass" type="password" class="form-control" name="pass" data-validation="required,confirmation" data-validation-confirm="pass_confirmation" style="padding:9px 0 9px 10px;" placeholder="<?php echo $custom_lable_array['cnf_pass_reset']; ?>" data-validation-error-msg="<?php echo $custom_lable_array['sign_up_emp_1stformpassvalmescnf']; ?>" />
														</div>
                                                    </div>      
												   </div>                                             
                                                   <div class="form-group">
                                                        <input name="forgot_password" class="btn btn-lg btn-primary btn-block" value="<?php echo $custom_lable_array['forgot_pass_btn_lbl']; ?>" type="submit">
                                                      </div>
                                                  <input name="action" type="hidden"  value="reset_password" />
                                                  <input name="user_agent" type="hidden"  value="NI-WEB" />
												</form> 
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
   </div>
<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
$.validate({
    form : '#reset_pass_form',
    modules : 'security',
    onError : function($form) {
	  $("#success_msgf").hide();
	  $("#error_msgf").slideDown();
	  $("#error_msgf").html("<?php echo $custom_lable_array['login_emp_form_val_mes']; ?>");
	},
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msgf").hide();
	  var datastring = $("#reset_pass_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'login_employer/reset_new_password/'.$query_get.'/'.$email_get; ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msgf").hide();
				$("#success_msgf").show();
				$("#success_msgf").html(data.errmessage);
				document.getElementById('reset_pass_form').reset();
				scroll_to_div('success_msgf');
				setTimeout(function(){ window.location='<?php echo $base_url.'login-employer' ?>';  }, 3000);
			}
			else
			{	
				$("#success_msgf").hide();
				$("#error_msgf").show();
				$("#error_msgf").html(data.errmessage);
				scroll_to_div('error_msgf');
			}
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
</script>