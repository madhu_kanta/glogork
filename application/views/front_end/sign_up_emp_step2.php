<?php 
$custom_lable_arr = $custom_lable->language;
if(isset($forstep2svar))
{
	$forstep2svar = $forstep2;
	$mobile = ($forstep2svar['mobile']!="" && !is_null($forstep2svar['mobile'])) ? explode("-",$forstep2svar['mobile']) : "";
}
else
{
	$forstep2svar = "";
	$mobile = "";
}
//print_r($forstep2svar);
$country_code  = $this->common_front_model->get_country_code();
$upload_path_profile = $this->common_front_model->fileuploadpaths('company_logos',1);
?>
<form method="post" name="emplregform2" id="emplregform2" action="<?php echo $base_url.'sign_up_employer/signupemp2'; ?>" >
	<div class="alert alert-danger" id="message2" style="display:none" ></div>
	<div class="alert alert-success" id="success_msg2" style="display:none" ></div>
	<div class="panel panel-primary">
<div class="panel-heading th_bgcolor"><strong style="color:white;"><i class="fa fa-industry" aria-hidden="true"></i> <?php echo $custom_lable_arr['section_compant_det']; ?></strong></div>
<div class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
    	<div class="margin-top-20 clearfix"></div>
        								<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<h5><?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogo']; ?></h5>
													<img src="<?php if($forstep2svar!='' && $forstep2svar['company_logo'] != '' && !is_null($forstep2svar['company_logo']) && file_exists($upload_path_profile.'/'.$forstep2svar['company_logo']))
			{ echo $upload_path_profile.'/'.$forstep2svar['company_logo']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/company-logo.png<?php } ?>" id="blah1" class="blah1" alt="" />
                                                   <input type="file"  name="company_logo" id="company_logo" data-validation=" mime size" data-validation-max-size="2M" data-validation-error-msg-size="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval']; ?>" data-validation-allowing="jpg, jpeg, png, gif" data-validation-error-msg-mime="<?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoval1']; ?>"/>
													<small id="file" class="form-text text-muted red-only"><i><?php echo $custom_lable_arr['sign_up_emp_1stformuploadtypemes']; ?></i></small>
												</div>
												<div class="progress" id="imageprogressbardiv" style="display:none;">
													<div class="progress-bar progress-bar-success" id="imageprogressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0%<?php echo $custom_lable_arr['emp_edit_data_image_succ_comp']; ?></div>
												</div>
												<div class="margin-top-10"></div>
											</div>
										</div>
		<div class="margin-top-20 clearfix"></div>
        <h6><?php echo $custom_lable_arr['sign_up_emp_companydet']; ?><span class="red-only"> *</span></h6>
        <div class="form-group">
            <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo ($forstep2svar!='') ? $forstep2svar['company_name'] : ""?>" placeholder="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnyplace']; ?>"  style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnyval']; ?>">
        </div>
        <div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?><span class="red-only"> *</span></h6>
        <div class="form-group">
        <select name="company_type" id="company_type" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnytypeval']; ?>">
            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['company_type'])) { echo  $this->common_front_model->get_list_multiple('company_type_master','str','','str',$forstep2svar['company_type']);}else{ echo $this->common_front_model->get_list_multiple('company_type_master','str','','str','') ; } ?>
        </select>
        </div>
        <div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_cmpsjizelbl']; ?><span class="red-only"> *</span></h6>
        <div class="form-group">
        <select name="company_size" id="company_size" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofileedit_emp_cmpnysizeval']; ?>">
            <!--<option value=""><?php //echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?></option>-->
            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['company_size'])) { echo  $this->common_front_model->get_list_multiple('company_size_master','str','','str',$forstep2svar['company_size']);}else{ echo $this->common_front_model->get_list_multiple('company_size_master','str','','str','') ; } ?>
        </select>
        </div>
        <div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_cmpweblbl']; ?></h6>
        <div class="form-group">
        <input type="text" name="company_website" class="form-control" id="company_website" style="border-radius:0px;text-transform:lowercase;"  placeholder="<?php echo $custom_lable_arr['specify'] .' '. $custom_lable_arr['website'].' '. $custom_lable_arr['example_website'];  ?>" value="<?php echo ($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['company_website'])) ? $forstep2svar['company_website'] : ""; ?>"  data-validation="url" data-validation-optional="true" >
        </div>
        <div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_cmpprolbl']; ?></h6>
        <div class="form-group">
            <textarea class="form-control" id="company_profile" name="company_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $custom_lable_arr['myprofile_emp_cmpprofiltexz']; ?>"><?php echo ($forstep2svar!='') ? $forstep2svar['company_profile'] : ""?></textarea>
        </div>
        <div class="margin-top-20"></div>
        <h6><?php echo $custom_lable_arr['myprofile_emp_industrylbl']; ?><span class="red-only"> *</span></h6>
        <div class="form-group">
            <select name="industry" id="industry" style="padding:9px 0 9px 10px;" class=" city" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['myprofile_emp_industryvalmes']; ?>">
		 		<?php if($forstep2svar!='') { echo $this->common_front_model->get_list('industries_master','str','','str',$forstep2svar['industry']); }else{ echo $this->common_front_model->get_list('industries_master','str','','str','') ; } ?>
         	</select>
        </div>
        <div class="margin-top-20"></div>
        <div class="col-md-12">
            <div class="row">
            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformmob']; ?><span class="red-only"> *</span></h5>
            <div class="col-md-2 col-xs-12" style="margin-left:-15px;">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                    <select  class="form-control" name="mobile_c_code"  style="padding:9px;" id="mobile_c_code" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformmobcodevalmes']; ?>">
                        <?php
                        foreach ($country_code as $code)
                        { ?>
                            <option value="<?php echo $code['country_code'];?>" <?php if($mobile!='' && isset($mobile[0]) && $mobile[0]==$code['country_code'] ){ echo "selected" ; } ?>><?php echo $code['country_code'];?> (<?php echo $code['country_name'];?>)</option>	
                        <?php }
                        ?>
                    </select>
                </div>
             </div>   
            </div>
            <div class="col-md-10 col-xs-12" style="margin-left:-15px;">
                <input id="mobile_num" type="text" class="form-control" name="mobile_num" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformmobnumplacehold']; ?>" style="padding:9px 0 9px 10px;" value="<?php if($mobile!='' && isset($mobile[1])){ echo $mobile[1] ; } ?>" data-validation="number,length"  data-validation-length="min8,max13" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformmobnumvalmes']; ?>">
            </div>
        </div>
		</div>
        <div class="margin-top-20"></div>
        <div class="row">
            <div class="col-md-6">
            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformcnt']; ?><span class="red-only"> *</span></h5>
            <select name="country" id="country" class="country" style="padding:9px 0 9px 10px;" onChange="dropdownChange('country','city','city_list');" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformcntvalmes']; ?>">
             <?php if($forstep2svar!='') { echo  $this->common_front_model->get_list('country_list','str','','str',$forstep2svar['country']);}else{ echo $this->common_front_model->get_list('country_list','str','','str','') ; } ?>
            </select>
            </div>
            <div class="col-md-6">
            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformcity']; ?><span class="red-only"> *</span></h5>
            <select name="city" id="city" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformvalmescity']; ?>">
            <option value=""><?php echo $custom_lable_arr['sign_up_emp_1stformcitydfltopt']; ?></option>
            </select>
            </div>
        </div>
        <div class="margin-top-20 clearfix"></div>                                        
        <div class="form-group">
        <h5><?php echo $custom_lable_arr['sign_up_emp_1stformaddress']; ?><span class="red-only"> *</span></h5>
        <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
        <textarea id="address" class="form-control" name="address" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformaddress']; ?>" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformaddressvalmessup']; ?>" data-validation="required"><?php if($forstep2svar!=''){ echo $forstep2svar['address'] ; } ?></textarea>
    	</div>
        </div>
        
    	<div class="margin-top-20 clearfix"></div>
        <h5><?php echo $custom_lable_arr['sign_up_emp_1stformpincode']; ?><span class="red-only"> *</span></h5>
        <div class="form-group">
        	<input type="text" class="form-control" name="pincode" id="pincode" placeholder="Pincode" style="padding:9px 0 9px 10px;"  data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformpincodevalmes']; ?>" value="<?php if($forstep2svar!='' && $forstep2svar['pincode']!='0'){ echo $forstep2svar['pincode'] ; }else { echo "";} ?>" data-validation="required,number">
        </div>
    </div>
</div>

<ul class="list-inline pull-right margin-top-20">
    <li><button type="button" class="btn btn-sm btn-default" onClick="prev_step_mm('1')"><?php echo $custom_lable_arr['previous_emp']; ?></button></li>
    <?php /*  ?><li><button type="button" class="btn btn-default next-step">Skip</button></li><?php */ ?>
    <li><button type="submit" class="btn btn-sm btn-primary btn-info-full next-step th_bgcolor"><?php echo $custom_lable_arr['sign_up_emp_1stformsave']; ?></button></li>
</ul>
</div>
<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
<input type="hidden" name="hash_tocken_id_return2" id="hash_tocken_id_return2" value="<?php echo $this->security->get_csrf_hash(); ?>"/>
</form>
<script>
$(".prev-step").click(function (e) {
   var $active = $('.wizard .nav-tabs li.active');
   prevTab($active);
});
$(document).ready(function()
{
<?php
if(isset($forstep2svar['country']) && $forstep2svar['country'] !='')
{
?>
dropdownChange('country','city','city_list',setcityval);
function setcityval()
{
	$("#city").val('<?php echo $forstep2svar['city']; ?>');
}
<?php }  ?>
	return false;
});
</script>