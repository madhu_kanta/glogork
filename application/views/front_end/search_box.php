<!-- 

<form method="post" id="text_search_form" action="<?php echo $base_url; ?>job-listing/posted-job"> 
	<div class="">
		<div>
			<div class="">
				<input type="text" class="ico-01 search" id="text_home_search" name="text_home_search" placeholder="<?php echo $this->lang->line('search_text_placeholder'); ?>" value="" data-validation="required"  data-validation-error-msg="<?php echo $this->lang->line('for_search_warning'); ?>" data-validation-optional-if-answered="location_home_search"/>
			</div>
			<div class="">
				<input type="text" class="ico-02 search" id="location_home_search" data-validation-optional-if-answered="text_home_search"  name="location_home_search" placeholder="<?php echo $this->lang->line('search_location_placeholder'); ?>" data-validation-error-msg="<?php echo $this->lang->line('for_search_warning'); ?>" value="" />
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="user_agent" value="NI-WEB">
			</div>
			<div >	
				<button class="button button-default"><i class="fa fa-search"></i></button>
			</div>
		    <div class="clearfix"></div>
		</div>
	</div>
</form> -->
<form method="post" id="text_search_form" action="<?php echo $base_url; ?>job-listing/posted-job"> 
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<h2 class="bounceInDown animated">Find job</h2>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12 padding-browse">
				<input type="text" class=" search" id="text_home_search" name="text_home_search" placeholder="<?php echo $this->lang->line('search_text_placeholder'); ?>" value="" data-validation="required"  data-validation-error-msg="<?php echo $this->lang->line('for_search_warning'); ?>" data-validation-optional-if-answered="location_home_search"/>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12 padding-browse-2">
				<input type="text" class="search" id="location_home_search" data-validation-optional-if-answered="text_home_search"  name="location_home_search" placeholder="<?php echo $this->lang->line('search_location_placeholder'); ?>" data-validation-error-msg="<?php echo $this->lang->line('for_search_warning'); ?>" value="" />
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="user_agent" value="NI-WEB">
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">	
				<button class="button button-default new-button-t"><i class="fa fa-search font-i"></i></button>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2 col-xs-12 col-sm-12 padding-browse">
				<div class="browse-jobs bounceInUp animated">
					Browse job offers by <a href="<?php echo $base_url; ?>browse-industries" class="input-new-t"> Industry</a> or <a href="<?php echo $base_url; ?>browse-location" class="input-new-t-2">Location</a>
				</div>
			</div>
		</div>
	</div>
</form>
				