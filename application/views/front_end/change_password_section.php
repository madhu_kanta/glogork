<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span> Change Password</span></div>
								<div class="panel-body" style="padding:10px;">
                                <form method="post" name="editpassword" id="editpassword" action="<?php echo $base_url.'my_profile/editpassword'; ?>">
                                <div class="alert alert-danger" id="messagepassword" style="display:none" ></div>
								<div class="alert alert-success" id="success_msgpassword" style="display:none" ></div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<!--<!--<!---->
                                            <div class="margin-top-20"></div>
                                                <h5>Old Password :<span class="red-only"> *</span></h5>
                                                <div>
                                                <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Enter your old password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required"/ >
                                                </div>
                                                <div class="margin-top-20"></div>
                                                <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                                <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                <h5>New Password :<span class="red-only"> *</span></h5>
                                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter your new password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" />
                                                </div>
                                                	<div class="col-md-6 col-sm-6 col-xs-12">
                                                <h5>Confirm Password :<span class="red-only"> *</span></h5>
                                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm your password" value="" style="padding:9px 0 9px 10px;border-radius:0;" data-validation="required" />
                                                </div>
                                                </div>
                                                <div class="text-center">
													<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
												</div>
										</div>
									</div>
                                  </form>  
								</div>
							</div>