<?php
$custom_lable_arr = $custom_lable->language;
?>
	<!-- Blog Posts -->
	
		<div class="padding-right">

			<!-- Post -->
            <?php 
				if(isset($blog_data) && $blog_data !="" && is_array($blog_data) && count($blog_data) > 0 )
				{ 
					foreach($blog_data as $blog_data_single)
					{
						$date = "";
			?>
			<div class="post-container">
				<?php 
				if($blog_data_single['blog_image']!='' && file_exists('./assets/blog_image/'.$blog_data_single['blog_image'])){ ?>
				<div class="post-img"><a href="<?php echo $base_url; ?>blog/<?php echo $blog_data_single['alias']; ?>"><img src="<?php echo $base_url; ?>assets/blog_image/<?php echo $blog_data_single['blog_image']; ?>" alt="<?php echo $blog_data_single['title']; ?>"></a><div class=""><!--hover-icon--></div></div>
                <?php } ?>
				<div class="post-content">
					<a href="<?php echo $base_url; ?>blog/<?php echo $blog_data_single['alias']; ?>"><h3><?php echo $blog_data_single['title']; ?></h3></a>
					<div class="meta-tags">
						<span><?php $date = strtotime( $blog_data_single['created_on'] );
						echo $this->common_front_model->displayDate($blog_data_single['created_on'],'F j, Y ');
                         //echo $dateformat = date( 'F j, Y ', $date ); ?></span>
					</div>
                    <p>
					<?php 
						if(isset($blog_data_single['content']) && $blog_data_single['content'] !="")
						{
							echo substr(strip_tags($blog_data_single['content']), 0, 250);
						}
					?>
                    </p>
					<a class="button" href="<?php echo $base_url; ?>blog/<?php echo $blog_data_single['alias']; ?>"><?php echo $custom_lable_arr['read_more'];?></a>
				</div>
			</div>
			<?php }//foreach end
			} ?>
			<!-- Pagination -->
            <?php echo $this->common_front_model->rander_pagination('blog/allblogs',$blog_count); ?>
		</div>
	
	<!-- Blog Posts / End -->
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />