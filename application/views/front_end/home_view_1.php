<?php
$custom_lable_arr = $custom_lable->language;

function convert_seconds($visted_times = '') {
    //$visted_times=$row["visit_time"];
    $currnt_date = date('Y-m-d H:i:s');
    $date1 = strtotime($visted_times);
    $date2 = strtotime($currnt_date);
    $diff = abs($date2 - $date1);
    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
    $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
    $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
    if ($years != 0) {
        $visit_time = $years . ' Years';
    } elseif ($months != 0) {
        $visit_time = $months . ' Months';
    } elseif ($days != 0) {
        $visit_time = $days . ' Days';
    } elseif ($hours != 0) {
        $visit_time = $hours . ' Hours';
    } elseif ($minutes != 0) {
        $visit_time = $minutes . ' Minutes';
    } elseif ($seconds != 0) {
        $visit_time = $seconds . ' seconds';
    }
    return $visit_time;
}
?>
<!-- <style>
.pagination ul li a[rel="start"] {
    width: 70px;
}
.ui-autocomplete-loading
{
        width:100% !important;
}
.tokenfield .token
{
        height:30px !important;
}
.carousel-control
{
        color:#000000 !important;
        width:5% !important;
}
.tokenfield.token
{
        float:left !important;
}
.tokenfield.token-input
{
        float:left !important;
        width:auto !important;
}

.new-form-control
{

        /*background: #fff url("../images/ico-02.png") no-repeat scroll 96% 20px / 14px 21px !important;*/
        background: #fff  !important;
        border: medium none !important;
    box-sizing: border-box !important;
    float: left !important;
    font-size: 18px !important;
    font-weight: 500 !important;
    margin-right: 2% !important;
    padding: 16px 15px !important;
    width: 44.5% !important;
}
@media only screen and (min-width: 1290px) and (max-width: 2000px){
        .search-container { padding: 150px 0;  }
        .new-form-control { width: 44.5% !important; background-position: 95% 20px; !important  }
}

@media only screen and (min-width: 990px) and (max-width: 1289px) {

.search-container { padding: 150px 0;!important }
        .new-form-control { width: 43% !important; background-position: 95.5% 20px; !important}
}
@media only screen and (min-width: 768px) and (max-width: 989px) {
        .search-container { padding: 100px 0;  }
        .new-form-control { width: 43% !important; background-position: 95% 20px; !important  }
}
@media only screen and (min-width: 480px) and (max-width: 767px) {
        .search-container { padding: 100px 0;  }
        .new-form-control { width: 100% !important; margin:0px 0px 10px 0; background-position: 95% 20px; !important  }
}
@media only screen and (max-width: 479px){
        .search-container { padding: 100px 0;  }
        .new-form-control { width: 100% !important; margin:0px 0px 10px 0; background-position: 95% 20px; !important  }
}
.new-full-time{
 color: white !important;
    font-weight: bold;
    padding: 5px;
}
.new-full-time-high{
 color: white !important;
    font-weight: bold;
    padding: 0px 4px;
}
</style> -->

<!-- Banner
================================================== -->
<div id="banner" class="with-transparent-header parallax background" style="margin-top:0px !important;background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
    <div class="container">
        <div class="sixteen columns">

            <?php
            if (!$this->common_front_model->checkLoginfrontempl()) {
                ?>
                <div class="search-container" style="margin-top:50px;">
                    <!-- Form -->
                    <!-- <h2 class="bounceInDown animated">Find job</h2> -->
                    <div class="bounceInDown animated">
                        <?php $this->load->view('front_end/search_box'); ?>
                    </div>
                    <!-- Browse Jobs -->
                    <!-- <div class="browse-jobs bounceInUp animated">
                            Browse job offers by <a href="<?php //echo $base_url;   ?>browse-industries"> Industry</a> or <a href="<?php //echo $base_url;   ?>browse-location">Location</a>
                    </div> -->

                    <!-- Announce -->
                    <div class="announce bounceInUp animated">
                        <?php //echo $custom_lable_arr['index_page_ban_tit'];?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="search-container">
                    <h2 class="bounceInDown animated">Welcome <?php echo $this->session->userdata['jobportal_employer']['fullname']; ?></h2>
                </div>
            <?php } ?>

        </div>
    </div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<?php
$featured_industries = $this->common_front_model->get_featured_list('industries_master', 'id', 'industries_name,id,icon_name', '12');
if ($featured_industries != '' && is_array($featured_industries) && count($featured_industries) > 0) {
    ?>
    <div class="container">
        <?php
        if (!$this->common_front_model->checkLoginfrontempl()) {
            ?>
            <div class="sixteen columns ">
                <h3 class="margin-bottom-25"><?php echo $custom_lable_arr['pop_cat_idtit']; ?></h3>
                <div id="popular-categories" class="new-border-bottom">
                    <?php foreach ($featured_industries as $single_industry) { ?>
                        <form method="post" id="text_search_form" action="<?php echo $base_url; ?>job-listing/posted-job">
                            <div style="list-style:none;"><button type="submit" class="industries123 tufelheightforbtn" ><i class="fo25 <?php echo $single_industry['icon_name']; ?>"></i><span style="word-wrap:break-word" class="fo12"><?php echo $single_industry['industries_name']; ?></span></button></div>
                                        <!--<li><a href="javascript:void(0)"><i class="<?php echo $single_industry['icon_name']; ?>"></i> <?php echo $single_industry['industries_name']; ?></a></li>-->
                            <input type="hidden" name="search_industry[]" value="<?php echo $single_industry['id']; ?>">
                            <input type="hidden" name="csrf_job_portal" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="user_agent" value="NI-WEB">
                        </form>
                    <?php } ?>
                </div>

                <div class="clearfix"></div>
                <div class="margin-top-30"></div>

                <a href="<?php echo $base_url; ?>browse-industries" class="button centered" style="border-radius: 100px;padding: 9px 18px;background: #0e68c2;"><?php echo $custom_lable_arr['brow_cat_idtit']; ?></a>
                <div class="margin-bottom-50"></div>
            </div>
            <?php
        } else {

        }
        ?>
    </div>
<?php } ?>
<!-- recent jobs section -->
<?php if (!$this->common_front_model->checkLoginfrontempl()) { ?> <!--checking if employer sees only their jobs -->

    <div class="container">


        <?php
        $varforpass = array();
        $highlight_jobs_count = $this->home_model->gethighlight_jobs_count();
        $recent_jobs_count = $this->home_model->getrecent_jobs_count();
        if ($recent_jobs_count > 0) {
            $recent_jobs_post = $this->home_model->getrecent_jobs();
            $varforpass['total_post_count'] = $recent_jobs_count;
            $varforpass['emp_posted_job'] = $recent_jobs_post;
            $varforpass['custom_lable'] = $custom_lable;
            ?>
                        <!--<div class="<?php //if($highlight_jobs_count > 0){   ?>eleven <?php //}else{   ?> sixteen <?php //}   ?>columns">-->
            <div class="eleven columns margin-top-0" style="margin-top:-7px !important;">
                <div class="new-browse-m-t">
                    <h4 style="font-size:26px;font-family:'Poppins', sans-serif;font-weight:100;"><?php echo $custom_lable_arr['rec_job_indtit']; ?></h4>
                    <div id="js_action_msg_div"></div>
                    <div class="job-list" id="main_content_ajax">
                        <?php $this->load->view('front_end/page_part/job_search_result_view_index_new', $varforpass); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php } else {
            ?>

            <div class="eleven columns">
                <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>

        <?php } ?>
        <!-- Job Spotlight -->
        <?php
        if ($highlight_jobs_count > 0) {
            $highlight_jobs_post = $this->home_model->gethighlight_jobs();
            $varforpass_highlight['total_post_count'] = $highlight_jobs_count;
            $varforpass_highlight['emp_posted_job'] = $highlight_jobs_post;
            $varforpass_highlight['custom_lable'] = $custom_lable;
            $this->load->view('front_end/page_part/job_high_result_view_index_new', $varforpass_highlight);
        }
        ?>
        <!-- Job Spotlight end -->
        <!-- Advertisment start -->
        <div class="five columns ">
            <?php
            $advert_data = $this->common_front_model->getadvertisement('Level 1');
            if (isset($advert_data) && $advert_data != '' && is_array($advert_data) && count($advert_data) > 0) {
                ?>
                <div class="widget">
                    <h4 class="margin-top-25"><?php echo $custom_lable_arr['blog_right_ad_title']; ?></h4>
                    <?php
                    if ($advert_data['type'] == 'Image') {
                        ?>
                        <div class="margin-top-10">
                            <a href="<?php echo $advert_data['link']; ?>" target="_blank"><img src="<?php echo $base_url; ?>assets/advertisement/<?php echo $advert_data['image']; ?>" alt="Ad" /></a>
                            <?php
                        } elseif ($advert_data['type'] == 'Google') {
                            echo $advert_data['google_adsense'];
                            ?>

                        <?php }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- Advertisment end -->

    </div>
    <?php
} else {
    $current_page_url = (isset($_SERVER['https']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $custom_lable_array = $custom_lable->language;
    $var_for_pass['limit_per_page'] = $limit_per_page;
    ?>

    <div class="container margin-bottom-50">
        <div class="sixteen columns">
            <h3 class="margin-bottom-25"><?php echo $custom_lable_arr['browse_resume']; ?></h3>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12" style="overload:hidden;">
            <div id="main_content_ajax">
                <?php $this->load->view('front_end/page_part/manage_job_application_result_view_home', $this->data); ?>
            </div>
            <div id="view_js_details"></div>
        </div>

        <div class="clearfix"></div>
        <div class="margin-top-50"></div>


        <link href="<?php echo $base_url; ?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    </div> <?php } ?>
<!-- recent jobs section end -->

<?php if (!$this->common_front_model->checkLoginfrontempl()) { ?>
    <!-- our recruiters section starts-->
    <div class="container margin-bottom-50">
        <div class="sixteen columns">
            <div class="info-banner text-center" style="border:1px solid;padding:20px;background-color:#E9F7FE;">
                <div class="col-md-8 col-xs-12">
                    <div class="info-content">
                        <h3><?php echo $custom_lable_arr['contac_out_rec_indtit']; ?></h3>
                        <p><?php echo $custom_lable_arr['contac_out_rec_indtit1']; ?></p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 text-center" style="margin:0 auto;text-align:center;">
                    <a href="<?php echo $base_url; ?>recruiters" class="button" ><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['contac_out_rec_indtit2']; ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- our recruiters section ends-->
<?php } else { ?>
    <div class="container margin-bottom-50">
        <div class="sixteen columns">
            <div class="info-banner text-center" style="border:1px solid;padding:20px;background-color:#E9F7FE;">
                <div class="col-md-8 col-xs-12">
                    <div class="info-content">
                        <h3><?php echo $custom_lable_arr['applied_our_js_indtit3']; ?></h3>
                        <p><?php echo $custom_lable_arr['applied_our_js_indtit1']; ?></p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 text-center" style="margin:0 auto;text-align:center;">
                    <a href="<?php echo $base_url; ?>/job-application/manage-job-application" class="button" ><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['applied_our_js_indtit3']; ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="clearfix"></div>
<?php
if (!$this->common_front_model->checkLoginfrontempl()) {

    $this->load->view('front_end/company_logo_list');
}
?>

<div class="clearfix"></div>

<?php
if (!$this->common_front_model->checkLoginfrontempl()) {

    $this->load->view('front_end/recent_view_job_view');
    /* echo $_SERVER['REQUEST_URI']."<br>";
      echo $test1 =  substr('/',1);echo "<br>hello";
      echo $test2 = substr($test1, strpos($test1, "/") + 1); */
}
?>
<!-- comment for now will ask and make dynamic -->
<!--<div class="infobox">
        <div class="container">
                <div class="sixteen columns ">Start Building Your Own Job Board Now <a href="my-account.html">Get Started</a></div>
        </div>
</div>-->
<!-- comment for now will ask and make dynamic end -->
<!-- Recent Posts section starts -->
<?php
$blog_index = $this->common_front_model->get_featured_list('blog_master', 'id', 'blog_image,id,content,title,alias,created_on', '3');
if ($blog_index != '' && count($blog_index) > 0) {
    ?>
    <div class="container ">
        <div class="sixteen columns">
            <h3 class="margin-bottom-25"><?php echo $custom_lable_arr['art_post_indtit']; ?></h3>
        </div>
        <?php
        foreach ($blog_index as $blog_single_index) {
            $date = "";
            ?>
            <div class="one-third column">
                <div class="recent-post">
                    <div class="recent-post-img"><a href="<?php echo $base_url; ?>blog/<?php echo $blog_single_index['alias']; ?>">
                            <?php if ($blog_single_index['blog_image'] != '' && file_exists('./assets/blog_image/' . $blog_single_index['blog_image'])) { ?>
                                <img class="img-responsive" src="<?php echo $base_url; ?>assets/blog_image/<?php echo $blog_single_index['blog_image']; ?>" alt="<?php echo $blog_single_index['title']; ?>" ></a><!--<div class="hover-icon">-->
                        <?php } else { ?>
                            <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-image-found.jpg"  alt="<?php echo $blog_single_index['title']; ?>" ></a><!--<div class="hover-icon">-->
                        <?php } ?>
                        <!--</div>--></div>


                    <a href="<?php echo $base_url; ?>blog/<?php echo $blog_single_index['alias']; ?>"><h4><?php echo $blog_single_index['title']; ?></h4></a>
                    <div class="meta-tags small">
                        <span><?php
                            $date = strtotime($blog_single_index['created_on']);
                            echo $this->common_front_model->displayDate($blog_single_index['created_on'], 'F j, Y ');
                            ?></span>
                        <!--<span><a href="#">0 Comments</a></span>-->
                    </div>
                    <p style="min-height: 75px; max-height: 75px;"><?php
                        if (isset($blog_single_index['content']) && $blog_single_index['content'] != "") {
                            echo substr(strip_tags($blog_single_index['content']), 0, 100);
                        }
                        ?></p>
                    <a href="<?php echo $base_url; ?>blog/<?php echo $blog_single_index['alias']; ?>" class="button"><?php echo $custom_lable_arr['read_more']; ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<!-- Recent Posts section ends -->

<!-- Legislazione Posts section starts -->
<?php
$legislazione_index = $this->common_front_model->get_featured_list('legislazione_master', 'id', 'legislazione_image,id,content,title,alias,created_on', '3');
if ($legislazione_index != '' && count($legislazione_index) > 0) {
    ?>
    <div class="container ">
        <div class="sixteen columns">
            <h3 class="margin-bottom-25"><?php echo $custom_lable_arr['leg_post_indtit']; ?></h3>
        </div>
        <?php
        foreach ($legislazione_index as $legislazione_single_index) {
            $date = "";
            ?>
            <div class="one-third column">
                <div class="recent-post">
                    <div class="recent-post-img"><a href="<?php echo $base_url; ?>legislazione/<?php echo $legislazione_single_index['alias']; ?>">
                            <?php if ($legislazione_single_index['legislazione_image'] != '' && file_exists('./assets/legislazione_image/' . $legislazione_single_index['legislazione_image'])) { ?>
                                <img class="img-responsive" src="<?php echo $base_url; ?>assets/legislazione_image/<?php echo $legislazione_single_index['legislazione_image']; ?>" alt="<?php echo $legislazione_single_index['title']; ?>" ></a><!--<div class="hover-icon">-->
                        <?php } else { ?>
                            <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-image-found.jpg"  alt="<?php echo $legislazione_single_index['title']; ?>" ></a><!--<div class="hover-icon">-->
                        <?php } ?>
                        <!--</div>--></div>


                    <a href="<?php echo $base_url; ?>legislazione/<?php echo $legislazione_single_index['alias']; ?>"><h4><?php echo $legislazione_single_index['title']; ?></h4></a>
                    <div class="meta-tags small">
                        <span><?php
                            $date = strtotime($legislazione_single_index['created_on']);
                            echo $this->common_front_model->displayDate($legislazione_single_index['created_on'], 'F j, Y ');
                            ?></span>
                        <!--<span><a href="#">0 Comments</a></span>-->
                    </div>
                    <p style="min-height: 75px; max-height: 75px;"><?php
                        if (isset($legislazione_single_index['content']) && $legislazione_single_index['content'] != "") {
                            echo substr(strip_tags($legislazione_single_index['content']), 0, 100);
                        }
                        ?></p>
                    <a href="<?php echo $base_url; ?>legislazione/<?php echo $legislazione_single_index['alias']; ?>" class="button"><?php echo $custom_lable_arr['read_more']; ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<!-- Legislazione Posts section ends -->













<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/animate.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/slideanim.css">

<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/new-home.css"  rel="stylesheet">


<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<?php
if ($this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') != "") {
    ?>
            <!--<link href="<?php echo $base_url; ?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<?php } ?>
<script>

    $('#text_home_search').tokenfield({
        autocomplete: {
            source: function (request, response) {
                jQuery.get("<?php echo $base_url; ?>job_listing/get_search_suggestion/" + request.term, {
                }, function (data) {
                    response(data);
                });
            },
            delay: 100
        },
        showAutocompleteOnFocus: true,
        createTokensOnBlur: true,
    });
    $('#text_home_search').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function (index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
        });
    });


    $('#location_home_search').tokenfield({
        autocomplete: {
            source: function (request, response) {
                jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/id/" + request.term, {
                }, function (data) {
                    response(data);
                });
            },
            delay: 100
        },
        showAutocompleteOnFocus: true,
        createTokensOnBlur: true,
    });

    $('#location_home_search').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function (index, token) {
            if (token.value === event.attrs.value)
                event.preventDefault();
        });
    });

    $.validate({
        form: '#text_search_form',
        modules: 'security,logic',
        onError: function ($form) {
            scroll_to_div('text_search_form', -300);
        }
    });

    function set_time_out_msg(div_id)
    {
        setTimeout(function () {
            $('#' + div_id).html('');
        }, 8000);
    }

    $(document).ready(function (e) {

        $('#text_home_search').tokenfield('setTokens', []);
        $('#text_home_search').val('');
        $('#location_home_search').tokenfield('setTokens', []);
        $('#location_home_search').val('');

        $('.tokenfield').find('.token-input').autocomplete({
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            }
        });


        $('#text_search_form').keyup(function (e) {
            if (e.which === 38 || e.which === 40) {
            }

        });
        if ($("#ajax_pagin_ul").length > 0)
        {
            load_pagination_code();
        }
    });
    function apply_for_job(job_id)
    {

<?php
if (!$this->common_front_model->get_userid() && $this->common_front_model->get_userid() == '') {
    //$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
    $return_after_login_url = (isset($_SERVER['https']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $this->session->set_userdata($return_after_login_url);
    ?>
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("return_after_login_url", '<?php echo $return_after_login_url; ?>');
            }

            var message = '<?php echo $custom_lable_arr['please_login_apply']; ?>';
            $('#js_action_msg_div').html('');
            $('#js_action_msg_div').slideUp('Slow');
            $('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
            $('#js_action_msg_div').slideDown('Slow');
            set_time_out_msg('js_action_msg_div');
            scroll_to_div('js_action_msg_div', -100);
            return false;
<?php }
?>

        var c = confirm("<?php echo $custom_lable_arr['confirm_apply_job']; ?>");
        if (c == true)
        {
            var hash_tocken_id = $("#hash_tocken_id").val();
            var datastring = 'csrf_job_portal=' + hash_tocken_id + '&job_id=' + job_id + '&user_agent=NI-WEB';
            show_comm_mask();
            $.ajax({
                url: "<?php echo $base_url . 'job_seeker_action/apply_for_job' ?>",
                type: 'post',
                data: datastring,
                dataType: "json",
                success: function (data)
                {
                    $("#hash_tocken_id").val(data.token);
                    if ($.trim(data.status) == 'success')
                    {
                        $('#js_action_msg_div').html('');
                        $('#js_action_msg_div').slideUp('Slow');
                        $('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + data.errmessage + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
                        $('#js_action_msg_div').slideDown('Slow');
                        if ($('.apply_for_job' + job_id).length)
                        {
                            $('.apply_for_job' + job_id).removeAttr('onClick');
                            $('.apply_for_job' + job_id).css('cursor', 'default');
                            $('.apply_for_job' + job_id).html('<i class="fa fa-check"></i>  <?php echo $custom_lable_arr['already_apply']; ?>');
                        }
                        set_time_out_msg('js_action_msg_div');
                        scroll_to_div('js_action_msg_div', -100);
                    } else
                    {
                        $('#js_action_msg_div').html('');
                        $('#js_action_msg_div').slideUp('Slow');
                        $('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + data.errmessage + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
                        $('#js_action_msg_div').slideDown('Slow');
                        set_time_out_msg('js_action_msg_div');
                        scroll_to_div('js_action_msg_div', -100);
                    }

                    hide_comm_mask();
                }
            });

            return false;
        }
    }
    function jobseeker_action(action, action_for, action_id)
    {
<?php
if (!$this->common_front_model->get_userid() && $this->common_front_model->get_userid() == '') {
    // $return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
    //$return_after_login_url = array('return_after_login_url'=>'/');
    //$this->session->set_userdata($return_after_login_url);
    $return_after_login_url = (isset($_SERVER['https']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $this->session->set_userdata($return_after_login_url);
    ?>
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("return_after_login_url", '<?php echo $return_after_login_url; ?>');
            }


            var message = '<?php echo $this->lang->line('please_login'); ?>';
            $('#js_action_msg_div').html('');
            $('#js_action_msg_div').slideUp('Slow');
            $('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
            $('#js_action_msg_div').slideDown('Slow');
            scroll_to_div('js_action_msg_div', -100);
            set_time_out_msg('js_action_msg_div');
            return false;
<?php }
?>
        show_comm_mask();
        var hash_tocken_id = $("#hash_tocken_id").val();
        var datastring = 'csrf_job_portal=' + hash_tocken_id + '&action=' + action + '&action_for=' + action_for + '&action_id=' + action_id + '&user_agent=NI-WEB';
        $.ajax({
            url: "<?php echo $base_url . 'job-seeker-action/seeker-action' ?>",
            type: 'post',
            data: datastring,
            success: function (data)
            {
                $("#hash_tocken_id").val(data.token);

                $('#js_action_msg_div').html('');
                $('#js_action_msg_div').slideUp('Slow');
                scroll_to_div('js_action_msg_div', -100);


                if (data.status == 'success')
                {


                    if (action == 'save_job')
                    {
                        $('.save_job_button' + action_id).html('<i class="fa fa-check"></i>  <?php echo $this->lang->line('saved'); ?>');
                        $('.save_job_button' + action_id).removeAttr('onClick');
                        $('.save_job_button' + action_id).attr('onClick', 'jobseeker_action("remove_save_job","job",' + action_id + ')')
                    }
                    if (action == 'remove_save_job')
                    {
                        $('.save_job_button' + action_id).html('<i class="fa fa-save"></i>  <?php echo $this->lang->line('save'); ?>');
                        $('.save_job_button' + action_id).removeAttr('onClick');
                        $('.save_job_button' + action_id).attr('onClick', 'jobseeker_action("save_job","job",' + action_id + ')')
                    }
                    if (action == 'block_emp')
                    {
                        $('.emp_block_action' + action_id).html('<span class="fa fa-check"></span>  <?php echo $this->lang->line('blocked'); ?>');
                        $('.emp_block_action' + action_id).removeAttr('onClick');
                        $('.emp_block_action' + action_id).attr('onClick', 'jobseeker_action("unblock_emp","Emp",' + action_id + ')')
                    }
                    if (action == 'unblock_emp')
                    {
                        $('.emp_block_action' + action_id).html('<i class="fa fa-lg fa-ban" aria-hidden="true"></i> <?php echo $this->lang->line('block'); ?>');
                        $('.emp_block_action' + action_id).removeAttr('onClick');
                        $('.emp_block_action' + action_id).attr('onClick', 'jobseeker_action("block_emp","Emp",' + action_id + ')')
                    }


                    $('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + data.errmessage + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
                    $('#js_action_msg_div').slideDown('Slow');
                    set_time_out_msg('js_action_msg_div');
                    //scroll_to_div('for_scrool_div',-100);
                    scroll_to_div('js_action_msg_div', -100);
                } else
                {

                    $('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + data.errmessage + '.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
                    $('#js_action_msg_div').slideDown('Slow');
                    set_time_out_msg('js_action_msg_div');
                    //scroll_to_div('for_scrool_div',-100);
                    scroll_to_div('js_action_msg_div', -100);

                }

                hide_comm_mask();
            }

        });

        return false;
    }

    $(document).ready(function (e) {
        $('#main_content_ajax').show();
        if ($("#ajax_pagin_ul").length > 0)
        {
            load_pagination_code();
        }

    });




    function show_details(div_id)
    {
        $('#' + div_id).slideDown();
        $('#close_box_' + div_id).slideDown();
        scroll_to_div(div_id, -200);
    }

    function hide_details(div_id)
    {
        $('#' + div_id).slideUp();
        $('#close_box_' + div_id).slideUp();
    }


    function close_js_details()
    {
        $('.hide-close-box').hide();
        $('#view_js_details').hide();
        $('#main_content_ajax').show();
        scroll_to_div('main_content_ajax');
    }

    function view_js_details(js_id)
    {
        $('.hide_job_details').hide();
        $('.hide-close-box').hide();

        show_comm_mask();
        var hash_tocken_id = $("#hash_tocken_id").val();
        var datastring = 'csrf_job_portal=' + hash_tocken_id + '&js_id=' + js_id + '&user_agent=NI-WEB';
        $.ajax({
            url: "<?php echo $base_url . 'employer-profile/view-js-details' ?>",
            type: 'post',
            data: datastring,
            success: function (data)
            {
                $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
                $('#view_js_details').show();
                $('#view_js_details').html(data);
                $('#main_content_ajax').hide();
                hide_comm_mask();
            }
        });

    }

    function download_resume(file, jidpkd)
    {
        show_comm_mask();
        var hash_tocken_id = $("#hash_tocken_id").val();
        var datastring = 'csrf_job_portal=' + hash_tocken_id + '&file=' + file + '&jidpkd=' + jidpkd + '&user_agent=NI-WEB';
        $.ajax({
            url: "<?php echo $base_url . 'job_application/download_resume' ?>",
            type: 'post',
            data: datastring,
            success: function (data)
            {
                $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
                if (data.status == 'success')
                {
                    window.open(data.file_download);
                    //alert(data.file_download);
                } else
                {
                    alert(data.errmessage);
                }

            }
        });
        hide_comm_mask();
    }

    //End newly added script

    function view_js_details(js_id)
    {
        $('.hide_job_details').hide();
        $('.hide-close-box').hide();

        show_comm_mask();
        var hash_tocken_id = $("#hash_tocken_id").val();
        var datastring = 'csrf_job_portal=' + hash_tocken_id + '&js_id=' + js_id + '&user_agent=NI-WEB';
        $.ajax({
            url: "<?php echo $base_url . 'employer-profile/view-js-details' ?>",
            type: 'post',
            data: datastring,
            success: function (data)
            {
                $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
                $('#view_js_details').show();
                $('#view_js_details').html(data);
                $('#main_content_ajax').hide();
                hide_comm_mask();
            }
        });

    }
    function download_resume(file, jidpkd)
    {
        show_comm_mask();
        var hash_tocken_id = $("#hash_tocken_id").val();
        var datastring = 'csrf_job_portal=' + hash_tocken_id + '&file=' + file + '&jidpkd=' + jidpkd + '&user_agent=NI-WEB';
        $.ajax({
            url: "<?php echo $base_url . 'job_application/download_resume' ?>",
            type: 'post',
            data: datastring,
            success: function (data)
            {
                $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
                if (data.status == 'success')
                {
                    window.open(data.file_download);
                    //alert(data.file_download);
                } else
                {
                    alert(data.errmessage);
                }
            }
        });
        hide_comm_mask();
    }

    //End newly added script
    $(document).ready(function () {
        var search_frm_sub_var = 0;
        $("#text_search_form").submit(function (e) {
            if (search_frm_sub_var == 0)
                e.preventDefault();
            else
                return true;
            var location_home_search = $('#location_home_search').val();

            if (location_home_search != '')
            {
                var hash_tocken_id = $("#hash_tocken_id").val();
                var datastring = '&csrf_job_portal=' + hash_tocken_id + '&location_home_search=' + location_home_search;
                $.ajax({
                    url: "<?php echo $base_url . 'job-listing/check-city' ?>",
                    type: 'post',
                    data: datastring,
                    success: function (data)
                    {

                        if (data == 1) {
                            search_frm_sub_var = 1;
                            $("#text_search_form").submit();
                        } else {
                            search_frm_sub_var = 0;
                            alert(data);
                        }

                    }
                });
            } else
            {
                search_frm_sub_var = 1;
                $("#text_search_form").submit();
            }
        });
    });


//alert(localStorage.getItem("lastname"));
</script>
<input type="hidden" name="testvale" value="<?php
if ($this->session->userdata('return_after_login_url')) {
    echo $this->session->userdata('return_after_login_url') . '/test';
}
?>">

