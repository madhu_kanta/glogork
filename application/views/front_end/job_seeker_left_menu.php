<?php
$upload_path_profile ='assets/js_photos/';
$custom_lable_array = $this->lang->language;
$custom_lable_arr = $custom_lable->language;
if($this->router->fetch_class() == 'my_profile')
{
    $percentage_stored = $this->my_profile_model->getprofile_completeness($user_data_values['id']);
}
?>
<div class="">
    <div class="panel-group" id="accordion">
    	<?php
		if(isset($this->common_front_model->page_profile) && $this->common_front_model->page_profile ==1)
		{
            
        ?>
        <style>


  .circle {
  width: 200px;
  margin:-9px 20px 20px;
  display: inline-block;
  position: relative;
  text-align: center;
  vertical-align: top;
}
.circle strong {
  position: absolute;
  top: 70px;
  left: 0;
  width: 100%;
  text-align: center;
  line-height: 45px;
  font-size: 43px;
}
            </style>
        <div class="box-profile">
                <div class="row new-row-2">
                    <div class="col-sm-3 col-md-2" style="margin-top: 20px;">
                    <div class="circle" id="circle-b">
                        <strong><img id="<?php if(isset($blah2)){?>blah2<?php }else{?>blah<?php }?>" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic']))
                { echo $upload_path_profile.'/'.$user_data['profile_pic']; ?><?php }else{ echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" class="progress-img" alt="your image"/></strong>
                    </div>
                        
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="img-box"><?php //echo $user_data_values['profile_pic'];?>
        
							<div class="img-box-p">
								<p class="per"><?php echo $percentage_stored; ?>%</p>
							</div>
							<!--<a href="#" class="btn-job-new"><i class="fa fa-plus"></i> Upload Photo</a>-->
                            <div id="image_val" style="position:fixed;  left:25%;  left:25%; top:18%; z-index:9999; opacity:0"></div>
							<div class="file-upload">
                           
								<label for="profile_img" class="file-upload__label"> <i class="fa fa-plus"></i> Upload Photo</label>
                                
								<input id="profile_img" name="profile_img" class="file-upload__input" type="file" name="file-upload"  onchange="displayprofile(); uploadprofilepic();">
                                
							</div>
						</div>
						
					</div>
					<div class="col-md-12">
						<p class="text-center m-h-g"><?php echo $user_data['fullname'];?></p>
                        <?php $jbrole = $this->my_profile_model->getdetailsfromid('role_master','id',$user_data['job_role'],'role_name'); ?>
						<p class="text-center m-h-g" style="font-size:12px;"><?php echo ($user_data['job_role']!='0' && $this->common_front_model->checkfieldnotnull($user_data['job_role']) && count($jbrole) > 0 && $jbrole['role_name']!='' ) ? $jbrole['role_name'] :  $custom_lable_arr['notavilablevar']; ?></p>
					</div>
				</div>
			</div>
			<div class="panel panel-default" style="border-radius:0px;position: relative;top: -35px;">
  <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
    <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowjsl('collapseOne')" >Edit your profile <span class="glyphicon glyphicon-edit"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowjsl('collapseOne')"><i id='icon_collapseOne' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;" ></i></a></h4>
  </div>
  <div id="collapseOne" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="widget" style="margin-bottom:0px;">
            <ul class="list-group nav nav-pills nav-stacked" ><!--nav nav-pills--> <!--or  nav-tabs nav-menu tabs-left-->
                
                <?php if(isset($blah2)){?>
                <li class="active ">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('profile_snapshot');"  data-toggle="tab"><i class="glyphicon glyphicon-user"></i> <?php echo $custom_lable_arr['myprofile_personal_det_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('other_details');"  data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_other_det_tit']?></a>
                    </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('social_links');"  data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_social_med_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('education_view');"  data-toggle="tab"><i class="glyphicon glyphicon-education"></i> <?php echo $custom_lable_arr['myprofile_educ_det_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('workdetails_view');"  data-toggle="tab"><i class="glyphicon glyphicon-plus"></i> <?php echo $custom_lable_arr['myprofile_work_det_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('language_view');"  data-toggle="tab"><i class="glyphicon glyphicon-plus"></i> <?php echo $custom_lable_arr['myprofile_lang_known_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('resumedwn_view');"  data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_resume_dwn_tit']?></a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="list-group-item" onclick="editsection('changepassword_view');"  data-toggle="tab"><i class="fa fa-key"></i> &nbsp; Change password</a>
                </li>

                <?php }else{?>
                <li class="active ">
                    <a class="list-group-item" href="#profile_snapshot_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-user"></i> <?php echo $custom_lable_arr['myprofile_personal_det_tit']?></a>
                </li>
                <li class="">
                    <a class="list-group-item" href="#other_details_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_other_det_tit']?></a>
                 </li>
                <li class="">
                    <a class="list-group-item" href="#socaila_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php echo $custom_lable_arr['myprofile_social_med_tit']?></a>
                 </li>
                <li class="">
                    <a class="list-group-item" href="#education_view_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-education"></i> <?php echo $custom_lable_arr['myprofile_educ_det_tit']?></a>
                 </li>
                <li class="">
                    <a class="list-group-item" href="#workdetails_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-plus"></i> <?php echo $custom_lable_arr['myprofile_work_det_tit']?></a>
                </li>
                <li class="">
                        <a class="list-group-item" href="#langknown_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-plus"></i> <?php echo $custom_lable_arr['myprofile_lang_known_tit']?></a>
                </li>
                <!-- <li class="">
                    <a class="list-group-item" href="#resume_dwn_viewdiv" data-toggle="tab"><i class="glyphicon glyphicon-file"></i> <?php //echo $custom_lable_arr['myprofile_resume_dwn_tit']?></a>
                </li> -->
                <!-- <li class="">
                    <a class="list-group-item" href="#change_password" data-toggle="tab"><i class="fa fa-key"></i> &nbsp;Change password</a>
                 </li> -->
                <li class="">
                <a class="list-group-item" href="#delete_profile" data-toggle="tab"><i class="fa fa-trash-o"></i> &nbsp;Delete Profile</a>
                </li>
                <?php }?>
                
            </ul>
        </div>
    </div>
  </div>
</div>
        <?php
		}
		else
		{
		?>
        	<div class="panel panel-default" style="border-radius:0px;position: relative;top: -41px;margin-top: 49px;">
  <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
    <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowjsl('collapseOne')">Edit your profile <span class="glyphicon glyphicon-edit"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowjsl('collapseOne')"><i id='icon_collapseOne' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;" ></i></a></h4>
  </div>
  <div id="collapseOne" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="widget" style="margin-bottom:0px;">
            <ul class="list-group nav nav-pills nav-stacked" ><!--nav nav-pills--> <!--or  nav-tabs nav-menu tabs-left-->            	<li class="">
            		<a class="list-group-item" href="<?php echo $base_url; ?>my_profile" ><i class="fa fa-check"></i> &nbsp;My profile </a>
                </li>
            </ul>
        </div>
    </div>
  </div>
</div>
        <?php
		}
		?>                
       <!--<div class="panel panel-default" style="border-radius:0px;">
          <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Account Setting <span class="glyphicon glyphicon-lock"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;"></i></a></h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" >
                        <li class="">
                            <a class="list-group-item" href="#change_password" data-toggle="tab"><i class="fa fa-key"></i> Change password</a>
                         </li>
                        <li class="">
                            <a class="list-group-item" href="#view_profile_picture" data-toggle="tab"><i class="fa fa-key"></i> Profile Picture</a>
                         </li>
                    </ul>
                </div>
            </div>
          </div>
        </div>-->
      <div class="panel panel-default" style="border-radius:0px;position: relative;top: -41px;">
          <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowjsl('collapseThree')"><?php echo $custom_lable_array['suggested_job_l_menu_lbl']; ?>  <span class="glyphicon glyphicon-list"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowjsl('collapseThree')"><i id='icon_collapseThree' class="indicator glyphicon glyphicon-chevron-down  pull-right" style="margin:10px; 10px;"></i></a></h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" >
                       
                        <li class="<?php if(isset($page) && $page=='suggested_job') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-listing/suggested-job"  ><i class="fa fa-check"></i> </i> &nbsp;<?php echo $custom_lable_array['suggested_job_l_menu_lbl']; ?> </a>
                        </li> 
                       
                    </ul>
                </div>
            </div>
          </div>
        </div> 
       
       <div class="panel panel-default" style="border-radius:0px;position: relative;top: -50px;">
          <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" onclick="change_arrowjsl('collapsefour')"><?php echo $custom_lable_array['activity_list']; ?>  <span class="glyphicon glyphicon-list"></span></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" onclick="change_arrowjsl('collapsefour')"><i id='icon_collapsefour' class="indicator glyphicon glyphicon-chevron-down pull-right" style="margin:10px; 10px;"></i></a></h4>
          </div>
          <div id="collapsefour" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="widget" style="margin-bottom:0px;">
                    <ul class="list-group nav nav-pills nav-stacked" >
                       
                        <li class="<?php if(isset($page) && $page=='apply_job') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-apply-job-list"  ><i class="fa fa-check"></i> </i> &nbsp;<?php echo $custom_lable_array['apply_job_list']; ?> </a>
                        </li> 
                       
                        <li class="<?php if(isset($get_list) && $get_list=='view_job') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list/<?php echo base64_encode('1');?>/<?php echo base64_encode('view_job');?>"  ><i class="fa fa-eye"></i> &nbsp;<?php echo $custom_lable_array['js_view_job_left_p_lbl']; ?> </a>
                        </li>
                        <li class="<?php if(isset($get_list) && $get_list=='save_job') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list/<?php echo base64_encode('1');?>/<?php echo base64_encode('save_job');?>" ><i class="fa fa-save"></i> &nbsp;<?php echo $custom_lable_array['js_save_job_left_p_lbl']; ?></a>
                        </li>
                        <li class="<?php if(isset($get_list) && $get_list=='like_job') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list/<?php echo base64_encode('1');?>/<?php echo base64_encode('like_job');?>"  ><i class="fa fa-heart"></i> &nbsp;<?php echo $custom_lable_array['js_like_job_left_p_lbl']; ?></a>
                        </li>
                        <li class="<?php if(isset($get_list) && $get_list=='follow_emp') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list-emp/<?php echo base64_encode('1');?>/<?php echo base64_encode('follow_emp');?>"><i class="fa fa-check"></i> &nbsp;<?php echo $custom_lable_array['js_follow_emp_left_p_lbl']; ?> </a>
                        </li>
                        <li class="<?php if(isset($get_list) && $get_list=='block_emp') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list-emp/<?php echo base64_encode('1');?>/<?php echo base64_encode('block_emp');?>"><i class="fa fa-ban"></i> &nbsp;<?php echo $custom_lable_array['js_block_emp_left_p_lbl']; ?> </a>
                        </li>
                        <li class="<?php if(isset($get_list) && $get_list=='like_emp') {echo "active";} ?>">
                            <a class="list-group-item" href="<?php echo $base_url; ?>job-seeker-action/js-activity-list-emp/<?php echo base64_encode('1');?>/<?php echo base64_encode('like_emp');?>"><i class="fa fa-heart"></i> &nbsp;<?php echo $custom_lable_array['js_like_emp_left_p_lbl']; ?> </a>
                        </li>
                    </ul>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
<script>

var change_arrow_process = 0 ;
var y = ['collapseOne','collapseTwo','collapseThree','collapsefour'];
function change_arrowjsl(id)
{	
	if(change_arrow_process == 0)
	{
		setTimeout(function(){
		change_arrow_process = 1;		
		$.each( y, function( key, value )
		{
			var className = $('#icon_'+value).attr('class');
			var idName = $('#icon_'+value).attr('id');
			if ( $('#'+value).hasClass("collapse in"))
			{				
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-down pull-right');
			}
			else
			{
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-up pull-right');				
			}
		});
		change_arrow_process = 0;
		},400);
	}	
}
</script>