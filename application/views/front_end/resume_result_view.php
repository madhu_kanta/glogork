<?php
$custom_lable_array = $custom_lable->language;
$text_home_search = '';
$text_search_lbl_val ='';
if($this->input->post('text_home_search') && $this->input->post('text_home_search')!='')
{
	$text_home_search = $this->input->post('text_home_search');
	$text_search_data = explode(',',$text_home_search);
	if(isset($text_search_data) && is_array($text_search_data) && count($text_search_data) > 0)
	{
		foreach($text_search_data as $search_data)
		{
			$get_string = explode('-',$search_data);
			$lable = $get_string[0];
			if(isset($get_string[1]) && $get_string[1]!='')
			{
				$lable = $get_string[1];
			}
			if(isset($get_string[0]) &&  trim($get_string[0]) == 'R' && is_numeric($lable))
			{   
				$lable = $this->common_front_model->valueFromId('role_master',$lable,'role_name');
			}
			$text_search_lbl[] = $lable;
			$text_search_value[] = $search_data;
			//$text_search_array_data[] = array('value'=>$search_data,'label'=>$lable);
		}
	}
	$text_search_lbl_val = implode(',',$text_search_lbl);
}
$var_for_pass['limit_per_page'] = $limit_per_page;
?>

<!-- Titlebar ================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/staffing.png); background-size:cover;">
	<div class="container">
		<div class="ten columns">
			<h2><i class="fa fa-briefcase"></i>  Browse Listed jobseekers </h2>
			<span><?php //echo $custom_lable_array['total_job_count_msg']; ?>Total jobseeker found <span id="total_job_count">0</span></span>
		</div>

<?php if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
				{ ?>
		<div class="six columns">
			<a href="<?php echo $base_url; ?>job-listing" target="_blank" class="button"><?php echo $custom_lable_array['post_new_job_text']; ?></a>
		</div>
        <?php } ?>

	</div>
</div>
<!-- Titlebar end================================================== -->

<!-- Content ================================================== -->
<div class="container">
	<div class="sixteen columns">
		<div class="info-banner text-center" style="border:1px solid;padding:20px;background-color:#E9F7FE;">
			<div class="col-md-8 col-xs-12">
				<div class="info-content">
					<h3><?php echo $custom_lable_array['contac_our_js_indtit2']; ?></h3>
					<p><?php echo $custom_lable_array['contac_our_js_indtit1']; ?></p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 text-center" style="margin:0 auto;text-align:center;">
				<a href="<?php echo $base_url; ?>browse-job-seekers"  target="_blank" class="button" ><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_array['contac_our_js_indtit3']; ?></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="container" >
<div class="margin-top-20"></div>
<hr>
	<div id="job_search_part">
  		<?php  $this->load->view('front_end/page_part/resume_search_option_view',$var_for_pass); ?>
    </div>
	
	<div class="eleven columns margin-top-10" id="for_scrool_div"> 
	<div class="">
		<h4><?php echo $custom_lable_array['browse_resume_text']; ?></h4>
        <?php /* ?>
		<form  method="post" id="text_search_form" class="list-search" onSubmit="return resume_option_search();">
			<div class="row">
				<div class="col-md-11"> 
					<input type="text" name="text_search" placeholder="<?php echo $custom_lable_array['search_resume_placeholder']; ?>" class="form-control" id="text_search" style="height:52px !important;padding:10px !important;" value="<?php echo $text_search_lbl_val; ?>" />
				</div>	
				<div class="col-md-1"> 
					<button style="height:40px;"><i class="fa fa-search"></i></button>
				</div>
			</div>
			
			<div class="clearfix"></div>
		</form>
        <?php */ ?>
		<div id="js_action_msg_div"></div>
		<ul class="job-list">
            <div id="main_content_ajax">
               <?php  
				   $this->load->view('front_end/page_part/resume_search_result_view',$var_for_pass);
				 ?>
            </div>
        </ul>
       
      <div class="clearfix"></div>
	</div>
	</div>
	
</div>
<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
					
                        <div class="small-dialog-headline">
                          <i class="fa fa-envelope"></i> <?php echo  $custom_lable_array['send_msg'];?>
                        </div>
                        <div class="clearfix margin-bottom-10"></div>
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                               
                        <div class="small-dialog-content">
                            <form  method="post"  id="send_msg_form" >
                                <div>
                                <input type="text" name="subject" data-validation="required" placeholder="<?php echo $custom_lable_array['sub_mes_com']; ?>" value="" />
                                </div>
                                <div>
                                <textarea placeholder="<?php echo $custom_lable_array['mes_mes_com']; ?>" data-validation="required" name="content" ></textarea>
                                </div>
                                <button class="send" ><i class="fa fa-check"></i> <?php echo $custom_lable_array['send_msg']; ?></button>
                                <input type="hidden" value="NI-WEB" name="user_agent">
                                <input type="hidden" value="send_message" name="action">
                                <input type="hidden" value="" name="sender_id" id="sender_id">
                                <input type="hidden" value="" name="receiver_id" id="receiver_id">
                                <input type="hidden" value="" name="email" id="email">
                            </form> 
                        </div>
                    
				</div>
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script>
function get_suggestion_list(list_id,return_val,get_list)
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'sign_up/get_suggestion_list/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
			return_val: return_val,
			get_list: get_list,
		  };
		},
	  }
	});
}

<?php

if(($this->input->post('text_home_search') && $this->input->post('text_home_search')!='') || $this->input->post('location_home_search') && $this->input->post('location_home_search')!='')
{ ?>
   var loading = 0;
  resume_option_search();
<?php }
?>
$('#text_search').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>browse-resume/get_search_suggestion/"+request.term, {
          }, function (data) {
            response(data);
		});
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#text_search').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
$(document).ready(function(e) {
	
	if($("#ajax_pagin_ul").length > 0)
	{   
		load_pagination_code();
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();  
	
	$('.tokenfield').find('.token-input').autocomplete({
    focus: function (event, ui) {
        event.preventDefault();
        $(this).val(ui.item.label);
    }
	});    
	get_suggestion_list('search_skill','key_skill_name','skill_list');
	get_suggestion_list('search_location','id','city_list');
	get_suggestion_list('search_company','company_name','company_list');
});
var loading = 0;
function resume_option_search()
{ 
	if(loading == 0)
	{
	loading = 1;
	show_comm_mask();
	var data_string = $('#resume_search_option').serialize();
	var text_search = $('#text_search').val();
	var hash_tocken_id = $("#hash_tocken_id").val();
	if(hash_tocken_id=='' || hash_tocken_id=='null' || hash_tocken_id=='undefined' || hash_tocken_id==undefined || hash_tocken_id==null)
	{
		var hash_tocken_id = '<?php echo $this->security->get_csrf_hash(); ?>';
	}
	var datastring = data_string+'&is_ajax=1'+'&csrf_job_portal='+hash_tocken_id+'&text_search='+text_search;
		
	$.ajax({	
		url : "<?php echo $base_url.'browse-resume' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$('#main_content_ajax').html(data);
			scroll_to_div('main_content_ajax');
			update_tocken($("#hash_tocken_id_temp").val());
			$("#hash_tocken_id_temp").remove();
			load_pagination_code();
			scroll_to_div('main_content_ajax',-100);
			hide_comm_mask();
			loading = 0;
		}
	});	
	 
	}
	return false;
}
function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html('');  }, 8000);
}
function show_modelpp(class_id)
	{ 
	    document.getElementById('send_msg_form').reset();
		var sender_id = $('.'+class_id).data('sender_id');
		var receiver_id = $('.'+class_id).data('receiver_id');
		var email = $('.'+class_id).data('email');
		$('#sender_id').val(sender_id);
		$('#receiver_id').val(receiver_id);
		$('#email').val(email);
		$.magnificPopup.open({
			items: {
				src: '#small-dialog',
			},
			type: 'inline'
		});
	}
$.validate({
    form : '#send_msg_form',
    modules : 'security',
    onError : function($form) {
	  $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('error_msg',-100);
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
	  set_time_out_msg('error_msg');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#send_msg_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'job_application/send_message_js' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('send_msg_form').reset();
				scroll_to_div('success_msg');
				set_time_out_msg('success_msg');
			}
			else
			{
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('error_msg');
				set_time_out_msg('error_msg');
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });	
</script>
