<?php $facebook_access = 'No';
$g_plus_accces = 'No';
$custom_lable_array = $custom_lable->language;

$js_fb_data = $this->session->userdata('js_fb_data');
if($js_fb_data != '')
{
	$js_fb_data = $this->session->userdata('js_fb_data');
	$FBID = $js_fb_data['FBID'];
	$social_first_name = $js_fb_data['fb_first_name'];
	$social_last_name = $js_fb_data['fb_last_name'];
	$social_email = $js_fb_data['fb_email'];
	$social_image = $js_fb_data['fb_image'];
	$social_image_name = isset($js_fb_data['fb_image_name']) ? $js_fb_data['fb_image_name'] : '';
}
            
$js_gplus_data = $this->session->userdata('js_gplus_data');
if($js_gplus_data != '')
{
	$js_gplus_data = $this->session->userdata('js_gplus_data');
	$gplus_id = $js_gplus_data['gplus_id'];
	$social_first_name = $js_gplus_data['gp_first_name'];
	$social_last_name = $js_gplus_data['gp_last_name'];
	$social_email = $js_gplus_data['gp_email'];
	$social_image = $js_gplus_data['gp_image'];
	$social_image_name = isset($js_gplus_data['gp_image_name']) ? $js_gplus_data['gp_image_name'] : '';
}

 ?>	
<!-- Titlebar ================================================== -->
<!--<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2><i class="ln ln-icon-Lock-User" style="font-size:40px;vertical-align:middle;"></i> My Account</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>My Account</li>
				</ul>
			</nav>
		</div>

	</div>
</div>-->
<style>
/*.terms_accept.form-error
{
	width: 278px !important;
	margin-top: 29px!important;
}*/
</style>
<!-- Titlebar end ================================================== -->
<?php /*?><div class="container" id="for_scrool_div">
		<div class="my-account" style="border:3px solid;padding:10px;">
			<ul class="tabs-nav">
                <li id="login_tab" onClick="for_active_tab();" class="<?php if(isset($perameter) && $perameter=='login') {echo "active";} ?>"><a href="#tab1"><?php echo $custom_lable_array['reg_tab_login']; ?></a></li>
                <li id="register_tab"  onClick="for_active_tab();" class="<?php if(isset($perameter)  && $perameter=='signup') {echo "active";} ?>"><a href="#tab2"><?php echo $custom_lable_array['reg_tab_register']; ?></a></li>
			</ul>

			<div class="tabs-container">
            
            	 <div class="input-group col-xs-12 col-sm-12 clearfix">
                       <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                       <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                       <?php if($this->session->flashdata('user_log_out'))
							{ ?>
                            <div class="alert alert-success">
                            	<?php echo $this->session->flashdata('user_log_out'); ?>
                            </div>
                       <?php } ?>
                       
                       <?php if($this->session->flashdata('user_log_error'))
							{ ?>
                            <div class="alert alert-danger">
                            	<?php echo $this->session->flashdata('user_log_error'); ?>
                            </div>
                       <?php } ?>
                       
                       
                </div>
				<!-- Login -->
				<div class="tab-content <?php if(isset($perameter) && $perameter=='login')  {echo "active";} ?>" id="tab1" style="display: none;">
					<form method="post" class="login" id="login_form"> 
						<?php  if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='')
								 {
								 	$js_login_detail = json_decode(get_cookie('js_login_detail'));
                                 } 
						 ?>
						<p class="form-row form-row-wide">
							<label for="username"><?php echo $custom_lable_array['login_lbl_username'] ?>:
								<i class="ln ln-icon-Male"></i>
								<input type="text" class="input-text" name="username" id="username"  placeholder="<?php echo $custom_lable_array['login_username_placeholder']; ?>" value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->username!='') {echo $js_login_detail->username;} ?>" data-validation="required"/>
							</label>
						</p>

						<p class="form-row form-row-wide">
							<label for="password"><?php echo $custom_lable_array['reg_lbl_pass']; ?>:
								<i class="ln ln-icon-Lock-2"></i>
								<input class="input-text" type="password" name="password" id="password" placeholder="<?php echo $custom_lable_array['login_password_placeholder']; ?>" value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->password!='') {echo $js_login_detail->password;} ?>" data-validation="required"/>
							</label>
						</p>
                        
                        <p class="form-row text-center">
                            <label for="rememberme" class="rememberme">
                           <input name="rememberme" type="checkbox" id="rememberme" <?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='') {echo "checked";} ?> value="Yes" /><?php echo $custom_lable_array['reg_lbl_remember']; ?></label>
                        </p>

						<p class="form-row text-center">
                            <input name="action" type="hidden"  value="login" />
                            <input name="user_agent" type="hidden"  value="NI-WEB" /> 
							<input type="submit" class="button border fw margin-top-10" name="login" value="<?php echo $custom_lable_array['login_submit_lbl']; ?>" />
							
						</p>

						<p class="lost_password text-center">
							<a href="<?php echo $base_url ?>login/forgot-password" ><?php echo $custom_lable_array['forgot_password_lbl']; ?></a>
						</p>
						
                    </form>
                    <hr style="margin-bottom:-10px;">
						<?php
						if(isset($social_access_detail) && $social_access_detail!='')
						{ ?>
						  <div class="col-xs-12 col-sm-12 col-md-12 separator social-login-box"> <br />
						  <p class="text-center"><?php echo $custom_lable_array['OR']; ?></p>
                          <?php 
						  foreach($social_access_detail as $social_access)
						   {
							  if($social_access['social_name'] == 'facebook' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
								  { $facebook_access = 'Yes';?>
								   <button href="#" onClick="window.location='<?php echo $base_url; ?>sign-up/fb-signin'"  class="btn btn-block facebook-bg" role="button"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size:20px;"></i><?php echo $custom_lable_array['fb_login_lbl']; ?></button>
								   <div class="margin-top-10"></div>
							<?php } 
							if($social_access['social_name'] == 'google' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
								  {
									  include_once 'gplus/gpConfig.php';
									  $authUrl = $gClient->createAuthUrl();
									  $g_plus_accces = 'Yes';?>
							 <button href="#" class="btn btn-block google-plus-bg" onClick="window.location='<?php echo filter_var($authUrl, FILTER_SANITIZE_URL); ?>'" role="button"><i class="fa fa-google-plus" aria-hidden="true" style="font-size:20px;"></i><?php echo $custom_lable_array['gplus_login_lbl']; ?></button>
							<div class="margin-top-10"></div>
							<?php }
							   }?>
                        </div>
				   <?php } ?>
				</div>

				<!-- Register -->
				<div class="tab-content <?php if(isset($perameter) && $perameter=='signup')  {echo "active";} ?>" id="tab2" style="display:none;">

					<form method="post" class="register" id="sign_up_form">

					<p class="form-row form-row-wide">
						<label for="username2"><?php echo $custom_lable_array['reg_lbl_fullname']; ?>:
							<i class="ln ln-icon-Male"></i>
							<input type="text" class="input-text" name="fullname" id="fullname" data-validation="required" value="<?php if( (isset($social_first_name) && $social_first_name!='' ) && (isset($social_last_name) && $social_last_name!='' ) ) {echo $social_first_name .' '.$social_last_name; } ?>" />
						</label>
					</p>

					<p class="form-row form-row-wide">
						<label for="email2"><?php echo $custom_lable_array['reg_lbl_email']; ?>:
							<i class="ln ln-icon-Mail"></i>
							<input type="text" class="input-text" name="email" id="email" data-validation="required,email" value="<?php if(isset($social_email) && $social_email!='') {echo $social_email; } ?>" />
						</label>
					</p>

					<p class="form-row form-row-wide">
						<label for="password1"><?php echo $custom_lable_array['reg_lbl_pass']; ?>:
							<i class="ln ln-icon-Lock-2"></i>
							<input class="input-text" type="password" name="password" id="password" data-validation="required,length" data-validation-length="min6"/>
						</label>
					</p>

					<p class="form-row form-row-wide">
						<label for="password2"><?php echo $custom_lable_array['reg_lbl_c_pass']; ?>:
							<i class="ln ln-icon-Lock-2"></i>
							<input class="input-text" type="password" name="c_password" id="c_password"  data-validation="required,confirmation" data-validation-confirm="password"/>
						</label>
					</p>

					<p class="form-row">
                         <input name="action" type="hidden"  value="sign_up" />
                         <input name="facebook_id" type="hidden" value="<?php if(isset($FBID) && $FBID!='') {echo $FBID;}?>" />
                         <input name="gplus_id" type="hidden" value="<?php if(isset($gplus_id) && $gplus_id!='') {echo $gplus_id;}?>" />
                         <input name="user_agent" type="hidden" value="NI-WEB" />
                         <input type="submit" class="button border fw margin-top-10" name="register" value="<?php echo $custom_lable_array['reg_submit_lbl']; ?>" />
					</p>
						
					</form>
                    
                    <hr style="margin-bottom:-10px;">
						<?php
						if(isset($social_access_detail) && $social_access_detail!='')
						{ ?>
						  <div class="col-xs-12 col-sm-12 col-md-12 separator social-login-box"> <br />
						  <p class="text-center"><?php echo $custom_lable_array['OR']; ?> </p>
                          <?php 
						  
							  if($facebook_access == 'Yes')
								  {?>
								   <button onClick="window.location='<?php echo $base_url; ?>sign-up/fb-signup'" class="btn btn-block facebook-bg" role="button"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size:20px;padding:5px;"></i> <?php echo $custom_lable_array['fb_signup_lbl']; ?></button>
								   <div class="margin-top-10"></div>
							<?php } 
							 if($g_plus_accces == 'Yes')
								  { ?>
							 <button href="#"  onClick="window.location='<?php echo filter_var($authUrl, FILTER_SANITIZE_URL); ?>'" class="btn btn-block google-plus-bg" role="button"><i class="fa fa-google-plus" aria-hidden="true" style="font-size:20px;"></i> <?php echo $custom_lable_array['gplus_signup_lbl']; ?></button>
							<div class="margin-top-10"></div>
							<?php }
							   ?>
                        </div>
				   <?php } ?>
				</div>
			</div>
		</div>
	</div><?php */?>
 <div id="titlebar1" class="photo-bg margin-bottom-0" style="background: url(<?php echo $base_url;?>assets/front_end/images/banner-home-02.jpg);no-repeat;background-size:cover;">
	<div class="container">
		<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
        <div class="panel panel-default" style="padding:0px;background-color:rgba(255,255,255,.7);">
            <div class="panel-heading box-shadow-radius">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-user"></span> <?php echo $custom_lable_array['login_register_lbl']; ?></h4>
            </div>
            <div class="panel-body">
                <div class="row" style="margin:0px;">
                    <div class="col-md-7 col-sm-7 col-xs-12" style="border-right: 1px dotted #ffffff;padding-right: 30px;">
                        <!-- Nav tabs -->
                        <ul class="tabs-nav">
							<li id="login_tab" onClick="for_active_tab();" class="<?php if(isset($perameter) && $perameter=='login') {echo "active";} ?>"><a href="#tab1"><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_array['reg_tab_login']; ?></a></li>
							<li id="register_tab"  onClick="for_active_tab();"  class="<?php if(isset($perameter)  && $perameter=='signup') {echo "active";} ?>"><a href="#tab2"><span class="fa fa-lg fa-user-plus"></span> <?php echo $custom_lable_array['reg_tab_register']; ?></a></li>
						</ul>
						
                        
                        
						<div class="tabs-container">
                        
                           <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                           <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                           <?php if($this->session->flashdata('user_log_out'))
                                { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('user_log_out'); ?>
                                </div>
                           <?php } ?>
                           
                           <?php if($this->session->flashdata('user_log_error'))
                                { ?>
                                <div class="alert alert-danger">
                                    <?php echo $this->session->flashdata('user_log_error'); ?>
                                </div>
                           <?php } ?>
                        
                        <?php  if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='')
								 {
								 	$js_login_detail = json_decode(get_cookie('js_login_detail'));
                                 } 
						 ?>
							<div class="tab-content <?php if(isset($perameter) && $perameter=='login')  {echo "active";} ?>" id="tab1" style="display: none;">
                            
								<form method="post" class="login" id="login_form" name="login_form">			
									<div class="group">
										<input type="text" class="text-filed" placeholder="<?php echo $custom_lable_array['login_username_placeholder'] ?>" style="padding:9px 0 9px 10px;" name="username" id="username"   value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->username!='') {echo $js_login_detail->username;} ?>" data-validation="required"><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="group">
										<input class="text-filed" type="password" name="password" id="password" placeholder="<?php echo $custom_lable_array['login_password_placeholder']; ?>" value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->password!='') {echo $js_login_detail->password;} ?>" data-validation="required" style="padding:9px 0 9px 10px;" ><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="row margin-bottom-0">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="form-row pull-left">
												<label for="rememberme" class="rememberme">
												<input  name="rememberme" type="checkbox" id="rememberme" <?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='') {echo "checked";} ?> value="Yes" /><?php echo $custom_lable_array['reg_lbl_remember']; ?></label>				
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p class="lost_password pull-right">
												<a href="<?php echo $base_url ?>login/forgot-password" style="color:grey;"><?php echo $custom_lable_array['forgot_password_lbl']; ?></a>
											</p>
										</div>
									</div>
                                    
									<div class="row margin-top-5" id="google_recaptcha">
									<div class="col-md-8" style="padding-top:10px;">
                                    <?php
									$rand_captcha = array( mt_rand(0,9), mt_rand(1, 9) );
									$captcha = array('captcha_main_login' => $rand_captcha);
									$this->session->set_userdata($captcha); 	
									$captcha_list = $this->session->userdata('captcha_main_login');
									
									echo $custom_lable_array['security_text'].' '. $captcha_list[0]?> + <?=$captcha_list[1]?> ? (<?php echo $custom_lable_array['security_question']; ?>) 
										</div>
										<div class="col-md-4 group">
											
                                            <input type="text"  data-validation="spamcheck"
          data-validation-captcha="<?=( $captcha_list[0] + $captcha_list[1] )?>" class="text-filed text-center"  name="validate_captcha"  id="captcha"><span class="highlight"></span><span class="bar"></span>
										</div>
									</div>
                                    <?php /* ?>
									<div class="row margin-top-5" id="google_recaptcha">
										<!--<div class="col-md-4">
											<img src="<?php echo $base_url;?>assets/front_end/images/captcha.jpg" />
										</div>-->
										<div class="col-md-8 group">
											
                                            <input type="text"  class="text-filed" id="captcha"  data-validation='recaptcha' placeholder="Captcha"  data-validation-error-msg='<?php echo $custom_lable_array['captcha_msg']; ?>' style="padding:9px 0 9px 10px;" ><span class="highlight"></span><span class="bar"></span>
										</div>
									</div>
                                    <?php */ ?>
									<p class="form-row text-center margin-top-0">
                                    <input name="action" type="hidden"  value="login" />
                                    <input name="user_agent" type="hidden"  value="NI-WEB" /> 
                                    <input type="hidden" name="login_attepmt_value" id="login_attepmt_value" value="<?php echo $this->session->userdata('login_attempt'); ?>">
										<input type="submit" class="button border fw margin-top-10" name="login" value="<?php echo $custom_lable_array['login_submit_lbl']; ?>" />
									</p>
									
                                     <!-- <?php
									//if(isset($social_access_detail) && $social_access_detail!='' && is_array($social_access_detail) && count($social_access_detail) > 0)
									//{ ?>
									   <div class="row margin-top-0">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="login-or">
												<hr class="hr-or">
												<span class="span-or"><?php //echo $custom_lable_array['OR']; ?></span>
											</div>
											<div class="row">
                                            <?php
										//foreach($social_access_detail as $social_access)
										//{	
											//if($social_access['social_name'] == 'facebook' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
								           // { $facebook_access = 'Yes';?>
												<div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
													<a href="#" onClick="window.location='<?php // echo $base_url; ?>sign-up/fb-signin'" class="btn btn-block facebook-bg box-shadow-radius" data-toggle="tooltip" title="<?php //echo $custom_lable_array['fb_login_lbl']; ?>"><i class="fa fa-lg fa-facebook-square"></i> <?php //echo $custom_lable_array['fb_login_lbl']; ?></a>
												</div>
                                       <?php //} 
									 // if($social_access['social_name'] == 'google' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
										//  {
											//   include_once 'gplus/gpConfig.php';
											//   $authUrl = $gClient->createAuthUrl();
											//   $g_plus_accces = 'Yes';?>
														<div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
															<a href="#"  onClick="window.location='<?php //echo filter_var($authUrl, FILTER_SANITIZE_URL); ?>'" class="btn btn-block google-plus-bg box-shadow-radius" data-toggle="tooltip" title="<?php //echo $custom_lable_array['gplus_login_lbl']; ?>"><i class="fa fa-google-plus"></i> <?php //echo $custom_lable_array['gplus_login_lbl']; ?></a>
														</div>
									  <?php //}
							       //  }?>
											
												
											</div>
										</div>
									</div>
                                    <?php //} ?> -->
								</form>
                                
                                 <a  href="<?php echo $base_url; ?>login-employer" style="text-decoration:none;float:right;"><?php echo $custom_lable_array['ask_emp']; ?></a>
							</div>
							
							<div class="tab-content <?php if(isset($perameter) && $perameter=='signup')  {echo "active";} ?>" id="tab2" style="display: none;">
								<form method="post" class="register" id="sign_up_form" >
                                <div class="row">
                               
                               		<!--<div class="col-md-12">
												
												<div class="col-md-6">
													
													<div class="input-group">
                                                    <h5><?php echo $custom_lable_array['gender_lbl']; ?><span class="red-only"> *</span></h5>
														<div id="radioBtn" class="btn-group">
															<a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Y" data-value='Male' style="font-size: 14px;padding: 5px 26px;"><?php echo $custom_lable_array['gender_lbl_male']; ?></a>
															<a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="N" data-value='Female' style="font-size: 14px;padding: 5px 24px;"><?php echo $custom_lable_array['gender_lbl_female']; ?></a>
														</div>
														<input type="hidden" name="gender" id="gender" value="Male">
													</div>
												</div>
											</div>-->
									
                                    <div class="group margin-top-20 ">
                                        <div class="col-sm-12">
                                        	<!--<h5><?php echo $custom_lable_array['gender_lbl']; ?><span class="red-only"> *</span></h5>-->
														<div id="radioBtn" class="btn-group">
															<a class="btn btn-primary btn-sm active" data-toggle="happy" onClick="change_gender('Male');" id="gender_male" data-title="Y" data-value='Male' style="font-size: 14px;padding: 5px 56px;"><?php echo $custom_lable_array['gender_lbl_male']; ?></a>
															<a class="btn btn-primary btn-sm notActive" data-toggle="happy" onClick="change_gender('Female');" data-title="N" data-value='Female' style="font-size: 14px;padding: 5px 54px;"><?php echo $custom_lable_array['gender_lbl_female']; ?></a>
														</div>
														<input type="hidden" name="gender" id="gender" value="Male">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>	
                                    <div class="margin-bottom-20"></div>	
									<div class="group margin-top-10">
                                    <div class="col-sm-4">
                      					<select name="title" id="title" class="form-control margin-top-10" data-validation="required">
                                        <option value="">Select Title</option>
                                          <?php $title_arr =  $this->common_front_model->get_personal_titles();
										  if(isset($title_arr) && $title_arr!='' && is_array($title_arr) && count($title_arr) > 0)
										  {
										   foreach($title_arr as $title)
										   { 
										   		$gn_class = '';
										   		if(strtolower(trim($title['personal_titles'])) == 'mr.' || strtolower(trim($title['personal_titles'])) == 'mr')
												{
													$gn_class = 'gn_classmale';
												}
												else if(strtolower(trim($title['personal_titles'])) == 'ms.' || strtolower(trim($title['personal_titles'])) == 'ms' || strtolower(trim($title['personal_titles'])) == 'mrs' || strtolower(trim($title['personal_titles'])) == 'mrs.' || strtolower(trim($title['personal_titles'])) == "ma'am" || strtolower(trim($title['personal_titles'])) == "ma'am.")
												{
													$gn_class = 'gn_classfemale';
												}
										   ?>
                                           	<option value="<?php echo $title['id']; ?>" class="<?php echo $gn_class; ?>"   ><?php echo $title['personal_titles']; ?></option>
										   <?php }
										   }
										   ?>
                                        </select><span class="highlight"></span><span class="bar"></span>
                                    </div>
                                    </div>
                                    <div class="group">
                                     <div class="col-sm-8">
										<input type="text" class="text-filed" name="fullname" id="fullname" data-validation="required" value="<?php if( (isset($social_first_name) && $social_first_name!='' ) && (isset($social_last_name) && $social_last_name!='' ) ) {echo $social_first_name .' '.$social_last_name; } ?>" style="padding:9px 0 9px 10px;" placeholder="<?php echo $custom_lable_array['reg_lbl_fullname']; ?>" /><span class="highlight"></span><span class="bar"></span>
                                        </div>
									</div>
                                 </div>
									<div class="group">
										<input type="text" class="text-filed" name="email" id="email" data-validation="required,email" value="<?php if(isset($social_email) && $social_email!='') {echo $social_email; } ?>" placeholder="<?php echo $custom_lable_array['reg_lbl_email']; ?>" style="padding:9px 0 9px 10px;" /><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="group">
										<input type="password" class="text-filed" placeholder="<?php echo $custom_lable_array['reg_lbl_pass']; ?>" style="padding:9px 0 9px 10px;" name="password" id="password" data-validation="required,length" data-validation-length="min6"><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="group">
										<input type="password" class="text-filed" placeholder="<?php echo $custom_lable_array['reg_lbl_c_pass']; ?>" style="padding:9px 0 9px 10px;" name="c_password" id="c_password"  data-validation="required,confirmation" data-validation-confirm="password"><span class="highlight"></span><span class="bar"></span>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-3">
											<span class="button-checkbox">
												<button type="button" class="btn btn-sm capitalized" data-color="info" tabindex="7"><?php echo $custom_lable_array['i_agree'];  ?></button>
                                                 <input name="action" type="hidden"  value="sign_up" />
                                                 <input name="facebook_id" type="hidden" value="<?php if(isset($FBID) && $FBID!='') {echo $FBID;}?>" />
                                                 <input name="gplus_id" type="hidden" value="<?php if(isset($gplus_id) && $gplus_id!='') {echo $gplus_id;}?>" />
                                                 <input name="user_agent" type="hidden" value="NI-WEB" />
                                                 <input type="hidden" name="social_image_name" value=" <?php if(isset($social_image_name) && $social_image_name!='') {echo $social_image_name;}?>">
												<input type="checkbox"  name="t_and_c" id="t_and_c" class="hidden terms_accept" data-validation="required"  data-validation-error-msg="<?php echo $custom_lable_array['accept_terms_msg']; ?>">
											</span>
										</div>
										<div class="col-xs-12 col-sm-9 col-md-9 small">
											<?php echo $custom_lable_array['by_clicking']; ?> <strong class="label label-primary"><?php echo $custom_lable_array['reg_tab_register']; ?></strong>, <?php echo $custom_lable_array['you_are'];  ?><a href="<?php echo $base_url;?>cms/terms-of-service" target="_blank"><?php echo $custom_lable_array['terms_and_condition']; ?></a> <?php echo $custom_lable_array['agree_msg_lbl'];  ?> 
										</div>
									</div>
									<p class="form-row">
										<input type="submit" class="button border fw margin-top-10" name="register" value="<?php echo $custom_lable_array['reg_submit_lbl']; ?>" />
									</p>
									
									<?php
								// 		if(isset($social_access_detail) && $social_access_detail!='')
						// 		//{ ?>
                        <!--              <div class="row margin-top-0">
						 				<div class="col-xs-12 col-sm-12 col-md-12">
											 <div class="login-or">
						 						<hr class="hr-or">
						 						<span class="span-or"><?php //echo $custom_lable_array['OR']; ?></span>
						 					  </div>
										<div class="row"> -->
										<?php 
						  
						  // 					  if($facebook_access == 'Yes')
						  // 						 // {?>
						  <!--                          <div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
													   <a href="#" onClick="window.location='<?php //echo $base_url; ?>sign-up/fb-signup'" class="btn btn-block facebook-bg box-shadow-radius" data-toggle="tooltip" title="<?php //echo $custom_lable_array['fb_signup_lbl']; ?>"><i class="fa fa-lg fa-facebook-square"></i> <?php //echo $custom_lable_array['fb_signup_lbl']; ?></a>
												   </div> -->
										  <?php //} 
						  // 					 if($g_plus_accces == 'Yes')
													//{ ?>
						  <!-- 						<div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
												  <a href="#"  OnClick="window.location='<?php //echo filter_var($authUrl, FILTER_SANITIZE_URL); ?>'" class="btn btn-block google-plus-bg box-shadow-radius" data-toggle="tooltip" title="<?php //echo $custom_lable_array['gplus_signup_lbl']; ?>"><i class="fa fa-google-plus"></i> <?php //echo $custom_lable_array['gplus_signup_lbl']; ?></a>
												   </div> -->
											   <?php //}?>
  <!-- 												
											   </div>
										   </div>
									  </div> -->
						   <?php   //} ?>
								</form>
							</div>
						</div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 hidden-xs">
                        <?php if(isset($jsregcms) && is_array($jsregcms) && count($jsregcms) > 0){ ?>
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3 class="margin-top-5"><?php echo $custom_lable_array['read_more']; ?></h3>
                            </div>
                        </div>
						<hr class="colorgraph">
						<?php  echo $this->common_front_model->checkfieldnotnull($jsregcms['page_content']) ? $jsregcms['page_content'] : '';   ?>
                        <?php } ?>
                        
                      <p><a href="<?php echo $base_url;?>sign-up" class="btn btn-info btn-block box-shadow-radius margin-top-10"><?php echo $custom_lable_array['get_started']; ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
        </div>
		</div>
  
	


	</div>
    </div>
    

<script>
/*$("input").keydown(function (e) {
   $('#captcha').css('display','none');
	if (e.which == 9)
	{
	  $('#captcha').css('display','none');
	}

});
$("input").blur(function (e) {
   $('#captcha').css('display','none');
	if (e.which == 9)
	{
	  $('#captcha').css('display','none');
	}

});*/

$('#register_tab').click(function(){
	$('#gender_male').trigger('click');
	change_gender('Male');
	});
$(document).ready(function(e) {
    change_gender('Male');
});

/*$('select')
 .on('beforeValidation', function(value, lang, config) {
  $('.chosen-select').css('display','block');
 });
$('select')
	.on('validation', function(value, lang, config) {
	  $('.chosen-select').css('display','none');
	 
 });*/
			 

$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    var gender =  $(this).data('value');
	$('#gender').val(gender);
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})




$('input')
    .on('beforeValidation', function(value, lang, config) {
		$('#t_and_c').removeClass('hidden');
		var login_attepmt = $('#login_attepmt_value').val();
		if(login_attepmt=='' || login_attepmt < 2)
		{
			$('#captcha').attr('data-validation-skipped','1');
		}
		else
		{
			//$('#captcha').css('display','block');
			$('#captcha').removeAttr('data-validation-skipped');
		}
	 });

$('input')
    .on('afterValidation', function(value, lang, config) {
		$('#t_and_c').addClass('hidden');
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	$('[data-toggle="popover"]').popover();
	//$('#captcha').css('display','none'); 
	$('#google_recaptcha').slideUp();
	<?php
	if($this->session->userdata('login_attempt')!='' && $this->session->userdata('login_attempt') >= 2)
	{ ?>  
	 $('#google_recaptcha').slideDown();
<?php } ?>
});
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});

function for_active_tab()
{
	$("#error_msg").slideUp();
	$("#success_msg").slideUp();
	document.getElementById('sign_up_form').reset();
	document.getElementById('login_form').reset();
}

$.validate({
    form : '#login_form',
    modules : 'security',
    reCaptchaSiteKey: '6LcyexsUAAAAABFtYaQ42upGalCg8j68YWxnAY8H',
    reCaptchaTheme: 'light',
    onError : function($form) {
	  //$('#captcha').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	   scroll_to_div('wrapper');
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#login_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'Login/login' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('login_form').reset();
				scroll_to_div('wrapper');
				
				/*Return user to last attempt  activity*/
				var return_after_login_url = localStorage.getItem("return_after_login_url");
				localStorage.removeItem("return_after_login_url");
				if(return_after_login_url!='' && return_after_login_url!=undefined && return_after_login_url!='undefined')
				{
					setTimeout(function(){ window.location = return_after_login_url }, 500);
					return false;
				}
				/*Return user to last attempt activity end*/
				<?php 
				/*if($this->session->userdata('return_after_login_url') && $this->session->userdata('return_after_login_url')!='')
				{
					$return_after_login_url =  substr($this->session->userdata('return_after_login_url'),1);   
					$return_after_login_url = substr($return_after_login_url, strpos($return_after_login_url, "/") + 1);
					//$this->session->unset_userdata('return_after_login_url');
					?>
					
					setTimeout(function(){ window.location='<?php echo $base_url.$return_after_login_url; ?>';  }, 500);
					return false;
					<?php 
				}*/
				?>
				setTimeout(function(){ window.location='<?php echo $base_url.'my_profile' ?>';  }, 500);
			}
			else
			{
				if(data.login_attempt >= 2)
				{
					$('#login_attepmt_value').val(data.login_attempt)
					$('#google_recaptcha').slideDown();
				}
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('wrapper');
			}
			 hide_comm_mask();
		}
	});	
	 
      return false; // Will stop the submission of the form
    }
  });
$.validate({
    form : '#sign_up_form',
    modules : 'security',
    onError : function($form) {
	  $('#t_and_c').addClass('hidden');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('wrapper');
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#sign_up_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'sign_up/add_account_detail' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('sign_up_form').reset();
				scroll_to_div('wrapper');
				setTimeout(function(){ window.location='<?php echo $base_url.'sign-up/add-resume' ?>';  }, 3000);
			}
			else
			{
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('wrapper');
			}
			 hide_comm_mask();
		}
	});	
	  //hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
  
function change_gender(gender)
{
	$('#title').val('');
	$('#title').css('display','block');
	if(gender=='Male')
	{
		$('.gn_classfemale').css('display','none');
		$('.gn_classmale').css('display','block');
		//$('#title').css('display','none');
		//$('#title').trigger('chosen:updated');
		
	}
	else if(gender=='Female')
	{
		$('.gn_classfemale').css('display','block');
		$('.gn_classmale').css('display','none');
		//$('#title').css('display','none');
		//$('#title').trigger('chosen:updated');
	}
}  
</script>