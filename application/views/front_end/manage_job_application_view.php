<?php 
$current_page_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$custom_lable_array = $custom_lable->language;	
$title= $custom_lable_array['manage_job_application_title'];
$sub_title = $custom_lable_array['job_application_lbl'];
$page = 'manage_job_application';
if(isset($page_name) && $page_name!='')
{
	$page = $page_name;
	$title = $custom_lable_array['shortlist_application_title'];
	$sub_title = $custom_lable_array['shortlist_application_lbl'];
}
?>

<div id="titlebar" class="photo-bg single" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/add-job.jpg); background-size:cover;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-file-text"></i>  <?php echo $title; ?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li> <?php echo $custom_lable_array['you_are_here']; ?> :</li>
					<li><a href="<?php echo $base_url; ?>"><?php echo $custom_lable_array['home_lbl']; ?></a></li>
                    <li><?php echo $title; ?></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="clearfix"></div>

	<div class="col-md-3 col-sm-12 col-xs-12">
		<?php include_once("employer_left_menu.php"); ?>
	</div>

	<div class="col-md-9 col-sm-12 col-xs-12" style="overload:hidden;">
		
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12" style="overload:hidden;">
					<div class="">
						<p class="margin-bottom-25" style="float: left;"><strong><a href="#"><?php echo $sub_title; ?></a></strong></p>
						<!--<strong><a href="#" class="btn btn-primary download-csv"><i class="fa fa-file-text"></i> Download CSV</a></strong>-->
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="col-md-12 col-sm-12 col-xs-12" style="overload:hidden;">
                    <div class="row" style="margin-bottom: 0px;">
						<form method="post" id="search_user_form" onSubmit="return search_applied_user_job();">
									<div class="col-md-5 col-sm-12 col-xs-12" style="padding-right:0px;">
										<input type="text" name="search_applied_user" class="form-control" placeholder="Search with  email , mobile or name" >
										<div class="margin-bottom-15"></div>
									</div>
									<div class="col-md-5 col-sm-12 col-xs-12" style="padding-right:0px;">
										<select name="job_title_id" class="form-control-no-single">
											<option value=""><?php echo $custom_lable_array['search_with_posted_job_lbl']; ?></option>
											<?php
											$emp_id = $this->common_front_model->get_empid();
											$where_arr = array('posted_by'=>$emp_id);
											$get_posted_job = $this->common_front_model->get_count_data_manual('job_posting_view',$where_arr,2,'id,job_title');
											if(isset($get_posted_job) && is_array($get_posted_job) && count($get_posted_job) > 0)
											{
											foreach($get_posted_job as $emp_job)
											{ ?>
											<option value="<?php echo $emp_job['id']; ?>"><?php echo $emp_job['job_title']; ?></option>
											<?php }
											}
											?>
										</select>
										<div class="margin-bottom-35"></div>
									</div>
						   <div class="col-md-2 col-sm-12 col-xs-12">  
							<button type="submit" class="button button-default" style="height:39px;width:100%;"><i class="fa fa-search"></i></button>
							</div>
                         </form>
					</div>
                   
				</div>
			</div>
			<div id="js_action_msg_div"></div> 
						<div id="main_content_ajax">
						<?php  
							   $this->load->view('front_end/page_part/manage_job_application_result_view',$this->data);
							 ?>
			            </div>
                        
                           <div id="view_js_details">
                            </div>
         
	</div>	

<div class="clearfix"></div>
<div class="margin-top-50"></div>

<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
					
                        <div class="small-dialog-headline">
                          <i class="fa fa-envelope"></i> <?php echo  $custom_lable_array['send_msg'];?>
                        </div>
                        <div class="clearfix margin-bottom-10"></div>
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                               
                        <div class="small-dialog-content">
                            <form  method="post"  id="send_msg_form" >
                                <div>
                                <input type="text" name="subject" data-validation="required" placeholder="Subject" value="" />
                                </div>
                                <div>
                                <textarea placeholder="Message" data-validation="required" name="content" ></textarea>
                                </div>
                                <button class="send" ><i class="fa fa-check"></i> <?php echo $custom_lable_array['send_msg']; ?></button>
                                <input type="hidden" value="NI-WEB" name="user_agent">
                                <input type="hidden" value="send_message" name="action">
                                <input type="hidden" value="" name="sender_id" id="sender_id">
                                <input type="hidden" value="" name="receiver_id" id="receiver_id">
                                <input type="hidden" value="" name="email" id="email">
                            </form> 
                        </div>
                    
				</div>

<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script>
$(document).ready(function(e) {
	$('#main_content_ajax').show();
	if($("#ajax_pagin_ul").length > 0)
	{   
		load_pagination_code();
	}

});

function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html('');  }, 8000);
}

<?php 
if(isset($page_name) && $page_name=='shortlisted_application')
{
	$request_url = 'shortlisted_application';
}
else
{
	$request_url = 'manage_job_application';
}
?>

function search_applied_user_job()
{
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var form_data = $('#search_user_form').serialize();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&'+form_data+'&is_ajax=1';
	$.ajax({	
		url : "<?php echo $base_url."job_application/$request_url" ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			    $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
				$('#main_content_ajax').html(data);	 
				load_pagination_code(); 
	    	    hide_comm_mask();
		}
	});	
	  
	return false;
}


function show_modelpp(class_id)
	{ 
	    document.getElementById('send_msg_form').reset();
		var sender_id = $('.'+class_id).data('sender_id');
		var receiver_id = $('.'+class_id).data('receiver_id');
		var email = $('.'+class_id).data('email');
		$('#sender_id').val(sender_id);
		$('#receiver_id').val(receiver_id);
		$('#email').val(email);
		$.magnificPopup.open({
			items: {
				src: '#small-dialog',
			},
			type: 'inline'
		});
	}

function show_details(div_id)
{
	$('#'+div_id).slideDown();
	$('#close_box_'+div_id).slideDown();
	scroll_to_div(div_id,-200);	 
}

function hide_details(div_id)
{  
	$('#'+div_id).slideUp();
	$('#close_box_'+div_id).slideUp();	 	 
}


function close_js_details()
{
	$('.hide-close-box').hide();
	$('#view_js_details').hide();
	$('#main_content_ajax').show();	 
	scroll_to_div('main_content_ajax'); 
}

function view_js_details(js_id)
{  
	$('.hide_job_details').hide();
	$('.hide-close-box').hide();
	
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&js_id='+js_id+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'employer-profile/view-js-details' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			    $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
				$('#view_js_details').show();
				$('#view_js_details').html(data);	  
				$('#main_content_ajax').hide();	  
				hide_comm_mask();
	    }
	});	
	  
}

function add_to_shortlist(app_id)
{  
	//$('.hide_job_details').hide();
	//$('.hide-close-box').hide();
	
	var c = confirm("<?php echo $custom_lable_array['confirm_add_to_shortlist']; ?>");
	if(c==true)
	{
		show_comm_mask();
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&app_id='+app_id+'&id='+app_id+'&user_agent=NI-WEB&mode=edit';
		$.ajax({	
			url : "<?php echo $base_url.'job_application/add_to_shortlist' ?>",
			type: 'post',
			data: datastring,
			success: function(data)
			{
					$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
					
					if(data.status =='success')
					{
						$('#js_action_msg_div').slideUp('Slow');
						$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_msg_div').slideDown('Slow');
						$('.short_list'+app_id).removeAttr('onClick');
						$('.short_list'+app_id).attr('onClick','remove_from_shortlist('+app_id+')');
						$('.short_list'+app_id).html(' <i class="fa fa-check"></i> <?php echo $custom_lable_array['added_to_shortlist']; ?>');
						set_time_out_msg('js_action_msg_div');
						scroll_to_div('js_action_msg_div',-100);
					}
					else
					{
						$('#js_action_msg_div').html('');
						$('#js_action_msg_div').slideUp('Slow');
						$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_msg_div').slideDown('Slow');
						set_time_out_msg('js_action_msg_div');
						scroll_to_div('js_action_msg_div',-100);
					}
					
					 hide_comm_mask();
			}
		});	
	 
	}
	
}

function remove_from_shortlist(app_id,total_record)
{  
	//$('.hide_job_details').hide();
	//$('.hide-close-box').hide();
	
	var c = confirm("<?php echo $custom_lable_array['confirm_remove_from_shortlist']; ?>");
	if(c==true)
	{
		show_comm_mask();
		var hash_tocken_id = $("#hash_tocken_id").val();
		var datastring = 'csrf_job_portal='+hash_tocken_id+'&app_id='+app_id+'&id='+app_id+'&user_agent=NI-WEB&mode=edit';
		$.ajax({	
			url : "<?php echo $base_url.'job_application/remove_from_shortlist' ?>",
			type: 'post',
			data: datastring,
			success: function(data)
			{
					$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
					//$("#hash_tocken_id").val(data.tocken);
					if(data.status =='success')
					{
						$('#js_action_msg_div').slideUp('Slow');
						$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_msg_div').slideDown('Slow');
						$('.short_list'+app_id).removeAttr('onClick');
						$('.short_list'+app_id).attr('onClick','add_to_shortlist('+app_id+')');
						$('.short_list'+app_id).html(' <i class="fa fa-plus-circle"></i> <?php echo $custom_lable_array['add_to_shortlist']; ?>');
						set_time_out_msg('js_action_msg_div');
						scroll_to_div('js_action_msg_div',-100);
						<?php
						if (strpos($current_page_url, 'shortlisted-application') !== false) 				 														                        {
							?>
							$('#seprate_result'+app_id).slideUp();
							var total_record = $('#shortlist_count').val();
							$('#shortlist_count').val(parseInt(total_record) - 1);
							if(total_record == 1)
							{
								$('#main_content_ajax').html('<div class="five columns"><img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg"></div>');
							}
							
							<?php 
						}
						?>
						
					}
					else
					{
						$('#js_action_msg_div').html('');
						$('#js_action_msg_div').slideUp('Slow');
						$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
						$('#js_action_msg_div').slideDown('Slow');
						set_time_out_msg('js_action_msg_div');
						scroll_to_div('js_action_msg_div',-100);
					}
					
					 hide_comm_mask();
			}
		});	
	 
	}
	
}



$.validate({
    form : '#send_msg_form',
    modules : 'security',
    onError : function($form) {
	  $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('error_msg',-100);
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
	  set_time_out_msg('error_msg');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#send_msg_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'job_application/send_message_js' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('send_msg_form').reset();
				scroll_to_div('success_msg');
				set_time_out_msg('success_msg');
			}
			else
			{
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('error_msg');
				set_time_out_msg('error_msg');
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
  
function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).slideUp();  }, 4000);
}

function download_resume(file,jidpkd)
{
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&file='+file+'&jidpkd='+jidpkd+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'job_application/download_resume' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
			if(data.status=='success')
			{
				window.open(data.file_download);
				//alert(data.file_download);
			}
			else
			{
				alert(data.errmessage);
			}
			 
		}
	});	
	hide_comm_mask();
}

</script>