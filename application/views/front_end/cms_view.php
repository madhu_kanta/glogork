<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/about-us-banner.jpg); background-size:cover;">
	<div class="container">
        <div class="sixteen columns">
            <h2><i class="fa fa-comment margin-right-10"></i><?php echo $cms_data['page_title']; ?></h2>
        </div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
<?php 
if(isset($cms_data['page_content']) && $cms_data['page_content'] !="")
{
	echo htmlspecialchars_decode($cms_data['page_content']);
}
	//echo $this->common_front_model->rander_pagination('home/test',100);
?>
</div>
<div class="margin-top-20">
</div>

<!--<script src="<?php //echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php //echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>-->