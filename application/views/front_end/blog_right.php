<?php $custom_lable_arr = $custom_lable->language; ?>
<!-- Widgets -->
<div class="five columns blog">
		<?php 
		$advert_data = $this->common_front_model->getadvertisement('Level 1'); 
			if(isset($advert_data) && $advert_data !='' && is_array($advert_data) && count($advert_data) > 0)
				{ ?>
        <div class="widget">
			<h4><?php echo  $custom_lable_arr['blog_right_ad_title']; ?></h4>
            <?php 
			
					if($advert_data['type']=='Image')
					{
			?>
            	<a href="<?php echo $advert_data['link']; ?>" target="_blank"><img src="<?php echo $base_url; ?>assets/advertisement/<?php echo $advert_data['image']; ?>" alt="Ad" /></a>
            <?php 	}elseif($advert_data['type']=='Google')
					{ echo $advert_data['google_adsense']; ?>
            
            <?php 	}
			 ?>
		</div>
        <?php } ?>

		<div class="widget">
			<h4><?php echo  $custom_lable_arr['right_contact_widget_title']; ?></h4>
			<div class="widget-box">
				<p><?php echo  $custom_lable_arr['right_contact_widget_subtitle']; ?></p>
				<a href="<?php echo $base_url; ?>contact" class="button widget-btn"><i class="fa fa-envelope"></i> <?php echo  $custom_lable_arr['right_contact_widget_linktitle']; ?></a>
			</div>
		</div>

		<!-- Tabs -->
       <?php if(isset($blog_count) && $blog_count > 0) 
	   		 {
	   ?>
		<div class="widget">

			<ul class="tabs-nav blog"><!--tabs-nav-->
				<!--<li class="active" data-toggle="tab" data-target="tabtesting" href="#tabtesting" ><a  onClick="makeactive();"><?php echo  $custom_lable_arr['blog_right_tab_title']; ?></a></li>-->
                <button type="button" class="active"><?php echo  $custom_lable_arr['blog_right_tab_title']; ?></button>
			</ul>

			<!-- Tabs Content -->
			<div class="tabs-container">
				<div class="tab-content" id="tabtesting" ><!--id="tab1"-->
					<ul class="widget-tabs">
						<!-- Post #1 -->
                        <?php 
							if(isset($recent_blog_data) && $recent_blog_data !='' && is_array($recent_blog_data) && count($recent_blog_data) > 0)
							{
							foreach($recent_blog_data as $recent_blog_data_single)
							{
						?>
						<li>
							
                            	<?php
								if($recent_blog_data_single['blog_image']!='' && file_exists('./assets/blog_image/'.$recent_blog_data_single['blog_image']))
								{ ?>
								<div class="widget-thumb">
									<a href="<?php echo $base_url; ?>blog/<?php echo $recent_blog_data_single['alias']; ?>"><img src="<?php echo $base_url; ?>assets/blog_image/<?php echo $recent_blog_data_single['blog_image']; ?>" alt="<?php echo $recent_blog_data_single['title']; ?>" /></a>
								</div>
								<?php }else{  ?>
								<div class="widget-thumb">
									<a href="<?php echo $base_url; ?>blog/<?php echo $recent_blog_data_single['alias']; ?>"><img src="<?php echo $base_url; ?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $recent_blog_data_single['title']; ?>" /></a>
								</div>
                                <?php }  ?>

							<div class="widget-text">
								<h5><a href="<?php echo $base_url; ?>blog/<?php echo $recent_blog_data_single['alias']; ?>"><?php echo $recent_blog_data_single['title']; ?></a></h5>
								<span><?php echo $this->common_front_model->displayDate($recent_blog_data_single['created_on'],'F j, Y '); ?></span>
							</div>
							<div class="clearfix"></div>
						</li>
                        <?php }
			 			}
						?>
					</ul>
				</div>
			</div>
		</div>
		<?php }  ?>
        <?php if(isset($singleblogflag) && $singleblogflag==true ) 
	   		 {
	   ?>
		<div class="widget">
			<h4><?php echo  $custom_lable_arr['blog_right_social_title']; ?></h4>
				<ul class="social-icons">
					<li><a class="facebook" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo urlencode($blog_data['title']); ?>&amp;p[url]=<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>&amp;&p[images][0]=<?php echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="icon-facebook"></i></a>
                    <!--<a class="facebook" href="http://www.facebook.com/share.php?u=<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>" onclick="return fbs_click()" target="_blank"><img src="<?php echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>" alt="Share on Facebook" rel="no-follow" /><i class="icon-facebook"></i></a>-->
                    </li>
					<li><a class="twitter" target="_blank" href="http://twitter.com/home?status=<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>" title="Click to share this post on Twitter" ><i class="icon-twitter"></i></a>
                    <!--<a class="twitter" target="_blank" href="http://twitter.com/home?status='<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>'" title="Click to share this post on Twitter"><img src="<?php echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>" alt="Share on Twitter" rel="no-follow"><i class="icon-twitter"></i></a>-->
                    </li>
					<li><a class="gplus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($base_url.'blog/'.$blog_data['alias']); ?>&title=<?php echo urlencode($blog_data['title']); ?>&summary=<?php echo urlencode($blog_data['title']); ?>&source=<?php echo urlencode($base_url); ?>
"><i class="icon-linkedin"></i></a></li>
				</ul>
		</div>
		<?php } ?>
		<div class="clearfix"></div>
		<div class="margin-bottom-40"></div>

	</div>
 <script>
 function makeactive()
{
	//return false;
	setTimeout(function(){ $('#tabtesting').removeAttr("style"); }, 10);
	return false;
}
 $(function(){
	/*$('head').append('<meta test="true"/>'); */
   	/*$( 'meta[name="viewport"]' ).delay( 1000 ).after( '<meta test="true"/>' );*/
});
$(document).ready(function(e) {
	
	$( '<meta property="og:image" content="<?php //echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>"/>' ).appendTo( 'head' );
	$('head').delay( 1000 ).append('<meta property="og:image" content="<?php //echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>"/>');
	$('head').delay( 1000 ).append('<meta name="twitter:image" content="<?php //echo urlencode($base_url.'assets/blog_image/'.$blog_data['blog_image']); ?>" />');
});

</script>   
<!-- Widgets / End -->
<!--<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>-->
<script>function fbs_click() {u=location.href;t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;}</script>


