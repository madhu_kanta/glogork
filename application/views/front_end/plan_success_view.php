<div class="clearfix"></div>
<div id="titlebar" class="single photo-bg" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner/pricing-banner.jpg);background-repeat: no-repeat;background-size: cover;min-width: 100%;background-position: center center;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-money" aria-hidden="true"></i> Membership Plan</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>Payment Done Successfully.</li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="container">    
    	<div class="sixteen columns">
            <div class="notification success margin-bottom-30 text-bold">
                <p>Your Payment has been successfully done, Your membership plan updated soon, Thank you
                <a style="margin-top:-8px" class="button" href="<?php echo $base_url.'my-plan/current-plan' ?>"> View Plan Detail</a>
                <br/>
                </p>
                
            </div>
        </div>
</div>
<div class="margin-top-40"></div>