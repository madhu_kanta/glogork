<?php $user_type = '';
	$user_id = '';
	$user_data = $this->common_front_model->get_logged_user_typeid();
	$user_type = $user_data['user_type'];
	$user_id = $user_data['user_id'];
	$display_detail ='No';
	if($user_type !='' && $user_id !='')
	{
		$plan_data = $this->my_plan_model->current_plan_detail();
		
		$plan_data_count =  $plan_data;
		if(isset($plan_data) && $plan_data !='' && is_array($plan_data) && count($plan_data) > 0)
		{
			$display_detail ='Yes';
			//print_r($plan_data);
		}
	}
	$check_yes = '<span class="fa fa-check text-success"></span> (Yes)';
	$check_no = '<span class="fa fa-times text-danger"></span> (No)';
?>
<div class="clearfix"></div>
<div id="titlebar" class="single photo-bg" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner/pricing-banner.jpg);background-repeat: no-repeat;background-size: cover;min-width: 100%;background-position: center center;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-money" aria-hidden="true"></i> <?php if($display_detail =='No'){ echo 'No Active Plan';} else{ echo 'Your current active plan - '.$plan_data['plan_name'];}?> </h2>
			<nav id="breadcrumbs">
				<ul>
					<li></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="container">
	<?php if($display_detail =='Yes')
	{
	?>
    <div class="sixteen columns">
        <div class="notification success margin-bottom-30 text-bold">
            <p>Current Plan
            <a style="margin-top:-8px" class="button" href="<?php echo $base_url.'my-plan/index' ?>"> Upgrade Now</a>
            <br/>
            </p>
        </div>
        
        <div class="row">
        	<div class="col-xs-12 col-md-3 text-center margin-top-15">
            	Plan Amount
                <hr class="margin-bottom-10"/>
                <?php echo $plan_data['plan_currency'].' '.$plan_data['plan_amount']; ?>
        	</div>
            <div class="col-xs-12 col-md-3 text-center margin-top-15">
            	Discount
                <hr class="margin-bottom-10"/>
                <?php
					if($plan_data['discount_amount'] !='' && $plan_data['coupan_code'] !='')
					{
						echo $plan_data['plan_currency'].' '.$plan_data['discount_amount']; 
						if(isset($plan_data['coupan_code']) && $plan_data['coupan_code'] !='')
						{
							echo ' ('.$plan_data['coupan_code'].')';
						}
					}
					else
					{
						echo 'N/A';
					}
				?>
        	</div>
            <div class="col-xs-12 col-md-3 text-center margin-top-15">
            	Service Tax / GST
                <hr class="margin-bottom-10"/>
                <?php echo $plan_data['plan_currency'].' '.$plan_data['service_tax']; ?>
        	</div>
            <div class="col-xs-12 col-md-3 text-center margin-top-15">
            	Total Amount
                <hr class="margin-bottom-10"/>
                <?php echo $plan_data['plan_currency'].' '.$plan_data['final_amount']; ?>
        	</div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Plan Duration
                <hr class="margin-bottom-10"/>
                <?php echo $plan_data['plan_duration'].' Days'; ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Plan Activated On
                <hr class="margin-bottom-10"/>
                <?php echo $this->common_front_model->displayDate($plan_data['activated_on'],'F j, Y'); ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Plan Expired On
                <hr class="margin-bottom-10"/>
                <span class="text-danger"><?php echo $this->common_front_model->displayDate($plan_data['expired_on'],'F j, Y'); ?></span>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Allowed Message (Remaining)
                <hr class="margin-bottom-10"/>
                <?php echo ($plan_data['message'] - $plan_data['message_used']).' out of '.$plan_data['message']; ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Allowed Contacts (Remaining)
                <hr class="margin-bottom-10"/>
                <?php echo ($plan_data['contacts'] - $plan_data['contacts_used']).' out of '.$plan_data['contacts']; ?>
        	</div>
        <?php
			if(isset($user_type) && $user_type == 'job_seeker')
			{
		?>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Relevant Jobs
                <hr class="margin-bottom-10"/>
                <?php
				if(isset($plan_data['relevant_jobs']) && $plan_data['relevant_jobs'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				}
				?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Job Notification
                <hr class="margin-bottom-10"/>
                <?php
				if(isset($plan_data['job_post_notification']) && $plan_data['job_post_notification'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				}
				?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Performance Report
                <hr class="margin-bottom-10"/>
                <?php
				if(isset($plan_data['performance_report']) && $plan_data['performance_report'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				}
				?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Highlight Application
                <hr class="margin-bottom-10"/>
                <?php
				if(isset($plan_data['highlight_application']) && $plan_data['highlight_application'] =='Yes')
				{
					echo $check_yes;
				}
				else
				{
					echo $check_no;
				}
				?>
        	</div>
        <?php
			}
			else
			{
		?>
        	<div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Job Life
                <hr class="margin-bottom-10"/>
                <?php echo $plan_data['job_life'].' Days'; ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Highlight Job (Remaining)
                <hr class="margin-bottom-10"/>
                <?php echo ($plan_data['highlight_job_limit'] - $plan_data['highlight_job_limit_used']).' out of '.$plan_data['highlight_job_limit']; ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Job Post Limit (Remaining)
                <hr class="margin-bottom-10"/>
                <?php echo ($plan_data['job_post_limit'] - $plan_data['job_post_limit_used']).' out of '.$plan_data['job_post_limit']; ?>
        	</div>
            <div class="col-xs-12 col-md-4 text-center margin-top-15">
            	Cv Access Limit (Remaining)
                <hr class="margin-bottom-10"/>
                <?php echo ($plan_data['cv_access_limit'] -$plan_data['cv_access_limit_used']).' out of '.$plan_data['cv_access_limit']; ?>
        	</div>
        <?php
			}
		?>    
        </div>
        
    </div>
    <?php
	}
	else
	{
	?>
    	<div class="sixteen columns">
            <div class="notification error margin-bottom-30 padding-bottom-30 text-bold" style="padding-bottom:45px !important">
            	<div class="col-xs-12 col-md-10">
                	<p>You have currently no active plan </p>
                </div>
                <div class="col-xs-12 visible-xs"><br/><br/></div>
                <div class="col-xs-12 col-md-2 text-center" style="margin:0 auto">
	                <a style="margin-top:-8px" class="button" href="<?php echo $base_url.'my-plan/index' ?>"> Upgrade Now</a>
                </div>
            </div>
        </div>
    <?php
	}
	?>
</div>
<div class="margin-top-40"></div>