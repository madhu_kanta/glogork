<?php $custom_lable_arr = $custom_lable->language; ?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/about-us-banner.jpg); background-size:cover;"><!--class="single"-->
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo  $custom_lable_arr['browse_location_tit']; ?></h2>
			<span><?php echo $custom_lable_arr['browse_location_subtitle']; ?></span>
		</div>
	</div>
</div>

<div id="categories">
<?php if(isset($city_data) && $city_data!='' && is_array($city_data) && count($city_data) > 0)
{
	/*echo "<pre>";
	print_r($jobs_by_industry_data);
	echo "</pre>";exit;*/
	$var_to_pass_in_view = array();
	$var_to_pass_in_view['city_data'] = $city_data;
	$var_to_pass_in_view['city_count'] = $city_count;
	$var_to_pass_in_view['limit_per_page'] = $limit_per_page;
	//$var_to_pass_in_view['custom_lable_arr'] = $custom_lable_arr;
?>
	<div class="container">
			<!--<form action="#" method="get" class="list-search">
				<button><i class="fa fa-search"></i></button>
				<input type="text" placeholder="Search Jobs Category (e.g. php, javascript etc..)" value=""/>
				<div class="clearfix"></div>
			</form>-->
	</div>
    
   <!-- Companies group result -->
	<div id="main_content_ajax">
		<?php $this->load->view('front_end/location_all_result',$var_to_pass_in_view); ?>
	</div>
	<!-- Companies group result End --> 

<?php //echo $this->common_front_model->rander_pagination('browse_companies',$company_count); ?>
<?php }
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>    
</div>    
<?php 
}
?>
</div>
<input type="hidden" name="base_url_ajax" id="base_url_ajax" value="<?php echo $base_url; ?>" />
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/animate.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/slideanim.css">
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">


<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<script>
$(document).ready(function(e) {
if($("#ajax_pagin_ul").length > 0)
{
	load_pagination_code();
}
});
</script>