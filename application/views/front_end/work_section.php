<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="panel-heading panel-bg bg-new-p" id="workdetails_vieweditdiv"><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-user"></span> Work History Details </span> </div>
<div class="alert alert-danger" id="messagework" style="display:none" ></div>
<div class="alert alert-success" id="success_msgwork" style="display:none" ></div>
<form method="post" id="workdetform" name="workdetform" action="<?php echo $base_url.'my_profile/editworkdet'; ?>" >
						<?php
						if($work_count > 0)
						{
							
							$customcoount_wrk = 0;
							if(isset($workdetails) && $workdetails !='' && is_array($workdetails) && count($workdetails) > 0)
							{
							foreach($workdetails as $workdetails_single)
							{
								
								$customcoount_wrk = $customcoount_wrk + 1;
							if($customcoount_wrk > 1) 
							{?>
							<hr class="th_bordercolor" style="border-width: 4px;" >
							<?php 	}
							$annual_salary_wrk = ($this->common_front_model->checkfieldnotnull($workdetails_single['annual_salary'])) ? explode(".",$workdetails_single['annual_salary']) : "";

							?>
						<div class="panel-body work_whole" style="padding:10px;" data-work_num="<?php echo $customcoount_wrk;?>">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['company_name']; ?></label>
									<input type="text" class="edit-input" name="companyname_<?php echo $workdetails_single['id'] ; ?>" id="companyname_<?php echo $workdetails_single['id'] ; ?>" data-cmpname = "<?php echo $customcoount_wrk;?>"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['company_name']; ?>" value="<?php echo ($this->common_front_model->checkfieldnotnull($workdetails_single['company_name'])) ? $workdetails_single['company_name'] : ""; ?>" data-validation="required">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['type_industry']; ?></label>
									<select name="industry_<?php echo $workdetails_single['id'] ; ?>" id="industry_<?php echo $workdetails_single['id'];?>" data-indusdat = "<?php echo $customcoount_wrk;?>" class="input-select-b select-drop-3 city" data-validation="required">
										<?php if(isset($ind_frm_tbl) && $ind_frm_tbl!='' && ($workdetails_single['industry']=='' || is_null($workdetails_single['industry'])) )
										{ 
											print_r($ind_frm_tbl); 
										}
										elseif(isset($ind_frm_tbl) && $ind_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($workdetails_single['industry']))
										{
											print_r($this->common_front_model->get_list('industries_master','str','','str',$workdetails_single['industry']));
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['type_industry']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['functional_area']; ?></label>
									<select onChange="dropdownChange('functional_area_<?php echo $workdetails_single['id'] ; ?>','job_role_<?php echo $workdetails_single['id'] ; ?>','role_master');" name="functional_area_<?php echo $workdetails_single['id'] ; ?>" id="functional_area_<?php echo $workdetails_single['id'] ; ?>" data-validation="required" class="input-select-b select-drop-3" data-funarea="<?php echo $customcoount_wrk;?>">
											<?php if(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && ($workdetails_single['functional_area']=='' || is_null($workdetails_single['functional_area'])) )
											{ 
												print_r($fnarea_frm_tbl); 
											}
											elseif(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($workdetails_single['functional_area']))
											{
												print_r($this->common_front_model->get_list('functional_area_master','str','','str',$workdetails_single['functional_area']));
											}
											else
											{ ?>
												<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['functional_area']; ?></option>
											<?php } ?>
                                    </select>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['job_role']; ?></label>
									<select name="job_role_<?php echo $workdetails_single['id'] ; ?>" id="job_role_<?php echo $workdetails_single['id'] ; ?>" data-validation="required" class="input-select-b select-drop-3" data-jbrol="<?php echo $customcoount_wrk;?>">
										<?php if(($workdetails_single['job_role']=='' || is_null($workdetails_single['job_role'])) && $this->common_front_model->checkfieldnotnull($workdetails_single['functional_area']))
										{ 
											print_r($this->common_front_model->get_list('role_master','str',$workdetails_single['functional_area'],'str',''));
										}
										elseif($this->common_front_model->checkfieldnotnull($workdetails_single['functional_area']) && $this->common_front_model->checkfieldnotnull($workdetails_single['job_role']))
										{
											print_r($this->common_front_model->get_list('role_master','str',$workdetails_single['functional_area'],'str',$workdetails_single['job_role']));
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['job_role']; ?></option>
										<?php } ?>
                                   	</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['joining_date']; ?> </label>
									<input type="text" class="edit-input datepicker datepicker-join" onkeydown="return false" data-validation="required" id="joining_date_<?php echo $workdetails_single['id'] ; ?>" name="joining_date_<?php echo $workdetails_single['id'] ; ?>" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['joining_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value="<?php echo ($this->common_front_model->checkfieldnotnull($workdetails_single['joining_date'])) ? $workdetails_single['joining_date'] : ""; ?>" data-jdate="<?php echo $customcoount_wrk;?>">
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['leaving_date'].' '.$custom_lable_arr['leaving_date_update'];?></label>
									<input type="text" class="edit-input datepicker datepicker-leave" onkeydown="return false" id="leaving_date_<?php echo $workdetails_single['id'] ; ?>" name="leaving_date_<?php echo $workdetails_single['id'] ; ?>" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['leaving_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value="<?php echo ($this->common_front_model->checkfieldnotnull($workdetails_single['leaving_date']) && $workdetails_single['leaving_date']!='0000-00-00') ? $workdetails_single['leaving_date'] : ""; ?>" data-ldate="<?php echo $customcoount_wrk;?>" >
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['annual_salary']; ?></label>
									<select name="annual_salary_<?php echo $workdetails_single['id'] ; ?>" id="annual_salary_<?php echo $workdetails_single['id'];?>" data-indusdat = "<?php echo $customcoount_wrk;?>" class="input-select-b select-drop-3 city" data-validation="required">
										<?php if(isset($salary_frm_tbl) && $salary_frm_tbl!='' && ($workdetails_single['annual_salary']=='' || is_null($workdetails_single['annual_salary'])) )
										{ 
											print_r($salary_frm_tbl); 
										}
										elseif(isset($salary_frm_tbl) && $salary_frm_tbl!='' && $this->common_front_model->checkfieldnotnull($workdetails_single['annual_salary']))
										{
											print_r($this->common_front_model->get_list('salary_range_list','str','','str',$workdetails_single['annual_salary']));
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['annual_salary']; ?></option>
										<?php } ?>
									</select>
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['achievements']; ?></label>
									<textarea class="edit-input" id="achievements_<?php echo $workdetails_single['id'] ; ?>" name="achievements_<?php echo $workdetails_single['id'] ; ?>" data-achv="<?php echo $customcoount_wrk;?>" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Provide few lines about your achievements there" data-original-title="" title=""><?php echo ($this->common_front_model->checkfieldnotnull($workdetails_single['achievements'])) ? $workdetails_single['achievements'] : ""; ?></textarea>
								</div>
								
							</div>
							<!--delete button starts-->
							<div class="col-xs-12">
								<div class="margin-top-20"></div>
									<div class="input-group-btn text-center clearfix">
									<button class="btn btn-danger deletepart" data-deletepart="work" data-deleteid="<?php echo $workdetails_single['id'] ; ?>" data-deleteid_count="<?php echo $customcoount_wrk ; ?>" onClick="deletepart('work','<?php echo $workdetails_single['id'] ; ?>','<?php echo $customcoount_wrk;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete Work details</button>
									<div class="margin-bottom-20"></div>
									</div>
							</div>
                           
							<!--delete button ends-->	
                        </div>
							<?php 
									}
									}//foreach ens
									 ?>	
									 <div class="clear"></div>
											<div class="margin-top-10"></div>
                                            <?php 
											if($work_count < 11){
											?>
                                            <div class="input-group-btn text-center">
                                                    <button class="btn btn-success" type="button"  id="addmorebutton_work"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_arr['myprofile_work_det_more']; ?></button>
												<div class="margin-bottom-20"></div>
                                            </div>        
                                        	<?php } ?>
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<button class="btn-job-new block new-save" type="submit"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
								</div>
							</div>
                            
						<?php 
							}
							else{?>
						<div class="panel-body work_whole" style="padding:10px;" data-work_num="1">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['company_name']; ?></label>
									<input type="text" class="edit-input" name="companyname_a_1" id="companyname_a_1" data-cmpname = "1"  placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['company_name']; ?>" value="" data-validation="required">
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['type_industry']; ?></label>
									<select name="industry_a_1" id="industry_a_1" data-indusdat = "1" class="input-select-b select-drop-3 city" data-validation="required">
										<?php if(isset($ind_frm_tbl) && $ind_frm_tbl!='')
										{ 
											print_r($ind_frm_tbl); 
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['type_industry']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['functional_area']; ?></label>
									<select onChange="dropdownChange('functional_area_a_1','job_role_a_1','role_master');" name="functional_area_a_1" id="functional_area_a_1" data-validation="required" class="input-select-b select-drop-3" data-funarea="1">
											<?php if(isset($fnarea_frm_tbl) && $fnarea_frm_tbl!='')
											{ 
												print_r($fnarea_frm_tbl); 
											}
											else
											{ ?>
												<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['functional_area']; ?></option>
											<?php } ?>
                                    </select>
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['job_role']; ?></label>
									<select name="job_role_a_1" id="job_role_a_1" data-validation="required" class="input-select-b select-drop-3" data-jbrol="1">
										
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['job_role']; ?></option>
										
                                   	</select>
								</div>
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['joining_date']; ?> </label>
									<input type="text" class="edit-input datepicker datepicker-join" onkeydown="return false" data-validation="required" id="joining_date_a_1" name="joining_date_a_1" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['joining_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value="" data-jdate="1">
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['leaving_date'].' '.$custom_lable_arr['leaving_date_update'];?></label>
									<input type="text" class="edit-input datepicker datepicker-leave" onkeydown="return false" id="leaving_date_a_1" name="leaving_date_a_1" placeholder="<?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['leaving_date']; ?>" data-date-format="yyyy-mm-dd" style="border-radius:0px;" value="" data-ldate="1" >
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['annual_salary']; ?></label>
									<select name="annual_salary_a_1" id="annual_salary_a_1" data-indusdat = "1" class="input-select-b select-drop-3 city" data-validation="required">
										<?php if(isset($salary_frm_tbl) && $salary_frm_tbl!='')
										{ 
											print_r($salary_frm_tbl); 
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['specify'].' '.$custom_lable_arr['annual_salary']; ?></option>
										<?php } ?>
									</select>
								</div>
								
							</div>
							<div class="row margin-top-10" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<label class="label-new"><?php echo $custom_lable_arr['achievements']; ?></label>
									<textarea class="edit-input" id="achievements_a_1" name="achievements_a_1" data-achv="1" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Provide few lines about your achievements there" data-original-title="" title=""></textarea>
								</div>
								
							</div>
                        </div>
							<div class="clear"></div>
							<div class="margin-top-10"></div>
							<?php 
							if($work_count < 11){
							?>
							<div class="input-group-btn text-center">
									<button class="btn btn-success" type="button"  id="addmorebutton_work"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><?php echo $custom_lable_arr['myprofile_work_det_more']; ?></button>
								    <div class="margin-bottom-20"></div>
							</div>        
							<?php } ?>
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									
									<button class="btn-job-new block new-save" type="submit"><i class="fa fa-fw fa-check" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']; ?></button>
									<!-- <button class="btn btn-default" type="button" onClick="viewsection('workdetails');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php ///echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']; ?></button> -->
								</div>
							</div>
						
						<?php
									}
									?>
                                    <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                                    
                                    </form>