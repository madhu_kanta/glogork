<?php 
$custom_lable_array = $custom_lable->language;
if(isset($get_list) &&  $get_list!='')
{
		$title= $custom_lable_array['view_job_list_title'];
		
		if($get_list=='like_job')
		{
			$title= $custom_lable_array['like_job_list_title'];
		}
		else if($get_list=='save_job')
		{
			$title= $custom_lable_array['save_job_list_title'];
		}
	
}
?>
<div class="clearfix"></div>
<div id="titlebar" class="photo-bg single" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/alert.jpg); background-size:cover;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-list" aria-hidden="true"></i> <?php echo $title; ?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li> <?php echo $custom_lable_array['you_are_here']; ?> :</li>
					<li><a href="<?php echo $base_url; ?>"><?php echo $custom_lable_array['home_lbl']; ?></a></li>
                    <li><?php echo $custom_lable_array['js_activity_lbl']; ?></li>
					<li><?php echo $title; ?></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<!--<div class="container">
	<div class="sixteen columns">
-->		<!--<p class="margin-bottom-25"> Your job alerts are shown below.</p>
		<div class="margin-top-30">
			<div class="pull-right">
				<span class="text-muted"><b>1</b>–<b>50</b> of <b>277</b></span>
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</button>
					<button type="button" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</button>
				</div>
			</div>
		</div>
		<br />
		<hr>
		-->
        <div class="col-md-3 col-sm-12 col-xs-12">
        	<?php include_once("job_seeker_left_menu.php"); ?>
		</div>
        <div class="col-md-9 col-sm-12 col-xs-12">
        	<div id="js_action_msg_div"></div> 
        	<div id="main_content_ajax">
			<?php  
				   $this->load->view('front_end/page_part/js_job_activity_result_view',$this->data);
				 ?>
           
       </div>
        </div>
			
	
		<!--<a href="#small-dialog" class="popup-with-zoom-anim button margin-top-20 margin-bottom-30"><i class="fa fa-plus"></i>Add Alert</a>
		<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
			<div class="small-dialog-headline">
				<i class="fa fa-briefcase"></i> Add Alert
			</div>
			<div class="small-dialog-content">
				<form action="#" method="post" class="login">
					<input type="text" placeholder="Alert Name" value=""/>
					<input type="text" placeholder="Keyword" value=""/>
					<input type="text" placeholder="Location" value=""/>
					<select data-placeholder="Email Frequency" class="chosen-select-no-single">
						<option value="">Email Frequency</option>
						<option value="1">Daily</option>
						<option value="2">Weekly</option>
						<option value="3">Fortnightly</option>
					</select>
					<div class="clearfix"></div>
					<div class="margin-top-15"></div>
					<select data-placeholder="Job Type" class="chosen-select" multiple>
						<option value="1">Full-Time</option>
						<option value="2">Part-Time</option>
						<option value="3">Internship</option>
						<option value="4">Freelance</option>
						<option value="5">Temporary</option>
					</select>
					<button class="send margin-top-20"><i class="fa fa-check"></i> Save Alert</button>
				</form>
			</div>
		</div>-->
	<!--</div>
</div>-->

<div class="clearfix"></div>
<script>

$(document).ready(function(e) {
	
	if($("#ajax_pagin_ul").length > 0)
	{   
		load_pagination_code();
	}

});



function close_model()
{ 
	$('.mfp-close').trigger('click');
}
function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html('');  }, 8000);
	
}


function jobseeker_action(action,action_for,action_id)
{
	<?php 
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{ 
	    $return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
		$this->session->set_userdata($return_after_login_url);
		?>
		
				var message = '<?php echo $this->lang->line('please_login'); ?>';
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').hide();
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').show();
				scroll_to_div('js_action_msg_div');
				set_time_out_msg('js_action_msg_div');
		   	    return false;
	<?php }
	?>
	var warning = $('#js_action').data('warning');
	var c_status = confirm(warning);
	if(c_status==true)
	{
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&action='+action+'&action_for='+action_for+'&action_id='+action_id+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'job-seeker-action/seeker-action' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			    $("#hash_tocken_id").val(data.token);
			    $('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
			
			
			if(data.status=='success')
			{   
					
					var action_val = $('#activity_list_count').val();
					load_pagination_code_js_activity();
					$('#js_activity'+action_id).html('');
					$('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_msg_div').show();
				   scroll_to_div('js_action_msg_div');
				   set_time_out_msg('js_action_msg_div');
				   
				}
			else
			{
					
					$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_msg_div').show();
					scroll_to_div('js_action_msg_div');
					set_time_out_msg('js_action_msg_div');
				
			}
			hide_comm_mask();
		}
	});	
	  
	}
	  return false;
}
function load_pagination_code_js_activity()
{
		
	    var page_number = 1;
		var url_action = '<?php echo $base_url; ?>job_seeker_action/js_activity_list/1/<?php echo base64_encode($this->session->userdata('js_activity_list')); ?>';
		page_number = typeof page_number !== 'undefined' ? page_number : 0;
		
		if(page_number == 0)
		{
			return false;
		}
		if(page_number != undefined && page_number !='' && page_number != 0 && url_action !='')
		{   
		    var search_form_id = $('#search_from_id').val();
			if(search_form_id!='' && search_form_id!='null' && search_form_id!='undefined'  && search_form_id!=null && search_form_id!=undefined)
			{   
				get_ajax_search(url_action,page_number,search_form_id);
			}
			else
			{
				search_form_id = '';
				get_ajax_search(url_action,page_number,search_form_id);
			}
			
		}
		return false;
   
}

</script>