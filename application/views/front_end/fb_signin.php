<?php
// added in v4.0.0
if (version_compare(PHP_VERSION, '5.4.0', '<')) {
  throw new Exception('The Facebook SDK v4 requires PHP version 5.4 or higher.');
}

/**
 * Register the autoloader for the Facebook SDK classes.
 * Based off the official PSR-4 autoloader example found here:
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class)
{
  // project-specific namespace prefix
  $prefix = 'Facebook\\';

  // base directory for the namespace prefix
  $base_dir = defined('FACEBOOK_SDK_V4_SRC_DIR') ? FACEBOOK_SDK_V4_SRC_DIR : __DIR__ . '/src_facebook/Facebook/';

  // does the class use the namespace prefix?
  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0) {
    // no, move to the next registered autoloader
    return;
  }

  // get the relative class name
  $relative_class = substr($class, $len);

  // replace the namespace prefix with the base directory, replace namespace
  // separators with directory separators in the relative class name, append
  // with .php
  $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

  // if the file exists, require it
  if (file_exists($file)) {
    require $file;
  }
});


use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
// init app with app id and secret
FacebookSession::setDefaultApplication( $fb_detail['client_key'],$fb_detail['client_secret'] );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper(''.$base_url.'sign_up/fb_signin' );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session
if ( isset( $session ) ) 
{
 $request = new FacebookRequest($session, 'GET', '/me?fields=email,first_name,last_name,gender,birthday' );
 $response = $request->execute();
  // get response
 
  		$graphObject = $response->getGraphObject();
     	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	  	$first_name = $graphObject->getProperty('first_name'); // To Get Facebook first name
		$last_name = $graphObject->getProperty('last_name'); // To Get Facebook first name
	    $femail = $graphObject->getProperty("email");   // To Get Facebook email ID
		$gender = $graphObject->getProperty("gender");
		//$graphObject->getProperty("bio");
		$bio =  '';  
		$fb_birthday = $graphObject->getProperty("birthday");
		$month='';
		$day='';
		$year='';
		if($fb_birthday!='')
		{
			$org=explode('/',$fb_birthday);
			if(isset($org[0]))
			{
				$month=$org[0];
			}
			if(isset($org[1]))
			{
				$day=$org[1];
			}			
			if(isset($org[2]))
			{
				$year=$org[2];
			}
		}
	   
	   $login_dt = $this->common_front_model->getCurrentDate(); // must save date and time on datetime field
	   if($femail!='')
	   {
		$chkemail=" or email='".$femail."'";
	   }
	   else
	   {
		$chkemail= "";   
	   }
	   
	   
	   
	   $where = "(facebook_id='".$fbid."' $chkemail) AND is_deleted!='Yes'";
	   $this->db->where($where);
	   $query = $this->db->get('jobseeker');
	   $reg_data = $query->row_array();
	   //echo $this->db->last_query;   
	  // echo $query->num_rows();  
	  // exit();
		 if($query->num_rows() > 0 && isset($reg_data) && is_array($reg_data) && count($reg_data) > 0)
         {
		     if($reg_data['status']!='UNAPPROVED')
		     {
				 
				 $user_data_array = array(
						 'user_id'	=> $reg_data['id'],
						 'full_name'	=> $reg_data['fullname'],
						 'email'		=> $reg_data['email'],
						 'gender'	=> $reg_data['gender'],
						 );
				$this->session->set_userdata('jobportal_user', $user_data_array);			 
                $email = $reg_data['email'];
				
				$this->db->set('last_login', $login_dt);
				$this->db->where('id', $reg_data['id']);
				$this->db->limit(1);
				$this->db->update('jobseeker');
			    redirect($this->base_url.'my-profile');
              }
              else
              {
				 $this->session->set_flashdata('user_log_out', $this->lang->line('inactive_login_account'));
				 redirect($base_url.'sign_up/login');
              }
         }
         else
         {
			$url = 'http://graph.facebook.com/'.$fbid.'/picture?type=large';
			$data = file_get_contents($url);
			$fileName = time().'.jpg';
			$file = fopen('/home/trialing/public_html/jobportal/assets/js_photos/'.$fileName, 'w+');
			$fl=fputs($file, $data);	
			fclose($file);//
			//copy($fileName, '/home/trialing/public_html/jobportal/assets/js_photos/'.$fileName);
	
			$user_fb_array = array(
									'FBID'	=> $fbid,
									'fb_first_name'	=> $first_name,
									'fb_last_name'	=>$last_name,
									'fb_email'	=> $femail,
									'fb_gender'	=> $gender,
									'fb_image_name'	=> $fileName,
									'fb_image'	=> $url,
									'month' =>  $month,
									'day' => $day,
									'year' => $year
								);
  			 $this->session->set_userdata('js_fb_data', $user_fb_array);
			 if($this->session->userdata('js_gplus_data'))
				{
					$this->session->unset_userdata('js_gplus_data');
				}
			 redirect($base_url.'sign_up');
         }   
} 
else
 {
	  //$loginUrl = $helper->getLoginUrl();
  
  $loginUrl = $helper->getLoginUrl(array(
   'scope' => 'email'
 ));
 
  header("Location: ".$loginUrl);
  
   
}

ob_flush();	
?>

