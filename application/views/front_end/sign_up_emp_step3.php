<?php 
$custom_lable_arr = $custom_lable->language;
if(isset($forstep2) && $forstep2!="" && is_array($forstep2) && count($forstep2) > 0)
{
	$forstep2svar = $forstep2;
	//$join_in = ($forstep2svar['join_in']!="" && !is_null($forstep2svar['join_in'])) ? explode(" ,",$forstep2svar['join_in']) : "";
}
else
{
	$forstep2svar = "";
	//$join_in = "";
}
//print_r($forstep2svar);
?>
<form method="post" name="emplregform3" id="emplregform3" action="<?php echo $base_url.'sign_up_employer/signupemp3'; ?>" enctype="multipart/form-data">
	<div class="alert alert-danger" id="message3" style="display:none" ></div>
	<div class="alert alert-success" id="success_msg3" style="display:none" ></div>
<div class="panel panel-primary">
								<div class="panel-heading th_bgcolor"><strong style="color:white;"><i class="fa fa-suitcase" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']; ?></strong></div>
								<div class="panel-body">
									<div class="col-md-12 col-sm-12 col-xs-12">
	                                    <h6><?php echo $custom_lable_arr['functional_area_empl_lbl']; ?><span class="red-only"> *</span></h6>
                                        <div class="form-group">
                                            <select name="functional_area[]" id="functional_area" class="chosen-select" onChange="dropdownChange('functional_area','currentdesignation','role_master','','multi');" multiple data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformaddressvalmessup']; ?>" data-validation="required">
                                            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['functional_area'])) { echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str',$forstep2svar['functional_area'],'1'); }else{ echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str','','1') ; } ?>
                                            </select>
                                        </div>
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['current_role_empl_lbl']; ?><span class="red-only"> *</span></h6>
										<div class="form-group">
											<!--<input type="text" class="form-control" name="currentdesignation" id="currentdesignation" placeholder="Hr / Manager"  style="padding:9px 0 9px 10px;" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Enter Your Current Designation" data-validation="required" data-validation-error-msg="Please provide your current designation" value="<?php //echo ($forstep2svar!='') ? $forstep2svar['designation'] : ""?>">-->
                                            <select name="currentdesignation[]" id="currentdesignation" class="chosen-select" multiple max-selected-options="3" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['emp_edit_des_mes1']; ?>">
                                            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['designation']) && $this->common_front_model->checkfieldnotnull($forstep2svar['functional_area'])) { 
											$fn_area_arr = explode(",",$forstep2svar['functional_area']);
											//echo $fn_area_arr;
											echo $this->common_front_model->get_list_multiple('role_master','str',$fn_area_arr,'str',$forstep2svar['designation'],1,'multi'); }else{ echo "<option value=''>".$custom_lable_arr['myprofile_emp_desemptopt']."</option>" ; } ?>
                                            </select>
										</div>
										<!--<h6>Current Employer<span class="red-only"> *</span></h6>-->
										<!--<div class="form-group">
											<input type="text" class="form-control" placeholder="Hr / Manager"  style="padding:9px 0 9px 10px;" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Enter Your Full Employer Name"  data-validation="required" data-validation-error-msg="Please provide your current employer name">
										</div>-->
										<?php /* ?>
										<div class="margin-top-20"></div>
										<h6>Joined In<span class="red-only"> *</span></h6>
										<div class="row">
											<div class="col-sm-6">
												<select name="join_month" id="join_month" class="city" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="Please provide your join in month">
													<option value="" >Select Month</option>
													<?php 
													for ($m=1; $m<=12; $m++) {
													 $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
													 ?>
													 <option value = '<?php echo $month; ?>' <?php if($join_in!='' && $join_in[0]==$month){ echo "selected"; } ?>><?php echo $month; ?></option>
													 <?php } ?>
												</select>
											</div>
											<div class="col-sm-6">
                                            	<select name="join_year" id="join_year" class="city" style="padding:9px 0 9px 10px;"  data-validation="required" data-validation-error-msg="Please provide your join in year">
													<option value=""><?php echo $custom_lable_arr['year'] ?></option>
													<?php
													$current_yr = date('Y');
													$start_yr = $current_yr - 25;
                                                    for($i = $current_yr ; $i >= $start_yr ; $i--)
												 	{  ?>
                                                        <option value="<?php echo  $i; ?>" <?php if($join_in!='' && $join_in[1]==$i){ echo "selected"; } ?>><?php echo  $i; ?> </option>	
                                                <?php	}
                                                    ?>	
												</select>
											</div>
										</div>
										
										<div class="margin-top-20"></div>
										<h6>Job Profile</h6>
										<div class="form-group">
											<textarea class="form-control" id="job_profile" name="job_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Enter Your Job description"><?php echo ($forstep2svar!='') ? $forstep2svar['job_profile'] : ""?></textarea>
										</div>
                                        <?php */ ?>
										<!--<hr class="margin-top-40"> -->
										
										<!--<div class="margin-top-20"></div>-->
										<!--<h6>Work Exprerience (Role in hiring)<span class="red-only"> *</span></h6>
                                        <div class="radio">
											<label><input type="radio" name="work_experience" value="Recruitment/Placement Consultant" >Recruitment/Placement Consultant<br />
											<span class="small">If you are in a Placement Consulting firm, and provide Hiring or Talent Acquisition solutions to clients</span></label>
										</div>
										<div class="radio">
											<label><input type="radio" name="work_experience" value="Company Recruiter, HR Professional" >Company Recruiter, HR Professional<br />
											<span class="small">If you work in the HR/Recruitments division of a company</span></label>
										</div>
										<div class="radio">
											<label><input type="radio" name="work_experience" value="Hiring Manager/Entrepreneur">Hiring Manager/Entrepreneur<br />
											<span class="small">If you are not in HR but are involved in hiring/interviewing for your team or company.</span></label>
										</div>
										<div class="radio">
											<label><input type="radio" name="work_experience" value=""  >Other<br />
											<span class="small">If you are involved in hiring and recruitment in any other capacity</span></label>

										</div>-->
                                        <div id="radiovaldiv"></div>
									</div>
								</div>
							</div>
                            
                                <div class="clearfix"></div>
                                <div class="panel panel-primary">
                                	<div class="panel-heading th_bgcolor">
                                		<?php echo $custom_lable_arr['skill_hire_for_tit']; ?>
                                	</div>
                                	<div class="panel-body">
                                    	<div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['indu_hire_for_lbl']; ?><span class="red-only"> *</span></h6>
										<div class="">
                                        <select name="industry_hire[]" id="industry_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['indu_hire_for_place']; ?>">
                                            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['industry_hire'])) { echo $this->common_front_model->get_list_multiple('industries_master','str','','str',$forstep2svar['industry_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('industries_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['fn_hire_for_lbl']; ?><span class="red-only"> *</span></h6>
										<div class="">
                                        <select name="function_area_hire[]" id="function_area_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['fn_hire_for_place']; ?>" >
                                            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['function_area_hire'])) { echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str',$forstep2svar['function_area_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('functional_area_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                        <div class="margin-top-20"></div>
										<h6><?php echo $custom_lable_arr['skill_hire_for_lbl']; ?><span class="red-only"> *</span></h6>
										<div class="">
                                        <select name="skill_hire[]" id="skill_hire" class=" chosen-select" multiple placeholder="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['skill_hire_for_place']; ?>">
                                            <?php if($forstep2svar!='' && $this->common_front_model->checkfieldnotnull($forstep2svar['skill_hire'])) { echo $this->common_front_model->get_list_multiple('key_skill_master','str','','str',$forstep2svar['skill_hire'],1); }else{ echo $this->common_front_model->get_list_multiple('key_skill_master','str','','str','',1) ; } ?>
                                            </select>
										</div>
                                	</div>
                                </div>
								<ul class="list-inline pull-right margin-top-20">
									<li><button type="button" class="btn btn-sm btn-default" onClick="prev_step_mm('2')"><?php echo $custom_lable_arr['previous_emp']; ?></button></li>
									<?php /* ?><li><button type="button" class="btn btn-default next-step">Skip</button></li><?php */ ?>
									<li><button type="submit" class="btn btn-sm btn-primary btn-info-full next-step th_bgcolor"><?php echo $custom_lable_arr['sign_up_emp_1stformsave']; ?></button></li>
								</ul>
<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
<input type="hidden" name="hash_tocken_id_return3" id="hash_tocken_id_return3" value="<?php echo $this->security->get_csrf_hash(); ?>"/>
</form>
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<script>
$(document).ready(function(e) {
	
});
// /*$('#currentdesignation').tokenfield({
//     autocomplete: {
//       source: function (request, response) {
//           jQuery.get("<?php //echo $base_url; ?>sign_up_employer/get_suggestion/"+request.term, {
//           }, function (data) {
//              response(data);
//           });
//       },
//       delay: 100
//     },
//     showAutocompleteOnFocus: false
// });
// $('#currentdesignation').on('tokenfield:createtoken', function (event) {
//     var existingTokens = $(this).tokenfield('getTokens');
//     $.each(existingTokens, function(index, token) {
//         if (token.value === event.attrs.value)
//             event.preventDefault();
//     });
// });*/
$(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        /*prevTab($active);*/
		prevTab('#step3');
    });
</script>