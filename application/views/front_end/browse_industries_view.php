<?php $custom_lable_arr = $custom_lable->language; ?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/about-us-banner.jpg); background-size:cover;"><!--class="single"-->
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo  $custom_lable_arr['browse_by_indus_tit']; ?></h2>
			<span><?php echo $custom_lable_arr['browse_by_indus_subtitle']; ?></span>
		</div>
	</div>
</div>

<div id="categories">
<?php if(isset($jobs_by_industry_data) && $jobs_by_industry_data!='' && is_array($jobs_by_industry_data) && count($jobs_by_industry_data) > 0 )
{
	/*echo "<pre>";
	print_r($jobs_by_industry_data);
	echo "</pre>";exit;*/
	$var_to_pass_in_view = array();
	$var_to_pass_in_view['jobs_by_industry_data'] = $jobs_by_industry_data;
	$var_to_pass_in_view['jobs_by_industry_count'] = $jobs_by_industry_count;
	$var_to_pass_in_view['limit_per_page'] = $limit_per_page;
?>
	<div class="container">
			<!--<form action="#" method="get" class="list-search">
				<button><i class="fa fa-search"></i></button>
				<input type="text" placeholder="Search Jobs Category (e.g. php, javascript etc..)" value=""/>
				<div class="clearfix"></div>
			</form>-->
	</div>
    
   <!-- Industries group result -->
	<div id="main_content_ajax">
		<?php $this->load->view('front_end/industry_all_result'); ?>
	</div>
	<!-- Industries group result End --> 
<?php /*?><?php 	
	foreach($jobs_by_industry_data as $jobs_by_industry_data_single)
	{
		
		 ?>
	<div class="categories-group">
		<div class="container">
			<h4><?php //echo $single_ind['industries_name']; ?></h4>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="list-group">
					<a href="#" class="list-group-item">Design <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Logo Design <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Graphic Design <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Video <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Adnimation <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Adobe Photoshop <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Illustration <span class="badge">12</span></a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="list-group">
					<a href="#" class="list-group-item">Art <span class="badge">12</span></a>
					<a href="#" class="list-group-item">3D <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Adobe Illustrator <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Drawing <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Web Design <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Cartoon <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Graphics <span class="badge">12</span></a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="list-group">
					<a href="#" class="list-group-item">Fashion Design <span class="badge">12</span></a>
					<a href="#" class="list-group-item">WordPress <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Editing <span class="badge">12</span></a>
					<a href="#" class="list-group-item">Writing <span class="badge">12</span></a>
					<a href="#" class="list-group-item">T-Shirt Design <span class="badge">12</span></a>
				</div>
			</div>
		</div>
	</div>	
<?php }//foreach end
?><?php */?>
<?php //echo $this->common_front_model->rander_pagination('browse_industries',$jobs_by_industry_count); ?>
<?php }
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>    
</div>    
<?php 
}
?>
</div>
<input type="hidden" name="base_url_ajax" id="base_url_ajax" value="<?php echo $base_url; ?>" />
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/animate.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/slideanim.css">
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">


<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>
<script>
$(document).ready(function(e) {
if($("#ajax_pagin_ul").length > 0)
{
	load_pagination_code();
}
});
</script>