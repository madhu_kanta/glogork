<?php $custom_lable_arr = $custom_lable->language;
if(isset($employer_details) && $employer_details!="" && is_array($employer_details) && count($employer_details) > 0)
{
	$employer_detailsvar = $employer_details;
	$employer_detailsvar['custom_lable_arr'] = $custom_lable_arr;
}
else
{
	$employer_detailsvar = "";
	$mobile = "";
	$this->session->unset_userdata('employer_reg');
}
if($this->session->has_userdata('employer_reg'))
{
	$allsessionstoredforreg = $this->session->userdata('employer_reg');
	$sessionstoredforreg = end($allsessionstoredforreg);
}
else
{
	$sessionstoredforreg = array();
}
?>
<style>
.help-block.form-error
{
	/*white-space:nowrap;*/
}
</style>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url.'assets/front_end/images/banner/emp-reg.jpg'; ?>); background-size:cover;">
	<div class="container">
		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-user"></i> <?php echo $custom_lable_arr['sign_up_emp_header_title']; ?></h2>
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
		<section>
			<div class="wizard">
				<div class="wizard-inner">
					<div class="connecting-line"></div>
					<ul class="nav nav-tabs" role="tablist">
                    <?php //print_r($sessionstoredforreg);?>
						<li id="li_step1" role="presentation" class="li_step <?php if(count($sessionstoredforreg) > 0) { }else{ ?>active<?php }?>">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo $custom_lable_arr['sign_up_emp_1sttabtitle']; ?>">
								<span class="round-tab">
									<i class="fa fa-user" aria-hidden="true"></i>
								</span>
							</a>
						</li>
						<li id="li_step2" role="presentation" class="li_step <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='2'){ echo "active";}elseif($sessionstoredforreg['step'] < '2'){ echo "disabled";} }else{ ?>disabled<?php }?>">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo $custom_lable_arr['sign_up_emp_2ndtabtitle']; ?>">
								<span class="round-tab">
									<i class="fa fa-industry" aria-hidden="true"></i>
								</span>
							</a>
						</li>
						<li id="li_step3" role="presentation" class="li_step <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='3'){ echo "active";}elseif($sessionstoredforreg['step'] < '3'){ echo "disabled";} }else{ ?>disabled<?php }?>">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?php echo $custom_lable_arr['sign_up_emp_3rdtabtitle']; ?>">
								<span class="round-tab">
									<i class="fa fa-suitcase" aria-hidden="true"></i>
								</span>
							</a>
						</li>
						<li id="li_step4" role="presentation" class="li_step <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='4'){ echo "active";}elseif($sessionstoredforreg['step'] < '4'){ echo "disabled";} }else{ ?>disabled<?php }?>">
							<a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="<?php echo $custom_lable_arr['sign_up_emp_4thtabtitle']; ?>">
								<span class="round-tab">
									<i class="fa fa-file-image-o" aria-hidden="true"></i>
								</span>
							</a>
						</li>
						<li id="li_step5" role="presentation" class="li_step <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='5'){ echo "active";}elseif($sessionstoredforreg['step'] < '5'){ echo "disabled";} }else{ ?>disabled<?php }?>">
							<a href="#step5" data-toggle="tab" aria-controls="complete" role="tab" title="<?php echo $custom_lable_arr['sign_up_emp_5thtabtitle']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-ok"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>
					<div class="tab-content">
                        <div class="tab-pane <?php if(count($sessionstoredforreg) > 0) { }else{ ?>active<?php }?>" role="tabpanel" id="step1">
                            <form method="post" name="emplregform" id="emplregform" action="<?php echo $base_url.'sign_up_employer/signupemp'; ?>" >
                            <div class="alert alert-danger" id="message" style="display:none" ></div>
                            <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                                    <div class="step2">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading th_bgcolor"><strong style="color:white;"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $custom_lable_arr['sign_up_emp_1stformtitle_head']; ?></strong>
                                            </div>
                                            <div class="panel-body">
                                                <div class="margin-top-20"></div>
                                                <div class="row">
                                                <div class="col-md-3 col-xs-12" >
                                                <div class="form-group">
                                                			<h5><?php echo $custom_lable_arr['sign_up_emp_1stformtitle']; ?><span class="red-only"> *</span></h5>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                                                <select  class="form-control" name="title" id="title" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformtitlevalmes']; ?>" >
                                                                    <?php
																	if($pers_title != '' && is_array($pers_title) && count($pers_title) > 0)
																	{
																		foreach ($pers_title as $pers_title_single)
																		{ ?>
																			<option value="<?php echo $pers_title_single['id'];?>" <?php if($employer_detailsvar!='' && $pers_title_single['id']==$employer_detailsvar['title'] ){ echo "selected" ; } ?>><?php echo $pers_title_single['personal_titles'];?> </option>	
																		<?php }
																	}
                                                                    ?>
                                                                    </select>
                                                            </div>
                                                </div>            
                                                        </div>
                                                <div class="col-md-9 ">
                                                	<div class="form-group">
                                                    	<h5 class=""><?php echo $custom_lable_arr['sign_up_emp_1stformfullname']; ?><span class="red-only "> *</span></h5>
                                                    	<div class=" input-group "><!-- form-group has-feedback has-feedback-left has-error -->
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                        <!--<i class="glyphicon glyphicon-user form-control-feedback"></i>-->
                                                        <input id="fullname" type="text" class="form-control" name="fullname" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformfullnameplacehold']; ?>" style="padding:9px 0 9px 10px;" data-validation="required" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformfullnamevalmes']; ?>" value="<?php if($employer_detailsvar!=''){ echo $employer_detailsvar['fullname'] ; } ?>">
                                                    </div>
                                                	</div>
                                                </div>
                                                </div>
                                                <div class="margin-top-20"></div>      
                                                <div class="form-group">
                                                	<h5><?php echo $custom_lable_arr['sign_up_emp_1stformabout']; ?></h5>
													<div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <textarea class="form-control" id="job_profile" name="job_profile" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="<?php echo $custom_lable_arr['sign_up_emp_1stformaboutphold']; ?>" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformaboutphold']; ?>" ><?php echo ($employer_detailsvar!='') ? $employer_detailsvar['job_profile'] : ""?></textarea>
												</div>
                                                </div>
                                                <div class="margin-top-20"></div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <h5><?php echo $custom_lable_arr['sign_up_emp_1stformemail']; ?><span class="red-only"> *</span></h5>
                                                        <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
                                                    <input id="emailid_confirmation" type="email" class="form-control" name="emailid_confirmation" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformemailplacehold']; ?>" style="padding:9px 0 9px 10px;text-transform: lowercase;" data-validation="required,email"  data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformemailvalmes']; ?>" value="<?php if($employer_detailsvar!=''){ echo $employer_detailsvar['email'] ; } ?>" />
                                                </div>
                                                	</div>
                                                    </div>
                                                    <div class="col-md-6">		
                                                    <div class="form-group">
                                                        <h5><?php echo $custom_lable_arr['sign_up_emp_1stformemailcnf']; ?><span class="red-only"> *</span></h5>
                                                        <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></span>
                                                    <input id="emailid" type="email" class="form-control" name="emailid" placeholder="<?php echo $custom_lable_arr['sign_up_emp_1stformemailplaceholdcnf']; ?>" style="padding:9px 0 9px 10px;text-transform: lowercase;" data-validation="required,confirmation" data-validation-confirm="emailid_confirmation" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformemailvalmescnf']; ?>" value="<?php if($employer_detailsvar!=''){ echo $employer_detailsvar['email'] ; } ?>"/>
                                                </div>
                                                	</div>
                                                    </div>
                                                </div>
                                                <div class="margin-top-20"></div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="control-group">
                                                        <div class="form-group">
                                                            <h5><?php echo $custom_lable_arr['sign_up_emp_1stformpass']; ?><span class="red-only"> *</span></h5>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                                                                <input id="pass_confirmation" type="password" class="form-control" name="pass_confirmation" style="padding:9px 0 9px 10px;" data-validation="length" data-validation-length="min6" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformpassvalmes']; ?>" />
                                        
                                                            </div>
                                                       </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">		
                                                    <div class="form-group">
                                                    	<h5><?php echo $custom_lable_arr['sign_up_emp_1stformpasscnf']; ?><span class="red-only">*</span></h5>
                                                    	<div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                                                        <input id="pass" type="password" class="form-control" name="pass" style="padding:9px 0 9px 10px;" data-validation="required,confirmation" data-validation-confirm="pass_confirmation" data-validation-error-msg="<?php echo $custom_lable_arr['sign_up_emp_1stformpassvalmescnf']; ?>" />
                                                    </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                
                                                
                                            <br />
                                            </div>
                                        </div>
                                        <div class="margin-top-20"></div>
                                        <ul class="list-inline pull-right">
                                            <li><button type="submit" class="btn btn-sm  btn-primary next-step th_bgcolor"><?php echo $custom_lable_arr['sign_up_emp_1stformsave']; ?> </button></li> <!--onClick="emplregister();"-->
                                        </ul>
                                    </div>
                                    <input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
                            </form>
                        </div>
						<div class="tab-pane <?php if(is_array($sessionstoredforreg) && count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='2'){ echo "active";}elseif($sessionstoredforreg['step'] < '2'){ echo "disabled";} }else{ ?><?php }?>" role="tabpanel" id="step2">
                        	<?php 
							if($this->session->has_userdata('employer_reg') && count($this->session->userdata('employer_reg')) > 0 && $this->session->userdata('employer_reg')!='' && is_array($sessionstoredforreg) && count($sessionstoredforreg) > 0 && ($sessionstoredforreg['step']=='2' || $sessionstoredforreg['step'] > '2'))
							{
								$this->load->view('front_end/sign_up_emp_step2',$employer_detailsvar);
							}
							?>
						</div>
						<div class="tab-pane <?php if(isset($sessionstoredforreg) && is_array($sessionstoredforreg) && count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='3'){ echo "active";}elseif($sessionstoredforreg['step'] < '3'){ echo "disabled";} }else{ ?><?php }?>" role="tabpanel" id="step3">
                        <?php 
							if($this->session->has_userdata('employer_reg') && count($this->session->userdata('employer_reg')) > 0 && $this->session->userdata('employer_reg')!='' && count($sessionstoredforreg) > 0 && ($sessionstoredforreg['step']=='3' || $sessionstoredforreg['step'] > '3'))
							{
								$this->load->view('front_end/sign_up_emp_step3',$employer_detailsvar); 
							}
							?>
						</div>
						<div class="tab-pane <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='4'){ echo "active";}elseif($sessionstoredforreg['step'] < '4'){ echo "disabled";} }else{ ?><?php }?>" role="tabpanel" id="step4">
							<?php 
							if($this->session->has_userdata('employer_reg') && count($this->session->userdata('employer_reg')) > 0 && $this->session->userdata('employer_reg')!='' && count($sessionstoredforreg) > 0 && ($sessionstoredforreg['step']=='4' || $sessionstoredforreg['step'] > '4'))
							{
								$this->load->view('front_end/sign_up_emp_step4',$employer_detailsvar); 
							}
							?>
						</div>
						<div class="margin-top-20"></div>
						<div class="tab-pane <?php if(count($sessionstoredforreg) > 0) { if($sessionstoredforreg['step']=='5'){ echo "active";}elseif($sessionstoredforreg['step'] < '5'){ echo "disabled";} }else{ ?><?php }?>" role="tabpanel" id="step5">
                        	<?php 
							if($this->session->has_userdata('employer_reg') && count($this->session->userdata('employer_reg')) > 0 && $this->session->userdata('employer_reg')!='' && count($sessionstoredforreg) > 0 && ($sessionstoredforreg['step']=='5' || $sessionstoredforreg['step'] > '5'))
							{
								$this->load->view('front_end/sign_up_emp_step5',$employer_detailsvar); 
							}
							?>
						</div>
						<div class="clearfix"></div>
					</div>
			</div> 
		</section>
   </div>
</div>

<!-- Content
================================================== -->
<link href="<?php echo $base_url; ?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<!--<script src="<?php // echo $base_url; ?>assets/front_end/js/jquery_form_validator-2.3.26.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<!-- WYSIWYG Editor -->
<script type="text/javascript" src="<?php echo $base_url;?>assets/front_end/js/jquery.sceditor.bbcode.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url;?>assets/front_end/js/jquery.sceditor.js"></script>

<script>
$.validate({
    form : '#emplregform',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg").hide();
      $("#message").slideDown();
	  /*$("#message").focus();*/
	  $("#message").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message").hide();
	  var datastring = $("#emplregform").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'sign_up_employer/signupemp' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.successmessage);
				scroll_to_div('success_msg',-100);
				setTimeout(function () {
                	if($.trim(data.stepnoreturn) != '' && $.trim(data.stepnoreturn) != 'undefined' && $.trim(data.stepnoreturn) != 'null' && $.trim(data.stepnoreturn) != null)
					{
						//getstep(data.stepnoreturn);
						getstep('2');
					}
					else
					{
						getstep('2');
					}
					     
                 }, 1500);
				
			}
			else
			{
				$("#success_msg").slideUp();
				$("#message").html(data.errormessage);
				$("#message").slideDown();
				scroll_to_div('message',-100);
			}	
				hide_comm_mask();	
		}
	});
	  
      return false; // Will stop the submission of the form
    }
  });
function validateform2()
{
	$.validate({
    form : '#emplregform2',
    modules : 'security, file',
    onError : function($form) {
	  $("#success_msg2").hide();
      $("#message2").slideDown();
	  $("#message2").focus();
	  $("#message2").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message2").hide();
	  //var datastring = $("#emplregform2").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var filename = $("#company_logo").val();
	  var form_data = new FormData();
	  var formDataArray = $("#emplregform2").serializeArray();
	  for(var i = 0; i < formDataArray.length; i++)
	  {
		var formDataItem = formDataArray[i];
		form_data.append(formDataItem.name, formDataItem.value);
	  }
	 	var fd = new FormData();
		var file_data = $('#company_logo')[0].files; // for multiple files
		//fd.append('company_logo', file_data.files[0]); 
		if(filename!='')
		{
			fd.append('company_logo', $('#company_logo')[0].files[0]); 	
		}
		else
		{
			//fd.append('company_logo', ''); 	
		}
		/*for(var i = 0;i<file_data.length;i++){
			fd.append("file_"+i, file_data[i]);
		}*/
		var other_data = $("#emplregform2").serializeArray();
		$.each(other_data,function(key,input){
			fd.append(input.name,input.value);
		});
	  fd.append('csrf_job_portal', hash_tocken_id);
	  //var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'sign_up_employer/signupemp2' ?>",
		type: 'post',
		data: fd,
		dataType:"json",
		contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
	    processData: false, // NEEDED, DON'T OMIT THIS
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message2").slideUp();
				$("#success_msg2").slideDown();
				$("#success_msg2").html(data.successmessage);
				scroll_to_div('success_msg2',-100);
				setTimeout(function () {
                	if($.trim(data.stepnoreturn) != '' && $.trim(data.stepnoreturn) != 'undefined' && $.trim(data.stepnoreturn) != 'null' && $.trim(data.stepnoreturn) != null)
					{
						//getstep(data.stepnoreturn);
						getstep('3');
					}
					else
					{
						getstep('3');
					}					     
                 }, 1500);
				
				$(".prev-step").click(function (e) {
					var $active = $('.wizard .nav-tabs li.active');
					prevTab($active);
				});
			}
			else
			{
				$("#success_msg2").slideUp();
				$("#message2").html(data.errormessage);
				$("#message2").slideDown();
				scroll_to_div('message2',-100);
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateform3()
{
	$.validate({
    form : '#emplregform3',
    modules : 'security',
    onError : function($form) {
	  $("#success_msg3").hide();
      $("#message3").slideDown();
	  $("#message3").focus();
	  $("#message3").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  /*if(!validateradio())
		{
			return false;
		}*/
    },
    onSuccess : function($form) {
		/*if(!validateradio())
		{
			return false;
		}*/
	  show_comm_mask();
	  $("#message3").hide();
	  var datastring = $("#emplregform3").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
		$.ajax({
		url : "<?php echo $base_url.'sign_up_employer/signupemp3' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message3").slideUp();
				$("#success_msg3").slideDown();
				$("#success_msg3").html(data.successmessage);
				scroll_to_div('success_msg3',-100);
				setTimeout(function () {
                	if($.trim(data.stepnoreturn) != '' && $.trim(data.stepnoreturn) != 'undefined' && $.trim(data.stepnoreturn) != 'null' && $.trim(data.stepnoreturn) != null)
					{
						//getstep(data.stepnoreturn);
						getstep('4');
					}
					else
					{
						getstep('4');
					}
                 }, 1500);
				
				$(".prev-step").click(function (e) {
					var $active = $('.wizard .nav-tabs li.active');
					prevTab($active);
				});
			}
			else
			{
				$("#success_msg3").slideUp();
				$("#message3").html(data.errormessage);
				$("#message3").slideDown();
				scroll_to_div('message3',-100);
			}
			hide_comm_mask();
		}
	});
	  	
      return false; // Will stop the submission of the form
    }
  });
}
function validateform4()
{
	$.validate({
    form : '#emplregform4',
    modules : 'file',
    onError : function($form) {
	  $("#success_msg4").hide();
      $("#message4").slideDown();
	  //$("#message4").focus();
	  $("#message4").html("<?php echo $custom_lable_arr['sign_up_emp_form_val_mes']; ?>");
	  scroll_to_div('message4',-100);
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#message4").hide();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var htmlobject=$( "#profile_img" ).get( 0 );
	  var filename = $("#profile_img").val();
	  var form_data = new FormData();
	  if(filename!='')
	  {
		form_data.append('profile_pic', $('input[name=profile_img]')[0].files[0]);  
	  }
	  
	  form_data.append('user_agent', 'NI-WEB');
	  form_data.append('csrf_job_portal', hash_tocken_id);
	  if(filename!='')
		{
	  		$.ajax({
		url : "<?php echo $base_url.'sign_up_employer/signupemp4' ?>",
		type: "POST",             // Type of request to be send, called as method
		data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		dataType:"json",
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		xhr: function(){
        //upload Progress
        var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				$("#imageprogressbardiv").show();
				xhr.upload.addEventListener('progress', function(event) {
					var percent = 0;
					var position = event.loaded || event.position;
					var total = event.total;
					if (event.lengthComputable) {
						percent = Math.ceil(position / total * 100);
					}
					//update progressbar
					$("#imageprogressbar").css("width", + percent +"%");
					$("#imageprogressbar").attr("aria-valuenow",  percent );
					$("#imageprogressbar").text(percent +"% <?php echo $custom_lable_arr['emp_edit_data_image_succ_comp']; ?> ");
				}, true);
			}
			return xhr;
		},
    	mimeType:"multipart/form-data",
		success: function(data)
		{	
			$("#hash_tocken_id").val(data.tocken);
			if($.trim(data.status) == 'success')
			{
				$("#message4").slideUp();
				$("#success_msg4").slideDown();
				$("#success_msg4").html(data.successmessage);
				scroll_to_div('success_msg4',-100);
				$("#profile_img").val('');
				$("#blah").hide();
				$("#image_val").removeAttr("class").addClass("alert alert-success alert-dismissable fade in");
				$("#image_val").html('<div class="col-xs-10">'+'<?php echo $custom_lable_arr['emp_edit_data_image_succ']; ?>'+'</div><div class="col-xs-2"><a href="javascript:void(0)" class="close" onclick="hideid1()"  aria-label="close">&times;</div>');<!--data-dismiss="alert"-->
				/*$("#image_val").show().css("opacity","1");
				setTimeout(function() { $("#image_val").hide(); }, 10000);*/
				$("#imagediv").removeAttr("class").addClass('col-lg-4 col-md-4 col-sm-4 col-xs-12 marginclass');
				setTimeout(function () {
                	if($.trim(data.stepnoreturn) != '' && $.trim(data.stepnoreturn) != 'undefined' && $.trim(data.stepnoreturn) != 'null' && $.trim(data.stepnoreturn) != null)
					{
						//getstep(data.stepnoreturn);
						getstep('5');
					}
					else
					{
						getstep('5');
					}
                 }, 1500);
				
				$(".prev-step").click(function (e) {
					var $active = $('.wizard .nav-tabs li.active');
					prevTab($active);
				});
			}
			else
			{
				$("#success_msg4").slideUp();
				$("#message4").html(data.errormessage);
				$("#message4").slideDown();
				$("#profile_img").val('');
				$("#imagediv").removeAttr("class").addClass("alert alert-danger alert-dismissable fade in");
				$("#image_val").html('<div class="col-xs-11">'+data.errormessage+'</div><div class="col-xs-1"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');/*data-dismiss="alert"*/
				$("#image_val").show().css("opacity","1");
				setTimeout(function() { $("#image_val").hide(); }, 10000);
				scroll_to_div('message4',-100);
			}
		hide_comm_mask();	
		}
	});
		}
	  	
      return false; // Will stop the submission of the form
    }
  });
}

function prev_step_mm(stepno)
{
	var $active = $('.wizard .nav-tabs li.active');
    prevTab($active);
	
	$('html, body').animate({
		scrollTop: $('.wizard').offset().top -55 }, 'slow');
	//alert(stepno);
	//$(".tab-pane").removeClass("active");
	//$(".li_step").removeClass("active");
	return false;
	//getstep(stepno);
}
function getstep(stepno)
{	
	var urlforstep = '<?php echo $base_url.'sign_up_employer/get_form_part' ?>'+'/'+stepno;
	var hash_tocken_id = $("#hash_tocken_id").val();
  	var datastring = "&csrf_job_portal="+hash_tocken_id;
	$.ajax({
		url : urlforstep,
		type: 'post',
		data: datastring,
		dataType:"html",
		async:false,
		success: function(data)
		{	
			$("#step"+stepno).html(data);
			var $active = $('.wizard .nav-tabs li.active');
			var $active_worth = $( "a[href='#step"+stepno+"']" ).closest('li');
			$active_worth.removeClass('disabled');
			$( "a[href='#step"+stepno+"']" ).click();
			//$active_worth.next().removeClass('disabled');
			/*nextTab($active_worth);*/
			//$(elem).next().find('a[data-toggle="tab"]').click();
			if(stepno=='2')
			{
				var cominginput = $(data).find("#hash_tocken_id_return2");
				var comingtoken = $(cominginput).val();
				$("#hash_tocken_id").val(comingtoken);
				validateform2();
				$("#company_logo").change(function() {
					if($("#company_logo").length > 0)
					{
						var myimage = $("#company_logo")[0];
						if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
						{
							document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
							$("#blah1").slideDown();
						}
						else
						{
							$("#blah1").hide();
							document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
							showmessage('Invalid image','10000');
						}
					}
				});
			}
			if(stepno=='3')
			{
				var cominginput = $(data).find("#hash_tocken_id_return3");
				var comingtoken = $(cominginput).val();
				$("#hash_tocken_id").val(comingtoken);
				validateform3();
				$(".chosen-select").chosen().trigger("chosen:updated");
			}
			if(stepno=='4')
			{
				var cominginput = $(data).find("#hash_tocken_id_return4");
				var comingtoken = $(cominginput).val();
				$("#hash_tocken_id").val(comingtoken);
				validateform4();
				var accheader = $(data).find(".accordion-header");
				$(accheader).addClass('active-header');
				$("#profile_img").change(function() {
					if($("#profile_img").length > 0)
					{
						var myimage = $("#profile_img")[0];
						if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
						{
							document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
							$("#blah").slideDown();
						}
						else
						{
							$("#blah").hide();
							document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
							/*$("#profile_img").val('');*/
							showmessage('Invalid image','10000');
						}
					}
				});
			}
			if(stepno=='5')
			{
				$('a[data-toggle="tab"]').attr("href",'');
			}
		},
		error: function(data)
		{	
			console.log(data);
		}
	});	
	hide_comm_mask();
}
</script>
<script>
$(document).ready(function () {
	validateform2();
	validateform3();
	validateform4();
    //Initialize tooltips
	$("#success_msg").hide();
	$("#message").hide();
    $('.nav-tabs > li a[title]').tooltip();
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
    /*$(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
    });*/
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
<?php if(count($sessionstoredforreg) > 0){?> 
	
<?php }  ?>
<?php if($employer_detailsvar!=""){?> 
dropdownChange('country','city','city_list',function() {setcityval();});
//$.when(dropdownChange('country','city','city_list')).then(setcityval());
function setcityval()
{
	$("#city").val('<?php echo $employer_detailsvar['city']; ?>');
}
<?php }  ?>	
});
function nextTab(elem) {
	$(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
//according menu
$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
	$('.accordion-header').toggleClass('inactive-header');
	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});
	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');
	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}

		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	return false;
});
</script>
<script>
<!-----------------Radio Button-------------------->
$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});
$('#radioBtn1 a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});
<!-----------------Popover-------------------->

$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
	
});
function join_monthchange()
{
	var currentyear = $("#join_year option:eq(1)").val();
	var currentmonth = '<?php echo intval(date('m')); ?>';
	var selectedmonth =  $("#join_month").val();
	if(selectedmonth > currentmonth)
	{
		$("#join_year option:eq(1)").hide();
	}
	else
	{
		$("#join_year option:eq(1)").show();
	}
}
$( "#join_month" ).change(function() {
  join_monthchange();
});

$("#profile_img").change(function() {
	if($("#profile_img").length > 0)
	{
		var myimage = $("#profile_img")[0];
		if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
		{
			document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
			$("#blah").slideDown();
		}
		else
		{
			$("#blah").hide();
			document.getElementById('blah').src = window.URL.createObjectURL(myimage.files[0]);
			/*$("#profile_img").val('');*/
			showmessage('Invalid image','10000');
		}
	}
});
$("#company_logo").change(function() {
	if($("#company_logo").length > 0)
	{
		var myimage = $("#company_logo")[0];
		if(myimage.files[0].type=='image/jpeg' || myimage.files[0].type=='image/png' || myimage.files[0].type=='image/gif')
		{
			document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
			$("#blah1").slideDown();
		}
		else
		{
			$("#blah1").hide();
			document.getElementById('blah1').src = window.URL.createObjectURL(myimage.files[0]);
			showmessage('Invalid image','10000');
		}
	}
});

/*function validateradio()
{
	var returnvar = true;
	if($('input[name=work_experience]:checked').length<=0)
	{
		$('#radiovaldiv').html("<span class='help-block form-error'>Please provide your work experience (Role in hiring)</span>");
	  	returnvar = false;
	}
	else
	{
		$('#radiovaldiv').html("");
	}
	return returnvar ;
}*/
function showmessage(message,fortime)
{
		$("#image_val").show().css("opacity","1");
		$("#image_val").addClass("row alert alert-danger alert-dismissable fade in");
		$("#image_val").html('<div class="col-xs-9">'+message+'</div><div class="col-xs-3"><a href="javascript:void(0)"  class="close" onclick="hideid1()"  aria-label="close">&times;</div>');
		setTimeout(function() { $("#image_val").hide(); }, fortime);
}
function hideid1()
{
	$("#image_val").hide().removeClass("alert alert-danger alert-dismissable fade in");
}
</script>