<?php
$custom_lable_arr = $custom_lable->language;
if($total_post_count!='' && $total_post_count > 0 && isset($emp_posted_job) && $emp_posted_job!='' && is_array($emp_posted_job) && count($emp_posted_job) > 0)
{
?>

<?php
					if(isset($emp_posted_job) && $emp_posted_job !='' && is_array($emp_posted_job) && count($emp_posted_job) > 0)
					{
						$j=0;
					foreach($emp_posted_job as $posted_job)
					{
						if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
						{
							$js_id = $this->common_front_model->get_userid();
							$where_arra = array('js_id'=>$js_id,'job_id'=>$posted_job['id']);
							$jobseeker_viewed_jobs = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,1);
							$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$posted_job['posted_by']);
							$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
							$check_already_apply = $this->common_front_model->get_count_data_manual('job_application_history',$where_arra,1,'','','',1);
						}
						$j++;
						$viewed_job_count = $this->common_front_model->get_counter('viewed_job',$posted_job['id']);
		   				$liked_job_count = $this->common_front_model->get_counter('liked_job',$posted_job['id']);
		   				$applied_job_count = $this->common_front_model->get_counter('applied_job',$posted_job['id']);
					?>
					<div class="box-main <?php if(isset($j) && $j>1){echo 'margin-top-25';}else{ echo'margin-top-20';}?>">
						<div class="row">
							<div class="col-md-8 col-xs-12 col-sm-8 new-margin-col">
                            <a href="<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($posted_job['id']);?>"  target="_blank" ><h2 class="box-main-h2"><?php echo $posted_job['job_title']; ?> (<?php
									if($posted_job['work_experience_from']!='' && $posted_job['work_experience_to']!='')
									{
										if($posted_job['work_experience_from']=='0' && $posted_job['work_experience_to']=='0')
										{
											echo "Fresher";
										}
										else
										{
											echo intval($posted_job['work_experience_from']).' - '.intval($posted_job['work_experience_to']). '  Yrs Exp.';
										}
									}
									else
									{
										echo $custom_lable_arr['notavilablevar'];
									}
									?>)</h2></a>
								<div class="box-share margin-top-15">
									<span> </span>
									<span class="share"><img src="<?php echo $base_url;?>assets/front_end/images/share.png" alt=""> <?php echo $custom_lable_arr['share']; ?> : </span>
									<?php
								$company_logo = '';
								if(isset($posted_job['company_logo']) && $posted_job['company_logo']!='')
								{
									$company_logo = $base_url.'assets/company_logos/'.$posted_job['company_logo'];
								}
								?>
									<div class="social-iocn-5">
										<div class="social-icons-2">
											<div><a href="" class="social-icon-2" rel="publisher"  data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_facebook']; ?>" onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($company_logo); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"> <i class="fa fa-facebook"></i></a></div>

											<div><a href="" class="social-icon-2" rel="linkedin"  data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_linkedin']; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  echo $base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']); ?>&title=<?php echo urlencode($posted_job['job_title']); ?>&summary=<?php echo urlencode($posted_job['job_title']); ?>&source=<?php echo urlencode($posted_job['job_title']); ?>')"> <i class="fa fa-linkedin"></i></a></div>
											<div><a href="" class="social-icon-2" data-toggle="tooltip"  title="<?php echo $custom_lable_arr['share_twitter']; ?>" onclick="window.open('http://twitter.com/home?status=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id'])); ?>')"> <i class="fa fa-twitter"></i></a></div>
											<div><a href="" class="social-icon-2" rel="pinterest"  data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_pinterest']; ?>" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id'])); ?>	&media=<?php echo urlencode($company_logo); ?>&description=<?php echo urlencode($posted_job['job_title']); ?>')"> <i class="fa fa-pinterest"></i></a></div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5 col-xs-12 col-sm-5 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/job-1.png" alt=""></span><span class="job-d"><?php echo $custom_lable_arr['job_shift']; ?> : <?php echo $posted_job['job_shift_type']; ?></span></p>
									</div>
									<div class="col-md-7 col-xs-12 col-sm-7 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/point.png" alt=""></span><span class="job-d">Location :<?php
									echo $this->common_front_model->checkfieldnotnull($posted_job['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($posted_job['location_hiring']) : $custom_lable_arr['notavilablevar']; ?></span></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5 col-xs-12 col-sm-5 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/diamond-2.png" alt=""></span><span class="job-d"><?php echo $custom_lable_arr['total_requirenment']; ?> : <?php echo $posted_job['total_requirenment']; ?></span></p>
									</div>
									<div class="col-md-7 col-xs-12 col-sm-7 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/payment-method.png" alt=""></span><span class="job-d">Salary : <?php if(isset($posted_job['job_salary_from']) && $posted_job['job_salary_from']!='')
									{
										if(is_numeric($posted_job['job_salary_from']))
										{
											  echo $this->common_front_model->valueFromId('salary_range',$posted_job['job_salary_from'],'salary_range').' lacs';
										  }
										  else if($posted_job['job_salary_from']!='')
										  {
											 echo  $posted_job['job_salary_from'];
										  }
									}else{
										echo $custom_lable_arr['notavilablevar'];
									}
									?> </span></p>
									</div>
								</div>

							</div>
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="row">
									<div class="col-md-12 col-xs-12 col-sm-3">
										<a href="" class="btn-job-new pull-right margin-top-10 button-mobile-new" style="border: 1px solid #e1b8bb;"><img src="<?php echo $base_url;?>assets/front_end/images/like.png" class="like-img" alt=""> <?php echo $custom_lable_arr['liked']; ?> : <?php echo $liked_job_count; ?></a>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-xs-12 col-sm-3">
										<p class="text-align-mobile" style="font-size: 15px; text-align:right;
										float: right;
										margin-top: 15px;font-family:'Poppins', sans-serif;"><?php echo substr(htmlspecialchars_decode($posted_job['job_description'],ENT_QUOTES),0,100) ?>...</p>
									</div>
								</div>
								<!-- <div class="row">
									<div class="col-md-12 col-xs-12 col-sm-3">
									<?php
									//$skill_keyword = explode(',',$posted_job['skill_keyword']);
									//foreach($skill_keyword as $skill)
									//{
									//	echo '<a href="#" class="btn-job-new pull-right pull-left-m">'.$skill.'</a>';
									//}
									?>
										 <a href="#" class="btn-job-new pull-right pull-left-m">Developing</a>
									</div>
								</div> -->

							</div>
						</div>
						<div class="row">
						<div class="col-md-12 col-xs-12 col-sm-12">
								<?php
									$skill_keyword = explode(',',$posted_job['skill_keyword']);
									$i=0;
									foreach($skill_keyword as $skill)
									{
										echo '
										<a class="btn-job-new new-check-box-modal">'.$skill.'</a>';
										$i++;
										if($i==6) break;
									}
									?>

										</div>
                        		</div>
						<div class="row">
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a href="" class="btn-job-new"><i class="fa fa-eye"></i> <?php echo $custom_lable_arr['viewd']; ?> : <?php echo $viewed_job_count; ?></a>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a href="" class="btn-job-new"><i class="fa fa-paper-plane"></i> <?php echo $custom_lable_arr['applied']; ?> : <?php echo $applied_job_count; ?></a>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a class="btn-job-new" onClick="window.open('<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($posted_job['id']);?>','_blank');" ><i class="fa fa-plus"></i> <?php echo $custom_lable_arr['more_details']; ?></a>
							</div>
							<?php
						if(!$this->common_front_model->checkLoginfrontempl() && $this->common_front_model->checkLoginfrontempl()==false )
						{
							$save_action = 'save_job';
							$save_text = $custom_lable_arr['save_job'];
							$save_icon = '<i class="fa fa-save"></i> ';
							$block_action = 'block_emp';
							$block_icon = '<i class="fa fa-ban"></i> ';
							$block_text = $custom_lable_arr['block'];
							if(isset($jobseeker_viewed_jobs) && $jobseeker_viewed_jobs!='' && is_array($jobseeker_viewed_jobs) && count($jobseeker_viewed_jobs) > 0)
							{
								if($jobseeker_viewed_jobs['is_saved']=='Yes')
								{
									$save_text = $custom_lable_arr['saved'];
									$save_icon = "<i class='fa fa-check'></i>";
									$save_action = 'remove_save_job';
								}
							}
							if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) && count($jobseeker_access_employer) > 0)
							{
								if($jobseeker_access_employer['is_block']=='Yes')
								{
									$block_action = 'unblock_emp';
									$block_icon = '<i class="fa fa-check"></i>';
									$block_text = $custom_lable_arr['blocked'];
								}
							}
						?>
							<div class="col-md-2 col-xs-6 col-sm-2">
							<?php
							if(isset($check_already_apply) && $check_already_apply!='' && is_array($check_already_apply) && count($check_already_apply) > 0)
							{ ?>
								<a class="btn-job-new apply" data-toggle="tooltip" title=" <?php echo $custom_lable_arr['already_apply']; ?>" style="width: 122px;padding: 6px 1px;font-size: 12px;"><i class="fa fa-check"></i> <?php echo $custom_lable_arr['already_apply']; ?></a>
								<?php }
							else
							{ ?>
								<a href="" class="btn-job-new apply apply_for_job<?php echo $posted_job['id']; ?>" onClick="apply_for_job('<?php echo $posted_job['id']; ?>');" data-toggle="tooltip" title=" <?php echo $custom_lable_arr['apply']; ?>"><i class="fa fa-paper-plane"></i> <?php echo $custom_lable_arr['apply']; ?></a>
							<?php } ?>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a href="" class="btn-job-new save-job save_job_button<?php echo $posted_job['id']; ?>" onClick="return jobseeker_action('<?php echo $save_action; ?>','job','<?php echo $posted_job['id']; ?>');" data-toggle="tooltip" title=" <?php echo $save_text ; ?>" save_job_button><?php echo $save_icon; ?> <?php echo $save_text ; ?></a>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a href="" class="btn-job-new block emp_block_action<?php echo $posted_job['posted_by']; ?>" data-toggle="tooltip" title="<?php echo $block_text; ?>" OnClick="return jobseeker_action('<?php echo $block_action; ?>','Emp','<?php echo $posted_job['posted_by']; ?>');"><?php echo $block_icon ; ?> <?php echo $block_text; ?></a>
							</div>
							<?php }
							else
					   		{
								if($posted_job['currently_hiring_status']=='Yes')
								{
									$class = 'label label-success';
									$lable = $custom_lable_arr['job_open_status'];
									$icon = 'fa fa-folder-open';
								}
								else
								{
									$class = 'label label-danger';
									$lable = $custom_lable_arr['job_closed_status'];
										$icon = 'fa fa-times-circle';
								}?>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a class="btn-job-new save-job" data-toggle="tooltip" title="<?php echo $custom_lable_arr['edit']; ?>" target="_blank"  onClick="window.location='<?php echo $base_url;?>job-listing/edit-posted-job/<?php echo base64_encode($posted_job['id']); ?>'"><i class="fa fa-save"></i> <?php echo  $custom_lable_arr['edit']; ?></a>
							</div>
							<div class="col-md-2 col-xs-6 col-sm-2">
								<a class="btn-job-new save-job <?php echo $class; ?>" data-toggle="tooltip" title="<?php echo $custom_lable_arr['currently_job_status']; ?>"><i class="<?php echo $icon; ?>"></i> <?php echo $lable; ?></a>
							</div>
							<?php
					   }
					    ?>
                        </div>
						</div>
                        <?php
                    }

					}//foreach ?>


                    <?php
 }
else
{
?>
<div class="five columns">
	<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
</div>
<?php }
?>
                    <input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />