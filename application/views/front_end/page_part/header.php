<?php 
	$userexitstvar = false;
	$custom_lable_arr = $custom_lable->language;
	$config_data = $this->common_front_model->data['config_data'];
	if(isset($this->data['social_access_detail']) && $this->data['social_access_detail']!='')
	{ 
	$social_access_detail = $this->data['social_access_detail'];
	}
	else
	{
		$where = array("status"=>'APPROVED',"is_deleted"=>'No');
		$check_social_access = $this->common_front_model->get_count_data_manual('social_login_master',$where,'2','social_name,client_key,client_secret');
	if($check_social_access!='' && is_array($check_social_access) && count($check_social_access) > 0 )
	{ 
		$this->data['social_access_detail'] = $check_social_access;
		$social_access_detail = $check_social_access;//print_r($social_access_detail);
		$gplus_detail = '';
		if(isset($this->data['social_access_detail']['1']) && $this->data['social_access_detail']['1']!='')
		{
			$gplus_detail = $this->data['social_access_detail']['1'];
		}
		//$this->data['gplus_detail'] = $gplus_detail;
		
		}
	}
	//$gplus_detail = $this->data['gplus_detail'];
	if($this->common_front_model->checkLoginfront())
	{
		$session_data = $this->session->userdata('jobportal_user');
		$where_arra = array('id'=>$session_data['user_id'],'status'=>'APPROVED','is_deleted'=>'No');
		$user_data_count = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,0,'');
		if($user_data_count!='' && $user_data_count > 0 )
		{
			$userexitstvar = true;
		}
		else
		{
			$userexitstvar = false;
			$this->session->unset_userdata('jobportal_user');
		}
	}
	else
	{
		if(isset($config_data['website_title']) && $config_data['website_title'] !='')
		{
			$page_title = $config_data['website_title'];
		}
	}


$active_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


?>
<!DOCTYPE html>
<html lang="en">
<!--  xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" -->
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php if(isset($config_data['upload_favicon']) && $config_data['upload_favicon'] !='' && file_exists('./assets/logo/'.$config_data['upload_favicon'])) { ?>
 <link rel="shortcut icon" href="<?php echo $base_url.'assets/logo/'.$config_data['upload_favicon']; ?>" />
<?php } ?>

<meta name="description" content="<?php if(isset($config_data['website_description']) && $config_data['website_description'] !=''){ echo $config_data['website_description'];} ?>" />
<meta name="keywords" content="<?php if(isset($config_data['website_keywords']) && $config_data['website_keywords'] !=''){ echo $config_data['website_keywords'];} ?>" />
    
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/bootstrap3.3.7.css?ver=1.0">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/style.css?ver=1.44">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/colors/custom.css?ver=<?php echo $config_data['colour_name'];?>" id="colors">
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-3.3.7.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/common.js?k=999"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<?php
if(!$this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') == "")
{  ?>
<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script><?php } ?>

			<style>
			.navbar-content small
			{
				word-wrap: break-word;
			}
			/* .menu-active
			{
				background-color: #008000 !important;
				color: #fff !important;
			} */

			.menu-active
			{
			border-bottom: 3px solid #9da5a459;
			background-color: #ff3434 !important;
			color: #ffffff !important;
			border-radius: 5px 5px 5px 5px;
			}
			
			.ui-tooltip-content {
			font-size:10px!important;
			font-family:Calibri;
			}
			.menu ul > li > a {
			padding: 10px 12px;
			}
			.new-reg{
			background-color: #ff3434!important;
			color: #fff!important;
			border: none;
			box-shadow: 2px 2px 4px rgba(0,0,0, .2);
			border-radius: 7px;
			}
			.new-login{
			background-color:#3c3c3c!important;
			color: #fff!important;
			border: none;
			box-shadow: 2px 2px 4px rgba(0,0,0, .2);
			border-radius: 7px;
			}
			
		</style>
</head>
<body>
<div id="wrapper"> <!--This wrapper will be end in the footer page  -->
<header class="sticky-header">
<div class="container">
	<div class="sixteen columns">
		<!-- Logo -->
		<div id="logo">        

                            
			<h1><a href="<?php echo $base_url; ?>">
							<?php  if(isset($config_data['upload_logo']) && $config_data['upload_logo'] !='' && file_exists('./assets/logo/'.$config_data['upload_logo']))
                                {  ?>
                				<img src="<?php echo $base_url.'assets/logo/'.$config_data['upload_logo']; ?>" alt="<?php echo $config_data['web_frienly_name']; ?>"/> 
<!--class="img-responsive"-->
                                <?php }else { ?>
                                <img src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $config_data['web_frienly_name'] ;?>" class="img-responsive" />
                                <?php } ?>
                                </a></h1>
		</div>

		<!-- Menu -->
		<nav id="navigation" class="menu">
			<ul id="responsive" class="header_menu_for_active">

				<li><a id="home-active" <?php if($active_url == $base_url ) {echo "class='menu-active'  ";} ?>  href="<?php echo $base_url; ?>"><?php echo $custom_lable_arr['header_menu_home_txt'];?></a></li>
				<!--<li><a href="#">Pages</a>
					<ul>
						<li><a href="job-page.html">Job Page</a></li>
						<li><a href="job-page-alt.html">Job Page Alternative</a></li>
						<li><a href="resume-page.html">Resume Page</a></li>
						<li><a href="shortcodes.html">Shortcodes</a></li>
						<li><a href="icons.html">Icons</a></li>
						<li><a href="pricing-tables.html">Pricing Tables</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</li>

				<li><a href="#">For Candidates</a>
					<ul>
						<li><a href="browse-jobs.html">Browse Jobs</a></li>
						<li><a href="browse-categories.html">Browse Categories</a></li>
						<li><a href="add-resume.html">Add Resume</a></li>
						<li><a href="manage-resumes.html">Manage Resumes</a></li>
						<li><a href="job-alerts.html">Job Alerts</a></li>
					</ul>
				</li>
-->                
                <li><a id="posted-job-active" <?php if($active_url == $base_url.'job-listing/posted-job' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>job-listing/posted-job"  >Browse Jobs</a></li>
                <li><a id="recruiters-active" <?php if($active_url == $base_url.'recruiters' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>recruiters">Recruiters</a></li>
                <li><a id="recruiters-companies-active" <?php if($active_url == $base_url.'recruiters/companies' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>recruiters/companies">Companies</a></li>
               
				<?php if($this->common_front_model->checkLoginfront())
				{ ?>
                <!--<li><a href="<?php echo $base_url; ?>my-profile">My Profile </a></li>-->
                <li><a id="message-active" <?php if($active_url == $base_url.'my-message' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>my-message">Message</a></li>
                <li ><a  <?php if($active_url == $base_url.'my-plan/current-plan' || $active_url == $base_url.'my-plan/index' ||  $active_url == $base_url.'my-plan/buy-now' ) {echo "class='menu-active'" ;} ?> href="#"  >Upgrade Plan</a>
					<ul>
						<li><a href="<?php echo $base_url; ?>my-plan/current-plan">Current Plan<?php //echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>
						<li><a href="<?php echo $base_url; ?>my-plan/index">Membership Plan<?php //echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
					</ul>
				</li>
                <?php 
				}
				else
				{
				?>
                <li  ><a href="#" <?php if($active_url == $base_url.'login-employer' || $active_url == $base_url.'sign-up-employer'  ) {echo "class='menu-active '  ";} ?> ><?php echo $custom_lable_arr['header_menu_foemp'];?></a>
					<ul>
						<li><a href="<?php echo $base_url; ?>login-employer"><?php echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>
						<li><a href="<?php echo $base_url; ?>sign-up-employer"><?php echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
					</ul>
				</li>
                <?php
				}
				?>
				<!--<li><a href="<?php echo $base_url; ?>blog/allblogs"><?php echo $custom_lable_arr['footer_menu_blog_txt'];?></a></li>-->
			</ul>
			<?php if(!$this->common_front_model->checkLoginfront() || ($this->common_front_model->checkLoginfront() && !$userexitstvar)){ ?>
            
			<ul class="responsive float-right">
				<li><a href="<?php echo $base_url; ?>sign-up" class="new-reg"><i class="fa fa-lg fa-user-plus"></i> <?php echo $custom_lable_arr['reg_tab_register']; ?></a></li>
                <?php 
				if (strpos($_SERVER["REQUEST_URI"],'sign_up') === FALSE && strpos($_SERVER["REQUEST_URI"],'sign-up')  === FALSE){
					
					       if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='')
								 {
								 	$js_login_detail = json_decode(get_cookie('js_login_detail'));
                                 } 
						 
					?>
                    <!--hidden-lg hidden-md-->
                <li class=""><a href="<?php echo $base_url; ?>sign-up/login" class="new-login"><i class="fa fa-user"></i> Login</a></li>
				<!--<li class="dropdown dropdown-login hidden-sm hidden-xs">
                 <a href="<?php echo $base_url; ?>sign-up/login" class="dropdown-toggle" data-toggle="dropdown"><b><span class="glyphicon glyphicon-log-in"></span> <?php echo $custom_lable_arr['reg_tab_login']; ?></b></a>
                	<ul id="login-dp" class="dropdown-menu">
                    	   <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                           <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                           
						<li>	
							<div class="col-md-12">
								     <?php
									 
									if(isset($social_access_detail) && $social_access_detail!='')
									{ ?>
                                    <div class="social-buttons">
									<div class="row">
                                     <?php
										foreach($social_access_detail as $social_access)
										{
                                        
                                        if($social_access['social_name'] == 'facebook' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
								            { $facebook_access = 'Yes';?>
										<div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
											<button href="#" class="btn btn-block btn-primary facebook-bg box-shadow-radius" data-toggle="tooltip" data-placement="bottom" onClick="window.location='<?php echo $base_url; ?>sign-up/fb-signin'" title="Login via Facebook"><i class="fa fa-lg fa-facebook"></i> </button>
										</div>
                                        <?php } 
										if($social_access['social_name'] == 'google' && $social_access['client_key'] !='' && $social_access['client_secret'] != '')
										  {
											                                         
											 $app_path = dirname( __FILE__ );
											 $base_path =  substr($app_path,0,-10);
											 include ($base_path.'/gplus/gpConfig.php');											  											  $authUrl = $gClient->createAuthUrl();
											  $g_plus_accces = 'Yes';?>
                                             
										<div class="col-md-6 col-sm-6 col-xs-12 margin-top-5">
											<button href="#"  onClick="window.location='<?php echo filter_var($authUrl, FILTER_SANITIZE_URL); ?>'" class="btn btn-block google-plus-bg bx-shadow-radius" data-toggle="tooltip" data-placement="bottom" title="Login via Google Plus"><i class="fa fa-google-plus"></i></button>
										</div>
                                        <?php } }?>
									</div>
								</div>
									<div class="login-or">
										<hr class="hr-or">
										<span class="span-or small"><?php echo $custom_lable_arr['OR']; ?></span>
									</div>
                                     <?php } ?>
								 <form class="form" role="form" method="post"  accept-charset="UTF-8" id="login_form_header">
										<div class="form-group">
											<label class="sr-only" for="exampleInputEmail2"><?php echo $custom_lable_arr['login_username_placeholder'] ?></label>
											<input type="text" name="username" class="form-control" id="exampleInputEmail2" placeholder="<?php echo $custom_lable_arr['login_username_placeholder'] ?>" value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->username!='') {echo $js_login_detail->username;} ?>"  data-validation="required" style="padding:5px 0 5px 15px;border-radius:20px;font-size:14px;"  />
										</div>
										<div class="form-group">
											<label class="sr-only" for="exampleInputPassword2"><?php echo $custom_lable_arr['reg_lbl_pass'] ?></label>
											<input type="password" name="password" value="<?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='' && $js_login_detail->password!='') {echo $js_login_detail->password;} ?>" class="form-control" id="exampleInputPassword2" placeholder="<?php echo $custom_lable_arr['reg_lbl_pass'] ?>" style="padding:5px 0 5px 15px;border-radius:20px;font-size:14px;" data-validation="required"  />
											
										</div>
                                        
                                   <div class="row margin-top-5" id="google_recaptcha_header">
									<div class="col-md-12 col-xs-12" style="font-weight:normal;font-size:12px;text-transform:lowercase;">
                                    <?php
									 $rand_captcha_header = array( mt_rand(0,9), mt_rand(1, 9) );
									 $captcha_header = array('captcha_header_login' => $rand_captcha_header);
									 $this->session->set_userdata($captcha_header); 	
									 $captcha_list_header = $this->session->userdata('captcha_header_login');
									 echo $custom_lable_arr['security_text'].' '. $captcha_list_header[0]?> + <?=$captcha_list_header[1]?> ? (<?php echo $custom_lable_arr['security_question']; ?>) 
										</div>
										<div class="col-md-12 col-xs-12 group">
											 <input type="text"  data-validation="spamcheck"
          data-validation-captcha="<?=( $captcha_list_header[0] + $captcha_list_header[1] )?>" name="validate_captcha_header" class="text-filed" id="validate_captcha"><span class="highlight"></span><span class="bar"></span>
										</div>
									</div>
                                        
                                        <?php /*?>
                                        <div class="row margin-top-5" id="google_recaptcha_header">
										<div class="col-md-8 group">
											 <input type="text"  class="text-filed" id="captcha_header"  data-validation='recaptcha' placeholder="Captcha"  data-validation-error-msg='<?php echo $custom_lable_arr['captcha_msg']; ?>' style="padding:9px 0 9px 10px;" ><span class="highlight"></span><span class="bar"></span>
										</div>
									</div>
                                    <?php */ ?>
										<div class="form-group">
											<button type="submit"  name="login" class="btn btn-primary btn-block box-shadowmore-radius" style="border-radius:20px;"><span class="glyphicon glyphicon-log-in"></span> <?php echo $custom_lable_arr['login_submit_lbl']; ?></button>
										</div>
										<div class="checkbox">
											<label class="small"><input name="rememberme" type="checkbox" id="rememberme" <?php if(get_cookie('js_login_detail') && get_cookie('js_login_detail')!='') {echo "checked";} ?> value="Yes"> <?php echo $custom_lable_arr['reg_lbl_remember']; ?></label>
										</div>
										<a href="<?php echo $base_url ?>login/forgot-password" class="small-11 margin-bottom-15"><?php echo $custom_lable_arr['forgot_password_lbl'] ?></a>
										
                                    <input name="action" type="hidden"  value="login" />
                                    <input name="user_agent" type="hidden"  value="NI-WEB" /> 
                                    <input type="hidden" id="login_attepmt_value" value="<?php echo $this->session->userdata('login_attempt'); ?>">
								 </form>
							</div>
							<hr class="colorgraph margin-bottom-0">
							<div class="text-center">
								<span class="small-11"><?php echo $custom_lable_arr['new_here']; ?></span> <a href="<?php echo $base_url ?>sign-up"><strong style="color:#999;"><?php echo $custom_lable_arr['reg_now']; ?></strong></a>
							</div>
						</li>
					</ul>
                </li>-->
               <?php } ?>
			</ul>
            <?php }else{
			$user_data = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arra,1,'');
			$upload_path_profile ='./assets/js_photos';
			 ?>
            <ul class="float-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin:0 0 0 15px;">
                    <span class="user-avatar pull-left" style="margin:8px -5px;"></span>
                    <span class="user-name"><?php /* ?><img id="headerprofilediv" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $base_url.$upload_path_profile.'/'.$user_data['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" alt="profile pic" class="img-responsive img-circle headerprofilediv" style="width:35px;margin:0 auto;"/><?php */ ?> <?php  echo $user_data['fullname']; ?></span><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="navbar-content text-center">
									<button onclick="window.location.href='<?php echo $setredir = (isset($this->router->class) && strtolower(trim($this->router->class))=='my_profile') ? "javascript:void(0)" : $base_url.'my_profile'; ?>'"><img id="headermenuprofilediv" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $base_url.$upload_path_profile.'/'.$user_data['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" alt="profile pic" class="img-responsive img-circle headermenuprofilediv" style="overflow:hidden;width:65px;margin:0 auto;"/></button>
									<br>
                                    <?php if(isset($this->router->class) && strtolower(trim($this->router->class))=='my_profile') { ?>
										<button class="change_photo" onclick="gotopage('<?php echo $setpar = (isset($this->router->class) && strtolower(trim($this->router->class))=='my_profile') ? "1" : "2"; ?>')" style="padding:0px;background:none;color:gray;font-size:10px;font-weight: normal;text-transform:capitalize;">Change Photo</button>
                                    <?php } ?>
									
										<!--<button class="change_photo" onclick="window.location.href='<?php echo $base_url.'my_profile' ?>'" style="padding:0px;background:none;color:gray;font-size:10px;font-weight: normal;text-transform:capitalize;">Change Photo</button>-->
									
									<div class="divider" style="margin:5px;"></div>
									<small class="text-center"><?php  echo $user_data['fullname']; ?></small>
									<!--<br>
									<small class="text-center"><?php  echo $user_data['email']; ?></small>-->
									<div class="divider"></div>
										<button onclick="window.location.href='<?php echo $base_url.'my_profile' ?>'" class="btn btn-primary btn-sm"><i class="fa fa-user" aria-hidden="true"></i> My Profile</button>
                                    <?php /* ?>    
									<div class="margin-top-10"></div>
										<button onclick="window.location.href='change-password.html'" class="btn btn-primary btn-sm"><i class="fa fa-cogs" aria-hidden="true"></i> Setting</button>
									<div class="margin-top-10"></div>
										<button onclick="window.location.href='#'" class="btn btn-primary btn-sm"><i class="fa fa-life-ring" aria-hidden="true"></i> Help!</button>
										<?php */ ?>    
								</div>
								<div class="margin-top-10"></div>
								<div class="col-xs-12">
									<div class="navbar-footer text-center">
										<div class="navbar-footer-content">
											<button class="btn btn-warning btn-sm" onclick="window.location.href='<?php echo $base_url; ?>login/log_out'"><i class="fa fa-power-off" aria-hidden="true"></i> Sign Out</button>
										</div>
									</div>
								</div>
							</div>
                        </li>
                    </ul>
                </li>
			</ul>
            <?php } ?>
		</nav>
		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
		</div>
	</div>
</div>
</header>
<div class="clearfix"></div>

<?php 
if (strpos($_SERVER["REQUEST_URI"],'sign_up') === FALSE  && strpos($_SERVER["REQUEST_URI"],'sign-up')  === FALSE && !$this->common_front_model->checkLoginfront()){
	
?>
<script>

$('#login-dp').click(function(event){
	$('.dropdown-login').trigger('hover');
	if($(event.target).is('#exampleInputPassword2') || $(event.target).is('#validate_captcha') ) {
		
	}
	else
	{
	 $('#exampleInputEmail2').focus();
	}
	//return false;
});

$('.dropdown-login').hover(function(){
	return false;
});


$('input')
    .on('beforeValidation', function(value, lang, config) {
		$('#t_and_c').removeClass('hidden');
		var login_attepmt = $('#login_attepmt_value').val();
		//alert(login_attepmt);
		if(login_attepmt=='' || login_attepmt==null || login_attepmt==undefined || login_attepmt < 2)
		{
			$('#captcha_header').attr('data-validation-skipped','1');
		}
		else
		{
			//$('#captcha_header').css('display','block');
			$('#captcha_header').removeAttr('data-validation-skipped');
		}
	 });
	 
$(document).ready(function(){
   

	$( ".header_menu_for_active li a" ).click(function(){
	   		var clicked_menu = $(this).attr('id');
			localStorage.setItem("last_active_menu",clicked_menu);	   
	   });
  
   var active_tab = 0;
     $( ".header_menu_for_active li a" ).each(function( index ) {
  			var active_tab_class = $( this ).attr('class');
			
			if(active_tab_class == 'menu-active')
			{
				 active_tab = 1; 	
			}
	});
	
	
	if(active_tab == 0)
	{
		if(typeof(Storage) !== "undefined" && localStorage.getItem('last_active_menu')!=undefined)
		{     
			$( '#'+localStorage.getItem('last_active_menu') ).addClass('menu-active');
		}
	}
	
	
	
   	$('#google_recaptcha_header').slideUp();
	<?php
	if($this->session->userdata('login_attempt')!='' && $this->session->userdata('login_attempt') >= 2)
	{ ?>  
	 $('#google_recaptcha_header').slideDown();
<?php } ?>
});

<?php
if(!$this->session->userdata('jobportal_user') && $this->session->userdata('jobportal_user') == "")
{  ?>
$.validate({
    form : '#login_form_header',
    modules : 'security',
    reCaptchaSiteKey: '6LcyexsUAAAAABFtYaQ42upGalCg8j68YWxnAY8H',
    reCaptchaTheme: 'light',
    onError : function($form) {
	  $('#captcha_header').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('wrapper');
	  $("#error_msg").html("<?php echo $custom_lable_arr['mandatory_field_msg']; ?>");
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#login_form_header").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'Login/login' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('login_form_header').reset();
				scroll_to_div('wrapper');
				
				<?php 
				if($this->session->userdata('return_after_login_url') && $this->session->userdata('return_after_login_url')!='')
				{
					$return_after_login_url =  substr($this->session->userdata('return_after_login_url'),1);   
					$return_after_login_url = substr($return_after_login_url, strpos($return_after_login_url, "/") + 1);
					?>
					setTimeout(function(){ window.location='<?php echo $base_url.$return_after_login_url; ?>';  }, 500);
					return false;
					<?php 
				}
				?>
				setTimeout(function(){ window.location='<?php echo $base_url.'my_profile' ?>';  }, 500);
			}
			else
			{
				if(data.login_attempt >= 2)
				{
					$('#login_attepmt_value').val(data.login_attempt)
					$('#google_recaptcha_header').slideDown();
				}
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('wrapper');
			}
			
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
<?php } ?>
</script>
<?php } ?>
