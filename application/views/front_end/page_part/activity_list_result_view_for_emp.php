<?php
$custom_lable_array = $custom_lable->language;
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('js_photos',1);
if(isset($get_list) &&  $get_list!='')
{
	if($get_list=='block_emp')
	{
		$confrim_action = $custom_lable_array['confrim_action_block_emp'];
		$activity_date_lbl = $custom_lable_array['block_emp_on'];
	}
	else if($get_list=='follow_emp')
	{
		$confrim_action = $custom_lable_array['confrim_action_remove_follow_emp'];
		$activity_date_lbl = $custom_lable_array['follow_emp_on'];
	}
	else
	{
		$confrim_action = $custom_lable_array['confrim_action_remove_like_emp'];
		$activity_date_lbl = $custom_lable_array['like_emp_on'];
	}
	
}
?>
<?php

if($activity_list_count!='' && $activity_list_count > 0 )
{ ?>
<table class="manage-table responsive-table">
			<tr>
				<th><i class="fa fa-file-text"></i> <?php echo $custom_lable_array['js_name_lbl']; ?></th>
				<th><i class="fa fa-user"></i> <?php echo $custom_lable_array['js_country_lbl']; ?> </th>
				<th><i class="fa fa-user"></i><?php echo $custom_lable_array['js_role_lbl']; ?> </th>
                <th><i class="fa fa-calendar"></i><?php echo $activity_date_lbl; ?> </th>
				<th></th>
			</tr>
	<?php
		foreach($activity_list_data as $activity_list)
	    { 
		        
				$activity_date = $activity_list['liked_on'];
				if($get_list=='block_emp')
				{
					$activity_date = $activity_list['blocked_on'];
				}
				else if($get_list=='follow_emp')
				{
					$activity_date = $activity_list['followed_on'];
				}
			
		?>
        	<tr>
            	
				<td class="title">
                <?php if(isset($activity_list['is_deleted']) && $activity_list['is_deleted']=='No')
				  {?>
                <a href="javascript:;" onClick=" return view_js_details('<?php echo $activity_list['js_id']; ?>');"><?php echo ($this->common_front_model->checkfieldnotnull($activity_list['fullname'])) ? $activity_list['fullname'] : "Not Available";?><span class="pending"><!--(Pending Approval)--></span></a>
                <?php }
				else
				{?>
					 <a><?php echo ($this->common_front_model->checkfieldnotnull($activity_list['fullname'])) ? $activity_list['fullname'] : "Not Available";?><span class="pending"><!--(Pending Approval)--></span></a>
				<?php }?>
                <div class="margin-bottom-10"></div>
                 <?php if($activity_list!='' && $activity_list['profile_pic'] != ''  && $activity_list['profile_pic_approval'] == 'APPROVED' && !is_null($activity_list['profile_pic']) && file_exists($upload_path_profile_cmp.'/'.$activity_list['profile_pic']))
				{ echo '<img src="'.$base_url.'assets/js_photos/'.$activity_list['profile_pic'].'"  class="blah1" style="max-width:60%;" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"  class="blah1" style="max-width:60%;" alt="" />';} ?>
                </td>
				<td><?php echo ($activity_list['country_name']!='0' && $this->common_front_model->checkfieldnotnull($activity_list['country_name'])) ? $activity_list['country_name'] : "Not Available";?>
                
              
                </td>
				<td><?php echo ($activity_list['role_name']!='0' && $this->common_front_model->checkfieldnotnull($activity_list['role_name'])) ? $activity_list['role_name'] : "Not Available";?></td>
				<td><?php echo $this->common_front_model->displayDate($activity_date);  ?></td>
				<td class="action">
                <!--popup-with-zoom-anim-->
					<!--<a  class=" btn btn-block btn-danger btn-xs" id="js_action" onClick="return jobseeker_action('<?php //echo $action; ?>','Emp','<?php //echo $activity_list['emp_id']; ?>');" data-warning="<?php //echo $confrim_action; ?>" ><span class="glyphicon glyphicon-trash"></span> <?php //echo $custom_lable_array['delete']; ?></a>-->
                  <?php if(isset($activity_list['is_deleted']) && $activity_list['is_deleted']=='No')
				  {?>
                    <a onClick="return view_js_details('<?php echo $activity_list['js_id']; ?>');" class="btn btn-block th_bgcolor btn-xs margin-bottom-5" ><span class="glyphicon glyphicon-eye-open"></span> <?php echo $custom_lable_array['view_detail']; ?></a>
              <?php }
			  		else
					{?>
                    	<strong><p>This Job Seeker Does Not Exist</p></strong>
				<?php }?>
				</td>
			</tr>
<?php	} ?>
</table>
<div >
 <?php  echo $this->common_front_model->rander_pagination('employer_profile/my_listing',$activity_list_count); ?>
</div>
<?php }
else
{ ?>
	<div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
   </div>
<?php }
?>
<input type="hidden" id="activity_list_count" value="<?php echo $activity_list_count; ?>" />
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
