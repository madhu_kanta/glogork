<?php
$custom_lable_arr = $custom_lable->language;
if($total_post_count > 0 && $total_post_count!='')
{		
?>
<div class="five columns  margin-top-0">
			<h4 style="font-size:26px;font-family:'Poppins', sans-serif;font-weight:100;"><?php echo $custom_lable_arr['high_job_indtit']; ?></h4>
			<div class="new-back margin-top-15">
			<?php $i=0;
					if(isset($emp_posted_job) && $emp_posted_job !='' && is_array($emp_posted_job) && count($emp_posted_job) > 0)
					{
						
					foreach($emp_posted_job as $posted_job)
					{
							
						if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
						{
							$js_id = $this->common_front_model->get_userid();
							$where_arra = array('js_id'=>$js_id,'job_id'=>$posted_job['id']);
							$jobseeker_viewed_jobs = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,1);
							$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$posted_job['posted_by']);
							$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
							$check_already_apply = $this->common_front_model->get_count_data_manual('job_application_history',$where_arra,1,'','','',1);				
						}
						$i++;
						$viewed_job_count = $this->common_front_model->get_counter('viewed_job',$posted_job['id']);
		   				$liked_job_count = $this->common_front_model->get_counter('liked_job',$posted_job['id']);
						   $applied_job_count = $this->common_front_model->get_counter('applied_job',$posted_job['id']);
						  
					?>
				<div class="job-box <?php if(isset($i) && $i>1){echo 'margin-top-20';}?>">
					<div class="row">
						<div class="col-md-3 col-xs-3 col-sm-3">
						<?php
								$company_logo = '';
								if(isset($posted_job['company_logo']) && $posted_job['company_logo']!='' && isset($posted_job['company_logo_approval']) &&  $posted_job['company_logo_approval']=='APPROVED')
								{
									$company_logo = $base_url.'assets/company_logos/'.$posted_job['company_logo'];
								}
								else{
									$company_logo = $base_url.$this->common_model->default_image;
								}
								?>
							<div class="job-box-img-2">
							<img src="<?php echo $company_logo; ?>" class="job-h-img" alt="" style="height: 61px;"/>
								<hr class="apply-hr"/>
								<a href="#" class="apply-job" onClick="apply_for_job('<?php echo $posted_job['id']; ?>');" data-toggle="tooltip" title=" <?php echo $custom_lable_arr['apply']; ?>" class="btn btn-success btn-sm apply_for_job<?php echo $posted_job['id']; ?>"><i class="fa fa-paper-plane"></i> <?php echo $custom_lable_arr['apply']; ?></a>
							</div>
						</div>
						<div class="col-md-9 col-xs-9 col-sm-9">
							<p class="m-r-t"><?php echo $posted_job['job_title']; ?></p>
							<p class="m-r-t new-p-job-2">@ <?php echo ucwords(strtolower($posted_job['company_name'])); ?></p>
							<p class="time-ago">Published
							<?php echo convert_seconds($posted_job['posted_on']);?>
								ago <?php 
									echo $this->common_front_model->checkfieldnotnull($posted_job['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($posted_job['location_hiring']) : $custom_lable_arr['notavilablevar']; ?></p>
						</div>
					</div>
				</div>
				<?php 
                    }
					
					}//foreach ?>
			</div>
		</div>
		<?php 
 }
else
{
?>	
<div class="five columns">
	<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
</div>
<?php }
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />