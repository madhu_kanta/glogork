<?php 
$custom_lable_array = $custom_lable->language;
if($total_resume_count > 0 && $total_resume_count!='')
{
	/*echo "<pre>";
	print_r($resume_data);
	exit;*/
	if(isset($resume_data) && $resume_data !='' && is_array($resume_data) && count($resume_data) > 0)
	{
	foreach($resume_data as $resume_data_single)
	{  
		if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
		{
			$js_id = $this->common_front_model->get_userid();
			$where_arra = array('js_id'=>$js_id,'job_id'=>$resume_data_single['id']);
			$jobseeker_viewed_jobs = $this->common_front_model->get_count_data_manual('jobseeker_viewed_jobs',$where_arra,1);
			$where_arra_emp = array('js_id'=>$js_id);
			$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
			$check_already_apply = $this->common_front_model->get_count_data_manual('job_application_history',$where_arra,1,'','','',1);
			
		}
		
		$viewed_job_count = $this->common_front_model->get_counter('viewed_job',$resume_data_single['id']);
		$liked_job_count = $this->common_front_model->get_counter('liked_job',$resume_data_single['id']);
		$applied_job_count = $this->common_front_model->get_counter('applied_job',$resume_data_single['id']);
	?>
		<li class="highlighted box-shadow">
					<div class="margin-top-10">
						<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
							<div class="col-md-8 col-sm-8 col-xs-12">
								<h4 class="no-line-height"><a href="<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($resume_data_single['id']);?>" target="_blank" ><?php echo $resume_data_single['fullname']; ?><span class="full-time"><?php /*echo $posted_job['job_shift_type'];*/ ?></span><span class="full-time"><?php /*echo $posted_job['total_requirenment'];*/ ?></span></a></h4>
								<div class="job-icons small">
									<span><i class="fa fa-briefcase"></i> 
									<?php 
									/*if($posted_job['work_experience_from']!='' && $posted_job['work_experience_to']!='')
									{
										if($posted_job['work_experience_from']=='0' && $posted_job['work_experience_to']=='0')
										{
											echo "Fresher";
										}
										else
										{
											echo $posted_job['work_experience_from'] .' to '.$posted_job['work_experience_to']. '  Year';
										}
									}
									else
									{
										echo "N/A";
									}*/
									?> </span>
									<br/>
									<span><i class="fa fa-map"></i> <?php 
									/*echo $this->common_front_model->checkfieldnotnull($posted_job['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($posted_job['location_hiring']) : 'N/A';*/ ?> </span>
									<br/>
									<span><i class="fa fa-money"></i> <?php
									
									/*if($posted_job['job_salary_from']!='' && $posted_job['job_salary_to']!='')
									{
										
											echo $posted_job['currency_type'] .' '. $posted_job['job_salary_from'] .' to '.$posted_job['job_salary_to']. ' lacs';
									}
									else if($posted_job['job_salary_from']!='')
									{
										   echo $posted_job['currency_type'] .' '. $posted_job['job_salary_from'];
									}
									else
									{
										echo "N/A";
									}*/
									
								?> </span>
								</div>
								<!--<div class="rating-block margin-bottom-5 margin-top-0">
									<button  type="button" class="btn btn-warning btn-xs">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-warning btn-xs">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-warning btn-xs">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-grey btn-xs">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-grey btn-xs">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
								</div>-->
							</div>
	
							<div class="col-md-2 col-sm-2 col-xs-4 text-center margin-bottom-10">
								<span class="small" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-eye"></i> <?php /*echo $custom_lable_array['viewd'];*/ ?>: <strong><?php echo $viewed_job_count; ?></strong></span>
							</div>
							
                            <div class="col-md-2 col-sm-2 col-xs-4 text-center margin-bottom-10">
								<span class="small" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-heart"></i> <?php /*echo $custom_lable_arr['liked'];*/ ?>: <strong><?php /*echo $liked_job_count;*/ ?></strong></span>
							</div>
                            
							<div class="col-md-2 col-sm-2 col-xs-4 text-center margin-bottom-10">
								<span class="small" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-check"></i> <?php /*echo $custom_lable_array['applied'];*/ ?>: <strong><?php /*echo $applied_job_count;*/ ?></strong></span>
							</div>
							<!--<div class="col-md-2 col-sm-2 col-xs-4 text-center margin-bottom-10">
								<span class="small" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-angle-double-right"></i><?php /*echo $custom_lable_array['similar_jobs'];*/ ?>:</span>
							</div>-->
	
							<hr class="hr">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="skills">
									<?php  
									//echo $posted_job['skill_keyword'];
									/*$skill_keyword = explode(',',$posted_job['skill_keyword']);
									foreach($skill_keyword as $skill)
									{
										echo '<span>'.$skill.'</span>';
									}*/
									?>
                                    
								</div>
								<div class="clearfix"></div>
	
								<p class="small"><?php /*echo substr(htmlspecialchars_decode($posted_job['job_description'],ENT_QUOTES),0,150)*/ ?>...</p>
	
								<div class="margin-bottom-15">
									<span class="small bg-job-view" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-eye"></i> <?php /*echo $custom_lable_array['viewd'];*/ ?>: <strong><?php /*echo $this->common_front_model->checkfieldnotnull($posted_job['no_of_views']) ? $posted_job['no_of_views'] : 'N/A';*/ ?></strong></span>
	
									<span class="small bg-job-view" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-check"></i> <?php /*echo $custom_lable_array['applied'];*/ ?>: <strong><?php /*echo $this->common_front_model->checkfieldnotnull($posted_job['no_of_apply']) ? $posted_job['no_of_apply'] : 'N/A';*/ ?></strong></span>
	
									<span class="small bg-job-view" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;cursor:pointer;"  onClick="window.open('<?php echo $base_url; ?>job-listing/view-job-details/<?php /*echo base64_encode($posted_job['id']);*/?>','_blank');" ><i class="fa fa-plus"></i> &nbsp; <?php /*echo $custom_lable_array['more_details'];*/ ?> </span>
	
    						<?php
							$company_logo = '';
							/*if(isset($posted_job['company_logo']) && $posted_job['company_logo']!='')
							{
								$company_logo = $base_url.'assets/company_logos/'.$posted_job['company_logo'];
							}*/
							?>
									<span class="margin-bottom-5 margin-top-5 text-center">
										<span class="small"> &nbsp; <?php echo $custom_lable_array['share']; ?>:</span>&nbsp;
										<button class="btn btn-twitter btn-sm" data-toggle="tooltip"  title="<?php echo $custom_lable_array['share_twitter']; ?>" onclick="window.open('http://twitter.com/home?status=<?php /*echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']));*/ ?>')"><i class="fa fa-twitter"></i></button>
	
										<button class="btn btn-danger btn-sm" rel="publisher" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_gplus']; ?>" onclick="window.open('https://plus.google.com/share?url=<?php /*echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']));*/ ?>')"><i class="fa fa-google-plus"></i></button>
	
										<button class="btn btn-facebook btn-sm" rel="publisher"  data-toggle="tooltip" title="<?php echo $custom_lable_array['share_facebook']; ?>" onClick=         "window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php /*echo urlencode($posted_job['job_title']);*/ ?>&amp;p[url]=<?php /*echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']));*/ ?>&amp;&p[images][0]=<?php /*echo urlencode($company_logo);*/ ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"><i class="fa fa-facebook"></i></button>
										<button class="btn btn-sm btn-info" rel="linkedin"  data-toggle="tooltip" title="<?php echo $custom_lable_array['share_linkedin']; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  /*echo $base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']);*/ ?>&title=<?php /*echo urlencode($posted_job['job_title']);*/ ?>&summary=<?php /*echo urlencode($posted_job['job_title']); ?>&source=<?php echo urlencode($posted_job['job_title']);*/ ?>')"><i class="fa fa-linkedin"></i></button>
                                        
                                        <button class="btn btn-danger btn-sm" rel="pinterest"  data-toggle="tooltip" title="<?php echo $custom_lable_array['share_pinterest']; ?>" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php /*echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($posted_job['id']));*/ ?>	&media=<?php /*echo urlencode($company_logo);*/ ?>&description=<?php /*echo urlencode($posted_job['job_title']);*/ ?>')"><i class="fa fa-pinterest-square"></i></button>
									</span>
								</div>
							</div>
						</div>
					</div>
                   
					<hr class="hr">
					<div class="row margin-top-10 margin-bottom-0">
						<div class="col-md-6 col-sm-6 col-xs-12 text-center small">
							<span class="col-md-5 col-sm-5 col-xs-12"><i class="fa fa-briefcase"></i>  
							<?php 
							/*if($posted_job['work_experience_from']!='' && $posted_job['work_experience_to']!='')
									{
										if($posted_job['work_experience_from']=='0' && $posted_job['work_experience_to']=='0')
										{
											echo "Fresher";
										}
										else
										{
											echo $posted_job['work_experience_from'] .' to '.$posted_job['work_experience_to']. '  Year';
										}
									}
									else
									{
										echo "N/A";
									}*/
							?></span>
							<span class="col-md-7 col-sm-7 col-xs-12"><i class="fa fa-map-marker"></i> <?php 
									/*echo $this->common_front_model->checkfieldnotnull($posted_job['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($posted_job['location_hiring']) : 'N/A';*/ ?> </span>
						</div>
						<?php
						if(!$this->common_front_model->checkLoginfrontempl() && $this->common_front_model->checkLoginfrontempl()==false )
						{ 
							$save_action = 'save_job';
							$save_text = $custom_lable_array['save_job'];
							$save_icon = '<i class="fa fa-lg fa-floppy-o" aria-hidden="true"></i>';
							$block_action = 'block_emp';
							$block_icon = '<i class="fa fa-lg fa-ban" aria-hidden="true"></i> ';
							$block_text = $custom_lable_array['block'];
							if(isset($jobseeker_viewed_jobs) && $jobseeker_viewed_jobs!='' && is_array($jobseeker_viewed_jobs) && count($jobseeker_viewed_jobs) > 0)
							{
								if($jobseeker_viewed_jobs['is_saved']=='Yes')
								{
									$save_text = $custom_lable_array['saved'];
									$save_icon = "<i class='fa fa-check'></i>";
									$save_action = 'remove_save_job';
								}
							}
							if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) && count($jobseeker_access_employer) > 0)
							{
								if($jobseeker_access_employer['is_block']=='Yes')
								{
									$block_action = 'unblock_emp';
									$block_icon = '<span class="fa fa-check"></span>';
									$block_text = $custom_lable_array['blocked'];
								} 
							}
						?>
                        <div class="row col-md-6 col-sm-6 col-xs-12 margin-bottom-10" style="text-align:right;">
							<!--<button type="button" data-toggle="tooltip" title=" <?php echo $custom_lable_array['apply']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-lg fa-star-o"></i> </button>-->
							<?php 
							if(isset($check_already_apply) && $check_already_apply!='' && is_array($check_already_apply) && count($check_already_apply) > 0)
							{ ?>
                            
							<button type="button" style="cursor:default;"	 data-toggle="tooltip" title=" <?php echo $custom_lable_array['already_apply']; ?>" class="btn btn-success btn-sm "><i class="fa fa-check" aria-hidden="true"></i> <?php echo $custom_lable_array['already_apply']; ?></button>
							<?php } 
							else
							{ ?>
                            <button type="button" onClick="apply_for_job(<?php /*echo $posted_job['id'];*/ ?>);" data-toggle="tooltip" title=" <?php echo $custom_lable_array['apply']; ?>" class="btn btn-success btn-sm apply_for_job<?php /*echo $posted_job['id'];*/ ?>"><i class="fa fa-lg fa-paper-plane" aria-hidden="true"></i> <?php echo $custom_lable_array['apply']; ?></button>
						<?php } ?>
                            
                            <button type="button" onClick="return jobseeker_action('<?php echo $save_action; ?>','job','<?php /*echo $posted_job['id'];*/ ?>');" data-toggle="tooltip" title=" <?php echo $save_text ; ?>" class="btn btn-info btn-sm save_job_button<?php /*echo $posted_job['id'];*/ ?>" save_job_button><?php echo $save_icon; ?> <?php echo $save_text ; ?></button> 
							
                            <button type="button" data-toggle="tooltip" title="<?php echo $block_text; ?>" OnClick="return jobseeker_action('<?php echo $block_action; ?>','Emp','<?php /*echo $posted_job['posted_by'];*/ ?>');" class="btn btn-warning btn-sm emp_block_action<?php /*echo $posted_job['posted_by'];*/ ?>"><?php echo $block_icon ; ?><?php echo $block_text; ?></button>
						</div>
                       <?php }
					   else
					   {
						   /*if($posted_job['currently_hiring_status']=='Yes')
						   {
							   $class = 'label label-success';
							   $lable = $custom_lable_array['job_open_status'];
							   $icon = 'fa fa-folder-open';
						   }
						   else
						   {
							   $class = 'label label-danger';
							   $lable = $custom_lable_array['job_closed_status'];
							    $icon = 'fa fa-times-circle';
						   }
						   
						   if($posted_job['job_highlighted']=='Yes')
						   {
							   $class_highlight = 'label label-success';
							   $lable_highlight = $custom_lable_array['job_highlighted'];
							   $icon_highlight = '<i class="fa fa-check" aria-hidden="true"></i>
';
							    $onclick_fn = '';
								$style = 'style="height:30px;"';
						   }
						   else
						   {
							   $class_highlight = 'btn btn-info btn-sm';
							   $lable_highlight = $custom_lable_array['add_to_highlighted'];
							   $icon_highlight = '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
';
							   $onclick_fn = 'onClick="add_to_highlight('.$posted_job['id'].')";'; 
							   $style = '';
							}*/
							/*
						   ?>
                           <div class="row col-md-6 col-sm-6 col-xs-12 margin-bottom-10" style="text-align:right;">
							<button type="button" data-toggle="tooltip" title="<?php echo $custom_lable_array['edit']; ?>" class="btn btn-success btn-sm" target="_blank"  onClick="window.location='<?php echo $base_url;?>job-listing/edit-posted-job/<?php echo base64_encode($posted_job['id']); ?>'"><i class="fa fa-pencil-square" aria-hidden="true"></i> <?php echo  $custom_lable_array['edit']; ?></button>
							<button type="button" style="height:30px;" data-toggle="tooltip" title="<?php echo $custom_lable_array['currently_job_status']; ?>" class="<?php echo $class; ?>"><i class="<?php echo $icon; ?>" aria-hidden="true"></i> <?php echo $lable;; ?></button>
							<button <?php echo $style; ?> type="button"  <?php echo $onclick_fn; ?> data-toggle="tooltip" title="<?php echo  $lable_highlight; ?>" class="<?php echo $class_highlight; ?>  add_to_highlight_class<?php echo $posted_job['id']; ?>">  <?php echo $icon_highlight; ?> <?php echo $lable_highlight; ?></button>
						</div>
                           <?php
						   */
					   }
					    ?>
						
					</div>
					<div class="clearfix"></div>
				</li>
<?php 
	}
	}

if(isset($get_page_access_name) && $get_page_access_name=='suggested_job')
{
	echo $this->common_front_model->rander_pagination('job_listing/suggested_job',$total_resume_count); 
}
else
{
	echo $this->common_front_model->rander_pagination('job_listing/posted_job',$total_resume_count); 
}
}
else
{
	?>
    <div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
   </div>
    <?php
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" id="search_from_id" value="job_search_option" /> 
<script>
$('#total_job_count').html('<?php echo $total_resume_count; ?>');

</script> 

			

			
		