<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/right-new.css?ver=1.0">
<?php
$custom_lable_array = $custom_lable->language;
$location_home_search = '';
if($this->input->post('location_home_search') && $this->input->post('location_home_search')!='')
{
	$location_home_search = $this->input->post('location_home_search');
}
/* for loaction search from location page*/
if($this->input->post('search_location'))
{
	$search_location_loc = $this->input->post('search_location');
	if(isset($search_location_loc) && !empty($search_location_loc) && is_array($search_location_loc) && count($search_location_loc) > 0)
	{
		$location_home_search = implode(',',$search_location_loc);
	}
}						 
?>
<form method="post" id="job_search_option" onChange="setTimeout(function(){ job_option_search(); }, 100);">
<div>
	<div class="five columns new-back">
        <div class="panel-group" id="accordion">
			<div class="panel panel-default" style="border-radius:0px;box-shadow:none;border-color:white;">
			  <div class="panel-heading panel-bg" style="padding: 5px 2px 6px 18px;border-radius:0px;">
				<h4 class="panel-title"> <span class="glyphicon glyphicon-map-marker icon-g"></span> 
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrow('collapseOne')"><?php echo $custom_lable_array['search_by_location']; ?> </a>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrow('collapseOne')"><i class="indicator glyphicon glyphicon-chevron-up pull-right" style="margin:10px; 10px;" id='icon_collapseOne'></i></a>
                </h4>
			  </div>
			  <div id="collapseOne" class="panel-collapse collapse in">
				<div class="panel-body box-search-new">
					<!--<div class="widget" style="margin-bottom:0px;">-->
						
                        <select id="search_location"  name="search_location[]" class="select2" data-placeholder="<?php echo $custom_lable_array['search_by_location']; ?>" style="width:100%;" multiple>
                        <?php 
						  if(isset($location_home_search) && $location_home_search!='')
						  {
							  $location_home_searchs = str_replace(',',"','",$location_home_search);
							  $location_home_search = explode(",",$location_home_search);
							  $search_data_array = array();
							  $where = '';
							  $i = 0;
							  if($location_home_search  !='' && is_array($location_home_search ) && count($location_home_search ) > 0)
							  {
								  foreach($location_home_search as $search_data)
									{
										if(is_numeric(trim($search_data)))
										{
											$search_data_array[] = trim($search_data);
											
										}
										else
										{ 
										  if($i>0)
											$where .= ' OR ';
										  $where .= "city_name='".trim($search_data)."'";
										  $i++;
										}
									}
						  		}
								if(isset($search_data_array) && is_array($search_data_array) && count($search_data_array)>0)
								{
									$search_data_array = implode(",",$search_data_array);
									$search_data_array = str_replace(",","','",$search_data_array);
									$search_data_array = "'".$search_data_array."'";
									if($where!='')
										$where .= " OR ";
									$where .= "id IN ($search_data_array)";
								}
								if($where!='')
									$where = "($where) AND is_deleted='No'";
								else
									$where = "is_deleted='No'";
						
							  //$where = "id IN ('".$location_home_searchs."')";
							  $city_data = $this->common_front_model->get_count_data_manual('city_master',$where,2);
							  if(isset($city_data) && $city_data !='' && is_array($city_data) && count($city_data) > 0)
							  {
								  foreach($city_data as $city)
								  {
								  ?>
									<option value="<?php echo $city['id']; ?>" selected="selected"><?php echo $city['city_name']; ?></option>
								  <?php 
								  }
							  }
						  }
						  ?>
                        </select>
							<!--<input type="text" placeholder="State / Province" value="" style="padding:9px 0 9px 10px;"/>
							<input type="text" placeholder="City" value="" style="padding:9px 0 9px 10px;"/>
							<input type="text" class="miles" placeholder="Miles" value="" style="padding:9px 0 9px 10px;"/>
							<label for="zip-code" class="from" style="padding:9px 0 9px 10px;">from</label>
							<input type="text" id="zip-code" class="zip-code" placeholder="Zip-Code" value="" style="padding:9px 0 9px 10px;"/>
							<button class="button" style="padding: 3px 10px 3px 10px;"><span class="glyphicon glyphicon-filter"></span> Filter</button>-->
						
					<!--</div>-->
				</div>
			  </div>
			</div>
			<div class="margin-top-10"></div>
			<div class="panel panel-default" style="border-radius:0px;box-shadow:none;border-color:white;">
			  <div class="panel-heading panel-bg" style="padding: 5px 2px 6px 18px;border-radius:0px;">
				<h4 class="panel-title"> <span class="glyphicon glyphicon-bookmark icon-g"></span> 
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" onclick="change_arrow('collapseTwo')"> <?php echo $custom_lable_array['by_skill']; ?> </a>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" onclick="change_arrow('collapseTwo')"><i class="indicator glyphicon glyphicon-chevron-up pull-right" style="margin:10px; 10px;" id='icon_collapseTwo'></i></a></h4>
			  </div>
			  <div id="collapseTwo" class="panel-collapse collapse in">
				<div class="panel-body box-search-new">
					<h5><?php echo $custom_lable_array['sort_by']; ?></h5>
					<select name="search_sort_by_posted" id="sort_by_posted" data-placeholder="<?php echo $custom_lable_array['sort_by']; ?>" class="form-control">
						<option value=""><?php echo $custom_lable_array['select_option']; ?></option>
                        <option value="30"> <?php echo $custom_lable_array['last_30_days']; ?> </option>
						<option value="15"> <?php echo $custom_lable_array['last_15_days']; ?> </option>
						<option value="7"> <?php echo $custom_lable_array['last_7_days']; ?></option>
						<option value="1"> <?php echo $custom_lable_array['last_1_days']; ?> </option>
					</select>

					
					<h5><?php echo $custom_lable_array['skill']; ?></h5>
					
						<select data-placeholder="<?php echo $custom_lable_array['skill']; ?>" name="search_skill[]" id="search_skill" class="select2" multiple style="width:100%;">
							<?php /*?><?php $skill_list =  $this->common_front_model->get_list('key_skill_master','','','array','',''); 
							foreach($skill_list as $skill)
							{ ?>
								<option value="<?php echo $skill['val']; ?>"><?php echo $skill['val']; ?></option>
							<?php }
							?><?php */?>
						</select>
					
                    
                    <h5><?php echo $custom_lable_array['job_education']; ?></h5>
					    <select data-placeholder="<?php echo $custom_lable_array['job_education']; ?>" name="job_edu[]" id="job_edu" class="chosen-select" multiple>
						<?php echo  $this->common_front_model->get_list('edu_list','','','str','',''); ?>
						</select>
					
					
					<!--<button class="button" style="padding: 3px 10px 3px 10px;"><span class="glyphicon glyphicon-filter"></span> Filter</button>-->
				</div>
			  </div>
			</div>
			<div class="margin-top-10"></div>
			<div class="panel panel-default" style="border-radius:0px;box-shadow:none;border-color:white;">
			  <div class="panel-heading panel-bg" style="padding: 5px 2px 6px 18px;border-radius:0px;">
				<h4 class="panel-title">  <span class="glyphicon glyphicon-briefcase icon-g"></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrow('collapseThree')"> <?php echo $custom_lable_array['extra_search']; ?> <!--<span class="glyphicon glyphicon-usd"></span>--></a>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"  onclick="change_arrow('collapseThree')"><i class="indicator glyphicon glyphicon-chevron-up pull-right" style="margin:10px; 10px;" id='icon_collapseThree'></i></a></h4>
			  </div>
				<div id="collapseThree" class="panel-collapse collapse in">
					<div class="panel-body box-search-new">
						
                        <?php /* search fn area hidden field */
							if($this->input->post('search_fn_area'))
							  {
									$search_fn_area_fn = $this->input->post('search_fn_area');
							  }
							  else
							  {
								  $search_fn_area_fn = array();
							  }
							  if(isset($search_fn_area_fn)  && is_array($search_fn_area_fn) && count($search_fn_area_fn) > 0 && !empty($search_fn_area_fn)){
								  foreach($search_fn_area_fn as $search_fn_area_fn_single)
                                {
						 ?>
                         		<input type="hidden" name="search_fn_area[]" value="<?php echo $search_fn_area_fn_single; ?>">
                         <?php } } /* search fn area hidden field end */  ?>
                         
									
								
                        <h5><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_array['industry']; ?></h5>
                        <?php if($this->input->post('search_industry'))
							  {
									$search_industry_ind = $this->input->post('search_industry');
							  }
							  else
							  {
								  $search_industry_ind = array();
							  }
						 ?>
						<select name="search_industry[]" id="search_industry" class="chosen-select" multiple>
							<?php $industries_master =   $this->common_front_model->get_list('industries_master','','','array','',''); 
							if(isset($industries_master) && $industries_master !='' && is_array($industries_master) && count($industries_master) > 0)
							{
							foreach($industries_master as $ind_list)
							{ ?>
								<option value="<?php echo $ind_list['id']; ?>" <?php if(is_array($search_industry_ind) && in_array($ind_list['id'],$search_industry_ind)){ echo "selected"; } ?>><?php echo $ind_list['val']; ?></option>
					  <?php }
					  		} ?>
						</select>
						
                        <?php if($this->input->post('search_company'))
							  {
									$search_company_cmp = $this->input->post('search_company');
							  }
							  else
							  {
								  $search_company_cmp = array();
							  }	
						 ?>
						<h5><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_array['search_company_lbl']; ?></h5>
						<select name="search_company[]" id="search_company" class="select2" multiple data-placeholder="<?php echo $custom_lable_array['search_company_lbl']; ?>">
							<?php if(isset($search_company_cmp)  && is_array($search_company_cmp) && count($search_company_cmp) > 0 && !empty($search_company_cmp)){   ?>
								<?php 
                                foreach($search_company_cmp as $search_company_cmp_single)
                                { ?>
									<option value="<?php echo $search_company_cmp_single; ?>" selected ><?php echo $search_company_cmp_single; ?></option>
								<?php } ?>
                            	<?php } ?>
						</select>
						
						<h5><!--<span class="glyphicon glyphicon-briefcase"></span>--> <?php echo $custom_lable_array['job_type']; ?></h5>
						<select name="search_job_type[]" id="search_job_type" class="chosen-select" multiple >
							<?php $job_type_list =  $this->common_front_model->get_list('job_type_list','','','array','',''); 
							if(isset($job_type_list) && $job_type_list !='' && is_array($job_type_list) && count($job_type_list) > 0)
							{
								foreach($job_type_list as $j_type_list)
								{
							?>
								<option value="<?php echo $j_type_list['id']; ?>"><?php echo $j_type_list['val']; ?></option>
							<?php }
							}
							?>
							
						</select>

						
						<h5> <?php echo $custom_lable_array['salary']; ?> </h5>
						<select name="search_salary" id="search_salary"  style="    padding: 10px 12px;
								border-radius: 3px;" >
							<option value=""><?php echo $custom_lable_array['salary_search']; ?></option>
						<?php $salary_range_list =  $this->common_front_model->get_list('salary_range_list','','','array','',''); 
							if(isset($salary_range_list) && $salary_range_list !='' && is_array($salary_range_list) && count($salary_range_list) > 0)
							{
								foreach($salary_range_list as $salary_r_list)
								{
						?>
								<option value="<?php echo $salary_r_list['id']; ?>"><?php echo $salary_r_list['val']; ?></option>
						<?php 	}
							}
							?>
							
						</select>
						<?php /*?><select name="search_salary" id="search_salary" class="form-control">
							<option value=""><?php echo $custom_lable_array['salary_search']; ?></option>
                            <option value="0/2"> 0-2 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="2/4"> 2-4 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="4/8"> 4-8 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="8/15"> 8-15 <?php echo $custom_lable_array['lacs']; ?></option>
                        </select><?php */?>
					</div>
				</div>
			</div>
			
    </div>
			<div class="margin-top-10"></div>
			<?php 
		$advert_data = $this->common_front_model->getadvertisement('Level 1'); 
			if(isset($advert_data) && $advert_data !='' && is_array($advert_data) && count($advert_data) > 0)
				{ ?>
				<div class="panel panel-default" style="border-radius:0px;box-shadow:none;border-color:white;">
				<?php 
			
					if($advert_data['type']=='Image')
					{
			?>
            	<div class="margin-top-10">
							
            	<a href="<?php echo $advert_data['link']; ?>" target="_blank"><img src="<?php echo $base_url; ?>assets/advertisement/<?php echo $advert_data['image']; ?>" alt="Ad" /></a>
            <?php 	}elseif($advert_data['type']=='Google')
					{ echo $advert_data['google_adsense']; ?>
            
            <?php 	}
			 ?>
			
				</div>
				<?php } ?>
		</div>
	</div>

</form>

