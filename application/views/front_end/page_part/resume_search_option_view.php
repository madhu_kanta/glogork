<?php
$custom_lable_array = $custom_lable->language;
$location_home_search = '';
if($this->input->post('location_home_search') && $this->input->post('location_home_search')!='')
{
	$location_home_search = $this->input->post('location_home_search');
    //$location_home_search = explode(',',$this->input->post('location_home_search'));
}
/* for loaction search from location page*/
if($this->input->post('search_location'))
{
	$search_location_loc = $this->input->post('search_location');
	if(isset($search_location_loc)  && is_array($search_location_loc) && count($search_location_loc) > 0 && !empty($search_location_loc))
	{
		$location_home_search = implode(',',$search_location_loc);
	}
		
}
/* for loaction search from location page*/

?>
<form method="post" id="resume_search_option" onChange="setTimeout(function(){ resume_option_search(); }, 100);" name="resume_search_option">
<div class="five columns">
        <div class="panel-group" id="accordion">
			<div class="panel panel-default" style="border-radius:0px;">
			  <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
				<h4 class="panel-title"> <span class="glyphicon glyphicon-map-marker"></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowres('collapseOne')"><?php echo $custom_lable_array['search_by_location']; ?> </a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="change_arrowres('collapseOne')"><i id='icon_collapseOne' class="indicator glyphicon glyphicon-chevron-down pull-right" style="margin:10px; 10px;"></i></a></h4>
			  </div>
			  <div id="collapseOne" class="panel-collapse collapse in">
				<div class="panel-body">
					<!--<div class="widget" style="margin-bottom:0px;">-->
						
                        <select id="search_location"  name="search_location[]" class="select2" data-placeholder="<?php echo $custom_lable_array['search_by_location']; ?>" style="width:100%;" multiple>
                        <?php 
						  if(isset($location_home_search) && $location_home_search!='')
						  {
							  $where = "id IN (".$location_home_search.")";
							  $city_data = $this->common_front_model->get_count_data_manual('city_master',$where,2);
							 // echo $this->db->last_query();
							 if(isset($city_data) && is_array($city_data) && count($city_data) > 0)
						  	{
							  foreach($city_data as $city)
							  { ?>
                              	<option value="<?php echo $city['id']; ?>" selected><?php echo $city['city_name']; ?></option>
							  <?php }
							}
						  }
						  ?>
                        </select>
							<!--<input type="text" placeholder="State / Province" value="" style="padding:9px 0 9px 10px;"/>
							<input type="text" placeholder="City" value="" style="padding:9px 0 9px 10px;"/>
							<input type="text" class="miles" placeholder="Miles" value="" style="padding:9px 0 9px 10px;"/>
							<label for="zip-code" class="from" style="padding:9px 0 9px 10px;">from</label>
							<input type="text" id="zip-code" class="zip-code" placeholder="Zip-Code" value="" style="padding:9px 0 9px 10px;"/>
							<button class="button" style="padding: 3px 10px 3px 10px;"><span class="glyphicon glyphicon-filter"></span> Filter</button>-->
						
					<!--</div>-->
				</div>
			  </div>
			</div>
			<div class="panel panel-default" style="border-radius:0px;">
			  <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
				<h4 class="panel-title"> <span class="glyphicon glyphicon-bookmark"></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" onclick="change_arrowres('collapseTwo')"> <?php echo $custom_lable_array['by_skill']; ?>  </a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" onclick="change_arrowres('collapseTwo')"><i id='icon_collapseTwo' class="indicator glyphicon glyphicon-chevron-down pull-right" style="margin:10px; 10px;"></i></a></h4>
			  </div>
			  <div id="collapseTwo" class="panel-collapse collapse in">
				<div class="panel-body">
                	<?php /* ?>
					<h5><?php echo $custom_lable_array['sort_by']; ?></h5>
					<select name="search_sort_by_posted" id="sort_by_posted" data-placeholder="<?php echo $custom_lable_array['sort_by']; ?>" class="chosen-select">
						<option value=""><?php echo $custom_lable_array['select_option']; ?></option>
                        <option value="30"> <?php echo $custom_lable_array['last_30_days']; ?> </option>
						<option value="15"> <?php echo $custom_lable_array['last_15_days']; ?> </option>
						<option value="7"> <?php echo $custom_lable_array['last_7_days']; ?></option>
						<option value="1"> <?php echo $custom_lable_array['last_1_days']; ?> </option>
					</select>
					<?php */ ?>
					<div class="margin-top-15"></div>
					<h5><?php echo $custom_lable_array['skill']; ?></h5>
					
						<select data-placeholder="<?php echo $custom_lable_array['skill']; ?>" name="search_skill[]" id="search_skill" class="select2" multiple style="width:100%;">
							<?php $skill_list =  $this->common_front_model->get_list('key_skill_master','','','array','',''); 
							if(isset($skill_list) && is_array($skill_list) && count($skill_list) > 0)
							{
							foreach($skill_list as $skill)
							{ ?>
								<option value="<?php echo $skill['val']; ?>"><?php echo $skill['val']; ?></option>
							<?php }
							}
							?>
						</select>
					<div class="margin-top-15"></div>
					<div class="margin-top-15"></div>
                    <?php /* ?>
                    <h5><?php echo $custom_lable_array['job_education']; ?></h5>
					    <select data-placeholder="<?php echo $custom_lable_array['job_education']; ?>" name="job_edu[]" id="job_edu" class="chosen-select" multiple>
						<?php echo  $this->common_front_model->get_list('edu_list','','','str','',''); ?>
						</select>
						<?php */?>
					<div class="margin-top-15"></div>
					
					<!--<button class="button" style="padding: 3px 10px 3px 10px;"><span class="glyphicon glyphicon-filter"></span> Filter</button>-->
				</div>
			  </div>
			</div>
			<div class="panel panel-default" style="border-radius:0px;">
			  <div class="panel-heading panel-bg" style="padding: 5px 0 0 18px;border-radius:0px;">
				<h4 class="panel-title">  <span class="glyphicon glyphicon-briefcase"></span> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowres('collapseThree')"> <?php echo $custom_lable_array['extra_search']; ?> <!--<span class="glyphicon glyphicon-usd"></span>--></a><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" onclick="change_arrowres('collapseThree')"><i id='icon_collapseThree' class="indicator glyphicon glyphicon-chevron-down pull-right" style="margin:10px; 10px;"></i></a></h4>
			  </div>
				<div id="collapseThree" class="panel-collapse collapse in">
					<div class="panel-body">
						
                        <?php /* search fn area hidden field */
							if($this->input->post('search_fn_area'))
							  {
									$search_fn_area_fn = $this->input->post('search_fn_area');
							  }
							  else
							  {
								  $search_fn_area_fn = array();
							  }
							  if(isset($search_fn_area_fn) && !empty($search_fn_area_fn) && is_array($search_fn_area_fn) && count($search_fn_area_fn) > 0){
								  foreach($search_fn_area_fn as $search_fn_area_fn_single)
                                {
						 ?>
                         		<input type="hidden" name="search_fn_area[]" value="<?php echo $search_fn_area_fn_single; ?>">
                         <?php } } /* search fn area hidden field end */  ?>
                         
									
								
                        <h5><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_array['industry']; ?></h5>
                        <?php if($this->input->post('search_industry'))
							  {
									$search_industry_ind = $this->input->post('search_industry');
							  }
							  else
							  {
								  $search_industry_ind = array();
							  }
						 ?>
						<select name="search_industry[]" id="search_industry" class="chosen-select" multiple>
							<?php $industries_master =   $this->common_front_model->get_list('industries_master','','','array','',''); 
							if(isset($industries_master) && $industries_master !='' && is_array($industries_master) && count($industries_master) > 0)
							{
							foreach($industries_master as $ind_list)
							{ ?>
								<option value="<?php echo $ind_list['id']; ?>" <?php if(is_array($search_industry_ind) && in_array($ind_list['id'],$search_industry_ind)){ echo "selected"; } ?>><?php echo $ind_list['val']; ?></option>
					  <?php }
					  } ?>
						</select>
						<div class="margin-top-15"></div>
                        <?php if($this->input->post('search_company'))
							  {
									$search_company_cmp = $this->input->post('search_company');
							  }
							  else
							  {
								  $search_company_cmp = array();
							  }	
						 ?>
                         <?php /* ?>
						<h5><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_array['search_company_lbl']; ?></h5>
						<select name="search_company[]" id="search_company" class="select2" multiple data-placeholder="<?php echo $custom_lable_array['search_company_lbl']; ?>">
							<?php if(isset($search_company_cmp)  && is_array($search_company_cmp) && count($search_company_cmp) > 0 && !empty($search_company_cmp)){   ?>
								<?php 
                                foreach($search_company_cmp as $search_company_cmp_single)
                                { ?>
									<option value="<?php echo $search_company_cmp_single; ?>" selected ><?php echo $search_company_cmp_single; ?></option>
								<?php } ?>
                            	<?php } ?>
						</select>
						<div class="margin-top-15"></div>
                        <?php */ ?>
                        <?php /* ?>
						<h5><!--<span class="glyphicon glyphicon-briefcase"></span>--> <?php echo $custom_lable_array['job_type']; ?></h5>
						<select name="search_job_type[]" id="search_job_type" class="chosen-select" multiple >
							<?php $job_type_list =  $this->common_front_model->get_list('job_type_list','','','array','',''); 
							
							foreach($job_type_list as $j_type_list)
							{ ?>
								<option value="<?php echo $j_type_list['id']; ?>"><?php echo $j_type_list['val']; ?></option>
							<?php }
							?>
							
						</select>
						<?php */ ?>
                        <?php /* ?>
						<div class="margin-top-15"></div>
						<h5> <?php echo $custom_lable_array['salary']; ?> </h5>
						<select name="search_salary" id="search_salary" class="chosen-select">
							<option value=""><?php echo $custom_lable_array['salary_search']; ?></option>
                            <option value="0/2"> 0-2 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="2/4"> 2-4 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="4/8"> 4-8 <?php echo $custom_lable_array['lacs']; ?></option>
                            <option value="8/15"> 8-15 <?php echo $custom_lable_array['lacs']; ?></option>
                        </select>
                        <?php */ ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
var change_arrow_process = 0 ;
var y = ['collapseOne','collapseTwo','collapseThree','collapsefour'];
function change_arrowres(id)
{	
	if(change_arrow_process == 0)
	{
		setTimeout(function(){
		change_arrow_process = 1;		
		$.each( y, function( key, value )
		{
			var className = $('#icon_'+value).attr('class');
			var idName = $('#icon_'+value).attr('id');
			if ( $('#'+value).hasClass("collapse in"))
			{				
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-down pull-right');
			}
			else
			{
				$('#'+idName).attr('class','indicator glyphicon glyphicon-chevron-up pull-right');				
			}
		});
		change_arrow_process = 0;
		},400);
	}	
}
</script>