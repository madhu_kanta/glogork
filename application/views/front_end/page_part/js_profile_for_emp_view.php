<?php
$custom_lable_arr = $this->lang->language;
$user_data = $user_data['js_data'] ;
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('js_photos',1);
?>
<?php if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
{ 
	$emp_id_stored_plan = $this->common_front_model->get_empid();
	$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','contacts');
} 
if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
{
	if(isset($user_data['mobile']) && $user_data['mobile'] !='')
	{
		$user_data['mobile'] = $this->common_model->mobile_disp;
	}
	if(isset($user_data['email']) && $user_data['email'] !='')
	{
		$user_data['email'] = $this->common_model->emial_disp;
	}
}
?>
<style>
foreditfontcolor
{
	color:#ffffff;
}
</style>
<div class="col-md-12 col-sm-12 col-xs-12 tab-pane active" id="profile_snapshot_viewdiv">
							<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;"><!--#D9534F-->
								<div class="panel-heading panel-bg media80" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_arr['js_basic_detail']; ?> :</span> <a href="javascript:void(0);" onClick="close_js_details();" class="btn btn-md btn-danger pull-right foreditfontcolor media3" style="margin:-3px;" ><i class="fa fa-close" aria-hidden="true" ></i> <?php echo $custom_lable_arr['close']; ?></a></div>
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
												<table class="table">
													<tbody>
														
                                                        
                                                        <tr>
                                                        <td>
                                                            <?php if($user_data!='' && $user_data['profile_pic'] != ''  && $user_data['profile_pic_approval'] == 'APPROVED' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile_cmp.'/'.$user_data['profile_pic']))
				{ echo '<img src="'.$base_url.'assets/js_photos/'.$user_data['profile_pic'].'"  class="blah1" style="max-width:60%;" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"  class="blah1" style="max-width:60%;" alt="" />'; } ?>
                                                            </td>
                                                        </tr>
														
                                                        <tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['reg_lbl_fullname']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ? $user_data['personal_titles'].' '.$user_data['fullname'] : "Not Available" ; ?>
                                                            </td>
                                                            
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_profile_text_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['profile_summary']!='0' && $this->common_front_model->checkfieldnotnull($user_data['profile_summary'])) ? $user_data['profile_summary'] : "Not Available";?></td>
														</tr>
														<?php 
														if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
														{
															if($return_data_plan_emp == 'Yes')
															{
																 ?>                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_email_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['email']!='0' && $this->common_front_model->checkfieldnotnull($user_data['email'])) ? $user_data['email'] : "Not Available";?></td>
														</tr>
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_mobile_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['mobile']!='0' && $this->common_front_model->checkfieldnotnull($user_data['mobile'])) ? $user_data['mobile'] : "Not Available";?></td>
														</tr>
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_address_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['address']!='0' && $this->common_front_model->checkfieldnotnull($user_data['address'])) ? $user_data['address'] : "Not Available";?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_country_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['country_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['country_name'])) ? $user_data['country_name'] : "Not Available";?></td>
														</tr>
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_city_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['city_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['city_name'])) ? $user_data['city_name'] : "Not Available";?></td>
														</tr>
                                                        <?php 
														if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
														{
															if($return_data_plan_emp == 'Yes')
															{
																 ?>
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_website_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['website']!='0' && $this->common_front_model->checkfieldnotnull($user_data['website'])) ? $user_data['website'] : "Not Available";?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
													</tbody>
												</table>
											</div>
									</div>
								</div>
							</div>
						</div>
                        
<div class="col-md-12 col-sm-12 col-xs-12 tab-pane active" id="profile_snapshot_viewdiv">
							<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_arr['js_job_lbl']; ?> :</span> <a href="javascript:void(0);" onClick="close_js_details();" class="btn btn-md btn-danger pull-right foreditfontcolor " style="margin:-3px;" ><i class="fa fa-close" aria-hidden="true" ></i> <?php echo $custom_lable_arr['close']; ?></a></div>
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
												<table class="table">
													<tbody>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['js_resume_headline_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['resume_headline'])) ? $user_data['resume_headline'] : "Not Available" ; ?>
                                                            </td>
														</tr>
														
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_industry_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['industries_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['industries_name'])) ? $user_data['industries_name'] : "Not Available";?></td>
														</tr>
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_functional_name_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['functional_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['functional_name'])) ? $user_data['functional_name'] : "Not Available";?></td>
														</tr>
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_role_name_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['role_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['role_name'])) ? $user_data['role_name'] : "Not Available";?></td>
														</tr>
                                                         
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_work_exp_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? $this->common_front_model->display_exp_salary($user_data['total_experience'],'experience') : "Not Available";?></td>
														</tr>
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_annual_salary_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6">
															<?php 
															$salary_range = $this->my_profile_model->getdetailsfromid('salary_range','id',$user_data['annual_salary'],'salary_range');
															echo ($user_data['annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($user_data['annual_salary']) && count($salary_range) > 0 && $salary_range['salary_range']!='' ) ? $salary_range['salary_range'] :  $custom_lable_arr['notavilablevar']; 
															
															//echo ( $this->common_front_model->checkfieldnotnull($user_data['annual_salary'])) ? $user_data['currency_type'] .' '.  $this->common_front_model->display_exp_salary($user_data['annual_salary']) : "Not Available";
															?></td>
														</tr>
                                                        
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['js_exp_salary_lbl']; ?> :</td>
															<td class="col-md-6 col-xs-6">
															<?php 
															$salary_range = $this->my_profile_model->getdetailsfromid('salary_range','id',$user_data['expected_annual_salary'],'salary_range');
															echo ($user_data['expected_annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary']) && count($salary_range) > 0 && $salary_range['salary_range']!='' ) ? $salary_range['salary_range'] :  $custom_lable_arr['notavilablevar']; 
															
															//echo ( $this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary'])) ? $user_data['exp_salary_currency_type'] .' '. $this->common_front_model->display_exp_salary($user_data['expected_annual_salary']) : "Not Available";
															
															?></td>
														</tr>
                                                        
                                                        
                                                        
													</tbody>
												</table>
											</div>
									</div>
								</div>
							</div>
						</div>                        
<?php 
if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
{
	if($return_data_plan_emp == 'Yes')
	{
		$this->common_front_model->check_for_plan_update($emp_id_stored_plan,'employer',$user_data['id']);
	}//if plan is valid
} ?>