<?php 
$custom_lable_array = $custom_lable->language;
$upload_path_profile ='./assets/js_photos';
$profile_pic = '';
if($total_resume_count > 0 && $total_resume_count!='')
{  ?>
<ul class="resumes-list">
	<?php 
	if(isset($resume_data) && $resume_data !='' && is_array($resume_data) && count($resume_data) > 0)
	{
	foreach($resume_data as $resume_data_single)
	{ 
	$profile_pic = ($resume_data_single['profile_pic_approval'] == 'APPROVED' && $resume_data_single['profile_pic'] != '' && !is_null($resume_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$resume_data_single['profile_pic'])) ? $base_url.'assets/js_photos/'.$resume_data_single['profile_pic'] : '' ;
	?>
	<li class="highlighted box-shadow">
					<div class="margin-top-10">
						<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
							<div class="col-md-2 col-sm-2 col-xs-12 text-center">
								<img src="<?php if($resume_data_single['profile_pic_approval'] == 'APPROVED' && $resume_data_single['profile_pic'] != '' && !is_null($resume_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$resume_data_single['profile_pic'])){ echo $base_url.'assets/js_photos/'.$resume_data_single['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/img_avatar1.png<?php } ?>" class="img-responsive text-center" alt="img_avatar1" />
							</div>
							<div class="col-md-8 col-sm-8 col-xs-12 margin">
								<h4 class="no-line-height"><a href="<?php echo $base_url; ?>share_profile/js-profile/<?php echo base64_encode($resume_data_single['id']);?>" target="_blank" ><?php echo $resume_data_single['fullname']; ?></a></h4>
								<span class="small-15 line-height-25 margin-top-0"><i class="fa fa-briefcase"></i> <?php echo ($resume_data_single['industry']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['industry']) && $this->common_front_model->checkfieldnotnull($resume_data_single['industries_name'])) ? $resume_data_single['industries_name'] :  $custom_lable_array['notavilablevar']; ?> 
                                (Exp. <?php 
							     $totalexp = ($this->common_front_model->checkfieldnotnull($resume_data_single['total_experience'])) ? explode("-",$resume_data_single['total_experience']) : "";
								 //print_r($totalexp);
								// echo '</br>';
								//echo $resume_data_single['total_experience'];
														if($totalexp!='')
														{
															if(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0'){ echo $totalexp[0].' '.$custom_lable_array['year'] ; } elseif(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]=='0') 
															{
																//echo "";
																echo  $custom_lable_array['notavilablevar'];
															}
															if(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!='0' ){ echo $totalexp[1].' '.$custom_lable_array['month'] ; }  elseif(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[0]=='0') { echo "Fresher" ; }
														}
														elseif($totalexp=='00-00' || $totalexp=='0-0')
														{
															echo "Fresher";
														}
														else
														{
															echo  $custom_lable_array['notavilablevar'];
														}
														
														
														?>)
                                </span>
								<div class="job-icons small">
									<span class=""><i class="fa fa-map"></i> <?php echo ($resume_data_single['city']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['city']) && $this->common_front_model->checkfieldnotnull($resume_data_single['city_name'])) ? $resume_data_single['city_name'] : $custom_lable_array['notavilablevar']; ?>,</span>
									<span><i class="fa fa-money"></i> 

								<?php echo ($resume_data_single['expected_annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['expected_annual_salary']) && $this->common_front_model->checkfieldnotnull($resume_data_single['ex_annual_salary_name'])) ? $resume_data_single['ex_annual_salary_name'] :  $custom_lable_array['notavilablevar']; ?> 
									
									<?php /*$annual_salary_exp = ($this->common_front_model->checkfieldnotnull($resume_data_single['expected_annual_salary'])) ? explode("-",$resume_data_single['expected_annual_salary']) : "";
													if($annual_salary_exp!='' && (isset($annual_salary_exp[0]) && $annual_salary_exp[0]!='' && $annual_salary_exp[0]!='0') || (isset($annual_salary_exp[1]) && $annual_salary_exp[1]!='' && $annual_salary_exp[1]!=0)) 
													{
														if($this->common_front_model->checkfieldnotnull($resume_data_single['exp_salary_currency_type'])) { echo  $resume_data_single['exp_salary_currency_type'] ; } 
														if($this->common_front_model->checkfieldnotnull($annual_salary_exp) && isset($annual_salary_exp[0]) && $annual_salary_exp[0]!='' ){ echo " ".$annual_salary_exp[0].' '.$custom_lable_array['lacs'] ; }
														if($this->common_front_model->checkfieldnotnull($annual_salary_exp) && isset($annual_salary_exp[1]) && $annual_salary_exp[1]!='' ){ echo " ".$annual_salary_exp[1].' '.$custom_lable_array['thousand'] ; }
													}
													else
													{
														echo  $custom_lable_array['notavilablevar'];
													}*/
														 ?></span>
								</div>
							</div>
							<hr class="hr">
							<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="small margin-top-15 margin-bottom-0"><u>Skills</u>:</p>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-12 nopadding">
									<div class="skills">
                                    <?php $skill_keyword = ($this->common_front_model->checkfieldnotnull($resume_data_single['key_skill'])) ? $resume_data_single['key_skill'] : '' ;
									if($skill_keyword != '')
									{
										$skill_keyword_arr = explode(',',$skill_keyword);
										foreach($skill_keyword_arr as $skill)
										{
											echo '<span>'.$skill.'</span>';
										}	
									}
									else
									{
										echo $custom_lable_array['notavilablevar'];
									}
									
										
									 ?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="small margin-top-10 margin-bottom-0"><u>About jobseeker</u>:</p>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-12">
									<p class="small margin-bottom-10 margin-top-10"><?php if($this->common_front_model->checkfieldnotnull($resume_data_single['profile_summary'])) { 
									if(strlen($resume_data_single['profile_summary']) > 1000)
									{
										echo  substr(htmlspecialchars_decode($resume_data_single['profile_summary'],ENT_QUOTES),0,1000).' .....'; 
									}
									else
									{
										echo $resume_data_single['profile_summary'];
									}
									
					}else{ 
									$custom_lable_array['notavilablevar']; 
					} ?></p>
                                    <!-- more details section -->
									
                                    <!-- more details section end  -->
								</div>
							</div>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                            	<!-- more details section -->
                                	<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="margin-top-10 margin-bottom-10" onClick="window.open('<?php echo $base_url; ?>share_profile/js-profile/<?php echo base64_encode($resume_data_single['id']);?>','_blank');" style="cursor:pointer;">
										<span class="small bg-job-view" style="border:1px solid #e0da94;padding:5px 5px;border-radius:2px;"><i class="fa fa-plus"></i> More Details</span>
										
									</div>
                                    </div>
                                    <!-- more details section end  -->
                            </div>
						</div>
					</div>
					<hr class="hr">
					<div class="row margin-top-10 margin-bottom-0">
						<div class="col-md-6 col-sm-6 col-xs-12 small">
							<div class="margin-bottom-5 margin-top-0" style="padding-left:15px;">
								<button class="btn btn-twitter btn-circle btn-sm" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_twitter']; ?>"  onclick="window.open('http://twitter.com/home?status=<?php echo urlencode($base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id'])); ?>')"><i class="fa fa-twitter"></i></button>
								<button class="btn btn-danger btn-circle btn-sm" rel="publisher" data-toggle="tooltip" title="<?php echo $custom_lable_array['share_gplus']; ?>" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode($base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id']))?>')"><i class="fa fa-google-plus"></i></button>
								<button class="btn btn-facebook btn-circle btn-sm" rel="publisher"  data-toggle="tooltip" title="<?php echo $custom_lable_array['share_facebook']; ?>"  onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo urlencode($resume_data_single['fullname']); ?>&amp;p[url]=<?php echo urlencode($base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($profile_pic); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"><i class="fa fa-facebook"></i></button>
								<button class="btn btn-sm btn-circle btn-info" rel="linkedin"  data-toggle="tooltip" title="<?php echo $custom_lable_array['share_linkedin']; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  echo $base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id']); ?>&title=<?php echo urlencode($resume_data_single['fullname']); ?>&summary=<?php echo urlencode($resume_data_single['profile_summary']); ?>&source=<?php echo urlencode($resume_data_single['fullname']); ?>')"><i class="fa fa-linkedin"></i></button>
							</div>
						</div>
						<div class="row col-md-6 col-sm-6 col-xs-12 margin-bottom-10" style="text-align:right;">
                        <?php  if($this->common_front_model->checkLoginfrontempl() && $this->common_front_model->get_empid()!='') 
				 	 			{ 
									$my_message_add = $base_url.'my-message'; 
								?>	
                                <button href="#small-dialog" data-sender_id="<?php echo $this->common_front_model->get_empid(); ?>" data-receiver_id="<?php echo $resume_data_single['id']; ?>" data-email="<?php echo $resume_data_single['email']; ?>" onClick="return show_modelpp('send_message<?php echo $resume_data_single['id']; ?>');" class="popup-with-zoom-anim  btn btn-success btn-sm send_message<?php echo $resume_data_single['id']; ?>"><i class="fa fa-lg fa-envelope"></i></button>
                                <!--btn btn-default--><!--<i class="fa fa-envelope"></i> --><?php /*echo $custom_lable_array['message'];*/ ?>
                                <!--<button onClick="window.open('<?php echo $my_message_add; ?>','_blank');" style="cursor:pointer;" data-toggle="tooltip" title="Send Message" class="btn btn-success btn-sm"><i class="fa fa-lg fa-envelope"></i> </button>-->
								<?php }
								else
								{ 
									$my_message_add = $base_url.'login-employer'; ?>
									<button onClick="window.open('<?php echo $my_message_add; ?>','_blank');" style="cursor:pointer;" data-toggle="tooltip" title="Send Message" class="btn btn-success btn-sm"><i class="fa fa-lg fa-envelope"></i> </button><!--onclick="window.location.href='#small-dialog'"-->
								<?php } ?>
						</div>
					</div>
					<div class="clearfix"></div>
				</li>
    <?php } 
	}
	?>
</ul>
<div class="clearfix"></div>
<?php 
echo $this->common_front_model->rander_pagination('browse-resume/index',$total_resume_count,$limit_per_page);
}
else
{
?>
    <div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
   </div>
<?php
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" id="search_from_id" value="resume_search_option" /> 
<script>
$('#total_job_count').html('<?php echo $total_resume_count; ?>');

</script>