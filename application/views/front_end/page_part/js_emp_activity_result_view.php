<?php
$custom_lable_array = $custom_lable->language;
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('company_logos',1);
if(isset($get_list) &&  $get_list!='')
{
	if($get_list=='block_emp')
	{
		$confrim_action = $custom_lable_array['confrim_action_block_emp'];
		$activity_date_lbl = $custom_lable_array['block_emp_on'];
	}
	else if($get_list=='follow_emp')
	{
		$confrim_action = $custom_lable_array['confrim_action_remove_follow_emp'];
		$activity_date_lbl = $custom_lable_array['follow_emp_on'];
	}
	else
	{
		$confrim_action = $custom_lable_array['confrim_action_remove_like_emp'];
		$activity_date_lbl = $custom_lable_array['like_emp_on'];
	}
	
}
?>
<?php
if($activity_list_count > 0 && $activity_list_count!='')
{ ?>
<table class="manage-table responsive-table">
			<tr>
				<th><i class="fa fa-file-text"></i> <?php echo $custom_lable_array['emp_name_lbl']; ?></th>
				<th><i class="fa fa-user"></i> <?php echo $custom_lable_array['sign_up_emp_companydet']; ?> </th>
				<th><i class="fa fa-user"></i><?php echo $custom_lable_array['myprofile_emp_cmpweblbl']; ?> </th>
                <th><i class="fa fa-calendar"></i><?php echo $activity_date_lbl; ?> </th>
				<th></th>
			</tr>
	<?php
	foreach($activity_list_data as $activity_list)
	    { 
		        $action_text = $custom_lable_array['blocked'];
				$activity_date = $activity_list['liked_on'];
				$action = 'unlike_emp';
				if($get_list=='block_emp')
				{
					$action_text = $custom_lable_array['blocked'];
					$action = 'unblock_emp';
					$activity_date = $activity_list['blocked_on'];
				}
				else if($get_list=='follow_emp')
				{
					$action = 'unfollow_emp';
					$action_text = $custom_lable_array['following'];
					$activity_date = $activity_list['followed_on'];
				}
			
		?>
        	<tr>
				<td class="title"><a href="javascript:;" onClick=" return view_emp_details('<?php echo $activity_list['emp_id']; ?>');"><?php echo ($this->common_front_model->checkfieldnotnull($activity_list['fullname'])) ? $activity_list['fullname'] : "Not Available";?> <!--<span class="pending">(Pending Approval)</span>--></a></td>
				<td><?php echo ($activity_list['company_name']!='0' && $this->common_front_model->checkfieldnotnull($activity_list['company_name'])) ? $activity_list['company_name'] : "Not Available";?>
                <div class="margin-bottom-10"></div>
               <?php if($activity_list!='' && $activity_list['company_logo'] != '' && !is_null($activity_list['company_logo']) && file_exists($upload_path_profile_cmp.'/'.$activity_list['company_logo']))
				{ echo '<img src="'.$base_url.'assets/company_logos/'.$activity_list['company_logo'].'"  class="blah1" style="max-width:60%;" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"  class="blah1" style="max-width:60%;" alt="" />';} ?>
                </td>
				<td><?php echo ($activity_list['company_website']!='0' && $this->common_front_model->checkfieldnotnull($activity_list['company_website'])) ? $activity_list['company_website'] : "Not Available";?></td>
				<td><?php echo $this->common_front_model->displayDate($activity_date);  ?></td>
				<td class="action">
					<?php if($activity_list['is_deleted']=='No')
                    { ?>
                        <!--popup-with-zoom-anim-->
                            <a  class=" btn btn-block btn-danger btn-xs" id="js_action" onClick="return jobseeker_action('<?php echo $action; ?>','Emp','<?php echo $activity_list['emp_id']; ?>');" data-warning="<?php echo $confrim_action; ?>" ><span class="glyphicon glyphicon-trash"></span> <?php echo $custom_lable_array['delete']; ?></a>
                            
                            <a onClick="return view_emp_details('<?php echo $activity_list['emp_id']; ?>');" class="btn btn-block th_bgcolor btn-xs margin-bottom-5" ><span class="glyphicon glyphicon-eye-open"></span> <?php echo $custom_lable_array['view_detail']; ?></a>
                     <?php }
					 else
					 {?>
                     	<strong><p>This Employer Does Not Exist</p></strong>
                     <?php }?>
				</td>
			</tr>
<?php	} ?>
</table>
<div >
 <?php  echo $this->common_front_model->rander_pagination('job_seeker_action/js_activity_list_emp',$activity_list_count); ?>
</div>
<?php }
else
{ ?>
	<div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
   </div>
<?php }
?>
<input type="hidden" id="activity_list_count" value="<?php echo $activity_list_count; ?>" />
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
