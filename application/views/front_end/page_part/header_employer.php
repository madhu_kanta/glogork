<?php $custom_lable_arr = $custom_lable->language;
	  $config_data = $this->common_front_model->data['config_data'];
	  if($this->common_front_model->checkLoginfrontempl())
	  {
		  $emp_id = $this->session->userdata('jobportal_employer');
		  $where_arra = array('id'=>$emp_id['employer_id'],'status'=>'APPROVED','is_deleted'=>'No');
		  $check_if_active = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,0,'','','','','');
		  if(!($check_if_active > 0))
		  {
				$this->session->unset_userdata('jobportal_employer');
		  }
	  }
	  else
	  {
		  if(isset($config_data['website_title']) && $config_data['website_title'] !='')
		  {
				$page_title = $config_data['website_title'];
		  }
	  }
	  
$active_url = (isset( $_SERVER['https'] ) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$active_url_emp_tab1 =  $base_url.'login-employer';
$active_url_emp_tab2 =  $base_url.'sign-up-employer';
	  
 ?>
<!DOCTYPE html>
<html lang="en">
<!--  xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" -->
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php if(isset($config_data['upload_favicon']) && $config_data['upload_favicon'] !='' && file_exists('./assets/logo/'.$config_data['upload_favicon'])) { ?>
 <link rel="shortcut icon" href="<?php echo $base_url.'assets/logo/'.$config_data['upload_favicon']; ?>" />
<?php } ?>

<meta name="description" content="<?php if(isset($config_data['website_description']) && $config_data['website_description'] !=''){ echo $config_data['website_description'];} ?>" />
<meta name="keywords" content="<?php if(isset($config_data['website_keywords']) && $config_data['website_keywords'] !=''){ echo $config_data['website_keywords'];} ?>" />
    
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/bootstrap3.3.7.css?ver=1.0">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/style.css?ver=1.0">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/colors/custom.css?ver=1.0" id="colors">
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-3.3.7.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/common.js?k=999"></script>

<style>
.navbar-content small
{
	word-wrap: break-word;
}
.menu-active
{
	background-color: #ff3434 !important;
	color: white !important;
}
</style>
</head>
<body>
<div id="wrapper"> <!--This wrapper will be end in the footer page  -->
<header class="sticky-header">
<div class="container">
	<div class="sixteen columns">
		<!-- Logo -->
		<div id="logo">
			<h1><a href="<?php echo $base_url; ?>">
								<?php  if(isset($config_data['upload_logo']) && $config_data['upload_logo'] !='' && file_exists('./assets/logo/'.$config_data['upload_logo']))
                                {  ?>
                					<img src="<?php echo $base_url.'assets/logo/'.$config_data['upload_logo']; ?>" alt="<?php echo $config_data['web_frienly_name']; ?>"/> 
<!--class="img-responsive"-->
                                <?php }else { ?>
                                	<img src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $config_data['web_frienly_name'] ;?>" class="img-responsive" />
                                <?php } ?></a></h1>
		</div>

		<!-- Menu -->
		<nav id="navigation" class="menu">
			<ul id="responsive" class="header_menu_for_active">
	            <li><a id="home-active" <?php if($active_url == $base_url ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>"><?php echo $custom_lable_arr['header_menu_home_txt'];?></a></li>
				<!--<li><a href="#">Pages</a>
					<ul>
						<li><a href="job-page.html">Job Page</a></li>
						<li><a href="job-page-alt.html">Job Page Alternative</a></li>
						<li><a href="resume-page.html">Resume Page</a></li>
						<li><a href="shortcodes.html">Shortcodes</a></li>
						<li><a href="icons.html">Icons</a></li>
						<li><a href="pricing-tables.html">Pricing Tables</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</li>-->
                <?php if(!$this->common_front_model->checkLoginfrontempl()){ ?>
                <li><a id="posted-job-active" <?php if($active_url == $base_url.'job-listing/posted-job' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>job-listing/posted-job">Browse Jobs</a></li>
                <li><a id="recruiters-active" <?php if($active_url == $base_url.'recruiters' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>recruiters">Recruiters</a></li>
                <li><a id="recruiters-companies-active" <?php if($active_url == $base_url.'recruiters/companies' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>recruiters/companies">Companies</a></li>
				<li id="emp_tab_for_active"><a <?php if($active_url == $base_url.'login-employer' || $active_url == $base_url.'sign-up-employer'  ) {echo "class='menu-active '  ";} ?> href="#" ><?php echo $custom_lable_arr['header_menu_foemp'];?></a>
					<ul>
						<li><a href="<?php echo $base_url; ?>login-employer"><?php echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>
						<li><a href="<?php echo $base_url; ?>sign-up-employer"><?php echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
					</ul>
				</li>
				<?php }else{
					$session_data = $this->session->userdata('jobportal_emplyoer');
				 ?>
                 <li><a  <?php if($active_url == $base_url.'browse-resume' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>browse-resume">JobSeekers</a></li>
                 <li><a <?php if($active_url == $base_url.'job-listing' || $active_url == $base_url.'job-listing/posted-job' ||  $active_url == $base_url.'job-application/manage-job-application'|| $active_url == $base_url.'job-application/download-cv') {echo "class='menu-active'  ";} ?> href="#">Jobs</a>
					<ul>
                    	<li><a href="<?php echo $base_url; ?>job-listing">Post Jobs</a></li>
						<li><a href="<?php echo $base_url; ?>job-listing/posted-job">View Posted Jobs</a></li>
                        <li><a href="<?php echo $base_url; ?>job-application/manage-job-application">Jobs Responses</a></li>
                        <li><a href="<?php echo $base_url; ?>job-application/download-cv">Download CV</a></li>
                      <!-- <li><a href="<?php //echo $base_url; ?>share-profile/emp-profile/<?php //echo base64_encode(11); ?>"> CV</a></li>-->
					</ul>
				</li>
                 <li><a id="employer_profile_tab_active"  <?php if($active_url == $base_url.'employer_profile' ) {echo "class='menu-active'  ";} ?>  href="<?php echo $base_url; ?>employer_profile"><?php echo $custom_lable_arr['header_menu_foemp_myprofile'];?></a></li>
                 <li><a id="my_message_tab_active" <?php if($active_url == $base_url.'my-message' ) {echo "class='menu-active'  ";} ?> href="<?php echo $base_url; ?>my-message">Message</a></li>
                <li><a <?php if($active_url == $base_url.'my-plan/current-plan' || $active_url == $base_url.'my-plan/index' ||  $active_url == $base_url.'my-plan/buy-now'  ) {echo "class='menu-active'" ;} ?> href="#">Upgrade Plan</a>
					<ul>
						<li><a href="<?php echo $base_url; ?>my-plan/current-plan">Current Plan<?php //echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>
						<li><a href="<?php echo $base_url; ?>my-plan/index">Membership Plan<?php //echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
					</ul>
				</li>
                <?php } ?>
				<!--<li><a href="<?php echo $base_url; ?>blog/allblogs"><?php echo $custom_lable_arr['footer_menu_blog_txt'];?></a></li>-->
			</ul>
			<?php if(!$this->common_front_model->checkLoginfrontempl()){ ?>
			<ul class="responsive float-right">
				<!--<li><a href="<?php //echo $base_url; ?>sign-up-employer"><i class="fa fa-user"></i> <?php //echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
				<li><a href="<?php //echo $base_url; ?>login-employer"><i class="fa fa-lock"></i> <?php //echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>-->
                 <li><a href="<?php echo $base_url; ?>sign-up"><i class="fa fa-user"></i> <?php echo $custom_lable_arr['header_menu_foemp_reg'];?></a></li>
				<li><a href="<?php echo $base_url; ?>sign-up/login"><i class="fa fa-lock"></i> <?php echo $custom_lable_arr['header_menu_foemp_login'];?></a></li>
			</ul>
            <?php }else{ 
			$session_data = $this->session->userdata('jobportal_employer');
			$where_arra = array('id'=>$session_data['employer_id'],'status'=>'APPROVED','is_deleted'=>'No');
			$user_data = $this->common_front_model->get_count_data_manual('employer_master_view',$where_arra,1,'');
			$upload_path_profile ='./assets/emp_photos';
			$upload_path_profile_or = $base_url.'assets/emp_photos'; 
			 ?>
            <ul class="float-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin:0 0 0 15px;">
                    <span class="user-avatar pull-left" style="margin:8px -5px;"></span>
                    <span class="user-name"><?php /* ?><img id="headerprofilediv" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $upload_path_profile_or.'/'.$user_data['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" alt="profile pic" class="img-responsive img-circle headerprofilediv" style="width:35px;margin:0 auto;"/><?php */ ?> <?php  echo $user_data['fullname']; ?></span><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="navbar-content text-center">
									<button onclick="window.location.href='<?php echo $setredir = (isset($this->router->class) && strtolower(trim($this->router->class))=='employer_profile') ? "javascript:void(0)" : $base_url.'employer_profile'; ?>'"><img id="headermenuprofilediv" src="<?php if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $upload_path_profile_or.'/'.$user_data['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/demoprofileimge.png<?php } ?>" alt="profile pic" class="img-responsive img-circle headermenuprofilediv" style="overflow:hidden;width:65px;margin:0 auto;"/></button>
									<br>
                                    
										<button class="change_photo" onclick="gotopage('<?php echo $setpar = (isset($this->router->class) && strtolower(trim($this->router->class))=='employer_profile') ? "1" : "2"; ?>')" style="padding:0px;background:none;color:gray;font-size:10px;font-weight: normal;text-transform:capitalize;">Change Photo</button>
									
										<!--<button class="change_photo" onclick="window.location.href='<?php echo $base_url.'employer_profile' ?>'" style="padding:0px;background:none;color:gray;font-size:10px;font-weight: normal;text-transform:capitalize;">Change Photo</button>-->
									
									<div class="divider" style="margin:5px;"></div>
									<small style="cursor:pointer" onclick="window.location.href='<?php echo $base_url.'employer_profile' ?>'" class="text-center"><?php  echo $user_data['fullname']; ?></small>
									<br>
									<!--<small class="text-center"><?php  echo $user_data['email']; ?></small>-->
									<div class="divider"></div>
										<!--<button onclick="window.location.href='<?php echo $base_url.'employer_profile' ?>'" class="btn btn-primary btn-sm"><i class="fa fa-user" aria-hidden="true"></i> Edit Profile</button>-->
                                    <?php /* ?>    
									<div class="margin-top-10"></div>
										<button onclick="window.location.href='change-password.html'" class="btn btn-primary btn-sm"><i class="fa fa-cogs" aria-hidden="true"></i> Setting</button>
									<div class="margin-top-10"></div>
										<button onclick="window.location.href='#'" class="btn btn-primary btn-sm"><i class="fa fa-life-ring" aria-hidden="true"></i> Help!</button>
										<?php */ ?>    
								</div>
								<div class="margin-top-10"></div>
								<div class="col-xs-12">
									<div class="navbar-footer text-center">
										<div class="navbar-footer-content">
											<button class="btn btn-warning btn-sm" onclick="window.location.href='<?php echo $base_url; ?>login-employer/log_out'"><i class="fa fa-power-off" aria-hidden="true"></i> Sign Out</button>
										</div>
									</div>
								</div>
							</div>
                        </li>
                    </ul>
                </li>
			</ul>
            <?php } ?>
		</nav>
		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
		</div>
	</div>
</div>
</header>
<div class="clearfix"></div>

<script>

$(document).ready(function(){

 
$( ".header_menu_for_active li a" ).click(function(){
	   		var clicked_menu = $(this).attr('id');
			localStorage.setItem("last_active_menu",clicked_menu);	   
	   });
  
   var active_tab = 0;
     $( ".header_menu_for_active li a" ).each(function( index ) {
  			var active_tab_class = $( this ).attr('class');
			
			if(active_tab_class == 'menu-active')
			{
				 active_tab = 1; 	
			}
	});
	
	
	if(active_tab == 0)
	{
		if(typeof(Storage) !== "undefined" && localStorage.getItem('last_active_menu')!=undefined)
		{     
			$( '#'+localStorage.getItem('last_active_menu') ).addClass('menu-active');
		}
	}
});
</script>