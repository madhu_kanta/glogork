<style>
.new-check-box-modal {
    float: left;
    width: auto;
    margin-right: 11px;
}
.row {
    margin-bottom: 2px;
}
</style>
<?php
$custom_lable_array = $custom_lable->language;
$custom_lable_arr = $custom_lable->language;
$upload_path_profile ='./assets/js_photos';
$profile_pic = '';
if($total_resume_count > 0 && $total_resume_count!='')
{  ?>
<ul class="resumes-list">
	<?php
	if(isset($resume_data) && $resume_data !='' && is_array($resume_data) && count($resume_data) > 0)
	{
	foreach($resume_data as $resume_data_single)
	{
	$profile_pic = ($resume_data_single['profile_pic_approval'] == 'APPROVED' && $resume_data_single['profile_pic'] != '' && !is_null($resume_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$resume_data_single['profile_pic'])) ? $base_url.'assets/js_photos/'.$resume_data_single['profile_pic'] : '' ;
	?>
					<div class="box-main <?php if(isset($j) && $j>1){echo 'margin-top-25';}else{ echo'margin-top-20';}?>">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12 text-center">
								<img src="<?php if($resume_data_single['profile_pic_approval'] == 'APPROVED' && $resume_data_single['profile_pic'] != '' && !is_null($resume_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$resume_data_single['profile_pic'])){ echo $base_url.'assets/js_photos/'.$resume_data_single['profile_pic'];  }else{?><?php echo $base_url; ?>assets/front_end/images/img_avatar1.png<?php } ?>" class="img-responsive text-center" alt="img_avatar1" />
							</div>
							<div class="col-md-8 col-xs-12 col-sm-8 new-margin-col">
                            <a href="<?php echo $base_url; ?>share_profile/js-profile/<?php echo base64_encode($resume_data_single['id']);?>"  target="_blank" ><h2 class="box-main-h2"><?php echo $resume_data_single['fullname']; ?> </h2></a>
								<div class="box-share margin-top-15">
									<span> </span>
									<span class="share"><img src="<?php echo $base_url;?>assets/front_end/images/share.png" alt=""> <?php echo $custom_lable_arr['share']; ?> : </span>
									<?php
								$company_logo = '';
								if(isset($resume_data_single['company_logo']) && $resume_data_single['company_logo']!='')
								{
									$company_logo = $base_url.'assets/company_logos/'.$resume_data_single['company_logo'];
								}
								?>
									<div class="social-iocn-5">
										<div class="social-icons-2">
											<div><a href="" class="social-icon-2" rel="publisher"   onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($profile_pic); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"> <i class="fa fa-facebook"></i></a></div>

											<div><a href="" class="social-icon-2" rel="linkedin"   onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  echo $base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id']); ?>&title=<?php echo urlencode($resume_data_single['fullname']); ?>&summary=<?php echo urlencode($resume_data_single['profile_summary']); ?>&source=<?php echo urlencode($resume_data_single['fullname']); ?>')"> <i class="fa fa-linkedin"></i></a></div>
											<div><a href="" class="social-icon-2"  onclick="window.open('http://twitter.com/home?status=<?php echo urlencode($base_url.'share_profile/js-profile/'.base64_encode($resume_data_single['id'])); ?>')"> <i class="fa fa-twitter"></i></a></div>
											<!-- <div><a href="" class="social-icon-2" rel="pinterest"   onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php //echo urlencode($base_url.'job-listing/view-job-details/'.base64_encode($resume_data_single['id'])); ?>	&media=<?php// echo urlencode($company_logo); ?>&description=<?php //echo urlencode($resume_data_single['job_title']); ?>')"> <i class="fa fa-pinterest"></i></a></div> -->

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-xs-12 col-sm-5 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/job-1.png" alt=""></span><span class="job-d"><?php echo ($resume_data_single['industry']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['industry']) && $this->common_front_model->checkfieldnotnull($resume_data_single['industries_name'])) ? $resume_data_single['industries_name'] :  $custom_lable_array['notavilablevar']; ?>
                                (Exp. <?php
							     $totalexp = ($this->common_front_model->checkfieldnotnull($resume_data_single['total_experience'])) ? explode("-",$resume_data_single['total_experience']) : "";
								 //print_r($totalexp);
								// echo '</br>';
								//echo $resume_data_single['total_experience'];
														if($totalexp!='')
														{
															if(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0'){ echo $totalexp[0].' '.$custom_lable_array['year'] ; } elseif(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]=='0')
															{
																//echo "";
																echo  $custom_lable_array['notavilablevar'];
															}
															if(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!='0' ){ echo $totalexp[1].' '.$custom_lable_array['month'] ; }  elseif(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[0]=='0') { echo "Fresher" ; }
														}
														elseif($totalexp=='00-00' || $totalexp=='0-0')
														{
															echo "Fresher";
														}
														else
														{
															echo  $custom_lable_array['notavilablevar'];
														}


														?>)</span></p>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-7 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/point.png" alt=""></span><span class="job-d">Location : <?php echo ($resume_data_single['city']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['city']) && $this->common_front_model->checkfieldnotnull($resume_data_single['city_name'])) ? $resume_data_single['city_name'] : $custom_lable_array['notavilablevar']; ?></span></p>
									</div>
								</div>
								<div class="row">

									<div class="col-md-7 col-xs-12 col-sm-7 new-p-job">
										<p class="img-job"><span><img src="<?php echo $base_url;?>assets/front_end/images/payment-method.png" alt=""></span><span class="job-d"> <?php echo ($resume_data_single['expected_annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($resume_data_single['expected_annual_salary']) && $this->common_front_model->checkfieldnotnull($resume_data_single['ex_annual_salary_name'])) ? $resume_data_single['ex_annual_salary_name'] :  $custom_lable_array['notavilablevar']; ?></p>
									</div>
								</div>

							</div>
							<div class="col-md-2 col-xs-12 col-sm-2">

                        <?php  if($this->common_front_model->checkLoginfrontempl() && $this->common_front_model->get_empid()!='')
				 	 			{
									$my_message_add = $base_url.'my-message';
								?>
                                <button href="#small-dialog" data-sender_id="<?php echo $this->common_front_model->get_empid(); ?>" data-receiver_id="<?php echo $resume_data_single['id']; ?>" data-email="<?php echo $resume_data_single['email']; ?>" onClick="return show_modelpp('send_message<?php echo $resume_data_single['id']; ?>');" class="popup-with-zoom-anim  btn btn-success btn-sm send_message<?php echo $resume_data_single['id']; ?>"><i class="fa fa-lg fa-envelope"></i></button>
                                <!--btn btn-default--><!--<i class="fa fa-envelope"></i> --><?php /*echo $custom_lable_array['message'];*/ ?>
                                <!--<button onClick="window.open('<?php echo $my_message_add; ?>','_blank');" style="cursor:pointer;" data-toggle="tooltip" title="Send Message" class="btn btn-success btn-sm"><i class="fa fa-lg fa-envelope"></i> </button>-->
								<?php }
								else
								{
									$my_message_add = $base_url.'login-employer'; ?>
									<button onClick="window.open('<?php echo $my_message_add; ?>','_blank');" style="cursor:pointer;" data-toggle="tooltip" title="Send Message" class="btn btn-success btn-sm"><i class="fa fa-lg fa-envelope"></i> </button><!--onclick="window.location.href='#small-dialog'"-->
								<?php } ?>

							</div>
							<div class="row">

							<div class="col-md-12 col-xs-12 col-sm-12">
								<div class="col-md-2 col-xs-6 col-sm-2" style="padding-left: 0;">
								<a class="btn-job-new" onClick="window.open('<?php echo $base_url; ?>share_profile/js-profile/<?php echo base64_encode($resume_data_single['id']);?>','_blank');" style="cursor:pointer;background: #3c487a;color: #fff;border: 1px solid #3c487a;"><i class="fa fa-plus"></i> <?php echo $custom_lable_arr['more_details']; ?></a>
							</div>
 <?php $skill_keyword = ($this->common_front_model->checkfieldnotnull($resume_data_single['key_skill'])) ? $resume_data_single['key_skill'] : '' ;
									if($skill_keyword != '')
									{
										$i=0;
										$skill_keyword_arr = explode(',',$skill_keyword);
										foreach($skill_keyword_arr as $skill)
										{
											$i++;
                                        if($i==6)
                                        {
                                            echo '<a class="btn-job-new new-check-box-modal">
                                            .......</a>';
                                            break;
                                        }
											echo '
										<a class="btn-job-new new-check-box-modal">'.$skill.'</a>';
										}
									}
									else
									{
										echo $custom_lable_array['notavilablevar'];
									}


									 ?>

									 </div>
                        		</div>
							<div class="">

								<div class="row">

									<div class="col-md-12 col-xs-12 col-sm-3">
										<p class="text-align-mobile" style="font-size: 15px;
										margin-top: 15px;font-family:'Poppins', sans-serif;">About jobseeker : <?php if($this->common_front_model->checkfieldnotnull($resume_data_single['profile_summary'])) {
									if(strlen($resume_data_single['profile_summary']) > 1000)
									{
										echo  substr(htmlspecialchars_decode($resume_data_single['profile_summary'],ENT_QUOTES),0,100).' .....';
									}
									else
									{
										echo $resume_data_single['profile_summary'];
									}

					}else{
									$custom_lable_array['notavilablevar'];
					} ?></p>
									</div>
								</div>

							</div>
						</div>

						<!-- <div class="row">


                        </div> -->

						</div>
                        <?php
                    }

					}//foreach ?>

					<?php
		if(isset($get_page_access_name) && $get_page_access_name=='suggested_job')
		{
			echo $this->common_front_model->rander_pagination('browse-resume/index',$total_resume_count,$limit_per_page);
		}
		else
		{
			echo $this->common_front_model->rander_pagination('browse-resume/index',$total_resume_count,$limit_per_page);

		} ?>
                    <?php
 }
else
{
?>
<div class="five columns">

	<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />

</div>
<?php }
?>
                    <input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" id="search_from_id" value="job_search_option" />
<script>
$('#total_job_count').html('<?php echo $total_resume_count; ?>');
</script>