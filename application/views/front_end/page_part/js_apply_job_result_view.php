<?php
$custom_lable_array = $custom_lable->language;
$confrim_action='';
$action = '';
?>
<?php

if($apply_list_count > 0 && $apply_list_count!='')
{ ?>
<table class="manage-table resumes responsive-table">
<tr>
				<th class="th_bgcolor"><i class="fa fa-file-text"></i> <?php echo $custom_lable_array['job_title']; ?></th>
				<th class="th_bgcolor"><i class="fa fa-calendar"></i> <?php echo $custom_lable_array['job_created_on']; ?></th>
				<th class="th_bgcolor"><i class="fa fa-tags"></i> <?php echo $custom_lable_array['skill_keyword']; ?></th>
				<th class="th_bgcolor"><i class="fa fa-map-marker"></i> <?php echo $custom_lable_array['location']; ?></th>
				<th class="th_bgcolor"><i class="fa fa-clock-o"></i> <?php echo $custom_lable_array['applied_on']; ?></th>
				<th class="th_bgcolor"><i class="fa fa-check-square-o"></i> <?php echo $custom_lable_array['current_status']; ?></th>
				<th class="th_bgcolor"></th>
			</tr>

<?php
	if(isset($apply_list_data) && $apply_list_data !='' && is_array($apply_list_data) && count($apply_list_data) > 0)
	{
		foreach($apply_list_data as $activity_list)
		{ 
			//echo $activity_list['employer_delete'];
			//echo "<pre>";
			//print_r($apply_list_data);
			//echo "</pre>";
				?>
			<tr id="js_activity<?php echo $activity_list['job_id']; ?>">
					<td class="alert-name"><a target="_blank" href="<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($activity_list['job_id']); ?>"><?php echo $activity_list['job_title']; ?></a></td>
					<td><?php echo $this->common_front_model->displayDate($activity_list['posted_on']); ?></td>
					<td class="keywords"><?php echo  $this->common_front_model->checkfieldnotnull($activity_list['skill_keyword']) ?  $activity_list['skill_keyword'] : 'N/A'; ?>
					</td>
					<td><?php echo $this->common_front_model->checkfieldnotnull($activity_list['location_hiring']) ?  $this->common_front_model->get_location_hiring_name($activity_list['location_hiring']) : 'N/A';  ?></td>
					<td><?php echo $this->common_front_model->displayDate($activity_list['applied_on']); ?></td>
					<td>
					<?php
					if($activity_list['currently_hiring_status']=='Yes')
					   {
						   $class = 'label label-success';
						   $lable = $custom_lable_array['job_open_status'];
						   $icon = 'fa fa-folder-open';
					   }
					   else
					   {
						   $class = 'label label-danger';
						   $lable = $custom_lable_array['job_closed_status'];
						   $icon = 'fa fa-times-circle';
					   }
					?>
					<button type="button" data-toggle="tooltip" title="<?php echo $custom_lable_array['currently_job_status']; ?>" class="<?php echo $class; ?>"><i class="<?php echo $icon; ?>" aria-hidden="true"></i> <?php echo $lable;; ?></button>	
					</td>
					<td class="action">
					<?php if($activity_list['employer_delete']=='No')
					{?>
						<!-- remove class = popup-with-zoom-anim-->
						<!--<a href="#small-dialog4" id="js_action" onClick="return jobseeker_action('<?php //echo $action; ?>','job','<?php// echo $activity_list['job_id']; ?>');" data-warning="<?php //echo $confrim_action; ?>" class=" btn btn-block btn-danger btn-xs margin-bottom-5"><span class="glyphicon glyphicon-trash"></span> <?php //echo $custom_lable_array['delete']; ?></a>-->
						<a href="<?php echo $base_url; ?>job-listing/view-job-details/<?php echo base64_encode($activity_list['job_id']); ?>" class="btn btn-block th_bgcolor btn-xs margin-bottom-5" target="_blank"><span class="glyphicon glyphicon-eye-open"></span> <?php echo $custom_lable_array['view_detail']; ?></a>
						
						 <a href="javascript:;" class="btn btn-block th_bgcolor btn-xs margin-bottom-5" onClick=" return view_emp_details('<?php echo $activity_list['posted_by']; ?>');"><span class="glyphicon glyphicon-eye-open"></span> <?php echo $custom_lable_array['view_detail_company']; ?></a>
						<!--<div id="small-dialog4" class="zoom-anim-dialog mfp-hide apply-popup">
							<div class="small-dialog-headline">
								<span class="glyphicon glyphicon-remove-sign"></span> <?php //echo $remove_title; ?>
							</div>
							<div id="js_action_msg_div"></div>
							<div class="small-dialog-content margin-bottom-25">
								<div class="alert alert-danger text-center">
									<span class="glyphicon glyphicon-warning-sign"></span><?php //echo $confrim_action; ?> <br />
									<span class="small"><?php //echo $custom_lable_array['warning_delete']; ?></span>
								</div>
								<hr>
								<div class="pull-right margin-top-0">
									<button class="btn-sm btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo $custom_lable_array['Yes']; ?></button>
									<button class="btn-sm btn-danger"  onClick="close_model();" data-dismiss="modal"><span class="glyphicon glyphicon-trash"></span> <?php //echo $custom_lable_array['No']; ?></button>
								</div>
							</div>
						</div>-->
					<?php }
					else{?>	
							<strong><p>This Employer Does Not Exist</p></strong>
					<?php }?>
					</td>
				</tr>
		<?php  
		} 
		}
	?>
    
</table>
<div >
 <?php  echo $this->common_front_model->rander_pagination('job_seeker_action/js_apply_job_list',$apply_list_count); ?>
</div>
	<?php 
}
else
{
	?>
     <div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
   </div>
    <?php
}
?>
<input type="hidden" id="apply_list_count" value="<?php echo $apply_list_count; ?>" />
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />

