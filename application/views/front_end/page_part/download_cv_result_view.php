<?php
$custom_lable_array = $custom_lable->language;
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('js_photos',1);
$increment = 0;
if(isset($apply_list_count) && $apply_list_count > 0 && $apply_list_count!='')
{
	if(isset($apply_list_data) && $apply_list_data !='' && is_array($apply_list_data) && count($apply_list_data) > 0) 
	{
	foreach($apply_list_data as $activity_list)
	{ 
	    $where_arr = array('id'=>$activity_list['js_id'],'resume_verification'=>'APPROVED','is_deleted'=>'No');
	    $js_details = $this->common_front_model->get_count_data_manual('jobseeker_view',$where_arr,1);
		
		if(isset($js_details) && $js_details !='' && is_array($js_details) && count($js_details) >0)
		{
			
		}
		else
		{
			continue;
		}		
?>
<div class="application" id="seprate_result<?php echo $activity_list['id']; ?>">
			<div class="app-content">
				<div class="info">
					 <?php if($js_details!='' && $js_details['profile_pic'] != ''  && $js_details['profile_pic_approval'] == 'APPROVED' && !is_null($js_details['profile_pic']) && file_exists($upload_path_profile_cmp.'/'.$js_details['profile_pic']))
				{ echo '<img src="'.$base_url.'assets/js_photos/'.$js_details['profile_pic'].'"  class="blah1" style="max-width:60%;" alt="" />'; ?><?php  }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"  class="blah1" style="max-width:60%;" alt="" />';  } ?>
                
                <?php if($js_details['is_deleted']=='No')
					{?>
					<a  style="cursor:pointer;"  onClick="view_js_details('<?php echo $js_details['id']; ?>');" ><span><?php echo ($this->common_front_model->checkfieldnotnull($js_details['fullname'])) ? $js_details['personal_titles'].' '.$js_details['fullname'] : "Not Available" ; ?></span> </a>
					<ul>
						<?php if($js_details['resume_file']!='' && $js_details['resume_verification']=='APPROVED')
						{
							 ?>
                        <li><a href="javascript:;" onClick="return download_resume('<?php echo base64_encode($js_details['resume_file']);/*$this->encryption->encrypt

($js_details['resume_file']);*//*$js_details['resume_file']*/ ?>','<?php echo base64_encode($js_details['id']); ?>');"  class="btn btn-default"><i class="fa fa-file-

text"></i><?php echo $custom_lable_array['download_cv']; ?> </a></li>
                        <?php } ?>

                        
						<li><a href="#small-dialog" data-sender_id="<?php echo $activity_list['posted_by']; ?>" data-receiver_id="<?php echo 

$js_details['id']; ?>" data-email="<?php echo $js_details['email']; ?>" onClick="return show_modelpp('send_message<?php echo $js_details['id']; ?>');" class="popup-

with-zoom-anim btn btn-default send_message<?php echo $js_details['id']; ?>"><i class="fa fa-envelope"></i><?php echo $custom_lable_array['message']; ?></a></li>
					</ul>
                    <?php }
					else
					{?>
                    	<a><span><?php echo ($this->common_front_model->checkfieldnotnull($js_details['fullname'])) ? $js_details['personal_titles'].' '.$js_details['fullname'] : "Not Available" ; ?></span> </a>
                    	<ul>
                        	<li><strong><p>This Job Seeker Does Not Exist</p></strong></li>
                        </ul>
				<?php }?>
					
				</div>
                <?php if($js_details['is_deleted']=='No'){?>
				<div class="buttons">
					<!--<a href="#one-1" class="btn btn-default app-link"><i class="fa fa-pencil"></i> Edit</a>-->
					<a href="#job_details<?php echo $increment; ?>" onClick="show_details('job_details<?php echo $increment; ?>');" class="btn btn-default app-link"><i class="fa fa-sticky-note"></i>  <?php echo $custom_lable_array['show_job_details']; ?></a>
					<a  onClick="view_js_details('<?php echo $js_details['id']; ?>');" class="btn btn-default app-link"><i class="fa fa-plus-circle"></i> <?php echo $custom_lable_array['show_js_details']; ?></a>
                   
                    <?php 
					if($activity_list['is_shortlisted'] =='No')
					{ ?>
                    
                    <a onClick="add_to_shortlist('<?php echo $activity_list['id']; ?>');" class="btn btn-default  short_list<?php echo $activity_list['id']; ?>"><i class="fa fa-plus-circle"></i> <?php echo $custom_lable_array['add_to_shortlist']; ?></a>
                   <?php } 
				   else
				   { ?>
                   <a  onClick="remove_from_shortlist('<?php echo $activity_list['id']; ?>');" class="btn btn-default  short_list<?php echo $activity_list['id']; ?>"><i class="fa fa-check"></i> <?php echo $custom_lable_array['added_to_shortlist']; ?></a>
				   <?php }
				   ?>
				</div>
                <?php }?>
				<div class="clearfix"></div>
			</div>
			<!--  Hidden Tabs -->
			<div class="app-tabs">
				<a href="javascript:;" id="close_box_job_details<?php echo $increment; ?>" onClick="hide_details('job_details<?php echo $increment; ?>');" class="close-tab button gray hide-close-box"><i class="fa fa-close"></i></a>
				
			    <div class="app-tab-content hide_job_details"  id="job_details<?php echo $increment; ?>">
                    <i><?php echo $custom_lable_array['job_title']; ?> :</i>   <span><?php echo ($this->common_front_model->checkfieldnotnull($activity_list['job_title'])) ? $activity_list['job_title'] : "Not Available" ; ?></span> 
                   <div class="clearfix"></div>
                  <i><?php echo $custom_lable_array['posted_on']; ?> :</i>    <span><?php echo ($this->common_front_model->checkfieldnotnull($activity_list

['posted_on'])) ?$this->common_front_model->displayDate($activity_list['posted_on']) : "Not Available" ; ?></span> 
			    </div>
			    
			</div>
			<!-- Footer -->
			<div class="app-footer">
				<!--<div class="rating no-stars">
					<div class="star-rating"></div>
					<div class="star-bg"></div>
				</div>-->
				<ul>
					<li><i class="fa fa-file-text-o"></i>  <?php echo ($this->common_front_model->checkfieldnotnull($activity_list

['company_name'])) ? $activity_list['company_name'] : "Not Available" ; ?></li>
					<li><i class="fa fa-calendar"></i> <?php echo ($this->common_front_model->checkfieldnotnull($activity_list['posted_on'])) ?

$this->common_front_model->displayDate($activity_list['posted_on']) : "Not Available" ; ?></li>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
 <?php    } 
}?> 
 <div >
 
 
 
 <?php 
 if(isset($page_name) && $page_name=='shortlisted_application')
	{
		echo $this->common_front_model->rander_pagination('job_application/shortlisted_application',$apply_list_count);
	}
	else
	{
		 echo $this->common_front_model->rander_pagination('job_application/download_cv',$apply_list_count);
	}
 ?>
</div>
<?php
   }
else
{
	?>
     <div class="five columns">
	  <img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
     </div>
    <?php
}
?>
<input type="hidden" id="apply_list_count" value="<?php echo $apply_list_count; ?>" />
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" id="search_from_id" value="search_user_form" /> 
<input type="hidden" id="shortlist_count" value="<?php echo $apply_list_count; ?>" />
<script>
$(document).ready(function(e) {
    $('.hide_job_details').hide();
	$('.hide-close-box').hide();
	
});
</script>