<!-- Footer ================================================== -->
<?php 
$custom_lable_arr = $custom_lable->language;
$config_data = $this->common_front_model->data; 
if(basename(__FILE__)=='footer_employer.php')
{
?>
<!--<div class="margin-top-30"></div>-->
<!-- style defined for footer padding decrement jay -->
<style>
.footer-bottom
{
	padding:10px 0px;
}
.social-icons
{
	margin-bottom:0px !important;
}
.container .social-icons-custom
{
	text-align:left;
}
.container .copyrights-custom
{
	text-align:right;
}
</style>
<?php } ?><!--<div class="margin-top-30"></div>-->

<div id="footer">
	<!-- Main -->
	<div class="container">

		<div class="six columns">
			<h4><?php echo $custom_lable_arr['footer_about_txt'];?></h4>
			<p><?php echo $this->common_front_model->getfooterabouttxt().'...';?><a href="<?php echo $base_url; ?>cms/about-us">Read more</a></p>
            
			<a href="<?php echo $base_url; ?>sign-up" class="button"><?php echo $custom_lable_arr['footer_get_started_btn_txt'];?></a>
		</div>

		<div class="three columns">
			<h4>Company</h4>
			<ul class="footer-links">
				<li><a href="<?php echo $base_url; ?>cms/about-us"><?php echo $custom_lable_arr['footer_menu_about_txt'];?></a></li>
				<li><a href="<?php echo $base_url; ?>cms/careers"><?php echo $custom_lable_arr['footer_menu_careers_txt'];?></a></li>
				<li><a href="<?php echo $base_url; ?>blog/allblogs"><?php echo $custom_lable_arr['footer_menu_blog_txt'];?></a></li>
				<li><a href="<?php echo $base_url; ?>cms/terms-of-service"><?php echo $custom_lable_arr['footer_menu_term_txt'];?></a></li>
				<li><a href="<?php echo $base_url; ?>cms/privacy-policy"><?php echo $custom_lable_arr['footer_menu_p_policy_txt'];?></a></li>
                <li><a href="<?php echo $base_url; ?>contact"><?php echo $custom_lable_arr['contact_page_title'];?></a></li>
                
			</ul>
		</div>
		
        <?php  if($this->common_front_model->checkLoginfrontempl())
				{ $my_message_add = $base_url.'my-message'; }else{ $my_message_add = $base_url.'login-employer'; } ?>
		<div class="three columns">
			<h4>Browse</h4>
			<ul class="footer-links">
				<!--<li><a href="#">Browse</a></li>
				<li><a href="#">Press Releases</a></li>-->
				<li><a href="<?php echo $my_message_add; ?>"><?php echo $custom_lable_arr['message_employer_foot'];?></a></li>
                <!--<li><a href="<?php echo $base_url; ?>browse-job-seekers"><?php echo $custom_lable_arr['search_by_jobseeker_foot'];?></a></li>-->
                <li><a href="<?php echo $base_url; ?>browse-resume">Browse resumes</a></li>
				<!--<li><a href="#">Testimonials</a></li>
				<li><a href="#">Timeline</a></li>-->
			</ul>
		</div>

		<div class="four columns">
			<h4><?php echo $custom_lable_arr['footer_menu_contact_tit'];?></h4>
			<ul class="footer-links">
				<span class="ftitle"><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo $custom_lable_arr['footer_menu_contact_tit1'];?> :</span><br />
				<span class="pnr"><i class="fa fa-lg fa-mobile"></i>&nbsp;&nbsp;<?php   echo $this->common_front_model->checkfieldnotnull($config_data['config_data']['contact_no']) ? $config_data['config_data']['contact_no'] : $custom_lable_arr['notavilablevar'] ;?><!--<br /><i class="fa fa-phone"></i> +91-9342627372--></span><br />
				<span class="" style="word-break: break-all;overflow-wrap: break-word;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:<?php   echo $this->common_front_model->checkfieldnotnull($config_data['config_data']['contact_email']) ? $config_data['config_data']['contact_email'] : $custom_lable_arr['notavilablevar'] ;?>"><?php   echo $this->common_front_model->checkfieldnotnull($config_data['config_data']['contact_email']) ? $config_data['config_data']['contact_email'] : $custom_lable_arr['notavilablevar'] ;?></a></span>

			</ul>
		</div>

	</div>

	<!-- Bottom -->
	<div class="container">
    <div class="footer-bottom">
        	<div class="two columns">
            	<h4>Follow Us</h4>
             </div>
             <div class="six columns social-icons-custom">   
				<ul class="social-icons">
					<li><a class="facebook" target="_blank" href="<?php echo $config_data['config_data']['facebook_link'];?>"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" target="_blank" href="<?php echo $config_data['config_data']['twitter_link'];?>"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" target="_blank" href="<?php echo $config_data['config_data']['google_link'];?>"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" target="_blank" href="<?php echo $config_data['config_data']['linkedin_link'];?>"><i class="icon-linkedin"></i></a></li>
				</ul>
            </div>
            <div class="eight columns copyrights-custom">
            	<div class="copyrights">©  <!--Copyright <?php echo '2016-'.date('Y') ;?> by <a href="<?php echo $base_url; ?>"><?php echo $config_data['config_data']['web_frienly_name'];?></a>. All Rights Reserved.--><?php echo $config_data['config_data']['footer_text'];?></div>	
            </div>
		</div>
		<!--<div class="footer-bottom">
			<div class="sixteen columns">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					<li><a class="facebook" target="_blank" href="<?php echo $config_data['config_data']['facebook_link'];?>"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" target="_blank" href="<?php echo $config_data['config_data']['twitter_link'];?>"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" target="_blank" href="<?php echo $config_data['config_data']['google_link'];?>"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" target="_blank" href="<?php echo $config_data['config_data']['linkedin_link'];?>"><i class="icon-linkedin"></i></a></li>
				</ul>
				<div class="copyrights">©  Copyright <?php echo (date('Y')-1).'-'.date('Y') ;?> by <a href="<?php echo $base_url; ?>"><?php echo $config_data['config_data']['web_frienly_name'];?></a>. All Rights Reserved.</div>
			</div>
		</div>-->
	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div> <!-- Wrapper / End -->
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="hash_tocken_id" />
<input type="hidden" name="base_url" value="<?php echo $base_url; ?>" id="base_url" />
<div id="lightbox-panel-mask"></div>
<div id="lightbox-panel-loader"><center><img alt="Please wait.." title="Please wait.." src='<?php echo $base_url; ?>assets/front_end/images/loader/ajax-loader_big.gif' /></center></div>



<script src="<?php echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<!--<script src="<?php echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>-->
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>
<script>
$(document).ready(function(e) {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
});	
<?php if(isset($perameter) && $perameter=='signup') { ?>
$(document).ready(function(e) {
	
    $("#login_tab").removeClass('active');
	$("#tab1").removeClass('active');
	$("#tab1").css('display','none');
	$("#tab2").css('display','block');
});
<?php } ?>
function gotopage(parameter)
{
	if(parameter=='1')
	{
		editsection('profile_photo');
	}
	else
	{
		window.location.href='<?php echo $base_url.'my_profile' ?>';
	}
}
</script>
</body>
</html>