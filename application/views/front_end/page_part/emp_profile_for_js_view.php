<?php
$custom_lable_arr = $this->lang->language;
$user_data = $user_data['emp_data'] ;
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('company_logos',1);
?>
<?php if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
{ 
	$js_id = $this->common_front_model->get_userid();
	$return_data_plan = $this->common_front_model->get_plan_detail($js_id,'job_seeker','contacts');
} 
?>
<style>
foreditfontcolor
{
	color:#ffffff;
}
</style>
<div class="col-md-12 col-sm-12 col-xs-12 tab-pane active" id="profile_snapshot_viewdiv">
							<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_arr['sign_up_emp_basic_detail']; ?> :</span> <a href="javascript:void(0);" onClick="close_emp_details();" class="btn btn-md btn-danger pull-right foreditfontcolor " style="margin:-3px;" ><i class="fa fa-close" aria-hidden="true" ></i> <?php echo $custom_lable_arr['close']; ?></a></div>
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
												<table class="table">
													<tbody>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['sign_up_emp_1stformfullname']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['fullname'])) ? $user_data['personal_titles'].' '.$user_data['fullname'] : "Not Available" ; ?></td>
														</tr>
														
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformabout']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['job_profile']!='0' && $this->common_front_model->checkfieldnotnull($user_data['job_profile'])) ? $user_data['job_profile'] : "Not Available";?></td>
														</tr>
													</tbody>
												</table>
											</div>
									</div>
								</div>
							</div>
						</div>
<div class="col-md-12 col-sm-12 col-xs-12 tab-pane active" id="profile_snapshot_viewdiv">
							<div class="panel panel-primary box-shadow1 th_bordercolor" style="border:none;border-radius:0px;border-bottom:1px solid;"><!--#D9534F-->
								<div class="panel-heading panel-bg" style=""><span class="th_bgcolor" style="padding:5px;color:#ffffff;"><!--background-color:#D9534F;--><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_arr['sign_up_emp_2ndtabtitle']; ?> :</span> <a href="javascript:void(0);" onClick="close_emp_details();" class="btn btn-md btn-danger pull-right foreditfontcolor " style="margin:-3px;" ><i class="fa fa-close" aria-hidden="true" ></i> <?php echo $custom_lable_arr['close']; ?></a></div>
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
												<table class="table">
													<tbody>
														<tr>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['sign_up_emp_1stformcompnaylogoview']; ?> :</td>
															<td class="col-md-6 col-xs-6" style="border-top:none;"><?php if($user_data!='' && $user_data['company_logo'] != '' && !is_null($user_data['company_logo']) && file_exists($upload_path_profile_cmp.'/'.$user_data['company_logo']))
				{ echo '<img src="'.$base_url.'assets/company_logos/'.$user_data['company_logo'].'"  class="blah1" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"  class="blah1" style="max-width:60%;" alt="" />'; } ?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6" style=""><?php echo $custom_lable_arr['sign_up_emp_companydet']; ?> :</td>
															<td class="col-md-6 col-xs-6" style=""><?php echo ($user_data['company_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['company_name'])) ? $user_data['company_name'] : "Not Available";?></td>
														</tr>
													<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmptypelbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['company_type_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['company_type_name'])) ? $user_data['company_type_name'] : "Not Available";?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpsjizelbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php if($user_data['company_size']!='0' && $this->common_front_model->checkfieldnotnull($user_data['company_size']) && $this->common_front_model->checkfieldnotnull($user_data['company_size_name'])) { echo $user_data['company_size_name'];}else  { echo $custom_lable_arr['notavilablevar'];} ?></td>
														</tr>
                                                        <?php 
														if(isset($js_id) && $js_id!='')
														{
															if($return_data_plan == 'Yes')
															{
																 ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpweblbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['company_website']!='0' && $this->common_front_model->checkfieldnotnull($user_data['company_website'])) ? $user_data['company_website'] : "Not Available";?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_cmpprolbl']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['company_profile']!='0' && $this->common_front_model->checkfieldnotnull($user_data['company_profile'])) ? $user_data['company_profile'] : $custom_lable_arr['notavilablevar'];?></td>
														</tr>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['myprofile_emp_industrylbl']; ?>:</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['industries_name']!='0' && $this->common_front_model->checkfieldnotnull($user_data['industries_name'])) ? $user_data['industries_name'] : "Not Available";?></td>
														</tr>
														
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformcnt']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['city']!='0' && $this->common_front_model->checkfieldnotnull($user_data['country_name']) && $this->common_front_model->checkfieldnotnull($user_data['country_name'])) ? $user_data['country_name'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
                                                        <?php 
														if(isset($js_id) && $js_id!='')
														{
															if($return_data_plan == 'Yes')
															{
																 ?>
														<tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformcity']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['city']!='0' && $this->common_front_model->checkfieldnotnull($user_data['city']) && $this->common_front_model->checkfieldnotnull($user_data['city_name'])) ? $user_data['city_name'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
                                                        <tr>
															<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['sign_up_emp_1stformpincode']; ?> :</td>
															<td class="col-md-6 col-xs-6"><?php echo ($user_data['pincode']!='0' && $this->common_front_model->checkfieldnotnull($user_data['pincode'])) ? $user_data['pincode'] : $custom_lable_arr['notavilablevar']; ?></td>
														</tr>
                                                        <?php 
															}//if plan is valid
														} ?>
														
														
													</tbody>
												</table>
											</div>
									</div>
								</div>
							</div>
						</div>                        
<?php 
if(isset($js_id) && $js_id!='')
{
	if($return_data_plan == 'Yes')
	{
		$this->common_front_model->check_for_plan_update($js_id,'job_seeker',$user_data['id']);
	}//if plan is valid
} ?>