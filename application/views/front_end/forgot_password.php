<?php
$custom_lable_array = $custom_lable->language;
?>

<div id="titlebar" class="single submit-page" style="background-color:#94AFCC;">
	<div class="container">
		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-lock" aria-hidden="true"></i> <?php echo $custom_lable_array['forgot_pass_page_title']; ?></h2>
		</div>
	</div>
</div>

<div class="container">
    <div class="row">
		<div class="">
			<div class="well">
				<div class="panel panel-primary">
					<div class="panel-heading"><?php echo $custom_lable_array['forgot_pass_page_title']; ?></div>
						<div class="panel-body">
							<div class="margin-top-10"></div>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="text-center">
											  <img src="<?php echo $base_url; ?>assets/front_end/images/shield.png" class="img-responsive text-center" alt="setting" style="width:30%;margin:0 auto;"/>
											  <h2 class="text-center"><?php echo $custom_lable_array['forgot_pass_page_title']; ?> ?</h2>
											  <p><?php echo $custom_lable_array['forgot_pass_page_text']; ?>.</p>
                                        <div class="alert alert-danger" id="error_msg_f" style="display:none" > </div>
                                        <div class="alert alert-success" id="success_msg_f" style="display:none" ></div>
               							   <form class="form-group" id="forgot_pass_form">
												<div class="panel-body">
													<div class="form-group">
														<div class="input-group">
														  <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
														  <input id="email" name="email" data-validation="required,email" placeholder="<?php echo $custom_lable_array['forgot_pass_p_h_text']; ?>" class="form-control"  type="email">
														</div>
                                                    </div>
                                                    <!--<div class="input-group">
														  <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
														  <input id="email" name="email" data-validation="required,email" placeholder="<?php echo $custom_lable_array['forgot_pass_p_h_text']; ?>" class="form-control"  type="email">
														</div>-->
                                                       
												  </div>
                                                   
                                                   
												  <div class="form-group">
													<input name="forgot_password" class="btn btn-lg btn-primary btn-block" value="<?php echo $custom_lable_array['forgot_pass_btn_lbl']; ?>" type="submit">
												  </div>
                                                  <input name="action" type="hidden"  value="forgot_password" />
                                                  <input name="user_agent" type="hidden"  value="NI-WEB" /> 
							
												</form> 
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
   </div>
   
<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
   
<script>
$.validate({
    form : '#forgot_pass_form',
    modules : 'security',
    onError : function($form) {
	  $("#error_msg_f").slideDown();
	  $("#error_msg_f").html("<?php echo $custom_lable_array['login_js_form_val_mes']; ?>");
	  scroll_to_div('error_msg_f',-100);
	},
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg_f").hide();
	  var datastring = $("#forgot_pass_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'login/reset_forgot_password' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			  hide_comm_mask();
			if($.trim(data.status) == 'success')
			{
				$("#error_msg_f").slideUp();
				$("#success_msg_f").slideDown();
				$("#success_msg_f").html(data.errmessage);
				document.getElementById('forgot_pass_form').reset();
				scroll_to_div('success_msg_f',-100);
			}
			else
			{
				$("#success_msg_f").slideUp();
				$("#error_msg_f").html(data.errmessage);
				$("#error_msg_f").slideDown();
				scroll_to_div('error_msg_f',-100);
			}
			
		}
	});	
	
      return false; // Will stop the submission of the form
    }
  });
</script>