<style>

</style>
<?php
$custom_lable_arr = $custom_lable->language;
?>
<!-- Titlebar ================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2><?php echo  $custom_lable_arr['all_blog_page_title']; ?></h2>
			<span><?php echo $custom_lable_arr['all_blog_page_subtitle']; ?></span>
		</div>

	</div>
</div>
<!-- Content ================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="eleven columns">
		<div class="padding-right">

			<!-- Post -->
			<div class="post-container">
            	<?php 
				if($blog_data['blog_image']!='' && file_exists('./assets/blog_image/'.$blog_data['blog_image'])){ ?>
				<div class="post-img"><a href="javascript:;"><img src="<?php echo $base_url; ?>assets/blog_image/<?php echo $blog_data['blog_image']; ?>" alt="<?php echo $blog_data['title']; ?>"></a></div>
                <?php } ?>
				<div class="post-content">
					<a href="javascript:;"><h3><?php echo $blog_data['title']; ?></h3></a>
					<div class="meta-tags">
						<span><?php //$date = strtotime( $blog_data['created_on'] );
                                    //echo $dateformat = date( 'F j, Y ', $date );
									echo $this->common_front_model->displayDate($blog_data['created_on'],'F j, Y '); ?></span>
						<!--<span><a href="#">0 Comments</a></span>-->
					</div>
					<div class="clearfix"></div>
					<div class="margin-bottom-25"></div>
					<?php 
						if(isset($blog_data['content']) && $blog_data['content'] !="")
						{
							//echo htmlspecialchars($blog_data['content']);
							echo htmlspecialchars_decode(stripslashes($blog_data['content']));
						}
					?>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Widgets -->
	<?php $this->load->view('front_end/blog_right'); ?>
	<!-- Widgets / End -->


</div>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script src="<?php echo $base_url; ?>assets/front_end/js/custom.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.superfish.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.flexslider-min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/waypoints.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.counterup.min.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/jquery.jpanelmenu.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/stacktable.js"></script>
<script src="<?php echo $base_url; ?>assets/front_end/js/headroom.min.js"></script>
