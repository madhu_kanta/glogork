<div class="panel panel-primary box-shadow1 th_bordercolor"  style="border:none;border-radius:0px;border-bottom:1px solid ;position: relative;top: -89px;box-shadow: 1px -1px 7px 1px rgba(0,0,0, .2);"><!--#D9534F-->
							<div class="panel-heading panel-bg" style="color:#ffffff;border-bottom:4px solid"><!--#D9534F--><!--padding:0px;--><span class="th_bgcolor" style="padding:5px;"><!--background-color:#D9534F;--><span class="	glyphicon glyphicon-file"></span> Other Details</span> <a href="javascript:void(0);" class="btn btn-md pull-right foreditfontcolor" style="margin:-3px;" onClick="editsection('other_details');"><i class="fa fa-fw fa-edit" aria-hidden="true"></i> Edit</a></div><!--background:none;-->
								
								<div class="panel-body" style="padding:10px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<table class="table">
												<tbody>
													<tr>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo $custom_lable_arr['profile_summary']; ?> :</td>
														<td class="col-md-6 col-xs-6" style="border-top:none;"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['profile_summary'])) ? $user_data['profile_summary'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['preferred_city']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php 
														
														if(isset($user_data['preferred_city']) && $user_data['preferred_city'] !='')
															{
																echo $this->common_model->valueFromId('city_master',$user_data['preferred_city'],'city_name');
															}
															else
															{
																 echo $custom_lable_arr['notavilablevar'];
															}
													//	echo ($this->common_front_model->checkfieldnotnull($user_data['preferred_city'])) ? $user_data['preferred_city'] : $custom_lable_arr['notavilablevar']; 
														
														?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['prefered_shift']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['prefered_shift'])) ? $user_data['prefered_shift'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6" ><?php echo $custom_lable_arr['website']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['website'])) ? $user_data['website'] : $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['total_experience'] ?> :</td>
														<td class="col-md-6 col-xs-6">
														<?php 
														if($totalexp!='')
														{
															if(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0'){ echo $totalexp[0].' '.$custom_lable_arr['year'] ; } elseif(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]=='0') { echo ""; }
															if(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!='0' ){ echo $totalexp[1].' '.$custom_lable_arr['month'] ; }  elseif(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[0]=='0') { echo "Fresher" ; }
														}
														elseif($totalexp=='00-00' || $totalexp=='0-0')
														{
															echo "Fresher";
														}
														else
														{
															echo  $custom_lable_arr['notavilablevar'];
														}
														?>
                                                        </td>
													</tr>
													<tr>
                                                    <?php $salary_range = $this->my_profile_model->getdetailsfromid('salary_range','id',$user_data['annual_salary'],'salary_range'); ?>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['current_annual_salary'] ?> :</td>
														<td class="col-md-6 col-xs-6">
														<?php echo ($user_data['annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($user_data['annual_salary']) && is_array($salary_range) && count($salary_range) > 0 && $salary_range['salary_range']!='' ) ? $salary_range['salary_range'] :  $custom_lable_arr['notavilablevar']; ?>
														</td>
													<?php /*?><td class="col-md-6 col-xs-6"><?php 
														
													if($annual_salary!='' && (isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0') || (isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!=0)) 

													{
														if($this->common_front_model->checkfieldnotnull($user_data['currency_type'])) { echo  $user_data['currency_type'] ; } 
														
														if($this->common_front_model->checkfieldnotnull($annual_salary) && isset($annual_salary[0]) && $annual_salary[0]!='' ){ echo " ".$annual_salary[0].' '.$custom_lable_arr['lacs'] ; }
														if($this->common_front_model->checkfieldnotnull($annual_salary) && isset($annual_salary[1]) && $annual_salary[1]!='' ){ echo " ".$annual_salary[1].' '.$custom_lable_arr['thousand'] ; }
													}
													else
													{
														echo  $custom_lable_arr['notavilablevar'];
													}
														 ?></td><?php */?>
                                                         
													</tr>
                                                    <!--<tr>
														<td class="col-md-6 col-xs-6">Marital Status:</td>
                                                        <?php $maritalstatusget = $this->my_profile_model->getdetailsfromid('marital_status_master','id',$user_data['marital_status'],'marital_status'); ?>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['marital_status']!='0' && $this->common_front_model->checkfieldnotnull($user_data['marital_status']) && count($maritalstatusget) > 0 && $maritalstatusget['marital_status']!='' ) ? $maritalstatusget['marital_status'] :  $custom_lable_arr['notavilablevar']; ?></td>
													</tr>-->
													<tr>
                                                    <?php $industries = $this->my_profile_model->getdetailsfromid('industries_master','id',$user_data['industry'],'industries_name'); ?>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['type_industry']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['industry']!='0' && $this->common_front_model->checkfieldnotnull($user_data['industry']) && count($industries) > 0 && $industries['industries_name']!='' ) ? $industries['industries_name'] :  $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['functional_area']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($user_data['functional_area']) && $user_data['functional_area']!='0' && $this->common_front_model->checkfieldnotnull($user_data['functional_name']) ) ? $user_data['functional_name'] :  $custom_lable_arr['notavilablevar']; ?>
                                                        </td>
													</tr>
													<tr>
                                                    <?php $jbrole = $this->my_profile_model->getdetailsfromid('role_master','id',$user_data['job_role'],'role_name'); ?>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['job_role']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($user_data['job_role']!='0' && $this->common_front_model->checkfieldnotnull($user_data['job_role']) && count($jbrole) > 0 && $jbrole['role_name']!='' ) ? $jbrole['role_name'] :  $custom_lable_arr['notavilablevar']; ?></td>
													</tr>
                                                    <tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['skill']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['key_skill'])) ? $user_data['key_skill'] : $custom_lable_arr['notavilablevar'] ; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['desire_job_type']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['desire_job_type'])) ? $user_data['desire_job_type'] : $custom_lable_arr['notavilablevar'] ; ?></td>
													</tr>
													<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['employment_type']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['employment_type'])) ? $user_data['employment_type'] : $custom_lable_arr['notavilablevar'] ; ?></td>
													</tr>
                                                    <!--<tr>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['prefered_shift']; ?> :</td>
														<td class="col-md-6 col-xs-6"><?php echo ($this->common_front_model->checkfieldnotnull($user_data['prefered_shift'])) ? $user_data['prefered_shift'] : $custom_lable_arr['notavilablevar'] ; ?></td>
													</tr>-->
                                                    <tr>
														<?php $salary_range = $this->my_profile_model->getdetailsfromid('salary_range','id',$user_data['expected_annual_salary'],'salary_range'); ?>
														<td class="col-md-6 col-xs-6"><?php echo $custom_lable_arr['expected_annual_salary'] ?> :</td>
														
														<td class="col-md-6 col-xs-6">
														<?php echo ($user_data['expected_annual_salary']!='0' && $this->common_front_model->checkfieldnotnull($user_data['expected_annual_salary']) && count($salary_range) > 0 && $salary_range['salary_range']!='' ) ? $salary_range['salary_range'] :  $custom_lable_arr['notavilablevar']; ?>
														</td>
														
														<?php /*?><td class="col-md-6 col-xs-6"><?php 
													if($annual_salary_exp!='' && (isset($annual_salary_exp[0]) && $annual_salary_exp[0]!='' && $annual_salary_exp[0]!='0') || (isset($annual_salary_exp[1]) && $annual_salary_exp[1]!='' && $annual_salary_exp[1]!=0)) 
													{
														if($this->common_front_model->checkfieldnotnull($user_data['exp_salary_currency_type'])) { echo  $user_data['exp_salary_currency_type']; } 
														if($this->common_front_model->checkfieldnotnull($annual_salary_exp) && isset($annual_salary_exp[0]) && $annual_salary_exp[0]!='' ){ echo " ".$annual_salary_exp[0].' '.$custom_lable_arr['lacs'] ; }
														if($this->common_front_model->checkfieldnotnull($annual_salary_exp) && isset($annual_salary_exp[1]) && $annual_salary_exp[1]!='' ){ echo " ".$annual_salary_exp[1].' '.$custom_lable_arr['thousand'] ; }
													}
													else
													{
														echo  $custom_lable_arr['notavilablevar'];
													}
														 ?></td><?php */?>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>