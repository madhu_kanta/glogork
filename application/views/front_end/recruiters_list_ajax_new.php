<?php $custom_lable_arr = $custom_lable->language;$upload_path_profile = $this->common_front_model->fileuploadpaths('emp_photos',1);
$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
  ?>
<?php 	
if(isset($recruiters_data) && $recruiters_data!='' && is_array($recruiters_data) && count($recruiters_data) > 0 )
{ $custom_counter_fordiv = 0 ; ?>
<div class="row">
<?php foreach($recruiters_data as $recruiters_data_single)
				{ $custom_counter_fordiv = $custom_counter_fordiv + 1;
					if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
					{
						$js_id = $this->common_front_model->get_userid();							
						$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$recruiters_data_single['id']);
						$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
					}
				  ?>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="m-k-g-s">
				<div class="margin-top-20">
					<div class="col-sm-6 col-md-4">
                    <?php  if($recruiters_data_single['profile_pic_approval'] == 'APPROVED' && $recruiters_data_single['profile_pic'] != '' && !is_null($recruiters_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$recruiters_data_single['profile_pic']))
				{  ?>
						<img class="img-responsive r-1-img" src="<?php echo $base_url ;?>assets/emp_photos/<?php echo $recruiters_data_single['profile_pic'] ;?>" alt="<?php echo $recruiters_data_single['fullname']?>"/>
                        <?php }else { ?>
                            <img class="img-responsive r-1-img" src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $recruiters_data_single['fullname']?>"/>
                            <?php } ?>
					</div>
					<div class="col-sm-6 col-md-8 new-p-job-new">
						<p><i class="fa fa-user font-job"></i> <span class="name-p"> <?php echo ($this->common_front_model->checkfieldnotnull($recruiters_data_single['fullname'])) ? $recruiters_data_single['personal_titles'].' '.$recruiters_data_single['fullname'] : $custom_lable_arr['notavilablevar'] ; ?></span></p>

						<p><i class="fa fa-home font-job-2"></i> <span class="name-p-2" > <?php echo ($this->common_front_model->checkfieldnotnull($recruiters_data_single['industry'])) ? substr($recruiters_data_single['industries_name'],0,22).((strlen($recruiters_data_single['industries_name'])>22)?'...':'') : $custom_lable_arr['notavilablevar'] ; ?></span></p>

						<p><i class="fa fa-building	
						font-job-2"></i> <span class="name-p-2"><a target="_blank" href="<?php echo $base_url; ?>recruiters/company/<?php echo base64_encode($recruiters_data_single['id']);?>"> <?php echo ($this->common_front_model->checkfieldnotnull($recruiters_data_single['company_name'])) ? substr($recruiters_data_single['company_name'],0,22).((strlen($recruiters_data_single['company_name'])>22)?'...':'') : $custom_lable_arr['notavilablevar'] ; ?></a></span></p>
					</div>
					
				</div>
				<hr class="job-hr">
				<div class="margin-top-0">
					<div class="col-sm-12 col-md-12">
						<p class="skill-p"><span style="color:#353535;font-weight:500;"><?php echo $custom_lable_arr['skill_hire_for_lbl']; ?></span> : 
						<span style="color:#969191;"><?php $skillhire_stored = ($recruiters_data_single['skill_hire']!='0' && $this->common_front_model->checkfieldnotnull($recruiters_data_single['skill_hire'])) ? $this->employer_profile_model->getdetailsfromids('key_skill_master','id',$recruiters_data_single['skill_hire'],'key_skill_name') : "";
								if($this->common_front_model->checkfieldnotnull($skillhire_stored) && is_array($skillhire_stored) && count($skillhire_stored) > 0)
								{
									$skillhire_stored_com = implode(',',$skillhire_stored);
									if(strlen($skillhire_stored_com) > 50)
									{
										echo  substr($skillhire_stored_com,0,50).' ...';
									}
									else
									{
										echo $skillhire_stored_com;
									}
									
								}
								else
								{
									echo $custom_lable_arr['notavilablevar'];
								}
								?></span></p>
					</div>
					<div class="col-sm-12 col-md-12 margin-top-10">
						<p class="skill-p"> 
						<span style="color:#969191;font-weight:normal;"><a target="_blank" href="<?php echo $base_url; ?>recruiters/company/<?php echo base64_encode($recruiters_data_single['id']);?>#activejobs" ><?php echo $this->our_recruiters_model->getrecruitersactivejob_count($recruiters_data_single['id']); ?> <?php echo $custom_lable_arr['recruiters_page_act_job']; ?></a> | <i class="fa fa-clock-o"></i> <?php echo $custom_lable_arr['recruiters_page_act_since']; ?>  <?php echo ($recruiters_data_single['register_date']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($recruiters_data_single['register_date'])) ? $this->common_front_model->displayDate($recruiters_data_single['register_date'],'F j, Y') : $custom_lable_arr['notavilablevar']; ?></span></p>
					</div>
				</div>
				<div class="margin-top-20">
                <?php 
                    $follow_action = 'follow_emp';
                    $follow_icon = '<span class="glyphicon glyphicon-user"></span>';
                    $follow_text = $custom_lable_arr['follow'];
                ?>
                <?php if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
                { 
                    if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && is_array($jobseeker_access_employer) && count($jobseeker_access_employer) > 0)
                    { 
                        if($jobseeker_access_employer['is_follow']=='Yes')
                        {
                            $follow_action = 'unfollow_emp';
                            $follow_icon = '<span class="glyphicon glyphicon-check"></span>';
                            $follow_text = $custom_lable_arr['following'];
                        }
                    }
                    ?>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-sm bnt-top-10 new-follow emp_follow_action_<?php echo $recruiters_data_single['id']; ?>"  OnClick="return jobseeker_action('<?php echo $follow_action; ?>','Emp','<?php echo $recruiters_data_single['id']; ?>');"><i class="fa fa-plus"></i> <?php echo $follow_text; ?></button>
					</div>
                    <?php } ?>
                    <?php
                    if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
                    { ?>
					<div class="col-sm-6 col-md-6 margin-top-15">
						<a href="#small-dialog" class="send-msg popup-with-zoom-anim myclassclick" data-emp_id="<?php echo $recruiters_data_single['id']; ?>"><i class="fa fa-paper-plane"></i> <?php echo  $custom_lable_arr['send_msg']; ?></a>
					</div>
                    <?php }
								?>
				</div>
				
				<hr class="job-hr new-hr-m">
				<div class="margin-top-20">
					<div class="col-sm-12 col-md-12">
						<p class="skill-p"> 
						<span style="color:#969191;font-weight:normal;" class="follower_count_<?php echo $recruiters_data_single['id']; ?>"><?php echo $this->our_recruiters_model->getrecruiterstotal_follower_count($recruiters_data_single['id']); ?></span> <?php echo $custom_lable_arr['followers']; ?></span></p>
					</div>
					
				</div>
			</div>
		</div>
        <?php 
				if($custom_counter_fordiv % 3 == 0)
				{ ?>
					</div>
                    <div class="row">
				<?php }
				} ?>
	</div>
    <?php echo $this->common_front_model->rander_pagination('recruiters/index',$recruiters_count,$limit_per_page); ?>  
    <?php 
}
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>    
</div>    
<?php 
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
<?php if($this->input->post('is_ajax') && $this->input->post('is_ajax')==1){ ?>
$(document).ready(function(e) {
	onpopupopenappend();
	validatemessageform();
});	
 $('.popup-with-zoom-anim').magnificPopup({
                          type: 'inline'
                     });
<?php } ?>					 
/*$(document).ready(function(e) {*/
    /*$.magnificPopup.open({
	items: {
		src: '#small-dialog',
	},
		type: 'inline'
	});*/
/*});*/
</script>