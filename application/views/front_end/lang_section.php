<?php $custom_lable_arr = $custom_lable->language; ?>
<div class="panel-heading panel-bg bg-new-p" id="language_vieweditdiv"><span class="th_bgcolor" style="padding:5px;"><span class="glyphicon glyphicon-globe"></span> Languages Known Details</span> </div>
						<div class="alert alert-danger" id="message_lang" style="display:none" ></div>
						<div class="alert alert-success" id="success_msg_lang" style="display:none" ></div>
						<form method="post" name="editlangdet" id="editlangdet" action="<?php echo $base_url.'my_profile/editlangdet'; ?>" >
                                    
									<?php 
									//$skill_lvl_lang = $this->common_front_model->get_list('skill_level_master');
                                    if(isset($language_details) && $language_details!='' && is_array($language_details) && count($language_details) > 0 ){
										$countlooplan = 0;	 
										foreach($language_details as $language_details_single)
										{$countlooplan = $countlooplan+1;
										if($countlooplan > 1) 
										{
									?>
                                    	<hr class="th_bordercolor" style="border-width: 4px;" >
                                    <?php } ?>
						<div class="panel-body lang_known_whole" style="padding:10px;"  data-lang_num="<?php echo $countlooplan; ?>">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Language Known </label>
									<select class="input-select-b select-drop-3 lang_known" id="lang_known_<?php echo $language_details_single['id'];?>" style="width:100%;" name="lang_known_<?php echo $language_details_single['id'];?>" data-validation="required" data-langknwon='<?php echo $countlooplan;?>'>
                                        <?php echo (count($language_details) > 0 && $this->common_front_model->checkfieldnotnull($language_details_single['language'])) ? "<option value='".$language_details_single['language']."'>".$language_details_single['language']."</option>" : ""; ?>
                                    </select>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									
									<label class="label-new">Proficiency Level </label>
									<select name="proficiency_<?php echo $language_details_single['id'];?>" id="proficiency_<?php echo $language_details_single['id'];?>" class="input-select-b select-drop-3 city">				
										<?php 
										if(isset($skill_lvl_lang) && $skill_lvl_lang !='' && ($language_details_single['language']=='' || is_null($language_details_single['language'])))
										{
											print_r($skill_lvl_lang);
										}
										elseif(isset($skill_lvl_lang) && $skill_lvl_lang !='' && $this->common_front_model->checkfieldnotnull($language_details_single['language']))
										{ 
											print_r($this->common_front_model->get_list('skill_level_master','str','','str',$language_details_single['proficiency_level'])); }
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['myprofile_pro_lvl_blak_opt']?></option> 	
										<?php }
											?>
									</select>
									
								</div>
							</div>
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="row" style="margin-bottom:0px;">
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_<?php echo $language_details_single['id'];?>[]" value="reading" <?php if($language_details_single['reading']=='Yes'){ echo "checked"; } ?>>
													<span class="indicator"></span>
													Read
												</label>
											</div>
										</div>
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_<?php echo $language_details_single['id'];?>[]" value="writing" <?php if($language_details_single['writing']=='Yes'){ echo "checked"; } ?>>
													<span class="indicator"></span>
													Write
												</label>
											</div>
										</div>
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_<?php echo $language_details_single['id'];?>[]" value="speaking" <?php if($language_details_single['speaking']=='Yes'){ echo "checked"; } ?>>
													<span class="indicator"></span>
													Speak
												</label>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12">
								<div style="margin-top: 10px;text-align: center;margin-bottom: 10px;">
									<button class="btn btn-danger deletepart" data-deletepart="lang" data-deleteid="<?php echo $language_details_single['id'];?>" data-deleteid_count="<?php echo $countlooplan;?>" onClick="deletepart('lang','<?php echo $language_details_single['id'];?>','<?php echo $countlooplan;?>')" type="button" id=""> <span class="glyphicon glyphicon-plus"aria-hidden="true"></span>Delete</button>
								</div>
							</div>
						</div>
							<?php }//foreach loop ends
									}//if already added a language
									else
									{ ?>
									<div class="panel-body lang_known_whole" style="padding:10px;"  data-lang_num="1">
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<label class="label-new">Language Known </label>
									<select class="input-select-b select-drop-3 lang_known" id="lang_known_a_1" style="width:100%;" name="lang_known_a_1" data-validation="required" data-langknwon='1' value=''>
                                        
                                    </select>
									
								</div>
								<div class="col-md-6 col-xs-12 col-sm-12">
									
									<label class="label-new">Proficiency Level </label>
									<select name="proficiency_a_1" id="proficiency_a_1" data-profie="1"  class="input-select-b select-drop-3 city">				
										<?php 
										if(isset($skill_lvl_lang) && $skill_lvl_lang !='')
										{
											print_r($skill_lvl_lang);
										}
										else
										{ ?>
											<option value=""><?php echo $custom_lable_arr['myprofile_pro_lvl_blak_opt']?></option> 	
										<?php }
											?>
									</select>
									
								</div>
							</div>
							<div class="row" style="margin-bottom:0px;">
								<div class="col-md-6 col-xs-12 col-sm-12">
									<div class="row" style="margin-bottom:0px;">
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_a_1[]" value="reading">
													<span class="indicator"></span>
													Read
												</label>
											</div>
										</div>
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_a_1[]" value="writing">
													<span class="indicator"></span>
													Write
												</label>
											</div>
										</div>
										<div class="col-md-4 col-xs-4 col-sm-4 margin-top-5">
											<div class="checkbox-new">
												<label class="checkbox-new" style="font-size:15px;">
													<input type="checkbox" name="checkbox_a_1[]" value="speaking">
													<span class="indicator"></span>
													Speak
												</label>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
						</div>
									<?php }?>
									<div class="clear"></div>
										<div class="margin-top-10"></div>
										<?php 
										if(count($language_details) < 11)
										{
										?>
                                        <div class="input-group-btn text-center">
												<button class="btn btn-success" type="button"  id="addmorebutton_lang"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add More Languages</button>
												<div class="margin-bottom-20"></div>
										</div>        
                                        <?php } ?>
							<div class="row margin-top-20" style="margin-bottom:0px;">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<!-- <a href="#" class="btn-job-new block new-save"> Save Settings</a> -->
									<button type="submit" class="btn-job-new block new-save"><i class="fa fa-fw fa-check" aria-hidden="true" ></i> <?php echo $custom_lable_arr['sign_up_emp_1stformupdatebtn']?></button>
											<!-- <button type="button" class="btn btn-default" onClick="viewsection('langknown');"><i class="fa fa-fw fa-times" aria-hidden="true"></i> <?php ////echo $custom_lable_arr['sign_up_emp_1stformcancellbtn']?></button> -->
								</div>
							</div>
							<input type="hidden" name="user_agent" id="user_agent" value="NI-WEB"/>
					</form>