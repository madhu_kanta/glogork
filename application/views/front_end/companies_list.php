<?php $custom_lable_arr = $custom_lable->language; 
$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
?>
<style>
.pagination ul li a[rel="start"] {
    width: 70px;
}
</style>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/staffing.png); background-size:cover;"><!--class="single"-->
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo  $custom_lable_arr['browse_companies_tit']; ?></h2>
			<span><?php echo $custom_lable_arr['browse_companies_subtitle']; ?></span>
		</div>
	</div>
</div>
<!-- Titlebar end
================================================== -->
<div class="clearfix"></div>
<div class="container margin">
	<div class="sixteen columns">
		<h3 class="margin-bottom-25"><?php echo $custom_lable_arr['contac_out_comp_indtit2'];?></h3>
	</div>
	<!--<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home"><span class="glyphicon glyphicon-phone"></span> Web, Software & IT</a></li>
		<li><a data-toggle="tab" href="#menu1"><span class="glyphicon glyphicon-eye-open"></span> Design Art & Multimedia</a></li>
		<li><a data-toggle="tab" href="#menu2"><span class="glyphicon glyphicon-briefcase"></span> Sales & Marketing</a></li>
	</ul>-->
	<!--<div class="tab-content">-->
		<div id="home" class="" style="margin-left: 20px;"><!--tab-pane fade in active-->
			<!--<div class="container">
				<h4><span class="glyphicon glyphicon-phone"></span> Web, Software & IT</h4>
				<br />
			</div>-->
            <div class="clearfix"></div>
            <div id="js_action_msg_div"></div>
			<div id="js_action_emp_div"></div>
            <div id="main_content_ajax">	
				<?php 
                    $this->load->view('front_end/companies_list_ajax');
                ?>
            </div>
		</div>
	<!--</div>-->
</div>
<?php ?>
<?php 
	$this->load->view('front_end/company_logo_list');
?>
<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
									
									<?php
									if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
									{ ?>
									<div class="small-dialog-headline">
									  <i class="fa fa-envelope"></i> <?php echo  $custom_lable_arr['send_msg']; ?>
									</div>
									<div class="clearfix margin-bottom-10"></div>
									<div class="alert alert-danger" id="error_msg" style="display:none" > </div>
									<div class="alert alert-success" id="success_msg" style="display:none" ></div>
										   
									<div class="small-dialog-content">
										<form  method="post"  id="send_msg_form" >
                                        	<div class="form-group">
											<input type="text" name="subject" data-validation="required" placeholder="<?php echo $custom_lable_arr['sub_mes_com']; ?>" value="" />
                                            </div>
                                            <div class="form-group">
											<textarea placeholder="<?php echo $custom_lable_arr['mes_mes_com']; ?>" data-validation="required" name="content" ></textarea>
                                            </div>
											<button class="send" ><i class="fa fa-check"></i> <?php echo $custom_lable_arr['send_msg']; ?></button>
											<input type="hidden" value="NI-WEB" name="user_agent">
											<input type="hidden" value="send_message" name="action">
											<input type="hidden" value="" name="emp_id" id="emp_id_msg">
										</form> 
									</div>
									<?php }
									else
									{ $this->session->set_userdata($return_after_login_url); ?>
									<div class="">
										<div class="alert alert-danger">
											 <?php echo  $custom_lable_arr['please_login']; ?>
										</div>
									</div>
									<?php }
									 ?>
								</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>                                
<script>

$(document).ready(function(e) {
	onpopupopenappend();
	validatemessageform();
if($("#ajax_pagin_ul").length > 0)
{
	load_pagination_code();
}
});
function jobseeker_action(action,action_for,action_id)
{
	<?php 
	if(!$this->common_front_model->get_userid() && $this->common_front_model->get_userid()=='')
	{ 
		$this->session->set_userdata($return_after_login_url);
		
	?>    var message = '<?php echo $this->lang->line('please_login'); ?>';
			if(action_for=='Emp')
			{
					$('#js_action_emp_div').html('');
					$('#js_action_emp_div').slideUp('Slow');
					$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_emp_div').slideDown('Slow');
					set_time_out_msg('js_action_emp_div');
					scroll_to_div('js_action_emp_div',-300)
							
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
				$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				$('#js_action_msg_div').slideDown('Slow');
				set_time_out_msg('js_action_msg_div');
				scroll_to_div('js_action_msg_div',-300)
		    
			}
			return false;
	<?php }
	?>
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&action='+action+'&action_for='+action_for+'&action_id='+action_id+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'job-seeker-action/seeker-action' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if(action_for=='Emp')
			{
				$('#js_action_emp_div').html('');
			    $('#js_action_emp_div').slideUp('Slow');
			}
			else
			{
				$('#js_action_msg_div').html('');
				$('#js_action_msg_div').slideUp('Slow');
			}
			
			if(data.status=='success')
			{   
				var followercount = parseInt($('.follower_count_'+action_id).html());
				if(action=='follow_emp')
				{
					$('.emp_follow_action_'+action_id).html('<span class="glyphicon glyphicon-check"></span> <?php echo $this->lang->line('following'); ?>');
					$('.emp_follow_action_'+action_id).removeAttr('onClick');
					$('.emp_follow_action_'+action_id).attr('onClick','jobseeker_action("unfollow_emp","Emp",'+action_id+')');
					//followercount!='null' && followercount!=null && 
					if(followercount!=='')
					{
						var followercount_incre = followercount + 1;
						$('.follower_count_'+action_id).html(followercount_incre);
					}
					
				}
				if(action=='unfollow_emp')
				{
					$('.emp_follow_action_'+action_id).html('<span class="glyphicon glyphicon-user"></span> <?php echo $this->lang->line('follow'); ?>');
					$('.emp_follow_action_'+action_id).removeAttr('onClick');
					$('.emp_follow_action_'+action_id).attr('onClick','jobseeker_action("follow_emp","Emp",'+action_id+')');
					//followercount!='null' && followercount!=null &&  && typeof followercount!==undefined 
					if(followercount!=='')
					{
							var followercount_decre = followercount - 1;
							$('.follower_count_'+action_id).html(followercount_decre);
					}
				}
				if(action_for=='Emp')
				{
					$('#js_action_emp_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_emp_div').slideDown('Slow');
				   set_time_out_msg('js_action_emp_div');
				   scroll_to_div('js_action_emp_div',-300)
				}
				else
				{
					$('#js_action_msg_div').append('<div role="alert" class="margin-top-10 alert alert-success" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
				   $('#js_action_msg_div').slideDown('Slow');
				   set_time_out_msg('js_action_msg_div');
				   scroll_to_div('js_action_msg_div',-300)
				}
				
			}
			else
			{
				if(action_for=='Emp')
				{
					$('#js_action_emp_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_emp_div').slideDown('Slow');
					set_time_out_msg('js_action_emp_div');
					scroll_to_div('js_action_emp_div',-300)
				}
				else
				{
					$('#js_action_msg_div').html('<div role="alert" class="margin-top-10 alert alert-danger" style="padding:0px 15px;"><p class="margin-top-10 margin-bottom-10" ><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.errmessage+'.&nbsp;&nbsp;&nbsp;&nbsp;</p></div>');
					$('#js_action_msg_div').slideDown('Slow');
					set_time_out_msg('js_action_msg_div');
					scroll_to_div('js_action_msg_div',-300)
				}
			}
		}
	});	
	  hide_comm_mask();
	  return false;
}
function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html(''); $('#'+div_id).removeAttr('class');  }, 4000);
}
function onpopupopenappend()
{
	$( ".myclassclick" ).click(function() {
	  var emp_id = $(this).data("emp_id"); 
	  $( "#emp_id_msg" ).val(emp_id);
	  validatemessageform();
	});
}
function validatemessageform()
{
$.validate({
    form : '#send_msg_form',
    modules : 'security',
    onError : function($form) {
	  $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  scroll_to_div('error_msg',-100);
	  $("#error_msg").html("<?php echo $custom_lable_arr['mandatory_field_msg']; ?>");
	  set_time_out_msg('error_msg');
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").hide();
	  var datastring = $("#send_msg_form").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'job_seeker_action/send_message' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				document.getElementById('send_msg_form').reset();
				scroll_to_div('success_msg',-100);
				set_time_out_msg('success_msg');
			}
			else
			{
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('error_msg',-100);
				set_time_out_msg('error_msg');
			}
		}
	});	
	  hide_comm_mask();
      return false; // Will stop the submission of the form
    }
  });
}
</script>