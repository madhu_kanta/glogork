<?php $custom_lable_arr = $custom_lable->language;$upload_path_profile = $this->common_front_model->fileuploadpaths('js_photos',1);
$return_after_login_url = array('return_after_login_url'=>$_SERVER['REQUEST_URI']);
  ?>
<?php 	
if(isset($jobseeker_data) && $jobseeker_data!='' && is_array($jobseeker_data) && count($jobseeker_data) > 0 )
{ $custom_counter_fordiv = 0 ; ?>
	<div class="row">
    			<?php foreach($jobseeker_data as $jobseeker_data_single)
				{ $custom_counter_fordiv = $custom_counter_fordiv + 1;
				$totalexp = ($this->common_front_model->checkfieldnotnull($jobseeker_data_single['total_experience'])) ? explode("-",$jobseeker_data_single['total_experience']) : "";
				
					/*if($this->common_front_model->get_userid() && $this->common_front_model->get_userid()!='')
					{
						$js_id = $this->common_front_model->get_userid();
						$where_arra_emp = array('js_id'=>$js_id,'emp_id'=>$jobseeker_data_single['id']);
						$jobseeker_access_employer = $this->common_front_model->get_count_data_manual('jobseeker_access_employer',$where_arra_emp,1);
					}*/
				  ?>
				<div class="five columns">
					<div class="panel panel-default box-jobs" style="border-radius:0px;">
						<div class="panel-body padding-top-10" style="min-height:100px;">
							<div class="col-sm-6 col-md-4">
                            <?php  if($jobseeker_data_single['profile_pic_approval'] == 'APPROVED' && $jobseeker_data_single['profile_pic'] != '' && !is_null($jobseeker_data_single['profile_pic']) && file_exists($upload_path_profile.'/'.$jobseeker_data_single['profile_pic']))
				{  ?>
								<img src="<?php echo $base_url ;?>assets/js_photos/<?php echo $jobseeker_data_single['profile_pic'] ;?>" alt="<?php echo $jobseeker_data_single['fullname']?>" class="img-responsive" />
                                <?php }else { ?>
                                <img src="<?php echo $base_url ;?>assets/front_end/images/no-image-found.jpg" alt="<?php echo $jobseeker_data_single['fullname']?>" class="img-responsive" />
                                <?php } ?>
							</div>
						<div class="col-sm-6 col-md-8">
							<small>
								<strong><a target="_blank" href="<?php echo $base_url; ?>share_profile/js_profile/<?php echo base64_encode($jobseeker_data_single['id']);?>"><?php echo ($this->common_front_model->checkfieldnotnull($jobseeker_data_single['fullname'])) ? $jobseeker_data_single['personal_titles'].' '.$jobseeker_data_single['fullname'] : $custom_lable_arr['notavilablevar'] ; ?></a></strong>
								<p class="small-11"><i class="fa fa-briefcase" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo ($this->common_front_model->checkfieldnotnull($jobseeker_data_single['industry']) && $this->common_front_model->checkfieldnotnull($jobseeker_data_single['industries_name'])) ? $jobseeker_data_single['industries_name'] : $custom_lable_arr['notavilablevar'] ; ?></p>
								<p class="small-11"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo ($this->common_front_model->checkfieldnotnull($jobseeker_data_single['city']) && $this->common_front_model->checkfieldnotnull($jobseeker_data_single['city_name'])) ? $jobseeker_data_single['city_name'] : $custom_lable_arr['notavilablevar'] ; ?></p>
							</small>
						</div>
                        
							<div class="col-sm-12 margin-top-10">
                            	<p class="small-11"><i>Total Experience :</i></p>
								<p class="small-11 justify" style="color:black;">
								<?php 
								if($totalexp!='')
								{
									if(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0'){ echo $totalexp[0].' '.$custom_lable_arr['year'] ; } elseif(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]=='0') { echo "Fresher"; }
									if(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!='0' ){ echo $totalexp[1].' '.$custom_lable_arr['month'] ; } 
								}
								elseif($totalexp=='00-00' || $totalexp=='0-0')
								{
									echo "Fresher";
								}
								else
								{
									echo  $custom_lable_arr['notavilablevar'];
								}
								?>
                                </p>
								<p class="small-12 margin-top-10"><i><span class="glyphicon glyphicon-time"></span> <?php echo $custom_lable_arr['recruiters_page_act_since']; ?>  <?php echo ($jobseeker_data_single['register_date']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($jobseeker_data_single['register_date'])) ? $this->common_front_model->displayDate($jobseeker_data_single['register_date'],'F j, Y') : $custom_lable_arr['notavilablevar']; ?></i></p>
							</div>
							<div class="col-sm-12">
                            	<?php 
									$follow_action = 'follow_emp';
									$follow_icon = '<span class="glyphicon glyphicon-user"></span>';
									$follow_text = $custom_lable_arr['follow'];
								?>
                            	<?php /* if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
								{ 
									if(isset($jobseeker_access_employer) && $jobseeker_access_employer!='' && count($jobseeker_access_employer) > 0)
									{ 
										if($jobseeker_access_employer['is_follow']=='Yes')
										{
											$follow_action = 'unfollow_emp';
											$follow_icon = '<span class="glyphicon glyphicon-check"></span>';
											$follow_text = $custom_lable_arr['following'];
										}
									}
								 ?>
								<button type="button" class="btn btn-sm bnt-top-10 emp_follow_action_<?php echo $jobseeker_data_single['id']; ?>"  OnClick="return jobseeker_action('<?php echo $follow_action; ?>','Emp','<?php echo $jobseeker_data_single['id']; ?>');"><i class="fa fa-plus"></i> <?php echo $follow_text; ?></button>
                                <?php } */ ?>
								<?php
								/*
								if(!$this->common_front_model->get_empid() && $this->common_front_model->get_empid()=='')
								{ ?>
								<!--<a href="#small-dialog" class="popup-with-zoom-anim button"><i class="fa fa-lg fa-envelope"></i><?php echo  $custom_lable_arr['send_msg']; ?></a>-->
                                <h6 class="pull-right small-13 myclassclick" data-emp_id="<?php echo $jobseeker_data_single['id']; ?>" style="margin-top:15px;"><a href="#small-dialog" class="popup-with-zoom-anim "><i class="fa fa-envelope" style="font-size:16px;"></i> <?php echo  $custom_lable_arr['send_msg']; ?></a></h6>								
								<?php } */
								?>
							</div>
						</div>
					</div>
				</div>
                <?php 
				if($custom_counter_fordiv % 3 == 0)
				{ ?>
					</div>
                    <div class="row">
				<?php }
				} ?>
	</div>
<?php echo $this->common_front_model->rander_pagination('browse-job-seekers/index',$jobseekers_count,$limit_per_page); ?>
<?php 
}
else
{ ?>
<div class="categories-group">
	<div class="container">
	    <div class="col-md-12 col-sm-12 col-xs-12">
        	<div class="list-group">
				<img class="img-responsive" src="<?php echo $base_url; ?>assets/front_end/images/no-data-found.jpg" />
            </div>
        </div>
    </div>    
</div>    
<?php 
}
?>
<input type="hidden" id="hash_tocken_id_temp" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
<?php if($this->input->post('is_ajax') && $this->input->post('is_ajax')==1){ ?>
$(document).ready(function(e) {
	onpopupopenappend();
	validatemessageform();
});	
 $('.popup-with-zoom-anim').magnificPopup({
                          type: 'inline'
                     });
<?php } ?>					 
/*$(document).ready(function(e) {*/
    /*$.magnificPopup.open({
	items: {
		src: '#small-dialog',
	},
		type: 'inline'
	});*/
/*});*/
</script>