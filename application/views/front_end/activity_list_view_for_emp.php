<?php 
$custom_lable_array = $custom_lable->language;
if(isset($get_list) &&  $get_list!='')
{
		$title= $custom_lable_array['like_emp_list_title_for_emp'];
		if($get_list=='block_emp')
		{
			$title= $custom_lable_array['block_emp_list_title_for_emp'];
		}
		else if($get_list=='follow_emp')
		{
			$title= $custom_lable_array['follow_emp_list_title_for_emp'];
		}
	
}
?>
<div class="clearfix"></div>
<div id="titlebar" class="photo-bg single" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/alert.jpg); background-size:cover;">
	<div class="container">
		<div class="sixteen columns">
			<h2><i class="fa fa-list" aria-hidden="true"></i> <?php echo $title; ?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li> <?php echo $custom_lable_array['you_are_here']; ?> :</li>
					<li><a href="<?php echo $base_url; ?>"><?php echo $custom_lable_array['home_lbl']; ?></a></li>
                    <li><?php echo $custom_lable_array['js_activity_lbl']; ?></li>
					<li><?php echo $title; ?></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div class="clearfix"></div>
	<!--<div class="sixteen columns">-->
		<!--<p class="margin-bottom-25"> Your job alerts are shown below.</p>
		<div class="margin-top-30">
			<div class="pull-right">
				<span class="text-muted"><b>1</b>–<b>50</b> of <b>277</b></span>
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</button>
					<button type="button" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</button>
				</div>
			</div>
		</div>
		<br />
		<hr>
		-->
        <div class="col-md-3 col-sm-12 col-xs-12">
        	<?php include_once("employer_left_menu.php"); ?>
		</div>
        <div class="col-md-9 col-sm-12 col-xs-12">
        	<div id="js_action_msg_div"></div> 
        	<div id="main_content_ajax">
			<?php  
				   $this->load->view('front_end/page_part/activity_list_result_view_for_emp',$this->data);
		    ?>
           
       </div>
        </div>
		
        <div class="col-md-9 col-sm-12 col-xs-12">
               <div id="view_js_details">
                </div>
         </div>
       <div class="clearfix"></div>  
        	
	
		<!--<a href="#small-dialog" class="popup-with-zoom-anim button margin-top-20 margin-bottom-30"><i class="fa fa-plus"></i>Add Alert</a>
		<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
			<div class="small-dialog-headline">
				<i class="fa fa-briefcase"></i> Add Alert
			</div>
			<div class="small-dialog-content">
				<form action="#" method="post" class="login">
					<input type="text" placeholder="Alert Name" value=""/>
					<input type="text" placeholder="Keyword" value=""/>
					<input type="text" placeholder="Location" value=""/>
					<select data-placeholder="Email Frequency" class="chosen-select-no-single">
						<option value="">Email Frequency</option>
						<option value="1">Daily</option>
						<option value="2">Weekly</option>
						<option value="3">Fortnightly</option>
					</select>
					<div class="clearfix"></div>
					<div class="margin-top-15"></div>
					<select data-placeholder="Job Type" class="chosen-select" multiple>
						<option value="1">Full-Time</option>
						<option value="2">Part-Time</option>
						<option value="3">Internship</option>
						<option value="4">Freelance</option>
						<option value="5">Temporary</option>
					</select>
					<button class="send margin-top-20"><i class="fa fa-check"></i> Save Alert</button>
				</form>
			</div>
		</div>-->
	<!--</div>-->
<script>

$(document).ready(function(e) {
	$('#main_content_ajax').show();
	if($("#ajax_pagin_ul").length > 0)
	{   
		load_pagination_code();
	}

});

function close_js_details()
{
	$('#view_js_details').hide();
	$('#main_content_ajax').show();	 
	scroll_to_div('main_content_ajax',-100); 
}

function view_js_details(js_id)
{  

	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&js_id='+js_id+'&user_agent=NI-WEB';
	$.ajax({	
		url : "<?php echo $base_url.'employer-profile/view-js-details' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			    $("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
				$('#view_js_details').show();
				$('#view_js_details').html(data);	  
				$('#main_content_ajax').hide();	
				 hide_comm_mask();  
	    }
	});	
	 
}

function close_model()
{
	$('.mfp-close').trigger('click');
}
function set_time_out_msg(div_id)
{
	setTimeout(function(){ $('#'+div_id).html('');  }, 8000);
	
}


</script>