<style>
	.new-form-control {
    width:100% !important;
	border-radius: 7px;
	position: relative;
    right: 39px;
	}
	.container-new h2 {
    font-size: 36px;
    color: #fff;
    margin-bottom: 30px;
    letter-spacing: -1px;
	}
	.browse-jobs {
    color: #dad8d8;
    margin-top: -26px;
	position: relative;
    right: 33px;
	}
	.container-new input {
    font-weight: normal;
	font-size: 13px;
	width: 100% !important;
    border-radius: 7px;
    position: relative;
    right: 39px;
	padding: 16px 15px !important;
	}
	
	

</style>

<?php $upload_path_profile ='./assets/js_photos';
$custom_lable_arr = $custom_lable->language;
$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode("-",$user_data['total_experience']) : "";
 ?>
 <div id="banner" class="with-transparent-header parallax background" style="margin-top:0px !important;height: 164px;background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container" style="margin-top:40px;">
		<div class="sixteen columns">

		 <?php if(!$this->common_front_model->checkLoginfrontempl())
		 {
		 ?>
			<div class="container-new">
				<!-- Form -->
				<!--<h2 class="bounceInDown animated">Find job</h2>-->
                <div class="bounceInDown animated">
					<?php $this->load->view('front_end/search_box');?>	
				</div>
				<!-- Browse Jobs -->
				<!--<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="browse-jobs bounceInUp animated">
					Browse job offers by <a href="<?php echo $base_url; ?>browse-industries"> Industry</a> or <a href="<?php echo $base_url; ?>browse-location">Location</a>
				</div>
				</div>
                </div>-->
				<!-- Announce -->
				<div class="announce bounceInUp animated">
					<?php //echo $custom_lable_arr['index_page_ban_tit'];?>
				</div>
			</div>
       <?php } else { ?>
			<div class="search-container">
			<h2 class="bounceInDown animated">Welcome <?php echo $this->session->userdata['jobportal_employer']['fullname'];?></h2>
			</div>
<?php } ?>

		</div>
	</div>
</div>
<!-- <?php //$upload_path_profile ='./assets/js_photos';
//$custom_lable_arr = $custom_lable->language;
//$totalexp = ($this->common_front_model->checkfieldnotnull($user_data['total_experience'])) ? explode("-",$user_data['total_experience']) : "";
 ?>
<div class="profile-head" style="background-image: url(<?php //echo $base_url; ?>assets/front_end/images/banner/pricing-hero.png);background-repeat: no-repeat;background-size: cover;min-width: 100%;background-position: center center;">
			<div class="margin-top-10"></div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center col-md-offset-2">
				<img id="headmenuprofilediv" src="<?php //if($user_data['profile_pic'] != '' && !is_null($user_data['profile_pic']) && file_exists($upload_path_profile.'/'.$user_data['profile_pic'])){ echo $base_url.'assets/js_photos/'.$user_data['profile_pic'];  }else{?><?php //echo $base_url; ?>assets/front_end/images/demoprofileimge2.png<?php //} ?>" class="img-responsive" />
			<h6><?php //echo $user_data['personal_titles'].' '.$user_data['fullname']; ?></h6>
				<div class="margin-top-10"></div>
				<div class="dropdown" style="color:black;">
					<button class="btn btn-warning text-center dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Visibility <span class="caret"></span></button>
					<ul class="dropdown-menu" style="margin:auto;">
						<li class=""><a href="javaScript:void(0);"  class="text-left foreditfontcolor" onClick="change_visibility('public');"><span id="public_sing" class="<?php //if($user_data['profile_visibility']=='No'){ echo "glyphicon glyphicon-ok" ; }else{ echo "glyphicon glyphicon-remove" ; } ?>"></span> I would like to UN-hide my profile</a></li>
						<li class="text-left"><a class="foreditfontcolor" href="javaScript:void(0);" onClick="change_visibility('private');"><span id="private_sing" class=" <?php //if($user_data['profile_visibility']=='Yes'){echo "glyphicon glyphicon-ok" ; }else{ echo "glyphicon glyphicon-remove" ; } ?>"></span> I would like to hide my profile</a></li>
					</ul>
                    <div class="alert alert-danger alert-dismissable left-0" id="messageprivacy" style="position: fixed; left: 30%; top: 30%; z-index: 9999; opacity: 1; display: none;" ></div>
					<div class="alert alert-success left-0" id="success_msgprivacy" style="position: fixed; left: 30%; top: 30%; z-index: 9999; opacity: 1; display: none;" ></div>
				</div>
				<div class="margin-top-20"></div>
				<div class="row col-md-offset-2">
					<div class="col-xs-12 social-btns text-center">
						<ul class="social-icons" >
                        	<?php  //if($this->common_front_model->checkfieldnotnull($user_data['facebook_url'])){ ?>
							<li data-toggle="tooltip" title="Your facebook profile link"><a class="facebook" target="_blank" href="<?php //echo $user_data['facebook_url']; ?>"><i class="icon-facebook" style="width:10%;"></i></a></li>
                             <?php //} ?>
                             <?php  //if($this->common_front_model->checkfieldnotnull($user_data['twitter_url'])){ ?>
							<li data-toggle="tooltip" title="Your twitter profile link"><a class="twitter" target="_blank" href="<?php //echo $user_data['twitter_url']; ?>"><i class="icon-twitter"></i></a></li>
                            <?php //} ?>
                            <?php  //if($this->common_front_model->checkfieldnotnull($user_data['gplus_url'])){ ?>
							<li data-toggle="tooltip" title="Your gplus profile link"><a class="gplus" target="_blank" href="<?php //echo $user_data['gplus_url']; ?>"><i class="icon-gplus"></i></a></li>
                            <?php //} ?>
                            <?php  //if($this->common_front_model->checkfieldnotnull($user_data['linkedin_url'])){ ?>
							<li data-toggle="tooltip" title="Your linkdin profile link"><a class="linkedin" target="_blank" href="<?php //echo $user_data['linkedin_url']; ?>"><i class="icon-linkedin"></i></a></li>
                            <?php //} ?>
						</ul>	
					</div>
				</div>
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12">
				<ul class="list-group">
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your full name"><span class="glyphicon glyphicon-user"></span> <?php //echo  $user_data['personal_titles'].' '.ucwords($user_data['fullname']); ?></li>
                    <?php  //if($this->common_front_model->checkfieldnotnull($user_data['job_role']) && $this->common_front_model->checkfieldnotnull($user_data['role_name'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;" data-toggle="tooltip" title="Your job role"><span class="glyphicon glyphicon-briefcase"></span> <?php //echo  ucwords($user_data['role_name']); ?></li>
                    <?php //} ?>
                    <?php  //if($this->common_front_model->checkfieldnotnull($user_data['total_experience'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"><span class="glyphicon glyphicon-briefcase" data-toggle="tooltip" title="Your total experience"></span> <?php 
					//if($totalexp!='')
														//{
														// 	if(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]!='0'){ echo $totalexp[0].' '.$custom_lable_arr['year'].' ' ; } elseif(isset($totalexp[0]) && $totalexp[0]!='' && $totalexp[0]=='0') { echo ""; }
														// 	if(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[1]!='0' ){ echo $totalexp[1].' '.$custom_lable_arr['month'] ; } elseif(isset($totalexp[1]) && $totalexp[1]!='' && $totalexp[0]=='0') { echo "Fresher" ; }
														// }
														// elseif($totalexp=='00-00' || $totalexp=='0-0')
														// {
														// 	echo "Fresher";
														// }
														// else
														// {
														// 	echo  $custom_lable_arr['notavilablevar'];
														// } ?></li>
                    <?php //} ?>
                    <?php  //if($user_data['country']!='0' && $this->common_front_model->checkfieldnotnull($user_data['country']) && $this->common_front_model->checkfieldnotnull($user_data['country_name'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"><span class="glyphicon glyphicon-map-marker" data-toggle="tooltip" title="Your country"></span> <?php //echo  $user_data['country_name']; ?></li>
                    <?php //} ?>
                    <?php  //if($this->common_front_model->checkfieldnotnull($user_data['address'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"  data-toggle="tooltip" title="Your address"><span class="glyphicon glyphicon-home"></span> <?php //echo  $user_data['address']; ?></li>
                    <?php //} ?>
                    <?php  //if($this->common_front_model->checkfieldnotnull($user_data['mobile'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"  data-toggle="tooltip" title="Your mobile"><span class="glyphicon glyphicon-phone"></span><?php //echo  $user_data['mobile']; ?></li>
                    <?php //} ?>
                    <?php  //if($this->common_front_model->checkfieldnotnull($user_data['email'])){ ?>
					<li class="list-group-item" style="color: black;padding: 8px 0 8px 25px;"  data-toggle="tooltip" title="Your email"><span class="glyphicon glyphicon-envelope"></span><a href="javascript:void(0);"  style="color: black;text-transform:lowercase;"><?php //echo  $user_data['email']; ?></a></li>
                    <?php // } ?>
				</ul>
			</div>
		</div> -->