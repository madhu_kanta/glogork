<?php
//Include Google client library 
include_once 'src/Google_Client.php';
include_once 'src/contrib/Google_Oauth2Service.php';

/*
 * Configuration and setup Google API
 */
$gplus_detail = '';
if(isset($this->data['social_access_detail']['1']) && $this->data['social_access_detail']['1']!='')
{
	$gplus_detail = $this->data['social_access_detail']['1'] ;
}
$clientId = $gplus_detail['client_key'];//'18769205332-3pdbdf0kbu5oj8q8fusd59tqq8s6vf9d.apps.googleusercontent.com'; //Google client ID
//'http://jobportal.trialme.in/sign_up/gplus_login_signup

$clientSecret = $gplus_detail['client_secret']; //'m2ef2dviPoL8N_CL4f59FK_f'; //Google client secret
$redirectURL = $base_url.'sign_up/gplus_login_signup'; //Callback URL 
/*Change $base_url after success of testing in $redirectURL*/
//Call Google API
$gClient = new Google_Client();
$gClient->setApplicationName('Login to Job portal');
$gClient->setClientId($clientId);
$gClient->setClientSecret($clientSecret);
$gClient->setRedirectUri($redirectURL);

$google_oauthV2 = new Google_Oauth2Service($gClient);
?>