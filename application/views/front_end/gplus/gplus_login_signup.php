<?php
include_once 'gpConfig.php';
//error_reporting(E_ALL);
if(isset($_GET['code'])){
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) {
	//Get user profile data from google
	$gpUserProfile = $google_oauthV2->userinfo->get();
	

	//Insert or update user data to the database
    $gpUserData = array(
        'oauth_provider'=> 'google',
        'oauth_uid'     => $gpUserProfile['id'],
        'first_name'    => $gpUserProfile['given_name'],
        'last_name'     => $gpUserProfile['family_name'],
        'email'         => $gpUserProfile['email'],
        'gender'        => $gpUserProfile['gender'],
        'locale'        => $gpUserProfile['locale'],
        'picture'       => $gpUserProfile['picture'],
        'link'          => $gpUserProfile['link']
    );
  
	//print_r($gpUserData);
	$this->db->where('gplus_id', $gpUserProfile['id']);
	$this->db->limit(1);
	$query = $this->db->get('jobseeker');
		
	if($query->num_rows() > 0)
	{
			     $this->session->set_flashdata('user_log_out', $this->lang->line('gplus_already_registred'));
				 if($gpUserProfile['email']!='')
				   {
					$chkemail=" or email='".$gpUserProfile['email']."'";
				   }
				   else
				   {
					$chkemail= "";   
				   }
	             $where = "(gplus_id='".$gpUserProfile['id']."' $chkemail) AND is_deleted!='Yes'";
				 $this->db->where($where);
	   			 $query = $this->db->get('jobseeker');
	   			 $reg_data = $query->row_array();
				 //print_r($reg_data); exit();
				 if(isset($reg_data['status']) && $reg_data['status']!='UNAPPROVED')
				 {
					 
					 $user_data_array = array(
							 'user_id'	=> $reg_data['id'],
							 'full_name'	=> $reg_data['fullname'],
							 'email'		=> $reg_data['email'],
							 'gender'	=> $reg_data['gender'],
							 );
					$this->session->set_userdata('jobportal_user', $user_data_array);			 
					$email = $reg_data['email'];
					$login_dt = $this->common_front_model->getCurrentDate(); /* must save date and time on datetime field*/
					$this->db->set('last_login', $login_dt);
					$this->db->where('id', $reg_data['id']);
					$this->db->limit(1);
					$this->db->update('jobseeker');					
					redirect($this->base_url.'my-profile');
									
				  }
				  else
				  {
					 $this->session->set_flashdata('user_log_out', $this->lang->line('inactive_login_account'));
					 $this->session->flashdata('user_log_out'); 
					 redirect($base_url.'sign-up');
				  }
          
				 exit();
			 }
	$url = $gpUserProfile['picture'];
	$data = file_get_contents($url);
	$fileName = time().'.jpg';
	$file = fopen('/home/trialing/public_html/jobportal/assets/js_photos/'.$fileName, 'w+');
	$fl=fputs($file, $data);	
	fclose($file);//
//echo $fileName;exit();	
	$user_gp_array = array(
							'gplus_id'	=> $gpUserProfile['id'],
							'gp_first_name'	=> $gpUserProfile['given_name'],
							'gp_last_name'	=>$gpUserProfile['family_name'],
							'gp_email'	=> $gpUserProfile['email'],
							'gp_gender'	=> $gpUserProfile['gender'],
							'gp_image'	=> $gpUserProfile['picture'],
							'gp_image_name'	=> $fileName,
						);
   if($this->session->userdata('js_fb_data'))
		{
			$this->session->unset_userdata('js_fb_data');
		}						
   $this->session->set_userdata('js_gplus_data', $user_gp_array);
  redirect($base_url.'sign-up');

} 
else
{
	$this->session->set_flashdata('user_log_error', $this->lang->line('gplus_error'));
	redirect($base_url.'sign-up');
}
?>