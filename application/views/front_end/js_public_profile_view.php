<?php
$upload_path_profile_cmp = $this->common_front_model->fileuploadpaths('js_photos',1);
$custom_lable_arr = $custom_lable->language;
$return_data_plan_emp = 'No';
?>
<?php if($this->common_front_model->get_empid() && $this->common_front_model->get_empid()!='')
{
	$emp_id_stored_plan = $this->common_front_model->get_empid();
	$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_id_stored_plan,'employer','contacts');
}
if(isset($this->common_model->is_demo_mode) && $this->common_model->is_demo_mode == 1)
{
	if(isset($js_details['mobile']) && $js_details['mobile'] !='')
	{
		$js_details['mobile'] = $this->common_model->mobile_disp;
	}
	if(isset($js_details['email']) && $js_details['email'] !='')
	{
		$js_details['email'] = $this->common_model->emial_disp;
	}
}
?>
<div class="clearfix"></div>
<div id="titlebar" class="photo-bg" style="background: url(<?php echo $base_url; ?>assets/front_end/images/banner/public-profile-img.jpg); background-size:cover;">
	<div class="container">
		<div class="ten columns">
			<h2><i class="fa fa-file-text"></i> <?php echo $custom_lable_arr['job_seeker_profile']; ?> </h2>
			<!--<span>Resume Viewed: 92 Employers</span>-->
		</div>
	</div>
</div>
<div class="margin-top-40"></div>
<div id="container-fluid">
    <div class="col-md-3 col-sm-3 col-xs-12">
		<div class="row panel panel-default" style="padding:0px;border-radius:0px;">
			<div class="panel-body" style="padding:0px;">

                <?php if($js_details!='' && $js_details['profile_pic'] != ''  && $js_details['profile_pic_approval'] == 'APPROVED' && !is_null($js_details['profile_pic']) && file_exists($upload_path_profile_cmp.'/'.$js_details['profile_pic']))
				{ echo '<img src="'.$base_url.'assets/js_photos/'.$js_details['profile_pic'].'"   style="width:100%" alt="" />'; ?><?php }else{ echo '<img src="'.$base_url.'assets/front_end/images/no-image-found.jpg"   style="width:100%" alt="" />'; } ?>

				<div class="text-center">
					<div class="col-md-8 col-sm-12 col-xs-8">
						<h3><?php echo $js_details['personal_titles'] .' '. $js_details['fullname']; ?></h3>
					</div>

				</div>
			</div>
			<hr>
			<div class="panel-body" style="margin-top:-10px;">
				<div class="row col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
					<span><i class="glyphicon glyphicon-briefcase"></i><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['role_name'])) ? $js_details['role_name'] : "Not Available";?></span>
<?php
if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
{
	if($return_data_plan_emp == 'Yes')
	{
		 ?>
					<hr class="hr-margin">
					<span><i class="glyphicon glyphicon-home"></i><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['country_name'])) ? $js_details['country_name'] : "Not Available";?> , <?php echo ( $this->common_front_model->checkfieldnotnull($js_details['city_name'])) ? $js_details['city_name'] : "Not Available";?> </span>
					<hr class="hr-margin">
					<span><i class="glyphicon glyphicon-phone"></i><?php if($js_details['mobile']!='')
						{ ?>
					<button onClick="return unlock_contact('<?php echo base64_encode($js_details['mobile']);/*echo $js_details['resume_file'];*/ ?>','<?php echo base64_encode($js_details['id']); ?>');" class="SF-Pro-Display-Regular">Unlock Contact </button>
                   <?php  } ?></span>
					<hr class="hr-margin">
					<span><i class="glyphicon glyphicon-envelope"></i><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['email'])) ? $js_details['email'] : "Not Available";?></span>
<?php
	}//if plan is valid
} ?>
					<hr class="hr-margin">
					<span><i class="glyphicon glyphicon-briefcase"></i><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['total_experience'])) ? $this->common_front_model->display_exp_salary($js_details['total_experience'],'experience') : "Not Available";?></span>
				</div>
				<hr>
				<div class="row col-md-12 col-sm-12 col-xs-12 text-center margin-bottom-20">
					<div class="row margin-bottom-10 text-center">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-primary btn-twitter box-shadow-radius" title="<?php echo $custom_lable_arr['share_twitter']; ?>" onclick="window.open('http://twitter.com/home?status=<?php echo urlencode($base_url.'share-profile/js-profile/'.base64_encode($js_details['id'])); ?>')" data-toggle="tooltip" ><i class="fa fa-twitter"></i></button>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-danger box-shadow-radius"   rel="publisher" data-toggle="tooltip" title="<?php echo $custom_lable_arr['share_gplus']; ?>" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode($base_url.'share-profile/js-profile/'.base64_encode($js_details['id']))?>')"><i class="fa fa-google-plus"></i></button>
						</div>

                        <?php
						$js_photo  = '';
						if(isset($js_details['profile_pic']) && $js_details['profile_pic_approval']=='APPROVED')
							{
								$js_photo = $base_url.'assets/js_photos/'.$js_details['profile_pic'];
							}
						?>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-primary box-shadow-radius" rel="publisher" title="<?php echo $custom_lable_arr['share_facebook']; ?>" data-toggle="tooltip" onClick=         "window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($base_url.'share-profile/js-profile/'.base64_encode($js_details['id'])); ?>&amp;&p[images][0]=<?php echo urlencode($js_photo); ?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"><i class="fa fa-facebook"></i></button>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-info box-shadow-radius" rel="linkedin"  data-toggle="tooltip"  title="<?php echo $custom_lable_arr['share_linkedin']; ?>" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=	<?php  echo $base_url.'share-profile/js-profile/'.base64_encode($js_details['id']); ?>&title=<?php echo urlencode($js_details['fullname']); ?>&summary=<?php echo urlencode($js_details['fullname']); ?>&source=<?php echo urlencode($js_details['fullname']); ?>')"><i class="fa fa-linkedin"></i></button>
						</div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
							<button class="btn btn-danger box-shadow-radius" title="<?php echo $custom_lable_arr['share_pinterest']; ?>" onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php echo urlencode($base_url.'share-profile/js-profile/'.base64_encode($js_details['id'])); ?>	&media=<?php echo urlencode($js_photo); ?>&description=<?php echo urlencode($js_details['fullname']); ?>')" data-toggle="tooltip" ><i class="fa fa-pinterest-square"></i></button>
						</div>
					</div>
				</div>
				<!--<div class="col-md-12 col-sm-12 col-xs-12 text-center margin-bottom-20">
					<div class="row rating-block text-center">
					<hr>
						<h4>Average user rating</h4>
						<h2 class="bold padding-bottom-7">4.3 <small>/ 5</small></h2>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
					</div>
				</div>-->
			</div>
		</div><br />
    </div>
	<!--<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="panel panel-default text-center" style="padding:10px;border-radius:0px;">
			<div class="user-rating col-md-4 col-sm-6 col-xs-12 text-center">
				<h5>Rating breakdown</h5>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 100%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">1</div>
				</div>
				<div class="pull-left">
					<div class="pull-left">
						<div>4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">1</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<div class="progress" style="height:9px; margin:8px 0;">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
							<span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
					</div>
					<div class="pull-right" style="margin-left:10px;">0</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center" style="border-right: 1px dotted grey;border-left: 1px dotted grey;">
				<h5><i class="fa fa-asterisk"></i> Skills</h5>
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:70%"> 70% <span class="small-13">HTML</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> 40% <span class="small-13">CSS</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:60%"> 60% <span class="small-13">Javascript</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:80%"> 80% <span class="small-13">Jquery</span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 text-center">
				<h4><i class="fa fa-globe"></i> Languages</h4>
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:80%"> 80% <span class="small-13">English</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> 40% <span class="small-13">Hindi</span>
					</div>
				</div>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:88%"> 88% <span class="small-13">Gujarati</span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>-->
    <div class="col-md-9 col-sm-9 col-xs-12">
		<div data-spy="scroll" class="panel panel-default" style="padding:10px;border-radius:0px;">
			<div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_default_1" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> <?php echo $custom_lable_arr['job_seeker_share_profile_about']; ?></a>
					</li>
					<li>
						<a href="#tab_default_2" data-toggle="tab"><i class="fa fa-lg fa-graduation-cap"></i> <?php echo $custom_lable_arr['job_seeker_share_profile_education']; ?></a>
					</li>

					<li>
						<a href="#tab_default_4" data-toggle="tab"><span class="glyphicon glyphicon-send"></span> <?php echo $custom_lable_arr['job_seeker_share_profile_work_exp']; ?></a>
					</li>
                    <?php if($js_details['resume_file']!='' && $js_details['resume_verification']=='APPROVED')
						{ ?>
					<button onClick="return download_resume('<?php echo base64_encode($js_details['resume_file']);/*echo $js_details['resume_file'];*/ ?>','<?php echo base64_encode($js_details['id']); ?>');" class="btn btn-sm btn-primary pull-right margin-top-5"><i class="fa fa-file-text"></i> <?php echo $custom_lable_arr['job_seeker_share_profile_download_resume']; ?> </button>
                   <?php  } ?>
				</ul>
				<div class="clearfix"></div>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab_default_1">
						<div class="col-md-12 col-sm-12 col-xs-12 margin-top-10">
							<h4> <?php echo $custom_lable_arr['share_profile_js_info_lbl']; ?>:</h4>
							<hr class="hr-dotted">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">
									<tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><span class="glyphicon glyphicon-user"></span><?php echo $custom_lable_arr['share_profile_js_name_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['fullname'])) ? $js_details['personal_titles'] .' '. $js_details['fullname'] : "Not Available";?></td>
									</tr>
                                    <?php
									if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
									{
										if($return_data_plan_emp == 'Yes')
										{
											 ?>
									<tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-envelope"></span> <?php echo $custom_lable_arr['share_profile_js_email_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['email'])) ? $js_details['email'] : "Not Available";?></td>
									</tr>
                                    <tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-phone"></span><?php echo $custom_lable_arr['share_profile_js_mobile_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php if($js_details['mobile']!='')
						{ ?>
					<button onClick="return unlock_contact('<?php echo base64_encode($js_details['mobile']);/*echo $js_details['resume_file'];*/ ?>','<?php echo base64_encode($js_details['id']); ?>');" class="SF-Pro-Display-Regular">Unlock Contact </button>
                   <?php  } ?></td>
									</tr>
                                    <?php
										}//if plan is valid
									} ?>
									<tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-calendar"></span> <?php echo $custom_lable_arr['share_profile_js_birth_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ($js_details['birthdate']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($js_details['birthdate'])) ? $this->common_front_model->displayDate($js_details['birthdate'],'Y-m-d'): "Not Available";?></td>
									</tr>
                                    <?php
									if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
									{
										if($return_data_plan_emp == 'Yes')
										{
											 ?>
									<tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-home"></span> <?php echo $custom_lable_arr['share_profile_js_address_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['address'])) ? $js_details['address'] : "Not Available";?></td>
									</tr>
                                    <?php
										}//if plan is valid
									} ?>
									<tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-home"></span> <?php echo $custom_lable_arr['share_profile_js_country_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['country_name'])) ? $js_details['country_name'] : "Not Available";?></td>
									</tr>
                                    <?php
									if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
									{
										if($return_data_plan_emp == 'Yes')
										{
											 ?>
									<tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-home"></span> <?php echo $custom_lable_arr['share_profile_js_city_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['city_name'])) ? $js_details['city_name'] : "Not Available";?></td>
									</tr>
                                    <tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-link"></span> <?php echo $custom_lable_arr['share_profile_js_website_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['website'])) ? $js_details['website'] : "Not Available";?></td>
									</tr>
                                    <?php
										}//if plan is valid
									} ?>


                                    <tr>
										<td class="col-sm-6 col-xs-6"><span class="glyphicon glyphicon-briefcase"></span> <?php echo $custom_lable_arr['share_profile_js_pstn_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['role_name'])) ? $js_details['role_name'] : "Not Available";?></td>
									</tr>
									<tr>
										<td class="col-sm-6 col-xs-6"></td>
										<td class="col-sm-6 col-xs-6"></td>
									</tr>
								</tbody>
							</table>
						</div>

                         <div class="col-md-12 col-sm-12 col-xs-12 margin-top-10">
							<h4> <?php echo $custom_lable_arr['share_profile_js_work_area_lbl']; ?> :</h4>

                            <hr class="hr-dotted">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">


                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_arr['share_profile_js_skill_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;">

                                       <?php
									   if($js_details['key_skill'] !='')
									   {
												$skill_keyword = explode(',',$js_details['key_skill']);
												foreach($skill_keyword as $skill)
												{

													echo ' <li style="list-style:none;"><span class="glyphicon glyphicon-triangle-right"></span> <a href="#">'.$skill.'</a></li>';
												}
									   }
									   else
									   {
										   echo "Not available";
									   }
									?>

                                        </td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_arr['share_profile_ind_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['industries_name'])) ? $js_details['industries_name'] : "Not Available";?></td>
									</tr>
									<tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-envelope"></span>--><?php echo $custom_lable_arr['share_profile_fn_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['functional_name'])) ? $js_details['functional_name'] : "Not Available";?></td>
									</tr>
                                    <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-phone"></span>--><?php echo $custom_lable_arr['share_profile_role_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['role_name'])) ? $js_details['role_name'] : "Not Available";?></td>
									</tr>

                                     <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-phone"></span>--><?php echo $custom_lable_arr['share_profile_desire_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['desire_job_type'])) ? $js_details['desire_job_type'] : "Not Available";?></td>
									</tr>

                                     <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-phone"></span>--> <?php echo $custom_lable_arr['share_profile_emp_type_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['employment_type'])) ? $js_details['employment_type'] : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-phone"></span>--><?php echo $custom_lable_arr['share_profile_p_s_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6"><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['prefered_shift'])) ? $js_details['prefered_shift'] : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-globe"></span>--> <?php echo $custom_lable_arr['share_profile_exp_salary_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6">
										<?php echo ( $this->common_front_model->checkfieldnotnull($js_details['ex_annual_salary_name'])) ? $js_details['ex_annual_salary_name'] : "Not Available";?>

										<?php /*echo ( $this->common_front_model->checkfieldnotnull($js_details['expected_annual_salary'])) ? $js_details['exp_salary_currency_type'] .' '. $this->common_front_model->display_exp_salary($js_details['expected_annual_salary']) : "Not Available"; */?></td>
									</tr>
									<tr>
										<td class="col-sm-6 col-xs-6"></td>
										<td class="col-sm-6 col-xs-6"></td>
									</tr>
								</tbody>
							</table>
						</div>

                        <?php
						 $where_arr = array('js_id'=>$js_details['id']);
						 $get_lang_data = $this->common_front_model->get_count_data_manual('jobseeker_language',$where_arr,2);
						?>
                        <?php
						if($get_lang_data!='' && is_array($get_lang_data) && count($get_lang_data) > 0)
						{ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 margin-top-10">
							<h4> <?php echo $custom_lable_arr['share_profile_lang_lbl']; ?> :</h4>
                            <?php

								foreach($get_lang_data as $lang_data)
									{?>

							        <hr class="hr-dotted">

                                    <h5 class="text-bg ">
                                           <?php echo $lang_data['language']; ?>
										</h5>
							         <table class="table table-user-information margin-top-20">
								<tbody class="list-group">


                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-user"></span>--><?php echo $custom_lable_arr['share_profile_lang_lvl_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($lang_data['proficiency_level'])) ? $lang_data['proficiency_level'] : "Not Available";?></td>
									</tr>
									<tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-envelope"></span>--><?php echo $custom_lable_arr['share_profile_lang_read']; ?>:</td>
										<td class="col-sm-6 col-xs-6">
                                        <?php
										if($lang_data['reading'] == 'Yes')
										{
											echo "<span class='glyphicon glyphicon-ok'></span>";
										}
										else
										{
											echo "<span class='glyphicon glyphicon-remove'></span>";
										}
										?>
                                        </td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-envelope"></span>--><?php echo $custom_lable_arr['share_profile_lang_write']; ?>:</td>
										<td class="col-sm-6 col-xs-6">
                                        <?php
										if($lang_data['writing'] == 'Yes')
										{
											echo "<span class='glyphicon glyphicon-ok'></span>";
										}
										else
										{
											echo "<span class='glyphicon glyphicon-remove'></span>";
										}
										?>
                                        </td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6"><!--<span class="glyphicon glyphicon-envelope"></span>--><?php echo $custom_lable_arr['share_profile_lang_speak']; ?>:</td>
										<td class="col-sm-6 col-xs-6">
                                        <?php
										if($lang_data['speaking'] == 'Yes')
										{
											echo "<span class='glyphicon glyphicon-ok'></span>";
										}
										else
										{
											echo "<span class='glyphicon glyphicon-remove'></span>";
										}
										?>
                                        </td>
									</tr>

									<tr>
										<td class="col-sm-6 col-xs-6"></td>
										<td class="col-sm-6 col-xs-6"></td>
									</tr>
								</tbody>
							</table>
                           <?php  } ?>
						</div>
                       <?php } ?>



						<h4><?php echo $custom_lable_arr['share_profile_profile_summary_lbl']; ?>:</h4>
						<hr class="hr-dotted">
						<p><?php echo ( $this->common_front_model->checkfieldnotnull($js_details['profile_summary'])) ? $js_details['profile_summary'] : "Not Available";?>.</p>
					</div>
					<?php
					$where_arr = array('js_id'=>$js_details['id']);
					$this->db->order_by("FIELD(is_certificate_X_XII,'Certi'),FIELD(is_certificate_X_XII,'Edu'),FIELD(is_certificate_X_XII,'XII'),FIELD(is_certificate_X_XII,'X')");

					$get_edu_data = $this->common_front_model->get_count_data_manual('jobseeker_education',$where_arr,2);

					if($get_edu_data!='' && is_array($get_edu_data) && count($get_edu_data) > 0)
					{
					?>

                    <div class="tab-pane" id="tab_default_2" style="padding:10px;">
						<?php

									$edu_count = 0;
									$certi_count = 0;
									foreach($get_edu_data as $edu_data)
									{?>

                                    	<?php
										if($edu_data['is_certificate_X_XII'] == 'X')
										{ ?>
											<div class="row margin-top-10">
										<div class="" style="padding:15px;border-radius:0px;">
								         <h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['standard_x_lbl']; ?>  :</h3>

										 <hr class="hr-dotted th_bgcolor">
										<div>
										<h5 class="text-bg ">
                                           <?php echo $custom_lable_arr['standard_x_lbl']; ?>
										</h5>
										 <div class="clearfix"></div>


					                     <div class="margin-top-10">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">


                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_institute_name']; ?>  :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['institute'])) ? $edu_data['institute']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_mark_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['marks'])) ? $edu_data['marks']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-globe"></span>--> <?php echo $custom_lable_arr['share_profile_passing_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['passing_year'])) ? $edu_data['passing_year']: "Not available";?></td>
									</tr>


								</tbody>
							</table>
						</div>

			                          </div>


			<?php
			                            ?>
             				          <div class="clearfix"></div>
						               </div>
									 </div>
										<?php }
										if($edu_data['is_certificate_X_XII'] == 'XII')
										{ ?>
                                        <div class="row margin-top-10">
										<div class="" style="padding:15px;border-radius:0px;">
								         <h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['standard_xii_lbl']; ?>  :</h3>

										 <hr class="hr-dotted th_bgcolor">
										<div>
										<h5 class="text-bg ">
                                    <?php echo $custom_lable_arr['standard_xii_lbl']; ?>
										</h5>
										 <div class="clearfix"></div>


					<div class="margin-top-10">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">


                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_institute_name']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['institute'])) ? $edu_data['institute']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_mark_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['marks'])) ? $edu_data['marks']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-globe"></span>--><?php echo $custom_lable_arr['share_profile_passing_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['passing_year'])) ? $edu_data['passing_year']: "Not available";?></td>
									</tr>


								</tbody>
							</table>
						</div>

			</div>


			<?php
			                            ?>
             				          <div class="clearfix"></div>
						               </div>
									 </div>
                                        <?php

										}

										if($edu_data['is_certificate_X_XII'] == 'Edu')
										{
											$edu_count++;
											$qualification_level = explode(',',$edu_data['qualification_level']);
											$edu_name = $this->common_front_model->valueFromId('educational_qualification_master',$qualification_level,'educational_qualification');
											 ?>
                                        <div class="row margin-top-10">
										<div class="" style="padding:15px;border-radius:0px;">
                                        <?php if($edu_count==1)
										{ ?>
								         <h3 class=""><i class="fa fa-suitcase"></i> Education details :</h3>
                                         <?php } ?>

										 <hr class="hr-dotted th_bgcolor">
										<div>
										<h5 class="text-bg ">
                                     <?php echo ( $this->common_front_model->checkfieldnotnull($edu_name)) ? $edu_name : "Not Available";?>
										</h5>
										 <div class="clearfix"></div>


					<div class="margin-top-10">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">

                                   <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['specialization']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['specialization'])) ? $edu_data['specialization']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_institute_name']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['institute'])) ? $edu_data['institute']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_p_mark_lbl']; ?>  :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['marks'])) ? $edu_data['marks']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-globe"></span>--><?php echo $custom_lable_arr['share_profile_passing_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['passing_year'])) ? $edu_data['passing_year']: "Not available";?></td>
									</tr>


								</tbody>
							</table>
						</div>

			</div>

             				          <div class="clearfix"></div>
						               </div>
									 </div>
                                        <?php

										}
										if($edu_data['is_certificate_X_XII'] == 'Certi')
										{
											$certi_count++;
											 ?>
                                        <div class="row margin-top-10">
										<div class="" style="padding:15px;border-radius:0px;">
                                        <?php if($certi_count==1)
										{ ?>
								         <h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['share_profile_certi_lbl']; ?>  :</h3>
                                         <?php } ?>

										 <hr class="hr-dotted th_bgcolor">
										<div>

										 <div class="clearfix"></div>

									     <div class="margin-top-10">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">


                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_profile_certi_name_lbl']; ?>  :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['institute'])) ? $edu_data['institute']  : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<i class="fa fa-money"></i>--> <?php echo $custom_lable_arr['share_profile_certi_d_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['specialization'])) ? $edu_data['specialization']  : "Not Available";?></td>
									</tr>
                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><!--<span class="glyphicon glyphicon-globe"></span>--> <?php echo $custom_lable_arr['share_profile_passing_lbl']; ?>:</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($edu_data['passing_year'])) ? $edu_data['passing_year']: "Not available";?></td>
									</tr>


								</tbody>
							</table>
						</div>

								       </div>
                      </div>
									 </div>
											<?php

										}

										 ?>

                               <?php } ?>


					</div>
                   <?php }
				    else
					{ ?>
                    <div class="tab-pane" id="tab_default_2" style="padding:10px;">
                    		No data found!.
                    </div>
					<?php }
				   ?>

                    <?php
					$where_arr = array('js_id'=>$js_details['id']);
					$get_work_data = $this->common_front_model->get_count_data_manual('jobseeker_workhistory',$where_arr,2);

					if($get_work_data!='' && is_array($get_work_data) && count($get_work_data) > 0)
					{
					?>

                    <div class="tab-pane" id="tab_default_4" style="padding:10px;">
						<div class="row margin-top-10">
							<div class="" style="padding:15px;border-radius:0px;">
								<h3 class=""><i class="fa fa-suitcase"></i> <?php echo $custom_lable_arr['share_work_exp_lbl']; ?> :</h3>

									<?php

									foreach($get_work_data as $work_data)
									{
										$role = explode(',',$work_data['job_role']);
										$industry = explode(',',$work_data['industry']);
										$functional_area = explode(',',$work_data['functional_area']);

										?>
										<hr class="hr-dotted th_bgcolor">
										<div>
										<h5 class="text-bg "><?php echo  $this->common_front_model->valueFromId('role_master',$role,'role_name'); ?></h5>
										 <div class="clearfix"></div>
										<h6 class=""><i class="fa fa-calendar"></i>  <?php echo ($work_data['joining_date']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($work_data['joining_date'])) ? $this->common_front_model->displayDate($work_data['joining_date'],'Y-m-d'): "Not Available";?>  - <kbd style="padding:2px;background:#D9534F;"><?php echo ($work_data['leaving_date']!='0000-00-00' && $this->common_front_model->checkfieldnotnull($work_data['leaving_date'])) ? $this->common_front_model->displayDate($work_data['leaving_date'],'Y-m-d'): "Presently working";?></kbd></h6>

					<div class="margin-top-10">
							<table class="table table-user-information margin-top-20">
								<tbody class="list-group">
									<tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><span class="glyphicon glyphicon-globe"></span> <?php echo $custom_lable_arr['share_company_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><?php echo ( $this->common_front_model->checkfieldnotnull($work_data['company_name'])) ? $work_data['company_name'] : "Not Available";?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><i class="fa fa-money"></i> &nbsp; <?php echo $custom_lable_arr['share_salary_lbl']; ?> :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;">
										<?php
										echo $this->common_front_model->valueFromId('salary_range',$work_data['annual_salary'],'salary_range');
										?>
										<?php /*echo ( $this->common_front_model->checkfieldnotnull($work_data['annual_salary'])) ? $work_data['currency_type'] .' '. $this->common_front_model->display_exp_salary($work_data['annual_salary']) : "Not Available";*/?></td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><span class="glyphicon glyphicon-globe"></span> <?php echo $custom_lable_arr['share_profile_ind_lbl']; ?>  :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;">
										<?php echo ( $industry!=0 && $this->common_front_model->checkfieldnotnull($industry)) ? $this->common_front_model->valueFromId('industries_master',$industry,'industries_name'): "Not Available";?>
										</td>
									</tr>

                                    <tr>
										<td class="col-sm-6 col-xs-6" style="border-top:none;"><span class="glyphicon glyphicon-globe"></span> <?php echo $custom_lable_arr['share_profile_fn_lbl']; ?>  :</td>
										<td class="col-sm-6 col-xs-6" style="border-top:none;">
										<?php echo ( $functional_area!=0 && $this->common_front_model->checkfieldnotnull($functional_area)) ? $this->common_front_model->valueFromId('functional_area_master',$functional_area,'functional_name'): "Not Available";?>
										</td>
									</tr>

									<tr>
										<td class="col-sm-6 col-xs-6"></td>
										<td class="col-sm-6 col-xs-6"></td>
									</tr>
								</tbody>
							</table>
						</div>

                <p><?php echo ( $this->common_front_model->checkfieldnotnull($work_data['achievements'])) ? $work_data['achievements'] : '';?></p>

			</div>


			<?php }
			?>
             <div class="clearfix"></div>
		</div>
						</div>
					</div>
                   <?php  }
				    else
					{ ?>
                    <div class="tab-pane" id="tab_default_4" style="padding:10px;">
                    		No data found!.
                    </div>
					<?php }
				   ?>

				</div>
			</div>
		</div>
    </div>
    				<!-- Modal  to unlock contact START-->
						 <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left:auto; margin-right:auto; z-index:9999;">
						   <div class="modal-dialog modal-lg">
						      <div class="modal-content">
						       <div class="modal-header">
						         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						         <h4 class="modal-title" id="myModalLabel"> Contact Number </h4>
						       </div>
						       <div class="modal-body" id="getCode">
						       </div>
						    </div>
						   </div>
						 </div>
						 <!-- $$ END -->
	<div class="col-md-3 col-sm-3 col-xs-12"></div>
	<!--<div class="col-md-9 col-sm-9 col-xs-12 margin-bottom-20">
		<div class="panel panel-default" style="padding:15px;border-radius:0px;">
			<h3 class="text-bg"><i class="fa fa-certificate"></i> Company Projects:</h3>
			<hr>
			<div>
				<h5 class="text-bg"><b>W3Schools.com</b></h5>
				<h6><i class="fa fa-calendar"></i> Forever</h6>
				<p>Web Development! All I need to know in one place</p>
				<hr>
			</div>
			<div>
				<h5 class="text-bg"><b>London Business School</b></h5>
				<h6><i class="fa fa-calendar"></i> 2013 - 2015</h6>
				<p>Master Degree</p>
				<hr>
			</div>
			<div>
				<h5 class="text-bg"><b>School of Coding</b></h5>
				<h6><i class="fa fa-calendar"></i> 2010 - 2013</h6>
				<p>Bachelor Degree</p><br />
			</div>
		</div>
    </div>-->
</div>
<div class="clearfix"></div>

<script>

    // start 22-05-2020 shakil
	function unlock_contact(contact,jidpkd)
			{
				show_comm_mask();
				var hash_tocken_id = $("#hash_tocken_id").val();
				var datastring = 'csrf_job_portal='+hash_tocken_id+'&contact='+contact+'&jidpkd='+jidpkd+'&user_agent=NI-WEB';

				$.ajax({
					url : "<?php echo $base_url.'browse_resume/unlock_contact' ?>",
					type: 'post',
					data: datastring,
					success: function(data)
					{
						$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
						if(data.status=='success')
						{
							hide_comm_mask();
							$("#getCode").html(data.contact);
							$("#getCodeModal").modal('show');
						}
						else
						{
							alert(data.errmessage);
						}
						 hide_comm_mask();
					}
				});

			}
function download_resume(file,jidpkd)
{
	show_comm_mask();
	var hash_tocken_id = $("#hash_tocken_id").val();
	//var datastring = 'csrf_job_portal='+hash_tocken_id+'&file='+file+'&user_agent=NI-WEB';
	var datastring = 'csrf_job_portal='+hash_tocken_id+'&file='+file+'&jidpkd='+jidpkd+'&user_agent=NI-WEB';
	$.ajax({
		url : "<?php echo $base_url.'share_profile/download_resume' ?>",
		type: 'post',
		data: datastring,
		success: function(data)
		{
			$("#hash_tocken_id").val('<?php echo $this->security->get_csrf_hash(); ?>');
			if(data.status=='success')
			{
				window.open(data.file_download);
			}
			else
			{
				alert(data.errmessage);
			}
			 hide_comm_mask();
		}
	});

}

</script>
<?php
if(isset($emp_id_stored_plan) && $emp_id_stored_plan!='')
{
	if($return_data_plan_emp == 'Yes')
	{
		$this->common_front_model->check_for_plan_update($emp_id_stored_plan,'employer',$js_details['id']);
	}//if plan is valid
} ?>