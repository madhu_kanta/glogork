<div id="main_div">
<div class="clearfix"></div>
<div id="titlebar" class="single submit-page" style="background-image:url(<?php echo $base_url; ?>assets/front_end/images/banner/payment-banner.jpg);background-repeat:no-repeat;background-size:cover;min-width:100%;background-position:center center;">
    <div class="container">
        <div class="sixteen columns">
            <h2 style="color:gray;"><i class="fa fa-2x fa-credit-card" style="vertical-align:middle;"></i> Plan Detail and Payment Overview</h2>
        </div>
    </div>
</div>
<?php
	$display_detail ='No';
	if($plan_id !='')
	{
		$user_type = '';
		$user_id = '';
		$user_data = $this->common_front_model->get_logged_user_typeid();
		$user_type = $user_data['user_type'];
		$user_id = $user_data['user_id'];
		if($user_type !='' && $user_id !='')
		{
			$plan_data = array();
			if($user_type == 'job_seeker')
			{
				$plan_table_name = 'credit_plan_jobseeker';
			}
			else
			{
				$plan_table_name = 'credit_plan_employer';
			}
			$plan_data = $this->common_front_model->get_count_data_manual($plan_table_name,array('status'=>'APPROVED','id'=>$plan_id),1,' * ','',0,'',0);
			$plan_data_count = count($plan_data);
			if(isset($plan_data) && $plan_data !='' && $plan_data_count > 0)
			{
				$display_detail ='Yes';
			}
		}
	}
	if(isset($display_detail) && $display_detail =='Yes')
	{
		//print_r($plan_data);
		$plan_amount = 0;
		$total_pay = 0;
		$discount_amt = 0;
		$discount_display = 'No';
		$coupan_code = '';
		$coupan_data = $this->session->userdata('coupan_data_reddem');
		if(isset($coupan_data) && $coupan_data !='' && is_array($coupan_data) && count($coupan_data) > 0)
		{
			$discount_display = 'Yes';
			if(isset($coupan_data['discount_amount']) && $coupan_data['discount_amount'] !='')
			{
				$discount_amt = $coupan_data['discount_amount'];
			}
			if(isset($coupan_data['coupan_code']) && $coupan_data['coupan_code'] !='')
			{
				$coupan_code = $coupan_data['coupan_code'];
			}
		}
		if(isset($plan_data['plan_amount']) && $plan_data['plan_amount'] !='')
		{
			$plan_amount = $plan_data['plan_amount'];
		}
		$service_tax = 0;
		$service_tax_amt = 0;
		$config_data = $this->common_front_model->get_site_config();
		if(isset($config_data['service_tax']) && $config_data['service_tax'] !='')
		{
			$service_tax =  $config_data['service_tax'];
		}
		if($plan_amount !='' && $plan_amount > 0 && $service_tax !='' && $service_tax > 0 )
		{
			$service_tax_amt = (($plan_amount - $discount_amt) * $service_tax) / 100;
		}
		$total_pay = ($plan_amount - $discount_amt) + $service_tax_amt;
		
		$data_array = array('discount_amount'=>$discount_amt,'coupan_code'=>$coupan_code,'plan_id'=>$plan_id,'user_type'=>$user_type,'service_tax'=>$service_tax_amt,'plan_amount'=>$plan_amount,'total_pay'=>$total_pay,'plan_data_array'=>$plan_data);
		$this->session->set_userdata('plan_data_session',$data_array);
?>
<div class="container">
    <div>
        <div class="sixteen columns">
            <div class="panel panel-default">
                <div class="panel-heading th_bgcolor"><span class="glyphicon glyphicon-credit-card"  style="vertical-align:middle;"></span> Selected Plan - <?php if(isset($plan_data['plan_name'])){echo $plan_data['plan_name'];} ?>
                </div>
                    <div class="panel-body th_bordercolor">
                        <div class="margin-top-10"></div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="panel panel-default credit-card-box">
                                    <div class="panel-heading display-table">
                                        <div class="row display-tr">
                                            <div class="col-md-6 col-xs-12" style="margin:0px;">
                                            <h3 class="panel-title display-td text-center">Payment Details</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <form id="payment-form" method="POST" action="javascript:void(0);">
                                            <div class="row" style="margin-bottom: 0px;">
                                                <div class="col-xs-12">
                                                    <div class="form-group row">
                                                        <div class="col-xs-6">Plan Amount </div>
                                                        <div class="col-xs-6 input-group"> : <?php echo $plan_data['plan_currency'].$plan_amount; ?>
                                                        </div>
                                                    </div>
                                                <?php
												if(isset($discount_display) && $discount_display !='' && $discount_display =='Yes')
												{
												?>    
                                                    <div class="form-group text-success row">
                                                        <div class="col-xs-6  text-success">Discount Amount </div>
                                                        <div class="col-xs-6 input-group  text-success"> : <?php echo $plan_data['plan_currency'].$discount_amt. ' ('.$coupan_code.')'; ?>
                                                        </div>
                                                    </div>
                                                    
                                                <?php
												}
												if($service_tax_amt !='' && $service_tax_amt > 0)
												{
												?>    
                                                    <div class="form-group row">
                                                        <div class="col-xs-6">Service Tax / GST</div>
                                                        <div class="col-xs-6 input-group"> : <?php echo $plan_data['plan_currency'].$service_tax_amt; ?>
                                                        </div>
                                                    </div>
                                               <?php
												}
											   ?>     
                                                    <div class="form-group row">
                                                        <div class="col-xs-6">Total Pay </div>
                                                        <div class="col-xs-6 input-group"> : <?php echo $plan_data['plan_currency'].$total_pay; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <?php
												if(isset($discount_display) && $discount_display !='' && $discount_display =='No' && $total_pay > 0)
												{
											?>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>COUPON CODE <span>(if any)</span></label>
                                                        <input type="text" class="form-control" placeholder="Coupon Code" name="couponcode" id="couponcode" value="" />
                                                        <span class="pull-right"><a onClick="return check_coupan_code()" class="text-success" href="javascript:;">Redeem Coupon</a></span>
                                                        <span id="err_couponcode" class="text-danger"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
												}
											?>
                                            <div class="row">
                                                <div class="col-xs-12">
                                            <?php
												if($total_pay > 0)
												{
											?>    
                                                    <button onClick="process_checkout()" class="subscribe btn btn-success btn-lg btn-block" type="button">Pay Now</button>
                                            <?php
												}
												else
												{
											?>    
                                                    <a class="subscribe btn btn-success btn-lg btn-block" href="<?php echo $base_url.'contact/contact-admin/plan-request'?>">Contact to Admin</a>
                                            <?php
												}
											?>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-xs-12">
                                                    <p class="payment-errors"></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-8" style="font-size: 12pt; line-height: 2em;">                                <p class="lead">Plan Benefits </p>
                                <ul class="list-unstyled" style="line-height: 2">
                                	<?php
										$check_yes = '<span class="fa fa-check text-success"></span>';
										$check_no = '<span class="fa fa-times text-danger"></span>';
										$displ_yes_no_mess = '';
										$displ_yes_no_contact = '';
										if(isset($plan_data['message']) && $plan_data['message'] !='' && $plan_data['message'] > 0)
										{
											$displ_yes_no_mess = $check_yes;
										}
										else
										{
											$displ_yes_no_mess = $check_no;
										}
										if(isset($plan_data['contacts']) && $plan_data['contacts'] !='' && $plan_data['contacts'] > 0)
										{
											$displ_yes_no_contact = $check_yes;
										}
										else
										{
											$displ_yes_no_contact = $check_no;
										}
									?>
                                    <li><?php echo $displ_yes_no_mess.' Allowed Message - '.$plan_data['message']; ?> </li>
                                  	<li><?php echo $displ_yes_no_contact.' Allowed Contacts - '.$plan_data['contacts']; ?> </li>
                                    <?php
									if($user_type == 'job_seeker')
									{	
										$displ_yes_no_rel_job = '';
										$displ_yes_no_job_not = '';
										$displ_yes_no_per_rep = '';
										$displ_yes_no_high_app = '';
										if(isset($plan_data['relevant_jobs']) && $plan_data['relevant_jobs'] =='Yes')
										{ 
											$displ_yes_no_rel_job = $check_yes;
										}
										else
										{
											$displ_yes_no_rel_job = $check_no;
										}
										if(isset($plan_data['job_post_notification']) && $plan_data['job_post_notification'] =='Yes')
										{ 
											$displ_yes_no_job_not = $check_yes;
										}
										else
										{
											$displ_yes_no_job_not = $check_no;
										}
										if(isset($plan_data['performance_report']) && $plan_data['performance_report'] =='Yes')
										{ 
											$displ_yes_no_per_rep = $check_yes;
										}
										else
										{
											$displ_yes_no_per_rep = $check_no;
										}
										if(isset($plan_data['highlight_application']) && $plan_data['highlight_application'] =='Yes')
										{ 
											$displ_yes_no_high_app = $check_yes;
										}
										else
										{
											$displ_yes_no_high_app = $check_no;
										}
									?>
									<li><?php echo $displ_yes_no_rel_job.' Relevant Jobs '; ?> </li>
                                    <li><?php echo $displ_yes_no_job_not.' Job Notification '; ?> </li>
                                    <li><?php echo $displ_yes_no_per_rep.' Performance Report '; ?> </li>
                                    <li><?php echo $displ_yes_no_high_app.' Highlight Application '; ?> </li>
                                <?php
									}
									else
									{
										$displ_yes_no_job_life = '';
										$displ_yes_no_job_high_lgt = '';
										$displ_yes_no_job_pl = '';
										$displ_yes_no_cval= '';
										if(isset($plan_data['job_life']) && $plan_data['job_life'] !='' && $plan_data['job_life'] > 0)
										{
											$displ_yes_no_job_life = $check_yes;
										}
										else
										{
											$displ_yes_no_job_life = $check_no;
										}
										if(isset($plan_data['highlight_job_limit']) && $plan_data['highlight_job_limit'] !='' && $plan_data['highlight_job_limit'] > 0)
										{
											$displ_yes_no_job_high_lgt = $check_yes;
										}
										else
										{
											$displ_yes_no_job_high_lgt = $check_no;
										}
										if(isset($plan_data['job_post_limit']) && $plan_data['job_post_limit'] !='' && $plan_data['job_post_limit'] > 0)
										{
											$displ_yes_no_job_pl = $check_yes;
										}
										else
										{
											$displ_yes_no_job_pl = $check_no;
										}
										if(isset($plan_data['cv_access_limit']) && $plan_data['cv_access_limit'] !='' && $plan_data['cv_access_limit'] > 0)
										{
											$displ_yes_no_cval = $check_yes;
										}
										else
										{
											$displ_yes_no_cval = $check_no;
										}
									?>
                                    <li><?php echo $displ_yes_no_job_life.' Job Life - '.$plan_data['job_life']; ?> </li>
                                    <li><?php echo $displ_yes_no_job_high_lgt.' Highlight Job - '.$plan_data['highlight_job_limit']; ?> </li>
                                    <li><?php echo $displ_yes_no_job_pl.' Job Post Limit - '.$plan_data['job_post_limit']; ?> </li>
                                    <li><?php echo $displ_yes_no_cval.' Cv Access Limit - '.$plan_data['cv_access_limit']; ?> </li>
                                    <?php
									}
								?>    
                                </ul>
                                <p></p>
                                <p>Plan Duration - <?php echo $plan_data['plan_duration']; ?> Days</p>                            </div>
                        </div>
                    </div>
            </div>
        </div>
   </div>
</div>
<?php
	}
	else
	{
?>
	<div class="alert alert-danger">Sorry, Please try again.</div> 
<?php
	}
	// payu-payment-process // for payu money payment gateway
	// payment-process //  for payu bizz payment gateway
	
?>
<form action="<?php echo $base_url.'my-plan/payment-process'; ?>" method="post" id="plan_submit" name="plan_submit">
	<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $plan_id; ?>" />
    <input type="hidden" id="hash_tocken_id" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</form>
<div class="margin-top-40"></div>
<script type="text/javascript">
	function process_checkout()
	{		
		$("#plan_submit").submit();	
	}
	function check_coupan_code()
	{
		$("#err_couponcode").html('');
		var couponcode = $("#couponcode").val();
		if(couponcode =='')
		{
			$("#err_couponcode").html('Please Enter Coupon Code');
			$("#err_couponcode").slideDown();
		}
		else
		{
			var form_data = '';
			var hash_tocken_id = $("#hash_tocken_id").val();
			var plan_id = $("#plan_id").val();
			//form_data = form_data + 'csrf_job_portal='+hash_tocken_id+ '&plan_id='+plan_id+ '&couponcode='+couponcode;
			show_comm_mask();
			$.ajax({
			   url: '<?php echo $base_url.'my-plan/check-coupan' ?>',
			   type: "post",
			   data: {'csrf_job_portal':hash_tocken_id,'plan_id':plan_id,'couponcode':couponcode},
			   dataType:"json",
			   success:function(data)
			   {
				   if(data.status =='success')
				   {
						$("#main_div").html(data.message);
				   }
				   else
				   {
						$("#err_couponcode").html(data.message);
						$("#err_couponcode").slideDown();
				   }
				   hide_comm_mask();
			   }
			});
		}
		return false;
	}
</script>
</div>