<!-- Banner
================================================== -->
<div id="banner" class="with-transparent-header parallax background" style="background-image: url(<?php echo $base_url; ?>assets/front_end/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container">
		<div class="sixteen columns">

			<div class="search-container">

				<!-- Form -->
				<h2 class="bounceInDown animated">Find job</h2>
                <div class="bounceInDown animated">
					<?php $this->load->view('front_end/search_box');?>	
				</div>
				<!-- Browse Jobs -->
				<div class="browse-jobs bounceInUp animated">
					Browse job offers by <a href="browse-categories.html"> category</a> or <a href="#">location</a>
				</div>

				<!-- Announce -->
				<div class="announce bounceInUp animated">
					We’ve over <strong>15 000</strong> job offers for you!
				</div>

			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<div class="container">
	<div class="sixteen columns ">
		<h3 class="margin-bottom-25">Popular Categories</h3>
		<ul id="popular-categories" class="">
			<li><a href="#"><i class="ln  ln-icon-Bar-Chart"></i> Accounting / Finance</a></li>
			<li><a href="#"><i class="ln ln-icon-Car"></i> Automotive Jobs</a></li>
			<li><a href="#"><i class="ln ln-icon-Worker"></i> Construction / Facilities</a></li>
			<li><a href="#"><i class="ln ln-icon-Student-Female"></i> Education Training</a></li>
			<li><a href="#"><i class="ln  ln-icon-Medical-Sign"></i> Healthcare</a></li>
			<li><a href="#"><i class="ln  ln-icon-Plates"></i> Restaurant / Food Service</a></li>
			<li><a href="#"><i class="ln  ln-icon-Globe"></i> Transportation / Logistics</a></li>
			<li><a href="#"><i class="ln  ln-icon-Laptop-3"></i> Telecommunications</a></li>
		</ul>

		<div class="clearfix"></div>
		<div class="margin-top-30"></div>

		<a href="browse-categories.html" class="button centered">Browse All Categories</a>
		<div class="margin-bottom-50"></div>
	</div>
</div>

<div class="container">

	<!-- Recent Jobs -->
	<div class="eleven columns ">
	<div class="padding-right">
		<h3 class="margin-bottom-25">Recent Jobs</h3>
		<ul class="job-list">

			<li class="highlighted"><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-01.png" alt="">
				<div class="job-list-content">
					<h4>Marketing Coordinator - SEO / SEM Experience <span class="full-time">Full-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> King</span>
						<span><i class="fa fa-map-marker"></i> Sydney</span>
						<span><i class="fa fa-money"></i> $100 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-02.png" alt="">
				<div class="job-list-content">
					<h4>Core PHP Developer for Site Maintenance <span class="part-time">Part-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Cubico</span>
						<span><i class="fa fa-map-marker"></i> London</span>
						<span><i class="fa fa-money"></i> $50 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-03.png" alt="">
				<div class="job-list-content">
					<h4>Restaurant Team Member - Crew <span class="full-time">Full-Time</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> King</span>
						<span><i class="fa fa-map-marker"></i> Sydney</span>
						<span><i class="fa fa-money"></i> $15 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-04.png" alt="">
				<div class="job-list-content">
					<h4>Power Systems User Experience Designer  <span class="internship">Internship</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Hexagon</span>
						<span><i class="fa fa-map-marker"></i> London</span>
						<span><i class="fa fa-money"></i> $75 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>

			<li><a href="job-page.html">
				<img src="<?php echo $base_url; ?>assets/front_end/images/job-list-logo-05.png" alt="">
				<div class="job-list-content">
					<h4>iPhone / Android Music App Development <span class="temporary">Temporary</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> Mates</span>
						<span><i class="fa fa-map-marker"></i> New York</span>
						<span><i class="fa fa-money"></i> $115 / hour</span>
					</div>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>
		</ul>

		<a href="browse-jobs.html" class="button centered"><i class="fa fa-plus-circle"></i> Show More Jobs</a>
		<div class="margin-bottom-55"></div>
	</div>
	</div>

	<!-- Job Spotlight -->
	<div class="five columns ">
		<h3 class="margin-bottom-5">Job Spotlight</h3>

		<!-- Navigation -->
		<div class="showbiz-navigation">
			<div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
			<div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
		</div>
		<div class="clearfix"></div>

		<!-- Showbiz Container -->
		<div id="job-spotlight" class="showbiz-container">
			<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
				<div class="overflowholder">

					<ul>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Social Media: Advertising Coordinator <span class="part-time">Part-Time</span></h4></a>
								<span><i class="fa fa-briefcase"></i> Mates</span>
								<span><i class="fa fa-map-marker"></i> New York</span>
								<span><i class="fa fa-money"></i> $20 / hour</span>
								<p>As advertising & content coordinator, you will support our social media team in producing high quality social content for a range of media channels.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Prestashop / WooCommerce Product Listing <span class="freelance">Freelance</span></h4></a>
								<span><i class="fa fa-briefcase"></i> King</span>
								<span><i class="fa fa-map-marker"></i> London</span>
								<span><i class="fa fa-money"></i> $25 / hour</span>
								<p>Etiam suscipit tellus ante, sit amet hendrerit magna varius in. Pellentesque lorem quis enim venenatis pellentesque.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Logo Design <span class="freelance">Freelance</span></h4></a>
								<span><i class="fa fa-briefcase"></i> Hexagon</span>
								<span><i class="fa fa-map-marker"></i> Sydney</span>
								<span><i class="fa fa-money"></i> $10 / hour</span>
								<p>Proin ligula neque, pretium et ipsum eget, mattis commodo dolor. Etiam tincidunt libero quis commodo.</p>
								<a href="job-page.html" class="button">Apply For This Job</a>
							</div>
						</li>


					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>
</div>

<!-- Testimonials -->
<div id="testimonials" class="">
	<!-- Slider -->
	<br><br>
	<div class="container fadeInDown animated">
		<div class="col-md-12 col-sm-12 col-xs-12">
                <div id="Carousel" class="carousel slide">

                <br>
                <!-- Carousel items -->
                <div class="carousel-inner">
					<div class="item active">
					 <ul class="row">
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/1.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/2.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/3.png" alt="Image" style="height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/4.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/5.jpg" alt="Image" style="height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/6.jpg" alt="Image" style="height:80px;"></a></li>
					 </ul><!--.row-->
                </div><!--.item-->

                <div class="item">
					 <ul class="row">
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/7.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/17.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/9.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/10.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/11.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/12.jpg" alt="Image" style="max-height:80px;"></a></li>
					 </ul><!--.row-->
                </div><!--.item-->

                <div class="item">
					 <ul class="row">
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/13.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/14.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/15.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/16.png" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/1.jpg" alt="Image" style="max-height:80px;"></a></li>
					   <li class="col-md-2 col-sm-6 col-xs-12"><a href="#" class="thumbnail"><img src="https://tmaxtech.co.th/images/Partner/2.jpg" alt="Image" style="height:80px;"></a></li>
                       </ul>
					 </div>
                </div>
			</div>
		</div>
		<br><br>
	</div>
</div>

<!-- Infobox -->
<div class="infobox">
	<div class="container">
		<div class="sixteen columns ">Start Building Your Own Job Board Now <a href="my-account.html">Get Started</a></div>
	</div>
</div>

<!-- Recent Posts -->
<div class="container ">
	<div class="sixteen columns">
		<h3 class="margin-bottom-25">Recent Posts</h3>
	</div>


	<div class="one-third column">

		<!-- Post #1 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="<?php echo $base_url; ?>assets/front_end/images/recent-post-01.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>Hey Job Seeker, It’s Time To Get Up And Get Hired</h4></a>
			<div class="meta-tags">
				<span>October 10, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>The world of job seeking can be all consuming. From secretly stalking the open reqs page of your dream company to sending endless applications.</p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>

	</div>


	<div class="one-third column">

		<!-- Post #2 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="<?php echo $base_url; ?>assets/front_end/images/recent-post-02.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>How to "Woo" a Recruiter and Land Your Dream Job</h4></a>
			<div class="meta-tags">
				<span>September 12, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>Struggling to find your significant other the perfect Valentine’s Day gift? If I may make a suggestion: woo a recruiter. </p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>

	</div>

	<div class="one-third column">

		<!-- Post #3 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="<?php echo $base_url; ?>assets/front_end/images/recent-post-03.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>11 Tips to Help You Get New Clients Through Cold Calling</h4></a>
			<div class="meta-tags">
				<span>August 27, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>If your dream employer appears on this list, you’re certainly in good company. But it also means you’re up for some intense competition.</p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>
	</div>

</div>



<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/animate.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>assets/front_end/css/slideanim.css">

<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">


<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>

<script>

$('#text_home_search').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>job_listing/get_search_suggestion/"+request.term, {
          }, function (data) {
            response(data);
		});
      },
      delay: 100
    },
    showAutocompleteOnFocus: true
});
$('#text_home_search').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});


$('#location_home_search').tokenfield({
    autocomplete: {
      source: function (request, response) {
          jQuery.get("<?php echo $base_url; ?>sign_up/get_suggestion_city/id/"+request.term, {
          }, function (data) {
             response(data);	
          });
      },
      delay: 100	
    },
    showAutocompleteOnFocus: true
});

$('#location_home_search').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});
 
 $.validate({
	 form : '#text_search_form',
     modules : 'security,logic',
	 onError : function($form) {
	  scroll_to_div('text_search_form',-300);
	 }
  });
</script>