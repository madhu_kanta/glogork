<link rel="stylesheet" href="http://192.168.1.111/Mohammad_Ali/job-portal/assets/front_end/css/left-pannel.css?ver=1.0">
<?php
$custom_lable_array = $custom_lable->language;
$key_skill = $this->employer_post_job_model->get_employer_industry_farea('skill_hire','array','',1); 
$key_skill_data = json_encode($key_skill); 

if(isset($emp_status) && $emp_data_values!='' && is_array($emp_data_values) && count($emp_data_values) > 0)
{
	$emp_status = $emp_data_values['status'];
}
if(isset($edit_job_data) && $edit_job_data!='' && $edit_job_data > 0)
{
	$edit_job_data = $edit_job_data;
}
?>
<div id="titlebar" class="single submit-page" style="background-color:#94AFCC;">
	<div class="container">

		<div class="sixteen columns">
			<h2 style="color:white;"><i class="fa fa-plus-circle"></i> <?php echo $custom_lable_array['post_new_job']; ?></h2>
		</div>

	</div>
</div>
<?php
if(isset($emp_status) && $emp_status == 'UNAPPROVED')
{  ?>
<div class="container">
	<!-- Submit Page -->
	<div class="sixteen columns">
		<div class="submit-page">
			<!-- Notice -->
			<div class="notification notice closeable margin-bottom-40 " style="border:1px solid;">
				<p><i class="fa fa-2x fa-hand-o-right" aria-hidden="true"></i><span style="marfin-top:-5px;"><a href="#"> <u><?php echo $custom_lable_array['account_verify']; ?></u> </a></span><?php echo $custom_lable_array['account_verify_msg']; ?></p>
			</div>
		</div>
	</div>
	<hr>
</div>
<?php }
else
{ 
?>
<style>

</style>
<div class="container" >
    <div class="row">
		<section>
			<div class="wizard">
<?php            
$return_data_plan_emp = $this->common_front_model->get_plan_detail($emp_data_values['id'],'employer','job_post_limit');

if($return_data_plan_emp != 'Yes')
{	?>
    <div class="alert alert-warning clearfix" style=" height: 75px!important;">
   	  <div class=" col-md-8 col-xs-12">
      <strong>Note !</strong> Please upgrade your membership to post a job.
      </div>
      <div class="col-md-8 col-xs-12 visible-xs"><br><br></div>
      <div class="col-md-4 col-xs-12">
      	<a style="margin-top:-8px" class="button pull-right" href="<?php echo $base_url.'my-plan/index' ?>"> Upgrade Now</a>
      </div>
    </div>
   
    
<?php }else{ ?>

				<!--<div class="wizard-inner">
					<div class="connecting-line"></div>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo $custom_lable_array['personal_info_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo $custom_lable_array['edu_qualification_lbl']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-education"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?php  echo $custom_lable_array['work_experiance']; ?>">
								<span class="round-tab">
									<i class="fa fa-building" aria-hidden="true"></i>
								</span>
							</a>
						</li>
                       
						<li role="presentation" class="disabled">
							<a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="<?php  echo $custom_lable_array['js_details']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a>
						</li>	
						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="<?php echo $custom_lable_array['upload_resume_pro_pic']; ?>">
								<span class="round-tab">
									<i class="glyphicon glyphicon-open-file"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>-->
					<div class="tab-content" >
                        <div class="alert alert-danger" id="error_msg" style="display:none" > </div>
                        <div class="alert alert-success" id="success_msg" style="display:none" ></div>
                
					    <div class="tab-pane active" role="tabpanel" id="step4">
                        
						  <form class="well" method="post" id="post_jobs">
							<div class="panel panel-default">
								<div class="panel-heading" style="background-color:#337AB7;"><strong style="color:white;"><?php  echo $custom_lable_array['post_new_job']; ?></strong></div>
                            
								<div class="panel-body work-exp-html">
								
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_title']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="job_title" class="form-control" id="job_title" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['job_title_p_holder'];  ?>" value="<?php if(isset($edit_job_data['job_title'])) {echo $edit_job_data['job_title'];} ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                               <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_description']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      	<textarea style="border-radius:0px;"rows="2" cols="2" name="job_description" class="form-control" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['job_description'];  ?>"><?php if(isset($edit_job_data['job_description'])) {echo $edit_job_data['job_description'];} ?></textarea>
                                                     
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                        
                                        <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['skill_keyword']; ?><span class="red-only"> *</span></h5>
													<div class="input-group bs-example">
													  <span class="input-group-addon"></span>
                                                       <input type="text" name="skill_keyword" class="form-control" id="skill_keyword" style="border-radius:0px;" data-validation="required"  placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['skill_keyword'];  ?>" value="<?php if(isset($edit_job_data['skill_keyword'])) {echo $edit_job_data['skill_keyword'];} ?>">
                                                     
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                        
          <!--                              <div class="row">-->
										<!--	<div class="col-md-12">-->
										<!--		<div class="col-md-3"></div>-->
										<!--		<div class="col-md-6">-->
										<!--			<h5><?php //echo $custom_lable_array['search_keyword']; ?></h5>-->
										<!--			<div class="input-group bs-example">-->
										<!--			  <span class="input-group-addon"></span>-->
          <!--                                             <input type="text" name="search_keyword" class="form-control" id="search_keyword" style="border-radius:0px;"  placeholder="<?php //echo $custom_lable_array['specify'] .' '. $custom_lable_array['search_keyword'];  ?>">-->
                                                     
										<!--			</div>-->
										<!--		</div>-->
										<!--	</div>-->
										<!--</div>-->
                                        
                                        <div class="row">
                                         <div class="col-md-12">
										<div class="col-md-3"></div>
                                       <div class="col-sm-6 nopadding ">
									  <h6><?php echo $custom_lable_array['work_experience'] ?> <span class="red-only"> *</span></h6>
									  <div class="form-group">
									  
									  <div class="col-md-6 col-sm-6 nopadding tufelpaleft15" style="padding-left:0px;">
                                        <select name="work_experience_from" data-validation="required" class="form-control" style="border-radius:0px;" >
											<option value=""><?php echo $custom_lable_array['from'] ?></option>
                                            <?php
											for($i=0;$i <= 25 ; $i=$i+0.5)
											{ $explode = explode('.',$i);
											  
											  $m_val = '0' ;
											  if(isset($explode[1]) && $explode[1]!='')
											  {
												  $m_val = $explode[1] + 1;
											  }
											  $val = $explode[0].'.'.$m_val;
											 ?>
											 <option value="<?php echo  $val; ?>" <?php if(isset($edit_job_data['work_experience_from']) && $edit_job_data['work_experience_from']==$val ) {echo "selected";} ?>><?php  if($explode[0]=='0' && $m_val=='0'){echo "Fresher";} else{ echo $explode[0] . ' Year '. $m_val . ' Month '; }?> </option>	
										<?php }
											?>
										</select>
                                        </div>
									  
										<div class="col-md-6 col-sm-6 nopadding tufelparight15" style="padding-right:0px;">
                                        <select name="work_experience_to" data-validation="required" class="form-control" style="border-radius:0px;" >
										    <option value=""><?php echo $custom_lable_array['to'] ?></option>
											 <?php
											for($i=0;$i <= 25 ; $i=$i+0.5)
											{ $explode = explode('.',$i);
											  
											  $m_val = '0' ;
											  if(isset($explode[1]) && $explode[1]!='')
											  {
												  $m_val = $explode[1] + 1;
											  }
											  $val = $explode[0].'.'.$m_val;
											 ?>
											 <option value="<?php echo  $val; ?>" <?php if(isset($edit_job_data['work_experience_to']) && $edit_job_data['work_experience_to']==$val ) {echo "selected";} ?>><?php  if($explode[0]=='0' && $m_val=='0'){echo "Fresher";} else{ echo $explode[0] . ' Year '. $m_val . ' Month '; } ?> </option>	
										<?php }
											?>
										</select>
                                        </div>
                                        
									  </div>
									</div>
                                     </div>
                                      </div> 
                                        
                               
                               <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['total_requirenment']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="total_requirenment" class="form-control form-control"  id="total_requirenment" style="border-radius:0px;" data-validation="required" data-placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['total_requirenment'];  ?>">
                                                      <option value=""><?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['total_requirenment'];  ?></option>
														  <?php
														  for($i=1;$i<=100;$i++)
														  { ?>
                                                            <option value="<?php echo $i; ?>"  <?php if(isset($edit_job_data['total_requirenment']) && $edit_job_data['total_requirenment']==$i ) {echo "selected";} ?>><?php echo $i; ?></option>
														  <?php }
                                                          ?>
                                                      </select>
                                                      
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                               
                               <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['location_hiring']; ?><span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="location_hiring[]" class="form-control select2" multiple id="location_hiring" style="border-radius:0px;" data-validation="required" data-placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['location_hiring'];  ?>">
														  <?php
														  if(isset($edit_job_data['location_hiring']) && $edit_job_data['location_hiring']!='')
														  {
															  
															 $where = "id IN (".$edit_job_data['location_hiring'].")";
															  $city_data = $this->common_front_model->get_count_data_manual('city_master',$where,2);
															  foreach($city_data as $city)
															  { ?>
																<option value="<?php echo $city['id']; ?>" selected><?php echo $city['city_name']; ?></option>
															  <?php }
															 // $selected_item = explode(',',$edit_job_data['location_hiring']);
														  }
														  
														   //echo  $this->common_front_model->get_list('city_list','','','str',$selected_item,'1'); ;
                                                          ?>
                                                      </select>
                                                      
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                       <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['company_hiring']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="text" name="company_hiring" class="form-control" id="company_hiring" style="border-radius:0px;"  data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['company_hiring'];  ?>" value="<?php if(isset($edit_job_data['company_hiring'])) {echo $edit_job_data['company_hiring'];} ?>">
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
												<div class="form-group">	
                                                    <h5><?php echo $custom_lable_array['job_industry']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="job_industry" data-validation="required" class="form-control">
                                                     <?php 
													 $selected_item='';
													 if(isset($edit_job_data['job_industry']) &&  $edit_job_data['job_industry']!='')
													  {
														  $selected_item = explode(',',$edit_job_data['job_industry']);
													  }
														 
														  
													 echo  $this->employer_post_job_model->get_employer_industry_farea('industry_hire','str',$selected_item); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
                                        	
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
												<div class="form-group">	
                                                    <h5><?php echo $custom_lable_array['job_functional_area']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                       <select name="functional_area" id="functional_area" data-validation="required" class="form-control" onChange="dropdownChange('functional_area','job_post_role','role_master');" >
                                                     <?php 
													 $selected_item='';
													 if(isset($edit_job_data['functional_area']) && $edit_job_data['functional_area']!='')
													  {
														  $selected_item = explode(',',$edit_job_data['functional_area']);                                                      }
														 
													 echo  $this->employer_post_job_model->get_employer_industry_farea('function_area_hire','str',$selected_item); ?>
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
										</div>
								
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_post_role']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_post_role" id="job_post_role" data-validation="required" class="form-control" data-placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['job_post_role'];  ?>">
                                                      <?php
													  
													   $functional_area='';
													   $job_post_role='';
													  if(isset($edit_job_data['functional_area']) && $edit_job_data['functional_area']!='')
														  {
															  $functional_area = $edit_job_data['functional_area'];                                                          }
														  
														  
													  if(isset($edit_job_data['job_post_role']) && $edit_job_data['job_post_role']!='')
													  {
														  $job_post_role = explode(',',$edit_job_data['job_post_role']);                                                      }
														  
													if($job_post_role!='' && $functional_area!='')
													{ 
													   echo  $this->common_front_model->get_list('role_master','',$functional_area,'str',$job_post_role); 
													}?>
                                                      
                                                      </select> 
                                                       
                                                      <!--<input type="text" name="job_post_role" class="form-control" id="job_post_role" style="border-radius:0px;" data-validation="required" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['job_post_role'];  ?>">--> 
													</div>
                                                    </div>
												</div>
											</div>
										</div>    
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_shift']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_shift" id="job_shift"  data-validation="required" class="form-control" >
                                                     <?php 
													 $job_shift='';
													 if(isset($edit_job_data['job_shift']) && $edit_job_data['job_shift']!='')
														  {
															  $job_shift = explode(',',$edit_job_data['job_shift']);                                                          }
														  
													 echo $this->common_front_model->get_list('shift_list','','','str',$job_shift); ?>											</select>
													</div>
                                                    </div>
												</div>
											</div>
									</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<h5><?php echo $custom_lable_array['job_type']; ?> </h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_type" id="job_type"   class="form-control" >
                                                     <?php 
													 $job_type='';
													 if(isset($edit_job_data['job_type']) && $edit_job_data['job_type']!='')
														  {
															  $job_type = explode(',',$edit_job_data['job_type']);                                                          }
													 echo $this->common_front_model->get_list('job_type_list','','','str',$job_type); ?>											</select>
													</div>
												</div>
											</div>
									</div> 
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_education']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_education[]" id="job_education" multiple data-validation="required" class="chosen-select" >
                                                     <?php 
													 $job_education='';
													 if(isset($edit_job_data['job_education']) && $edit_job_data['job_education']!='')
														  {
															  $job_education = explode(',',$edit_job_data['job_education']);                                                          }
													 echo $this->common_front_model->get_list('edu_list','','','str',$job_education); ?>											</select>
													</div>
                                                    </div>
												</div>
											</div>
									</div>
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['currently_hiring']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select data-validation="required" name="currently_hiring_status" id="currently_hiring_status"   class="form-control" >
                                                      <option value=""><?php echo $custom_lable_array['currently_hiring']; ?></option>
                                                      <option value="Yes" <?php if(isset($edit_job_data['currently_hiring_status']) && $edit_job_data['currently_hiring_status']=='Yes') {echo "selected";} ?>>Yes</option>
                                                      <option value="No" <?php if(isset($edit_job_data['currently_hiring_status']) && $edit_job_data['currently_hiring_status']=='No') {echo "selected";} ?>>No</option>
                                                       </select>
													</div>
                                                    </div>
												</div>
											</div>
									</div>    
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">	
                                                    <h5><?php echo $custom_lable_array['desire_candidate_profile']; ?></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      
                                                      <textarea style="border-radius:0px;"rows="2" cols="2" name="desire_candidate_profile" class="form-control"  placeholder="<?php echo  $custom_lable_array['desire_candidate_profile'];  ?>"><?php if(isset($edit_job_data['desire_candidate_profile'])) {echo $edit_job_data['desire_candidate_profile'];} ?></textarea>
                                                     
													</div>
                                                    </div>
												</div>
											</div>
										</div>     
                                
                                  <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_payment']; ?> </h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_payment" id="job_payment" class="form-control" >
                                                     <?php 
													 $job_payment='';
													 if(isset($edit_job_data['job_payment']) && $edit_job_data['job_payment']!='')
														  {
															  $job_payment = explode(',',$edit_job_data['job_payment']);                                                          }
													 echo $this->common_front_model->get_list('job_payment_list','','','str',$job_payment); ?>											</select>
													</div>
                                                    </div>
												</div>
											</div>
									</div>
                                    
                                   <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['job_life']; ?> </h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="job_life" id="job_life" class="form-control"  >
                                                      
													  <?php
													   if(isset($edit_job_data) && $edit_job_data!='')
													   { ?>
                                                       <option selected value="<?php echo $edit_job_data['job_life']; ?>"><?php echo $edit_job_data['job_life']; ?></option>
                                                      <?php  }
													  else
													  { 
													  	 $get_empid = $this->common_front_model->get_empid();
													     $where = array('emp_id'=>$get_empid ,'current_plan'=>'Yes');
													     $get_job_life = $this->common_front_model->get_count_data_manual('plan_employer',$where,'1','job_life');
													  ?>
                                                      
                                                       <option selected value="<?php echo $get_job_life['job_life']; ?>"><?php echo $get_job_life['job_life']; ?></option>
                                                      
													  <?php }
													   ?>
													 
												
                                                      
                                                     </select>
													</div>
                                                    </div>
												</div>
											</div>
									</div>   
                                             
                                <div class="row">
											<div class="col-md-12">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<div class="form-group">
                                                    <h5><?php echo $custom_lable_array['link_job']; ?> </h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <input type="url" name="link_job" class="form-control" id="link_job" style="border-radius:0px;" value="<?php  if(isset($edit_job_data['link_job']) ) {echo $edit_job_data['link_job'] ;} ?>" placeholder="<?php echo $custom_lable_array['specify'] .' '. $custom_lable_array['link_job'];  ?>">
													</div>
                                                    </div>
												</div>
											</div>
									</div>
                                    <?php
									if(isset($edit_job_data['job_salary_from']) && $edit_job_data['job_salary_from']!='')
									{
									$salary = $edit_job_data['job_salary_from'];
									if(is_numeric($salary))
									  {
										  $selected_salary = 'Define';
									  }
									  else
									  {
										  $selected_salary = 'Company_rule';
									  }
									}
									?> 
                                <div class="row">
                                     <div class="col-md-12">
										<div class="col-md-3"></div>
										<div class="col-sm-6">
                                        <div class="form-group">
                                                    <h5><?php echo $custom_lable_array['annual_salary']; ?> <span class="red-only"> *</span></h5>
													<div class="input-group">
													  <span class="input-group-addon"></span>
                                                      <select name="define_salary" id="define_salary" onChange="check_salary_type();" data-validation="required" class="form-control" >
														<option value=""><?php echo $custom_lable_array['select_option'] ?></option>
														<option value="Define" <?php if(isset($selected_salary) && $selected_salary=='Define') {echo "selected";} ?>><?php echo $custom_lable_array['define_salary'] ?></option>
														<option value="Company_rule" <?php if(isset($selected_salary) && $selected_salary=='Company_rule') {echo "selected";} ?>><?php echo $custom_lable_array['company_rule']; ?></option>
											 </select>
										</div>
                                       </div>
                                        
                                        <div class="margin-top-20"></div>
                                          <div id="salary_drop_list_div">

												 <select name="salary_from" data-validation="required" class="form-control">
                                                     <?php 
													 $salary_from='';
													 if(isset($edit_job_data['job_salary_from']) &&  $edit_job_data['job_salary_from']!='')
													  {
														  $salary_from = explode(',',$edit_job_data['job_salary_from']);
													  }
													echo $this->common_front_model->get_list('salary_range_list','','','str',$salary_from); ?>
                                                     </select>
											
												
                                          	<?php /*?><div class="col-sm-4 nopadding tufelpaleft15" style="padding-left:0px;">
											     <select name="currency_type" id="salary_currency_type"  data-validation="required"  class="form-control" >					
												<?php 
												$currency_type='';
											 if(isset($edit_job_data['currency_type']) && $edit_job_data['currency_type']!='')
												  {
													  $currency_type = explode(',',$edit_job_data['currency_type']);                                                   }
												
												echo  $this->common_front_model->get_list('currency_master','','','str',$currency_type); ?>
											   </select>
											</div>
                                            
											<div class="col-sm-4 nopadding">
											<select name="salary_from" id="salary_from"   data-validation="required" class="form-control" onchange="get_salary_from()" >
												<option value=""><?php echo $custom_lable_array['from'] ?></option>
												
                                                <?php

											for($i=1;$i <= 99 ; $i=$i+0.5)
											{ $explode = explode('.',$i);
											  
											  $m_val = '0' ;
											  if(isset($explode[1]) && $explode[1]!='' && $explode[1]!='0')
											  {
												  $m_val = $explode[1];
												  $display = $explode[0].'.'.$m_val;
											  }
											  else
											  {
												  $display = $explode[0];
											  }
											  $val = $explode[0].'.'.$m_val;
											 ?>
											 <option value="<?php echo  $val; ?>" <?php if(isset($edit_job_data['job_salary_from']) && $edit_job_data['job_salary_from']==$val) {echo "selected";} ?> ><?php   echo $display. ' Lacs '; ?></option>	
										<?php }
											?>

											</select>
											</div>
                                            <div id="new_select" class="col-sm-4 nopadding " style="padding-right:0px;">
        									</div>
											<div class="col-sm-4 nopadding tufelparight15" style="padding-right:0px;">
											  <select name="salary_to"   data-validation="required" id="salary_to"  class="form-control">
												<option value=""><?php echo $custom_lable_array['to'] ?></option>
											<?php
											
											for($i_to=1;$i_to <= 99 ; $i_to=$i_to+0.5)
											{ $explode = explode('.',$i_to);
											  
											  $m_val = '0' ;
											  if(isset($explode[1]) && $explode[1]!='' && $explode[1]!='0')
											  {
												  $m_val = $explode[1];
												  $display = $explode[0].'.'.$m_val;
											  }
											  else
											  {
												  $display = $explode[0];
											  }
											  $val = $explode[0].'.'.$m_val;
											 ?>
											 <option value="<?php echo  $val; ?>" <?php if(isset($edit_job_data['job_salary_to']) && $edit_job_data['job_salary_to']==$val) {echo "selected";} ?>><?php   echo $display. ' Lacs '; ?>
                                             </option>	
										<?php }
											?>
										
											 
											</select>
											</div><?php */?>

                                          </div>
                                          
                                        <div id="salary_text_list_div">
                                        (<span id="pres-max-length" class="red-only">100 </span> <?php echo $custom_lable_array['character_left'] ?> )
                                        	<input type="text"   data-validation="required" name="job_salary_text" class="form-control" id="job_salary_text" style="border-radius:0px;" data-validation-length="0-100"  placeholder="<?php echo $custom_lable_array['specify'];  ?>" value="<?php if(isset($selected_salary) && $selected_salary =='Company_rule' ) {echo $edit_job_data['job_salary_from'];}else{echo $custom_lable_array['company_rule'];} ?>">
                                        </div>
                                        
										</div>
                                     </div>
                                   </div>  
                                         
                                
								</div>
								
                                
									<div class="margin-bottom-20"></div>
									
									<ul class="list-inline pull-right margin-top-20">
									
										<li><input type="submit" class="btn btn-primary btn-info-full next-step" value="<?php echo $custom_lable_array['save']; ?>"> </li>
									</ul>
								
                       <input name="action" type="hidden"  value="save_job" />
                       <input name="user_agent" type="hidden"  value="NI-WEB" />
                       <?php
					   if(isset($edit_job_data) && $edit_job_data!='')
					   { ?>
                       <input name="mode" type="hidden"  value="edit" />
                       <input name="id" type="hidden"  value="<?php echo $edit_job_data['id']; ?>" />
					   <?php }
					   ?>
                                </div>
                               <div class="clear"></div>
                        </form>
						
						</div>
                        <div class="margin-top-20"></div>
						
					</div>
<?php } ?>                    
			</div> 
		</section>
   </div>
</div>
<?php } 
?>
<!--<link href="<?php echo $base_url;?>assets/front_end/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo $base_url;?>assets/front_end/css/theme-default.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $base_url; ?>assets/front_end/css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
<link href="<?php echo $base_url; ?>assets/front_end/css/select2.min.css" type="text/css" rel="stylesheet">

<!--<script src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-datepicker.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/select2.full.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url; ?>assets/front_end/js/bootstrap-tokenfield.js" charset="UTF-8"></script>

<script>

function get_suggestion_list(list_id,return_val,get_list)
{
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	$('#'+list_id).select2({
	 placeholder: "Select a language",
	  ajax: {
		url: base_url+'sign_up/get_suggestion_list/',
		type: "POST",
		dataType:'json',
		data: function (params) {
		  return {
			q: params.term, // search term
			page: params.page,
			csrf_job_portal: hash_tocken_id,
			return_val: return_val,
			get_list: get_list,
		  };
		},
	  }
	});
}	

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

function check_salary_type()
{
	var s_val = $('#define_salary').val();
	if(s_val=='Define')
	{
		$('#salary_drop_list_div').slideDown();
		$('#salary_text_list_div').slideUp();
		$('#job_salary_text').val('');
	}
	else
	{
		$('#salary_drop_list_div').slideUp();
		$('#salary_text_list_div').slideDown();
		$('#salary_to').val('');
		$('#salary_from').val('');
		//$('#salary_to').trigger('chosen:updated');
		//$('#salary_from').trigger('chosen:updated');
	}
}

$('#skill_keyword').tokenfield({
    autocomplete: {
    source: <?php echo $key_skill_data; ?> ,
    delay: 100,
  },
  showAutocompleteOnFocus: true,

})
$('#skill_keyword').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});




function for_active_tab()
{
	//$('.chosen-select').css('display','none');
	var $active = $('.wizard .nav-tabs li.active');
	$active.next().removeClass('disabled');
	nextTab($active);
}

/*
$('select')
    .on('beforeValidation', function(value, lang, config) {
      $('.chosen-select').css('display','block');
	 });
$('select')
    .on('validation', function(value, lang, config) {
      $('.chosen-select').css('display','none');
	  //$('.chosen-select').trigger('click');
 });	*/


/*$('.chosen-select').click(function(){
	$('.chosen-select').css('display','none');
});	 
*/
$('#job_salary_text').restrictLength( $('#pres-max-length') );
	
$.validate({
    form : '#post_jobs',
    modules : 'security',
    onError : function($form) {
	  scroll_to_div('wrapper',-100);
	  //$('.chosen-select').css('display','none');
      $("#error_msg").slideDown();
	  $("#error_msg").focus();
	  $("#error_msg").html("<?php echo $custom_lable_array['mandatory_field_msg']; ?>");
	  return false;
    },
    onSuccess : function($form) {
	  show_comm_mask();
	  $("#error_msg").slideUp();
	  var datastring = $("#post_jobs").serialize();
	  var hash_tocken_id = $("#hash_tocken_id").val();
	  var datastring = datastring + "&csrf_job_portal="+hash_tocken_id;
	
		$.ajax({
		url : "<?php echo $base_url.'job_listing/save_job' ?>",
		type: 'post',
		data: datastring,
		dataType:"json",
		success: function(data)
		{
			$("#hash_tocken_id").val(data.token);
			if($.trim(data.status) == 'success')
			{
				for_active_tab();
				$("#error_msg").slideUp();
				$("#success_msg").slideDown();
				$("#success_msg").html(data.errmessage);
				//$('.chosen-select').css('display','none');
				
				<?php if(!isset($edit_job_data))
				{ ?>
				document.getElementById('post_jobs').reset();
				$('.chosen-select').removeAttr('selected');
				$('#skill_keyword').tokenfield('destroy');
				$('#skill_keyword').val('');
				$('#location_hiring').val('');
				$('#location_hiring').trigger('change');
				$('.chosen-select').trigger('chosen:updated');
				$('#salary_drop_list_div').hide();
				$('#salary_text_list_div').hide();
	
			<?php } ?>
				scroll_to_div('wrapper');
			}
			else
			{
				//$('.chosen-select').css('display','none');
				$("#success_msg").slideUp();
				$("#error_msg").html(data.errmessage);
				$("#error_msg").slideDown();
				scroll_to_div('wrapper');
			}
			 hide_comm_mask();
		}
	});	
	 
      return false; // Will stop the submission of the form
    }
  });

 
 

$(document).ready(function(){
	
	$('#salary_drop_list_div').hide();
	$('#salary_text_list_div').hide();
	get_suggestion_list('location_hiring','id','city_list');
	<?php
		if(isset($selected_salary) && $selected_salary == 'Define')
		{
			?>
			   $('#salary_drop_list_div').show();
			<?php
		}
		else if(isset($selected_salary) && $selected_salary == 'Company_rule')
		{
			?>
			  $('#salary_text_list_div').show();
			<?php
		}
	?>
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });


   $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
	
	
	
	$('.accordion-header').toggleClass('inactive-header');

	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});

	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');

	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}

		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	
	
});

$(document).on("change", "#salary_from", function()
{
	document.getElementById("salary_to").innerHTML = "";
	$('#salary_to').val();
    var salary_from = this.value;
	//alert(salary_from);
    GetSalaryTo(salary_from);  
});
function GetSalaryTo(salary_from) 
{
	var salary_from = document.getElementById('salary_from').value;
	var base_url = '<?php echo $base_url ; ?>';
	var hash_tocken_id = $("#hash_tocken_id").val();
	var datastring = "csrf_job_portal="+hash_tocken_id+"&salary_to="+salary_from;
	
	$.ajax({
		url : "<?php echo $base_url.'job_listing/get_salary_to/' ?>",
		type: 'post',
		dataType:'json',
		data: datastring,
		success: function(data)
		{
			//alert(data.salary_to);
			var t = data.salary_to;
			var x = 0.5;
		   for(var i=t; i<=99; i=parseFloat(i) + parseFloat(x))
		   {
				 $('#salary_to').append("<option value="+parseFloat(i)+">"+parseFloat(i)+" Lacs</option>");
		   }
		}
	});

}

</script>
