<?php

header("access-control-allow-origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
defined('BASEPATH') OR exit('No direct script access allowed');
//use Restserver\Libraries\REST_Controller;
require APPPATH .'/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;


class MY_Controller extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data = array();
        $config['protocol'] = 'smtp';
        $config['charset'] = 'utf-8';
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['smtp_port'] = SMTP_PORT;
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->load->library('email');

        $this->data["site_property"] = $this->site_model->get_site_properties();
        $this->data["social_links"] = $this->db->get("social_media")->row();
        $this->orders_cron_model->check_order_status_and_payment_status();

        $this->db->where("created_at <= ", strtotime("-12 hours"));
        $this->db->delete("temp_cart");

        $config_aws = array(
            'bucket_name' => AWS_BUCKET_NAME,
            'region' => AWS_SERVER_REGION,
            'scheme' => AWS_SERVER_SCHEME
        );

        $this->load->library('cis3integration_lib', $config_aws);
    }

    function admin_view($design = null) {
        $this->load->view("admin/includes/header", $this->data);
        //$this->load->view("admin/");
        $this->load->view("admin/includes/footer", $this->data);
    }

    function test_view($design = null) {
        $this->load->view("test/header", $this->data);
        //$this->load->view("admin/");
        $this->load->view("test/footer", $this->data);
    }

    function front_view($design = null) {
        $this->load->view("includes/header", $this->data);
        $this->load->view($design);
        $this->load->view("includes/footer", $this->data);
    }

    function check_login_status($user_id) {
        if ($this->user_model->check_user_status($user_id) == false) {
            $this->session->set_flashdata("type", "inactive");
            redirect("login");
        }
        if ($this->user_model->check_mobile_verified($user_id) == false) {
            $this->session->set_flashdata("type", "email_inactive");
            redirect("login");
        }
        return true;
    }

    function get_user_id($token) {
        $user_id = $this->user_model->get_user_id_by_token($token);
        return $user_id;
    }

    function is_user_logged($is_redirect = false) {
        if (!$this->session->userdata('user_id')) {
            if ($is_redirect == "redirect_to_professional_registration") {
                redirect("professional/register?no_redirect=true");
            } else if ($is_redirect == "redirect_to_customer_registration") {
                redirect("customer/register?no_redirect=true");
            } else {
                redirect("login");
            }
            return false;
        } else {
            $user_id = $this->session->userdata('user_id');
            return $this->check_login_status($user_id);
        }
    }

    function routing_process() {
        $user_id = $this->session->userdata("user_id");
        $role = $this->user_model->get_user_role_id($user_id);
        switch ($role) {
            case 5:
                redirect("franchise/dashboard");
            case 3:
                redirect("customer/dashboard");
            case 4:
                redirect("vendor/dashboard");
            case 1:
                redirect("admin/login");
        }
    }

    function is_token_required() {
        if (!$this->input->get_post('token')) {
            $arr = array('err_code' => "invalid", "error_type" => "token_required", "message" => "Unauthorized access..");
            echo json_encode($arr);
            die;
        }
    }

    function check_about_subscription($user_id) {
        return true;
        $response = $this->customer_model->check_subscription_plan_is_expired($user_id);
        if ($response == "SUBSCRITPION_IS_REQUIRED") {
            redirect("customer/subscription_plans?err_type=SUBSCRITPION_IS_REQUIRED");
        } else if ($response == "SUBSCRITPION_IS_EXPIRED") {
            redirect("customer/subscription_plans?err_type=SUBSCRITPION_IS_EXPIRED");
        } else {
            return true;
        }
    }

    function vendor_view($design = null) {

        if ($this->vendor_model->check_for_user_logged() == true) {
            $vendor_id = $this->session->userdata("vendor_id");
            $this->data["restaurant_details"] = $this->vendor_model->get_vendor_details($vendor_id);
        }

        $this->load->view("vendor/includes/header", $this->data);
        $this->load->view("vendor/" . $design);
        $this->load->view("vendor/includes/footer", $this->data);
    }

    function show_under_construction() {
        echo "Under Construction";
        die;
    }

}
